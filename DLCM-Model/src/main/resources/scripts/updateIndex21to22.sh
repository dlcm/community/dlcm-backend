#!/bin/bash
ES_HOST=:9200
DLCM_SRC=/Users/von/Documents/src/dlcm-backend

# Create new indices
http PUT $ES_HOST/yareta2
http PUT $ES_HOST/yareta2private
http PUT $ES_HOST/yareta2registry

# Put new settings in public index
http POST $ES_HOST/yareta2/_close
cat $DLCM_SRC/DLCM-DataMgmt/src/main/resources/mapping/settings.json | http PUT $ES_HOST/yareta2/_settings
http POST $ES_HOST/yareta2/_open

# Put new mapping
cat $DLCM_SRC/DLCM-DataMgmt/src/main/resources/mapping/index.json | http PUT $ES_HOST/yareta2/_mapping
cat $DLCM_SRC/DLCM-DataMgmt/src/main/resources/mapping/indexprivate.json | http PUT $ES_HOST/yareta2private/_mapping

# Reindex from old index
echo '{"source":{"index":"yareta"},"dest":{"index":"yareta2"}}' | http POST $ES_HOST/_reindex
echo '{"source":{"index":"yaretaprivate"},"dest":{"index":"yareta2private"}}' | http POST $ES_HOST/_reindex
echo '{"source":{"index":"yaretaregistry"},"dest":{"index":"yareta2registry"}}' | http POST $ES_HOST/_reindex

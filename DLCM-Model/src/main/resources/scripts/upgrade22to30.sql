/*
 Dec. 2024
 commitID=
*/
DROP TABLE org_unit_dissemination_policies;


/*
 Sept. 2024
 commitID=
 Upgrade sequence (Spring Boot 3)
*/
SET @next_hibernate_sequence_val = NEXT VALUE FOR hibernate_sequence;
SET @next_status_history_seq_val = @next_hibernate_sequence_val + 1000; -- Increment by 1000 which is the cache size

DELIMITER //

CREATE PROCEDURE create_status_sequence(IN seq_start_value INT)
BEGIN
    SET @create_sequence_sql = CONCAT('CREATE SEQUENCE status_history_seq START WITH ', seq_start_value, ' INCREMENT BY 50');
    PREPARE stmt FROM @create_sequence_sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END //

DELIMITER ;

CALL create_status_sequence(@next_status_history_seq_val);

-- additional_fields_form
ALTER TABLE `additional_fields_form`
    MODIFY COLUMN `type` enum('FORMLY') NOT NULL;

-- aip
ALTER TABLE `aip`
    MODIFY COLUMN `status` enum('CHECK_PENDING','CHECKING','CHECKED','CLEANING','CLEANED','COMPLETED','DOWNLOADING','IN_PREPARATION','IN_PROGRESS','STORED','INDEXING','READY','IN_ERROR','PRESERVATION_ERROR','DISPOSABLE','DISPOSAL_APPROVED_BY_ORGUNIT','DISPOSAL_APPROVED','DISPOSED','FIX_PENDING','FIXING','METADATA_EDITION_PENDING','EDITING_METADATA','UPDATING_RETENTION','REINDEXING','RELOADED','RESUBMITTING','PACKAGE_REPLICATION_PENDING','REPLICATING_PACKAGE','TOMBSTONE_REPLICATION_PENDING','REPLICATING_TOMBSTONE', 'METADATA_UPGRADE_PENDING', 'UPGRADING_METADATA', 'COMPLIANCE_LEVEL_UPDATE_PENDING', 'UPDATING_COMPLIANCE_LEVEL') DEFAULT NULL;
ALTER TABLE `aip`
    MODIFY COLUMN `data_sensitivity` enum('UNDEFINED','BLUE','GREEN','YELLOW','ORANGE','RED','CRIMSON') DEFAULT NULL;
ALTER TABLE `aip`
    MODIFY COLUMN `data_use_policy` enum('NONE','LICENSE','CLICK_THROUGH_DUA','SIGNED_DUA','EXTERNAL_DUA') DEFAULT NULL;
ALTER TABLE `aip`
    MODIFY COLUMN `archive_container` enum('UNDEFINED','ZIP','BAG_IT') NOT NULL;
ALTER TABLE `aip`
    MODIFY COLUMN `access` enum('PUBLIC','RESTRICTED','CLOSED') DEFAULT NULL;
ALTER TABLE `aip`
    MODIFY COLUMN `embargo_access` enum('PUBLIC','RESTRICTED','CLOSED') DEFAULT NULL;

-- aip_checksum
ALTER TABLE `aip_checksum`
    MODIFY COLUMN `checksum_algo` enum('CRC32','MD5','SHA1','SHA256') DEFAULT NULL;
ALTER TABLE `aip_checksum`
    MODIFY COLUMN `checksum_origin` enum('DLCM','DLCM_TOMBSTONE','USER','PORTAL') DEFAULT NULL;
ALTER TABLE `aip_checksum`
    MODIFY COLUMN `checksum_type` enum('COMPLETE','PARTIAL') DEFAULT NULL;


-- archive_public_data
ALTER TABLE `archive_public_data`
    MODIFY COLUMN `data_file_type` enum('ARCHIVE_THUMBNAIL','ARCHIVE_README','ARCHIVE_DUA') DEFAULT NULL;

-- aip_data_file_checksum
ALTER TABLE `aip_data_file_checksum`
    MODIFY COLUMN `checksum_algo` enum('CRC32','MD5','SHA1','SHA256') DEFAULT NULL;
ALTER TABLE `aip_data_file_checksum`
    MODIFY COLUMN `checksum_origin` enum('DLCM','DLCM_TOMBSTONE','USER','PORTAL') DEFAULT NULL;
ALTER TABLE `aip_data_file_checksum`
    MODIFY COLUMN `checksum_type` enum('COMPLETE','PARTIAL') DEFAULT NULL;

-- aip_data_file
ALTER TABLE `aip_data_file`
    MODIFY COLUMN `data_category` enum('Primary','Observational','Experimental','Simulation','Derived','Reference','Digitalized','Secondary','Publication','DataPaper','Documentation','Software','Code','Binaries','VirtualMachine','Administrative','Document','WebSite','Other','Package','InformationPackage','UpdatePackage','Metadata','CustomMetadata','UpdatedMetadata','Internal','DatasetThumbnail','ArchiveThumbnail','ArchiveReadme','DatafileThumbnail','ArchiveDataUseAgreement') DEFAULT NULL;
ALTER TABLE `aip_data_file`
    MODIFY COLUMN `data_type` enum('Primary','Observational','Experimental','Simulation','Derived','Reference','Digitalized','Secondary','Publication','DataPaper','Documentation','Software','Code','Binaries','VirtualMachine','Administrative','Document','WebSite','Other','Package','InformationPackage','UpdatePackage','Metadata','CustomMetadata','UpdatedMetadata','Internal','DatasetThumbnail','ArchiveThumbnail','ArchiveReadme','DatafileThumbnail','ArchiveDataUseAgreement') DEFAULT NULL;
ALTER TABLE `aip_data_file`
    MODIFY COLUMN `status` enum('CHANGE_RELATIVE_LOCATION','CHANGE_DATA_CATEGORY','CLEANED','CLEANING','EXCLUDED_FILE','CHECK_COMPLIANCE','CHECK_COMPLIANCE_CLEANED','CHECKED_COMPLIANCE','CHECKED_COMPLIANCE_CLEANED','IGNORED_FILE','FILE_FORMAT_IDENTIFIED','FILE_FORMAT_SKIPPED','FILE_FORMAT_UNKNOWN','IN_ERROR','PROCESSED','READY','RECEIVED','TO_PROCESS','VIRUS_CHECKED','VIRUS_SKIPPED') DEFAULT NULL;

-- deposit
ALTER TABLE `deposit`
    MODIFY COLUMN `access` enum('PUBLIC','RESTRICTED','CLOSED') DEFAULT NULL;
ALTER TABLE `deposit`
    MODIFY COLUMN `embargo_access` enum('PUBLIC','RESTRICTED','CLOSED') DEFAULT NULL;
ALTER TABLE `deposit`
    MODIFY COLUMN `status` enum('APPROVED','CHECKED','CLEANED','CLEANING','COMPLETED','DELETING','IN_ERROR','IN_PROGRESS','IN_VALIDATION','REJECTED','SUBMITTED','EDITING_METADATA','UPGRADING_METADATA','CHECKING_COMPLIANCE','CHECKING_COMPLIANCE_CLEANED','COMPLIANCE_ERROR','CANCEL_EDITING_METADATA','EDITING_METADATA_REJECTED','SUBMISSION_AGREEMENT_APPROVED') DEFAULT NULL;

-- deposit_data_file
ALTER TABLE `deposit_data_file`
    MODIFY COLUMN `data_category` enum('Primary','Observational','Experimental','Simulation','Derived','Reference','Digitalized','Secondary','Publication','DataPaper','Documentation','Software','Code','Binaries','VirtualMachine','Administrative','Document','WebSite','Other','Package','InformationPackage','UpdatePackage','Metadata','CustomMetadata','UpdatedMetadata','Internal','DatasetThumbnail','ArchiveThumbnail','ArchiveReadme','DatafileThumbnail','ArchiveDataUseAgreement') DEFAULT NULL;
ALTER TABLE `deposit_data_file`
    MODIFY COLUMN `data_type` enum('Primary','Observational','Experimental','Simulation','Derived','Reference','Digitalized','Secondary','Publication','DataPaper','Documentation','Software','Code','Binaries','VirtualMachine','Administrative','Document','WebSite','Other','Package','InformationPackage','UpdatePackage','Metadata','CustomMetadata','UpdatedMetadata','Internal','DatasetThumbnail','ArchiveThumbnail','ArchiveReadme','DatafileThumbnail','ArchiveDataUseAgreement') DEFAULT NULL;
ALTER TABLE `deposit_data_file`
    MODIFY COLUMN `data_type` enum('Primary','Observational','Experimental','Simulation','Derived','Reference','Digitalized','Secondary','Publication','DataPaper','Documentation','Software','Code','Binaries','VirtualMachine','Administrative','Document','WebSite','Other','Package','InformationPackage','UpdatePackage','Metadata','CustomMetadata','UpdatedMetadata','Internal','DatasetThumbnail','ArchiveThumbnail','ArchiveReadme','DatafileThumbnail','ArchiveDataUseAgreement') DEFAULT NULL;
ALTER TABLE `deposit_data_file`
    MODIFY COLUMN `status` enum('CHANGE_RELATIVE_LOCATION','CHANGE_DATA_CATEGORY','CLEANED','CLEANING','EXCLUDED_FILE','CHECK_COMPLIANCE','CHECK_COMPLIANCE_CLEANED','CHECKED_COMPLIANCE','CHECKED_COMPLIANCE_CLEANED','IGNORED_FILE','FILE_FORMAT_IDENTIFIED','FILE_FORMAT_SKIPPED','FILE_FORMAT_UNKNOWN','IN_ERROR','PROCESSED','READY','RECEIVED','TO_PROCESS','VIRUS_CHECKED','VIRUS_SKIPPED') DEFAULT NULL;

-- deposit_data_file_checksum
ALTER TABLE `deposit_data_file_checksum`
    MODIFY COLUMN `checksum_algo` enum('CRC32','MD5','SHA1','SHA256') DEFAULT NULL;
ALTER TABLE `deposit_data_file_checksum`
    MODIFY COLUMN `checksum_origin` enum('DLCM','DLCM_TOMBSTONE','USER','PORTAL') DEFAULT NULL;
ALTER TABLE `deposit_data_file_checksum`
    MODIFY COLUMN `checksum_type` enum('COMPLETE','PARTIAL') DEFAULT NULL;

-- dip
ALTER TABLE `dip`
    MODIFY COLUMN `access` enum('PUBLIC','RESTRICTED','CLOSED') DEFAULT NULL;
ALTER TABLE `dip`
    MODIFY COLUMN `embargo_access` enum('PUBLIC','RESTRICTED','CLOSED') DEFAULT NULL;
ALTER TABLE `dip`
    MODIFY COLUMN `data_use_policy` enum('NONE','LICENSE','CLICK_THROUGH_DUA','SIGNED_DUA','EXTERNAL_DUA') DEFAULT NULL;
ALTER TABLE `dip`
    MODIFY COLUMN `data_sensitivity` enum('UNDEFINED','BLUE','GREEN','YELLOW','ORANGE','RED','CRIMSON') DEFAULT NULL;
ALTER TABLE `dip`
    MODIFY COLUMN `status` enum('CHECK_PENDING','CHECKING','CHECKED','CLEANING','CLEANED','COMPLETED','DOWNLOADING','IN_PREPARATION','IN_PROGRESS','STORED','INDEXING','READY','IN_ERROR','PRESERVATION_ERROR','DISPOSABLE','DISPOSAL_APPROVED_BY_ORGUNIT','DISPOSAL_APPROVED','DISPOSED','FIX_PENDING','FIXING','METADATA_EDITION_PENDING','EDITING_METADATA','UPDATING_RETENTION','REINDEXING','RELOADED','RESUBMITTING','PACKAGE_REPLICATION_PENDING','REPLICATING_PACKAGE','TOMBSTONE_REPLICATION_PENDING','REPLICATING_TOMBSTONE', 'METADATA_UPGRADE_PENDING', 'UPGRADING_METADATA') DEFAULT NULL;

-- dip_data_file
ALTER TABLE `dip_data_file`
    MODIFY COLUMN `data_category` enum('Primary','Observational','Experimental','Simulation','Derived','Reference','Digitalized','Secondary','Publication','DataPaper','Documentation','Software','Code','Binaries','VirtualMachine','Administrative','Document','WebSite','Other','Package','InformationPackage','UpdatePackage','Metadata','CustomMetadata','UpdatedMetadata','Internal','DatasetThumbnail','ArchiveThumbnail','ArchiveReadme','DatafileThumbnail','ArchiveDataUseAgreement') DEFAULT NULL;
ALTER TABLE `dip_data_file`
    MODIFY COLUMN `data_type` enum('Primary','Observational','Experimental','Simulation','Derived','Reference','Digitalized','Secondary','Publication','DataPaper','Documentation','Software','Code','Binaries','VirtualMachine','Administrative','Document','WebSite','Other','Package','InformationPackage','UpdatePackage','Metadata','CustomMetadata','UpdatedMetadata','Internal','DatasetThumbnail','ArchiveThumbnail','ArchiveReadme','DatafileThumbnail','ArchiveDataUseAgreement') DEFAULT NULL;
ALTER TABLE `dip_data_file`
    MODIFY COLUMN `status` enum('CHANGE_RELATIVE_LOCATION','CHANGE_DATA_CATEGORY','CLEANED','CLEANING','EXCLUDED_FILE','CHECK_COMPLIANCE','CHECK_COMPLIANCE_CLEANED','CHECKED_COMPLIANCE','CHECKED_COMPLIANCE_CLEANED','IGNORED_FILE','FILE_FORMAT_IDENTIFIED','FILE_FORMAT_SKIPPED','FILE_FORMAT_UNKNOWN','IN_ERROR','PROCESSED','READY','RECEIVED','TO_PROCESS','VIRUS_CHECKED','VIRUS_SKIPPED') DEFAULT NULL;

-- dip_data_file_checksum
ALTER TABLE `dip_data_file_checksum`
    MODIFY COLUMN `checksum_algo` enum('CRC32','MD5','SHA1','SHA256') DEFAULT NULL;
ALTER TABLE `dip_data_file_checksum`
    MODIFY COLUMN `checksum_origin` enum('DLCM','DLCM_TOMBSTONE','USER','PORTAL') DEFAULT NULL;
ALTER TABLE `dip_data_file_checksum`
    MODIFY COLUMN `checksum_type` enum('COMPLETE','PARTIAL') DEFAULT NULL;

-- dissemination_policy
ALTER TABLE `dissemination_policy`
    MODIFY COLUMN `type` enum('IIIF','WEB') DEFAULT NULL;

-- global_banner
ALTER TABLE `global_banner`
    MODIFY COLUMN `type` enum('CRITICAL','WARNING','INFO') DEFAULT NULL;

-- license
ALTER TABLE `license`
    MODIFY COLUMN `od_conformance` enum('APPROVED','EMPTY','NOT_REVIEWED','REJECTED') DEFAULT NULL;
ALTER TABLE `license`
    MODIFY COLUMN `osd_conformance` enum('APPROVED','EMPTY','NOT_REVIEWED','REJECTED') DEFAULT NULL;
ALTER TABLE `license`
    MODIFY COLUMN `status` enum('ACTIVE','RETIRED','SUPERSEDED') DEFAULT NULL;

-- metadata_type
ALTER TABLE `metadata_type`
    MODIFY COLUMN `metadata_format` enum('CUSTOM','JSON','SCHEMA_LESS','XML') NOT NULL;

-- notification
ALTER TABLE `notification`
    MODIFY COLUMN `notification_mark` enum('READ','UNREAD') NOT NULL;
ALTER TABLE `notification`
    MODIFY COLUMN `notification_status` enum('APPROVED','PENDING','REFUSED','NON_APPLICABLE') NOT NULL;

-- order_query
ALTER TABLE `order_query`
    MODIFY COLUMN `query_type` enum('ADVANCED','DIRECT','SIMPLE') NOT NULL;
ALTER TABLE `order_query`
    MODIFY COLUMN `status` enum('DOWNLOADING','IN_DISSEMINATION_PREPARATION','IN_ERROR','IN_PREPARATION','IN_PROGRESS','READY','SUBMITTED') DEFAULT NULL;

-- preservation_job
ALTER TABLE `preservation_job`
    MODIFY COLUMN `job_recurrence` enum('DAILY','MONTHLY','ONCE','WEEKLY','YEARLY') NOT NULL;
ALTER TABLE `preservation_job`
    MODIFY COLUMN `job_type` enum('ARCHIVE_CHECK','CLEAN_SUBMISSION','SIMPLE_CLEAN_SUBMISSION','DISPOSAL','FIXITY','MIGRATION','ARCHIVE_PRELOAD_SMALL','ARCHIVE_PRELOAD_BIG','PURGE_SUBMISSION_TEMP_FILES','PURGE_ORDER','REBUILD_REGISTRY','REINDEX','REINDEX_ALL','RELOAD','REPLICATION','REPLICATION_CHECK','CHECK_COMPLIANCE_LEVEL') NOT NULL;
ALTER TABLE `preservation_job`
    ADD COLUMN `max_items` bigint(20) NOT NULL DEFAULT 0;
ALTER TABLE `preservation_job`
    ADD COLUMN `parameters` varchar(255) DEFAULT NULL;

-- preservation_job_execution
ALTER TABLE `preservation_job_execution`
    MODIFY COLUMN `status` enum('COMPLETED','CREATED','IN_ERROR','IN_PROGRESS','READY') DEFAULT NULL;

-- preservation_job_execution_report_line
ALTER TABLE `preservation_job_execution_report_line`
    MODIFY COLUMN `status` enum('ERROR','IGNORED','PROCESSED') NOT NULL;

-- scheduled_task
ALTER TABLE `scheduled_task`
    MODIFY COLUMN `task_type` enum('NOTIFY_DEPOSITS_TO_VALIDATE','NOTIFY_APPROVED_DEPOSITS_CREATOR','NOTIFY_COMPLETED_DEPOSITS_CREATOR','NOTIFY_COMPLETED_DEPOSITS_CONTRIBUTORS','NOTIFY_FORGOTTEN_DEPOSITS_TO_VALIDATE','NOTIFY_DEPOSITS_IN_ERROR','NOTIFY_COMPLETED_ARCHIVES_CREATOR','NOTIFY_COMPLETED_ARCHIVES_APPROVERS','NOTIFY_ORG_UNIT_CREATION_TO_VALIDATE','NOTIFY_JOIN_MEMBER_ORG_UNIT_TO_VALIDATE','NOTIFY_ARCHIVE_ACCESS_REQUEST') NOT NULL;

-- sip_data_file
ALTER TABLE `sip_data_file`
    MODIFY COLUMN `data_category` enum('Primary','Observational','Experimental','Simulation','Derived','Reference','Digitalized','Secondary','Publication','DataPaper','Documentation','Software','Code','Binaries','VirtualMachine','Administrative','Document','WebSite','Other','Package','InformationPackage','UpdatePackage','Metadata','CustomMetadata','UpdatedMetadata','Internal','DatasetThumbnail','ArchiveThumbnail','ArchiveReadme','DatafileThumbnail','ArchiveDataUseAgreement') DEFAULT NULL;
ALTER TABLE `sip_data_file`
    MODIFY COLUMN `data_type` enum('Primary','Observational','Experimental','Simulation','Derived','Reference','Digitalized','Secondary','Publication','DataPaper','Documentation','Software','Code','Binaries','VirtualMachine','Administrative','Document','WebSite','Other','Package','InformationPackage','UpdatePackage','Metadata','CustomMetadata','UpdatedMetadata','Internal','DatasetThumbnail','ArchiveThumbnail','ArchiveReadme','DatafileThumbnail','ArchiveDataUseAgreement') DEFAULT NULL;


-- sip_data_file_checksum
ALTER TABLE `sip_data_file_checksum`
    MODIFY COLUMN `checksum_algo` enum('CRC32','MD5','SHA1','SHA256') DEFAULT NULL;
ALTER TABLE `sip_data_file_checksum`
    MODIFY COLUMN `checksum_origin` enum('DLCM','DLCM_TOMBSTONE','USER','PORTAL') DEFAULT NULL;
ALTER TABLE `sip_data_file_checksum`
    MODIFY COLUMN `checksum_type` enum('COMPLETE','PARTIAL') DEFAULT NULL;

-- sip
ALTER TABLE `sip`
    MODIFY COLUMN `status` enum('CHECK_PENDING','CHECKING','CHECKED','CLEANING','CLEANED','COMPLETED','DOWNLOADING','IN_PREPARATION','IN_PROGRESS','STORED','INDEXING','READY','IN_ERROR','PRESERVATION_ERROR','DISPOSABLE','DISPOSAL_APPROVED_BY_ORGUNIT','DISPOSAL_APPROVED','DISPOSED','FIX_PENDING','FIXING','METADATA_EDITION_PENDING','EDITING_METADATA','UPDATING_RETENTION','REINDEXING','RELOADED','RESUBMITTING','PACKAGE_REPLICATION_PENDING','REPLICATING_PACKAGE','TOMBSTONE_REPLICATION_PENDING','REPLICATING_TOMBSTONE', 'METADATA_UPGRADE_PENDING', 'UPGRADING_METADATA') DEFAULT NULL;
ALTER TABLE `sip`
    MODIFY COLUMN `data_sensitivity` enum('UNDEFINED','BLUE','GREEN','YELLOW','ORANGE','RED','CRIMSON') DEFAULT NULL;
ALTER TABLE `sip`
    MODIFY COLUMN `data_use_policy` enum('NONE','LICENSE','CLICK_THROUGH_DUA','SIGNED_DUA','EXTERNAL_DUA') DEFAULT NULL;
ALTER TABLE `sip`
    MODIFY COLUMN `access` enum('PUBLIC','RESTRICTED','CLOSED') DEFAULT NULL;
ALTER TABLE `sip`
    MODIFY COLUMN `embargo_access` enum('PUBLIC','RESTRICTED','CLOSED') DEFAULT NULL;

-- submission_policy
ALter TABLE `submission_policy`
    MODIFY COLUMN `submission_agreement_type` enum('WITHOUT','ON_FIRST_DEPOSIT','FOR_EACH_DEPOSIT') DEFAULT NULL;


-- download token
ALTER TABLE download_token
    DROP COLUMN `type`;


--  Add new checksum algo enum SHA512
ALTER TABLE `aip_data_file_checksum`
    MODIFY COLUMN `checksum_algo` enum ('CRC32','MD5','SHA1','SHA256','SHA512') DEFAULT NULL;
ALTER TABLE `deposit_data_file_checksum`
    MODIFY COLUMN `checksum_algo` enum ('CRC32','MD5','SHA1','SHA256','SHA512') DEFAULT NULL;
ALTER TABLE `dip_data_file_checksum`
    MODIFY COLUMN `checksum_algo` enum ('CRC32','MD5','SHA1','SHA256','SHA512') DEFAULT NULL;
ALTER TABLE `sip_data_file_checksum`
    MODIFY COLUMN `checksum_algo` enum ('CRC32','MD5','SHA1','SHA256','SHA512') DEFAULT NULL;

-- archive user fingerprint
ALTER TABLE `archive_user_fingerprint`
    MODIFY COLUMN `type` enum('BACKEND_FINGERPRINT') NOT NULL;

-- dissemenation policy
ALTER TABLE dissemination_policy DROP COLUMN destination_server;
ALTER TABLE dissemination_policy DROP COLUMN sub_folder;
ALTER TABLE dissemination_policy MODIFY COLUMN `type` enum ('IIIF', 'HEDERA', 'PUBLIC', 'ARCHIVIST') DEFAULT NULL;
ALTER TABLE dissemination_policy
    ADD COLUMN parameters longtext DEFAULT NULL;

-- notifications
UPDATE notification
SET response_message = 'An explanation was not mandatory when the refusal occurred.'
WHERE notification_type_res_id IN ('ACCESS_DATASET_REQUEST', 'JOIN_ORGUNIT_REQUEST', 'CREATE_ORGUNIT_REQUEST', 'APPROVE_DISPOSAL_REQUEST', 'APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST')
  AND notification_status = 'REFUSED'
  AND response_message IS NULL;

-- order add dissemination_policy_id
ALTER TABLE `order_query`
    ADD COLUMN `dissemination_policy_id` VARCHAR(50) NOT NULL;

UPDATE `order_query`
    SET `dissemination_policy_id` = 'ARCHIVIST_DISSEMINATION_POLICY';

DROP TABLE `org_unit_dissemination_policies`;

DROP TABLE `dissemination_policy`;

UPDATE `order_query` set dissemination_policy_id = 'public-dissemination-policy' where dissemination_policy_id = 'PUBLIC_DISSEMINATION_POLICY';

UPDATE `order_query` set dissemination_policy_id = 'archivist-dissemination-policy' where dissemination_policy_id = 'ARCHIVIST_DISSEMINATION_POLICY';

DELETE FROM `order_aip` where order_id in (select res_id from `order_query` where dissemination_policy_id not in ('archivist-dissemination-policy', 'public-dissemination-policy'));

DELETE FROM `order_dip` where order_id in (select res_id from `order_query` where dissemination_policy_id not in ('archivist-dissemination-policy', 'public-dissemination-policy'));

DELETE FROM `order_subset_item` where order_res_id in (select res_id from `order_query` where dissemination_policy_id not in ('archivist-dissemination-policy', 'public-dissemination-policy'));

DELETE FROM `order_query` where dissemination_policy_id not in ('archivist-dissemination-policy', 'public-dissemination-policy');

-- update dissemenation policy type
ALTER TABLE dissemination_policy
    MODIFY COLUMN `type` enum ('IIIF', 'HEDERA', 'ARCHIVIST', 'PUBLIC', 'BASIC', 'OAIS') DEFAULT NULL;

UPDATE `dissemination_policy`
SET type = 'OAIS'
WHERE type = 'ARCHIVIST';

UPDATE `dissemination_policy`
SET type = 'BASIC'
WHERE type = 'PUBLIC';

UPDATE `dissemination_policy`
SET res_id = 'basic-dissemination-policy'
WHERE res_id = 'public-dissemination-policy';

UPDATE `dissemination_policy`
SET res_id = 'oais-dissemination-policy'
WHERE res_id = 'archivist-dissemination-policy';


UPDATE `dissemination_policy`
SET name = 'Dissemination policy for basic'
WHERE res_id = 'basic-dissemination-policy';

UPDATE `dissemination_policy`
SET name = 'Dissemination policy for oais'
WHERE res_id = 'oais-dissemination-policy';

ALTER TABLE dissemination_policy
    MODIFY COLUMN `type` enum ('IIIF', 'HEDERA', 'BASIC', 'OAIS') DEFAULT NULL;

UPDATE `order_query`
SET dissemination_policy_id = 'basic-dissemination-policy'
WHERE dissemination_policy_id = 'public-dissemination-policy';

UPDATE `order_query`
SET dissemination_policy_id = 'oais-dissemination-policy'
WHERE dissemination_policy_id = 'archivist-dissemination-policy';

ALTER TABLE `dissemination_policy`
    ADD COLUMN `download_filename` enum ('ARCHIVE_ID','ARCHIVE_NAME') DEFAULT NOT NULL;

ALTER TABLE `dissemination_policy`
    ADD COLUMN `prefix` VARCHAR(255) default NULL;

ALTER TABLE `dissemination_policy`
    ADD COLUMN `suffix` VARCHAR(255) default NULL;

UPDATE `dissemination_policy`
SET prefix ='', suffix ='', download_file_name= 'ARCHIVE_NAME'
WHERE res_id = 'basic-dissemination-policy';

UPDATE `dissemination_policy`
SET prefix ='', suffix ='oais', download_file_name= 'ARCHIVE_ID'
WHERE res_id = 'oais-dissemination-policy';

UPDATE `dissemination_policy`
SET prefix ='', suffix ='hedera', download_file_name= 'ARCHIVE_NAME'
WHERE res_id = 'hedera-dissemination-policy';


UPDATE `dissemination_policy`
SET prefix ='', suffix ='iiif', download_file_name= 'ARCHIVE_NAME'
WHERE res_id = 'iiif-dissemination-policy';

-- This query Creates a new JSON object with your new NAME field first
-- Merges it with the existing parameters (minus the new field)
-- Handles NULL cases properly
UPDATE `dissemination_policy`
SET parameters = JSON_MERGE_PRESERVE(
        JSON_OBJECT('name',
                    COALESCE(
                            JSON_EXTRACT(parameters, '$.name'),
                            ''
                    )
        ),
        COALESCE(
                JSON_REMOVE(parameters, '$.name'),
                '{}'
        )
                 )
WHERE type IN ('IIIF', 'HEDERA')
  AND parameters IS NOT NULL;

-- Implement IIIF dissemenation policy
-- add new column to order_query
ALTER TABLE order_query
    ADD COLUMN organizational_unit_dissemination_policy_id varchar(50) null;

-- add new parameters to dissemination policy of type IIIF.
UPDATE dissemination_policy
SET parameters = JSON_SET(
        CASE
            WHEN parameters IS NULL OR parameters = '' THEN '{}'
            ELSE parameters
            END,
        '$.mediaDirectory', 'media',
        '$.manifestDirectory', 'manifests',
        '$.mediaPattern', '**/*.jpeg',
        '$.manifestPattern', '**/*.json'
                 )
WHERE type IN ('IIIF')
  AND parameters IS NOT NULL;


update notification set sent_time = NOW() where sent_time is null;

-- add new column for order_query to store checksum
ALTER TABLE order_query
    ADD COLUMN checksum varchar(255) null;


ALTER TABLE dissemination_policy
    MODIFY COLUMN `type` enum ('IIIF', 'HEDERA', 'BASIC', 'OAIS') NOT NULL;

ALTER TABLE dissemination_policy
    MODIFY COLUMN download_file_name enum ('ARCHIVE_ID', 'ARCHIVE_NAME') NOT NULL;
/*
 Nov. 2022
 commitID=63f2f6e3
 This commit fixes a bug that could potentially generate citations/bibliography texts for the wrong archive. 
 The easiest way to fix this if it happened is to delete all citation table records and let them being automatically regenerated on demand.
*/

TRUNCATE citation;

/*
 Nov. 2022
 commitID=da6d26aa
 Update size of 'text' field for citations:
*/

ALTER TABLE citation CHANGE text text TEXT NOT NULL;

/*
 Nov. 2022
 commitID=b22b9ed9
 Solidify Image, rename image column in image_content:
*/

ALTER TABLE funding_agency_logo RENAME COLUMN image TO image_content;
ALTER TABLE institution_logo RENAME COLUMN image TO image_content;
ALTER TABLE license_logo RENAME COLUMN image TO image_content;
ALTER TABLE org_unit_logo RENAME COLUMN image TO image_content;
ALTER TABLE person_avatar RENAME COLUMN image TO image_content;

/*
 Jan. 2023
 commitID=4163e32c
 Add unique index on org_unit_person_roles table on orgUnit and Person.
 Beware to not start the application before the renaming otherwise there will be column person_id AND a column person_res_id.
*/

ALTER TABLE org_unit_person_roles RENAME COLUMN person_res_id TO person_id;
ALTER TABLE org_unit_person_roles RENAME COLUMN role_res_id TO role_id;
ALTER TABLE org_unit_person_roles ADD CONSTRAINT OrgUnit_Person UNIQUE KEY(org_unit_id,person_id);

/*
 Feb. 2023
 commitID=71338c35
 Change size of code column for research_domain table
*/

ALTER TABLE research_domain MODIFY COLUMN code VARCHAR(50);

/*
 Feb. 2023
 commitID=c3aa5e3c
*/

/* Move funding_agency_logo from institution_logo */
ALTER TABLE funding_agency DROP FOREIGN KEY FKb0ffjvmxh31bab5vfddbaaxj8;
INSERT INTO funding_agency_logo SELECT * FROM institution_logo WHERE res_id IN (SELECT il.res_id FROM institution_logo il, funding_agency fa WHERE il.res_id=fa.logo_id);
DELETE FROM institution_logo WHERE res_id IN (SELECT il.res_id FROM institution_logo il, funding_agency fa WHERE il.res_id=fa.logo_id);
ALTER TABLE funding_agency ADD CONSTRAINT `FKcyp9paobfa04gyp8uyn05c0t8` FOREIGN KEY (`logo_id`) REFERENCES `funding_agency_logo` (`res_id`);

/*
 Mar. 2023
 commitID=81dfe00
 This allow to have boolean value stored only on VARCHAR(5) instead of VARCHAR(255) by default
*/

ALTER TABLE aip MODIFY checksum_checking_succeed VARCHAR(5);
ALTER TABLE aip MODIFY disposition_approval VARCHAR(5);
ALTER TABLE aip MODIFY archival_unit VARCHAR(5);
ALTER TABLE aip MODIFY contains_updated_metadata VARCHAR(5);
ALTER TABLE deposit MODIFY contains_updated_metadata VARCHAR(5);
ALTER TABLE deposit MODIFY agreement VARCHAR(5);
ALTER TABLE deposit MODIFY contains_updated_metadata VARCHAR(5);
ALTER TABLE dip MODIFY contains_updated_metadata VARCHAR(5);
ALTER TABLE sip MODIFY contains_updated_metadata VARCHAR(5);
ALTER TABLE sip MODIFY disposition_approval VARCHAR(5);
ALTER TABLE order_query MODIFY public_order VARCHAR(5);
ALTER TABLE org_unit MODIFY is_empty VARCHAR(5);
ALTER TABLE org_unit_dissemination_policies MODIFY default_policy VARCHAR(5);
ALTER TABLE org_unit_preservation_policies MODIFY default_policy VARCHAR(5);
ALTER TABLE org_unit_submission_policies MODIFY default_policy VARCHAR(5);
ALTER TABLE preservation_policy MODIFY disposition_approval VARCHAR(5);
ALTER TABLE submission_policy MODIFY submission_approval VARCHAR(5);
ALTER TABLE preservation_job MODIFY enable VARCHAR(5);
ALTER TABLE scheduled_task MODIFY enabled VARCHAR(5);
ALTER TABLE user MODIFY disabled VARCHAR(5);
ALTER TABLE license MODIFY domain_content VARCHAR(5);
ALTER TABLE license MODIFY domain_data VARCHAR(5);
ALTER TABLE license MODIFY domain_software VARCHAR(5);
ALTER TABLE license MODIFY is_generic VARCHAR(5);
ALTER TABLE person MODIFY verified_orcid VARCHAR(5);

/*
 Mar. 2023
 commitID=4ba9ba15	
*/ 
	
ALTER TABLE funding_agency_logo RENAME COLUMN image_content TO file_content;
ALTER TABLE institution_logo RENAME COLUMN image_content TO file_content;
ALTER TABLE license_logo RENAME COLUMN image_content TO file_content;
ALTER TABLE org_unit_logo RENAME COLUMN image_content TO file_content;
ALTER TABLE person_avatar RENAME COLUMN image_content TO file_content;

/*
 Mar. 2023
 commitID=33554156
*/
ALTER TABLE deposit MODIFY COLUMN is_referenced_by VARCHAR(1024);

/*
 Mar. 2023
 commitID=
 */
SHOW index FROM org_unit_archive_acl;
/* delete unique key constraint for aip_id and user_id */
ALTER TABLE org_unit_archive_acl DROP INDEX `UKi8vpmqsk0g6o2fqpteco4k7oa`;


/*
 Apr. 2023
 commitID=0c16702e
 */
ALTER TABLE `aip` ADD COLUMN `update_number` bigint(20) DEFAULT NULL;


/*
 Apr. 2023
 commitID=6f657276
 */
ALTER TABLE `aip` ADD COLUMN `collection_file_number` bigint(20) DEFAULT NULL;
ALTER TABLE `aip` ADD COLUMN `collection_archive_size` bigint(20) DEFAULT NULL;

/*
 May 2023
 commitID=6a1ba183
 */
ALTER TABLE research_domain RENAME subject_area;
ALTER TABLE research_domain_labels RENAME subject_area_labels;
ALTER TABLE org_unit_research_domains RENAME org_unit_subject_areas;
ALTER TABLE org_unit_subject_areas RENAME COLUMN research_domain_id TO subject_area_id;

/*
 May 2023
 commitId=003eeecb
*/
ALTER TABLE deposit ADD COLUMN data_use_policy VARCHAR(255) NULL;
ALTER TABLE sip ADD COLUMN data_use_policy VARCHAR(255) NULL;
ALTER TABLE aip ADD COLUMN data_use_policy VARCHAR(255) NULL;

UPDATE deposit set data_use_policy='LICENSE' where access = 'PUBLIC';
UPDATE deposit set data_use_policy='NONE' where access != 'PUBLIC';

UPDATE sip set data_use_policy='LICENSE' where access = 'PUBLIC';
UPDATE sip set data_use_policy='NONE' where access != 'PUBLIC';

UPDATE aip set data_use_policy='LICENSE' where access = 'PUBLIC';
UPDATE aip set data_use_policy='NONE' where access != 'PUBLIC';


/**
 May 2023
 commitId=99354413
*/
ALTER TABLE deposit ADD COLUMN content_structure_public VARCHAR(5) NULL;
UPDATE deposit SET content_structure_public = "true" WHERE access = "PUBLIC";
UPDATE deposit SET content_structure_public = "false" WHERE access != "PUBLIC";

/**
 June 2023
 commitId=
 1) Identify archive with data tag ORANGE or RED or CRIMSON
 2) Move archive files to secure storage location
 3) Update archive Id with secure location
*/
SELECT res_id, data_sensitivity, archive_id FROM aip WHERE data_sensitivity='ORANGE' OR data_sensitivity='RED' OR data_sensitivity='CRIMSON';
UPDATE aip SET archive_id = REPLACE(archive_id,'file:///<normal-location>','file:///<secure-location>') WHERE data_sensitivity='ORANGE' OR data_sensitivity='RED' OR data_sensitivity='CRIMSON';

/**
 July 2023
 commitId=
*/
ALTER TABLE deposit DROP COLUMN agreement;


/**
 July 2023
 commitId=
*/
DROP TABLE archive_thumbnail;
DROP TABLE archive_thumbnail_logo;

/**
 July 2023=
 */
ALTER TABLE sip ADD COLUMN publication_date date DEFAULT NULL;
ALTER TABLE aip ADD COLUMN publication_date date DEFAULT NULL;
UPDATE sip SET sip.publication_date=(SELECT deposit.publication_date FROM deposit WHERE deposit.res_id = sip.deposit_id);

/**
 Only for storagions
 Command to run
   mysqldump -u root -p dlcm_ing_* sip > sip.sql
   mysql -u root -p dlcm_sto_* < sip.sql

   UPDATE aip a SET a.publication_date=(SELECT s.publication_date FROM sip s INNER JOIN aip_sip ON s.res_id = aip_sip.sip_id AND aip_sip.package_id = a.res_id);
   DELETE FROM sip;
*/

/**
 August 2023=
 commitId=34df22e3
 This allow to notify institution manager for create org unit request notification
 */
ALTER TABLE notification_type ADD COLUMN notified_institution_manager varchar(5) NOT NULL;
UPDATE notification_type SET notified_institution_manager='false' WHERE true;
UPDATE notification_type SET notified_institution_manager='true' WHERE res_id LIKE 'CREATE_ORGUNIT_REQUEST';

/*
 August 2023
 commitId=
*/
ALTER TABLE `preservation_policy` ADD COLUMN `main_storage` int(11) NOT NULL;
ALTER TABLE `sip` ADD COLUMN `main_storage` int(11) NOT NULL;
ALTER TABLE `aip` ADD COLUMN `last_archiving` datetime(6) NOT NULL;
UPDATE `aip` SET `last_archiving`= `last_update_when`;

/**
  August 2023
  commitId=2df08385
*/

UPDATE notification SET notification_status='NON_APPLICABLE'
WHERE notification_type_res_id IN (SELECT res_id FROM notification_type WHERE notification_category = 'INFO');
ALTER TABLE notification ADD COLUMN notification_mark VARCHAR(255) NOT NULL;
UPDATE notification SET notification_mark='UNREAD';

/**
  September 2023
  commitId=
  init submission agreement type to without for existing submission policies
*/
ALTER TABLE submission_policy ADD COLUMN submission_agreement_type varchar(255) DEFAULT NULL;
UPDATE submission_policy SET submission_agreement_type='WITHOUT';

/**
  September 2023
  commitId=3137ad2f
*/
UPDATE notification SET notification_status="APPROVED" where notification_status="PROCESSED";

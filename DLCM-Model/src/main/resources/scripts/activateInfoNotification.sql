# On admin database
update notification_type set notified_org_unit_role_res_id="APPROVER" where res_id="COMPLETED_ARCHIVE_INFO";
update notification_type set notified_org_unit_role_res_id="STEWARD" where res_id="CREATED_DEPOSIT_INFO";
update notification_type set notified_org_unit_role_res_id="STEWARD" where res_id="IN_ERROR_DEPOSIT_INFO";
update notification_type set notified_application_role_res_id="ADMIN" where res_id="IN_ERROR_SIP_INFO";
update notification_type set notified_application_role_res_id="ADMIN" where res_id="IN_ERROR_AIP_INFO";
update notification_type set notified_application_role_res_id="ADMIN" where res_id="IN_ERROR_DIP_INFO";
update notification_type set notified_application_role_res_id="ADMIN" where res_id="IN_ERROR_DOWNLOADED_AIP_INFO";

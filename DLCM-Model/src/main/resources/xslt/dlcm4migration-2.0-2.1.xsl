<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:mets="http://www.loc.gov/METS/" xmlns:datacite="http://datacite.org/schema/kernel-4" xmlns:premis="http://www.loc.gov/premis/v3"
	xmlns:dlcm="http://www.dlcm.ch/dlcm/v2" xmlns:fits="http://hul.harvard.edu/ois/xml/ns/fits/fits_output">

	<xsl:output method="xml" omit-xml-declaration="no" indent="yes" encoding="UTF-8" />

	<!-- Browse all nodes -->
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" />
		</xsl:copy>
	</xsl:template>

	<!-- METS Profile -->
	<xsl:template match="mets:mets/@PROFILE">
		<xsl:attribute name="PROFILE">
			<xsl:text>dlcm_profile-2.1.xml</xsl:text>
		</xsl:attribute>
	</xsl:template>

	<!-- METS MDTYPEVERSION -->
	<xsl:template match="mets:mets/mets:dmdSec/mets:mdWrap/@MDTYPEVERSION">
		<xsl:attribute name="MDTYPEVERSION">
			<xsl:text>DataCite Metadata Schema 4.3 (dlcm_datacite-2.0.xsd)</xsl:text>
		</xsl:attribute>
	</xsl:template>

</xsl:stylesheet>

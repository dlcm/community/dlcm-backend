<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:mets="http://www.loc.gov/METS/" xmlns:datacite="http://datacite.org/schema/kernel-4" xmlns:premis="http://www.loc.gov/premis/v3"
	xmlns:dlcm1="http://www.dlcm.ch/dlcm/v1" xmlns:dlcm="http://www.dlcm.ch/dlcm/v2" xmlns:fits="http://hul.harvard.edu/ois/xml/ns/fits/fits_output"
	exclude-result-prefixes="dlcm1">

	<xsl:output method="xml" omit-xml-declaration="no" indent="yes" encoding="UTF-8" />

	<!-- Browse all nodes -->
	<xsl:template match="@* | node()" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" />
		</xsl:copy>
	</xsl:template>

	<!-- Migrate namespaces -->
	<xsl:template match="*">
		<xsl:element name="{name()}" namespace="{namespace-uri(.)}">
			<xsl:namespace name="datacite">
				<xsl:text>http://datacite.org/schema/kernel-4</xsl:text>
			</xsl:namespace>
			<xsl:namespace name="dlcm">
				<xsl:text>http://www.dlcm.ch/dlcm/v2</xsl:text>
			</xsl:namespace>
			<xsl:namespace name="fits">
				<xsl:text>http://hul.harvard.edu/ois/xml/ns/fits/fits_output</xsl:text>
			</xsl:namespace>
			<xsl:namespace name="mets">
				<xsl:text>http://www.loc.gov/METS/</xsl:text>
			</xsl:namespace>
			<xsl:namespace name="premis">
				<xsl:text>http://www.loc.gov/premis/v3</xsl:text>
			</xsl:namespace>
			<xsl:namespace name="xlink">
				<xsl:text>http://www.w3.org/1999/xlink</xsl:text>
			</xsl:namespace>
			<xsl:apply-templates select="@* | node()" />
		</xsl:element>
	</xsl:template>

	<!-- Migrate dlcm info to v2 -->
	<xsl:template match="dlcm1:dlcmInfo">
		<xsl:element name="dlcm:dlcmInfo">
			<xsl:element name="dlcm:complianceLevel">
				<xsl:value-of select="./dlcm1:complianceLevel" />
			</xsl:element>
			<xsl:element name="dlcm:dataClassification">
				<xsl:attribute name="category"><xsl:text>PrimaryData</xsl:text></xsl:attribute>
				<xsl:text>Unknown</xsl:text>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- Migrate METS Profile -->
	<xsl:template match="mets:mets/@PROFILE">
		<xsl:attribute name="PROFILE"
			><xsl:text>dlcm_profile-2.0.xml</xsl:text>
		</xsl:attribute>
	</xsl:template>

	<!-- Migrate METS MDTYPEVERSION -->
	<xsl:template match="mets:mets/mets:dmdSec/mets:mdWrap/@MDTYPEVERSION">
		<xsl:attribute name="MDTYPEVERSION">
			<xsl:text>DataCite Metadata Schema 4.3 (dlcm_datacite-2.0.xsd)</xsl:text>
		</xsl:attribute>
	</xsl:template>

	<!-- Add DataCite 'nameType' attribute for each 'datacite:creatorName' -->
	<xsl:template match="datacite:creatorName[not(@nameType)]">
		<xsl:element name="datacite:creatorName">
			<xsl:attribute name="nameType">
                <xsl:text>Personal</xsl:text>
            </xsl:attribute>
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>

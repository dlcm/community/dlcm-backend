<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:datacite="http://datacite.org/schema/kernel-4" xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" exclude-result-prefixes="datacite">

	<xsl:output method="xml" omit-xml-declaration="yes" indent="yes" encoding="UTF-8" />

	<xsl:strip-space elements="*" />

	<xsl:template match="datacite:rights[@rightsURI='https://www.dlcm.ch/access-level/final']">
		<xsl:copy>
			<xsl:apply-templates select='@*|node()' />
		</xsl:copy>
		<xsl:if test=".='PUBLIC'">
			<xsl:element name="datacite:rights">
				<xsl:attribute name="rightsURI">
          <xsl:text>info:eu-repo/semantics/openAccess</xsl:text>
        </xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:if test=".='RESTRICTED'">
			<xsl:element name="datacite:rights">
				<xsl:attribute name="rightsURI">
          <xsl:text>info:eu-repo/semantics/restrictedAccess</xsl:text>
        </xsl:attribute>
			</xsl:element>
		</xsl:if>
		<xsl:if test=".='CLOSED'">
			<xsl:element name="datacite:rights">
				<xsl:attribute name="rightsURI">
          <xsl:text>info:eu-repo/semantics/closedAccess</xsl:text>
        </xsl:attribute>
			</xsl:element>
		</xsl:if>


	</xsl:template>

	<xsl:template match='@*|node()'>
		<xsl:copy>
			<xsl:apply-templates select='@*|node()' />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:mets="http://www.loc.gov/METS/" xmlns:datacite="http://datacite.org/schema/kernel-4" xmlns:premis="http://www.loc.gov/premis/v3"
	xmlns:dlcm="http://www.dlcm.ch/dlcm/v2" xmlns:fits="http://hul.harvard.edu/ois/xml/ns/fits/fits_output">

	<xsl:output method="xml" omit-xml-declaration="no" indent="yes" encoding="UTF-8" />

	<!-- Browse all nodes -->
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" />
		</xsl:copy>
	</xsl:template>

	<!-- METS Profile -->
	<xsl:template match="mets:mets/@PROFILE">
		<xsl:attribute name="PROFILE">
			<xsl:text>dlcm_profile-3.1.xml</xsl:text>
		</xsl:attribute>
	</xsl:template>
	
	<!-- METS premis:file IANA (mime-type) -->
	<xsl:template match="premis:object[@xsi:type='premis:file']/premis:objectCharacteristics/premis:format">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" />
		</xsl:copy>
		<xsl:element name="premis:format">
			<xsl:element name="premis:formatRegistry">
				<xsl:element name="premis:formatRegistryName">
					<xsl:text>IANA</xsl:text>
				</xsl:element>
				<xsl:element name="premis:formatRegistryKey">
				    <xsl:value-of select="../premis:objectCharacteristicsExtension/fits:fits/fits:identification/fits:identity[1]/@mimetype" />
				</xsl:element>
	         </xsl:element>
         </xsl:element>
	</xsl:template>

</xsl:stylesheet>

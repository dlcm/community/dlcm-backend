<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:mets="http://www.loc.gov/METS/" xmlns:datacite="http://datacite.org/schema/kernel-4" xmlns:premis="http://www.loc.gov/premis/v3"
	xmlns:dlcm="http://www.dlcm.ch/dlcm/v3" xmlns:fits="http://hul.harvard.edu/ois/xml/ns/fits/fits_output" exclude-result-prefixes="datacite">

	<xsl:output method="xml" omit-xml-declaration="yes" indent="yes" encoding="UTF-8" />

	<xsl:function name="dlcm:generateTag">
		<xsl:param name="input" />
		<xsl:variable name="output">
			<xsl:choose>
				<xsl:when test="contains(normalize-space($input), ' ')">
					<xsl:value-of select="string-join(tokenize(normalize-space($input),' '),'-')" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="normalize-space($input)" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:sequence select="$output" />
	</xsl:function>

	<!-- Browse all nodes -->
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" />
		</xsl:copy>
	</xsl:template>

	<!-- Normalize 'rightsURI' attribute for each 'datacite:rights' -->
	<xsl:template match="datacite:rights[not(@rightsURI)]">
		<xsl:element name="datacite:rights">
			<xsl:attribute name="rightsURI" />
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>

	<!-- Normalize 'publisherIdentifier' attribute for each 'datacite:publisher' -->
	<xsl:template match="datacite:publisher[not(@publisherIdentifier)]">
		<xsl:element name="datacite:publisher">
			<xsl:attribute name="publisherIdentifier" />
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>
	
	<!-- Normalize 'affiliationIdentifier' attribute for each 'datacite:affiliation' -->
	<xsl:template match="datacite:affiliation[not(@affiliationIdentifier)]">
		<xsl:element name="datacite:affiliation">
			<xsl:attribute name="affiliationIdentifier" />
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>

	<!-- Normalize 'titleType' attribute for each 'datacite:title' -->
	<xsl:template match="datacite:title[not(@titleType)]">
		<xsl:element name="datacite:title">
			<xsl:attribute name="titleType" />
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>

	<!-- Normalize 'nameType' attribute for each 'datacite:creatorName' -->
	<xsl:template match="datacite:creatorName[not(@nameType)]">
		<xsl:element name="datacite:creatorName">
			<xsl:attribute name="nameType">
                <xsl:text>Personal</xsl:text>
            </xsl:attribute>
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>

	<!-- Normalize 'nameType' attribute for each 'datacite:contributorName' -->
	<xsl:template match="datacite:contributorName[not(@nameType)]">
		<xsl:element name="datacite:contributorName">
			<xsl:attribute name="nameType">
                <xsl:text>Personal</xsl:text>
            </xsl:attribute>
			<xsl:value-of select="." />
		</xsl:element>
	</xsl:template>

	<!-- Normalize nameIdentifier content for ORCID -->
	<xsl:template match="datacite:nameIdentifier[@nameIdentifierScheme='ORCID']">
		<xsl:choose>
			<xsl:when test="contains(text(),'https://orcid.org/')">
				<xsl:element name="datacite:nameIdentifier">
					<xsl:attribute name="nameIdentifierScheme">
                        <xsl:value-of select="@nameIdentifierScheme" />
                    </xsl:attribute>
                    <xsl:attribute name="schemeURI">
                        <xsl:value-of select="@schemeURI" />
                    </xsl:attribute>
					<xsl:value-of select="substring-after(.,'https://orcid.org/')" />
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="datacite:nameIdentifier">
					<xsl:attribute name="nameIdentifierScheme">
                        <xsl:value-of select="@nameIdentifierScheme" />
                    </xsl:attribute>
                    <xsl:attribute name="schemeURI">
                        <xsl:value-of select="@schemeURI" />
                    </xsl:attribute>
					<xsl:value-of select="." />
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Split date range in 2 dates (Start & End) for 'Collected' & 'Valid' date -->
	<xsl:template match="datacite:date[@dateType='Collected'] | datacite:date[@dateType='Valid']">
		<xsl:choose>
			<xsl:when test="contains(text(),'/')">
				<xsl:element name="datacite:date">
					<xsl:attribute name="dateType">
                        <xsl:value-of select="@dateType" />
                        <xsl:text>Start</xsl:text>
                    </xsl:attribute>
					<xsl:value-of select="substring-before(.,'/')" />
				</xsl:element>
				<xsl:element name="datacite:date">
					<xsl:attribute name="dateType">
                        <xsl:value-of select="@dateType" />
                        <xsl:text>End</xsl:text>
                    </xsl:attribute>
					<xsl:value-of select="substring-after(.,'/')" />
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="datacite:date">
					<xsl:attribute name="dateType">
                        <xsl:value-of select="@dateType" />
                    </xsl:attribute>
					<xsl:value-of select="." />
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Custom Metadata -->
	<xsl:template match="mets:mets">
		<custom-metadata>
			<xsl:for-each select="./mets:dmdSec/mets:mdRef">
				<custom-metadata>
					<xsl:attribute name="name">
                <xsl:value-of select="@MDTYPEVERSION" />
            </xsl:attribute>
					<xsl:attribute name="mime-type">
                <xsl:value-of select="@MIMETYPE" />
            </xsl:attribute>
					<xsl:attribute name="type">
                <xsl:value-of select="@MDTYPE" />
            </xsl:attribute>
					<xsl:attribute name="size">
                <xsl:value-of select="@SIZE" />
            </xsl:attribute>
				</custom-metadata>
			</xsl:for-each>
		</custom-metadata>
	</xsl:template>

	<!-- PREMIS 3 -->
	<xsl:template match="premis:premis[@version='3.0']">
		<items>
			<xsl:for-each-group select="premis:object" group-by="@xsi:type">
				<xsl:if test="current-grouping-key()='premis:intellectualEntity'">
					<item type="package">
						<packages>
							<xsl:for-each select="current-group()">
								<xsl:variable name="packageId" select="./premis:objectIdentifier/premis:objectIdentifierValue" />
								<xsl:if test="not(./premis:objectIdentifier/premis:objectIdentifierType = 'dlcm.organizational-units')">
									<xsl:element name="{substring-after(./premis:objectIdentifier/premis:objectIdentifierType,'.')}">
										<xsl:attribute name="id">
                                        <xsl:value-of select="$packageId" />
                                    </xsl:attribute>
										<name>
											<xsl:value-of select="./premis:originalName" />
										</name>
										<!-- PREMIS Events of date file -->
										<events>
											<xsl:for-each select="../premis:event/premis:linkingObjectIdentifier[premis:linkingObjectIdentifierValue=$packageId]">
												<xsl:variable name="tagName" select="dlcm:generateTag(../premis:eventType)" />
												<xsl:element name="{$tagName}">
													<xsl:attribute name="date">
                                                <xsl:value-of select="../premis:eventDateTime" />
                                            </xsl:attribute>
												</xsl:element>
											</xsl:for-each>
										</events>
									</xsl:element>
								</xsl:if>
							</xsl:for-each>
						</packages>
						<!-- PREMIS Agents -->
						<xsl:for-each select="../premis:agent">
							<agents>
								<xsl:attribute name="type">
                                    <xsl:value-of select="./premis:agentType" /> 
                            </xsl:attribute>
								<xsl:attribute name="name">
                                   <xsl:value-of select="./premis:agentIdentifier/premis:agentIdentifierValue" /> 
                               </xsl:attribute>
								<xsl:attribute name="version">
                                    <xsl:value-of select="./premis:agentVersion" />
                            </xsl:attribute>
							</agents>
						</xsl:for-each>
					</item>
				</xsl:if>
				<xsl:if test="current-grouping-key()='premis:file'">
					<xsl:for-each-group select="current-group()/premis:objectIdentifier" group-by="premis:objectIdentifierType">
						<xsl:choose>
							<xsl:when test="current-grouping-key()='dlcm.data'">
								<xsl:for-each select="current-group()">
									<item type="dataFile">
										<xsl:variable name="dataFileId" select="./premis:objectIdentifierValue" />
										<xsl:attribute name="id">
											<xsl:value-of select="$dataFileId" />
										</xsl:attribute>
										<!-- File info -->
										<file>
											<xsl:attribute name="fullName">
			                                    <xsl:value-of select="../premis:originalName" />
			                                </xsl:attribute>
											<xsl:attribute name="mimeType">
			                                    <xsl:value-of
												select="../premis:objectCharacteristics/premis:format/premis:formatRegistry[premis:formatRegistryName/text()='IANA']/premis:formatRegistryKey" />
			                                </xsl:attribute>
											<xsl:attribute name="size">
                  <xsl:value-of select="../premis:objectCharacteristics/premis:size" />
           </xsl:attribute>
											<xsl:choose>
												<xsl:when test="matches(../premis:originalName,'/')">
													<xsl:attribute name="name">
			                                           <xsl:value-of select="tokenize(../premis:originalName,'/')[last()]" />
			                                        </xsl:attribute>
													<xsl:attribute name="path">
			                                           <xsl:value-of
														select="string-join(remove(tokenize(../premis:originalName,'/'),count(tokenize(../premis:originalName,'/'))),'/')" />
			                                        </xsl:attribute>
												</xsl:when>
												<xsl:otherwise>
													<xsl:attribute name="name">
			                                           <xsl:value-of select="../premis:originalName" />
			                                        </xsl:attribute>
													<xsl:attribute name="path">/</xsl:attribute>
												</xsl:otherwise>
											</xsl:choose>
										</file>
										<!-- Format info -->
										<format>
											<xsl:attribute name="version">
                  <xsl:value-of
												select="../premis:objectCharacteristics/premis:format/premis:formatRegistry[premis:formatRegistryName/text()='PRONOM']/../premis:formatDesignation/premis:formatVersion" />
              </xsl:attribute>
											<xsl:attribute name="PRONOM">
                  <xsl:value-of
												select="../premis:objectCharacteristics/premis:format/premis:formatRegistry[premis:formatRegistryName/text()='PRONOM']/premis:formatRegistryKey" />
              </xsl:attribute>
											<xsl:attribute name="contentType">
                  <xsl:value-of
												select="../premis:objectCharacteristics/premis:format/premis:formatRegistry[premis:formatRegistryName/text()='IANA']/premis:formatRegistryKey" />
              </xsl:attribute>
											<xsl:attribute name="description">
                  <xsl:value-of
												select="../premis:objectCharacteristics/premis:format/premis:formatRegistry[premis:formatRegistryName/text()='PRONOM']/../premis:formatDesignation/premis:formatName" />
              </xsl:attribute>
										</format>
										<!-- DLCM Info -->
										<dlcm>
											<xsl:attribute name="complianceLevel">
	                                    		<xsl:value-of
												select="../premis:objectCharacteristics/premis:objectCharacteristicsExtension/dlcm:dlcmInfo/dlcm:complianceLevel" />
	                                		</xsl:attribute>
											<xsl:choose>
												<xsl:when test="../premis:objectCharacteristics/premis:objectCharacteristicsExtension/dlcm:dlcmInfo/dlcm:dataClassification">
													<xsl:attribute name="category">
	                                            		<xsl:value-of
														select="../premis:objectCharacteristics/premis:objectCharacteristicsExtension/dlcm:dlcmInfo/dlcm:dataClassification/@category" />
	                                        		</xsl:attribute>
													<xsl:attribute name="type">
	                                            		<xsl:value-of
														select="../premis:objectCharacteristics/premis:objectCharacteristicsExtension/dlcm:dlcmInfo/dlcm:dataClassification" />
	                                        		</xsl:attribute>
												</xsl:when>
												<xsl:otherwise>
													<xsl:attribute name="category">Undefined</xsl:attribute>
													<xsl:attribute name="type">Undefined</xsl:attribute>
												</xsl:otherwise>
											</xsl:choose>
										</dlcm>
										<!-- PREMIS Events of date file -->
										<events>
											<xsl:for-each select="../../premis:event/premis:linkingObjectIdentifier[premis:linkingObjectIdentifierValue=$dataFileId]">
												<xsl:variable name="tagName" select="dlcm:generateTag(../premis:eventType)" />
												<xsl:element name="{$tagName}">
													<xsl:attribute name="date">
	                                            		<xsl:value-of select="../premis:eventDateTime" />
	                                        		</xsl:attribute>
												</xsl:element>
											</xsl:for-each>
										</events>
										<!-- Checksum info -->
										<checksums>
											<xsl:for-each select="../premis:objectCharacteristics/premis:fixity">
												<xsl:attribute name="{./premis:messageDigestAlgorithm}">
													<xsl:value-of select="./premis:messageDigest" />
	                                   			</xsl:attribute>
											</xsl:for-each>
										</checksums>
										<!-- FITS info -->
										<technical-metadata>
											<xsl:for-each select="../premis:objectCharacteristics/premis:objectCharacteristicsExtension/fits:fits/fits:metadata">
												<xsl:choose>
													<xsl:when test="./fits:image">
														<image>
															<xsl:attribute name="width">
	                                            				<xsl:value-of select="./fits:image/fits:imageWidth[@status='SINGLE_RESULT']" />
	                                        				</xsl:attribute>
															<xsl:attribute name="height">
	                                            				<xsl:value-of select="./fits:image/fits:imageHeight[@status='SINGLE_RESULT']" />
	                                        				</xsl:attribute>
															<xsl:attribute name="color-space">
	                                            				<xsl:value-of select="./fits:image/fits:colorSpace[@status='SINGLE_RESULT']" />
	                                        				</xsl:attribute>
															<xsl:attribute name="orientation">
	                                            				<xsl:value-of select="./fits:image/fits:orientation[@status='SINGLE_RESULT']" />
	                                        				</xsl:attribute>
														</image>
													</xsl:when>
													<xsl:when test="./fits:document">
														<document>
															<xsl:attribute name="title">
	                                            				<xsl:value-of select="./fits:document/fits:title[@status='SINGLE_RESULT']" />
	                                        				</xsl:attribute>
															<xsl:attribute name="author">
	                                            				<xsl:value-of select="./fits:document/fits:author[@status='SINGLE_RESULT']" />
	                                        				</xsl:attribute>
															<xsl:attribute name="subject">
	                                            				<xsl:value-of select="./fits:document/fits:subject[@status='SINGLE_RESULT']" />
	                                        				</xsl:attribute>
															<xsl:attribute name="description">
	                                            				<xsl:value-of select="./fits:document/fits:description[@status='SINGLE_RESULT']" />
	                                        				</xsl:attribute>
															<xsl:attribute name="character-count">
	                                            				<xsl:value-of select="./fits:document/fits:characterCount[@status='SINGLE_RESULT']" />
	                                        				</xsl:attribute>
															<xsl:attribute name="word-count">
					                                            <xsl:value-of select="./fits:document/fits:wordCount[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="paragraph-count">
					                                            <xsl:value-of select="./fits:document/fits:paragraphCount[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="line-count">
					                                            <xsl:value-of select="./fits:document/fits:lineCount[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="page-count">
					                                            <xsl:value-of select="./fits:document/fits:pageCount[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="graphics-count">
					                                            <xsl:value-of select="./fits:document/fits:graphicsCount[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
														</document>
													</xsl:when>
													<xsl:when test="./fits:text">
														<text>
															<xsl:attribute name="charset">
						                                            <xsl:value-of select="./fits:text/fits:charset[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="linebreak">
					                                            <xsl:value-of select="./fits:text/fits:linebreak[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
														</text>
													</xsl:when>
													<xsl:when test="./fits:video">
														<video>
															<xsl:attribute name="duration">
					                                            <xsl:value-of select="./fits:video/fits:duration[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="encoding">
					                                            <xsl:value-of select="./fits:video/fits:encoding[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="format">
					                                            <xsl:value-of select="./fits:video/fits:format[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="bit-rate">
					                                            <xsl:value-of select="./fits:video/fits:bitRate[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:for-each select="./fits:video/fits:track[@status='SINGLE_RESULT']">
																<track>
																	<xsl:attribute name="type">
						                                                <xsl:value-of select="@type" />
						                                            </xsl:attribute>
																	<xsl:attribute name="codec-family">
						                                                <xsl:value-of select="fits:codecFamily" />
						                                            </xsl:attribute>
																</track>
															</xsl:for-each>
														</video>
													</xsl:when>
													<xsl:when test="./fits:audio">
														<audio>
															<xsl:attribute name="author">
					                                            <xsl:value-of select="./fits:audio/fits:author[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="title">
					                                            <xsl:value-of select="./fits:audio/fits:title[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="duration">
					                                            <xsl:value-of select="./fits:audio/fits:duration[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="channels">
					                                            <xsl:value-of select="./fits:audio/fits:channels[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="bit-rate">
					                                            <xsl:value-of select="./fits:audio/fits:bitRate[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="bit-depth">
					                                            <xsl:value-of select="./fits:audio/fits:bitDepth[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
														</audio>
													</xsl:when>
													<xsl:when test="./fits:container">
														<container>
															<xsl:attribute name="compression-method">
					                                            <xsl:value-of select="./fits:container/fits:compressionMethod[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
															<xsl:attribute name="original-size">
					                                            <xsl:value-of select="./fits:container/fits:originalSize[@status='SINGLE_RESULT']" />
					                                        </xsl:attribute>
														</container>
													</xsl:when>
												</xsl:choose>
											</xsl:for-each>
										</technical-metadata>
									</item>
								</xsl:for-each>
							</xsl:when>
							<xsl:when test="current-grouping-key()='dlcm.aip'">
								<xsl:for-each select="current-group()">
									<item type="aip">
										<xsl:variable name="aipId" select="./premis:objectIdentifierValue" />
										<xsl:attribute name="id">
											<xsl:value-of select="position()" />
										</xsl:attribute>
										<!-- AIP info -->
										<aip>
											<xsl:attribute name="size">
	                                    		<xsl:value-of select="../premis:objectCharacteristics/premis:size" />
	                                		</xsl:attribute>
											<xsl:attribute name="id">
												<xsl:value-of select="$aipId" />
											</xsl:attribute>
										</aip>
										<!-- Checksum info -->
										<checksums>
											<xsl:for-each select="../premis:objectCharacteristics/premis:fixity">
												<xsl:attribute name="{./premis:messageDigestAlgorithm}">
													<xsl:value-of select="./premis:messageDigest" />
	                                   			</xsl:attribute>
											</xsl:for-each>
										</checksums>
									</item>
								</xsl:for-each>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each-group>
				</xsl:if>
			</xsl:for-each-group>
		</items>
	</xsl:template>

</xsl:stylesheet>

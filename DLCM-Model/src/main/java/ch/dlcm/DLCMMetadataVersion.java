/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DLCMMetadataVersion.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.validation.ValidationError;

@Schema(description = """
        DLCM metadata are based on METS container, DataCite as descriptive metadata and PREMIS as administrative metadata.
        Metadata version:
        - 1.0 = DataCite 4.0 + PREMIS 3.0 + DLCM Info 1.0
        - 1.1 = DataCite 4.0 + PREMIS 3.0 + DLCM Info 1.0
        - 2.0 = DataCite 4.3 + PREMIS 3.0 + DLCM Info 2.0 + Data File Categories
        - 2.1 = DataCite 4.3 + PREMIS 3.0 + DLCM Info 2.1 + Dataset Thumbnail support
        - 3.0 = DataCite 4.4 + PREMIS 3.0 + DLCM Info 2.1
        - 3.1 = DataCite 4.4 + PREMIS 3.0 + DLCM Info 2.2 + Archive Thumbnail & DUA & README support
        - 4.0 = Datacite 4.5 + PREMIS 3.0 + DLCM Info 3.0 + Update of Archive Thumbnail & DUA & README
        """)
public enum DLCMMetadataVersion {
  V1_0("1.0", 10), // v1.0: DataCite 4.0 + Premis 3.0 + DLCM Info 1.0
  V1_1("1.1", 11), // v1.1: DataCite 4.0 + Premis 3.0 + DLCM Info 1.0
  V2_0("2.0", 20), // v2.0: DataCite 4.3 + Premis 3.0 + DLCM Info 2.0 + Data File Categories
  V2_1("2.1", 21), // v2.1: DataCite 4.3 + Premis 3.0 + DLCM Info 2.1 + Dataset Thumbnail support
  V3_0("3.0", 30), // v3.0: DataCite 4.4 + Premis 3.0 + DLCM Info 2.1
  V3_1("3.1", 31), // v3.1: DataCite 4.4 + Premis 3.0 + DLCM Info 2.2 + support DUA
  V4_0("4.0", 40); // v4.0: DataCite 4.5 + Premis 3.0 + DLCM Info 3.0 + Update of Archive Thumbnail & DUA & README

  private static final String METS_PROFILE_PREFIX = "dlcm_profile-";
  private static final String METS_SCHEMA_PREFIX = "dlcm_mets-";
  private static final String PREMIS_3 = "premis-3.0.xsd";
  private static final String REP_INFO_SCHEMA_PREFIX = "dlcm_datacite-";
  private static final String INDEXING_XSL = "dlcm4indexing.xsl";

  private final String versionName;
  private final int versionNumber;

  DLCMMetadataVersion(String versionName, int versionNumber) {
    this.versionName = versionName;
    this.versionNumber = versionNumber;
  }

  public static DLCMMetadataVersion getDefaultVersion() {
    return DLCMMetadataVersion.V4_0;
  }

  public static DLCMMetadataVersion fromVersion(String versionName) {
    return switch (versionName) {
      case "1.0" -> V1_0;
      case "1.1" -> V1_1;
      case "2.0" -> V2_0;
      case "2.1" -> V2_1;
      case "3.0" -> V3_0;
      case "3.1" -> V3_1;
      case "4.0" -> V4_0;
      default -> throw new SolidifyValidationException(new ValidationError("Unknown version"));
    };
  }

  public static boolean isAtLeastVersion(DLCMMetadataVersion versionToSupport, DLCMMetadataVersion versionToCheck) {
    return versionToCheck != null && versionToCheck.getVersionNumber() >= versionToSupport.getVersionNumber();
  }

  public String getAdministrativeInfoSchema() {
    return PREMIS_3;
  }

  public String getMetsProfile() {
    return METS_PROFILE_PREFIX + this.getVersion() + SolidifyConstants.XML_EXT;
  }

  public String getMetsSchema() {
    return METS_SCHEMA_PREFIX + this.getVersion() + SolidifyConstants.XSD_EXT;
  }

  public String getRepresentationInfoSchema() {
    return switch (this.versionName) {
      case "1.0" -> REP_INFO_SCHEMA_PREFIX + this.getVersion() + SolidifyConstants.XSD_EXT;
      case "1.1" -> REP_INFO_SCHEMA_PREFIX + V1_0.getVersion() + SolidifyConstants.XSD_EXT;
      case "2.0" -> REP_INFO_SCHEMA_PREFIX + this.getVersion() + SolidifyConstants.XSD_EXT;
      case "2.1" -> REP_INFO_SCHEMA_PREFIX + V2_0.getVersion() + SolidifyConstants.XSD_EXT;
      case "3.0" -> REP_INFO_SCHEMA_PREFIX + V3_0.getVersion() + SolidifyConstants.XSD_EXT;
      case "3.1" -> REP_INFO_SCHEMA_PREFIX + V3_0.getVersion() + SolidifyConstants.XSD_EXT;
      case "4.0" -> REP_INFO_SCHEMA_PREFIX + V4_0.getVersion() + SolidifyConstants.XSD_EXT;
      default -> throw new SolidifyRuntimeException("Wrong metadata version: " + this.versionName);
    };
  }

  public String getIndexingTransformation() {
    return INDEXING_XSL;
  }

  @JsonValue
  public String getVersion() {
    return this.versionName;
  }

  @JsonIgnore
  public int getVersionNumber() {
    return this.versionNumber;
  }

  @JsonIgnore
  public String getMajorVersion() {
    return this.versionName.substring(0, this.versionName.indexOf('.') + 1);
  }

  @Override
  public String toString() {
    return this.getVersion();
  }

}

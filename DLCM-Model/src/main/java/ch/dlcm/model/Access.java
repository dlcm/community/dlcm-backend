/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - Access.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        Access level of the archive:
        - PUBLIC => Open Access & Everyone
        - RESTRICTED => Team members (i.e., Org. Unit) & Trusted parties
        - CLOSED => Case by case & Individuals
        """)
public enum Access {
  // Sorted from the most open to the most restricted access

  //@formatter:off
  PUBLIC(10),
  RESTRICTED(20),
  CLOSED(30);
  //@formatter:on

  @Schema(description = "The level of the access level. The lowest value is more open.")
  private final int level;

  Access(int level) {
    this.level = level;
  }

  public int value() {
    return this.level;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ComplianceLevel.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        Compliance level to define the preservation quality based on data file format:
        - NOT_ASSESSED => The format could not be evaluated
        - NO_COMPLIANCE => The format could not be determined
        - WEAK_COMPLIANCE => The format was determined: content type detected
        - AVERAGE_COMPLIANCE => The format was determined: WEAK_COMPLIANCE + PRONOM identifier detected
        - FULL_COMPLIANCE => The format was determined: AVERAGE_COMPLIANCE + the format is part of golden formats
        """)
public enum ComplianceLevel {

  //@formatter:off
  NOT_ASSESSED(0),
  NO_COMPLIANCE(1),
  WEAK_COMPLIANCE(2),
  AVERAGE_COMPLIANCE(3),
  FULL_COMPLIANCE(4);
  //@formatter:on

  @Schema(description = "The level of the compliance level. The lowest value is less compliant.")
  private final int level;

  ComplianceLevel(int level) {
    this.level = level;
  }

  public static ComplianceLevel fromValue(int value) {
    for (ComplianceLevel complianceLevel : values()) {
      if (complianceLevel.value() == value) {
        return complianceLevel;
      }
    }
    return null;
  }

  public int value() {
    return this.level;
  }
}

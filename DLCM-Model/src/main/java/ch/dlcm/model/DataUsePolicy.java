/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DataTag.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        Data use policy defines the terms to conditions to access to an archive:
        - NONE => no policy
        - LICENSE => To respect the license
        - CLICK_THROUGH_DUA => To click to accept the Data Use Agreement (DUA)
        - SIGNED_DUA => To sign to accept the Data Use Agreement (DUA)
        - EXTERNAL_DUA => The Data Use Agreement (DUA) is managed externally
        """)
public enum DataUsePolicy {
  // @formatter:off
  NONE(10),
  LICENSE(20),
  CLICK_THROUGH_DUA(30),
  SIGNED_DUA(40),
  EXTERNAL_DUA(50);
  // @formatter:on

  @Schema(description = "The level of the data use policy. The lowest value is less restrictive.")
  private final int level;

  DataUsePolicy(int level) {
    this.level = level;
  }

  public static DataUsePolicy fromValue(int value) {
    for (DataUsePolicy dataUsePolicy : values()) {
      if (dataUsePolicy.value() == value) {
        return dataUsePolicy;
      }
    }
    return null;
  }

  public int value() {
    return this.level;
  }
}

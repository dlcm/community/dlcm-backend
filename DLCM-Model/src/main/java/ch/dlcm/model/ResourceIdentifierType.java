/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ResourceIdentifierType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import ch.unige.solidify.model.SolidifyIdentifierType;

public enum ResourceIdentifierType implements SolidifyIdentifierType {
  // List of identifier type

  //@formatter:off
  RES_ID("Resource ID"),
  ORCID("ORCID"),
  ROR_ID("ROR ID"),
  SPDX_ID("SPDX License ID"),
  NAME("Name"),
  ACRONYM("Acronym");
  //@formatter:on

  public static final String DEFAULT = "RES_ID";

  private final String description;

  ResourceIdentifierType(String description) {
    this.description = description;
  }

  @Override
  public String getName() {
    return this.name();
  }

  @Override
  public String getDescription() {
    return this.description;
  }
}

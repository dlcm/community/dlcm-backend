/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - VirusCheck.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;

import java.io.StringReader;
import java.time.OffsetDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Embedded;
import jakarta.persistence.Lob;
import jakarta.validation.Valid;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.StringTool;

@Schema(description = "Virus check information.")
@Embeddable
public class VirusCheck implements ToolContainer {

  @Schema(description = "The execution date of the virus check.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(name = "virusCheckDate", length = DB_DATE_LENGTH)
  private OffsetDateTime checkDate;

  @Schema(description = "The result details of the virus check.")
  @Lob
  @Column(name = "virusDetails", length = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String details;

  @Schema(description = "The virus check tool used to verify if the file contains virus.")
  @Embedded
  @Valid
  @AttributeOverride(name = "name", column = @Column(name = "virusToolName"))
  @AttributeOverride(name = "description", column = @Column(name = "virusToolDescription"))
  @AttributeOverride(name = "version", column = @Column(name = "virusToolVersion"))
  @AttributeOverride(name = "puid", column = @Column(name = "virusToolPuid"))
  private Tool tool;

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof VirusCheck)) {
      return false;
    }
    final VirusCheck castOther = (VirusCheck) other;
    return Objects.equals(this.getCheckDate(), castOther.getCheckDate()) && Objects.equals(this.getDetails(), castOther.getDetails()) && Objects
            .equals(this.getTool(), castOther.getTool());
  }

  public OffsetDateTime getCheckDate() {
    return this.checkDate;
  }

  public String getDetails() {
    return this.details;
  }

  @JsonIgnore
  public Object getSmartDetails(JAXBContext jaxbContext) throws JAXBException {
    final Unmarshaller um = jaxbContext.createUnmarshaller();
    return um.unmarshal(new StringReader(this.getDetails()));
  }

  @Override
  public Tool getTool() {
    return this.tool;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getCheckDate(), this.getDetails(), this.getTool());
  }

  public void setCheckDate(OffsetDateTime checkDate) {
    this.checkDate = checkDate;
  }

  public void setDetails(String details) {
    if (details != null) {
      this.details = details;
    }
  }

  @Override
  public void setTool(Tool tool) {
    this.tool = tool;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - Package.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import java.util.List;

import ch.dlcm.DLCMConstants;

public enum Package {

  // @formatter:off
  ROOT("/"),
  METADATA_FILE(DLCMConstants.METADATA_FILE),
  METADATA("/" + DLCMConstants.METADATA_FOLDER),
  RESEARCH("/" + DLCMConstants.RESEARCH_DATA_FOLDER),
  ADMINISTRATION("/" + DLCMConstants.ADMINISTRATION_DATA_FOLDER),
  DOC("/" + DLCMConstants.DOC_FOLDER),
  SOFTWARE("/" + DLCMConstants.SOFTWARE_FOLDER),
  SCHEMAS("/" + DLCMConstants.SCHEMA_FOLDER),
  INTERNAL("/" + DLCMConstants.INTERNAL_FOLDER),
  DATASET_THUMBNAIL(DLCMConstants.DATASET_THUMBNAIL),
  ARCHIVE_THUMBNAIL(DLCMConstants.ARCHIVE_THUMBNAIL),
  ARCHIVE_README(DLCMConstants.ARCHIVE_README),
  ARCHIVE_DUA(DLCMConstants.ARCHIVE_DUA),
  UPDATE("/" + DLCMConstants.UPDATE_FOLDER),
  UPDATE_PACKAGE(DLCMConstants.UPDATE_PACKAGE_FILE);
  // @formatter:on

  public static List<String> getMetadataStructure() {
    return List.of(METADATA_FILE.getName(), METADATA.getName(), SCHEMAS.getName());
  }

  public static List<String> getMetadataAntPath() {
    return List.of(METADATA_FILE.getName(), METADATA.getName() + "/**", SCHEMAS.getName() + "/**");
  }

  public static List<String> getCommonFoldersAntPath(String fileAntPattern) {
    if (fileAntPattern.equals("/")) {
      fileAntPattern = DLCMConstants.ANT_ALL;
    } else {
      // Normalize
      if (fileAntPattern.startsWith("/")) {
        fileAntPattern = fileAntPattern.substring(1);
      }

      // Folder
      if (fileAntPattern.endsWith("/")) {
        fileAntPattern += DLCMConstants.ANT_ALL;
      }
    }
    // Add common folders (~ data categories)
    return List.of(Package.RESEARCH.getName().substring(1) + "/" + fileAntPattern,
            Package.DOC.getName().substring(1) + "/" + fileAntPattern,
            Package.ADMINISTRATION.getName().substring(1) + "/" + fileAntPattern,
            Package.SOFTWARE.getName().substring(1) + "/" + fileAntPattern);
  }

  private final String name;

  Package(String s) {
    this.name = s;
  }

  public String getName() {
    return this.name;
  }

  public boolean isSubFolder() {
    return this.name.startsWith("/");
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - Role.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.security;

import static ch.dlcm.DLCMRestFields.LEVEL_FIELD;
import static ch.dlcm.DLCMRestFields.RES_ID_FIELD;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.model.settings.InstitutionPersonRole;
import ch.dlcm.model.settings.OrganizationalUnitPersonRole;
import ch.dlcm.rest.ModuleName;

@Schema(description = "A role defines a user’s access level on the platform, giving different permissions for performing actions on it.")
@Entity
public class Role extends ResourceNormalized {

  public static final Role MANAGER = new Role("Manager", 10);
  public static final Role STEWARD = new Role("Steward", 20);
  public static final Role APPROVER = new Role("Approver", 30);
  public static final Role CREATOR = new Role("Creator", 40);
  public static final Role VISITOR = new Role("Visitor", 50);

  public static final String MANAGER_ID = "MANAGER";
  public static final String STEWARD_ID = "STEWARD";
  public static final String APPROVER_ID = "APPROVER";
  public static final String CREATOR_ID = "CREATOR";
  public static final String VISITOR_ID = "VISITOR";

  private static final List<Role> roleList = Arrays.asList(MANAGER, STEWARD, APPROVER, CREATOR, VISITOR);

  public static final String DEFAULT_ROLE_ID = Role.VISITOR_ID;

  @Schema(description = "The name of the role.")
  @NotNull
  @Column(unique = true)
  private String name;

  @Schema(description = "The level of the role.")
  @NotNull
  @Column(unique = true)
  private int level;

  @JsonIgnore
  @OneToMany(mappedBy = OrganizationalUnitPersonRole.PATH_TO_ROLE, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private List<OrganizationalUnitPersonRole> organizationalUnitPersons = new ArrayList<>();

  @JsonIgnore
  @OneToMany(mappedBy = InstitutionPersonRole.PATH_TO_ROLE, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  private List<InstitutionPersonRole> institutionPersons = new ArrayList<>();

  public Role() {
    // no-arg constructor
  }

  public Role(String name, int level) {
    this.setResId(name.toUpperCase());
    this.name = name;
    this.level = level;
  }

  public String getName() {
    return this.name;
  }

  public int getLevel() {
    return this.level;
  }

  public List<OrganizationalUnitPersonRole> getOrganizationalUnitPersons() {
    return this.organizationalUnitPersons;
  }

  public List<InstitutionPersonRole> getInstitutionPersons() {
    return this.institutionPersons;
  }

  public static List<Role> getRoleList() {
    return roleList;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public void setName(String name) {
    this.name = name;
  }

  public static Optional<Role> maxRole(Optional<Role> orgUnitRole, Optional<Role> inheritedRole) {
    if (orgUnitRole.isEmpty() && inheritedRole.isEmpty()) {
      return Optional.empty();
    }
    if (orgUnitRole.isEmpty()) {
      return inheritedRole;
    }
    if (inheritedRole.isEmpty()) {
      return orgUnitRole;
    }
    if (orgUnitRole.get().getLevel() < inheritedRole.get().getLevel()) {
      return orgUnitRole;
    } else {
      return inheritedRole;
    }
  }

  @Override
  public List<String> getNonUpdatableFields() {
    return Arrays.asList(RES_ID_FIELD, LEVEL_FIELD);
  }

  @Override
  public String toString() {
    return "Role " + this.name;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Role))
      return false;
    final Role role = (Role) o;
    return this.getResId().equals(role.getResId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.name, this.level, this.organizationalUnitPersons);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DownloadTokenType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.security;

import ch.unige.solidify.model.security.SolidifyDownloadTokenType;

public enum DownloadTokenType implements SolidifyDownloadTokenType {

  // @formatter:off
  DEPOSIT("deposit"),
  DEPOSIT_DATAFILE("deposit_datafile"),
  SIP("sip"),
  SIP_DATAFILE("sip_datafile"),
  AIP("aip"),
  AIP_DATAFILE("aip_datafile"),
  AIP_DOWNLOAD("aip_download"),
  AIP_DOWNLOAD_DATAFILE("aip_download_datafile"),
  DIP("dip"),
  DIP_DATAFILE("dip_datafile"),
  DIP_DATAFILE_ONLY("dip_datafile_only"),
  STORED_AIP("stored_aip"),
  ARCHIVE("archive"),
  ORDER("order"),
  ORGANIZATIONAL_UNIT_ADDITIONAL_FIELDS_FORM("organizational_unit_additional_fields_form");
  // @formatter:on

  private final String label;

  DownloadTokenType(String t) {
    this.label = t;
  }

  @Override
  public String getLabel() {
    return this.label;
  }
}

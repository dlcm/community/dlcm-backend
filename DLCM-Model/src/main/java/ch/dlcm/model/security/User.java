/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - User.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.security;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;
import static ch.unige.solidify.SolidifyConstants.EXTERNAL_UID_KEY;
import static ch.unige.solidify.SolidifyConstants.USER_PROPERTY_FILTER_NAME;

import java.time.OffsetDateTime;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.data.jpa.domain.Specification;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthUserDto;
import ch.unige.solidify.auth.model.UserInfo;
import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.model.SolidifyApplicationRole;
import ch.unige.solidify.rest.SearchableResourceNormalized;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.specification.UserSearchSpecification;

@Schema(description = "A user represents a user of the platform. A person and roles are associated to it.")
@Entity
@JsonFilter(USER_PROPERTY_FILTER_NAME)
public class User extends SearchableResourceNormalized<User> implements UserInfo {
  private static final int LAST_LOGIN_IP_ADDRESS_SIZE = 255;

  @Schema(description = "The date of the user last login.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH)
  private OffsetDateTime lastLoginTime;

  @Schema(description = "The IP address from last login of the user.")
  @Size(max = LAST_LOGIN_IP_ADDRESS_SIZE)
  private String lastLoginIpAddress;

  @Schema(description = "The application role of the user.")
  @NotNull
  @ManyToOne(targetEntity = SolidifyApplicationRole.class)
  private SolidifyApplicationRole applicationRole;

  @Schema(description = "If the user is diabled.")
  @NotNull
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean disabled = Boolean.FALSE;

  @Schema(description = "The email of the user.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  @Column(unique = true)
  private String email;

  @Schema(description = "The external UID of the user.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  @Column(unique = true)
  private String externalUid;

  @Schema(description = "The first name of the user.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String firstName;

  @Schema(description = "The last name of the user.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String lastName;

  @Schema(description = "The home organization of the user.")
  @NotNull
  @Size(min = 1, max = 512)
  private String homeOrganization;

  @Schema(description = "The linked person of the user.")
  @ManyToOne(targetEntity = Person.class)
  @JoinColumn(name = DLCMConstants.DB_PERSON_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private Person person;

  public User() {
  }

  /**
   * Create a new user already logged in the authorization server
   *
   * @param authUserDto
   */
  public User(AuthUserDto authUserDto) {
    super();
    this.setResId(authUserDto.getResId());
    this.setExternalUid(authUserDto.getExternalUid());
    this.setEmail(authUserDto.getEmail());
    this.setApplicationRole(new SolidifyApplicationRole(authUserDto.getApplicationRole()));
    this.setFirstName(authUserDto.getFirstName());
    this.setLastName(authUserDto.getLastName());
    this.setHomeOrganization(authUserDto.getHomeOrganization());
    this.getCreation().setWhen(authUserDto.getCreationWhen());
    this.getCreation().setWho(authUserDto.getCreationWho());
    this.getLastUpdate().setWhen(authUserDto.getLastUpdateWhen());
    this.getLastUpdate().setWho(authUserDto.getLastUpdateWho());
    if (authUserDto.getLastLogin() != null) {
      this.setLastLoginIpAddress(authUserDto.getLastLogin().getLoginIpAddress());
      this.setLastLoginTime(authUserDto.getLastLogin().getLoginTime());
    }
  }

  /**
   * Update a user with information from authorization server
   */
  public void setAuthUserDto(AuthUserDto authUserDto) {
    // Application Role
    if (authUserDto.getApplicationRole() != null) {
      this.setApplicationRole(new SolidifyApplicationRole(authUserDto.getApplicationRole()));
    }

    // Last login
    if (authUserDto.getLastLogin() != null) {
      if (authUserDto.getLastLogin().getLoginIpAddress() != null) {
        this.setLastLoginIpAddress(authUserDto.getLastLogin().getLoginIpAddress());
      }
      if (authUserDto.getLastLogin().getLoginTime() != null) {
        this.setLastLoginTime(authUserDto.getLastLogin().getLoginTime());
      }
    }
  }

  @Override
  public ApplicationRole getApplicationRole() {
    return this.applicationRole;
  }

  @Override
  @Transient
  @JsonIgnore
  public CacheMessage getCacheMessage() {
    final CacheMessage cacheMessage = super.getCacheMessage();
    cacheMessage.addOtherProperty(EXTERNAL_UID_KEY, this.getExternalUid());
    return cacheMessage;
  }

  @Override
  public String getEmail() {
    return this.email;
  }

  @Override
  public String getExternalUid() {
    return this.externalUid;
  }

  @Override
  public String getFirstName() {
    return this.firstName;
  }

  @Schema(description = "The full name of the user.")
  @Override
  public String getFullName() {
    if (this.getPerson() == null) {
      return this.getLastName() + ", " + this.getFirstName();
    }
    return this.getPerson().getFullName();
  }

  public String getHomeOrganization() {
    return this.homeOrganization;
  }

  @Override
  public String getLastName() {
    return this.lastName;
  }

  @Override
  public Person getPerson() {
    return this.person;
  }

  public String getLastLoginIpAddress() {
    return this.lastLoginIpAddress;
  }

  public OffsetDateTime getLastLoginTime() {
    return this.lastLoginTime;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Transient
  @JsonIgnore
  public boolean isAccountNonLocked() {
    return this.isEnabled();
  }

  @Schema(description = "If the user is enable.")
  @Override
  public boolean isEnabled() {
    return !this.disabled;
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public void setApplicationRole(SolidifyApplicationRole applicationRole) {
    this.applicationRole = applicationRole;
  }

  public void setDisabled(Boolean disabled) {
    this.disabled = disabled;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setExternalUid(String externalUid) {
    this.externalUid = externalUid;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setHomeOrganization(String homeOrganization) {
    this.homeOrganization = homeOrganization;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public void setLastLoginTime(OffsetDateTime lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }

  public void setLastLoginIpAddress(String lastLoginIpAddress) {
    if (lastLoginIpAddress != null && lastLoginIpAddress.length() > LAST_LOGIN_IP_ADDRESS_SIZE) {
      this.lastLoginIpAddress = lastLoginIpAddress.substring(0, LAST_LOGIN_IP_ADDRESS_SIZE);
    }
    this.lastLoginIpAddress = lastLoginIpAddress;
  }

  @Transient
  @JsonIgnore
  public void setUniqueId(String uniqueId) {
    this.setExternalUid(uniqueId);
  }

  @Override
  public Specification<User> getSearchSpecification(SearchCriteria criteria) {
    return new UserSearchSpecification(criteria);
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("User [aai=").append(this.externalUid).append(", email=").append(this.email).append(", application role")
            .append(this.applicationRole).append("]");
    return builder.toString();
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof User)) {
      return false;
    }
    if (!super.equals(other)) {
      return false;
    }
    final User castOther = (User) other;
    return Objects.equals(this.applicationRole, castOther.applicationRole) && Objects.equals(this.email, castOther.email)
            && Objects.equals(this.externalUid, castOther.externalUid) && Objects.equals(this.firstName, castOther.firstName)
            && Objects.equals(this.homeOrganization, castOther.homeOrganization) && Objects.equals(this.lastName, castOther.lastName)
            && Objects.equals(this.person, castOther.person);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.applicationRole, this.email, this.externalUid, this.firstName,
            this.homeOrganization, this.lastName, this.person);
  }

  @Transient
  @JsonIgnore
  public OffsetDateTime getCreationWhen() {
    return this.creation.getWhen();
  }

  @Transient
  @JsonIgnore
  public OffsetDateTime getLastUpdateWhen() {
    return this.lastUpdate.getWhen();
  }

  @Transient
  @JsonIgnore
  public String getCreationWho() {
    return this.creation.getWho();
  }

  @Transient
  @JsonIgnore
  public String getLastUpdateWho() {
    return this.lastUpdate.getWho();
  }

}

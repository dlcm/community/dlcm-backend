/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - TaskType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.schedule;

public enum TaskType {
  NOTIFY_DEPOSITS_TO_VALIDATE, NOTIFY_APPROVED_DEPOSITS_CREATOR, NOTIFY_COMPLETED_DEPOSITS_CREATOR, NOTIFY_COMPLETED_DEPOSITS_CONTRIBUTORS,
  NOTIFY_FORGOTTEN_DEPOSITS_TO_VALIDATE, NOTIFY_DEPOSITS_IN_ERROR, NOTIFY_COMPLETED_ARCHIVES_CREATOR, NOTIFY_COMPLETED_ARCHIVES_APPROVERS,
  NOTIFY_ORG_UNIT_CREATION_TO_VALIDATE, NOTIFY_JOIN_MEMBER_ORG_UNIT_TO_VALIDATE, NOTIFY_ARCHIVE_ACCESS_REQUEST
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - InstitutionPersonRoleKey.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.security.Role;

@Embeddable
public class InstitutionPersonRoleKey implements Serializable {

  @Serial
  private static final long serialVersionUID = 6921662829895192845L;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_INSTITUTION_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private Institution institution;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_PERSON_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private Person person;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_ROLE_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private Role role;

  public Institution getInstitution() {
    return this.institution;
  }

  public Person getPerson() {
    return this.person;
  }

  public void setInstitution(Institution institution) {
    this.institution = institution;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public Role getRole() {
    return this.role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.institution, this.person, this.role);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    InstitutionPersonRoleKey other = (InstitutionPersonRoleKey) obj;
    return Objects.equals(this.institution, other.institution) && Objects.equals(this.person, other.person)
            && Objects.equals(this.role, other.role);
  }

}

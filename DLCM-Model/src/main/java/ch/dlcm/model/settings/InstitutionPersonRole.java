/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - InstitutionPersonRole.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.util.Objects;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.rest.JoinResource;

import ch.dlcm.DLCMConstants;
import ch.dlcm.message.cache.InstitutionPersonCacheMessage;
import ch.dlcm.model.security.Role;

@Entity
@Table(name = "institutionPersonRoles", uniqueConstraints = {
        @UniqueConstraint(name = "Institution_Person", columnNames = { DLCMConstants.DB_INSTITUTION_ID, DLCMConstants.DB_PERSON_ID }) })
public class InstitutionPersonRole extends JoinResource<InstitutionPersonRoleKey> {

  public static final String PATH_TO_INSTITUTION = "compositeKey.institution";
  public static final String PATH_TO_PERSON = "compositeKey.person";
  public static final String PATH_TO_ROLE = "compositeKey.role";
  public static final String INSTITUTION_RELATION_PROPERTY_NAME = "personRoles";
  public static final String PERSON_RELATION_PROPERTY_NAME = "institutionRoles";
  public static final String ROLE_RELATION_PROPERTY_NAME = "institutionPersons";

  @EmbeddedId
  @JsonIgnore
  private InstitutionPersonRoleKey compositeKey;

  public InstitutionPersonRole() {
    this.compositeKey = new InstitutionPersonRoleKey();
  }

  @Override
  @JsonIgnore
  public CacheMessage getCacheMessage() {
    return new InstitutionPersonCacheMessage();
  }

  @Override
  public InstitutionPersonRoleKey getCompositeKey() {
    return this.compositeKey;
  }

  @JsonIgnore
  public Institution getInstitution() {
    return this.compositeKey.getInstitution();
  }

  @JsonIgnore
  public Person getPerson() {
    return this.compositeKey.getPerson();
  }

  @JsonIgnore
  public Role getRole() {
    return this.compositeKey.getRole();
  }

  @Override
  public void setCompositeKey(InstitutionPersonRoleKey compositeKey) {
    this.compositeKey = compositeKey;
  }

  @JsonIgnore
  public void setInstitution(Institution institution) {
    this.compositeKey.setInstitution(institution);
  }

  @JsonIgnore
  public void setPerson(Person person) {
    this.compositeKey.setPerson(person);
  }

  @JsonIgnore
  public void setRole(Role role) {
    this.compositeKey.setRole(role);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof InstitutionPersonRole ipr))
      return false;
    if (!super.equals(o))
      return false;
    return Objects.equals(this.compositeKey, ipr.compositeKey);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.compositeKey);
  }
}

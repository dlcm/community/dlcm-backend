/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - SubmissionAgreement.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.policies.SubmissionAgreementFile;
import ch.dlcm.rest.ModuleName;

@Schema(description = "A submission agreement defines the conditions of the submissions.")
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "title", "version" }, name = "title_version_unique") })
public class SubmissionAgreement extends ResourceNormalized implements ResourceFileInterface {

  @Schema(description = "The title of the submission agreement")
  @NotNull
  private String title;

  @Schema(description = "The description of the submission agreement")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Schema(description = "The version of the submission agreement")
  @NotNull
  private String version;

  @Schema(description = "The submission agreement file")
  @NotNull
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = DLCMConstants.SUBMISSION_AGREEMENT_FILE_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private SubmissionAgreementFile submissionAgreementFile;

  public String getTitle() {
    return this.title;
  }

  public String getDescription() {
    return this.description;
  }

  public String getVersion() {
    return this.version;
  }

  public SubmissionAgreementFile getSubmissionAgreementFile() {
    return this.submissionAgreementFile;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public void setSubmissionAgreementFile(SubmissionAgreementFile submissionAgreementFile) {
    this.submissionAgreementFile = submissionAgreementFile;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @JsonIgnore
  @Override
  public ResourceFile setNewResourceFile() {
    this.setSubmissionAgreementFile(new SubmissionAgreementFile());
    return this.getSubmissionAgreementFile();
  }

  @JsonIgnore
  @Override
  public ResourceFile getResourceFile() {
    return this.submissionAgreementFile;
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.submissionAgreementFile = (SubmissionAgreementFile) resourceFile;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    SubmissionAgreement that = (SubmissionAgreement) o;
    return Objects.equals(this.description, that.description) && Objects.equals(this.title, that.title) && Objects.equals(
            this.version, that.version) && Objects.equals(this.submissionAgreementFile, that.submissionAgreementFile);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.description, this.title, this.version, this.submissionAgreementFile);
  }
}

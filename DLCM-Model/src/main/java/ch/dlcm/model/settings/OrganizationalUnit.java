/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrganizationalUnit.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.converter.KeywordsConverter;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.policies.OrganizationalUnitDisseminationPolicy;
import ch.dlcm.model.policies.OrganizationalUnitPreservationPolicy;
import ch.dlcm.model.policies.OrganizationalUnitSubmissionPolicy;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.security.Role;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Schema(description = "An organizational unit is a logical entity that represents a research project or laboratory or any other organizational group of researchers. A preservation space is associated to it.")
@Entity
@Table(name = "orgUnit")
public class OrganizationalUnit extends ResourceNormalized implements ResourceFileInterface, OrganizationalUnitAwareResource {

  @Schema(description = "The closing date of the organizational unit.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate closingDate;

  @Schema(description = "The default license of the organizational unit.")
  @ManyToOne(targetEntity = License.class)
  private License defaultLicense;

  @Schema(description = "The default preservation policy of the organizational unit.")
  @Transient
  private PreservationPolicy defaultPreservationPolicy;

  @Schema(description = "The default submission policy of the organizational unit.")
  @Transient
  private SubmissionPolicy defaultSubmissionPolicy;

  @Schema(description = "The description of the organizational unit.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @JsonIgnore
  @OneToMany(mappedBy = OrganizationalUnitDisseminationPolicy.PATH_TO_ORG_UNIT, cascade = CascadeType.ALL, orphanRemoval = true)
  private List<OrganizationalUnitDisseminationPolicy> disseminationPolicies = new ArrayList<>();

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "orgUnitFundingAgencies", joinColumns = { @JoinColumn(name = DLCMConstants.DB_ORG_UNIT_ID) }, inverseJoinColumns = {
          @JoinColumn(name = DLCMConstants.DB_AGENCY_ID) }, uniqueConstraints = {
                  @UniqueConstraint(columnNames = { DLCMConstants.DB_ORG_UNIT_ID, DLCMConstants.DB_AGENCY_ID }) })
  private List<FundingAgency> fundingAgencies;

  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  @ManyToMany
  @JoinTable(name = "orgUnitInstitutions", joinColumns = { @JoinColumn(name = DLCMConstants.DB_ORG_UNIT_ID) }, inverseJoinColumns = {
          @JoinColumn(name = DLCMConstants.DB_INSTITUTION_ID) }, uniqueConstraints = {
                  @UniqueConstraint(columnNames = { DLCMConstants.DB_ORG_UNIT_ID, DLCMConstants.DB_INSTITUTION_ID }) })
  private List<Institution> institutions;

  @Schema(description = "If the organizational unit contains any data (deposits, sip, aip).")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean isEmpty;

  @Schema(description = "The keywords associated with this organizational unit, they are used as default for deposits created within this organizational unit.")
  @Convert(converter = KeywordsConverter.class)
  private List<String> keywords;

  @Schema(description = "The name of the organizational unit.")
  @NotNull
  @Column(unique = true)
  @Size(min = 1)
  private String name;

  @Schema(description = "The opening date of the organizational unit.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  @NotNull
  private LocalDate openingDate;

  @JsonIgnore
  @OneToMany(mappedBy = OrganizationalUnitPersonRole.PATH_TO_ORG_UNIT, cascade = CascadeType.ALL, orphanRemoval = true)
  private List<OrganizationalUnitPersonRole> personRoles = new ArrayList<>();

  @JsonIgnore
  @OneToMany(mappedBy = OrganizationalUnitPreservationPolicy.PATH_TO_ORG_UNIT, cascade = CascadeType.ALL, orphanRemoval = true)
  private List<OrganizationalUnitPreservationPolicy> preservationPolicies = new ArrayList<>();

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "orgUnitSubjectAreas", joinColumns = { @JoinColumn(name = DLCMConstants.DB_ORG_UNIT_ID) }, inverseJoinColumns = {
          @JoinColumn(name = DLCMConstants.DB_SUBJECT_AREA_ID) }, uniqueConstraints = {
                  @UniqueConstraint(columnNames = { DLCMConstants.DB_ORG_UNIT_ID, DLCMConstants.DB_SUBJECT_AREA_ID }) })
  private List<SubjectArea> subjectAreas;

  @JsonIgnore
  @OneToMany(mappedBy = OrganizationalUnitSubmissionPolicy.PATH_TO_ORG_UNIT, cascade = CascadeType.ALL, orphanRemoval = true)
  private List<OrganizationalUnitSubmissionPolicy> submissionPolicies = new ArrayList<>();

  @Schema(description = "The URL of the organizational unit.")
  private URL url;

  @Schema(description = "The logo of the organizational unit.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = DLCMConstants.DB_LOGO_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private OrganizationalUnitLogo logo;

  @OneToMany(mappedBy = "organizationalUnit", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<AdditionalFieldsForm> additionalFieldsForms = new ArrayList<>();

  @JsonIgnore
  @OneToMany(mappedBy = "organizationalUnit", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<ArchiveACL> archiveACLs = new ArrayList<>();

  public boolean addFundingAgency(FundingAgency fundingAgency) {
    final boolean result = this.fundingAgencies.add(fundingAgency);
    if (!fundingAgency.getOrganizationalUnits().contains(this)) {
      fundingAgency.addOrganizationalUnit(this);
    }
    return result;
  }

  public boolean addInstitution(Institution institution) {
    final boolean result = this.institutions.add(institution);
    if (!institution.getOrganizationalUnits().contains(this)) {
      institution.addOrganizationalUnit(this);
    }
    return result;
  }

  public boolean addSubjectArea(SubjectArea subjectArea) {
    final boolean result = this.subjectAreas.add(subjectArea);
    if (!subjectArea.getOrganizationalUnits().contains(this)) {
      subjectArea.addOrganizationalUnit(this);
    }
    return result;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.INSTITUTION).withRel(ResourceName.INSTITUTION));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PRES_POLICY).withRel(ResourceName.PRES_POLICY));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.SUB_POLICY).withRel(ResourceName.SUB_POLICY));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.DIS_POLICY).withRel(ResourceName.DIS_POLICY));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PERSON).withRel(ResourceName.PERSON));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.FUNDING_AGENCY).withRel(ResourceName.FUNDING_AGENCY));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.ADDITIONAL_FIELDS_FORMS).withRel(ResourceName.ADDITIONAL_FIELDS_FORMS));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.ARCHIVE_ACL).withRel(ResourceName.ARCHIVE_ACL));
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.CLOSE).withRel(ActionName.CLOSE));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.UPLOAD_LOGO).withRel(DLCMActionName.UPLOAD_LOGO));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.DOWNLOAD_LOGO).withRel(DLCMActionName.DOWNLOAD_LOGO));

    }
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof OrganizationalUnit)) {
      return false;
    }
    if (!super.equals(other)) {
      return false;
    }
    final OrganizationalUnit castOther = (OrganizationalUnit) other;
    return Objects.equals(this.name, castOther.name) && Objects.equals(this.description, castOther.description) && Objects
            .equals(this.institutions, castOther.institutions) && Objects.equals(this.personRoles, castOther.personRoles);
  }

  public LocalDate getClosingDate() {
    return this.closingDate;
  }

  @JsonIgnore
  public String getDefault() {
    return Role.DEFAULT_ROLE_ID;
  }

  public License getDefaultLicense() {
    return this.defaultLicense;
  }

  public PreservationPolicy getDefaultPreservationPolicy() {
    if (this.defaultPreservationPolicy == null) {
      for (final OrganizationalUnitPreservationPolicy policy : this.getPreservationPolicies()) {
        if (Boolean.TRUE.equals(policy.getDefaultPolicy())) {
          this.defaultPreservationPolicy = policy.getPreservationPolicy();
        }
      }
    }
    return this.defaultPreservationPolicy;
  }

  public SubmissionPolicy getDefaultSubmissionPolicy() {
    if (this.defaultSubmissionPolicy == null) {
      for (final OrganizationalUnitSubmissionPolicy policy : this.getSubmissionPolicies()) {
        if (Boolean.TRUE.equals(policy.getDefaultPolicy())) {
          this.defaultSubmissionPolicy = policy.getSubmissionPolicy();
        }
      }
    }
    return this.defaultSubmissionPolicy;
  }

  public String getDescription() {
    return this.description;
  }

  @JsonIgnore
  public List<OrganizationalUnitDisseminationPolicy> getDisseminationPolicies() {
    return this.disseminationPolicies;
  }

  public List<FundingAgency> getFundingAgencies() {
    return this.fundingAgencies;
  }

  public List<Institution> getInstitutions() {
    return this.institutions;
  }

  public Boolean getIsEmpty() {
    return this.isEmpty;
  }

  public List<String> getKeywords() {
    return this.keywords;
  }

  public String getName() {
    return this.name;
  }

  public LocalDate getOpeningDate() {
    return this.openingDate;
  }

  public List<OrganizationalUnitPersonRole> getPersonRoles() {
    return this.personRoles;
  }

  @JsonIgnore
  public List<OrganizationalUnitPreservationPolicy> getPreservationPolicies() {
    return this.preservationPolicies;
  }

  public List<SubjectArea> getSubjectAreas() {
    return this.subjectAreas;
  }

  public OrganizationalUnitLogo getLogo() {
    return this.logo;
  }

  @JsonIgnore
  public List<OrganizationalUnitSubmissionPolicy> getSubmissionPolicies() {
    return this.submissionPolicies;
  }

  public URL getUrl() {
    return this.url;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.name, this.description, this.institutions, this.personRoles);
  }

  @PrePersist
  @PreUpdate
  @Override
  public void init() {
    if (this.openingDate == null) {
      this.openingDate = LocalDate.now();
    }
    if (this.isEmpty == null) {
      this.isEmpty = true;
    }
  }

  @Schema(description = "If the organizational unit is currently open.")
  public boolean isOpen() {
    final LocalDate now = LocalDate.now();
    return (this.openingDate != null && !this.openingDate.isAfter(now) && (this.closingDate == null || this.closingDate.isAfter(now)));
  }

  @Override
  @JsonIgnore
  public boolean isDeletable() {
    return this.isEmpty;
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public boolean removeFundingAgency(FundingAgency fundingAgency) {
    final boolean result = this.fundingAgencies.remove(fundingAgency);
    if (fundingAgency.getOrganizationalUnits().contains(this)) {
      fundingAgency.removeOrganizationalUnit(this);
    }
    return result;
  }

  public void setLogo(OrganizationalUnitLogo logo) {
    // Don't set dirty field, logo is updated through dedicated endpoints
    this.logo = logo;
  }

  public void setClosingDate(LocalDate closingDate) {
    this.closingDate = closingDate;
  }

  public void setDefaultLicense(License defaultLicense) {
    this.defaultLicense = defaultLicense;
  }

  public void setDefaultPreservationPolicy(PreservationPolicy defaultPolicy) {
    this.defaultPreservationPolicy = defaultPolicy;
  }

  public void setDefaultSubmissionPolicy(SubmissionPolicy defaultPolicy) {
    this.defaultSubmissionPolicy = defaultPolicy;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setInstitutions(List<Institution> institutions) {
    this.institutions = institutions;
  }

  public void setIsEmpty(Boolean isEmpty) {
    this.isEmpty = isEmpty;
  }

  public void setKeywords(List<String> keywords) {
    this.keywords = keywords;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setOpeningDate(LocalDate openingDate) {
    this.openingDate = openingDate;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  @Override
  public String toString() {
    return super.toString() + " (name=" + this.getName() + ")";
  }

  public boolean removeInstitution(Institution institution) {
    final boolean result = this.institutions.remove(institution);
    if (institution.getOrganizationalUnits().contains(this)) {
      institution.removeOrganizationalUnit(this);
    }
    return result;
  }

  public boolean removeSubjectArea(SubjectArea subjectArea) {
    final boolean result = this.subjectAreas.remove(subjectArea);
    if (subjectArea.getOrganizationalUnits().contains(this)) {
      subjectArea.removeOrganizationalUnit(this);
    }
    return result;
  }

  @Override
  @JsonIgnore
  public String getOrganizationalUnitId() {
    return this.getResId();
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setLogo(new OrganizationalUnitLogo());
    return this.getLogo();
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.setLogo((OrganizationalUnitLogo) resourceFile);
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getLogo();
  }

  @Override
  public List<String> getPatchableSubResources() {
    return List.of("logo");
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - Person.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.model.OrcidInfo;
import ch.unige.solidify.auth.model.PersonInfo;
import ch.unige.solidify.model.OrcidToken;
import ch.unige.solidify.model.PersonWithOrcid;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.SearchableResourceNormalized;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.security.Role;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.specification.PersonSearchSpecification;

@Schema(description = "A person is associated to a user on the platform. It contains information such as the person’s ORCID, institution and organizational units it belongs to.")
@Entity
public class Person extends SearchableResourceNormalized<Person>
        implements PersonWithOrcid, RemoteResourceContainer, ResourceFileInterface, PersonInfo {

  @JsonIgnore
  @OneToMany(mappedBy = OrganizationalUnitPersonRole.PATH_TO_PERSON, cascade = CascadeType.ALL, orphanRemoval = true)
  protected List<OrganizationalUnitPersonRole> organizationalUnitRoles = new ArrayList<>();

  @JsonIgnore
  @OneToMany(mappedBy = InstitutionPersonRole.PATH_TO_PERSON, cascade = CascadeType.ALL, orphanRemoval = true)
  protected List<InstitutionPersonRole> institutionRoles = new ArrayList<>();

  @Schema(description = "The first name of the person.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String firstName;

  @Schema(description = "The last name of the person.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String lastName;

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "personInstitutions", joinColumns = { @JoinColumn(name = DLCMConstants.DB_PERSON_ID) }, inverseJoinColumns = {
          @JoinColumn(name = DLCMConstants.DB_INSTITUTION_ID) }, uniqueConstraints = {
                  @UniqueConstraint(columnNames = { DLCMConstants.DB_PERSON_ID, DLCMConstants.DB_INSTITUTION_ID }) })
  private List<Institution> institutions = new ArrayList<>();

  @Schema(description = "The ORCID of the person.")
  @Pattern(regexp = OrcidInfo.ORCID_PATTERN, message = OrcidInfo.ORCID_MESSAGE)
  @Column(unique = true)
  private String orcid;

  @Schema(description = "If the person ORCID is verified.")
  @NotNull
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean verifiedOrcid = false;

  @Schema(description = "The ORCID token of the person.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "orcid_token_id", referencedColumnName = DLCMConstants.DB_RES_ID, nullable = true)
  @JsonIgnore
  private OrcidToken orcidToken;

  @Schema(description = "The avatar of the person.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = DLCMConstants.DB_AVATAR_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private PersonAvatar avatar;

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "personNotificationTypes", joinColumns = { @JoinColumn(name = DLCMConstants.DB_PERSON_ID) }, inverseJoinColumns = {
          @JoinColumn(name = DLCMConstants.DB_NOTIFICATION_TYPE_ID) }, uniqueConstraints = {
                  @UniqueConstraint(columnNames = { DLCMConstants.DB_PERSON_ID, DLCMConstants.DB_NOTIFICATION_TYPE_ID }) })
  private List<NotificationType> subscribedNotificationTypes = new ArrayList<>();

  public boolean addInstitution(Institution institution) {
    final boolean result = this.institutions.add(institution);
    if (!institution.getPeople().contains(this)) {
      institution.addPerson(this);
    }
    return result;
  }

  @Override
  public <T> boolean addItem(T t) {
    if (t instanceof Institution i) {
      return this.addInstitution(i);
    }
    if (t instanceof NotificationType n) {
      return this.addNotificationType(n);
    }
    return false;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.INSTITUTION).withRel(ResourceName.INSTITUTION));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.ORG_UNIT).withRel(ResourceName.ORG_UNIT));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.DOWNLOAD_AVATAR).withRel(DLCMActionName.DOWNLOAD_AVATAR));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.UPLOAD_AVATAR).withRel(DLCMActionName.UPLOAD_AVATAR));
      if (!StringTool.isNullOrEmpty(this.getOrcid())) {
        this.add(
                Tool.filter(
                        linkBuilder.slash(this.getOrcid()).toUriComponentsBuilder(),
                        DLCMConstants.IDENTIFIER_TYPE_PARAM,
                        ResourceIdentifierType.ORCID.toString())
                        .withRel(ResourceIdentifierType.ORCID.toString().toLowerCase()));
      }
    }
  }

  @JsonIgnore
  public String getDefault() {
    return Role.DEFAULT_ROLE_ID;
  }

  /************************************************************/

  public String getFirstName() {
    return this.firstName;
  }

  @Schema(description = "The full name of the person.")
  @Override
  public String getFullName() {
    return this.getLastName() + ", " + this.getFirstName();
  }

  public List<Institution> getInstitutions() {
    return this.institutions;
  }

  public String getLastName() {
    return this.lastName;
  }

  @Override
  public String getOrcid() {
    return this.orcid;
  }

  public List<OrganizationalUnitPersonRole> getOrganizationalUnitRoles() {
    return this.organizationalUnitRoles;
  }

  public List<InstitutionPersonRole> getInstitutionRoles() {
    return this.institutionRoles;
  }

  public PersonAvatar getAvatar() {
    return this.avatar;
  }

  @JsonIgnore
  public List<NotificationType> getSubscribedNotifications() {
    return this.subscribedNotificationTypes;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public boolean removeInstitution(Institution institution) {
    final boolean result = this.institutions.remove(institution);
    if (institution.getPeople().contains(this)) {
      institution.removePerson(this);
    }
    return result;
  }

  @Override
  public <T> boolean removeItem(T t) {
    if (t instanceof Institution i) {
      return this.removeInstitution(i);
    }
    if (t instanceof OrganizationalUnit ou) {
      return this.removeOrganizationalUnit(ou);
    }
    if (t instanceof OrganizationalUnitPersonRole oup) {
      return this.removeOrganizationalUnitRole(oup);
    }
    if (t instanceof NotificationType n) {
      return this.removeNotificationType(n);
    }
    return false;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @Override
  public void setOrcid(String orcid) {
    this.orcid = orcid;
  }

  public void setAvatar(PersonAvatar avatar) {
    this.avatar = avatar;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("Person [resId=").append(this.getResId())
            .append(", firstName=").append(this.firstName)
            .append(", lastName=").append(this.lastName)
            .append(", orcid=").append(this.orcid).append("]");
    return builder.toString();
  }

  protected boolean removeOrganizationalUnit(OrganizationalUnit unit) {
    final OrganizationalUnit organizationalUnit = unit;
    return this.organizationalUnitRoles.removeIf(ouprPredicate -> ouprPredicate.getCompositeKey()
            .getOrganizationalUnit() == organizationalUnit);
  }

  private boolean removeOrganizationalUnitRole(OrganizationalUnitPersonRole unitPersonRole) {
    return this.organizationalUnitRoles.removeIf(ouprPredicate -> ouprPredicate.equals(unitPersonRole));
  }

  public boolean addNotificationType(NotificationType notificationType) {
    final boolean result = this.subscribedNotificationTypes.add(notificationType);
    if (!notificationType.getSubscribers().contains(this)) {
      notificationType.addSubscriber(this);
    }
    return result;
  }

  public boolean removeNotificationType(NotificationType notificationType) {
    final boolean result = this.subscribedNotificationTypes.remove(notificationType);
    if (notificationType.getSubscribers().contains(this)) {
      notificationType.removeSubscriber(this);
    }
    return result;
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setAvatar(new PersonAvatar());
    return this.getAvatar();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getAvatar();
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.setAvatar((PersonAvatar) resourceFile);
  }

  @Override
  public Specification<Person> getSearchSpecification(SearchCriteria criteria) {
    return new PersonSearchSpecification(criteria);
  }

  @Override
  public Boolean isVerifiedOrcid() {
    return this.verifiedOrcid;
  }

  @Override
  public void setVerifiedOrcid(Boolean verifiedOrcid) {
    this.verifiedOrcid = verifiedOrcid;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.avatar, this.firstName, this.institutionRoles, this.institutions, this.lastName, this.orcid,
            this.organizationalUnitRoles, this.subscribedNotificationTypes, this.verifiedOrcid);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    Person other = (Person) obj;
    return Objects.equals(this.avatar, other.avatar) && Objects.equals(this.firstName, other.firstName)
            && Objects.equals(this.institutionRoles, other.institutionRoles) && Objects.equals(this.institutions, other.institutions)
            && Objects.equals(this.lastName, other.lastName) && Objects.equals(this.orcid, other.orcid)
            && Objects.equals(this.organizationalUnitRoles, other.organizationalUnitRoles)
            && Objects.equals(this.subscribedNotificationTypes, other.subscribedNotificationTypes)
            && Objects.equals(this.verifiedOrcid, other.verifiedOrcid);
  }

  public OrcidToken getOrcidToken() {
    return this.orcidToken;
  }

  @Override
  public void setOrcidToken(OrcidToken orcidToken) {
    this.orcidToken = orcidToken;
  }

}

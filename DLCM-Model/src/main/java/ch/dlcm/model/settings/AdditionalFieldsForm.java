/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AdditionalFieldsForm.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.rest.ModuleName;

@Entity
public class AdditionalFieldsForm extends ResourceNormalized {

  @JsonIgnore
  @Transient
  private Boolean isDescriptionDirty = false;

  @ManyToOne
  @JoinColumn(name = "org_unit_res_id", nullable = false)
  @JsonIgnore
  private OrganizationalUnit organizationalUnit;

  /**
   * Used by specification when filtering on parent OrganizationalUnit
   */
  @Transient
  private String parentId;

  @NotNull
  @Enumerated(EnumType.STRING)
  private FormDescriptionType type;

  @NotNull
  private String name;

  @Lob
  @NotNull
  @Column(length = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String description;

  /********************************************************************************/

  public OrganizationalUnit getOrganizationalUnit() {
    return this.organizationalUnit;
  }

  public void setOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    this.organizationalUnit = organizationalUnit;
  }

  public FormDescriptionType getType() {
    return this.type;
  }

  public void setType(FormDescriptionType type) {
    this.type = type;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    if (!Objects.equals(this.description, description)) {
      this.description = description;
      this.isDescriptionDirty = true;
    }
  }

  /********************************************************************************/

  /**
   * Return true if the description has been modified since the object was created
   *
   * @return
   */
  @JsonIgnore
  public boolean isDescriptionDirty() {
    return this.isDescriptionDirty;
  }

  @JsonIgnore
  public String getParentResourceId() {
    return this.parentId;
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public void init() {

  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AdditionalFieldsForm)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    final AdditionalFieldsForm that = (AdditionalFieldsForm) o;
    return this.type == that.type &&
            Objects.equals(this.name, that.name) &&
            Objects.equals(this.organizationalUnit, that.organizationalUnit) &&
            Objects.equals(this.description, that.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.organizationalUnit, this.type, this.name, this.description);
  }
}

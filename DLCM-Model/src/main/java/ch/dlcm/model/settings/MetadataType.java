/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - MetadataType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.net.URL;
import java.util.Objects;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.model.MetadataFormat;
import ch.dlcm.rest.ModuleName;

@Schema(description = "The metadata type allow to define custom metadata.")
@Entity
@Table(name = "metadataType", uniqueConstraints = @UniqueConstraint(columnNames = { "name", "version" }))
public class MetadataType extends ResourceNormalized {

  @Schema(description = "The description of the metadata type.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Schema(description = "The format of the metadata type.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private MetadataFormat metadataFormat;

  @Schema(description = "The schema of the metadata type.")
  @Lob
  @Column(length = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String metadataSchema;

  @Schema(description = "The name of the metadata type.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String name;

  @Schema(description = "The URL of the metadata type.")
  private URL url;

  @Schema(description = "The version of the metadata type.")
  @NotNull
  @Size(min = 1, max = 50)
  private String version;

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(SolidifyConstants.SCHEMA).withRel(SolidifyConstants.SCHEMA));
    }
  }

  public String getDescription() {
    return this.description;
  }

  @Schema(description = "The name and the version of the metadata type.")
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public String getFullName() {
    if (StringTool.isNullOrEmpty(this.getName()) && StringTool.isNullOrEmpty(this.getVersion())) {
      return null;
    }
    return this.getName() + "-" + this.getVersion();
  }

  public MetadataFormat getMetadataFormat() {
    return this.metadataFormat;
  }

  public String getMetadataSchema() {
    return this.metadataSchema;
  }

  public String getName() {
    return this.name;
  }

  public URL getUrl() {
    return this.url;
  }

  public String getVersion() {
    return this.version;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setMetadataFormat(MetadataFormat metadataFormat) {
    this.metadataFormat = metadataFormat;
  }

  public void setMetadataSchema(String metadataSchema) {
    this.metadataSchema = metadataSchema;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.description, this.metadataFormat, this.metadataSchema, this.name, this.url, this.version);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    MetadataType other = (MetadataType) obj;
    return Objects.equals(this.description, other.description) && this.metadataFormat == other.metadataFormat
            && Objects.equals(this.metadataSchema, other.metadataSchema)
            && Objects.equals(this.name, other.name) && Objects.equals(this.url, other.url) && Objects.equals(this.version, other.version);
  }

}

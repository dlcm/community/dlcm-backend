/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - RatingType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.rest.ModuleName;

@Schema(description = "The rating types is the type of grade for a dataset: Quality or Usefulness.")
@Entity
public class RatingType extends ResourceNormalized {

  public static final RatingType USEFULNESS = new RatingType("Usefulness");
  public static final RatingType QUALITY = new RatingType("Quality");

  private static final List<RatingType> RATING_TYPE_LIST = Arrays.asList(USEFULNESS, QUALITY);

  @Schema(description = "The name of the rating type.")
  @NotNull
  @Column(unique = true)
  private String name;

  public RatingType() {
  }

  public RatingType(String resId) {
    this.setResId(resId.toUpperCase());
    this.name = resId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public void init() {
  }

  @Override
  public String managedBy() {
    return ModuleName.ACCESS;
  }

  @Override
  public String toString() {
    return "RatingType " + this.getResId();
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof RatingType))
      return false;
    final RatingType that = (RatingType) o;
    return this.getResId().equals(that.getResId()) &&
            this.getName().equals(that.getName());
  }

  public static List<RatingType> getRatingTypeList() {
    return RATING_TYPE_LIST;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.name);
  }

}

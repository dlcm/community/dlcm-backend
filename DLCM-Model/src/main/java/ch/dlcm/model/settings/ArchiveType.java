/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.rest.ModuleName;

@Schema(description = "Archive type based on DataCite notion.")
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "masterTypeId", "typeName" }) })
public class ArchiveType extends ResourceNormalized {

  public static final String DATASET_ID = ch.dlcm.model.xml.dlcm.v4.mets.ResourceType.DATASET.value().toUpperCase();
  public static final String COLLECTION_ID = ch.dlcm.model.xml.dlcm.v4.mets.ResourceType.COLLECTION.value().toUpperCase();

  @Schema(description = "The master type of the archive type.")
  @ManyToOne
  @JoinColumn(name = "masterTypeId")
  private ArchiveType masterType;

  @OneToMany(mappedBy = "masterType", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<ArchiveType> subTypes = new ArrayList<>();

  @Schema(description = "The name of the archive type.")
  @NotNull
  private String typeName;

  public String getTypeName() {
    return this.typeName;
  }

  public ArchiveType getMasterType() {
    return this.masterType;
  }

  public void setMasterType(ArchiveType masterType) {
    this.masterType = masterType;
  }

  @JsonIgnore
  public List<ArchiveType> getSubTypes() {
    return this.subTypes;
  }

  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  @Schema(description = "If the archive type is a master type.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public boolean isMasterArchiveType() {
    return this.masterType == null;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.masterType, this.typeName);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    ArchiveType other = (ArchiveType) obj;
    return Objects.equals(this.masterType, other.masterType) && Objects.equals(this.typeName, other.typeName);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - Institution.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.converter.WeakManyToManyConverter;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.RorInterface;
import ch.dlcm.model.security.Role;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Schema(description = "An institution represents academic institutions, such as UNIGE and HES-SO.")
@Entity
public class Institution extends ResourceNormalized implements RemoteResourceContainer, ResourceFileInterface, RorInterface {

  @Schema(description = "The description of the institution.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Schema(description = "The email suffixes list of the institution.")
  @Convert(converter = WeakManyToManyConverter.class)
  private List<String> emailSuffixes = new ArrayList<>();

  @Schema(description = "The name of the institution.")
  @NotNull
  @Column(unique = true)
  @Size(min = 1)
  private String name;

  @Schema(description = "The org. unit list of the institution.")
  @ManyToMany(mappedBy = "institutions")
  @JsonIgnore
  private List<OrganizationalUnit> organizationalUnits;

  @Schema(description = "The members of the institution.")
  @JsonIgnore
  @OneToMany(mappedBy = InstitutionPersonRole.PATH_TO_INSTITUTION, cascade = CascadeType.ALL, orphanRemoval = true)
  private List<InstitutionPersonRole> personRoles = new ArrayList<>();

  @ManyToMany(mappedBy = "institutions")
  @JsonIgnore
  private List<Person> people;

  @Schema(description = "The URL of the institution.")
  private URL url;

  @Schema(description = "The ROR identifier of the institution.")
  @Column(unique = true)
  private String rorId;

  @Schema(description = "The logo of the institution.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = DLCMConstants.DB_LOGO_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private InstitutionLogo logo;

  @Schema(description = "The other identifiers list of the institution.")
  @Transient
  private Map<String, String> identifiers = new HashMap<>();

  @Override
  public <T> boolean addItem(T t) {
    if (t instanceof Person p) {
      return this.addPerson(p);
    }
    if (t instanceof OrganizationalUnit orgUnit) {
      return this.addOrganizationalUnit(orgUnit);
    }
    return false;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PERSON).withRel(ResourceName.PERSON));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.ORG_UNIT).withRel(ResourceName.ORG_UNIT));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.UPLOAD_LOGO).withRel(DLCMActionName.UPLOAD_LOGO));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.DOWNLOAD_LOGO).withRel(DLCMActionName.DOWNLOAD_LOGO));
      this.add(Tool.filter(
              linkBuilder.slash(this.getRorId()).toUriComponentsBuilder(),
              DLCMConstants.IDENTIFIER_TYPE_PARAM,
              ResourceIdentifierType.NAME.toString())
              .withRel(ResourceIdentifierType.NAME.toString().toLowerCase()));
      if (!StringTool.isNullOrEmpty(this.getRorId())) {
        this.add(
                Tool.filter(
                        linkBuilder.slash(this.getRorId()).toUriComponentsBuilder(),
                        DLCMConstants.IDENTIFIER_TYPE_PARAM,
                        ResourceIdentifierType.ROR_ID.toString())
                        .withRel(ResourceIdentifierType.ROR_ID.toString().toLowerCase()));
      }
    }
  }

  public void setOrganizationalUnit(List<OrganizationalUnit> organizationalUnits) {
    this.organizationalUnits = organizationalUnits;
  }

  public boolean addOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    final boolean result = this.organizationalUnits.add(organizationalUnit);
    if (!organizationalUnit.getInstitutions().contains(this)) {
      organizationalUnit.addInstitution(this);
    }
    return result;
  }

  public boolean addPerson(Person p) {
    final boolean result = this.people.add(p);
    if (!p.getInstitutions().contains(this)) {
      p.addInstitution(this);
    }
    return result;
  }

  public String getDescription() {
    return this.description;
  }

  public List<String> getEmailSuffixes() {
    return this.emailSuffixes;
  }

  public String getName() {
    return this.name;
  }

  public List<OrganizationalUnit> getOrganizationalUnits() {
    return this.organizationalUnits;
  }

  public List<Person> getPeople() {
    return this.people;
  }

  public URL getUrl() {
    return this.url;
  }

  @Override
  public String getRorId() {
    return this.rorId;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public <T> boolean removeItem(T t) {
    if (t instanceof Person p) {
      return this.removePerson(p);
    }
    if (t instanceof OrganizationalUnit orgUnit) {
      return this.removeOrganizationalUnit(orgUnit);
    }
    return false;
  }

  public boolean removeOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    final boolean result = this.organizationalUnits.remove(organizationalUnit);
    if (organizationalUnit.getInstitutions().contains(this)) {
      organizationalUnit.removeInstitution(this);
    }
    return result;
  }

  public boolean removePerson(Person p) {
    final boolean result = this.people.remove(p);
    if (p.getInstitutions().contains(this)) {
      p.removeInstitution(this);
    }
    return result;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setEmailSuffixes(List<String> emailSuffixes) {
    this.emailSuffixes = emailSuffixes;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  @Override
  public void setRorId(String rorId) {
    this.rorId = rorId;
  }

  public InstitutionLogo getLogo() {
    return this.logo;
  }

  public void setLogo(InstitutionLogo logo) {
    this.logo = logo;
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setLogo(new InstitutionLogo());
    return this.getLogo();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getLogo();
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.setLogo((InstitutionLogo) resourceFile);
  }

  @Override
  public Map<String, String> getIdentifiers() {
    return this.identifiers;
  }

  public List<InstitutionPersonRole> getPersonRoles() {
    return this.personRoles;
  }

  public boolean addInstitutionPersonRole(Person person, Role role) {
    final InstitutionPersonRole institutionPersonRole = new InstitutionPersonRole();
    institutionPersonRole.setInstitution(this);
    institutionPersonRole.setRole(role);
    institutionPersonRole.setPerson(person);
    return this.personRoles.add(institutionPersonRole);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.description, this.emailSuffixes, this.name, this.rorId, this.url);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    Institution other = (Institution) obj;
    return Objects.equals(this.description, other.description) && Objects.equals(this.emailSuffixes, other.emailSuffixes)
            && Objects.equals(this.name, other.name) && Objects.equals(this.rorId, other.rorId) && Objects.equals(this.url, other.url);
  }

}

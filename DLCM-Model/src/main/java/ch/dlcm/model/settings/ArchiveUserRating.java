/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveUserRating.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import static ch.dlcm.DLCMRestFields.ARCHIVE_ID_FIELD;
import static ch.dlcm.DLCMRestFields.RATING_TYPE_FIELD;
import static ch.dlcm.DLCMRestFields.RES_ID_FIELD;
import static ch.dlcm.DLCMRestFields.USER_FIELD;
import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.security.User;
import ch.dlcm.rest.ModuleName;

@Schema(description = "Archive ratings are the grade assigned by an user to an archive.")
@Entity
@Table(name = "archiveUserRatings", uniqueConstraints = {
        @UniqueConstraint(columnNames = { DLCMConstants.DB_ARCHIVE_ID, DLCMConstants.DB_USER_ID, DLCMConstants.DB_RATING_TYPE_ID }) })
public class ArchiveUserRating extends ResourceNormalized {

  @Schema(description = "The archive identifier of the archive rating.")
  @NotNull
  @Column(name = DLCMConstants.DB_ARCHIVE_ID, length = DB_ID_LENGTH)
  private String archiveId;

  @Schema(description = "The user of the archive rating.")
  @NotNull
  @ManyToOne(targetEntity = User.class)
  @JoinColumn(name = DLCMConstants.DB_USER_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private User user;

  @Schema(description = "The rating type of the archive rating.")
  @NotNull
  @ManyToOne(targetEntity = RatingType.class)
  @JoinColumn(name = DLCMConstants.DB_RATING_TYPE_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private RatingType ratingType;

  @Schema(description = "The grade of the archive rating.")
  @NotNull
  private Integer grade;

  public String getArchiveId() {
    return this.archiveId;
  }

  public User getUser() {
    return this.user;
  }

  public RatingType getRatingType() {
    return this.ratingType;
  }

  public void setArchiveId(String archiveId) {
    this.archiveId = archiveId;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void setRatingType(RatingType ratingType) {
    this.ratingType = ratingType;
  }

  public Integer getGrade() {
    return this.grade;
  }

  public void setGrade(Integer grade) {
    this.grade = grade;
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public void init() {
    // Nothing to init
  }

  @Override
  public List<String> getNonUpdatableFields() {
    return Arrays.asList(RES_ID_FIELD, ARCHIVE_ID_FIELD, RATING_TYPE_FIELD, USER_FIELD);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.archiveId, this.grade, this.ratingType, this.user);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    ArchiveUserRating other = (ArchiveUserRating) obj;
    return Objects.equals(this.archiveId, other.archiveId) && Objects.equals(this.grade, other.grade)
            && Objects.equals(this.ratingType, other.ratingType)
            && Objects.equals(this.user, other.user);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrganizationalUnitPersonRole.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.util.Objects;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.rest.JoinResource;

import ch.dlcm.DLCMConstants;
import ch.dlcm.message.cache.OrganizationalUnitPersonCacheMessage;
import ch.dlcm.model.security.Role;

@Entity
@Table(name = "orgUnitPersonRoles", uniqueConstraints = {
        @UniqueConstraint(name = "OrgUnit_Person", columnNames = { DLCMConstants.DB_ORG_UNIT_ID, DLCMConstants.DB_PERSON_ID }) })
public class OrganizationalUnitPersonRole extends JoinResource<OrganizationalUnitPersonRoleKey> {

  public static final String PATH_TO_ORG_UNIT = "compositeKey.organizationalUnit";
  public static final String PATH_TO_PERSON = "compositeKey.person";
  public static final String PATH_TO_ROLE = "compositeKey.role";
  public static final String ORG_UNIT_RELATION_PROPERTY_NAME = "personRoles";
  public static final String PERSON_RELATION_PROPERTY_NAME = "organizationalUnitRoles";
  public static final String ROLE_RELATION_PROPERTY_NAME = "organizationalUnitPersons";

  @EmbeddedId
  @JsonIgnore
  private OrganizationalUnitPersonRoleKey compositeKey;

  public OrganizationalUnitPersonRole() {
    this.compositeKey = new OrganizationalUnitPersonRoleKey();
  }

  @Override
  @JsonIgnore
  public CacheMessage getCacheMessage() {
    return new OrganizationalUnitPersonCacheMessage(this.getPerson().getResId(), this.getOrganizationalUnit().getResId());
  }

  @Override
  public OrganizationalUnitPersonRoleKey getCompositeKey() {
    return this.compositeKey;
  }

  @JsonIgnore
  public OrganizationalUnit getOrganizationalUnit() {
    return this.compositeKey.getOrganizationalUnit();
  }

  @JsonIgnore
  public Person getPerson() {
    return this.compositeKey.getPerson();
  }

  @JsonIgnore
  public Role getRole() {
    return this.compositeKey.getRole();
  }

  @Override
  public void setCompositeKey(OrganizationalUnitPersonRoleKey compositeKey) {
    this.compositeKey = compositeKey;
  }

  @JsonIgnore
  public void setOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    this.compositeKey.setOrganizationalUnit(organizationalUnit);
  }

  @JsonIgnore
  public void setPerson(Person person) {
    this.compositeKey.setPerson(person);
  }

  @JsonIgnore
  public void setRole(Role role) {
    this.compositeKey.setRole(role);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof OrganizationalUnitPersonRole))
      return false;
    if (!super.equals(o))
      return false;
    final OrganizationalUnitPersonRole that = (OrganizationalUnitPersonRole) o;
    return Objects.equals(this.compositeKey, that.compositeKey);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.compositeKey);
  }
}

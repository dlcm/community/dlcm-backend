/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveStatistics.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.util.Objects;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.validation.Valid;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.model.StatisticsInfo;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;

@Schema(description = "Archive statistics are the number of view/download for an archive.")
@Entity
public class ArchiveStatistics extends ResourceNormalized {

  @Valid
  @Embedded
  private StatisticsInfo statistics = new StatisticsInfo();

  @JsonUnwrapped
  public StatisticsInfo getStatistics() {
    return this.statistics;
  }

  public void setStatistics(StatisticsInfo statistics) {
    this.statistics = statistics;
  }

  public void addView() {
    this.getStatistics().setViewNumber(this.getStatistics().getViewNumber() + 1);
  }

  public void addDownload() {
    this.getStatistics().setDownloadNumber(this.getStatistics().getDownloadNumber() + 1);
  }

  @Override
  public void init() {
    this.statistics.init();
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.ADD_VIEW).withRel(DLCMActionName.ADD_VIEW));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.ADD_DOWNLOAD).withRel(DLCMActionName.ADD_DOWNLOAD));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.STATISTICS).withRel(DLCMActionName.STATISTICS));
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.statistics);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    ArchiveStatistics other = (ArchiveStatistics) obj;
    return Objects.equals(this.statistics, other.statistics);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - SubjectArea.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.data.jpa.domain.Specification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.Label;
import ch.unige.solidify.rest.SearchableResourceNormalized;
import ch.unige.solidify.util.SearchCriteria;

import ch.dlcm.DLCMConstants;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.specification.SubjectAreaSearchSpecification;

@Schema(description = "A subject area delineates a range of academic, administrative, and patrimonial disciplines.\nEach of these disciplines may follow its own classification system, reflecting their unique attributes and contexts (for example: SNF Disciplines, http://www.snf.ch/SiteCollectionDocuments/allg_disziplinenliste.pdf, or re3data Subjects, https://www.re3data.org/browse/by-subject/, for academic disciplines). A subject area could be associated with a specific organizational unit.")
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "source", "code" }) })
public class SubjectArea extends SearchableResourceNormalized<SubjectArea> {

  @Schema(description = "The code of the subject area.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_ID_LENGTH)
  private String code;

  @Schema(description = "The name labels for each supported languages of the subject area.")
  @ElementCollection
  @Column(name = DLCMConstants.DB_DOMAIN_ID)
  @CollectionTable(joinColumns = {
          @JoinColumn(name = DLCMConstants.DB_DOMAIN_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = { DLCMConstants.DB_DOMAIN_ID,
                  DLCMConstants.DB_LANGUAGE_ID }))
  private List<Label> labels = new ArrayList<>();

  @Schema(description = "The name of the subject area.")
  @NotNull
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String name;

  @Schema(description = "The source of the subject area.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_SHORT_STRING_LENGTH)
  private String source;

  @ManyToMany(mappedBy = "subjectAreas")
  @JsonIgnore
  private List<OrganizationalUnit> organizationalUnits;

  public String getCode() {
    return this.code;
  }

  public List<Label> getLabels() {
    return this.labels;
  }

  public String getName() {
    return this.name;
  }

  public String getSource() {
    return this.source;
  }

  public List<OrganizationalUnit> getOrganizationalUnits() {
    return this.organizationalUnits;
  }

  public boolean addOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    boolean result = this.organizationalUnits.add(organizationalUnit);
    if (result && !organizationalUnit.getSubjectAreas().contains(this)) {
      result = organizationalUnit.addSubjectArea(this);
    }
    return result;
  }

  public boolean removeOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    boolean result = true;
    if (this.organizationalUnits.contains(organizationalUnit)) {
      result = this.organizationalUnits.remove(organizationalUnit);
      if (result && organizationalUnit.getSubjectAreas().contains(this)) {
        result = organizationalUnit.removeSubjectArea(this);
      }
    }
    return result;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setLabels(List<Label> labels) {
    this.labels = labels;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSource(String source) {
    this.source = source;
  }

  @Override
  public Specification<SubjectArea> getSearchSpecification(SearchCriteria criteria) {
    return new SubjectAreaSearchSpecification(criteria);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.code, this.labels, this.name, this.organizationalUnits, this.source);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    SubjectArea other = (SubjectArea) obj;
    return Objects.equals(this.code, other.code) && Objects.equals(this.labels, other.labels) && Objects.equals(this.name, other.name)
            && Objects.equals(this.organizationalUnits, other.organizationalUnits) && Objects.equals(this.source, other.source);
  }

}

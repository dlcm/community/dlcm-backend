/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrganizationalUnitPersonRoleKey.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.security.Role;

@Embeddable
public class OrganizationalUnitPersonRoleKey implements Serializable {
  @Serial
  private static final long serialVersionUID = -7546695814255442690L;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_ORG_UNIT_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private OrganizationalUnit organizationalUnit;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_PERSON_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private Person person;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_ROLE_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private Role role;

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof OrganizationalUnitPersonRoleKey)) {
      return false;
    }
    final OrganizationalUnitPersonRoleKey castOther = (OrganizationalUnitPersonRoleKey) other;
    return Objects.equals(this.getOrganizationalUnit(), castOther.getOrganizationalUnit())
            && Objects.equals(this.getPerson(), castOther.getPerson());
  }

  public OrganizationalUnit getOrganizationalUnit() {
    return this.organizationalUnit;
  }

  public Person getPerson() {
    return this.person;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getOrganizationalUnit(), this.getPerson());
  }

  public void setOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    this.organizationalUnit = organizationalUnit;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public Role getRole() {
    return this.role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

}

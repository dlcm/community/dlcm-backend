/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveACL.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.settings;

import static ch.dlcm.DLCMRestFields.AIP_ID_FIELD;
import static ch.dlcm.DLCMRestFields.DELETED;
import static ch.dlcm.DLCMRestFields.ORGANIZATIONAL_UNIT_FIELD;
import static ch.dlcm.DLCMRestFields.RES_ID_FIELD;
import static ch.dlcm.DLCMRestFields.USER_FIELD;
import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.security.User;
import ch.dlcm.rest.ModuleName;

@Schema(description = "An archive access control list (ACL) is used to grant users access to restricted or closed archives.")
@Entity
@Table(name = "orgUnitArchiveAcl")
public class ArchiveACL extends ResourceNormalized implements OrganizationalUnitAwareResource, ResourceFileInterface {

  @Schema(description = "The signed DUA of the ACL.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = DLCMConstants.DB_DUA_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private SignedDuaFile signedDuaFile;

  @Schema(description = "The archive identifier of the ACL.")
  @NotNull
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String aipId;

  @Schema(description = "The organizational unit of the ACL.")
  @NotNull
  @ManyToOne(targetEntity = OrganizationalUnit.class)
  @JoinColumn(name = DLCMConstants.DB_ORG_UNIT_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private OrganizationalUnit organizationalUnit;

  @Schema(description = "The user of the ACL.")
  @NotNull
  @ManyToOne(targetEntity = User.class)
  @JoinColumn(name = DLCMConstants.DB_USER_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private User user;

  @Schema(description = "The expiration date of the ACL.")
  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH) // Used by MySQL5ExtendedDialect
  private OffsetDateTime expiration;

  @Schema(description = "If the archive ACL is deleted (logical deletion).")
  @NotNull
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean deleted = false;

  public String getAipId() {
    return this.aipId;
  }

  public void setAipId(String aipId) {
    this.aipId = aipId;
  }

  public OrganizationalUnit getOrganizationalUnit() {
    return this.organizationalUnit;
  }

  public void setOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    this.organizationalUnit = organizationalUnit;
  }

  public User getUser() {
    return this.user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public OffsetDateTime getExpiration() {
    return this.expiration;
  }

  public void setExpiration(OffsetDateTime expiration) {
    this.expiration = expiration;
  }

  public Boolean isDeleted() {
    return this.deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  public SignedDuaFile getSignedDuaFile() {
    return this.signedDuaFile;
  }

  public void setSignedDuaFile(SignedDuaFile signedDuaFile) {
    this.signedDuaFile = signedDuaFile;
  }

  @Schema(description = "If the archive ACL is expired.")
  @JsonProperty(access = Access.READ_ONLY)
  public boolean isExpired() {
    if (this.getExpiration() == null) {
      return false;
    }
    return (this.getExpiration().isBefore(OffsetDateTime.now(ZoneOffset.UTC)));
  }

  @Override
  public void init() {
    if (this.expiration == null) {
      // Be default 3 month access
      this.expiration = OffsetDateTime.now(ZoneOffset.UTC).plusMonths(3);
    }
    if (this.deleted == null) {
      this.deleted = false;
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public List<String> getNonUpdatableFields() {
    return Arrays.asList(RES_ID_FIELD, AIP_ID_FIELD, USER_FIELD, ORGANIZATIONAL_UNIT_FIELD, DELETED);
  }

  @Override
  @JsonIgnore
  public String getOrganizationalUnitId() {
    return this.getOrganizationalUnit().getResId();
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setSignedDuaFile(new SignedDuaFile());
    return this.getSignedDuaFile();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getSignedDuaFile();
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.setSignedDuaFile((SignedDuaFile) resourceFile);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.aipId, this.deleted, this.expiration, this.organizationalUnit, this.signedDuaFile, this.user);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    ArchiveACL other = (ArchiveACL) obj;
    return Objects.equals(this.aipId, other.aipId) && Objects.equals(this.deleted, other.deleted)
            && Objects.equals(this.expiration, other.expiration) && Objects.equals(this.organizationalUnit, other.organizationalUnit)
            && Objects.equals(this.signedDuaFile, other.signedDuaFile) && Objects.equals(this.user, other.user);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - Deposit.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preingest;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_DEFAULT_STRING_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_LONG_STRING_LENGTH;

import java.net.URI;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.persistence.CascadeType;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Transient;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.converter.KeywordsConverter;
import ch.unige.solidify.model.Language;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.SearchableResourceNormalized;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.AssertThatAnotherFieldHasValue;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMDocConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.deserializer.OffsetDateTimeDeserializer;
import ch.dlcm.listener.HistoryListener;
import ch.dlcm.listener.OrganizationalUnitDataListener;
import ch.dlcm.model.Access;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileOwnerInterface;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.EmbargoInfo;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.MetadataVersionAware;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.ArchiveType;
import ch.dlcm.model.settings.License;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.model.settings.SubjectArea;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.specification.DepositSearchSpecification;

@Schema(description = """
        A deposit is a simple package to prepare a SIP.
        Deposit status:
        - APPROVED => Approved deposit. The SIP preparation is in progress.
        - CANCEL_EDITING_METADATA => A cancellation of metadata edition is in progress.
        - CHECKED => Checked deposit. The deposit is ready for SIP preparation.
        - CHECKING_COMPLIANCE => A compliance check is in progress for completed deposits.
        - CHECKING_COMPLIANCE_CLEANED => A compliance check is in progress for cleaned deposits.
        - CLEANED => Cleaned deposit: the data file are purged based on submission policy.
        - CLEANING => A clean process is in progress.
        - COMPLETED => Completed deposit. The preparation of SIP is completed.
        - COMPLIANCE_ERROR => An error occurred during a compliance check.
        - DELETING => A deletion process is in progress.
        - EDITING_METADATA_REJECTED => The metadata edition was rejected.
        - EDITING_METADATA => The deposit is in edition mode for metadata.
        - IN_ERROR => An error occurred during the SIP preparation.
        - IN_PROGRESS => The deposit is in progress, ready for adding data files.
        - IN_VALIDATION => The deposit is waiting for an approval.
        - REJECTED => The deposit was rejected.
        - SUBMITTED => Submitted deposit. The SIP preparation is in progress.
        - UPGRADING_METADATA => The deposit is in upgrade mode of metadata version.
        """)
@Entity
@EntityListeners({ HistoryListener.class, OrganizationalUnitDataListener.class })
@AssertThatAnotherFieldHasValue.List({
        @AssertThatAnotherFieldHasValue(fieldName = "access", fieldValue = "PUBLIC", dependFieldName = "licenseId") })
public class Deposit extends SearchableResourceNormalized<Deposit> implements RemoteResourceContainer, DataFileOwnerInterface<DepositDataFile>,
        OrganizationalUnitAwareResource, MetadataVersionAware {

  public enum DepositStatus {
    APPROVED, CHECKED, CLEANED, CLEANING, COMPLETED, DELETING, IN_ERROR, IN_PROGRESS, IN_VALIDATION, REJECTED, SUBMITTED, EDITING_METADATA, UPGRADING_METADATA, CHECKING_COMPLIANCE, CHECKING_COMPLIANCE_CLEANED, COMPLIANCE_ERROR, CANCEL_EDITING_METADATA, EDITING_METADATA_REJECTED, SUBMISSION_AGREEMENT_APPROVED;

    public static boolean isCompletedProcess(DepositStatus status) {
      return (status == DepositStatus.COMPLETED
              || status == DepositStatus.CLEANED
              || status == DepositStatus.REJECTED
              || status == DepositStatus.EDITING_METADATA_REJECTED);
    }

    public static boolean isInError(DepositStatus status) {
      return (status == DepositStatus.IN_ERROR
              || status == DepositStatus.COMPLIANCE_ERROR);
    }
  }

  @Schema(description = "The access level of the deposit.")
  @Enumerated(EnumType.STRING)
  private Access access;

  @Schema(description = "The data sensitivity of the deposit.")
  @Enumerated(EnumType.STRING)
  @NotNull
  private DataTag dataSensitivity;

  @Schema(description = "The data use policy of the deposit.")
  @Enumerated(EnumType.STRING)
  private DataUsePolicy dataUsePolicy;

  @Schema(description = "The number of deposit data files.", accessMode = AccessMode.READ_ONLY)
  @Transient
  private int dataFileNumber;

  @Schema(description = "The compliance level of the deposit.")
  @Transient
  private ComplianceLevel complianceLevel;

  @ElementCollection
  @Column(name = DLCMConstants.DB_AIP_ID, length = DB_ID_LENGTH)
  @CollectionTable(name = "depositAip", joinColumns = {
          @JoinColumn(name = DLCMConstants.DB_COLLECTION_ID, referencedColumnName = DLCMConstants.DB_RES_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = {
                  DLCMConstants.DB_COLLECTION_ID, DLCMConstants.DB_AIP_ID }))
  @JsonIgnore
  private List<String> collection = new ArrayList<>();

  @Schema(description = "The start date of the deposit for collecting data.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
  @Column(length = DB_DATE_LENGTH)
  private OffsetDateTime collectionBegin;

  @Schema(description = "The end date of the deposit for collecting data.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
  @Column(length = DB_DATE_LENGTH)
  private OffsetDateTime collectionEnd;

  @JsonIgnore
  @OneToMany(mappedBy = "compositeResId.deposit", cascade = CascadeType.ALL, orphanRemoval = true)
  @OrderBy("position ASC")
  private List<DepositContributor> contributors = new ArrayList<>();

  @JsonIgnore
  @OneToMany(mappedBy = "infoPackage", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<DepositDataFile> dataFiles = new ArrayList<>();

  @Schema(description = "The description of the deposit.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_LARGE_STRING_LENGTH)
  private String description;

  @Schema(description = "The Digital Object Identifier (DOI) of the deposit.")
  @Column(unique = true)
  private String doi;

  @Schema(description = "The ARK Identifier of the deposit.")
  @Column(unique = true)
  private String ark;

  @Schema(description = "The embargo of the deposit.")
  @Embedded
  @Valid
  private EmbargoInfo embargo = new EmbargoInfo();

  @Schema(description = "The keyword List of the deposit.")
  @Convert(converter = KeywordsConverter.class)
  private List<String> keywords;

  @Schema(description = "The language object of the deposit.")
  @Transient
  private Language language;

  @Schema(description = "The language identifier of the deposit.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String languageId;

  @Schema(description = "The license identifier of the deposit.")
  private String licenseId;

  @Schema(description = "The archive type identifier of the deposit.")
  private String archiveTypeId;

  @Schema(description = "The archive type object of the deposit.")
  @Transient
  private ArchiveType archiveType;

  @Schema(description = "The metadata version of the deposit.")
  @NotNull
  private DLCMMetadataVersion metadataVersion;

  @Schema(description = "The organizational unit object of the deposit.")
  @Transient
  private OrganizationalUnit organizationalUnit;

  @Schema(description = "The organizational unit identifier of the deposit.")
  @NotNull
  @Size(min = 1)
  @Column(name = DLCMConstants.DB_ORG_UNIT_ID, length = DB_ID_LENGTH)
  private String organizationalUnitId;

  @Schema(description = "The preparation identifier of the deposit.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String preparationId;

  @Schema(description = "The preservation policy object of the deposit.")
  @Transient
  private PreservationPolicy preservationPolicy;

  @Schema(description = "The preservation policy identifier of the deposit.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String preservationPolicyId;

  @Schema(description = "The publication date of the deposit.")
  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate publicationDate;

  @JsonIgnore
  private URI sip;

  @Schema(description = "The generated SIP identifier of the deposit.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String sipId;

  @Schema(description = "The status of the deposit.")
  @Enumerated(EnumType.STRING)
  private DepositStatus status;

  @Schema(description = "The detailed message related to the deposit status.")
  @Size(max = DB_LONG_STRING_LENGTH)
  private String statusMessage;

  @Schema(description = "The submission policy object of the deposit.")
  @Transient
  private SubmissionPolicy submissionPolicy;

  @Schema(description = "The submission policy identifier of the deposit.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String submissionPolicyId;

  @Schema(description = "The title of the deposit.")
  @NotNull
  @Size(min = 1, max = DB_DEFAULT_STRING_LENGTH)
  private String title;

  @Schema(description = DLCMDocConstants.ARCHIVE_UPDATED_METADATE)
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean containsUpdatedMetadata = false;

  @Schema(description = DLCMDocConstants.ARCHIVE_PUBLIC_STRUCTURE)
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean contentStructurePublic = false;

  @Schema(description = "DOI indicates that Archive A is identical to B, used to register two separate instances of the same resource.")
  private String isIdenticalTo;

  @Schema(description = "DOI indicates A is replaced by B.")
  private String isObsoletedBy;

  @Schema(description = "List of DOI which indicates A is used as a source of information by B.")
  @Column(length = DB_LONG_STRING_LENGTH)
  @Convert(converter = KeywordsConverter.class)
  private List<String> isReferencedBy;

  @ElementCollection
  @Column(name = DLCMConstants.DB_SUBJECT_AREA_ID, length = DB_ID_LENGTH)
  @CollectionTable(name = "depositSubjectArea", joinColumns = {
          @JoinColumn(name = DLCMConstants.DB_DEPOSIT_ID, referencedColumnName = DLCMConstants.DB_RES_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = {
                  DLCMConstants.DB_DEPOSIT_ID, DLCMConstants.DB_SUBJECT_AREA_ID }))
  @JsonIgnore
  private List<String> subjectAreas = new ArrayList<>();

  @Schema(description = "The id of the page listing all anonymized deposits of a given deposit")
  @Transient
  private String anonymizedDepositPageId;

  @Override
  public <T> boolean addItem(T t) {

    if (t instanceof Person person) {
      return this.addContributor(person);
    } else if (t instanceof DepositDataFile depositDataFile) {
      return this.addDataFile(depositDataFile);
    } else if (t instanceof ArchivalInfoPackage aip) {
      return this.addAip(aip);
    } else {
      if (t instanceof SubjectArea subjectArea) {
        this.addSubjectArea(subjectArea);
      } else if (t instanceof Language lang) {
        this.setLanguageId(lang.getResId());
      } else if (t instanceof License license) {
        this.setLicenseId(license.getResId());
      } else if (t instanceof OrganizationalUnit orgUnit) {
        this.setOrganizationalUnitId(orgUnit.getResId());
      } else if (t instanceof SubmissionPolicy subPolicy) {
        this.setSubmissionPolicyId(subPolicy.getResId());
      } else if (t instanceof PreservationPolicy presPolicy) {
        this.setPreservationPolicyId(presPolicy.getResId());
      } else if (t instanceof ArchiveType arcType) {
        this.setArchiveTypeId(arcType.getResId());
      } else {
        throw new UnsupportedOperationException("Missing implementation 'addItem' method (" + t.getClass().getSimpleName() + ")");
      }

      return true;
    }
  }

  @Schema(description = "Some eventual additional fields values generated through an additional fields form.")
  @Lob
  @Column(length = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String additionalFieldsValues;

  @Schema(description = "The additional fields form ID used to generate the additional fields values.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String additionalFieldsFormId;

  @JsonIgnore
  @Transient
  private Boolean isStatusDirty = false;

  /****************************************************************/

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.CONTRIBUTOR).withRel(ResourceName.CONTRIBUTOR));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.SUBJECT_AREA).withRel(ResourceName.SUBJECT_AREA));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.DATAFILE).withRel(ResourceName.DATAFILE));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.AIP).withRel(ResourceName.AIP));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.SUBMISSION_AGREEMENT).withRel(ResourceName.SUBMISSION_AGREEMENT));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.LICENSE).withRel(ResourceName.LICENSE));
      if (this.getStatus() == DepositStatus.COMPLETED) {
        this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.SIP_FILE).withRel(ResourceName.SIP_FILE));
        this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.CLEAN).withRel(DLCMActionName.CLEAN));
      }
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.UPLOAD).withRel(ActionName.UPLOAD));
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.UPLOAD_ARCHIVE).withRel(ActionName.UPLOAD_ARCHIVE));
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));

      switch (this.getStatus()) {
        case IN_PROGRESS -> {
          if (this.getSubmissionPolicy() != null) {
            if (Boolean.TRUE.equals(this.getSubmissionPolicy().getSubmissionApproval())) {
              this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.SUBMIT_FOR_APPROVAL).withRel(DLCMActionName.SUBMIT_FOR_APPROVAL));
            } else {
              this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.APPROVE).withRel(DLCMActionName.APPROVE));
            }
          }
        }
        case IN_VALIDATION -> {
          this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.APPROVE).withRel(DLCMActionName.APPROVE));
          this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.REJECT).withRel(DLCMActionName.REJECT));
          this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.ENABLE_REVISION).withRel(DLCMActionName.ENABLE_REVISION));
        }
        case IN_ERROR, REJECTED -> this
                .add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.ENABLE_REVISION).withRel(DLCMActionName.ENABLE_REVISION));
        case COMPLETED, CLEANED -> this
                .add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.CHECK_COMPLIANCE).withRel(DLCMActionName.CHECK_COMPLIANCE));
        default -> {
          // Do nothing
        }
      }
    }
  }

  public Access getAccess() {
    return this.access;
  }

  public DataTag getDataSensitivity() {
    return this.dataSensitivity;
  }

  public DataUsePolicy getDataUsePolicy() {
    return this.dataUsePolicy;
  }

  @JsonIgnore
  public List<String> getCollection() {
    return this.collection;
  }

  public OffsetDateTime getCollectionBegin() {
    return this.collectionBegin;
  }

  public OffsetDateTime getCollectionEnd() {
    return this.collectionEnd;
  }

  @Schema(description = "The number of archives in the deposit.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public int getCollectionSize() {
    return this.getCollection().size();
  }

  public ComplianceLevel getComplianceLevel() {
    return this.complianceLevel;
  }

  public void setComplianceLevel(ComplianceLevel complianceLevel) {
    this.complianceLevel = complianceLevel;
  }

  @JsonIgnore
  public List<String> getContributorIds() {
    return this.getContributors().stream().sorted(Comparator.comparing(DepositContributor::getPosition))
            .map(DepositContributor::getPersonId)
            .toList();
  }

  @JsonIgnore
  public List<DepositContributor> getContributors() {
    return this.contributors;
  }

  public int getDataFileNumber() {
    return this.dataFileNumber;
  }

  public void setDataFileNumber(int dataFileNumber) {
    this.dataFileNumber = dataFileNumber;
  }

  @Override
  public List<DepositDataFile> getDataFiles() {
    return this.dataFiles;
  }

  public List<String> getSubjectAreas() {
    return this.subjectAreas;
  }

  /****************************************************************/

  @JsonIgnore
  public DepositStatus getDefaultStatus() {
    return DepositStatus.IN_PROGRESS;
  }

  public String getDescription() {
    return this.description;
  }

  public String getDoi() {
    return this.doi;
  }

  public String getArk() {
    return this.ark;
  }

  public EmbargoInfo getEmbargo() {
    return this.embargo;
  }

  @JsonIgnore
  public Access getEmbargoAccess() {
    return this.getEmbargo().getAccess();
  }

  @JsonIgnore
  public Integer getEmbargoMonths() {
    return this.getEmbargo().getMonths();
  }

  @JsonIgnore
  public OffsetDateTime getEmbargoStartDate() {
    return this.getEmbargo().getStartDate();
  }

  @Override
  @JsonIgnore
  public List<Class<? extends Resource>> getEmbeddedResourceTypes() {
    final List<Class<? extends Resource>> embeddedResources = new ArrayList<>();

    embeddedResources.add(Language.class);
    embeddedResources.add(OrganizationalUnit.class);
    embeddedResources.add(SubmissionPolicy.class);
    embeddedResources.add(PreservationPolicy.class);
    embeddedResources.add(ArchiveType.class);

    return embeddedResources;
  }

  public boolean getHasEmbargo() {
    return this.hasEmbargo();
  }

  public List<String> getKeywords() {
    return this.keywords;
  }

  public Language getLanguage() {
    return this.language;
  }

  public Boolean getContentStructurePublic() {
    return this.contentStructurePublic;
  }

  /****************************************************************
   * Embedded resources
   */

  public String getLanguageId() {
    return this.languageId;
  }

  /*****/

  public String getLicenseId() {
    return this.licenseId;
  }

  @Override
  public DLCMMetadataVersion getMetadataVersion() {
    return this.metadataVersion;
  }

  public OrganizationalUnit getOrganizationalUnit() {
    return this.organizationalUnit;
  }

  /*****/

  @Override
  public String getOrganizationalUnitId() {
    return this.organizationalUnitId;
  }

  public String getPreparationId() {
    return this.preparationId;
  }

  public String getArchiveTypeId() {
    return this.archiveTypeId;
  }

  public ArchiveType getArchiveType() {
    return this.archiveType;
  }

  /*****/

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public PreservationPolicy getPreservationPolicy() {
    if (this.getPreservationPolicyId() == null) {
      if (this.getOrganizationalUnit() != null) {
        return this.getOrganizationalUnit().getDefaultPreservationPolicy();
      } else {
        return null;
      }
    }
    return this.preservationPolicy;
  }

  public String getPreservationPolicyId() {
    return this.preservationPolicyId;
  }

  public LocalDate getPublicationDate() {
    return this.publicationDate;
  }

  public URI getSip() {
    return this.sip;
  }

  public String getSipId() {
    return this.sipId;
  }

  public DepositStatus getStatus() {
    return this.status;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public String getAnonymizedDepositPageId() {
    return anonymizedDepositPageId;
  }

  /****************************************************************
   * Linked resources
   */

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public SubmissionPolicy getSubmissionPolicy() {
    if (this.getSubmissionPolicyId() == null) {
      if (this.getOrganizationalUnit() != null) {
        return this.getOrganizationalUnit().getDefaultSubmissionPolicy();
      } else {
        return null;
      }
    }
    return this.submissionPolicy;
  }

  public String getSubmissionPolicyId() {
    return this.submissionPolicyId;
  }

  @Override
  public <T> List<String> getSubResourceIds(Class<T> type) {

    List<String> subResourceIds = Collections.emptyList();

    if (type == Person.class && this.getContributors() != null) {
      subResourceIds = this.getContributors().stream()
              .map(DepositContributor::getPersonId)
              .toList();
    } else if (type == Language.class && this.getLanguageId() != null) {
      subResourceIds = CollectionTool.initList(this.getLanguageId());
    } else if (type == License.class && this.getLicenseId() != null) {
      subResourceIds = CollectionTool.initList(this.getLicenseId());
    } else if (type == OrganizationalUnit.class && this.getOrganizationalUnitId() != null) {
      subResourceIds = CollectionTool.initList(this.getOrganizationalUnitId());
    } else if (type == ArchivalInfoPackage.class && this.getCollection() != null) {
      subResourceIds = this.getCollection();
    } else if (type == SubmissionPolicy.class && this.getSubmissionPolicyId() != null) {
      subResourceIds = CollectionTool.initList(this.getSubmissionPolicyId());
    } else if (type == PreservationPolicy.class && this.getPreservationPolicyId() != null) {
      subResourceIds = CollectionTool.initList(this.getPreservationPolicyId());
    } else if (type == ArchiveType.class && this.getArchiveTypeId() != null) {
      subResourceIds = CollectionTool.initList(this.getArchiveTypeId());
    } else if (type == SubjectArea.class && this.getSubjectAreas() != null) {
      subResourceIds = this.getSubjectAreas();
    } else if (type == null) {
      throw new IllegalArgumentException("type should not be null");
    } else if (!this.getEmbeddedResourceTypes().contains(type)) {
      throw new IllegalArgumentException("Class " + type.getCanonicalName() + " is not a subresource of Deposit");
    }

    return subResourceIds;
  }

  /****************************************************************/

  public String getTitle() {
    return this.title;
  }

  @Schema(description = "If the deposit has an embargo.")
  public boolean hasEmbargo() {
    return (this.embargo != null && this.embargo.hasEmbargoDefined());
  }

  public String getAdditionalFieldsValues() {
    return this.additionalFieldsValues;
  }

  public void setAdditionalFieldsValues(String additionalFieldsValues) {
    this.additionalFieldsValues = additionalFieldsValues;
  }

  public String getAdditionalFieldsFormId() {
    return this.additionalFieldsFormId;
  }

  public void setAdditionalFieldsFormId(String additionalFieldsFormId) {
    this.additionalFieldsFormId = additionalFieldsFormId;
  }

  public String getIsIdenticalTo() {
    return this.isIdenticalTo;
  }

  public List<String> getIsReferencedBy() {
    if (this.isReferencedBy == null) {
      this.isReferencedBy = new ArrayList<>();
    }
    return this.isReferencedBy;
  }

  public String getIsObsoletedBy() {
    return this.isObsoletedBy;
  }

  public void setContentStructurePublic(Boolean structureContentPublic) {
    this.contentStructurePublic = structureContentPublic;
  }

  /****************************************************************/

  @PrePersist
  @PreUpdate
  @Override
  public void init() {
    if (this.status == null) {
      this.status = this.getDefaultStatus();
    }
    if (this.publicationDate == null) {
      this.publicationDate = LocalDate.now();
    }
    if (this.access == null) {
      this.access = Access.PUBLIC;
      this.contentStructurePublic = true;
    }
    if (this.dataSensitivity == null) {
      this.dataSensitivity = DataTag.BLUE;
    }
    if (this.dataUsePolicy == null) {
      this.dataUsePolicy = DataUsePolicy.LICENSE;
    }
    if (this.metadataVersion == null) {
      this.metadataVersion = DLCMMetadataVersion.getDefaultVersion();
    }
  }

  public boolean isCollection() {
    return (!this.collection.isEmpty());
  }

  @Override
  public boolean isDeletable() {
    return (this.getStatus() == DepositStatus.REJECTED
            || this.getStatus() == DepositStatus.IN_ERROR
            || this.getStatus() == DepositStatus.IN_PROGRESS
            || this.getStatus() == DepositStatus.IN_VALIDATION
            || this.getStatus() == DepositStatus.COMPLETED
            || this.getStatus() == DepositStatus.CLEANED);
  }

  @Override
  @JsonIgnore
  public boolean isModifiable() {
    return (this.getStatus() != DepositStatus.APPROVED
            && this.getStatus() != DepositStatus.REJECTED
            && this.getStatus() != DepositStatus.COMPLETED
            && this.getStatus() != DepositStatus.CLEANED);
  }

  public boolean getContainsUpdatedMetadata() {
    return this.containsUpdatedMetadata;
  }

  @Override
  public String managedBy() {
    return ModuleName.PREINGEST;
  }

  @Override
  public <T> boolean removeItem(T t) {

    if (t instanceof Person person) {
      return this.removeContributor(person);
    } else if (t instanceof DepositDataFile depositDataFile) {
      return this.removeDataFile(depositDataFile);
    } else if (t instanceof ArchivalInfoPackage aip) {
      return this.removeAip(aip);
    } else {

      if (t instanceof Language) {
        this.setLanguageId(null);
      } else if (t instanceof License) {
        this.setLicenseId(null);
      } else if (t instanceof OrganizationalUnit) {
        this.setOrganizationalUnitId(null);
      } else if (t instanceof SubmissionPolicy) {
        this.setSubmissionPolicyId(null);
      } else if (t instanceof PreservationPolicy) {
        this.setPreservationPolicyId(null);
      } else if (t instanceof ArchiveType) {
        this.setArchiveTypeId(null);
      } else if (t instanceof SubjectArea subjectArea) {
        return this.removeSubjectArea(subjectArea);
      } else {
        throw new UnsupportedOperationException("Missing implementation 'removeItem' method (" + t.getClass().getSimpleName() + ")");
      }

      return true;
    }

  }

  public void setAccess(Access access) {
    this.access = access;
  }

  public void setDataSensitivity(DataTag dataSensitivity) {
    this.dataSensitivity = dataSensitivity;
  }

  public void setDataUsePolicy(DataUsePolicy dataUsePolicy) {
    this.dataUsePolicy = dataUsePolicy;
  }

  public void setCollectionBegin(OffsetDateTime collectionBegin) {
    this.collectionBegin = collectionBegin;
  }

  public void setCollectionEnd(OffsetDateTime collectionEnd) {
    this.collectionEnd = collectionEnd;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public void setArk(String ark) {
    this.ark = ark;
  }

  public void setEmbargo(EmbargoInfo embargo) {
    this.embargo = embargo;
  }

  public void setEmbargoAccess(Access access) {
    this.getEmbargo().setAccess(access);
  }

  public void setEmbargoMonths(Integer months) {
    this.getEmbargo().setMonths(months);
  }

  public void setEmbargoStartDate(OffsetDateTime startDate) {
    this.getEmbargo().setStartDate(startDate);
  }

  public void setErrorStatusWithMessage(String statusMessage) {
    this.setStatus(DepositStatus.IN_ERROR);
    this.setStatusMessage(statusMessage);
  }

  public void setKeywords(List<String> keywords) {
    this.keywords = keywords;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

  public void setLanguageId(String languageId) {
    this.languageId = languageId;
  }

  public void setLicenseId(String licenseId) {
    this.licenseId = licenseId;
  }

  public void setArchiveTypeId(String archiveTypeId) {
    this.archiveTypeId = archiveTypeId;
  }

  public void setArchiveType(ArchiveType archiveType) {
    this.archiveType = archiveType;
  }

  public void setMetadataVersion(DLCMMetadataVersion metadataVersion) {
    this.metadataVersion = metadataVersion;
  }

  public void setOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    this.organizationalUnit = organizationalUnit;
  }

  public void setOrganizationalUnitId(String organizationalUnitId) {
    this.organizationalUnitId = organizationalUnitId;
  }

  public void setPreparationId(String preparationId) {
    this.preparationId = preparationId;
  }

  @JsonIgnore
  public void setPreservationPolicy(PreservationPolicy preservationPolicy) {
    this.preservationPolicy = preservationPolicy;
  }

  public void setPreservationPolicyId(String preservationPolicyId) {
    this.preservationPolicyId = preservationPolicyId;
  }

  public void setPublicationDate(LocalDate publicationDate) {
    this.publicationDate = publicationDate;
  }

  public void setSip(URI sip) {
    this.sip = sip;
  }

  public void setSipId(String sipId) {
    this.sipId = sipId;
  }

  public void setStatus(DepositStatus status) {
    if (this.getStatus() != status) {
      this.status = status;
      this.isStatusDirty = true;
    }
  }

  /**
   * Return true if the status has been modified since the object has been created
   *
   * @return
   */
  @JsonIgnore
  public boolean isStatusDirty() {
    return this.isStatusDirty;
  }

  public void setStatusMessage(String message) {
    this.statusMessage = StringTool.truncateWithEllipsis(message, DB_LONG_STRING_LENGTH);
  }

  public void setAnonymizedDepositPageId(String anonymizedDepositPageId) {
    this.anonymizedDepositPageId = anonymizedDepositPageId;
  }

  @JsonIgnore
  public void setSubmissionPolicy(SubmissionPolicy submissionPolicy) {
    this.submissionPolicy = submissionPolicy;
  }

  public void setSubmissionPolicyId(String submissionPolicyId) {
    this.submissionPolicyId = submissionPolicyId;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setContainsUpdatedMetadata(boolean containsUpdatedMetadataMetadata) {
    this.containsUpdatedMetadata = containsUpdatedMetadataMetadata;
  }

  public void setIsIdenticalTo(String doi) {
    this.isIdenticalTo = doi;
  }

  public void setIsReferencedBy(List<String> dois) {
    this.isReferencedBy = dois;
  }

  public void setIsObsoletedBy(String doi) {
    this.isObsoletedBy = doi;
  }

  private boolean addAip(ArchivalInfoPackage aip) {
    this.collection.add(aip.getResId());
    return true;
  }

  private boolean addContributor(Person person) {
    final DepositContributor contributor = new DepositContributor();
    contributor.setDeposit(this);
    contributor.setPersonId(person.getResId());
    contributor.setPosition(this.contributors.size() * 10);
    return this.contributors.add(contributor);
  }

  @Override
  public boolean addDataFile(DepositDataFile depositDataFile) {
    if (this.getStatus() == DepositStatus.IN_PROGRESS ||
            this.getStatus() == DepositStatus.EDITING_METADATA) {
      return this.dataFiles.add(depositDataFile);
    } else {
      return false;
    }
  }

  @Override
  public Specification getSearchSpecification(SearchCriteria criteria) {
    return new DepositSearchSpecification(criteria);
  }

  public boolean addSubjectArea(SubjectArea subjectArea) {
    return this.subjectAreas.add(subjectArea.getResId());
  }

  private boolean removeAip(ArchivalInfoPackage t) {
    return !this.collection.contains(t.getResId()) || this.collection.remove(t.getResId());
  }

  private boolean removeContributor(Person person) {
    return this.contributors.removeIf(depositContributor -> depositContributor.getPersonId().equals(person.getResId()));
  }

  private boolean removeDataFile(DepositDataFile t) {
    if (this.getStatus() == DepositStatus.IN_PROGRESS) {
      return this.dataFiles.remove(t);
    } else {
      return false;
    }
  }

  public boolean removeSubjectArea(SubjectArea subjectArea) {
    if (this.subjectAreas.contains(subjectArea.getResId())) {
      return this.subjectAreas.remove(subjectArea.getResId());
    }
    return false;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    Deposit deposit = (Deposit) o;
    return this.dataFileNumber == deposit.dataFileNumber &&
            this.access == deposit.access &&
            this.dataSensitivity == deposit.dataSensitivity &&
            this.dataUsePolicy == deposit.dataUsePolicy &&
            this.complianceLevel == deposit.complianceLevel &&
            Objects.equals(this.collection, deposit.collection) &&
            Objects.equals(this.collectionBegin, deposit.collectionBegin) &&
            Objects.equals(this.collectionEnd, deposit.collectionEnd) &&
            Objects.equals(this.contributors, deposit.contributors) &&
            Objects.equals(this.dataFiles, deposit.dataFiles) &&
            Objects.equals(this.description, deposit.description) &&
            Objects.equals(this.doi, deposit.doi) &&
            Objects.equals(this.ark, deposit.ark) &&
            Objects.equals(this.embargo, deposit.embargo) &&
            Objects.equals(this.keywords, deposit.keywords) &&
            Objects.equals(this.language, deposit.language) &&
            Objects.equals(this.languageId, deposit.languageId) &&
            Objects.equals(this.licenseId, deposit.licenseId) &&
            Objects.equals(this.archiveTypeId, deposit.archiveTypeId) &&
            Objects.equals(this.archiveType, deposit.archiveType) &&
            this.metadataVersion == deposit.metadataVersion &&
            Objects.equals(this.organizationalUnit, deposit.organizationalUnit) &&
            Objects.equals(this.organizationalUnitId, deposit.organizationalUnitId) &&
            Objects.equals(this.preparationId, deposit.preparationId) &&
            Objects.equals(this.preservationPolicy, deposit.preservationPolicy) &&
            Objects.equals(this.preservationPolicyId, deposit.preservationPolicyId) &&
            Objects.equals(this.publicationDate, deposit.publicationDate) &&
            Objects.equals(this.sip, deposit.sip) &&
            Objects.equals(this.sipId, deposit.sipId) &&
            this.status == deposit.status &&
            Objects.equals(this.statusMessage, deposit.statusMessage) &&
            Objects.equals(this.submissionPolicy, deposit.submissionPolicy) &&
            Objects.equals(this.submissionPolicyId, deposit.submissionPolicyId) &&
            Objects.equals(this.title, deposit.title) &&
            Objects.equals(this.containsUpdatedMetadata, deposit.containsUpdatedMetadata) &&
            Objects.equals(this.contentStructurePublic, deposit.contentStructurePublic) &&
            Objects.equals(this.isIdenticalTo, deposit.isIdenticalTo) &&
            Objects.equals(this.isObsoletedBy, deposit.isObsoletedBy) &&
            Objects.equals(this.isReferencedBy, deposit.isReferencedBy) &&
            Objects.equals(this.subjectAreas, deposit.subjectAreas) &&
            Objects.equals(this.additionalFieldsValues, deposit.additionalFieldsValues) &&
            Objects.equals(this.additionalFieldsFormId, deposit.additionalFieldsFormId) &&
            Objects.equals(this.isStatusDirty, deposit.isStatusDirty);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.access, this.dataSensitivity, this.dataUsePolicy, this.dataFileNumber,
            this.complianceLevel, this.collection, this.collectionBegin, this.collectionEnd, this.contributors, this.dataFiles, this.description,
            this.doi, this.ark, this.embargo, this.keywords, this.language, this.languageId, this.licenseId,
            this.archiveTypeId, this.archiveType, this.metadataVersion, this.organizationalUnit, this.organizationalUnitId, this.preparationId,
            this.preservationPolicy, this.preservationPolicyId, this.publicationDate, this.sip, this.sipId, this.status, this.statusMessage,
            this.submissionPolicy, this.submissionPolicyId, this.title, this.containsUpdatedMetadata, this.contentStructurePublic,
            this.isIdenticalTo, this.isObsoletedBy, this.isReferencedBy, this.subjectAreas, this.additionalFieldsValues,
            this.additionalFieldsFormId, this.isStatusDirty);
  }

  @Override
  public boolean checkDataFileToAdd(DataCategory dataCategory, DataCategory dataType) {
    // Deposit in creation
    if (this.getStatus() == DepositStatus.IN_PROGRESS) {
      return true;
    }
    // Deposit in metadata edition
    if (this.getStatus() == DepositStatus.EDITING_METADATA) {
      if (dataCategory == null || dataType == null) {
        return false;
      }
      switch (dataCategory) {
        case Internal -> {
          if (this.getMetadataVersion().getVersionNumber() < DLCMMetadataVersion.V4_0.getVersionNumber()) {
            return false;
          }
          return switch (dataType) {
            case ArchiveDataUseAgreement, ArchiveReadme, ArchiveThumbnail -> true;
            default -> false;
          };
        }
        case Package -> {
          return switch (dataType) {
            case UpdatedMetadata -> true;
            default -> false;
          };
        }
        default -> {
          return false;
        }
      }
    }
    return false;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositDataFile.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preingest;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.util.SearchCriteria;

import ch.dlcm.DLCMConstants;
import ch.dlcm.listener.DataFileListener;
import ch.dlcm.listener.HistoryListener;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.specification.DepositDataFileSearchSpecification;

@Entity
@EntityListeners({ HistoryListener.class, DataFileListener.class })
@Table(indexes = { @Index(name = "fileSearch", columnList = "relativeLocation, fileName, " + DLCMConstants.DB_PACKAGE_ID) })
public class DepositDataFile extends AbstractDataFile<Deposit, DepositDataFile> {

  @ElementCollection
  @CollectionTable(name = "depositDataFileChecksum", joinColumns = {
          @JoinColumn(name = DLCMConstants.DB_FILE_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = { DLCMConstants.DB_FILE_ID,
                  "checksumAlgo", "checksumType",
                  "checksumOrigin" }))
  private List<DataFileChecksum> checksums = new ArrayList<>();

  @NotNull
  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_PACKAGE_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private Deposit infoPackage;

  public DepositDataFile() {
    super();
  }

  public DepositDataFile(Deposit deposit) {
    super();
    this.infoPackage = deposit;
  }

  @Override
  public void addLinks(final WebMvcLinkBuilder linkBuilder, final boolean mainRes, final boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (this.getStatus() == DataFileStatus.PROCESSED || this.getStatus() == DataFileStatus.READY
            || this.getStatus() == DataFileStatus.IGNORED_FILE) {
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
    }
    if (this.getStatus() == DataFileStatus.IN_ERROR) {
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.RESUME).withRel(ActionName.RESUME));
    }
    if (this.getStatus() == DataFileStatus.IGNORED_FILE) {
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.VALIDATE).withRel(DLCMActionName.VALIDATE));
    }
    this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
  }

  @PreUpdate
  public void checkUpdate() {
    this.check();
  }

  @Override
  public List<DataFileChecksum> getChecksums() {
    return this.checksums;
  }

  @Override
  public URI getDownloadUri() {
    if (this.getStatus() != DataFileStatus.PROCESSED && this.getStatus() != DataFileStatus.READY
            && this.getStatus() != DataFileStatus.IGNORED_FILE) {
      throw new SolidifyRuntimeException("Data file not available");
    }
    if (this.getFinalData() == null) {
      throw new SolidifyRuntimeException("Data file not accessible");
    }
    return this.getFinalData();
  }

  @Override
  public String getContentType() {
    if (this.getStatus() != DataFileStatus.PROCESSED && this.getStatus() != DataFileStatus.READY
            && this.getStatus() != DataFileStatus.IGNORED_FILE) {
      throw new SolidifyRuntimeException("Data file not available");
    }
    if (this.getFileFormat() == null) {
      throw new SolidifyRuntimeException("Data file format not accessible");
    }
    return this.getFileFormat().getContentType();
  }

  @Override
  public String managedBy() {
    return ModuleName.PREINGEST;
  }

  public void setChecksums(List<DataFileChecksum> checksums) {
    this.checksums = checksums;
  }

  @Override
  public Deposit getInfoPackage() {
    return this.infoPackage;
  }

  @Override
  public void setInfoPackage(Deposit infoPackage) {
    this.infoPackage = infoPackage;
  }

  @Override
  public Specification<DepositDataFile> getSearchSpecification(SearchCriteria criteria) {
    return new DepositDataFileSearchSpecification(criteria);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    DepositDataFile that = (DepositDataFile) o;
    return Objects.equals(this.checksums, that.checksums) && Objects.equals(this.infoPackage, that.infoPackage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.checksums, this.infoPackage);
  }
}

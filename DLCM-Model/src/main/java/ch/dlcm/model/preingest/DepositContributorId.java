/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositContributorId.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preingest;

import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.dlcm.DLCMConstants;

@Embeddable
public class DepositContributorId implements Serializable {
  @Serial
  private static final long serialVersionUID = -2843643368591256483L;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_DEPOSIT_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private Deposit deposit;

  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String personId;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof DepositContributorId)) {
      return false;
    }
    final DepositContributorId that = (DepositContributorId) o;
    return Objects.equals(this.getDeposit(), that.getDeposit()) &&
            Objects.equals(this.getPersonId(), that.getPersonId());
  }

  @JsonIgnore
  public Deposit getDeposit() {
    return this.deposit;
  }

  @JsonIgnore
  public String getPersonId() {
    return this.personId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getDeposit(), this.getPersonId());
  }

  @JsonIgnore
  public void setDeposit(Deposit deposit) {
    this.deposit = deposit;
  }

  @JsonIgnore
  public void setPersonId(String personId) {
    this.personId = personId;
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName() + ": " + DLCMConstants.DB_DEPOSIT_ID + "=" + this.getDeposit().getResId() + " "
            + DLCMConstants.DB_PERSON_ID + "=" + this.getPersonId();
  }
}

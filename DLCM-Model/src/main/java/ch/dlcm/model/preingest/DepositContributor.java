/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositContributor.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preingest;

import java.util.Objects;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import ch.unige.solidify.rest.JoinResource;

@Entity
@Table(name = "depositContributors")
public class DepositContributor extends JoinResource<DepositContributorId> {

  @EmbeddedId
  @JsonUnwrapped
  private DepositContributorId compositeResId;

  private Integer position;

  public DepositContributor() {
    this.compositeResId = new DepositContributorId();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof DepositContributor)) {
      return false;
    }
    final DepositContributor that = (DepositContributor) o;
    return Objects.equals(this.getDeposit(), that.getDeposit()) && Objects.equals(this.getPersonId(), that.getPersonId());
  }

  @Override
  public DepositContributorId getCompositeKey() {
    return this.compositeResId;
  }

  @Transient
  @JsonIgnore
  public Deposit getDeposit() {
    return this.compositeResId.getDeposit();
  }

  @Transient
  @JsonIgnore
  public String getPersonId() {
    return this.compositeResId.getPersonId();
  }

  public Integer getPosition() {
    return this.position;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getDeposit(), this.getPersonId());
  }

  @PrePersist
  @PreUpdate
  public void init() {
    if (this.position == null) {
      this.position = 0;
    }
  }

  @Override
  public void setCompositeKey(DepositContributorId compositeResId) {
    this.compositeResId = compositeResId;
  }

  @Transient
  @JsonIgnore
  public void setDeposit(Deposit deposit) {
    this.compositeResId.setDeposit(deposit);
  }

  @Transient
  @JsonIgnore
  public void setPersonId(String personId) {
    this.compositeResId.setPersonId(personId);
  }

  public void setPosition(Integer position) {
    this.position = position;
  }

  @Override
  public String toString() {
    return super.toString() + " (position = " + this.getPosition() + ")";
  }
}

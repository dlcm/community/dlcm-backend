/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - Contributor.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preingest;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.rest.NoSqlResource;

import ch.dlcm.model.settings.Person;
import ch.dlcm.model.settings.PersonAvatar;
import ch.dlcm.rest.ResourceName;

@Schema(description = "Contributors are people who are involved in archives.")
public class Contributor extends NoSqlResource {

  @Schema(description = "The deposit number of the contributor.")
  private Integer depositNumber;

  @Schema(description = "The linked person of the contributor.")
  @JsonIgnore
  private Person person = new Person();

  public Contributor() {
  }

  public Contributor(Person person, Integer depositNumber) {
    this.setPerson(person);
    this.setResId(person.getResId());
    this.setDepositNumber(depositNumber);
  }

  public Contributor(String resId) {
    this.setResId(resId);
    this.person.setResId(resId);
  }

  public Integer getDepositNumber() {
    return this.depositNumber;
  }

  public void setDepositNumber(Integer depositNumber) {
    this.depositNumber = depositNumber;
  }

  public Person getPerson() {
    return this.person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  @Override
  public String getResId() {
    return this.person.getResId();
  }

  @Override
  public void setResId(String resId) {
    this.person.setResId(resId);
    super.setResId(resId);
  }

  public String getFirstName() {
    return this.person.getFirstName();
  }

  public void setFirstName(String firstName) {
    this.person.setFirstName(firstName);
  }

  public String getLastName() {
    return this.person.getLastName();
  }

  public void setLastName(String lastName) {
    this.person.setLastName(lastName);
  }

  public String getFullName() {
    return this.person.getFullName();
  }

  public String getOrcid() {
    return this.person.getOrcid();
  }

  public void setOrcid(String orcid) {
    this.person.setOrcid(orcid);
  }

  public boolean isVerifiedOrcid() {
    return this.person.isVerifiedOrcid();
  }

  public void setVerifiedOrcid(Boolean verifiedOrcid) {
    this.person.setVerifiedOrcid(verifiedOrcid);
  }

  public ChangeInfo getCreation() {
    return this.person.getCreation();
  }

  public ChangeInfo getLastUpdate() {
    return this.person.getLastUpdate();
  }

  public PersonAvatar getAvatar() {
    return this.person.getAvatar();
  }

  public void setAvatar(PersonAvatar avatar) {
    this.person.setAvatar(avatar);
  }

  @Override
  public boolean update(NoSqlResource item) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder) {
    super.addLinks(linkBuilder);
    this.add(linkBuilder.slash(this.person.getResId()).slash(ResourceName.DEPOSIT).withRel(ResourceName.DEPOSIT));
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("Contributor: firstName=").append(this.getFirstName())
            .append(", lastName=").append(this.getLastName())
            .append(", orcid=").append(this.getOrcid())
            .append(", depositNumber=").append(this.getDepositNumber());
    return builder.toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj)) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final Contributor other = (Contributor) obj;
    if (this.depositNumber == null) {
      if (other.depositNumber != null) {
        return false;
      }
    } else if (!this.depositNumber.equals(other.depositNumber)) {
      return false;
    }
    if (this.person == null) {
      return other.person == null;
    } else
      return this.person.equals(other.person);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.depositNumber == null) ? 0 : this.depositNumber.hashCode());
    result = prime * result + ((this.person == null) ? 0 : this.person.hashCode());
    return result;
  }
}

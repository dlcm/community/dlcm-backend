/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AnonymizedDeposit.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preingest;

import java.time.OffsetDateTime;
import java.util.Objects;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;

import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.DLCMConstants;
import ch.dlcm.rest.ModuleName;

@Entity
public class AnonymizedDeposit extends ResourceNormalized {

  @Schema(description = "The corresponding non anonymized deposit")
  @NotNull
  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_PACKAGE_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private Deposit deposit;

  @Schema(description = "The timestamp when the zip file of the anonymized deposit has been created")
  @NotNull
  private OffsetDateTime timestamp;

  @Schema(description = "The checksum of the checksums of all data files")
  @NotNull
  private String aggregateChecksum;

  @Schema(description = "The id of the page listing all anonymized deposits of a given deposit")
  @NotNull
  private String pageId;

  public Deposit getDeposit() {
    return deposit;
  }

  public OffsetDateTime getTimestamp() {
    return timestamp;
  }

  public String getAggregateChecksum() {
    return aggregateChecksum;
  }

  public String getPageId() {
    return pageId;
  }

  public void setDeposit(Deposit deposit) {
    this.deposit = deposit;
  }

  public void setTimestamp(OffsetDateTime timestamp) {
    this.timestamp = timestamp;
  }

  public void setAggregateChecksum(String aggregateChecksum) {
    this.aggregateChecksum = aggregateChecksum;
  }

  public void setPageId(String linkId) {
    this.pageId = linkId;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.PREINGEST;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    AnonymizedDeposit that = (AnonymizedDeposit) o;
    return Objects.equals(deposit, that.deposit) && Objects.equals(timestamp, that.timestamp) && Objects.equals(
            aggregateChecksum, that.aggregateChecksum) && Objects.equals(pageId, that.pageId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), deposit, timestamp, aggregateChecksum, pageId);
  }
}

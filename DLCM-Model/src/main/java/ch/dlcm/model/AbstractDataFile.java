/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AbstractDataFile.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.data.jpa.domain.Specification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Transient;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.SearchableResourceNormalized;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.oais.MetadataVersionAware;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.settings.MetadataType;

@MappedSuperclass
public abstract class AbstractDataFile<T extends Resource & MetadataVersionAware, V> extends SearchableResourceNormalized<V> {

  private static final String MISSING_DATA_CATEGORY = "Missing data category";
  private static final String MISSING_DATA_TYPE = "Missing data type";

  private static final int DB_SOURCE_DATA_LENGTH = 2024;
  private static final int DB_FINAL_DATA_LENGTH = 2024;
  private static final int DB_RELATIVE_LOCATION_LENGTH = 460;

  //@formatter:off
  public enum DataFileStatus {
    // Special processing status
    CHANGE_RELATIVE_LOCATION, CHANGE_DATA_CATEGORY, CLEANED, CLEANING, EXCLUDED_FILE,
    CHECK_COMPLIANCE, CHECK_COMPLIANCE_CLEANED, CHECKED_COMPLIANCE, CHECKED_COMPLIANCE_CLEANED, IGNORED_FILE,
    // File format identification status
    FILE_FORMAT_IDENTIFIED, FILE_FORMAT_SKIPPED, FILE_FORMAT_UNKNOWN,
    IN_ERROR, PROCESSED,
    // Normal processing status
    READY, RECEIVED, TO_PROCESS,
    // Virus check status
    VIRUS_CHECKED, VIRUS_SKIPPED
  }
  //@formatter:on
  private static final String DATA_FILE_PREFIX = "{Data File} ";

  private ComplianceLevel complianceLevel;

  @JsonProperty(required = true)
  @Enumerated(EnumType.STRING)
  private DataCategory dataCategory;

  @JsonProperty(required = true)
  @Enumerated(EnumType.STRING)
  private DataCategory dataType;

  @Embedded
  @Valid
  private FileFormat fileFormat = new FileFormat();

  private Long fileSize;

  @Column(length = DB_FINAL_DATA_LENGTH)
  private URI finalData;

  @NotNull
  private String fileName;

  @Transient
  private MetadataType metadataType;

  @JsonIgnore
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String metadataTypeId;

  @Size(max = DB_RELATIVE_LOCATION_LENGTH)
  @Pattern(regexp = "(^\\/$)|(^\\/.*(?<!\\/)$)")
  private String relativeLocation;

  @NotNull
  @Column(length = DB_SOURCE_DATA_LENGTH)
  private URI sourceData;

  @Enumerated(EnumType.STRING)
  private DataFileStatus status;

  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String statusMessage;

  @Embedded
  @Valid
  private VirusCheck virusCheck = new VirusCheck();

  /********************************************************************************************************************/

  /**
   * Throws an IllegalArgumentException if data category and data type are not correct
   */
  public void check() {
    // Data category & type consistency
    if (!DataCategory.check(this.getDataCategory(), this.getDataType())) {
      throw new IllegalArgumentException("Wrong data category: " + this.getDataCategory() + "/" + this.getDataType());
    }
    // Custom metadata check
    if (this.getDataCategory().equals(DataCategory.Package) && this.getDataType().equals(DataCategory.CustomMetadata)
            && this.getMetadataTypeId() == null
            && (this.getMetadataType() == null || this.getMetadataType().getResId() == null)) {
      throw new IllegalArgumentException("Missing metadata type for " + this.getDataCategory() + "/" + this.getDataType());
    }
  }

  /**
   * @return the list of data file checksums
   */
  public abstract List<DataFileChecksum> getChecksums();

  /**
   * Return true if the data file is in-progress
   */
  public boolean isInProgress() {
    return (this.getStatus() != DataFileStatus.IN_ERROR &&
            this.getStatus() != DataFileStatus.READY &&
            this.getStatus() != DataFileStatus.CLEANED &&
            this.getStatus() != DataFileStatus.EXCLUDED_FILE &&
            this.getStatus() != DataFileStatus.IGNORED_FILE &&
            this.getStatus() != DataFileStatus.CHECKED_COMPLIANCE_CLEANED &&
            this.getStatus() != DataFileStatus.CHECKED_COMPLIANCE);
  }

  /**
   * @param status the status of the data file
   */
  public void setFileStatus(DataFileStatus status) {
    this.setStatus(status);
    this.setStatusMessage(null);
  }

  public abstract T getInfoPackage();

  public abstract void setInfoPackage(T infoPackage);

  public boolean checkSource() {
    final String[] schemes = { "http", "https", "file" };
    final UrlValidator urlValidator = new UrlValidator(schemes, UrlValidator.ALLOW_LOCAL_URLS);
    return urlValidator.isValid(this.sourceData.toString());
  }

  public Path defineTargetFile(String preparationPath) {
    String target = preparationPath;
    // Deposit metadata edition
    if (this.getInfoPackage() instanceof Deposit deposit
            && deposit.getStatus().equals(Deposit.DepositStatus.EDITING_METADATA)) {
      if (DataCategory.Internal.equals(this.dataCategory)) {
        target += Package.UPDATE.getName() + Package.INTERNAL.getName() + "/" + this.defineMetadataFileTargetName();
      } else if (DataCategory.Package.equals(this.dataCategory)
              && DataCategory.UpdatedMetadata.equals(this.dataType)) {
        target += Package.UPDATE.getName() + "/" + this.defineTargetFileName(this.getFileName());
      } else {
        throw new SolidifyRuntimeException(this.dataCategory + "/" + this.dataType + " is not supported in metadata edition");
      }
      deposit.setContainsUpdatedMetadata(true);
    } else {
      target += this.defineTargetFolderName() + "/" + this.defineTargetFileName(this.getFileName());
    }
    final Path targetPath = Paths.get(target);
    if (FileTool.ensureFolderExists(targetPath.getParent())) {
      return targetPath;
    } else {
      throw new SolidifyRuntimeException(DATA_FILE_PREFIX + " Impossible to create directory : " + targetPath.getParent());
    }
  }

  @JsonIgnore
  public String getInitialPath() {
    if (this.dataCategory == null) {
      throw new SolidifyCheckingException(MISSING_DATA_CATEGORY);
    }
    return switch (this.dataCategory) {
      case Package -> switch (this.dataType) {
        case CustomMetadata -> Package.METADATA.getName();
        case UpdatedMetadata -> Package.UPDATE.getName();
        default -> Package.ROOT.getName();
      };
      case Primary -> Package.RESEARCH.getName();
      case Secondary -> Package.DOC.getName();
      case Software -> Package.SOFTWARE.getName();
      case Administrative -> Package.ADMINISTRATION.getName();
      case Internal -> Package.INTERNAL.getName();
      default -> throw new SolidifyRuntimeException(this.dataCategory + " is not a data category");
    };
  }

  public String defineTargetFolderName() {
    if (this.dataCategory == null) {
      throw new SolidifyCheckingException(MISSING_DATA_CATEGORY);
    }
    switch (this.dataCategory) {
      case Primary, Secondary, Software, Administrative -> {
        return this.getInitialPath() + this.getExtendedRelativeLocation();
      }
      case Internal -> {
        if (DataCategory.DatafileThumbnail.equals(this.dataCategory)) {
          return this.getInitialPath() + this.getExtendedRelativeLocation();
        }
        return this.getInitialPath();
      }
      default -> {
        return this.getInitialPath();
      }
    }
  }

  public String defineTargetFileName(String sourceFileName) {
    if (this.dataCategory == null) {
      throw new SolidifyCheckingException(MISSING_DATA_CATEGORY);
    }
    if (this.dataType == null) {
      throw new SolidifyCheckingException(MISSING_DATA_TYPE);
    }
    switch (this.dataCategory) {
      case Package -> {
        return switch (this.dataType) {
          case CustomMetadata, InformationPackage -> sourceFileName;
          case Metadata -> Package.METADATA_FILE.getName();
          case UpdatedMetadata -> DLCMConstants.UPDATED_METADATA_FILE_PREFIX + this.getNowDateForFile() + SolidifyConstants.XML_EXT;
          case UpdatePackage -> DLCMConstants.UPDATE_PACKAGE_FILE + "-" + this.getNowDateForFile();
          default -> throw new SolidifyRuntimeException(this.dataType + " is not a data type for the Package data category");
        };
      }
      case Primary, Secondary, Software, Administrative -> {
        return sourceFileName;
      }
      case Internal -> {
        return switch (this.dataType) {
          case DatasetThumbnail -> Package.DATASET_THUMBNAIL.getName();
          case ArchiveThumbnail -> Package.ARCHIVE_THUMBNAIL.getName();
          case ArchiveDataUseAgreement -> Package.ARCHIVE_DUA.getName();
          case ArchiveReadme -> Package.ARCHIVE_README.getName();
          case DatafileThumbnail -> sourceFileName + DLCMConstants.THUMBNAIL_EXTENSION;
          default -> throw new SolidifyRuntimeException(this.dataType + " is not a data type for the Internal data category");
        };
      }
      default -> throw new SolidifyRuntimeException(this.dataCategory + " is not a data category");
    }
  }

  private String defineMetadataFileTargetName() {
    if (this.dataType == null) {
      throw new SolidifyCheckingException(MISSING_DATA_TYPE);
    }
    return switch (this.dataType) {
      case ArchiveThumbnail -> DLCMConstants.DLCM_ARCHIVE + "-" + this.getNowDateForFile() + DLCMConstants.THUMBNAIL_EXTENSION;
      case ArchiveReadme -> DLCMConstants.DLCM_ARCHIVE + "-" + this.getNowDateForFile() + DLCMConstants.README_EXTENSION;
      case ArchiveDataUseAgreement -> DLCMConstants.DLCM_ARCHIVE + "-" + this.getNowDateForFile() + DLCMConstants.DUA_EXTENSION;
      default -> throw new SolidifyRuntimeException(this.dataType + " is not a data type for the update package");
    };
  }

  private String getNowDateForFile() {
    return OffsetDateTime.now().format(DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT_FOR_FILE));
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof AbstractDataFile)) {
      return false;
    }

    final AbstractDataFile<?, ?> castOther = (AbstractDataFile<?, ?>) other;
    return super.equals(other)
            && Objects.equals(this.getSourceData(), castOther.getSourceData())
            && Objects.equals(this.getRelativeLocation(), castOther.getRelativeLocation())
            && Objects.equals(this.getFinalData(), castOther.getFinalData())
            && Objects.equals(this.getFileFormat(), castOther.getFileFormat())
            && Objects.equals(this.getVirusCheck(), castOther.getVirusCheck())
            && Objects.equals(this.getFileSize(), castOther.getFileSize())
            && Objects.equals(this.getStatus(), castOther.getStatus())
            && Objects.equals(this.getDataCategory(), castOther.getDataCategory())
            && Objects.equals(this.getDataType(), castOther.getDataType())
            && Objects.equals(this.getComplianceLevel(), castOther.getComplianceLevel())
            && Objects.equals(this.getInfoPackage(), castOther.getInfoPackage())
            && Objects.equals(this.getChecksums(), castOther.getChecksums());
  }

  public ComplianceLevel getComplianceLevel() {
    return this.complianceLevel;
  }

  public DataCategory getDataCategory() {
    return this.dataCategory;
  }

  public DataCategory getDataType() {
    return this.dataType;
  }

  /*
   * ************************************** Getters and Setters **************************************
   */

  public FileFormat getFileFormat() {
    return this.fileFormat;
  }

  @JsonProperty(access = Access.READ_ONLY)
  public String getFileName() {
    return this.fileName;
  }

  public Long getFileSize() {
    return this.fileSize;
  }

  public URI getFinalData() {
    return this.finalData;
  }

  @JsonProperty(access = Access.READ_ONLY)
  public String getFullFileName() {
    return this.getExtendedRelativeLocation() + "/" + this.getFileName();
  }

  public MetadataType getMetadataType() {
    return this.metadataType;
  }

  public String getMetadataTypeId() {
    return this.metadataTypeId;
  }

  @JsonIgnore
  public Path getPath() {
    return Paths.get(this.getFinalData());
  }

  public String getRelativeLocation() {
    return this.relativeLocation;
  }

  @JsonProperty(access = Access.READ_ONLY)
  public String getSmartSize() {
    if (this.fileSize == null) {
      return DLCMConstants.UNKNOWN;
    }
    return StringTool.formatSmartSize(this.fileSize);
  }

  public URI getSourceData() {
    return this.sourceData;
  }

  public DataFileStatus getStatus() {
    return this.status;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public VirusCheck getVirusCheck() {
    return this.virusCheck;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + (this.getSourceData() != null ? this.getSourceData().hashCode() : 0);
    result = 31 * result + (this.getRelativeLocation() != null ? this.getRelativeLocation().hashCode() : 0);
    result = 31 * result + (this.getFinalData() != null ? this.getFinalData().hashCode() : 0);
    result = 31 * result + (this.getFileFormat() != null ? this.getFileFormat().hashCode() : 0);
    result = 31 * result + (this.getVirusCheck() != null ? this.getVirusCheck().hashCode() : 0);
    result = 31 * result + (this.getFileSize().hashCode());
    result = 31 * result + (this.getStatus() != null ? this.getStatus().hashCode() : 0);
    result = 31 * result + (this.getDataCategory() != null ? this.getDataCategory().hashCode() : 0);
    result = 31 * result + (this.getDataType() != null ? this.getDataType().hashCode() : 0);
    result = 31 * result + (this.getComplianceLevel() != null ? this.getComplianceLevel().hashCode() : 0);
    result = 31 * result + (this.getInfoPackage() != null ? this.getInfoPackage().hashCode() : 0);
    result = 31 * result + (this.getChecksums() != null ? this.getChecksums().hashCode() : 0);
    return result;
  }

  @Override
  @PrePersist
  public void init() {
    if (this.status == null) {
      this.status = DataFileStatus.RECEIVED;
    }
    if (this.relativeLocation == null) {
      this.relativeLocation = "/";
    } else {
      this.setRelativeLocation(AbstractDataFile.checkRelativeLocation(this.getRelativeLocation()));
    }
    if (this.dataCategory == null) {
      this.dataCategory = DataCategory.Primary;
    }
    if (this.dataType == null) {
      this.dataType = DataCategory.Reference;
    }
    if (this.fileFormat == null) {
      this.fileFormat = new FileFormat();
    }
    if (this.fileSize == null) {
      this.fileSize = 0L;
    }
    if (this.complianceLevel == null) {
      this.complianceLevel = ComplianceLevel.NOT_ASSESSED;
    }

    this.check();
  }

  @JsonIgnore
  public boolean isAvailable() {
    return this.getStatus() == DataFileStatus.PROCESSED || this.getStatus() == DataFileStatus.READY;
  }

  @JsonIgnore
  public boolean isInRoot() {
    return (StringTool.isNullOrEmpty(this.getRelativeLocation()) || this.getRelativeLocation().equals("/"));
  }

  public void setComplianceLevel(ComplianceLevel complianceLevel) {
    this.complianceLevel = complianceLevel;
  }

  public void setDataCategory(DataCategory dataCat) {
    this.dataCategory = dataCat;
  }

  public void setDataType(DataCategory dataType) {
    this.dataType = dataType;
  }

  public void setFileFormat(FileFormat fileFormat) {
    this.fileFormat = fileFormat;
  }

  public void setFileSize(Long size) {
    this.fileSize = size;
  }

  @SuppressWarnings("squid:S2637")
  public void setFinalData(URI finalData) {
    this.finalData = this.replacePlusCharacterInFileUri(finalData);
    this.fileName = this.computeFileName();
  }

  public void setMetadataType(MetadataType metadataType) {
    this.metadataType = metadataType;
    if (this.metadataType != null && this.metadataType.getResId() != null) {
      this.setMetadataTypeId(this.metadataType.getResId());
    }
  }

  public void setPath(Path file) {
    // Do nothing
  }

  public void setRelativeLocation(String relativeLocation) {
    this.relativeLocation = relativeLocation;
  }

  @SuppressWarnings("squid:S2637")
  public void setSourceData(URI sourceData) {
    this.sourceData = this.replacePlusCharacterInFileUri(sourceData);
    this.fileName = this.computeFileName();
  }

  /*
   * ************************************* Functional methods
   **************************************/

  public void setStatus(DataFileStatus status) {
    this.status = status;
  }

  public void setStatusMessage(String message) {
    this.statusMessage = StringTool.truncateWithEllipsis(message, SolidifyConstants.DB_LONG_STRING_LENGTH);
  }

  public void setStatusWithMessage(DataFileStatus status, String message) {
    this.setStatus(status);
    this.setStatusMessage(message);
  }

  public void setVirusCheck(VirusCheck virusCheck) {
    this.virusCheck = virusCheck;
  }

  public static String checkRelativeLocation(String relativeLocation) {
    if (StringTool.isNullOrEmpty(relativeLocation)) {
      return "/";
    }
    // Check if root
    if (!relativeLocation.equals("/")) {
      // Replace multiple /
      relativeLocation = relativeLocation.replaceAll("/+", "/");

      // Check starting / & add it if needed
      if (!relativeLocation.startsWith("/")) {
        relativeLocation = "/" + relativeLocation;
      }
      // Check 2nd starting / & remove it if needed
      while (relativeLocation.indexOf('/', 1) == 1) {
        relativeLocation = "/" + relativeLocation.substring(2, relativeLocation.length());
      }

      // Check ending / & remove it if needed
      while (relativeLocation.endsWith("/")) {
        relativeLocation = relativeLocation.substring(0, relativeLocation.length() - 1);
      }

      // Don't allow '..' in path
      final Path relativeLocationPath = Paths.get(relativeLocation);
      if (IntStream.range(0, relativeLocationPath.getNameCount())
                .mapToObj(relativeLocationPath::getName)
                .map(Path::toString)
                .anyMatch(".."::equals)) {

        throw new IllegalArgumentException("Relative location should not contains '..'");
      }
    }
    return relativeLocation;
  }

  private String getExtendedRelativeLocation() {
    if (this.isInRoot()) {
      return "";
    }
    return this.getRelativeLocation();
  }

  private void setMetadataTypeId(String metadataTypeId) {
    this.metadataTypeId = metadataTypeId;
  }

  private String computeFileName() {
    URI fileURI = this.finalData;
    if (fileURI == null) {
      fileURI = this.sourceData;
    }

    if (fileURI != null) {
      final int lastIndexOfSlash = fileURI.toString().lastIndexOf('/');
      return StringTool.decodeUrl(fileURI.toString().substring(lastIndexOfSlash + 1));
    }
    return null;
  }

  @Override
  public Specification<V> getSearchSpecification(SearchCriteria criteria) {
    throw new UnsupportedOperationException("Searchable not implemented for datafile of type " + this.getClass().getSimpleName());
  }

  private URI replacePlusCharacterInFileUri(URI uri) {
    if (uri == null) {
      return null;
    }
    if (uri.toString().startsWith("file://")) {
      try {
        return new URI(uri.toString().replace("+", DLCMConstants.PERCENT_ENCODING_PLUS_CHAR));
      } catch (URISyntaxException e) {
        throw new IllegalStateException("Error in file URI treatment");
      }
    } else {
      return uri;
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - FileFormat.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import java.io.StringReader;
import java.util.Objects;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Embedded;
import jakarta.persistence.Lob;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.StringTool;

@Schema(description = "File format informations.")
@Embeddable
public class FileFormat implements ToolContainer {

  private static final int DB_MD5_LENGTH = 256;
  private static final int DB_VERSION_LENGTH = 50;
  private static final int DB_PUID_LENGTH = 20;

  @Schema(description = "The content type of the file.")
  @Size(max = SolidifyConstants.DB_MEDIUM_STRING_LENGTH)
  @Column(length = SolidifyConstants.DB_MEDIUM_STRING_LENGTH)
  private String contentType;

  @Schema(description = "The result details of the file format identification.")
  @Lob
  @Column(name = "formatDetails", length = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String details;

  @Schema(description = "The format description of the file.")
  private String format;

  @Schema(description = "The MD5 checksum of the file.")
  @Size(max = DB_MD5_LENGTH)
  @Column(length = DB_MD5_LENGTH)
  private String md5;

  @Schema(description = "The format PRONOM identifier of the  file.")
  @Size(max = DB_PUID_LENGTH)
  @Column(length = DB_PUID_LENGTH)
  private String puid;

  @Schema(description = "The format identification tool used to get the file format.")
  @Embedded
  @Valid
  @AttributeOverride(name = "name", column = @Column(name = "formatToolName"))
  @AttributeOverride(name = "description", column = @Column(name = "formatToolDescription"))
  @AttributeOverride(name = "version", column = @Column(name = "formatToolVersion"))
  @AttributeOverride(name = "puid", column = @Column(name = "formatToolPuid"))
  private Tool tool;

  @Schema(description = "The format version of the file.")
  @Size(max = DB_VERSION_LENGTH)
  @Column(name = "formatVersion", length = DB_VERSION_LENGTH)
  private String version;

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof FileFormat)) {
      return false;
    }
    final FileFormat castOther = (FileFormat) other;
    return Objects.equals(this.getContentType(), castOther.getContentType()) && Objects.equals(this.getFormat(), castOther.getFormat())
            && Objects
                    .equals(this.getVersion(), castOther.getVersion())
            && Objects.equals(this.getPuid(), castOther.getPuid()) && Objects
                    .equals(this.getMd5(), castOther.getMd5())
            && Objects.equals(this.getDetails(), castOther.getDetails()) && Objects
                    .equals(this.getTool(), castOther.getTool());
  }

  public String getContentType() {
    return this.contentType;
  }

  public String getDetails() {
    return this.details;
  }

  public String getFormat() {
    return this.format;
  }

  public String getMd5() {
    return this.md5;
  }

  public String getPuid() {
    return this.puid;
  }

  @JsonIgnore
  public Object getSmartDetails(JAXBContext jaxbContext) throws JAXBException {
    final Unmarshaller um = jaxbContext.createUnmarshaller();
    return um.unmarshal(new StringReader(this.getDetails()));
  }

  @Override
  public Tool getTool() {
    return this.tool;
  }

  public String getVersion() {
    return this.version;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getContentType(), this.getFormat(), this.getVersion(), this.getPuid(), this.getMd5(), this.getDetails(),
            this.getTool());
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public void setDetails(String details) {
    if (details != null) {
      this.details = details;
    }
  }

  public void setFormat(String format) {
    if (format != null && format.length() > SolidifyConstants.DB_DEFAULT_STRING_LENGTH) {
      this.format = StringTool.truncateWithEllipsis(format, SolidifyConstants.DB_DEFAULT_STRING_LENGTH);
    } else {
      this.format = format;
    }
  }

  public void setMd5(String md5) {
    this.md5 = md5;
  }

  public void setPuid(String puid) {
    this.puid = puid;
  }

  @Override
  public void setTool(Tool tool) {
    this.tool = tool;
  }

  public void setVersion(String version) {
    this.version = version;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - SubmissionAgreementUserDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.display;

import java.util.Objects;

import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.ResourceBase;
import ch.unige.solidify.util.ReflectionTool;

import ch.dlcm.model.policies.SubmissionAgreementUser;
import ch.dlcm.model.security.User;

public class SubmissionAgreementUserDTO extends User implements JoinResourceContainer<SubmissionAgreementUser> {

  private final SubmissionAgreementUser joinResource;


  public SubmissionAgreementUserDTO(SubmissionAgreementUser submissionAgreementUser) {
    ReflectionTool.copyFields(this, submissionAgreementUser.getUser(), ResourceBase.class);
    this.add(submissionAgreementUser.getUser().getLinks());
    this.joinResource = submissionAgreementUser;
  }

  @Override
  public SubmissionAgreementUser getJoinResource() {
    return this.joinResource;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    SubmissionAgreementUserDTO that = (SubmissionAgreementUserDTO) o;
    return Objects.equals(joinResource, that.joinResource);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), joinResource);
  }
}

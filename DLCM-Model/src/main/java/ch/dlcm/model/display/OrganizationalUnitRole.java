/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrganizationalUnitRole.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.display;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.ResourceName;

public class OrganizationalUnitRole extends OrganizationalUnit implements ExtendedResourceInterface<Role> {

  @Transient
  List<Role> roles = new ArrayList<>();

  public OrganizationalUnitRole() {
  }

  public OrganizationalUnitRole(OrganizationalUnit organizationalUnit) {
    this.mapResourceToExtendedResource(this, organizationalUnit);
  }

  @Override
  @JsonIgnore
  public Role getItem() {
    return null;
  }

  @Override
  @JsonProperty(ResourceName.ROLE)
  public List<Role> getItems() {
    return this.roles;
  }

  @Override
  @JsonIgnore
  public void setItem(Role item) {
  }

  @Override
  public void setItems(List<Role> roles) {
    this.roles = roles;
  }

}

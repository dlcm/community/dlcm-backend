/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ExtendedResourceInterface.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.display;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import ch.unige.solidify.util.ReflectionTool;

public interface ExtendedResourceInterface<V> {
  void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly);

  V getItem();

  List<V> getItems();

  String getResId();

  default <T, V> void mapResourceToExtendedResource(T extendedResource, V resource) {
    final Class<?>[] classes = { resource.getClass(), resource.getClass().getSuperclass(), resource.getClass().getSuperclass().getSuperclass() };
    for (final Class<?> clz : classes) {
      final Field[] fields = clz.getDeclaredFields();
      for (final Field fieldToCheck : fields) {

        fieldToCheck.setAccessible(true);

        try {

          final Field extendedResourceField = ReflectionTool.getField(extendedResource, fieldToCheck.getName());
          if (!Modifier.isFinal(extendedResourceField.getModifiers())) {
            final Object fieldValue = fieldToCheck.get(resource);
            fieldToCheck.set(extendedResource, fieldValue);
          }

        } catch (final IllegalAccessException e) {
          LoggerFactory.getLogger(ExtendedResourceInterface.class)
                  .error("Could not convert " + resource.getClass().getName() + " to " + extendedResource.getClass().getName(), e);
        } catch (final NoSuchFieldException e) {
          LoggerFactory.getLogger(ExtendedResourceInterface.class).error("Field " + fieldToCheck.getName() + " not found", e);
        }

        fieldToCheck.setAccessible(false);
      }
    }
  }

  void setItem(V item);

  void setItems(List<V> items);
}

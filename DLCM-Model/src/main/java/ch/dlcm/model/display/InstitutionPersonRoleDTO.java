/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - InstitutionPersonRoleDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.display;

import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.ResourceBase;
import ch.unige.solidify.util.ReflectionTool;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.InstitutionPersonRole;

public class InstitutionPersonRoleDTO extends Role implements JoinResourceContainer<InstitutionPersonRole> {

  InstitutionPersonRole joinResource;

  public InstitutionPersonRoleDTO() {
  }

  public InstitutionPersonRoleDTO(InstitutionPersonRole institutionPersonRole) {
    ReflectionTool.copyFields(this, institutionPersonRole.getRole(), ResourceBase.class);
    this.add(institutionPersonRole.getRole().getLinks());
    this.joinResource = institutionPersonRole;
  }

  @Override
  public InstitutionPersonRole getJoinResource() {
    return this.joinResource;
  }
}

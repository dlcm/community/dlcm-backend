/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - PackageStatus.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        OAIS Package Status:
        - CHECKED => Checked package during archiving process
        - CHECKING => A package verification is in progress during checking process
        - CHECK_PENDING => A package verification is pending during checking process
        - CLEANED => Cleaned package during cleaning process for SIP only
        - CLEANING => A package clean is in progress during cleaning process for SIP only
        - COMPLIANCE_LEVEL_UPDATE_PENDING => A package compliance update is pending
        - COMPLIANCE_LEVEL_UPDATED => The compliance levels of the package have been updated
        - COMPLETED => Completed package
        - DISPOSABLE => The Package is candidate for disposal process for AIP only
        - DISPOSAL_APPROVED_BY_ORGUNIT => Disposal org. unit approval done during disposal process for AIP only
        - DISPOSAL_APPROVED => Disposal approval done during disposal process for AIP only
        - DISPOSED => Disposed package for AIP only
        - DOWNLOADING => A package download is in progress
        - EDITING_METADATA => A package metadata edition is in progress
        - FIXING => A package correction is in progress
        - FIXITY_ERROR => Error when checking checksums
        - FIX_PENDING => A package correction is pending
        - INDEXING => A package indexing is in progress
        - IN_ERROR => Package in error during archiving process
        - IN_PREPARATION => Package in preparation during archiving process
        - IN_PROGRESS => A package archiving process is in progress
        - METADATA_EDITION_PENDING => A metadata edition is pending
        - METADATA_UPGRADE_PENDING => A metadata version upgrade is pending
        - PACKAGE_REPLICATION_PENDING => A package replication is pending
        - PRESERVATION_ERROR => Package in error during checking process
        - READY => Package Ready
        - REINDEXING => A package re-indexing is in progress
        - RELOADED => Reloaded package from storage location
        - REPLICATING_PACKAGE => A package replication is in progress
        - REPLICATING_TOMBSTONE => A tombstone package replication is in progress
        - RESUBMITTING => A package re-submission is in progress
        - STORED => Package stored on storage location
        - TOMBSTONE_REPLICATION_PENDING => A tombstone replication is pending
        - UPDATING_COMPLIANCE_LEVEL => A package compliance update is in progress
        - UPDATING_RETENTION => A package retention update is in progress during disposal process
        - UPGRADING_METADATA => A metadata version upgrade is in progress
        """)
public enum PackageStatus {
  //@formatter:off
  CHECK_PENDING, CHECKING, CHECKED,
  CLEANING,CLEANED,
  COMPLETED, DOWNLOADING, IN_PREPARATION, IN_PROGRESS, STORED, INDEXING, READY,
  IN_ERROR, PRESERVATION_ERROR, FIXITY_ERROR,
  DISPOSABLE, DISPOSAL_APPROVED_BY_ORGUNIT, DISPOSAL_APPROVED, DISPOSED,
  FIX_PENDING, FIXING,
  METADATA_EDITION_PENDING, EDITING_METADATA, UPDATING_RETENTION,
  METADATA_UPGRADE_PENDING, UPGRADING_METADATA,
  COMPLIANCE_LEVEL_UPDATE_PENDING, UPDATING_COMPLIANCE_LEVEL,COMPLIANCE_LEVEL_UPDATED,
  REINDEXING,
  RELOADED,
  RESUBMITTING,
  PACKAGE_REPLICATION_PENDING, REPLICATING_PACKAGE,
  TOMBSTONE_REPLICATION_PENDING,REPLICATING_TOMBSTONE;
  //@formatter:on

  public static boolean isDisposable(PackageStatus status) {
    return status.equals(DISPOSABLE);
  }

  public static boolean isDisposalInProgress(PackageStatus status) {
    return (isDisposable(status)
            || status.equals(DISPOSAL_APPROVED)
            || status.equals(DISPOSAL_APPROVED_BY_ORGUNIT));
  }

  public static boolean isDisposalProcess(PackageStatus status) {
    return (status.equals(DISPOSABLE)
            || status.equals(DISPOSAL_APPROVED)
            || status.equals(DISPOSAL_APPROVED_BY_ORGUNIT)
            || status.equals(DISPOSED));
  }

  public static boolean isCleaningProcess(PackageStatus status) {
    return (status.equals(CLEANING)
            || status.equals(CLEANED));
  }

  public static boolean isDisposalApprovedProcess(String status) {
    return (DISPOSAL_APPROVED.toString().equals(status)
            || DISPOSAL_APPROVED_BY_ORGUNIT.toString().equals(status)
            || DISPOSED.toString().equals(status));
  }

  public static boolean isReindexingProcess(PackageStatus status) {
    return (status.equals(REINDEXING));
  }

  public static boolean isMetadataEditionProcess(PackageStatus status) {
    return (status.equals(METADATA_EDITION_PENDING)
            || status.equals(EDITING_METADATA));
  }

  public static boolean isCompletedProcess(PackageStatus status) {
    return (status == PackageStatus.COMPLETED
            || status == PackageStatus.CLEANED
            || status == PackageStatus.DISPOSED);
  }

  public static boolean isInError(PackageStatus status) {
    return (status == PackageStatus.IN_ERROR
            || status == PackageStatus.PRESERVATION_ERROR);
  }

  public static boolean isCheckProcess(PackageStatus status) {
    return (status == PackageStatus.CHECK_PENDING
            || status == PackageStatus.CHECKING);
  }

  public static boolean isPackageReplicationProcess(PackageStatus status) {
    return (status == PackageStatus.PACKAGE_REPLICATION_PENDING
            || status == PackageStatus.REPLICATING_PACKAGE);
  }

  public static boolean isTombstoneReplicationProcess(PackageStatus status) {
    return (status == PackageStatus.TOMBSTONE_REPLICATION_PENDING
            || status == PackageStatus.REPLICATING_TOMBSTONE);
  }

  public static boolean isMetadataUpgradeProcess(PackageStatus status) {
    return (status == PackageStatus.METADATA_UPGRADE_PENDING
            || status == PackageStatus.UPGRADING_METADATA);
  }

  public static boolean isComplianceUpdateProcess(PackageStatus status) {
    return (status == PackageStatus.COMPLIANCE_LEVEL_UPDATE_PENDING
            || status == PackageStatus.UPDATING_COMPLIANCE_LEVEL);
  }

  public static boolean isInProgress(PackageStatus status) {
    return (!isInError(status) && !isCompletedProcess(status) && !isDisposable(status));
  }

}

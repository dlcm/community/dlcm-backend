/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - FileUploadDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.dto;

import java.io.InputStream;
import java.util.Map;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;

/**
 * DTO for uploading data file
 *
 * @param <S> The type of the datafile such as DepositDataFile, SIPDataFile, AIPDataFile
 */
public class FileUploadDto<S extends AbstractDataFile<?, ?>> {
  private String originalFileName;
  private InputStream inputStream;
  private S dataFile;
  private DataCategory dataCategory;
  private DataCategory dataType;
  private String relLocation;
  private String metadataType;
  private Map<ChecksumAlgorithm, String> checksums;
  private Map<ChecksumAlgorithm, DataFileChecksum.ChecksumOrigin> checksumsOrigin;

  public FileUploadDto() {
    this.setRelLocation("/");
  }

  public FileUploadDto(S depositDataFile) {
    this.setRelLocation("/");
    this.setDataFile(depositDataFile);
  }

  public String getOriginalFileName() {
    return this.originalFileName;
  }

  public void setOriginalFileName(String originalFileName) {
    this.originalFileName = originalFileName;
  }

  public InputStream getInputStream() {
    return this.inputStream;
  }

  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  public S getDataFile() {
    return this.dataFile;
  }

  public void setDataFile(S dataFile) {
    this.dataFile = dataFile;
  }

  public Map<ChecksumAlgorithm, String> getChecksums() {
    return this.checksums;
  }

  public void setChecksums(Map<ChecksumAlgorithm, String> checksums) {
    this.checksums = checksums;
  }

  public Map<ChecksumAlgorithm, DataFileChecksum.ChecksumOrigin> getChecksumsOrigin() {
    return this.checksumsOrigin;
  }

  public void setChecksumsOrigin(Map<ChecksumAlgorithm, DataFileChecksum.ChecksumOrigin> checksumsOrigin) {
    this.checksumsOrigin = checksumsOrigin;
  }

  public DataCategory getDataCategory() {
    return this.dataCategory;
  }

  public void setDataCategory(DataCategory dataCategory) {
    this.dataCategory = dataCategory;
  }

  public void setDataCategory(String dataCategory) {
    if (StringTool.isNullOrEmpty(dataCategory)) {
      return;
    }
    this.dataCategory = DataCategory.valueOf(dataCategory);
  }

  public DataCategory getDataType() {
    return this.dataType;
  }

  public void setDataType(DataCategory dataType) {
    this.dataType = dataType;
  }

  public void setDataType(String dataType) {
    if (StringTool.isNullOrEmpty(dataType)) {
      return;
    }
    this.dataType = DataCategory.valueOf(dataType);
  }

  public String getRelLocation() {
    return this.relLocation;
  }

  public void setRelLocation(String relLocation) {
    this.relLocation = StringTool.decode64IfEncoded(relLocation);
  }

  public String getMetadataType() {
    return this.metadataType;
  }

  public void setMetadataType(String metadataType) {
    this.metadataType = metadataType;
  }
}

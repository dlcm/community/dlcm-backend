/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AuthorizedInstitutionDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.dto;

import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.model.CreatedChangeInfo;
import ch.unige.solidify.model.LastModifiedChangeInfo;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;

public class AuthorizedInstitutionDto extends Institution {

  private Role role;

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public static AuthorizedInstitutionDto fromEntity(Institution institution) {
    AuthorizedInstitutionDto institutionDto = new AuthorizedInstitutionDto();
    institutionDto.setResId(institution.getResId());
    institutionDto.setName(institution.getName());
    institutionDto.setDescription(institution.getDescription());
    institutionDto.setRorId(institution.getRorId());
    institutionDto.setEmailSuffixes(institution.getEmailSuffixes());
    institutionDto.setUrl(institution.getUrl());
    institutionDto.setLogo(institution.getLogo());
    institutionDto.creation = new CreatedChangeInfo();
    institutionDto.creation.setFullName(institution.getCreation().getFullName());
    institutionDto.creation.setWhen(institution.getCreation().getWhen());
    institutionDto.creation.setWho(institution.getCreation().getWho());
    institutionDto.lastUpdate = new LastModifiedChangeInfo();
    institutionDto.lastUpdate.setFullName(institution.getLastUpdate().getFullName());
    institutionDto.lastUpdate.setWhen(institution.getLastUpdate().getWhen());
    institutionDto.lastUpdate.setWho(institution.getLastUpdate().getWho());
    return institutionDto;
  }
}

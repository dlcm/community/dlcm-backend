/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AuthorizedOrganizationalUnitDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.dto;

import java.util.Objects;

import ch.unige.solidify.model.CreatedChangeInfo;
import ch.unige.solidify.model.LastModifiedChangeInfo;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;

public class AuthorizedOrganizationalUnitDto extends OrganizationalUnit {

  private Role role;

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  private Role roleFromOrganizationalUnit;

  public Role getRoleFromOrganizationalUnit() {
    return roleFromOrganizationalUnit;
  }

  public void setRoleFromOrganizationalUnit(Role roleFromOrganizationalUnit) {
    this.roleFromOrganizationalUnit = roleFromOrganizationalUnit;
  }

  private Role roleFromInstitution;

  public Role getRoleFromInstitution() {
    return roleFromInstitution;
  }

  public void setRoleFromInstitution(Role roleFromInstitution) {
    this.roleFromInstitution = roleFromInstitution;
  }

  public static AuthorizedOrganizationalUnitDto fromEntity(OrganizationalUnit organizationalUnit) {
    AuthorizedOrganizationalUnitDto orgUnitDto = new AuthorizedOrganizationalUnitDto();
    orgUnitDto.setResId(organizationalUnit.getResId());
    orgUnitDto.setName(organizationalUnit.getName());
    orgUnitDto.setClosingDate(organizationalUnit.getClosingDate());
    orgUnitDto.setDefaultLicense(organizationalUnit.getDefaultLicense());
    orgUnitDto.setDefaultPreservationPolicy(organizationalUnit.getDefaultPreservationPolicy());
    orgUnitDto.setDefaultSubmissionPolicy(organizationalUnit.getDefaultSubmissionPolicy());
    orgUnitDto.setDescription(organizationalUnit.getDescription());
    orgUnitDto.setIsEmpty(organizationalUnit.getIsEmpty());
    orgUnitDto.setKeywords(organizationalUnit.getKeywords());
    orgUnitDto.setOpeningDate(organizationalUnit.getOpeningDate());
    orgUnitDto.setUrl(organizationalUnit.getUrl());
    orgUnitDto.setLogo(organizationalUnit.getLogo());
    orgUnitDto.creation = new CreatedChangeInfo();
    orgUnitDto.creation.setFullName(organizationalUnit.getCreation().getFullName());
    orgUnitDto.creation.setWhen(organizationalUnit.getCreation().getWhen());
    orgUnitDto.creation.setWho(organizationalUnit.getCreation().getWho());
    orgUnitDto.lastUpdate = new LastModifiedChangeInfo();
    orgUnitDto.lastUpdate.setFullName(organizationalUnit.getLastUpdate().getFullName());
    orgUnitDto.lastUpdate.setWhen(organizationalUnit.getLastUpdate().getWhen());
    orgUnitDto.lastUpdate.setWho(organizationalUnit.getLastUpdate().getWho());
    return orgUnitDto;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    AuthorizedOrganizationalUnitDto that = (AuthorizedOrganizationalUnitDto) o;
    return Objects.equals(role, that.role) && Objects.equals(roleFromOrganizationalUnit, that.roleFromOrganizationalUnit)
            && Objects.equals(roleFromInstitution, that.roleFromInstitution);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), role, roleFromOrganizationalUnit, roleFromInstitution);
  }
}

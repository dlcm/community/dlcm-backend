/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AverageRating.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.dto;

import ch.dlcm.model.settings.RatingType;

public class AverageRating {

  private String ratingType;

  private Integer averageValue;

  private Long numberVotes;

  public AverageRating() {
  }

  public AverageRating(String ratingType, double averageValue, long numberVotes) {
    this.ratingType = ratingType;
    this.averageValue = (int) averageValue;
    this.numberVotes = numberVotes;
  }

  public String getRatingType() {
    return this.ratingType;
  }

  public Integer getAverageValue() {
    return this.averageValue;
  }

  public void setRatingType(String ratingType) {
    this.ratingType = ratingType;
  }

  public void setAverageValue(Integer averageValue) {
    this.averageValue = averageValue;
  }

  public Long getNumberVotes() {
    return this.numberVotes;
  }

  public void setNumberVotes(Long numberVotes) {
    this.numberVotes = numberVotes;
  }

  public void init(RatingType ratingType) {
    if (this.ratingType == null) {
      this.ratingType = ratingType.getResId();
    }
    if (this.averageValue == null) {
      this.averageValue = 0;
    }
    if (this.numberVotes == null) {
      this.numberVotes = 0L;
    }
  }
}

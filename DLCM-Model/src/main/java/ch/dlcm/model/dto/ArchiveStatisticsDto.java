/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveStatisticsDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.dto;

import java.util.ArrayList;
import java.util.List;

import ch.dlcm.model.StatisticsInfo;
import ch.dlcm.model.settings.RatingType;

public class ArchiveStatisticsDto {

  private StatisticsInfo statistics;

  private List<AverageRating> averageRatings;

  public ArchiveStatisticsDto() {
  }

  public StatisticsInfo getStatistics() {
    return this.statistics;
  }

  public void setStatistics(StatisticsInfo statistics) {
    this.statistics = statistics;
  }

  public List<AverageRating> getAverageRatings() {
    return this.averageRatings;
  }

  public void setAverageRating(List<AverageRating> averageRatings) {
    this.averageRatings = averageRatings;
  }

  public void init(List<RatingType> ratingTypeList) {
    if (this.statistics == null) {
      this.statistics = new StatisticsInfo();
      this.statistics.init();
    }
    if (this.averageRatings == null) {
      this.averageRatings = new ArrayList<>();
      for (RatingType ratingType : ratingTypeList) {
        AverageRating averageRating = new AverageRating();
        averageRating.init(ratingType);
        this.averageRatings.add(averageRating);
      }
    }
  }
}

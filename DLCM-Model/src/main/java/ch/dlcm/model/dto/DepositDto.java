/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.dto;

import java.util.List;

import jakarta.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.dlcm.model.preingest.Deposit;

public class DepositDto extends Deposit {
  private List<DepositStatus> statusList;

  @Transient
  @JsonIgnore
  public List<DepositStatus> getStatusList() {
    return this.statusList;
  }

  public void setStatusList(List<DepositStatus> statusList) {
    this.statusList = statusList;
  }

}

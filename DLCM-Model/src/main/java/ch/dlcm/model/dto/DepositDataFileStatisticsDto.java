/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositDataFileStatisticsDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.dto;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import ch.dlcm.model.AbstractDataFile.DataFileStatus;

public class DepositDataFileStatisticsDto {
  private DataFileStatus dataFileStatus;
  private long numberOfOccurence;

  public DepositDataFileStatisticsDto(DataFileStatus dataFileStatus, long numberOfOccurrence) {
    this.dataFileStatus = dataFileStatus;
    this.numberOfOccurence = numberOfOccurrence;
  }

  public DataFileStatus getDataFileStatus() {
    return this.dataFileStatus;
  }

  public long getNumberOfOccurence() {
    return this.numberOfOccurence;
  }

  public static Map<DataFileStatus, Long> getMapFromList(List<DepositDataFileStatisticsDto> listStatistics) {
    Map<DataFileStatus, Long> mapStatistics = new EnumMap<>(DataFileStatus.class);
    for (DepositDataFileStatisticsDto stat : listStatistics) {
      mapStatistics.put(stat.getDataFileStatus(), stat.getNumberOfOccurence());
    }
    return mapStatistics;
  }
}

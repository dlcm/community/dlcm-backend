/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchivePublicData.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.index;

import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.DLCMConstants;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;

@Schema(description = """
        Archive public data are data files which stored in the archive but with a public access:
        - ARCHIVE_THUMBNAIL => Dataset Thumbnail
        - ARCHIVE_README    => Dataset README
        - ARCHIVE_DUA       => Dataset Data Use Agreement
        """)
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "aipId", "dataFileType" }, name = "aipId_dataFileType") })
public class ArchivePublicData extends ResourceNormalized implements ResourceFileInterface {

  public enum PublicDataFileType {
    ARCHIVE_THUMBNAIL, ARCHIVE_README, ARCHIVE_DUA
  }

  @Schema(description = "The archive identifier of the archive public data.")
  @NotNull
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String aipId;

  @Schema(description = "The data file type of the archive public data.")
  @Enumerated(EnumType.STRING)
  private PublicDataFileType dataFileType;

  @Schema(description = "The data file of the archive public data.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = DLCMConstants.DB_DUA_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private ArchivePublicDataFile dataFile;

  public String getAipId() {
    return this.aipId;
  }

  public void setAipId(String aipId) {
    this.aipId = aipId;
  }

  public void setDataFileType(PublicDataFileType dataFileType) {
    this.dataFileType = dataFileType;
  }

  public PublicDataFileType getDataFileType() {
    return this.dataFileType;
  }

  public ArchivePublicDataFile getDataFile() {
    return this.dataFile;
  }

  public void setDataFile(ArchivePublicDataFile dataFile) {
    this.dataFile = dataFile;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.DATAMGMT;
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getDataFile();
  }

  @Override
  public ResourceFile setNewResourceFile() {
    this.setDataFile(new ArchivePublicDataFile());
    return this.getDataFile();
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.setDataFile((ArchivePublicDataFile) resourceFile);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.dataFile, this.dataFileType);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    ArchivePublicData other = (ArchivePublicData) obj;
    return Objects.equals(this.dataFile, other.dataFile) && this.dataFileType == other.dataFileType;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.UPLOAD_DATASET_FILE).withRel(DLCMActionName.UPLOAD_DATASET_FILE));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.DOWNLOAD_DATASET_FILE).withRel(DLCMActionName.DOWNLOAD_DATASET_FILE));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.DELETE_DATASET_FILE).withRel(DLCMActionName.DELETE_DATASET_FILE));
    }
  }

  public static String getMetadataFieldForPublicData(PublicDataFileType publicDataType) {
    switch (publicDataType) {
      case ARCHIVE_DUA:
        return DLCMConstants.AIP_DUA;
      case ARCHIVE_README:
        return DLCMConstants.AIP_README;
      case ARCHIVE_THUMBNAIL:
        return DLCMConstants.AIP_THUMBNAIL;

    }
    throw new SolidifyProcessingException("Wrong data file type: " + publicDataType);
  }

  public static String getMetadataFieldForPublicDataContentType(PublicDataFileType publicDataType) {
    switch (publicDataType) {
      case ARCHIVE_DUA:
        return DLCMConstants.AIP_DUA_CONTENT_TYPE;
      case ARCHIVE_README:
        return DLCMConstants.AIP_README_CONTENT_TYPE;
      case ARCHIVE_THUMBNAIL:
        return DLCMConstants.AIP_THUMBNAIL_CONTENT_TYPE;

    }
    throw new SolidifyProcessingException("Wrong data file type: " + publicDataType);
  }
}

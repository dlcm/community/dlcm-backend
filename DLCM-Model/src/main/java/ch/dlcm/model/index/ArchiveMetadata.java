/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveMetadata.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.index;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.model.oai.OAIRecord;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.deserializer.ArchiveMetadataDeserializer;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.index.ArchivePublicData.PublicDataFileType;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

@Schema(description = "Public metadata of an archive")
@JsonDeserialize(using = ArchiveMetadataDeserializer.class)
public class ArchiveMetadata extends IndexMetadata implements OrganizationalUnitAwareResource, OAIRecord {

  public void addLinksForAccess(WebMvcLinkBuilder linkBuilder) {
    this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.DOWNLOAD_STATUS).withRel(DLCMActionName.DOWNLOAD_STATUS));
    this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.PREPARE_DOWNLOAD).withRel(DLCMActionName.PREPARE_DOWNLOAD));
    this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
    if (this.isPublicDataPresent(PublicDataFileType.ARCHIVE_THUMBNAIL).booleanValue()) {
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.THUMBNAIL).withRel(DLCMActionName.THUMBNAIL));
    }
    if (this.isPublicDataPresent(PublicDataFileType.ARCHIVE_DUA).booleanValue()) {
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.DUA).withRel(DLCMActionName.DUA));
    }
    if (this.isPublicDataPresent(PublicDataFileType.ARCHIVE_README).booleanValue()) {
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.README).withRel(DLCMActionName.README));
    }
    if (!this.isUnit().booleanValue()) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.AIP).withRel(ResourceName.AIP));
    }
    this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.STATISTICS).withRel(DLCMActionName.STATISTICS));
    if (this.isStructureContentPublic().booleanValue()) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PACKAGES).withRel(ResourceName.PACKAGES));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.DATAFILE).withRel(ResourceName.DATAFILE));
      this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.LIST_FOLDERS).withRel(DLCMActionName.LIST_FOLDERS));
    }
    this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.RATING).withRel(DLCMActionName.RATING));
    this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.BIBLIOGRAPHIES).withRel(DLCMActionName.BIBLIOGRAPHIES));
    this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.CITATIONS).withRel(DLCMActionName.CITATIONS));
    super.addLinks(linkBuilder);
  }

  @Schema(description = "The current access level of the archive, deducted from the final access level and an eventual embargo.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public Access getCurrentAccess() {
    final Optional<Access> embargoAccessLevel = this.getEmbargoAccessLevel();
    if (embargoAccessLevel.isPresent()) {
      final OffsetDateTime embargoEndDate = this.getEmbargoEndDate().orElseThrow(() -> new RuntimeException(
              "ArchiveMetadata " + this.getResId() + " has an embargo AccessLevel but does not contain any embargo end date"));
      if (embargoEndDate.isBefore(OffsetDateTime.now())) {
        // embargo is over
        return this.getFinalAccessLevel();
      } else {
        // embargo is still running
        return embargoAccessLevel.get();
      }
    } else {
      // no embargo
      return this.getFinalAccessLevel();
    }
  }

  @JsonIgnore
  public Optional<Access> getEmbargoAccessLevel() {
    return this.getAccessLevel(DLCMConstants.AIP_EMBARGO_LEVEL);
  }

  @JsonIgnore
  public Optional<OffsetDateTime> getEmbargoEndDate() {
    final String endEmbargo = (String) this.getMetadata(DLCMConstants.AIP_EMBARGO_END_DATE);
    if (!StringTool.isNullOrEmpty(endEmbargo)) {
      return Optional.of(OffsetDateTime.parse(endEmbargo));
    }
    return Optional.empty();
  }

  @JsonIgnore
  public Boolean isStructureContentPublic() {
    final Boolean isStructureContentPublic = (Boolean) this.getMetadata(DLCMConstants.AIP_CONTENT_STRUCTURE_PUBLIC);
    if (isStructureContentPublic == null) {
      return false;
    }
    return isStructureContentPublic;
  }

  @JsonIgnore
  public Access getFinalAccessLevel() {
    return this.getAccessLevel(DLCMConstants.AIP_ACCESS_LEVEL)
            .orElseThrow(() -> new RuntimeException("Final access level is unknown for ArchiveMetadata " + this.getResId()));
  }

  @JsonIgnore
  public DataTag getDataTag() {
    final String dataTag = (String) this.getMetadata(DLCMConstants.AIP_DATA_TAG);
    if (!StringTool.isNullOrEmpty(dataTag)) {
      return DataTag.valueOf(dataTag);
    }
    return DataTag.UNDEFINED;
  }

  @Override
  @JsonIgnore
  public String getOrganizationalUnitId() {
    return (String) this.getMetadata(DLCMConstants.AIP_ORGA_UNIT);
  }

  @JsonIgnore
  public String getMetadataVersion() {
    return (String) this.getMetadata(DLCMConstants.METADATA_VERSION_FIELD);
  }

  @JsonIgnore
  public long getArchiveSize() {
    Object size = this.getMetadata(DLCMConstants.AIP_SIZE);
    if (size instanceof Integer sizeInt) {
      return sizeInt.longValue();
    } else if (size instanceof Long sizeLong) {
      return sizeLong.longValue();
    }
    return Long.parseLong((String) size);
  }

  @JsonIgnore
  public Boolean isPublicDataPresent(PublicDataFileType publicDataType) {
    final Boolean isPresent = (Boolean) this.getMetadata(ArchivePublicData.getMetadataFieldForPublicData(publicDataType));
    if (isPresent == null) {
      return false;
    }
    return isPresent;
  }

  @JsonIgnore
  public String getPublicDataContentType(PublicDataFileType publicDataType) {
    return (String) this.getMetadata(ArchivePublicData.getMetadataFieldForPublicDataContentType(publicDataType));
  }

  @JsonIgnore
  public Boolean isUnit() {
    final Boolean unit = (Boolean) this.getMetadata(DLCMConstants.AIP_UNIT);
    if (unit == null) {
      return false;
    }
    return unit;
  }

  @Override
  @JsonIgnore
  public String getOAIMetadata() {
    return (String) this.getMetadata(DLCMConstants.DATACITE_XML_FIELD);
  }

  private Optional<Access> getAccessLevel(String accessField) {
    final String accessLevel = (String) this.getMetadata(accessField);
    if (!StringTool.isNullOrEmpty(accessLevel)) {
      return Optional.of(Access.valueOf(accessLevel));
    }
    return Optional.empty();
  }

  @Override
  @JsonIgnore
  public OffsetDateTime getCreationDate() {
    final String creation = (String) this.getMetadata(DLCMConstants.AIP_LAST_UPDATE.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
    return OffsetDateTime.parse(creation).toInstant().atOffset(ZoneOffset.UTC).withNano(0);
  }

  @Override
  @JsonIgnore
  public String getOaiId() {
    return this.getResId();
  }

  @JsonIgnore
  public String getIdentifierType() {
    if (!this.isPublicIndex()) {
      return null;
    }
    return (String) this.getMetadata(DLCMConstants.IDENTIFIER_TYPE_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
  }

  @JsonIgnore
  public String getDoi() {
    if (!this.isPublicIndex()) {
      return null;
    }
    final String doi = this.getIdentifier(DLCMConstants.DOI);
    if (doi == null) {
      return this.getAlternateIdentifier(DLCMConstants.DOI);
    }
    return doi;
  }

  @JsonIgnore
  public String getArk() {
    if (!this.isPublicIndex()) {
      return null;
    }
    final String ark = this.getIdentifier(DLCMConstants.ARK);
    if (ark == null) {
      return this.getAlternateIdentifier(DLCMConstants.ARK);
    }
    return ark;
  }

  @JsonIgnore
  public String getResourceTypeGeneral() {
    return this.getMetadata(DLCMConstants.RESOURCE_TYPE_GENERAL_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)).toString();
  }

  @JsonIgnore
  public String getResourceType() {
    return this.getMetadata(DLCMConstants.RESOURCE_TYPE_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)).toString();
  }

  @JsonIgnore
  public String getTitle() {
    return this.getMetadata(DLCMConstants.TITLE_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)).toString();
  }

  @JsonIgnore
  public String getAbstract() {
    if (this.getField(DLCMConstants.DESCRIPTIONS_FIELD) != null) {
      List<Map<String, Object>> descriptionList = this.getField(DLCMConstants.DESCRIPTION_FIELD);
      for (Map<String, Object> description : descriptionList) {
        if (description.get(DLCMConstants.DATACITE_DESCRIPTION_TYPE_FIELD).equals(DLCMConstants.ABSTRACT)) {
          return (String) description.get(DLCMConstants.CONTENT_FIELD);
        }
      }
    }
    return null;
  }

  @JsonIgnore
  public String getAbstractLanguage() {
    if (this.getField(DLCMConstants.DESCRIPTIONS_FIELD) != null) {
      List<Map<String, Object>> descriptionList = this.getField(DLCMConstants.DESCRIPTION_FIELD);
      for (Map<String, Object> description : descriptionList) {
        if (description.get(DLCMConstants.DATACITE_DESCRIPTION_TYPE_FIELD).equals(DLCMConstants.ABSTRACT)) {
          return (String) description.get(DLCMConstants.LANGUAGE_FIELD);
        }
      }
    }
    return null;
  }

  @JsonIgnore
  public List<String> getKeywords() {
    if (((Map<String, Object>) this.getMetadata(DLCMConstants.DATACITE_RESOURCE_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)))
            .containsKey(DLCMConstants.DATACITE_SUBJECTS_NAME)
            && this.getMetadata(DLCMConstants.SUBJECT_SCHEME_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP))
                    .equals(DLCMConstants.KEYWORDS)) {
      String keywords = (String) this.getMetadata(DLCMConstants.SUBJECT_CONTENT_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
      if (!StringTool.isNullOrEmpty(keywords)) {
        return Arrays.asList(keywords.split(SolidifyConstants.FIELD_SEP));
      }
    }
    return Collections.emptyList();
  }

  @JsonIgnore
  public List<Map<String, Object>> getRelations() {
    if (((Map<String, Object>) this.getMetadata(DLCMConstants.DATACITE_RESOURCE_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)))
            .containsKey(DLCMConstants.DATACITE_RELATIONS_NAME)
            &&
            ((Map<String, Object>) this.getMetadata(DLCMConstants.RELATIONS_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)))
                    .containsKey(DLCMConstants.DATACITE_RELATION_NAME)) {
      return this.getField(DLCMConstants.RELATION_FIELD);
    }
    return Collections.emptyList();
  }

  @JsonIgnore
  public List<Map<String, Object>> getRights() {
    return this.getField(DLCMConstants.RIGHTS_FIELD);
  }

  @JsonIgnore
  public String getPublisher() {
    return this.getMetadata(DLCMConstants.PUBLISHER_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)).toString();
  }

  @JsonIgnore
  public List<Map<String, String>> getCreators() {
    List<Map<String, Object>> creators = this.getField(DLCMConstants.CREATOR_FIELD);
    List<Map<String, String>> creatorsList = new ArrayList<>();
    for (Map<String, Object> creator : creators) {
      Map<String, String> creatorMap = new HashMap<>();

      if (creator.containsKey(DLCMConstants.DATACITE_FAMILY_NAME)) {
        String familyName = creator.get(DLCMConstants.DATACITE_FAMILY_NAME).toString();
        creatorMap.put(DLCMConstants.FAMILY_NAME, familyName);
      }
      if (creator.containsKey(DLCMConstants.DATACITE_FAMILY_NAME)) {
        String givenName = creator.get(DLCMConstants.DATACITE_GIVEN_NAME).toString();
        creatorMap.put(DLCMConstants.GIVEN_NAME, givenName);
      }
      if (creator.containsKey(DLCMConstants.DATACITE_IDENTIFIER_NAME)) {
        String orcid = ((Map<String, Object>) creator.get(DLCMConstants.DATACITE_IDENTIFIER_NAME)).get(DLCMConstants.CONTENT_FIELD).toString();
        creatorMap.put(DLCMConstants.PERSON_ORCID, orcid);
      }

      if (!creatorMap.isEmpty()) {
        creatorsList.add(creatorMap);
      }
    }
    return creatorsList;
  }

  @JsonIgnore
  public OffsetDateTime getIssuedDate() {
    return this.getDate(DLCMConstants.DATE_ISSUED_ATTRIBUTE);
  }

  @JsonIgnore
  public OffsetDateTime getCreatedDate() {
    return this.getDate(DLCMConstants.DATE_CREATED_ATTRIBUTE);
  }

  @JsonIgnore
  public OffsetDateTime getUpdatedDate() {
    return this.getDate(DLCMConstants.DATE_UPDATED_ATTRIBUTE);
  }

  @JsonIgnore
  public OffsetDateTime getAcceptedDate() {
    return this.getDate(DLCMConstants.DATE_ACCEPTED_ATTRIBUTE);
  }

  @JsonIgnore
  public OffsetDateTime getAvailableDate() {
    return this.getDate(DLCMConstants.DATE_AVAILABLE_ATTRIBUTE);
  }

  @JsonIgnore
  public OffsetDateTime getCollectedStartDate() {
    return this.getDate(DLCMConstants.DATE_COLLECTED_START_ATTRIBUTE);
  }

  @JsonIgnore
  public OffsetDateTime getCollectedEndDate() {
    return this.getDate(DLCMConstants.DATE_COLLECTED_END_ATTRIBUTE);
  }

  @JsonIgnore
  public String getRetention() {
    return this.getMetadata(DLCMConstants.AIP_RETENTION.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)).toString();
  }

  @JsonIgnore
  public DataUsePolicy getDataUsePolicy() {
    final String duaType = (String) this.getMetadata(DLCMConstants.AIP_DATA_USE_POLICY);
    if (!StringTool.isNullOrEmpty(duaType)) {
      return DataUsePolicy.valueOf(duaType);
    }
    return DataUsePolicy.NONE;
  }

  private OffsetDateTime getDate(String dateType) {
    List<Map<String, String>> dates = (List<Map<String, String>>) this
            .getMetadata(DLCMConstants.DATES_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
    for (Map<String, String> date : dates) {
      if (date.get(DLCMConstants.DATE_TYPE).equals(dateType)) {
        String dateStr = date.get(DLCMConstants.CONTENT_FIELD);
        return OffsetDateTime.parse(dateStr);
      }
    }
    return null;
  }

  private String getIdentifier(String identifierType) {
    if (((String) this.getMetadata(DLCMConstants.IDENTIFIER_TYPE_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP))).equals(identifierType)) {
      return (String) this.getMetadata(DLCMConstants.IDENTIFIER_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
    }
    return null;
  }

  private String getAlternateIdentifier(String identifierType) {
    if (this.getField(DLCMConstants.ALTERNATE_IDENTIFIERS_FIELD) != null) {
      List<Map<String, Object>> alternateIdList = this.getField(DLCMConstants.ALTERNATE_IDENTIFIER_FIELD);
      for (Map<String, Object> alternateId : alternateIdList) {
        if (alternateId.get(DLCMConstants.ALTERNATE_ID_TYPE).equals(identifierType)) {
          return (String) alternateId.get(DLCMConstants.CONTENT_FIELD);
        }
      }
    }
    return null;
  }

  private boolean isPublicIndex() {
    return !this.getIndex().endsWith(DLCMConstants.PRIVATE);
  }
}

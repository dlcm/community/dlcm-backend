/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - Tool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import java.util.Objects;

import jakarta.persistence.Embeddable;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Software tool information.")
@Embeddable
public class Tool {

  @Schema(description = "The description of the tool.")
  private String description;
  @Schema(description = "The name of the tool.")
  private String name;
  @Schema(description = "The PRONOM identifier of the tool.")
  private String puid;
  @Schema(description = "The version of the tool.")
  private String version;

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof Tool)) {
      return false;
    }
    final Tool castOther = (Tool) other;
    return Objects.equals(this.getName(), castOther.getName()) && Objects.equals(this.getDescription(), castOther.getDescription()) && Objects
            .equals(this.getVersion(), castOther.getVersion()) && Objects.equals(this.getPuid(), castOther.getPuid());
  }

  public String getDescription() {
    return this.description;
  }

  public String getName() {
    return this.name;
  }

  public String getPuid() {
    return this.puid;
  }

  public String getVersion() {
    return this.version;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getName(), this.getDescription(), this.getVersion(), this.getPuid());
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPuid(String puid) {
    this.puid = puid;
  }

  public void setVersion(String version) {
    this.version = version;
  }

}

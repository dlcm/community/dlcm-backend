/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - AipCopy.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preservation;

import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;

public class AipCopy {

  private ArchivalInfoPackage aip;
  private String storageUrl;
  private Boolean noUpdatedCopy;

  public AipCopy() {
  }

  public AipCopy(String storageUrl, ArchivalInfoPackage aip) {
    this.storageUrl = storageUrl;
    this.aip = aip;
    this.noUpdatedCopy = false;
  }

  public AipCopy(String storageUrl, ArchivalInfoPackage aip, boolean updatedCopy) {
    this.storageUrl = storageUrl;
    this.aip = aip;
    this.noUpdatedCopy = updatedCopy;
  }

  public ArchivalInfoPackage getAip() {
    return this.aip;
  }

  public PackageStatus getStatus() {
    if (this.aip != null) {
      return this.aip.getInfo().getStatus();
    } else {
      return null;
    }
  }

  public String getStorageUrl() {
    return this.storageUrl;
  }

  public void setAip(ArchivalInfoPackage aip) {
    this.aip = aip;
  }

  public void setStorageUrl(String storageUrl) {
    this.storageUrl = storageUrl;
  }

  public boolean isNoUpdatedCopy() {
    return noUpdatedCopy;
  }

  public void setNoUpdatedCopy(boolean noUpdatedCopy) {
    this.noUpdatedCopy = noUpdatedCopy;
  }
}

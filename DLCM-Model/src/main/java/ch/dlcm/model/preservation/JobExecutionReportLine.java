/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - JobExecutionReportLine.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preservation;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_LONG_STRING_LENGTH;

import java.io.Serial;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.util.StringTool;

@Entity
@Table(name = "preservationJobExecutionReportLine", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "resId", "reportId", "url" }) })
public class JobExecutionReportLine implements Serializable {

  @Serial
  private static final long serialVersionUID = -1878314122681443591L;

  public enum ItemStatus {
    ERROR, IGNORED, PROCESSED
  }

  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH) // Used by MySQL5ExtendedDialect
  private OffsetDateTime changeTime;

  @Size(max = DB_LONG_STRING_LENGTH)
  private String errorMessage = null;

  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String reportId;

  @NotNull
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String resId;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @JsonIgnore
  private long seq;

  @NotNull
  @Enumerated(EnumType.STRING)
  private ItemStatus status;

  private String url = null;

  public JobExecutionReportLine() {

  }

  public JobExecutionReportLine(String resId, ItemStatus status, String url) {
    this.resId = resId;
    this.status = status;
    this.url = url;
    this.changeTime = OffsetDateTime.now(ZoneOffset.UTC);
  }

  public JobExecutionReportLine(String resId, ItemStatus status, String url, String errorMessage) {
    this.resId = resId;
    this.status = status;
    this.url = url;
    this.errorMessage = StringTool.truncateWithEllipsis(errorMessage, DB_LONG_STRING_LENGTH);
    this.changeTime = OffsetDateTime.now(ZoneOffset.UTC);
  }

  public JobExecutionReportLine(String resId, String errorMessage, ItemStatus status, String url, OffsetDateTime dateTime) {
    this.resId = resId;
    this.errorMessage = StringTool.truncateWithEllipsis(errorMessage, DB_LONG_STRING_LENGTH);
    this.status = status;
    this.url = url;
    this.changeTime = dateTime;
  }

  public OffsetDateTime getChangeTime() {
    return this.changeTime;
  }

  public String getErrorMessage() {
    return this.errorMessage;
  }

  public String getReportId() {
    return this.reportId;
  }

  public String getResId() {
    return this.resId;
  }

  public long getSeq() {
    return this.seq;
  }

  public ItemStatus getStatus() {
    return this.status;
  }

  public String getUrl() {
    return this.url;
  }

  public void setReportId(String reportId) {
    this.reportId = reportId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof JobExecutionReportLine))
      return false;
    JobExecutionReportLine that = (JobExecutionReportLine) o;
    return this.seq == that.seq &&
            Objects.equals(this.changeTime, that.changeTime) &&
            Objects.equals(this.errorMessage, that.errorMessage) &&
            Objects.equals(this.reportId, that.reportId) &&
            Objects.equals(this.resId, that.resId) &&
            this.status == that.status &&
            Objects.equals(this.url, that.url);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.changeTime, this.errorMessage, this.reportId, this.resId, this.seq, this.status, this.url);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - JobExecution.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preservation;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_LONG_STRING_LENGTH;

import java.beans.Transient;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.listener.HistoryListener;
import ch.dlcm.model.preservation.PreservationJob.JobStatus;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;

@Schema(description = "Preservation job execution information")
@Entity
@EntityListeners({ HistoryListener.class })
@Table(name = "preservationJobExecution", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "jobId", "runNumber" }) })
public class JobExecution extends ResourceNormalized {

  @NotNull
  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_JOB_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private PreservationJob preservationJob;

  private int completionStatus = 0;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH)
  private OffsetDateTime endDate;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = DLCMConstants.DB_JOB_EXECUTION_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  @JsonIgnore
  private List<JobExecutionReport> executionReports = new ArrayList<>();

  private long runNumber;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH)
  private OffsetDateTime startDate;

  @Enumerated(EnumType.STRING)
  private JobStatus status;

  @Size(max = DB_LONG_STRING_LENGTH)
  private String statusMessage;

  public boolean addExecutionReport() {
    return this.getExecutionReports().add(this.newExecutionReport());
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.REPORT).withRel(DLCMActionName.REPORT));
    if (this.status != JobStatus.COMPLETED) {
      if (this.status == JobStatus.CREATED) {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.START).withRel(ActionName.START));
      } else {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.RESUME).withRel(ActionName.RESUME));
      }
    }
  }

  public int getCompletionStatus() {
    return this.completionStatus;
  }

  public OffsetDateTime getEndDate() {
    return this.endDate;
  }

  public List<JobExecutionReport> getExecutionReports() {
    return this.executionReports;
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public long getIgnoredItems() {
    if (this.getLastExecutionReport() != null) {
      return this.getLastExecutionReport().getIgnoredItems();
    } else {
      return 0;
    }
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public long getInErrorItems() {
    if (this.getLastExecutionReport() != null) {
      return this.getLastExecutionReport().getInErrorItems();
    } else {
      return 0;
    }
  }

  @Transient
  @JsonIgnore
  public JobExecutionReport getLastExecutionReport() {
    if (this.getExecutionReports().isEmpty()) {
      return null;
    }
    return this.getExecutionReports().stream().max(Comparator.comparing(JobExecutionReport::getExecutionNumber))
            .orElseThrow(NoSuchElementException::new);
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public long getProcessedItems() {
    if (this.getLastExecutionReport() != null) {
      return this.getLastExecutionReport().getProcessedItems();
    } else {
      return 0;
    }
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public int getReportNumber() {
    return this.getExecutionReports().size();
  }

  public long getRunNumber() {
    return this.runNumber;
  }

  public OffsetDateTime getStartDate() {
    return this.startDate;
  }

  public JobStatus getStatus() {
    return this.status;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public long getTotalItems() {
    if (this.getLastExecutionReport() != null) {
      return this.getLastExecutionReport().getTotalItems();
    } else {
      return 0;
    }
  }

  @Override
  public void init() {
    if (this.status == null) {
      this.status = JobStatus.CREATED;
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.PRES_PLANNING;
  }

  public void setCompletionStatus(int completionStatus) {
    this.completionStatus = completionStatus;
  }

  public void setEndDate(OffsetDateTime endDate) {
    this.endDate = endDate;
  }

  public void setRunNumber(long runNumber) {
    this.runNumber = runNumber;
  }

  public void setStartDate(OffsetDateTime startDate) {
    this.startDate = startDate;
  }

  public void setStatus(JobStatus status) {
    this.status = status;
  }

  public void setStatusMessage(String message) {
    this.statusMessage = StringTool.truncateWithEllipsis(message, DB_LONG_STRING_LENGTH);
  }

  public PreservationJob getPreservationJob() {
    return this.preservationJob;
  }

  public void setPreservationJob(PreservationJob preservationJob) {
    this.preservationJob = preservationJob;
  }

  private JobExecutionReport newExecutionReport() {
    final JobExecutionReport report = new JobExecutionReport();
    report.setJobExecutionId(this.getResId());
    report.setExecutionNumber(this.getReportNumber() + 1L);
    return report;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof JobExecution))
      return false;
    if (!super.equals(o))
      return false;
    JobExecution that = (JobExecution) o;
    return this.completionStatus == that.completionStatus &&
            this.runNumber == that.runNumber &&
            Objects.equals(this.endDate, that.endDate) &&
            Objects.equals(this.executionReports, that.executionReports) &&
            Objects.equals(this.preservationJob, that.preservationJob) &&
            Objects.equals(this.startDate, that.startDate) &&
            this.status == that.status &&
            Objects.equals(this.statusMessage, that.statusMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.completionStatus, this.endDate, this.executionReports, this.preservationJob, this.runNumber,
            this.startDate, this.status, this.statusMessage);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - PreservationJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preservation;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Schema(description = """
        The preservation job allows to run manage mass processing of archives. There are different types of job:
        - ARCHIVE_CHECK => Run the archive check on all archives
        - ARCHIVE_PRELOAD_BIG => Preload the archives bigger than 4GB to prepare DIP
        - ARCHIVE_PRELOAD_SMALL => Preload the archives smaller than 4GB to prepare DIP
        - CHECK_COMPLIANCE_LEVEL => coming soon
        - CLEAN_SUBMISSION => Purge deposits & SIP when archives are completed and replicated based on submission policy
        - SIMPLE_CLEAN_SUBMISSION => Purge deposits & SIP when archives are completed without checking replication based on submission policy
        - DISPOSAL => Trigger the disposal process
        - FIXITY => Run the fixity check for all archives
        - MIGRATION => Comming soon
        - PURGE_ORDER => Purge order data when order completed
        - PURGE_SUBMISSION_TEMP_FILES => Purge temporary files of submission process
        - REINDEX => Re-index all archives on main storage
        - REINDEX_ALL => Re-index all archives on all storages
        - RELOAD => Reload all archives from the storage
        - REPLICATION => Run the replication process on different storage nodes
        - REPLICATION_CHECK => Run the replication check on all archives
        """)
@Entity
public class PreservationJob extends ResourceNormalized
        implements RemoteResourceContainer {

  public enum JobStatus {
    COMPLETED, CREATED, IN_ERROR, IN_PROGRESS, READY
  }

  @Schema(description = "If the preservation job is enable.")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean enable;

  @JsonIgnore
  @OneToMany(mappedBy = "preservationJob", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<JobExecution> executions = new ArrayList<>();

  @Schema(description = "The recurrence of the preservation job.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private JobRecurrence jobRecurrence;

  @Schema(description = "The job type of the preservation job.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private JobType jobType;

  @Schema(description = "The name of the preservation job.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String name;

  private JobScheduling scheduling;

  @Schema(description = """
          The maximum items to process of the preservation job.
          The 0 value means no limit => all items.
          """)
  private long maxItems = 0;

  @Schema(description = """
          The parameters of the preservation job.
          The format is 'param1=value1&param2=value2&...&paramN=valueN.
          Parameters (not exhaustive list):
          - filter on metadata version: info.metadataVersion=<value>
          - filter on org. unit: info.organizationalUnitId=<value>
          - filter on archive size: archiveSize=<value>
          - filter on archive unit: archivalUnit=true
          - sort in last modification: sort=lastUpdate.when,asc
          """)
  private String parameters;

  @Override
  public <T> boolean addItem(T t) {
    if (t instanceof JobExecution je) {
      return this.addJobExecution(je);
    }
    return false;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      if (this.isEnable()) {
        if (this.getLastExecutionStatus() == JobStatus.IN_ERROR) {
          this.add(linkBuilder.slash(this.getResId()).slash(ActionName.RESUME)
                  .withRel(ActionName.RESUME));
          if (this.getJobRecurrence() != JobRecurrence.ONCE) {
            this.add(linkBuilder.slash(this.getResId()).slash(ActionName.START)
                    .withRel(ActionName.START));
          }
        } else {
          this.add(linkBuilder.slash(this.getResId()).slash(ActionName.START).withRel(ActionName.START));
        }
      }
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PRES_JOB_EXECUTION)
              .withRel(ResourceName.PRES_JOB_EXECUTION));
    }
  }

  public Boolean getEnable() {
    return this.enable;
  }

  @Schema(description = "The number of the preservation job executions.")
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public int getExecutionNumber() {
    return this.getExecutions().size();
  }

  public List<JobExecution> getExecutions() {
    return this.executions;
  }

  public JobRecurrence getJobRecurrence() {
    return this.jobRecurrence;
  }

  public JobType getJobType() {
    return this.jobType;
  }

  @Transient
  @JsonIgnore
  public JobExecution getLastExecution() {
    if (this.getExecutions().isEmpty()) {
      return null;
    }
    return this.getExecutions().stream().max(Comparator.comparing(JobExecution::getRunNumber))
            .orElseThrow(NoSuchElementException::new);
  }

  @Schema(description = "The status of the last job execution.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public JobStatus getLastExecutionStatus() {
    if (this.getLastExecution() != null && !this.getExecutions().isEmpty()) {
      return this.getLastExecution().getStatus();
    }
    return null;
  }

  public String getName() {
    return this.name;
  }

  public JobScheduling getScheduling() {
    return this.scheduling;
  }

  @PrePersist
  @Override
  public void init() {
    if (this.jobRecurrence == null) {
      this.jobRecurrence = JobRecurrence.ONCE;
    }
    if (this.enable == null) {
      this.enable = true;
    }
  }

  public boolean isEnable() {
    return this.enable;
  }

  @Override
  public String managedBy() {
    return ModuleName.PRES_PLANNING;
  }

  @Override
  public <T> boolean removeItem(T t) {
    if (t instanceof JobExecution je) {
      return this.removeJobExecution(je);
    }
    return false;
  }

  public boolean resume() {
    if (this.getExecutions().isEmpty()) {
      return this.addItem(this.newJobExecution());
    }
    if (this.getLastExecutionStatus().equals(JobStatus.IN_ERROR)) {
      this.getLastExecution().setStatus(JobStatus.READY);
      this.getLastExecution().setStatusMessage(null);
      return true;
    }
    return false;
  }

  public void setEnable(Boolean enable) {
    this.enable = enable;
  }

  public void setJobRecurrence(JobRecurrence jobRecurrence) {
    this.jobRecurrence = jobRecurrence;
  }

  public void setJobType(JobType jobType) {
    this.jobType = jobType;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setScheduling(JobScheduling scheduling) {
    this.scheduling = scheduling;
  }

  public boolean start() {
    if (this.getExecutions().isEmpty()) {
      return this.addItem(this.newJobExecution());
    }
    if ((this.getLastExecutionStatus() == JobStatus.COMPLETED || this.getLastExecutionStatus() == JobStatus.IN_ERROR)
            && (this.getJobRecurrence() != JobRecurrence.ONCE && this.getJobType().isRecurring())) {
      return this.addItem(this.newJobExecution());
    }
    return false;
  }

  private boolean addJobExecution(JobExecution e) {
    this.getExecutions().add(e);
    return true;
  }

  private JobExecution newJobExecution() {
    final JobExecution exe = new JobExecution();
    exe.setPreservationJob(this);
    exe.setStatus(JobStatus.READY);
    exe.setRunNumber(this.getExecutionNumber() + 1L);
    return exe;
  }

  private boolean removeJobExecution(JobExecution e) {
    return this.getExecutions().remove(e);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    PreservationJob other = (PreservationJob) obj;
    return Objects.equals(this.enable, other.enable) && Objects.equals(this.executions, other.executions)
            && this.jobRecurrence == other.jobRecurrence
            && this.jobType == other.jobType && this.maxItems == other.maxItems && Objects.equals(this.name, other.name)
            && Objects.equals(this.parameters, other.parameters) && Objects.equals(this.scheduling, other.scheduling);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.enable, this.executions, this.jobRecurrence, this.jobType, this.maxItems, this.name,
            this.parameters, this.scheduling);
    return result;
  }

  public long getMaxItems() {
    return this.maxItems;
  }

  public void setMaxItems(long maxItems) {
    this.maxItems = maxItems;
  }

  public String getParameters() {
    return this.parameters;
  }

  public void setParameters(String parameters) {
    this.parameters = parameters;
  }

}

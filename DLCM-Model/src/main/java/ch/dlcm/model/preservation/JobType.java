/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - JobType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preservation;

import java.util.ArrayList;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Preservation job type")
public enum JobType {
  // @formatter:off
  ARCHIVE_CHECK("Archive Check", true, true),
  ARCHIVE_PRELOAD_BIG("Archive [big size] Preloading", true, true),
  ARCHIVE_PRELOAD_SMALL("Archive [small size] Preloading", true, true),
  CHECK_COMPLIANCE_LEVEL("Check compliance level", true, true),
  CLEAN_SUBMISSION("Clean Submission", true, true),
  COMPLIANCE_LEVEL_UPDATE("Compliance Level Update", false, true),
  DISPOSAL("Trigger Disposal Processes", true, true),
  FIXITY("Fixity", true, true),
  METADATA_MIGRATION("Metadata Version Migration", false, true),
  PURGE_ORDER("Purge order", true, true),
  PURGE_SUBMISSION_TEMP_FILES("Purge Submission Temporary Files", true, false),
  REINDEX_ALL("Re-index on all storage", false, true),
  REINDEX("Re-index on main storage", false, true),
  RELOAD("Reload", false, false),
  REPLICATION_CHECK("Replication Check", true, true),
  REPLICATION("Replication", true, true),
  SIMPLE_CLEAN_SUBMISSION("Simple Clean Submission", true, true);
  // @formatter:on

  public static List<String> getJobType() {
    final List<String> list = new ArrayList<>();
    for (final JobType jt : JobType.values()) {
      list.add(jt.getLabel());
    }
    return list;
  }

  @Schema(description = "If the job has a report.")
  private Boolean hasReport;

  @Schema(description = "If the job is recurring.")
  private Boolean isRecurring;

  @Schema(description = "The label of the job.")
  private String label;

  JobType(String label, boolean isRecurring, boolean hasReport) {
    this.label = label;
    this.isRecurring = isRecurring;
    this.hasReport = hasReport;
  }

  public boolean getHasReport() {
    return this.hasReport;
  }

  public String getLabel() {
    return this.label;
  }

  public boolean isRecurring() {
    return this.isRecurring;
  }
}

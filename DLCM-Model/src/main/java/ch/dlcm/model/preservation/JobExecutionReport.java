/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - JobExecutionReport.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preservation;

import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;

@Entity
@Table(name = "preservationJobExecutionReport", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "jobExecutionId", "executionNumber" }) })
public class JobExecutionReport extends ResourceNormalized {

  private long executionNumber;

  private long ignoredItems = 0;

  private long inErrorItems = 0;

  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String jobExecutionId;

  @JsonIgnore
  @OneToMany(mappedBy = "reportId", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<JobExecutionReportLine> listReportLines = new ArrayList<>();

  private long processedItems = 0;

  public void addIgnoredItem() {
    this.setIgnoredItems(this.getIgnoredItems() + 1);
  }

  public void addInErrorItem() {
    this.setInErrorItems(this.getInErrorItems() + 1);
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.REPORT).withRel(DLCMActionName.REPORT));
  }

  public void addProcessedItem() {
    this.setProcessedItems(this.getProcessedItems() + 1);
  }

  public boolean addReportLine(JobExecutionReportLine t) {
    t.setReportId(this.getResId());
    return this.listReportLines.add(t);
  }

  public long getExecutionNumber() {
    return this.executionNumber;
  }

  public long getIgnoredItems() {
    return this.ignoredItems;
  }

  public long getInErrorItems() {
    return this.inErrorItems;
  }

  public String getJobExecutionId() {
    return this.jobExecutionId;
  }

  public List<JobExecutionReportLine> getListReportLines() {
    return this.listReportLines;
  }

  public long getProcessedItems() {
    return this.processedItems;
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public long getTotalItems() {
    return this.getProcessedItems() + this.getIgnoredItems() + this.getInErrorItems();
  }

  @Override
  public void init() {
  }

  @Override
  public String managedBy() {
    return ModuleName.PRES_PLANNING;
  }

  public void setExecutionNumber(long executionNumber) {
    this.executionNumber = executionNumber;
  }

  public void setIgnoredItems(long ignoredItems) {
    this.ignoredItems = ignoredItems;
  }

  public void setInErrorItems(long inErrorItems) {
    this.inErrorItems = inErrorItems;
  }

  public void setJobExecutionId(String jobExecutionId) {
    this.jobExecutionId = jobExecutionId;
  }

  public void setListReportLines(List<JobExecutionReportLine> listReportLines) {
    this.listReportLines = listReportLines;
  }

  public void setProcessedItems(long processedItems) {
    this.processedItems = processedItems;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof JobExecutionReport))
      return false;
    if (!super.equals(o))
      return false;
    JobExecutionReport that = (JobExecutionReport) o;
    return this.executionNumber == that.executionNumber &&
            this.ignoredItems == that.ignoredItems &&
            this.inErrorItems == that.inErrorItems &&
            this.processedItems == that.processedItems &&
            Objects.equals(this.jobExecutionId, that.jobExecutionId) &&
            Objects.equals(this.listReportLines, that.listReportLines);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.executionNumber, this.ignoredItems, this.inErrorItems, this.jobExecutionId, this.listReportLines,
            this.processedItems);
  }
}

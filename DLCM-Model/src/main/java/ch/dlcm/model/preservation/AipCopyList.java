/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - AipCopyList.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preservation;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ch.unige.solidify.rest.NoSqlResource;

public class AipCopyList extends NoSqlResource {

  private List<AipCopy> copies = new ArrayList<>();

  public AipCopyList() {
  }

  public AipCopyList(String resId) {
    this.setResId(resId);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AipCopyList)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    final AipCopyList that = (AipCopyList) o;
    return this.copies.equals(that.copies);
  }

  public List<AipCopy> getCopies() {
    return this.copies;
  }

  public OffsetDateTime getCreationDate() {
    OffsetDateTime date = null;
    for (final AipCopy aipCopy : this.copies) {
      if (aipCopy.getAip() != null && (date == null || aipCopy.getAip().getCreationTime().isBefore(date))) {
        date = aipCopy.getAip().getCreationTime();
      }
    }

    return date;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.copies);
  }

  public void setCopies(List<AipCopy> copies) {
    this.copies = copies;
  }

  @Override
  public boolean update(NoSqlResource item) {
    return false;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - JobScheduling.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preservation;

import java.util.Objects;

import jakarta.persistence.Embeddable;

@Embeddable
public class JobScheduling {

  private Integer hour;
  private Integer month; // 1 for January, 2 for February, 3 March, etc.
  private Integer monthDay;
  private Integer weekDay; // 1 for Monday, 2 for Tuesday, etc.

  public JobScheduling() {
  }

  public JobScheduling(Integer hour, Integer weekDay, Integer monthDay, Integer month) {
    this.hour = hour;
    this.weekDay = weekDay;
    this.monthDay = monthDay;
    this.month = month;
  }

  public Integer getHour() {
    return this.hour;
  }

  public Integer getMonth() {
    return this.month;
  }

  public Integer getMonthDay() {
    return this.monthDay;
  }

  public Integer getWeekDay() {
    return this.weekDay;
  }

  public void setHour(Integer hour) {
    this.hour = hour;
  }

  public void setMonth(Integer month) {
    this.month = month;
  }

  public void setMonthDay(Integer monthDay) {
    this.monthDay = monthDay;
  }

  public void setWeekDay(Integer weekDay) {
    this.weekDay = weekDay;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof JobScheduling))
      return false;
    JobScheduling that = (JobScheduling) o;
    return Objects.equals(this.hour, that.hour) &&
            Objects.equals(this.month, that.month) &&
            Objects.equals(this.monthDay, that.monthDay) &&
            Objects.equals(this.weekDay, that.weekDay);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.hour, this.month, this.monthDay, this.weekDay);
  }

}

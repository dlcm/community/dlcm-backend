/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - StoredAIP.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.preservation;

import java.net.URI;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.NoSqlResource;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.StoredArchiveInterface;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.oais.RepresentationInfo;

public class StoredAIP extends NoSqlResource implements StoredArchiveInterface {

  @NotNull
  @Size(min = 1)
  private String aipId;

  @NotNull
  private URI archiveUri;

  @NotNull
  @Size(min = 1)
  @Column(name = DLCMConstants.DB_ORG_UNIT_ID)
  private String organizationalUnitId;

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder) {
    super.addLinks(linkBuilder);
    this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof StoredAIP)) {
      return false;
    }
    if (!super.equals(other)) {
      return false;
    }
    final StoredAIP castOther = (StoredAIP) other;
    return Objects.equals(this.getAipId(), castOther.getAipId())
            && Objects.equals(this.getOrganizationalUnitId(), castOther.getOrganizationalUnitId())
            && Objects.equals(this.getArchiveUri(), castOther.getArchiveUri());
  }

  public String getAipId() {
    return this.aipId;
  }

  public String getArchiveId() {
    return this.organizationalUnitId + "/" + this.aipId;
  }

  @Override
  public URI getArchiveUri() {
    return this.archiveUri;
  }

  @Override
  public String getOrganizationalUnitId() {
    return this.organizationalUnitId;
  }

  @Override
  public String getResId() {
    return this.organizationalUnitId + SolidifyConstants.ID_CONCAT + this.aipId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.getAipId(), this.getOrganizationalUnitId(), this.getArchiveUri());
  }

  public void setArchiveUri(URI archiveId) {
    this.archiveUri = archiveId;
  }

  @Override
  public void setResId(String resId) {
    String sep;
    if (resId.contains("/")) {
      sep = "/";
    } else {
      sep = SolidifyConstants.ID_CONCAT;
    }
    this.aipId = resId.substring(resId.indexOf(sep) + 1);
    this.organizationalUnitId = resId.substring(0, resId.indexOf(sep));
  }

  @Override
  public boolean update(NoSqlResource item) {
    return false;
  }

  public ArchivalInfoPackage getAIP() {
    ArchivalInfoPackage aip = new ArchivalInfoPackage();
    aip.setResId(this.getAipId());
    RepresentationInfo info = new RepresentationInfo();
    info.setDataSensitivity(this.getDataSensitivity());
    info.setOrganizationalUnitId(this.getOrganizationalUnitId());
    aip.setInfo(info);
    aip.setArchiveContainer(this.getArchiveContainer());
    aip.setArchiveId(this.getArchiveUri().toString());
    return aip;
  }

  @Override
  public ArchiveContainer getArchiveContainer() {
    return ArchiveContainer.UNDEFINED;
  }

  @Override
  public DataTag getDataSensitivity() {
    return DataTag.UNDEFINED;
  }
}

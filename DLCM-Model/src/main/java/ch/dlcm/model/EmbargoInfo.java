/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - EmbargoInfo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;

import java.io.Serial;
import java.io.Serializable;
import java.time.OffsetDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.util.StringTool;

import ch.dlcm.deserializer.AccessDeserializer;

@Schema(description = "Embargo information.")
@Embeddable
public class EmbargoInfo implements Serializable {
  @Serial
  private static final long serialVersionUID = 897067055832480384L;

  @Schema(description = "The access level during the embargo.")
  @Column(name = "embargoAccess")
  @Enumerated(EnumType.STRING)
  @JsonDeserialize(using = AccessDeserializer.class)
  private Access access;

  @Schema(description = "The month number of the embargo duration.")
  @Column(name = "embargoMonths")
  private Integer months;

  @Schema(description = "The starting date of the embargo period.")
  @Column(name = "embargoStartDate", length = DB_DATE_LENGTH)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  private OffsetDateTime startDate;

  /***************/

  public EmbargoInfo() {

  }

  public EmbargoInfo(OffsetDateTime startDate, Integer months, Access access) {
    this.startDate = startDate;
    this.months = months;
    this.access = access;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final EmbargoInfo other = (EmbargoInfo) obj;
    if (this.access != other.access) {
      return false;
    }
    if (this.months == null) {
      if (other.months != null) {
        return false;
      }
    } else if (!this.months.equals(other.months)) {
      return false;
    }
    if (this.startDate == null) {
      return other.startDate == null;
    } else
      return this.startDate.equals(other.startDate);
  }

  public Access getAccess() {
    return this.access;
  }

  /***************/

  @Transient
  @JsonIgnore
  public OffsetDateTime getEndDate() {
    return this.startDate.plusMonths(this.months);
  }

  public Integer getMonths() {
    return this.months;
  }

  /***************/

  public OffsetDateTime getStartDate() {
    return this.startDate;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((this.access == null) ? 0 : this.access.hashCode());
    result = prime * result + ((this.months == null) ? 0 : this.months.hashCode());
    result = prime * result + ((this.startDate == null) ? 0 : this.startDate.hashCode());
    return result;
  }

  public void setAccess(Access access) {
    this.access = access;
  }

  public void setMonths(Integer months) {
    if (months != null && months == 0) {
      months = null;
    }
    this.months = months;
  }

  public void setStartDate(OffsetDateTime startDate) {
    this.startDate = startDate;
  }

  public boolean hasEmbargoStarted() {
    return this.hasEmbargoDefined() && this.getStartDate() != null;
  }

  public boolean hasEmbargoDefined() {
    return this.getMonths() != null && this.getMonths() > 0;
  }

  public OffsetDateTime calculateEndDate(OffsetDateTime creationTime) {
    return creationTime.plusMonths(this.months);
  }
}

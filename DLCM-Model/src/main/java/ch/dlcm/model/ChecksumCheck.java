/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ChecksumCheck.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.StringTool;

@Schema(description = "Checksum verification information.")
@Embeddable
public class ChecksumCheck {

  @Schema(description = "The execution date of checksum verification.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(name = "checksumCheckDate", length = DB_DATE_LENGTH)
  private OffsetDateTime checkDate;

  @Schema(description = "The execution result of checksum verification.")
  @Column(name = "checksumCheckingSucceed", length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean checkingSucceed;

  public ChecksumCheck() {
  }

  public ChecksumCheck(boolean checkingSucceed) {
    this.checkingSucceed = checkingSucceed;
    this.checkDate = OffsetDateTime.now(ZoneOffset.UTC);
  }

  public ChecksumCheck(OffsetDateTime checkDate, boolean checkingSucceed) {
    this.checkDate = checkDate;
    this.checkingSucceed = checkingSucceed;
  }

  public OffsetDateTime getCheckDate() {
    return this.checkDate;
  }

  public Boolean isCheckingSucceed() {
    return this.checkingSucceed;
  }

  public void setCheckDate(OffsetDateTime checkDate) {
    this.checkDate = checkDate;
  }

  public void setCheckingSucceed(Boolean checkingSucceed) {
    this.checkingSucceed = checkingSucceed;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - NotificationType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.notification;

import static ch.dlcm.model.notification.NotificationCategory.INFO;
import static ch.dlcm.model.notification.NotificationCategory.REQUEST;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.model.SolidifyApplicationRole;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.ModuleName;

@Schema(description = """
        List of notification type:
        - ACCESS_DATASET_REQUEST
        - APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST
        - APPROVE_DISPOSAL_REQUEST
        - COMPLETED_ARCHIVE_INFO
        - CREATED_DEPOSIT_INFO
        - IN_ERROR_AIP_INFO
        - IN_ERROR_DEPOSIT_INFO
        - IN_ERROR_DIP_INFO
        - IN_ERROR_DOWNLOADED_AIP_INFO
        - IN_ERROR_SIP_INFO
        - JOIN_ORGUNIT_REQUEST
        - MY_APPROVED_DEPOSIT_INFO
        - MY_COMPLETED_ARCHIVE_INFO
        - MY_COMPLETED_DEPOSIT_INFO
        - MY_INDIRECT_COMPLETED_DEPOSIT_INFO
        - VALIDATE_DEPOSIT_REQUEST
        """)
@Entity
public class NotificationType extends ResourceNormalized {
  public static final NotificationType ACCESS_DATASET_REQUEST = new NotificationType("ACCESS_DATASET_REQUEST", REQUEST, Role.STEWARD);
  public static final NotificationType JOIN_ORGUNIT_REQUEST = new NotificationType("JOIN_ORGUNIT_REQUEST", REQUEST, Role.MANAGER,
          true);
  public static final NotificationType CREATE_ORGUNIT_REQUEST = new NotificationType("CREATE_ORGUNIT_REQUEST", REQUEST,
          AuthApplicationRole.ADMIN, true);
  public static final NotificationType IN_ERROR_SIP_INFO = new NotificationType("IN_ERROR_SIP_INFO", INFO, AuthApplicationRole.ADMIN);
  public static final NotificationType IN_ERROR_AIP_INFO = new NotificationType("IN_ERROR_AIP_INFO", INFO, AuthApplicationRole.ADMIN);
  public static final NotificationType VALIDATE_DEPOSIT_REQUEST = new NotificationType("VALIDATE_DEPOSIT_REQUEST", REQUEST, Role.APPROVER);
  public static final NotificationType MY_APPROVED_DEPOSIT_INFO = new NotificationType("MY_APPROVED_DEPOSIT_INFO", INFO);
  public static final NotificationType MY_COMPLETED_DEPOSIT_INFO = new NotificationType("MY_COMPLETED_DEPOSIT_INFO", INFO);
  public static final NotificationType MY_INDIRECT_COMPLETED_DEPOSIT_INFO = new NotificationType("MY_INDIRECT_COMPLETED_DEPOSIT_INFO", INFO);

  public static final NotificationType CREATED_DEPOSIT_INFO = new NotificationType("CREATED_DEPOSIT_INFO", INFO, Role.STEWARD);
  public static final NotificationType IN_ERROR_DEPOSIT_INFO = new NotificationType("IN_ERROR_DEPOSIT_INFO", INFO, Role.STEWARD);
  public static final NotificationType IN_ERROR_DIP_INFO = new NotificationType("IN_ERROR_DIP_INFO", INFO, AuthApplicationRole.ADMIN);
  public static final NotificationType IN_ERROR_DOWNLOADED_AIP_INFO = new NotificationType("IN_ERROR_DOWNLOADED_AIP_INFO", INFO,
          AuthApplicationRole.ADMIN);
  public static final NotificationType APPROVE_DISPOSAL_REQUEST = new NotificationType("APPROVE_DISPOSAL_REQUEST", REQUEST,
          AuthApplicationRole.ADMIN);
  public static final NotificationType APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST = new NotificationType("APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST",
          REQUEST, Role.STEWARD);
  public static final NotificationType MY_COMPLETED_ARCHIVE_INFO = new NotificationType("MY_COMPLETED_ARCHIVE_INFO", INFO);
  public static final NotificationType COMPLETED_ARCHIVE_INFO = new NotificationType("COMPLETED_ARCHIVE_INFO", INFO, Role.APPROVER);

  @Schema(description = "The role to notify.")
  @ManyToOne
  private Role notifiedOrgUnitRole;

  @Schema(description = "If the insitution manage has to be notified.")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  @NotNull
  private Boolean notifiedInstitutionManager = false;

  @Schema(description = "The application role to notify.")
  @ManyToOne
  private SolidifyApplicationRole notifiedApplicationRole;

  @Schema(description = "The category of the nofication type.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private NotificationCategory notificationCategory;

  @Schema(description = "The list of users who subcrided to the notification.")
  @JsonIgnore
  @ManyToMany(mappedBy = "subscribedNotificationTypes")
  private List<Person> subscribers;

  public NotificationType() {
    // no-arg constructor for JPA
  }

  NotificationType(String resId, NotificationCategory notificationCategory) {
    this.setResId(resId);
    this.notificationCategory = notificationCategory;
  }

  NotificationType(String resId, NotificationCategory notificationCategory, Role notifiedOrgUnitRole) {
    this.setResId(resId);
    this.notificationCategory = notificationCategory;
    this.notifiedOrgUnitRole = notifiedOrgUnitRole;
  }

  NotificationType(String resId, NotificationCategory notificationCategory, Role notifiedOrgUnitRole,
          Boolean notifiedInstitutionManager) {
    this.setResId(resId);
    this.notificationCategory = notificationCategory;
    this.notifiedOrgUnitRole = notifiedOrgUnitRole;
    this.notifiedInstitutionManager = notifiedInstitutionManager;
  }

  NotificationType(String resId, NotificationCategory notificationCategory, ApplicationRole notifiedApplicationRole) {
    this.setResId(resId);
    this.notificationCategory = notificationCategory;
    this.notifiedApplicationRole = new SolidifyApplicationRole(notifiedApplicationRole);
  }

  NotificationType(String resId, NotificationCategory notificationCategory, ApplicationRole notifiedApplicationRole,
          Boolean notifiedInstitutionManager) {
    this.setResId(resId);
    this.notificationCategory = notificationCategory;
    this.notifiedApplicationRole = new SolidifyApplicationRole(notifiedApplicationRole);
    this.notifiedInstitutionManager = notifiedInstitutionManager;
  }

  public static List<NotificationType> getAllNotificationTypes() {
    return Arrays.asList(ACCESS_DATASET_REQUEST, JOIN_ORGUNIT_REQUEST, CREATE_ORGUNIT_REQUEST, IN_ERROR_SIP_INFO, IN_ERROR_AIP_INFO,
            VALIDATE_DEPOSIT_REQUEST, CREATED_DEPOSIT_INFO, IN_ERROR_DEPOSIT_INFO, IN_ERROR_DOWNLOADED_AIP_INFO, IN_ERROR_DIP_INFO,
            APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST, APPROVE_DISPOSAL_REQUEST, MY_APPROVED_DEPOSIT_INFO, MY_COMPLETED_DEPOSIT_INFO,
            MY_INDIRECT_COMPLETED_DEPOSIT_INFO, MY_COMPLETED_ARCHIVE_INFO, COMPLETED_ARCHIVE_INFO);
  }

  public void setNotificationCategory(NotificationCategory notificationCategory) {
    this.notificationCategory = notificationCategory;
  }

  public NotificationCategory getNotificationCategory() {
    return this.notificationCategory;
  }

  public Role getNotifiedOrgUnitRole() {
    return this.notifiedOrgUnitRole;
  }

  public Boolean getNotifiedInstitutionManager() {
    return this.notifiedInstitutionManager;
  }

  public ApplicationRole getNotifiedApplicationRole() {
    return this.notifiedApplicationRole;
  }

  public List<Person> getSubscribers() {
    return this.subscribers;
  }

  @Override
  public void init() {
    // Do nothing
  }

  public boolean addSubscriber(Person p) {
    final boolean result = this.subscribers.add(p);
    if (!p.getSubscribedNotifications().contains(this)) {
      p.addNotificationType(this);
    }
    return result;
  }

  public boolean removeSubscriber(Person p) {
    final boolean result = this.subscribers.remove(p);
    if (p.getSubscribedNotifications().contains(this)) {
      p.removeNotificationType(this);
    }
    return result;
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public String toString() {
    return this.getResId();
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof NotificationType))
      return false;
    final NotificationType that = (NotificationType) o;
    return this.getResId().equals(that.getResId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.notifiedOrgUnitRole, this.notifiedApplicationRole);
  }

}

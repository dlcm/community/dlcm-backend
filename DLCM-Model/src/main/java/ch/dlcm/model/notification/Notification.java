/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - Notification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.notification;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;

import java.time.OffsetDateTime;
import java.util.Objects;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.listener.HistoryListener;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.model.settings.SignedDuaFile;
import ch.dlcm.rest.ModuleName;

@Schema(description = "Notifications can be sent to organizational units managers in order to request access to closed archive, or to ask for membership.")
@Entity
@EntityListeners(HistoryListener.class)
public class Notification extends ResourceNormalized implements ResourceFileInterface {

  @Schema(description = "The signed DUA of the notification.")
  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = DLCMConstants.DB_DUA_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private SignedDuaFile signedDuaFile;

  @Schema(description = "The recipient (person) of the notification.")
  @ManyToOne
  private Person recipient;

  @Schema(description = "The emitter (user) of the notification.")
  @NotNull
  @ManyToOne
  private User emitter;

  @Schema(description = "The message sent by the notification.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_LARGE_STRING_LENGTH)
  private String message;

  @Schema(description = "Any message sent in response to the notification.")
  @Size(min = 1, max = SolidifyConstants.DB_LARGE_STRING_LENGTH)
  private String responseMessage;

  @Schema(description = "The status of the notification.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private NotificationStatus notificationStatus;

  @Schema(description = "The mark (read/unread) of the notification.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private NotificationMark notificationMark;

  @Schema(description = "The type of the notification.")
  @NotNull
  @ManyToOne
  private NotificationType notificationType;

  @Schema(description = "The notified organizational unit.")
  @ManyToOne
  private OrganizationalUnit notifiedOrgUnit;

  @Schema(description = "The target object identifer of the notification.")
  private String objectId;

  @Schema(description = "The message related to the status of the notification.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String statusMessage;

  @Schema(description = "The date and the time when the notification was sent.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH)
  private OffsetDateTime sentTime;

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Notification)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    final Notification that = (Notification) o;
    return this.notificationType == that.notificationType &&
            this.notificationStatus == that.notificationStatus &&
            this.notificationMark == that.notificationMark &&
            Objects.equals(this.emitter, that.emitter) &&
            Objects.equals(this.notifiedOrgUnit, that.notifiedOrgUnit) &&
            Objects.equals(this.message, that.message) &&
            Objects.equals(this.responseMessage, that.responseMessage) &&
            Objects.equals(this.recipient, that.recipient) &&
            Objects.equals(this.signedDuaFile, that.signedDuaFile);
  }

  public User getEmitter() {
    return this.emitter;
  }

  public String getMessage() {
    return this.message;
  }

  public String getResponseMessage() {
    return this.responseMessage;
  }

  public NotificationStatus getNotificationStatus() {
    return this.notificationStatus;
  }

  public NotificationMark getNotificationMark() {
    return this.notificationMark;
  }

  public NotificationType getNotificationType() {
    return this.notificationType;
  }

  public OrganizationalUnit getNotifiedOrgUnit() {
    return this.notifiedOrgUnit;
  }

  public OffsetDateTime getSentTime() {
    return this.sentTime;
  }

  public Person getRecipient() {
    return this.recipient;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public String getObjectId() {
    return this.objectId;
  }

  public SignedDuaFile getSignedDuaFile() {
    return this.signedDuaFile;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.notificationType, this.notificationStatus, this.notificationMark, this.emitter,
            this.notifiedOrgUnit, this.message, this.responseMessage, this.signedDuaFile);
  }

  @Override
  public void init() {
    if (this.notificationStatus == null) {
      this.notificationStatus = NotificationStatus.PENDING;
      this.statusMessage = null;
    }
    if (this.notificationMark == null) {
      this.notificationMark = NotificationMark.UNREAD;
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public void setEmitter(User emitter) {
    this.emitter = emitter;
  }

  public void setRecipient(Person recipient) {
    this.recipient = recipient;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void setResponseMessage(String responseMessage) {
    this.responseMessage = responseMessage;
  }

  public void setNotificationStatus(NotificationStatus notificationStatus) {
    this.notificationStatus = notificationStatus;
  }

  public void setNotificationMark(NotificationMark notificationMark) {
    this.notificationMark = notificationMark;
  }

  public void setNotificationType(NotificationType notificationType) {
    this.notificationType = notificationType;
  }

  public void setNotifiedOrgUnit(OrganizationalUnit notifiedOrgUnit) {
    this.notifiedOrgUnit = notifiedOrgUnit;
  }

  public void setStatusMessage(String message) {
    this.statusMessage = StringTool.truncateWithEllipsis(message, SolidifyConstants.DB_LONG_STRING_LENGTH);
  }

  public void setSignedDuaFile(SignedDuaFile duaFile) {
    this.signedDuaFile = duaFile;
  }

  public void setObjectId(String objectId) {
    this.objectId = objectId;
  }

  public void setSentTime(OffsetDateTime sentTime) {
    this.sentTime = sentTime;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
    }
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    this.setSignedDuaFile(new SignedDuaFile());
    return this.getSignedDuaFile();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getSignedDuaFile();
  }

  @Override
  public void setResourceFile(ResourceFile resourceFile) {
    this.setSignedDuaFile((SignedDuaFile) resourceFile);
  }

  @Override
  @JsonIgnore
  public boolean isModifiable() {
    return (this.getNotificationStatus() != NotificationStatus.REFUSED && this.getNotificationStatus() != NotificationStatus.APPROVED);
  }
}

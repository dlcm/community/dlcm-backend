/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - RepresentationInfo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.oais;

import static ch.dlcm.DLCMRestFields.METADATA_VERSION_FIELD;
import static ch.unige.solidify.SolidifyConstants.DB_DEFAULT_STRING_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_LONG_STRING_LENGTH;
import static ch.unige.solidify.SolidifyConstants.RES_ID_FIELD;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Embedded;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.EmbeddableEntity;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMDocConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.EmbargoInfo;
import ch.dlcm.model.PackageStatus;

@Schema(description = "OAIS Information Package: SIP, AIP or DIP.")
@Embeddable
public class RepresentationInfo implements EmbeddableEntity {

  @Schema(description = "The final access level of the package.")
  @Enumerated(EnumType.STRING)
  private Access access;

  @Schema(description = "The data sensitivity of the package.")
  @Enumerated(EnumType.STRING)
  private DataTag dataSensitivity;

  @Schema(description = "The data use policy type of the package.")
  @Enumerated(EnumType.STRING)
  private DataUsePolicy dataUsePolicy;

  @Schema(description = "The compliance level of the package.")
  private ComplianceLevel complianceLevel;

  @Schema(description = "The description of the package.")
  @Size(max = DB_LONG_STRING_LENGTH)
  private String description;

  @Embedded
  @Valid
  private EmbargoInfo embargo = new EmbargoInfo();

  @Schema(description = "The license identifier of the package.")
  private String licenseId;

  @Schema(description = "The metadata version of the package.")
  @NotNull
  private DLCMMetadataVersion metadataVersion;

  @Schema(description = "The name of the package.")
  @NotNull
  @Size(min = 1, max = DB_DEFAULT_STRING_LENGTH)
  private String name;

  @Schema(description = "The organizational Unit ID of the package.")
  @NotNull
  @Size(min = 1, max = DB_ID_LENGTH)
  @Column(name = DLCMConstants.DB_ORG_UNIT_ID, length = DB_ID_LENGTH)
  private String organizationalUnitId;

  @Schema(description = "The status of the package.")
  @Enumerated(EnumType.STRING)
  private PackageStatus status;

  @Schema(description = "The detailed message related to the package status.")
  @Size(max = DB_LONG_STRING_LENGTH)
  private String statusMessage;

  @Schema(description = DLCMDocConstants.ARCHIVE_UPDATED_METADATE)
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean containsUpdatedMetadata = false;

  @Schema(description = DLCMDocConstants.ARCHIVE_PUBLIC_STRUCTURE)
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean contentStructurePublic = false;

  public RepresentationInfo() {
    // Do nothing
  }

  public RepresentationInfo(RepresentationInfo info) {
    this.access = info.access;
    this.dataSensitivity = info.dataSensitivity;
    this.complianceLevel = info.complianceLevel;
    this.description = info.description;
    this.embargo = info.embargo;
    this.licenseId = info.licenseId;
    this.metadataVersion = info.metadataVersion;
    this.name = info.name;
    this.organizationalUnitId = info.organizationalUnitId;
    this.status = info.status;
    this.statusMessage = info.statusMessage;
    this.containsUpdatedMetadata = info.containsUpdatedMetadata;
    this.dataUsePolicy = info.dataUsePolicy;
    this.contentStructurePublic = info.contentStructurePublic;
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof RepresentationInfo)) {
      return false;
    }
    final RepresentationInfo castOther = (RepresentationInfo) other;
    return Objects.equals(this.status, castOther.status) && Objects.equals(this.name, castOther.name) && Objects
            .equals(this.description, castOther.description);
  }

  public Access getAccess() {
    return this.access;
  }

  public DataTag getDataSensitivity() {
    return this.dataSensitivity;
  }

  public ComplianceLevel getComplianceLevel() {
    return this.complianceLevel;
  }

  public boolean hasEmbargo() {
    return (this.embargo != null && this.embargo.hasEmbargoStarted());
  }

  @Schema(description = "The current access level of the package, deducted from the final access level and an eventual embargo.", accessMode = AccessMode.READ_ONLY)
  public Access getCurrentAccess() {

    if (!this.hasEmbargo()) {

      /*
       * No embargo --> current Access is final Access
       */
      return this.getAccess();

    } else {

      /*
       * Check if the embargo is over
       */
      final OffsetDateTime endOfEmbargoDate = this.getEmbargo().getEndDate();

      if (endOfEmbargoDate.isBefore(OffsetDateTime.now())) {

        /*
         * Embargo is over --> current Access is final Access
         */
        return this.getAccess();

      } else {

        /*
         * Embargo is still running --> current Access is embargo access
         */

        return this.embargo.getAccess();
      }
    }
  }

  public String getDescription() {
    return this.description;
  }

  public EmbargoInfo getEmbargo() {
    return this.embargo;
  }

  public String getLicenseId() {
    return this.licenseId;
  }

  public DLCMMetadataVersion getMetadataVersion() {
    return this.metadataVersion;
  }

  public String getName() {
    return this.name;
  }

  public String getOrganizationalUnitId() {
    return this.organizationalUnitId;
  }

  public DataUsePolicy getDataUsePolicy() {
    return this.dataUsePolicy;
  }

  public Boolean getContentStructurePublic() {
    return Objects.requireNonNullElse(this.contentStructurePublic, false);
  }

  /*******************************/

  public PackageStatus getStatus() {
    return this.status;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public boolean getContainsUpdatedMetadata() {
    return this.containsUpdatedMetadata;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.status, this.name, this.description);
  }

  /*******************************/

  public void init() {
    if (this.status == null) {
      this.status = PackageStatus.IN_PREPARATION;
    }
    if (this.metadataVersion == null) {
      this.metadataVersion = DLCMMetadataVersion.getDefaultVersion();
    }
  }

  @Schema(description = "If the current access level is public")
  public boolean isAccessCurrentlyPublic() {
    return this.getCurrentAccess() == Access.PUBLIC;
  }

  public void setAccess(Access access) {
    this.access = access;
  }

  public void setDataSensitivity(DataTag dataSensitivity) {
    this.dataSensitivity = dataSensitivity;
  }

  public void setComplianceLevel(ComplianceLevel complianceLevel) {
    this.complianceLevel = complianceLevel;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setEmbargo(EmbargoInfo embargo) {
    this.embargo = embargo;
  }

  public void setLicenseId(String licenseId) {
    this.licenseId = licenseId;
  }

  public void setMetadataVersion(DLCMMetadataVersion metadataVersion) {
    this.metadataVersion = metadataVersion;
  }

  public void setDataUsePolicy(DataUsePolicy dataUsePolicy) {
    this.dataUsePolicy = dataUsePolicy;
  }

  public void setContentStructurePublic(Boolean structureContentPublic) {
    this.contentStructurePublic = structureContentPublic;
  }

  /*******************************/

  public void setName(String name) {
    this.name = name;
  }

  public void setOrganizationalUnitId(String organizationalUnitId) {
    this.organizationalUnitId = organizationalUnitId;
  }

  public void setStatus(PackageStatus status) {
    this.status = status;
  }

  public void setStatusMessage(String message) {
    this.statusMessage = StringTool.truncateWithEllipsis(message, DB_LONG_STRING_LENGTH);
  }

  public void setStatusWithMessage(PackageStatus status, String message) {
    this.setStatus(status);
    this.setStatusMessage(message);
  }

  public void setContainsUpdatedMetadata(boolean containsUpdatedMetadata) {
    this.containsUpdatedMetadata = containsUpdatedMetadata;
  }

  @Override
  public List<String> getNonUpdatableFields() {
    return Arrays.asList(RES_ID_FIELD, METADATA_VERSION_FIELD);
  }

  public Map<String, Object> toUpdateMap() {
    Map<String, Object> infoMap = new HashMap<>();
    infoMap.put("name", this.getName());
    infoMap.put("description", this.getDescription());
    infoMap.put("access", this.getAccess());
    if (this.getEmbargo() == null) {
      infoMap.put("embargo", null);
    } else {
      Map<String, Object> embargoMap = new HashMap<>();
      embargoMap.put("access", this.getEmbargo().getAccess());
      embargoMap.put("months", this.getEmbargo().getMonths());
      embargoMap.put("startDate", this.getEmbargo().getStartDate());
      infoMap.put("embargo", embargoMap);
    }
    infoMap.put("dataSensitivity", this.getDataSensitivity());
    infoMap.put("licenseId", this.getLicenseId());
    infoMap.put("duaType", this.getDataUsePolicy());
    infoMap.put("contentStructurePublic", this.getContentStructurePublic());
    return infoMap;
  }
}

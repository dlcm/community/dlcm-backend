/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - InfoPackageInterface.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.oais;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileOwnerInterface;
import ch.dlcm.model.PackageStatus;

public interface InfoPackageInterface<V extends AbstractDataFile<?, ?>> extends DataFileOwnerInterface<V> {

  RepresentationInfo getInfo();

  String getResId();

  boolean isReady();

  @JsonIgnore
  default boolean isValid() {
    for (AbstractDataFile<?, ?> df : this.getDataFiles()) {
      if (df.getDataCategory() != DataCategory.Package ||
              (df.getDataType() != DataCategory.InformationPackage && df.getDataType() != DataCategory.UpdatedMetadata)) {
        return false;
      }
    }
    return true;
  }

  boolean isIndexable();

  @JsonIgnore
  default boolean isInProgress() {
    return PackageStatus.isInProgress(this.getInfo().getStatus());
  }

  @JsonIgnore
  default boolean isCompleted() {
    return PackageStatus.isCompletedProcess(this.getInfo().getStatus());
  }

  @JsonIgnore
  default boolean isInError() {
    return PackageStatus.isInError(this.getInfo().getStatus());
  }

  default void setPackageStatus(PackageStatus status) {
    this.getInfo().setStatus(status);
    this.getInfo().setStatusMessage(null);
  }

  default void setPackageStatusWithMessage(PackageStatus status, String statusMessage) {
    this.getInfo().setStatusWithMessage(status, statusMessage);
  }

  @Override
  default boolean checkDataFileToAdd(DataCategory category, DataCategory type) {
    if (category != DataCategory.Package) {
      return false;
    } else if ((this.getInfo().getStatus() == PackageStatus.IN_PREPARATION
            || this.getInfo().getStatus() == PackageStatus.PACKAGE_REPLICATION_PENDING
            || this.getInfo().getStatus() == PackageStatus.TOMBSTONE_REPLICATION_PENDING)
            && type == DataCategory.InformationPackage) {
      // Package in creation or in replication
      return true;
    } else if ((this.getInfo().getStatus() == PackageStatus.EDITING_METADATA
            || this.getInfo().getStatus() == PackageStatus.METADATA_EDITION_PENDING)
            && type == DataCategory.UpdatePackage) {
      // Package in metadata edition
      return true;
    }
    return false;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - SubmissionInfoPackage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.oais;

import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMDocConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.listener.HistoryListener;
import ch.dlcm.listener.OrganizationalUnitDataListener;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.policies.PreservationPolicyInterface;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Schema(description = "Submission Information Package (SIP)")
@Entity
@Table(name = "sip")
@EntityListeners({ HistoryListener.class, OrganizationalUnitDataListener.class })
public class SubmissionInfoPackage extends ResourceNormalized implements RemoteResourceContainer, InfoPackageInterface<SipDataFile>,
        PreservationPolicyInterface, OrganizationalUnitAwareResource, MetadataVersionAware {

  @Schema(description = DLCMDocConstants.PACKAGE_DISPOSAL_APPROVAL)
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean dispositionApproval;

  @Schema(description = DLCMDocConstants.PACKAGE_RETENTION)
  // Default => 10 years; 0 => forever
  private Integer retention;

  @Schema(description = "The main storage indice in configuration if the SIP. O means default storage (first one).")
  // 0 => defaut storage (first one) ; x => indice of storage in configuration
  private Integer mainStorage;

  @JsonIgnore
  private URI aip;

  @Schema(description = "The generated AIP identifier of the SIP.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String aipId;

  @ElementCollection
  @Column(name = DLCMConstants.DB_AIP_ID, length = DB_ID_LENGTH)
  @CollectionTable(name = "sipAip", joinColumns = {
          @JoinColumn(name = DLCMConstants.DB_COLLECTION_ID, referencedColumnName = DLCMConstants.DB_RES_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = {
                  DLCMConstants.DB_COLLECTION_ID, DLCMConstants.DB_AIP_ID }))
  @JsonIgnore
  private List<String> collection = new ArrayList<>();

  @JsonIgnore
  @OneToMany(mappedBy = "infoPackage", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<SipDataFile> dataFiles = new ArrayList<>();

  @Schema(description = "The source deposit identifier of the SIP.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String depositId;

  @Schema(description = "The OAIS representation information of the SIP.")
  @NotNull
  @Embedded
  @Valid
  private RepresentationInfo info = new RepresentationInfo();

  @Schema(description = "The organizational unit object of the SIP.")
  @Transient
  private OrganizationalUnit organizationalUnit;

  @Schema(description = "The submission policy object of the SIP.")
  @Transient
  private SubmissionPolicy submissionPolicy;

  @Schema(description = "The submission policy identifier of the SIP.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String submissionPolicyId;

  @Schema(description = "The publication date of the deposit.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate publicationDate;

  @Override
  public <T> boolean addItem(T t) {
    if (t instanceof SipDataFile sipDataFile) {
      return this.addDataFile(sipDataFile);
    }
    if (t instanceof ArchivalInfoPackage aipPackage) {
      return this.addAip(aipPackage);
    }
    return false;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.DATAFILE).withRel(ResourceName.DATAFILE));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.AIP).withRel(ResourceName.AIP));
      if (this.getInfo().getStatus() == PackageStatus.COMPLETED) {
        this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.AIP_FILE).withRel(ResourceName.AIP_FILE));
        this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.CLEAN).withRel(DLCMActionName.CLEAN));
      }
      if (this.getInfo().getStatus() != PackageStatus.IN_PREPARATION) {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
      }
      if (this.getInfo().getStatus() != PackageStatus.COMPLETED && this.getInfo().getStatus() != PackageStatus.CLEANING
              && this.getInfo().getStatus() != PackageStatus.CLEANED) {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.UPLOAD).withRel(ActionName.UPLOAD));
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.RESUME).withRel(ActionName.RESUME));
      }
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
    }
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof SubmissionInfoPackage)) {
      return false;
    }
    final SubmissionInfoPackage castOther = (SubmissionInfoPackage) other;
    return Objects.equals(this.info, castOther.info) && Objects.equals(this.dataFiles, castOther.dataFiles) && Objects
            .equals(this.depositId, castOther.depositId) && Objects.equals(this.aipId, castOther.aipId)
            && Objects.equals(this.aip, castOther.aip);
  }

  public URI getAip() {
    return this.aip;
  }

  public String getAipId() {
    return this.aipId;
  }

  @JsonIgnore
  public List<String> getCollection() {
    return this.collection;
  }

  @Schema(description = DLCMDocConstants.AIP_COLLECTION_SIZE, accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public int getCollectionSize() {
    return this.getCollection().size();
  }

  @Schema(description = "The number of SIP data files.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  public int getDataFileNumber() {
    return this.getDataFiles().size();
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<SipDataFile> getDataFiles() {
    return this.dataFiles;
  }

  public String getDepositId() {
    return this.depositId;
  }

  @Override
  public URI getDownloadUri() {
    if (this.getInfo().getStatus() == PackageStatus.IN_PREPARATION) {
      throw new SolidifyRuntimeException("Package not available");
    }
    if (this.getDataFiles().isEmpty()) {
      throw new SolidifyRuntimeException("Package not accessible");
    }
    return this.getDataFiles().get(0).getFinalData();
  }

  @Override
  @JsonIgnore
  public List<Class<? extends Resource>> getEmbeddedResourceTypes() {
    final List<Class<? extends Resource>> embeddedResources = new ArrayList<>();

    embeddedResources.add(OrganizationalUnit.class);
    embeddedResources.add(SubmissionPolicy.class);

    return embeddedResources;
  }

  @Override
  public RepresentationInfo getInfo() {
    return this.info;
  }

  public LocalDate getPublicationDate() {
    return this.publicationDate;
  }

  /*******/

  public OrganizationalUnit getOrganizationalUnit() {
    return this.organizationalUnit;
  }

  @Schema(description = "The organizational unit identifier of the SIP.")
  @Override
  public String getOrganizationalUnitId() {
    return this.getInfo().getOrganizationalUnitId();
  }

  /*******/

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public SubmissionPolicy getSubmissionPolicy() {
    if (this.getSubmissionPolicyId() == null) {
      if (this.getOrganizationalUnit() != null) {
        return this.getOrganizationalUnit().getDefaultSubmissionPolicy();
      } else {
        return null;
      }
    }
    return this.submissionPolicy;
  }

  public String getSubmissionPolicyId() {
    return this.submissionPolicyId;
  }

  @Override
  public <T> List<String> getSubResourceIds(Class<T> type) {

    List<String> subResourceIds = Collections.emptyList();

    if (type == ArchivalInfoPackage.class && this.getCollection() != null) {
      subResourceIds = this.getCollection();
    } else if (type == SubmissionPolicy.class && this.getSubmissionPolicyId() != null) {
      subResourceIds = CollectionTool.initList(this.getSubmissionPolicyId());
    } else if (type == OrganizationalUnit.class && this.getOrganizationalUnitId() != null) {
      subResourceIds = CollectionTool.initList(this.getOrganizationalUnitId());
    } else if (type == null) {
      throw new IllegalArgumentException("type should not be null");
    } else if (!this.getEmbeddedResourceTypes().contains(type)) {
      throw new IllegalArgumentException("Class " + type.getCanonicalName() + " is not a subresource of SubmissionInfoPackage");
    }

    return subResourceIds;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.info, this.dataFiles, this.depositId, this.aipId, this.aip);
  }

  @Override
  @PrePersist
  public void init() {
    this.info.init();
    PreservationPolicyInterface.super.init();
    if (this.getMainStorage() == null) {
      this.setMainStorage(0);
    }
  }

  public boolean isCollection() {
    return (!this.collection.isEmpty());
  }

  @Override
  public boolean isDeletable() {
    return (this.getInfo().getStatus() == PackageStatus.COMPLETED
            || this.getInfo().getStatus() == PackageStatus.CLEANED
            || this.getInfo().getStatus() == PackageStatus.IN_ERROR);
  }

  @Override
  @JsonIgnore
  public boolean isIndexable() {
    return false;
  }

  @Override
  public boolean isModifiable() {
    return this.getInfo().getStatus() == PackageStatus.IN_PREPARATION
            || this.getInfo().getStatus() == PackageStatus.EDITING_METADATA
            || this.getInfo().getStatus() == PackageStatus.IN_ERROR;
  }

  @Schema(description = "If SIP package is ready.", accessMode = AccessMode.READ_ONLY)
  @Override
  @JsonProperty(access = Access.READ_ONLY)
  public boolean isReady() {
    if (this.getDataFiles().isEmpty() && this.getCollection().isEmpty()) {
      return false;
    }
    for (final SipDataFile df : this.getDataFiles()) {
      if (df.getStatus() != DataFileStatus.READY) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String managedBy() {
    return ModuleName.INGEST;
  }

  @Override
  public <T> boolean removeItem(T t) {
    if (t instanceof SipDataFile sipDataFile) {
      return this.removeDataFile(sipDataFile);
    } else if (t instanceof ArchivalInfoPackage aipPackage) {
      return this.removeAip(aipPackage);
    }
    return false;
  }

  public void setAip(URI aip) {
    this.aip = aip;
  }

  public void setAipId(String aipId) {
    this.aipId = aipId;
  }

  public void setDataFiles(List<SipDataFile> dataFiles) {
    this.dataFiles = dataFiles;
  }

  public void setDepositId(String depositId) {
    this.depositId = depositId;
  }

  public void setInfo(RepresentationInfo info) {
    this.info = info;
  }

  public void setOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    this.organizationalUnit = organizationalUnit;
  }

  public void setPublicationDate(LocalDate publicationDate) {
    this.publicationDate = publicationDate;
  }

  @JsonIgnore
  public void setSubmissionPolicy(SubmissionPolicy submissionPolicy) {
    this.submissionPolicy = submissionPolicy;
  }

  public void setSubmissionPolicyId(String submissionPolicyId) {
    this.submissionPolicyId = submissionPolicyId;
  }

  private boolean addAip(ArchivalInfoPackage t) {
    return this.collection.add(t.getResId());
  }

  public void setMainStorage(Integer mainStorage) {
    this.mainStorage = mainStorage;
  }

  @Override
  public boolean addDataFile(SipDataFile t) {
    return this.dataFiles.add(t);
  }

  private boolean removeAip(ArchivalInfoPackage t) {
    return this.collection.remove(t.getResId());
  }

  private boolean removeDataFile(SipDataFile t) {
    return this.dataFiles.remove(t);
  }

  @JsonIgnore
  @Override
  public DLCMMetadataVersion getMetadataVersion() {
    return this.info.getMetadataVersion();
  }

  @JsonIgnore
  public SipDataFile getInformationPackage() {
    final List<SipDataFile> informationPackageList = this.getDataFiles()
            .stream()
            .filter(df -> df.getDataCategory().equals(DataCategory.Package) && df.getDataType().equals(DataCategory.InformationPackage))
            .toList();
    final int nbInformationPackages = informationPackageList.size();
    if (nbInformationPackages == 0) {
      throw new NoSuchElementException("No information package present");
    } else if (nbInformationPackages > 1) {
      throw new SolidifyRuntimeException("There should be only one information package");
    } else {
      return informationPackageList.get(0);
    }
  }

  @Override
  public Boolean getDispositionApproval() {
    return this.dispositionApproval;
  }

  @Override
  public Integer getRetention() {
    return this.retention;
  }

  @Override
  public void setDispositionApproval(Boolean dispositionApproval) {
    this.dispositionApproval = dispositionApproval;
  }

  @Override
  public void setRetention(Integer retention) {
    this.retention = retention;
  }

  public Integer getMainStorage() {
    return this.mainStorage;
  }

}

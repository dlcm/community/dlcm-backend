/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchivalInfoPackage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.oais;

import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.net.URI;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.persistence.CascadeType;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMDocConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.listener.HistoryListener;
import ch.dlcm.listener.OrganizationalUnitDataListener;
import ch.dlcm.listener.PackageListener;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.ChecksumCheck;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.DataFileChecksum.ChecksumOrigin;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.FileFormat;
import ch.dlcm.model.FileInterface;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StoredArchiveInterface;
import ch.dlcm.model.VirusCheck;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.policies.PreservationPolicyInterface;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Schema(description = "Archival Information Package (AIP)")
@Entity
@Table(name = "aip")
@EntityListeners({ HistoryListener.class, PackageListener.class, OrganizationalUnitDataListener.class })
public class ArchivalInfoPackage extends ResourceNormalized
        implements RemoteResourceContainer, InfoPackageInterface<AipDataFile>, FileInterface, PreservationPolicyInterface,
        OrganizationalUnitAwareResource, MetadataVersionAware, StoredArchiveInterface {

  @Schema(description = DLCMDocConstants.PACKAGE_DISPOSAL_APPROVAL)
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean dispositionApproval;

  @Schema(description = DLCMDocConstants.PACKAGE_RETENTION)
  // Default => 10 years; 0 => forever
  private Integer retention;

  @Schema(description = "The container type of the AIP.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private ArchiveContainer archiveContainer;

  @Schema(description = "if the AIP is an unit (AIU) or a collection (AIC).")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean archivalUnit;

  @Schema(description = "The storage URI of the AIP.")
  private String archiveId;

  // Archive file number
  @Schema(description = "The total number of files in the AIP.")
  private Long archiveFileNumber;
  @Schema(description = "The number of updates in the AIP.")
  private Long updateNumber;
  @Schema(description = "The total number of filed in the collection AIP.")
  private Long collectionFileNumber;

  // Archive size
  @Schema(description = "The size in bytes of the AIP.")
  private Long archiveSize;
  @Schema(description = "The size in bytes of the tombstone AIP.")
  private Long tombstoneSize;
  @Schema(description = "The total size in bytes of the collection AIP.")
  private Long collectionArchiveSize;

  @Schema(description = "The last checksum verification information of the AIP.")
  @Embedded
  private ChecksumCheck checksumCheck = new ChecksumCheck();

  @Schema(description = "The checksum list of the AIP.")
  @ElementCollection
  @CollectionTable(name = "aipChecksum", joinColumns = {
          @JoinColumn(name = DLCMConstants.DB_AIP_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = { DLCMConstants.DB_AIP_ID,
                  "checksumAlgo", "checksumType", "checksumOrigin" }))
  private List<DataFileChecksum> checksums = new ArrayList<>();

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "aipAip", joinColumns = { @JoinColumn(name = DLCMConstants.DB_COLLECTION_ID) }, inverseJoinColumns = {
          @JoinColumn(name = DLCMConstants.DB_RES_ID) }, uniqueConstraints = {
                  @UniqueConstraint(columnNames = { DLCMConstants.DB_COLLECTION_ID, DLCMConstants.DB_RES_ID }) })
  private List<ArchivalInfoPackage> collection = new ArrayList<>();

  @JsonIgnore
  @ManyToMany(mappedBy = "collection")
  private List<ArchivalInfoPackage> collectionParents = new ArrayList<>();

  @JsonIgnore
  @OneToMany(mappedBy = "infoPackage", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<AipDataFile> dataFiles = new ArrayList<>();

  @Schema(description = "The file format identification of the AIP.")
  @Embedded
  private FileFormat fileFormat = new FileFormat();

  @Schema(description = "The OAIS representation information of the AIP.")
  @NotNull
  @Embedded
  @Valid
  private RepresentationInfo info = new RepresentationInfo();

  @Schema(description = "The source SIP list (IDs) of the AIP.")
  @ElementCollection
  @Column(name = DLCMConstants.DB_SIP_ID, length = DB_ID_LENGTH)
  @CollectionTable(name = "aipSip", joinColumns = {
          @JoinColumn(name = DLCMConstants.DB_PACKAGE_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = { DLCMConstants.DB_PACKAGE_ID,
                  DLCMConstants.DB_SIP_ID }))
  private List<String> sipIds = new ArrayList<>();

  @Schema(description = "The file virus check of the AIP.")
  @Embedded
  private VirusCheck virusCheck = new VirusCheck();

  @Transient
  @JsonIgnore
  private Path workingAip;

  @ManyToMany(mappedBy = "aipPackages")
  @JsonIgnore
  private List<Order> orders = new ArrayList<>();

  @ManyToMany(mappedBy = "aips")
  @JsonIgnore
  private List<DisseminationInfoPackage> dipPackages = new ArrayList<>();

  @Schema(description = "The publication date of the AIP.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate publicationDate;

  @Schema(description = "The last archiving process date of the AIP: the first archiving date or the last metadata edition.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @NotNull
  private OffsetDateTime lastArchiving;

  @Override
  public <T> boolean addItem(T t) {
    if (t instanceof AipDataFile aipDataFile) {
      return this.addDataFile(aipDataFile);
    }
    if (t instanceof ArchivalInfoPackage aip) {
      return this.addAIP(aip);
    }
    return false;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.DATAFILE).withRel(ResourceName.DATAFILE));
      if (this.getInfo().getStatus() != PackageStatus.COMPLETED && this.getInfo().getStatus() != PackageStatus.STORED) {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.UPLOAD).withRel(ActionName.UPLOAD));
      }
      if (this.isCollection()) {
        this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.AIP).withRel(ResourceName.AIP));
      }
      if (this.getInfo().getStatus() != PackageStatus.IN_PREPARATION) {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
      }
      if (this.getInfo().getStatus() != PackageStatus.COMPLETED) {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.RESUME).withRel(ActionName.RESUME));
      }
      if (this.getInfo().getStatus() == PackageStatus.COMPLETED
              || this.getInfo().getStatus() == PackageStatus.STORED
              || this.getInfo().getStatus() == PackageStatus.DISPOSABLE
              || this.getInfo().getStatus() == PackageStatus.DISPOSED) {
        this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.REINDEX).withRel(DLCMActionName.REINDEX));
      }
      if (this.getInfo().getStatus() == PackageStatus.COMPLETED || this.getInfo().getStatus() == PackageStatus.STORED) {
        this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.CHECK).withRel(DLCMActionName.CHECK));
        this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.CHECK_FIXITY).withRel(DLCMActionName.CHECK_FIXITY));
      }
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
    }
  }

  public void addLinksForAccess(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    this.add(linkBuilder.slash(this.getResId()).withSelfRel());
    if (!subResOnly) {
      this.add((Tool.referenceLink(linkBuilder.slash(this.getResId()).toUriComponentsBuilder(), ModuleName.ACCESS)).withSelfRel());
    }
    this.add(linkBuilder.withRel(ActionName.LIST));
    if (mainRes) {
      this.add((Tool.parentLink(linkBuilder.toUriComponentsBuilder())).withRel(ActionName.MODULE));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.DATAFILE).withRel(ResourceName.DATAFILE));
      if (this.getInfo().getStatus() == PackageStatus.COMPLETED) {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
      }
      if (this.isCollection()) {
        this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.AIP).withRel(ResourceName.AIP));
      }
      if (this.getInfo().getStatus() != PackageStatus.COMPLETED) {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.RESUME).withRel(ActionName.RESUME));
      }
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
    } else {
      this.add((Tool.parentLink(linkBuilder.toUriComponentsBuilder())).withRel(ActionName.PARENT));
    }
  }

  public void addSip(String sip) {
    if (this.sipIds == null) {
      this.sipIds = new ArrayList<>();
    }
    this.sipIds.add(sip);
  }

  @Override
  public boolean equals(final Object other) {
    if (other == null) {
      return false;
    }
    if (!(other instanceof ArchivalInfoPackage)) {
      return false;
    }
    if (!super.equals(other)) {
      return false;
    }

    final ArchivalInfoPackage castOther = (ArchivalInfoPackage) other;
    if (this.getInfo() != null ? !this.getInfo().equals(castOther.getInfo()) : castOther.getInfo() != null) {
      return false;
    }
    if (this.getArchivalUnit() != null ? !this.getArchivalUnit().equals(castOther.getArchivalUnit()) : castOther.getArchivalUnit() != null) {
      return false;
    }
    if (this.getFileSize() != castOther.getFileSize()) {
      return false;
    }
    if (this.getArchiveId() != null ? !this.getArchiveId().equals(castOther.getArchiveId()) : castOther.getArchiveId() != null) {
      return false;
    }
    if (this.getCollection() != null ? !this.getCollection().equals(castOther.getCollection()) : castOther.getCollection() != null) {
      return false;
    }
    if (this.getDataFiles() != null ? !this.getDataFiles().equals(castOther.getDataFiles()) : castOther.getDataFiles() != null) {
      return false;
    }
    if (this.getDataFiles() != null ? !this.getDataFiles().equals(castOther.getDataFiles()) : castOther.getDataFiles() != null) {
      return false;
    }
    if (this.getChecksums() != null ? !this.getChecksums().equals(castOther.getChecksums()) : castOther.getDataFiles() != null) {
      return false;
    }
    return this.getSipIds() != null ? this.getSipIds().equals(castOther.getSipIds()) : castOther.getSipIds() == null;
  }

  @Override
  public ArchiveContainer getArchiveContainer() {
    return this.archiveContainer;
  }

  @JsonIgnore
  public Map<String, Object> getAIPMetadata() {
    final Map<String, Object> aipMetadata = new HashMap<>();
    // Change info
    aipMetadata.put(DLCMConstants.AIP_CREATION, this.getCreation().getWhen().toString());
    aipMetadata.put(DLCMConstants.AIP_LAST_UPDATE, this.getLastUpdate().getWhen().toString());
    // Title
    aipMetadata.put(DLCMConstants.AIP_TITLE, this.getInfo().getName());
    // Compliance Level
    aipMetadata.put(DLCMConstants.AIP_COMPLIANCE_LEVEL, this.getInfo().getComplianceLevel().name());
    // Access Level
    aipMetadata.put(DLCMConstants.AIP_ACCESS_LEVEL, this.getInfo().getAccess().toString());
    // Embargo
    if (this.getInfo().hasEmbargo()) {
      aipMetadata.put(DLCMConstants.AIP_EMBARGO_LEVEL, this.getInfo().getEmbargo().getAccess().toString());
      aipMetadata.put(DLCMConstants.AIP_EMBARGO_END_DATE, this.getInfo().getEmbargo().getEndDate().toString());
    }
    // Data Tag
    aipMetadata.put(DLCMConstants.AIP_DATA_TAG, this.getInfo().getDataSensitivity().toString());
    // DUA Type
    aipMetadata.put(DLCMConstants.AIP_DATA_USE_POLICY, this.getInfo().getDataUsePolicy().toString());
    // AIP container: BagIt or Zip
    aipMetadata.put(DLCMConstants.AIP_CONTAINER, this.getArchiveContainer().toString());
    // AIP unit or collection
    aipMetadata.put(DLCMConstants.AIP_UNIT, !this.isCollection());
    // File number & size
    aipMetadata.put(DLCMConstants.AIP_FILE_NUMBER, this.getArchiveFileNumber());
    aipMetadata.put(DLCMConstants.AIP_METADATA_FILE_NUMBER, this.getUpdateNumber() + 1);
    if (this.isCollection()) {
      aipMetadata.put(DLCMConstants.AIP_TYPE, "collection");
      aipMetadata.put(DLCMConstants.AIP_TOTAL_FILE_NUMBER, this.getCollectionFileNumber());
      aipMetadata.put(DLCMConstants.AIP_SIZE, this.getCollectionArchiveSize());
    } else {
      aipMetadata.put(DLCMConstants.AIP_TYPE, "archive");
      aipMetadata.put(DLCMConstants.AIP_TOTAL_FILE_NUMBER, this.getArchiveFileNumber() - this.getUpdateNumber() - 1);
      aipMetadata.put(DLCMConstants.AIP_SIZE, this.getArchiveSize());
    }
    // OrgUnit
    aipMetadata.put(DLCMConstants.AIP_ORGA_UNIT, this.getInfo().getOrganizationalUnitId());
    // Retention
    aipMetadata.put(DLCMConstants.AIP_RETENTION, this.getRetention());
    aipMetadata.put(DLCMConstants.AIP_RETENTION_END, this.getRetentionEnd().toString());
    aipMetadata.put(DLCMConstants.AIP_RETENTION_DURATION, this.getSmartRetention());
    // Disposal process approval
    aipMetadata.put(DLCMConstants.AIP_DISPOSITION_APPROVAL, this.getDispositionApproval());
    // if disposed
    if (this.getTombstoneSize() != null && this.getTombstoneSize() > 0) {
      aipMetadata.put(DLCMConstants.AIP_TOMBSTONE_SIZE, this.getTombstoneSize());
    }
    aipMetadata.put(DLCMConstants.AIP_CONTENT_STRUCTURE_PUBLIC, this.getInfo().getContentStructurePublic());
    return aipMetadata;
  }

  public Boolean getArchivalUnit() {
    return this.archivalUnit;
  }

  public Long getArchiveFileNumber() {
    return this.archiveFileNumber;
  }

  public Long getUpdateNumber() {
    return this.updateNumber;
  }

  public Long getCollectionFileNumber() {
    return this.collectionFileNumber;
  }

  public String getArchiveId() {
    return this.archiveId;
  }

  public Long getArchiveSize() {
    return this.archiveSize;
  }

  public Long getTombstoneSize() {
    return this.tombstoneSize;
  }

  public Long getCollectionArchiveSize() {
    return this.collectionArchiveSize;
  }

  public ChecksumCheck getChecksumCheck() {
    return this.checksumCheck;
  }

  public List<DataFileChecksum> getChecksums() {
    return this.checksums;
  }

  public List<DataFileChecksum> getChecksums(ChecksumOrigin checksumOrigin) {
    return this.getChecksums().stream()
            .filter(c -> c.getChecksumOrigin().equals(checksumOrigin))
            .toList();
  }

  public List<ArchivalInfoPackage> getCollection() {
    return this.collection;
  }

  public List<ArchivalInfoPackage> getCollectionParents() {
    return this.collectionParents;
  }

  @JsonIgnore
  public List<String> getCollectionIds() {
    return this.collection.stream()
            .map(ArchivalInfoPackage::getResId)
            .toList();
  }

  @Schema(description = DLCMDocConstants.AIP_COLLECTION_SIZE, accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public int getCollectionSize() {
    return this.getCollection().size();
  }

  @Schema(description = "The number of AIP data files.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  public int getDataFileNumber() {
    return this.getDataFiles().size();
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<AipDataFile> getDataFiles() {
    return this.dataFiles;
  }

  @Override
  public URI getDownloadUri() {
    if (this.getInfo().getStatus() == PackageStatus.IN_PREPARATION) {
      throw new SolidifyRuntimeException("Package not available");
    }
    if (this.getDataFiles().isEmpty()) {
      throw new SolidifyRuntimeException("Package not accessible");
    }
    if (this.getInfo().getStatus() == PackageStatus.STORED || this.getInfo().getStatus() == PackageStatus.INDEXING
            || this.getInfo().getStatus() == PackageStatus.COMPLETED) {
      return URI.create(this.getArchiveId());
    }
    return this.getDataFiles().get(0).getFinalData();
  }

  @JsonIgnore
  public URI getDownloadUriForAccess() {
    if (this.getInfo().getStatus() == PackageStatus.IN_PREPARATION) {
      throw new SolidifyRuntimeException("Package not available");
    }
    if (this.getDataFiles().isEmpty() || !this.isReady()) {
      throw new SolidifyRuntimeException("Package not accessible");
    }
    return this.getDataFiles().get(0).getFinalData();
  }

  @Override
  public FileFormat getFileFormat() {
    return this.fileFormat;
  }

  @Override
  @JsonIgnore
  public Long getFileSize() {
    return this.getArchiveSize();
  }

  @Override
  public RepresentationInfo getInfo() {
    return this.info;
  }

  public LocalDate getPublicationDate() {
    return this.publicationDate;
  }

  public OffsetDateTime getLastArchiving() {
    return this.lastArchiving;
  }

  @Override
  @JsonIgnore
  public Path getPath() {
    return this.workingAip;
  }

  @Schema(description = "The end of the AIP retention.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  public OffsetDateTime getRetentionEnd() {
    if (this.getRetention() == null || this.getCreation() == null || this.getCreation().getWhen() == null) {
      return null;
    }
    return this.getPublicationDate().atStartOfDay().atOffset(ZoneOffset.UTC).plusDays(this.getRetention());
  }

  public List<String> getSipIds() {
    return this.sipIds;
  }

  @JsonIgnore
  public FileFormat getSmartFileFormat() {
    if (this.getInfo().getStatus() != PackageStatus.COMPLETED && this.getInfo().getStatus() != PackageStatus.EDITING_METADATA) {
      throw new SolidifyProcessingException("Package not in COMPLETED nor EDITING_METADATA status");
    }
    if (this.fileFormat != null) {
      return this.fileFormat;
    }
    if (!this.getDataFiles().isEmpty() && this.getDataFiles().get(0).getFileFormat() != null) {
      return this.getDataFiles().get(0).getFileFormat();
    }

    // If no FileFormat has been found above, and the type is InformationPackage, we assume it is a ZIP
    if (!this.getDataFiles().isEmpty() && this.getDataFiles().get(0).getDataCategory() == DataCategory.Package
            && this.getDataFiles().get(0).getDataType() == DataCategory.InformationPackage) {
      final FileFormat format = new FileFormat();
      format.setContentType(SolidifyConstants.ZIP_MIME_TYPE);
      return format;
    }

    throw new SolidifyProcessingException("Package not ready: no file format found");
  }

  @Schema(description = "The size in human-readable format of the AIP.", accessMode = AccessMode.READ_ONLY)
  @Override
  @JsonProperty(access = Access.READ_ONLY)
  public String getSmartSize() {
    if (this.archiveSize == null) {
      return DLCMConstants.UNKNOWN;
    }
    return StringTool.formatSmartSize(this.archiveSize);
  }

  @Schema(description = "The size in human-readable format of the tombstone AIP.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  public String getSmartTombstoneSize() {
    if (this.tombstoneSize == null) {
      return DLCMConstants.UNKNOWN;
    }
    return StringTool.formatSmartSize(this.tombstoneSize);
  }

  @Schema(description = "The total size in human-readable format of the collection AIP.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  public String getSmartCollectionArchiveSize() {
    if (this.collectionArchiveSize == null) {
      return DLCMConstants.UNKNOWN;
    }
    return StringTool.formatSmartSize(this.collectionArchiveSize);
  }

  @Override
  public VirusCheck getVirusCheck() {
    return this.virusCheck;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + (this.getInfo() != null ? this.getInfo().hashCode() : 0);
    result = 31 * result + (this.getArchivalUnit() != null ? this.getArchivalUnit().hashCode() : 0);
    result = 31 * result + (this.getArchiveId() != null ? this.getArchiveId().hashCode() : 0);
    result = 31 * result + (this.getFileSize().hashCode());
    result = 31 * result + (this.getCollection() != null ? this.getCollection().hashCode() : 0);
    result = 31 * result + (this.getDataFiles() != null ? this.getDataFiles().hashCode() : 0);
    result = 31 * result + (this.getSipIds() != null ? this.getSipIds().hashCode() : 0);
    return result;
  }

  @Override
  @PrePersist
  public void init() {
    this.info.init();

    PreservationPolicyInterface.super.init();

    if (this.archivalUnit == null) {
      this.archivalUnit = true;
    }
    if (this.archiveFileNumber == null) {
      this.archiveFileNumber = 0L;
    }
    if (this.updateNumber == null) {
      this.updateNumber = 0L;
    }
    if (this.collectionFileNumber == null) {
      this.collectionFileNumber = 0L;
    }
    if (this.archiveSize == null) {
      this.archiveSize = 0L;
    }
    if (this.tombstoneSize == null) {
      this.tombstoneSize = 0L;
    }
    if (this.collectionArchiveSize == null) {
      this.collectionArchiveSize = 0L;
    }
  }

  @JsonIgnore
  public boolean isAlreadyStored() {
    // Check if AIP already stored
    return (!StringTool.isNullOrEmpty(this.getArchiveId())
            && this.getArchiveSize() != null
            && this.getArchiveSize() > 0);
  }

  @JsonIgnore
  public boolean isAlreadyDisposed() {
    // Check if AIP already disposed
    return (!StringTool.isNullOrEmpty(this.getArchiveId())
            && this.getTombstoneSize() != null
            && this.getTombstoneSize() > 0);
  }

  @JsonIgnore
  public boolean isTombstone() {
    // Check if AIP already disposed
    return (this.getTombstoneSize() != null && this.getTombstoneSize() > 0);
  }

  @JsonIgnore
  public boolean isCollection() {
    if (this.archivalUnit == null) {
      return false;
    }
    return (!this.archivalUnit);
  }

  @Override
  public boolean isDeletable() {
    // Check if already disposed
    if (this.getInfo().getStatus() == PackageStatus.DISPOSED
            || (this.isInProgress() && !this.isDisposalInProgress())) {
      return false;
    }
    // Check retention
    if (this.getRetention() == 0) {
      // Keep Forever
      throw new SolidifyUndeletableException("The AIP package is archived forever");
    } else if (this.getRetentionEnd().isAfter(OffsetDateTime.now(ZoneOffset.UTC))) {
      // In retention period: creation + retention > today
      throw new SolidifyUndeletableException("The AIP package is not ready for disposition");
    }
    // Check collection
    if (this.isCollection() && !this.isCollectionDisposed()) {
      // Collection content not disposed
      throw new SolidifyUndeletableException("The AIP collection content is not disposed");
    }
    return true;
  }

  @Override
  @JsonIgnore
  public boolean isIndexable() {
    if (this.isReloaded()) {
      return true;
    }
    if (this.isCollection()) {
      return this.isCollectionIndexable();
    } else {
      return this.isUnitReady();
    }
  }

  @Override
  public boolean isModifiable() {
    return this.getInfo().getStatus() == PackageStatus.IN_PREPARATION
            || this.getInfo().getStatus() == PackageStatus.METADATA_EDITION_PENDING
            || this.getInfo().getStatus() == PackageStatus.PACKAGE_REPLICATION_PENDING
            || this.getInfo().getStatus() == PackageStatus.TOMBSTONE_REPLICATION_PENDING
            || this.getInfo().getStatus() == PackageStatus.IN_ERROR;
  }

  @Schema(description = "If AIP package is ready.", accessMode = AccessMode.READ_ONLY)
  @Override
  @JsonProperty(access = Access.READ_ONLY)
  public boolean isReady() {
    if (this.isReloaded()) {
      return true;
    }
    if (this.isCollection()) {
      return this.isCollectionReady();
    } else {
      return this.isUnitReady();
    }
  }

  @JsonIgnore
  public boolean isReloaded() {
    return (this.getArchiveContainer() == ArchiveContainer.UNDEFINED && !StringTool.isNullOrEmpty(this.getArchiveId()));
  }

  @JsonIgnore
  public boolean isDisposalInProgress() {
    return (this.getInfo().getStatus() == PackageStatus.DISPOSABLE
            || this.getInfo().getStatus() == PackageStatus.DISPOSAL_APPROVED_BY_ORGUNIT
            || this.getInfo().getStatus() == PackageStatus.DISPOSAL_APPROVED);
  }

  @Override
  public String managedBy() {
    return ModuleName.ARCHIVALSTORAGE;
  }

  @Override
  public <T> boolean removeItem(T t) {
    if (t instanceof AipDataFile aipDataFile) {
      return this.removeDataFile(aipDataFile);
    }
    if (t instanceof ArchivalInfoPackage aip) {
      return this.removeAIP(aip);
    }
    return false;
  }

  public void reset() {
    this.setPackageStatus(PackageStatus.IN_PROGRESS);
    this.fileFormat = new FileFormat();
    this.virusCheck = new VirusCheck();
    this.getChecksums().clear();
    this.archiveId = null;
    this.archiveSize = 0L;
    this.archiveFileNumber = 0L;
    this.updateNumber = 0L;
    this.tombstoneSize = 0L;
  }

  public void resetForReplication() {
    this.setPackageStatus(PackageStatus.IN_PROGRESS);
    this.fileFormat = new FileFormat();
    this.virusCheck = new VirusCheck();
    this.getChecksums().clear();
    this.archiveId = null;
  }

  public void setArchiveContainer(ArchiveContainer archiveContainer) {
    this.archiveContainer = archiveContainer;
  }

  public void setArchivalUnit(Boolean archivalUnit) {
    this.archivalUnit = archivalUnit;
  }

  public void setArchiveFileNumber(Long archiveFileNumber) {
    this.archiveFileNumber = archiveFileNumber;
  }

  public void setUpdateNumber(Long updateNumber) {
    this.updateNumber = updateNumber;
  }

  public void incrementUpdateNumber() {
    this.updateNumber += 1;
  }

  public void setCollectionFileNumber(Long collectionFileNumber) {
    this.collectionFileNumber = collectionFileNumber;
  }

  public void setCollectionArchiveSize(Long collectionArchiveSize) {
    this.collectionArchiveSize = collectionArchiveSize;
  }

  public void setArchiveId(String archiveId) {
    this.archiveId = archiveId;
  }

  public void setArchiveSize(Long archiveSize) {
    this.archiveSize = archiveSize;
  }

  public void setTombstoneSize(Long tombstoneSize) {
    this.tombstoneSize = tombstoneSize;
  }

  public void setChecksumCheck(ChecksumCheck checksumCheck) {
    this.checksumCheck = checksumCheck;
  }

  public void setChecksums(List<DataFileChecksum> checksums) {
    this.checksums.addAll(checksums);
  }

  public void setCollection(List<ArchivalInfoPackage> collection) {
    this.collection = collection;
  }

  @Override
  public void setComplianceLevel(ComplianceLevel complianceLevel) {
    this.getInfo().setComplianceLevel(complianceLevel);
  }

  public void setDataFiles(List<AipDataFile> dataFiles) {
    this.dataFiles = dataFiles;
  }

  @Override
  public void setFileFormat(FileFormat fileFormat) {
    this.fileFormat = fileFormat;
  }

  @Override
  @JsonIgnore
  public void setFileSize(Long size) {
    this.setArchiveSize(size);
  }

  public void setInfo(RepresentationInfo info) {
    this.info = info;
  }

  @Override
  public void setPath(Path workingAip) {
    this.workingAip = workingAip;
  }

  public void setSipIds(List<String> sipIds) {
    this.sipIds = sipIds;
  }

  @Override
  public void setVirusCheck(VirusCheck virusCheck) {
    this.virusCheck = virusCheck;
  }

  public void setPublicationDate(LocalDate publicationDate) {
    this.publicationDate = publicationDate;
  }

  public void setLastArchiving(OffsetDateTime lastArchiving) {
    this.lastArchiving = lastArchiving;
  }

  private boolean addAIP(ArchivalInfoPackage t) {
    return this.getCollection().add(t);
  }

  @Override
  public boolean addDataFile(AipDataFile t) {
    return this.getDataFiles().add(t);
  }

  private boolean isCollectionIndexable() {
    if (this.getCollection().isEmpty()) {
      return false;
    }
    for (final ArchivalInfoPackage aip : this.getCollection()) {
      if (aip.getInfo().getStatus() != PackageStatus.COMPLETED && aip.getInfo().getStatus() != PackageStatus.REINDEXING
              && aip.getInfo().getStatus() != PackageStatus.STORED && aip.getInfo().getStatus() != PackageStatus.INDEXING) {
        return false;
      }
    }
    return true;
  }

  private boolean isCollectionReady() {
    if (this.getCollection().isEmpty()) {
      return false;
    }
    for (final ArchivalInfoPackage aip : this.getCollection()) {
      if (aip.getInfo().getStatus() != PackageStatus.COMPLETED) {
        return false;
      }
    }
    return true;
  }

  private boolean isCollectionDisposed() {
    if (this.getCollection().isEmpty()) {
      return false;
    }
    for (final ArchivalInfoPackage aip : this.getCollection()) {
      if (aip.getInfo().getStatus() != PackageStatus.DISPOSED) {
        return false;
      }
    }
    return true;
  }

  private boolean isUnitReady() {
    if (this.getInfo().getStatus() == PackageStatus.COMPLETED) {
      return true;
    }
    for (final AipDataFile df : this.getDataFiles()) {
      if (df.getStatus() != DataFileStatus.READY) {
        return false;
      }
    }
    return true;
  }

  private boolean removeAIP(ArchivalInfoPackage t) {
    return this.getCollection().remove(t);
  }

  private boolean removeDataFile(AipDataFile t) {
    return this.getDataFiles().remove(t);
  }

  @JsonIgnore
  @Override
  public String getOrganizationalUnitId() {
    return this.getInfo().getOrganizationalUnitId();
  }

  public List<Order> getOrders() {
    return this.orders;
  }

  public List<DisseminationInfoPackage> getDipPackages() {
    return this.dipPackages;
  }

  @JsonIgnore
  @Override
  public DLCMMetadataVersion getMetadataVersion() {
    return this.info.getMetadataVersion();
  }

  @Override
  public Boolean getDispositionApproval() {
    return this.dispositionApproval;
  }

  @Override
  public Integer getRetention() {
    return this.retention;
  }

  @Override
  public void setDispositionApproval(Boolean dispositionApproval) {
    this.dispositionApproval = dispositionApproval;
  }

  @Override
  public void setRetention(Integer retention) {
    this.retention = retention;
  }

  @JsonIgnore
  @Override
  public DataTag getDataSensitivity() {
    return this.getInfo().getDataSensitivity();
  }

  @JsonIgnore
  @Override
  public URI getArchiveUri() {
    if (StringTool.isNullOrEmpty(this.getArchiveId())) {
      return null;
    }
    return URI.create(this.getArchiveId());
  }

}

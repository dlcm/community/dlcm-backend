/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DipDataFile.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.oais;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;

import ch.dlcm.DLCMConstants;
import ch.dlcm.listener.DataFileListener;
import ch.dlcm.listener.HistoryListener;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.rest.ModuleName;

@Entity
@EntityListeners({ HistoryListener.class, DataFileListener.class })
public class DipDataFile extends AbstractDataFile<DisseminationInfoPackage, DipDataFile> {

  @ElementCollection
  @CollectionTable(name = "dipDataFileChecksum", joinColumns = {
          @JoinColumn(name = DLCMConstants.DB_FILE_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = { DLCMConstants.DB_FILE_ID,
                  "checksumAlgo", "checksumType",
                  "checksumOrigin" }))
  private List<DataFileChecksum> checksums = new ArrayList<>();

  @NotNull
  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_PACKAGE_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private DisseminationInfoPackage infoPackage;

  public DipDataFile() {
    super();
  }

  public DipDataFile(DisseminationInfoPackage dip) {
    super();
    this.infoPackage = dip;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (this.isAvailable()) {
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
    }
    if (this.getStatus() != DataFileStatus.READY) {
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.RESUME).withRel(ActionName.RESUME));
    }
    this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
  }

  @PreUpdate
  public void checkUpdate() {
    this.check();
  }

  @Override
  public List<DataFileChecksum> getChecksums() {
    return this.checksums;
  }

  @Override
  public URI getDownloadUri() {
    if (!this.isAvailable()) {
      throw new SolidifyRuntimeException("Data file not available");
    }
    if (this.getFinalData() == null) {
      throw new SolidifyRuntimeException("Data file not accessible");
    }
    return this.getFinalData();
  }

  @Override
  public String getContentType() {
    if (!this.isAvailable()) {
      throw new SolidifyRuntimeException("Data file not available");
    }
    if (this.getFileFormat() == null) {
      throw new SolidifyRuntimeException("Data file format not accessible");
    }
    return this.getFileFormat().getContentType();
  }

  @Override
  public String managedBy() {
    return ModuleName.ACCESS;
  }

  public void setChecksums(List<DataFileChecksum> checksums) {
    this.checksums = checksums;
  }

  @Override
  public DisseminationInfoPackage getInfoPackage() {
    return this.infoPackage;
  }

  @Override
  public void setInfoPackage(DisseminationInfoPackage infoPackage) {
    this.infoPackage = infoPackage;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.checksums, this.infoPackage);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    DipDataFile other = (DipDataFile) obj;
    return Objects.equals(this.checksums, other.checksums) && Objects.equals(this.infoPackage, other.infoPackage);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DisseminationInfoPackage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.oais;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.listener.HistoryListener;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.access.Order;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Schema(description = "Dissemination Information Package (DIP)")
@Entity
@Table(name = "dip")
@EntityListeners({ HistoryListener.class })
public class DisseminationInfoPackage extends ResourceNormalized implements RemoteResourceContainer, InfoPackageInterface<DipDataFile>,
        OrganizationalUnitAwareResource, MetadataVersionAware {

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "dipAip", joinColumns = { @JoinColumn(name = DLCMConstants.DB_PACKAGE_ID) }, inverseJoinColumns = {
          @JoinColumn(name = DLCMConstants.DB_AIP_ID) }, uniqueConstraints = {
                  @UniqueConstraint(columnNames = { DLCMConstants.DB_PACKAGE_ID, DLCMConstants.DB_AIP_ID }) })
  private List<ArchivalInfoPackage> aips = new ArrayList<>();

  @JsonIgnore
  @OneToMany(mappedBy = "infoPackage", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<DipDataFile> dataFiles = new ArrayList<>();

  @Schema(description = "The OAIS representation information of the DIP.")
  @NotNull
  @Embedded
  @Valid
  private RepresentationInfo info = new RepresentationInfo();

  @ManyToMany(mappedBy = "dipPackages")
  @JsonIgnore
  private List<Order> orders;

  @Override
  public <T> boolean addItem(T t) {
    if (t instanceof DipDataFile df) {
      return this.addDataFile(df);
    }
    if (t instanceof ArchivalInfoPackage aip) {
      return this.addAIP(aip);
    }
    return false;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.DATAFILE).withRel(ResourceName.DATAFILE));
      if (this.getInfo().getStatus() != PackageStatus.IN_PREPARATION) {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
      }
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.AIP).withRel(ResourceName.AIP));
    }
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof DisseminationInfoPackage)) {
      return false;
    }
    final DisseminationInfoPackage castOther = (DisseminationInfoPackage) other;
    return Objects.equals(this.info, castOther.info) && Objects.equals(this.dataFiles, castOther.dataFiles)
            && Objects.equals(this.aips, castOther.aips);
  }

  @Schema(description = "The source AIP identifier list of the DIP.")
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public List<String> getAipIds() {
    return this.aips.stream().map(ArchivalInfoPackage::getResId).toList();
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<DipDataFile> getDataFiles() {
    return this.dataFiles;
  }

  @Override
  public URI getDownloadUri() {
    if (this.getInfo().getStatus() == PackageStatus.IN_PREPARATION) {
      throw new SolidifyRuntimeException("Package not available");
    }
    if (this.getDataFiles().isEmpty()) {
      throw new SolidifyRuntimeException("Package not accessible");
    }
    return this.getDataFiles().get(0).getFinalData();
  }

  @Override
  public RepresentationInfo getInfo() {
    return this.info;
  }

  @Override
  @JsonIgnore
  public String getOrganizationalUnitId() {
    return this.info.getOrganizationalUnitId();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.info, this.dataFiles, this.aips);
  }

  @Override
  @PrePersist
  public void init() {
    this.info.init();
  }

  @Override
  public boolean isDeletable() {
    return this.getInfo().getStatus() == PackageStatus.COMPLETED;
  }

  @Override
  @JsonIgnore
  public boolean isIndexable() {
    return false;
  }

  @Override
  public boolean isModifiable() {
    return false;
  }

  @Schema(description = "If DIP package is ready.", accessMode = AccessMode.READ_ONLY)
  @Override
  public boolean isReady() {
    if (this.getDataFiles().isEmpty()) {
      return false;
    }
    for (final DipDataFile df : this.getDataFiles()) {
      if (df.getStatus() != DataFileStatus.READY) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String managedBy() {
    return ModuleName.ACCESS;
  }

  @Override
  public <T> boolean removeItem(T t) {
    if (t instanceof DipDataFile df) {
      return this.removeDataFile(df);
    }
    if (t instanceof ArchivalInfoPackage aip) {
      return this.removeAIP(aip);
    }
    return false;
  }

  public void setDataFiles(List<DipDataFile> dataFiles) {
    this.dataFiles = dataFiles;
  }

  public void setInfo(RepresentationInfo info) {
    this.info = info;
  }

  @Override
  public boolean addDataFile(DipDataFile t) {
    return this.dataFiles.add(t);
  }

  private boolean addAIP(ArchivalInfoPackage t) {
    return this.getAips().add(t);
  }

  private boolean removeDataFile(DipDataFile t) {
    return this.dataFiles.remove(t);
  }

  private boolean removeAIP(ArchivalInfoPackage t) {
    return this.getAips().remove(t);
  }

  public List<ArchivalInfoPackage> getAips() {
    return this.aips;
  }

  public void setAips(List<ArchivalInfoPackage> aips) {
    this.aips = aips;
  }

  public List<Order> getOrders() {
    return this.orders;
  }

  @JsonIgnore
  @Override
  public DLCMMetadataVersion getMetadataVersion() {
    return this.info.getMetadataVersion();
  }

  @Override
  public boolean checkDataFileToAdd(DataCategory category, DataCategory type) {
    return true;
  }
}

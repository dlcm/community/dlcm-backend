/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AipStatus.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.oais;

import java.time.OffsetDateTime;

import ch.dlcm.model.PackageStatus;

public class AipStatus {

  private OffsetDateTime creationTime;
  private String resId;
  private PackageStatus status;
  private OffsetDateTime updateTime;

  public AipStatus() {
    // This empty constructor is needed for Jackson deserialization
  }

  public AipStatus(ArchivalInfoPackage aip) {
    this.resId = aip.getResId();
    this.status = aip.getInfo().getStatus();
    this.creationTime = aip.getCreationTime();
    this.updateTime = aip.getUpdateTime();
  }

  public OffsetDateTime getCreationTime() {
    return this.creationTime;
  }

  public String getResId() {
    return this.resId;
  }

  public PackageStatus getStatus() {
    return this.status;
  }

  public OffsetDateTime getUpdateTime() {
    return this.updateTime;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - StatisticsInfo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import static ch.dlcm.DLCMRestFields.DOWNLOAD_NUMBER_FIELD;
import static ch.dlcm.DLCMRestFields.RES_ID_FIELD;
import static ch.dlcm.DLCMRestFields.VIEW_NUMBER_FIELD;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.Embeddable;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.rest.EmbeddableEntity;

@Schema(description = "Archive statistics information.")
@Embeddable
public class StatisticsInfo implements EmbeddableEntity {

  @Schema(description = "The view number of the archive.")
  private Long viewNumber;

  @Schema(description = "The download number of the archive.")
  private Long downloadNumber;

  public Long getViewNumber() {
    return this.viewNumber;
  }

  public void setViewNumber(Long viewNumber) {
    this.viewNumber = viewNumber;
  }

  public Long getDownloadNumber() {
    return this.downloadNumber;
  }

  public void setDownloadNumber(Long downloadNumber) {
    this.downloadNumber = downloadNumber;
  }

  public void init() {
    if (this.viewNumber == null) {
      this.viewNumber = 0L;
    }
    if (this.downloadNumber == null) {
      this.downloadNumber = 0L;
    }
  }

  @Override
  public List<String> getNonUpdatableFields() {
    return Arrays.asList(RES_ID_FIELD, VIEW_NUMBER_FIELD, DOWNLOAD_NUMBER_FIELD);
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof StatisticsInfo)) {
      return false;
    }
    if (!super.equals(other)) {
      return false;
    }
    final StatisticsInfo castOther = (StatisticsInfo) other;
    return Objects.equals(this.viewNumber, castOther.viewNumber) && Objects.equals(this.downloadNumber, castOther.downloadNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.viewNumber, this.downloadNumber);
  }

}

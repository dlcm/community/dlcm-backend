/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - CleanTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.tool;

import java.text.Normalizer;
import java.util.regex.Pattern;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.StringTool;

public class CleanTool {

  // Not supported as filename
  private static final Pattern UNWANTED_CHARACTERS = Pattern.compile("[*,:/?\"<>|]");
  private static final int MAX_TITLE_LENGTH = SolidifyConstants.DB_DEFAULT_STRING_LENGTH;

  /**
   * Method to remove accents, unwanted characters, truncate if longer than 100 characters
   *
   * @param value
   * @return
   */
  public static String cleanTitle(String value) {
    if (!StringTool.isNullOrEmpty(value)) {
      value = CleanTool.removeAccents(value);
      value = CleanTool.removeUnwantedCharacters(value);
      value = CleanTool.trim(value);
      value = CleanTool.replaceBlankCharsBySpace(value);
      value = StringTool.truncateOnSpaceWithEllipsis(value, MAX_TITLE_LENGTH, ".");
      value = CleanTool.removeFinalDot(value);
    }
    return value;
  }

  /**
   * Replace chars with accents by the char only.
   * Note: long hyphens for instance are also replaced by standard hyphens.
   *
   * @param value
   * @return
   */
  public static String removeAccents(String value) {
    if (value != null) {
      // Split accented chars into chars + accents
      value = Normalizer.normalize(value, Normalizer.Form.NFKD);
      // Remove accents, letting chars only
      value = value.replaceAll("\\p{M}", "");
    }
    return value;
  }

  public static String removeUnwantedCharacters(String value) {
    if (value != null) {
      value = UNWANTED_CHARACTERS.matcher(value).replaceAll("");
    }
    return value;
  }

  public static String removeFinalDot(String value) {
    if (value != null && value.endsWith(".")) {
      value = value.substring(0, value.length() - 1);
    }
    return value;
  }

  /**
   * Replaces \t \n \x0B \f \r chars by single spaces
   * <p>
   * Note: this method transforms a multilines text to single line text
   *
   * @param value
   * @return
   */
  public static String replaceBlankCharsBySpace(String value) {
    if (value != null) {
      value = value.replaceAll("\\s+", " ");
    }
    return value;
  }

  public static String trim(String value) {
    if (value != null) {
      value = value.trim();
    }
    return value;
  }

  private CleanTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}

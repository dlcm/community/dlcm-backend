/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - MetadataMigrationTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.tool;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;

public class MetadataMigrationTool {

  public static void migrateMetadataToDefaultVersion(DLCMMetadataVersion version, Path metadataXml) throws IOException {
    // Copy original metadata XML file
    final Path originalXml = metadataXml.getParent()
            .resolve(metadataXml.getFileName().toString()
                    .replaceFirst(SolidifyConstants.XML_EXT,
                            "-" + OffsetDateTime.now().format(DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT_FOR_FILE))
                                    + SolidifyConstants.XML_EXT));
    if (!FileTool.copyFile(metadataXml, originalXml)) {
      throw new SolidifyProcessingException("Cannot copy the original metadata file: " + metadataXml + " => " + originalXml);
    }
    // Migrate the metadata
    DLCMMetadataVersion fromVersion = version;
    do {
      final DLCMMetadataVersion toVersion = MetadataMigrationTool.getMigrationPath().get(fromVersion);
      MetadataMigrationTool.migrateMetadata(fromVersion, toVersion, metadataXml);
      fromVersion = toVersion;
    } while (fromVersion != DLCMMetadataVersion.getDefaultVersion());
  }

  public static void migrateMetadata(DLCMMetadataVersion fromVersion, DLCMMetadataVersion toVersion, Path metadataXml) throws IOException {
    final Path xsl = MetadataMigrationTool.getMigrationTransformation(fromVersion, toVersion);
    final Path migratedXml = XMLTool.getOutputFile(metadataXml);
    XMLTool.transform(metadataXml, xsl);
    XMLTool.wellformed(migratedXml);
    XMLTool.validate(MetadataMigrationTool.getDlcmSchema(toVersion), migratedXml);
    FileTool.moveFile(migratedXml, metadataXml);
  }

  public static Map<DLCMMetadataVersion, DLCMMetadataVersion> getMigrationPath() {
    final Map<DLCMMetadataVersion, DLCMMetadataVersion> migrationPath = new EnumMap<>(DLCMMetadataVersion.class);
    migrationPath.put(DLCMMetadataVersion.V1_0, DLCMMetadataVersion.V1_1);
    migrationPath.put(DLCMMetadataVersion.V1_1, DLCMMetadataVersion.V2_0);
    migrationPath.put(DLCMMetadataVersion.V2_0, DLCMMetadataVersion.V2_1);
    migrationPath.put(DLCMMetadataVersion.V2_1, DLCMMetadataVersion.V3_0);
    migrationPath.put(DLCMMetadataVersion.V3_0, DLCMMetadataVersion.V3_1);
    migrationPath.put(DLCMMetadataVersion.V3_1, DLCMMetadataVersion.V4_0);
    return migrationPath;
  }

  public static Path getMigrationTransformation(DLCMMetadataVersion fromVersion, DLCMMetadataVersion toVersion) throws IOException {
    final ClassPathResource xsl = new ClassPathResource(
            DLCMConstants.XSL_HOME + "/"
                    + DLCMConstants.METADATA_MIGRATION_XLS_PREFIX
                    + "-" + fromVersion.getVersion()
                    + "-" + toVersion.getVersion()
                    + SolidifyConstants.XSL_EXT);
    return xsl.getFile().toPath();
  }

  public static List<URL> getDlcmSchema(DLCMMetadataVersion version) throws IOException {
    final List<URL> list = new ArrayList<>();
    final ClassPathResource xsd = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + version.getMetsSchema());
    list.add(xsd.getURL());
    // Add FITS schema
    list.add(new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/fits_output.xsd").getURL());
    return list;
  }

  private MetadataMigrationTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}

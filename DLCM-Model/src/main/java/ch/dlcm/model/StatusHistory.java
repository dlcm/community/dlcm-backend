/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - StatusHistory.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.io.Serial;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;

@Entity
@Table(indexes = @Index(name = "status_history_res_id", columnList = "resId"))
public class StatusHistory implements Serializable {
  public static final String CREATED_STATUS = "CREATED";

  /**
   *
   */
  @Serial
  private static final long serialVersionUID = 481362136969468794L;

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH) // Used by MySQL5ExtendedDialect
  private OffsetDateTime changeTime;

  private String createdBy;

  @Transient
  private String creatorName;

  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @NotNull
  @JsonIgnore
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String resId;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @JsonIgnore
  private long seq;

  @NotNull
  private String status;

  @NotNull
  @JsonIgnore
  private String type;

  public StatusHistory() {
  }

  public StatusHistory(String type, String id, OffsetDateTime datetime, String createdBy) {
    this.type = type;
    this.resId = id;
    this.status = CREATED_STATUS;
    this.changeTime = datetime;
    this.createdBy = createdBy;
  }

  public StatusHistory(String type, String id, OffsetDateTime datetime, String status, String desc, String createdBy) {
    this.type = type;
    this.resId = id;
    this.status = status;
    this.changeTime = datetime;
    this.description = desc;
    this.createdBy = createdBy;
  }

  public StatusHistory(String type, String id, String status) {
    this.type = type;
    this.resId = id;
    this.status = status;
    this.changeTime = OffsetDateTime.now(ZoneOffset.UTC);
    this.createdBy = DLCMConstants.DLCM;
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof StatusHistory castOther)) {
      return false;
    }
    return Objects.equals(this.seq, castOther.seq) &&
            Objects.equals(this.resId, castOther.resId) &&
            Objects.equals(this.type, castOther.type) &&
            Objects.equals(this.changeTime, castOther.changeTime) &&
            Objects.equals(this.status, castOther.status) &&
            Objects.equals(this.description, castOther.description);
  }

  public OffsetDateTime getChangeTime() {
    return this.changeTime;
  }

  public String getCreatedBy() {
    return this.createdBy;
  }

  public String getCreatorName() {
    if (StringTool.isNullOrEmpty(this.creatorName)) {
      return this.getCreatedBy();
    } else {
      return this.creatorName;
    }
  }

  public String getDescription() {
    return this.description;
  }

  public String getResId() {
    return this.resId;
  }

  public long getSeq() {
    return this.seq;
  }

  public String getStatus() {
    return this.status;
  }

  public String getType() {
    return this.type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.seq, this.resId, this.type, this.changeTime, this.status, this.description);
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public void setCreatorName(String creatorName) {
    this.creatorName = creatorName;
  }

  public void setDescription(String description) {
    this.description = StringTool.truncateWithEllipsis(description, SolidifyConstants.DB_LONG_STRING_LENGTH);
  }

  @Override
  public String toString() {
    return "StatusHistory [resId=" + this.resId + ", type=" + this.type + ", changeTime=" + this.changeTime + ", status=" + this.status
            + ", description="
            + this.description + "]";
  }

}

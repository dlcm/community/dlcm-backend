/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DataFileChecksum.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.EmbeddableEntity;
import ch.unige.solidify.util.StringTool;

@Embeddable
public class DataFileChecksum implements EmbeddableEntity {
  public enum ChecksumOrigin {
    DLCM, DLCM_TOMBSTONE, USER, PORTAL
  }

  public enum ChecksumType {
    COMPLETE, PARTIAL
  }

  @NotNull
  private String checksum;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(length = SolidifyConstants.DB_SHORT_STRING_LENGTH)
  private ChecksumAlgorithm checksumAlgo;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(length = SolidifyConstants.DB_SHORT_STRING_LENGTH)
  private ChecksumOrigin checksumOrigin;

  @NotNull
  @Enumerated(EnumType.STRING)
  @Column(length = SolidifyConstants.DB_SHORT_STRING_LENGTH)
  private ChecksumType checksumType;

  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH) // Used by MySQL5ExtendedDialect
  private OffsetDateTime creationTime;

  @SuppressWarnings("squid:S2637")
  public DataFileChecksum() {
    this.checksumAlgo = ChecksumAlgorithm.MD5;
    this.checksumOrigin = ChecksumOrigin.DLCM;
    this.checksumType = ChecksumType.COMPLETE;
    this.creationTime = OffsetDateTime.now(ZoneOffset.UTC);
  }

  @SuppressWarnings("squid:S2637")
  public DataFileChecksum(ChecksumAlgorithm algo) {
    this.checksumAlgo = algo;
    this.checksumOrigin = ChecksumOrigin.DLCM;
    this.checksumType = ChecksumType.COMPLETE;
    this.creationTime = OffsetDateTime.now(ZoneOffset.UTC);
  }

  @SuppressWarnings("squid:S2637")
  public DataFileChecksum(ChecksumAlgorithm algo, ChecksumType type, ChecksumOrigin origin) {
    this.checksumAlgo = algo;
    this.checksumOrigin = origin;
    this.checksumType = type;
    this.creationTime = OffsetDateTime.now(ZoneOffset.UTC);
  }

  public DataFileChecksum(ChecksumAlgorithm algo, ChecksumType type, ChecksumOrigin origin, String checksum) {
    this.checksumAlgo = algo;
    this.checksumOrigin = origin;
    this.checksumType = type;
    this.checksum = checksum;
    this.creationTime = OffsetDateTime.now(ZoneOffset.UTC);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof DataFileChecksum)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }

    final DataFileChecksum that = (DataFileChecksum) o;

    if (this.getChecksumAlgo() != null ? !this.getChecksumAlgo().equals(that.getChecksumAlgo()) : that.getChecksumAlgo() != null) {
      return false;
    }
    if (this.getChecksumOrigin() != null ? !this.getChecksumOrigin().equals(that.getChecksumOrigin()) : that.getChecksumOrigin() != null) {
      return false;
    }
    if (this.getChecksumType() != null ? !this.getChecksumType().equals(that.getChecksumType()) : that.getChecksumType() != null) {
      return false;
    }
    if (this.getCreationTime() != null ? !this.getCreationTime().equals(that.getCreationTime()) : that.getCreationTime() != null) {
      return false;
    }
    return this.getChecksum() != null ? this.getChecksum().equals(that.getChecksum()) : that.getChecksum() == null;
  }

  public String getChecksum() {
    return this.checksum;
  }

  public ChecksumAlgorithm getChecksumAlgo() {
    return this.checksumAlgo;
  }

  public ChecksumOrigin getChecksumOrigin() {
    return this.checksumOrigin;
  }

  public ChecksumType getChecksumType() {
    return this.checksumType;
  }

  public OffsetDateTime getCreationTime() {
    return this.creationTime;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + (this.getChecksumAlgo() != null ? this.getChecksumAlgo().hashCode() : 0);
    result = 31 * result + (this.getChecksumOrigin() != null ? this.getChecksumOrigin().hashCode() : 0);
    result = 31 * result + (this.getChecksumType() != null ? this.getChecksumType().hashCode() : 0);
    result = 31 * result + (this.getCreationTime() != null ? this.getCreationTime().hashCode() : 0);
    result = 31 * result + (this.getChecksum() != null ? this.getChecksum().hashCode() : 0);
    return result;
  }

  public void init() {
    if (this.creationTime == null) {
      this.creationTime = OffsetDateTime.now(ZoneOffset.UTC);
    }
  }

  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  public void setChecksumAlgo(ChecksumAlgorithm checksumAlgo) {
    this.checksumAlgo = checksumAlgo;
  }

  public void setChecksumOrigin(ChecksumOrigin checksumOrigin) {
    this.checksumOrigin = checksumOrigin;
  }

  public void setChecksumType(ChecksumType checksumType) {
    this.checksumType = checksumType;
  }

  public void setCreationTime(OffsetDateTime creationTime) {
    this.creationTime = creationTime;
  }

}

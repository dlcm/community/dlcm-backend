/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - FileInfoUpdate.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

public class FileInfoUpdate {
  public enum FileAction {
    ADDED, UPDATED, DELETED
  }

  private FileAction action;

  private String fileType;
  private String fileNameForPreviousVersion;

  private DataCategory type;


  public FileInfoUpdate(String fileType, FileAction action, String fileNameForPreviousVersion, DataCategory type) {
    this.fileType = fileType;
    this.action = action;
    this.fileNameForPreviousVersion = fileNameForPreviousVersion;
    this.type = type;
  }

  public DataCategory getType() {
    return type;
  }

  public void setType(DataCategory type) {
    this.type = type;
  }
  public String getFileNameForPreviousVersion() {
    return fileNameForPreviousVersion;
  }

  public void setFileNameForPreviousVersion(String fileNameForPreviousVersion) {
    this.fileNameForPreviousVersion = fileNameForPreviousVersion;
  }

  public String getFileType() {
    return fileType;
  }

  public void setFileType(String fileType) {
    this.fileType = fileType;
  }

  public FileAction getAction() {
    return action;
  }

  public void setAction(FileAction action) {
    this.action = action;
  }

  public String toString() {
    return fileType + " " + action.toString();
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DataTag.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        DataTage represents the data sensitivity of the archive:
        - UNDEFINED => Not defined (Data sensitivity not set to support previous archives)
        - BLUE => Public (Non-confidential information, stored and shared freely)
        - GREEN => Controlled public (Not harmful personal information, shared with some access control)
        - YELLOW => Accountable (Potentially harmful personal information, shared with loosely verified and/or approved recipients)
        - ORANGE => More accountable (Sensitive personal information, shared with verified and/or approved recipients under agreement)
        - RED => Fully accountable (Very sensitive personal information, shared with strong verification of approved recipients under signed agreement)
        - CRIMSON => Maximum restricted (Maximum sensitive, explicit permission for each transaction, strong verification of approved recipients under signed agreement)
        """)
public enum DataTag {

  // @formatter:off
  UNDEFINED(0, "Undefined",Access.PUBLIC),
  BLUE(10, "Public", Access.PUBLIC),
  GREEN(20, "Controlled Public", Access.RESTRICTED),
  YELLOW(30, "Accountable", Access.RESTRICTED),
  ORANGE(40, "More Accountable", Access.CLOSED),
  RED(50, "Fully Accountable", Access.CLOSED),
  CRIMSON(60, "Maximally Restricted", Access.CLOSED);
  // @formatter:on

  @Schema(description = "The level of the data tag. The lowest value is less sensitive.")
  private final int level;
  @Schema(description = "The description of the data tag.")
  private final String description;
  @Schema(description = "The minimun level of access level compatible with the data tag.")
  private final Access minimunAccessLevel;

  DataTag(int level, String description, Access accessLevel) {
    this.level = level;
    this.description = description;
    this.minimunAccessLevel = accessLevel;
  }

  public int value() {
    return this.level;
  }

  public String getDescription() {
    return this.description;
  }

  public Access getMinimunAccessLevel() {
    return this.minimunAccessLevel;
  }

  public boolean isAccessLevelAllowed(Access accessLevelToCheck) {
    return switch (this.minimunAccessLevel) {
      case CLOSED -> accessLevelToCheck.equals(Access.CLOSED);
      case RESTRICTED -> (accessLevelToCheck.equals(Access.RESTRICTED) || accessLevelToCheck.equals(Access.CLOSED));
      case PUBLIC -> true;
    };
  }

}

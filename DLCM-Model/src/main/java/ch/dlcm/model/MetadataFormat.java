/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - MetadataFormat.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;

@Schema(description = """
        The metadata format:
        - JSON
        - XML
        - SCHEMA_LESS
        - CUSTOM
        """)
public enum MetadataFormat {

  //@formatter:off
  CUSTOM(SolidifyConstants.OCTET_STREAM_MIME_TYPE),
  JSON(SolidifyConstants.JSON_MIME_TYPE),
  SCHEMA_LESS(SolidifyConstants.OCTET_STREAM_MIME_TYPE),
  XML(SolidifyConstants.XML_MIME_TYPE);
 //@formatter:on

  @Schema(description = "The mime-type of the metadata format.")
  private final String mimeType;

  MetadataFormat(String mimeType) {
    this.mimeType = mimeType;
  }

  public String getMimeType() {
    return this.mimeType;
  }
}

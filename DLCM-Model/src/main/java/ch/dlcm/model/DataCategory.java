/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DataCategory.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("squid:S00115")
public enum DataCategory {

  // @formatter:off
  Primary(null),
    Observational(DataCategory.Primary),
    Experimental(DataCategory.Primary),
    Simulation(DataCategory.Primary),
    Derived(DataCategory.Primary),
    Reference(DataCategory.Primary),
    Digitalized(DataCategory.Primary),
  Secondary(null),
    Publication(DataCategory.Secondary),
    DataPaper(DataCategory.Secondary),
    Documentation(DataCategory.Secondary),
  Software(null),
    Code(DataCategory.Software),
    Binaries(DataCategory.Software),
    VirtualMachine(DataCategory.Software),
  Administrative(null),
    Document(DataCategory.Administrative),
    WebSite(DataCategory.Administrative),
    Other(DataCategory.Administrative),
  Package(null),
    InformationPackage(DataCategory.Package),
    UpdatePackage(DataCategory.Package),
    Metadata(DataCategory.Package),
    CustomMetadata(DataCategory.Package),
    UpdatedMetadata(DataCategory.Package),
  Internal(null),
    DatasetThumbnail(DataCategory.Internal), // deprecated
    ArchiveThumbnail(DataCategory.Internal),
    ArchiveReadme(DataCategory.Internal),
    DatafileThumbnail(DataCategory.Internal),
    ArchiveDataUseAgreement(DataCategory.Internal);
  // @formatter:on

  private final DataCategory category;

  DataCategory(DataCategory parent) {
    this.category = parent;
  }

  public static boolean check(DataCategory category, DataCategory subCategory) {
    return listCategories().contains(category) && listCategories(category).contains(subCategory);
  }

  public static List<DataCategory> listCategories() {
    return listCategories(null);
  }

  public static List<DataCategory> listSubCategories(String category) {
    return listCategories(DataCategory.valueOf(category));
  }

  public static List<DataCategory> listSubCategories(DataCategory category) {
    return listCategories(category);
  }

  private static List<DataCategory> listCategories(DataCategory parent) {
    final List<DataCategory> list = new ArrayList<>();
    for (final DataCategory dc : DataCategory.values()) {
      if (dc.category == parent) {
        list.add(dc);
      }
    }
    return list;
  }

}

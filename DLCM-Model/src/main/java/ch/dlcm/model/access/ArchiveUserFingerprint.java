/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveUserFingerprint.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.access;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.rest.ModuleName;

@Entity
public class ArchiveUserFingerprint extends ResourceNormalized {
  private String fingerprint;

  @NotNull
  @Enumerated(EnumType.STRING)
  private ArchiveUserFingerprint.FingerprintType type;

  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH) // Used by MySQL5ExtendedDialect
  private OffsetDateTime expiration;

  private String archiveId;

  public ArchiveUserFingerprint(String archiveId, String fingerprint, FingerprintType type) {
    this.setArchiveId(archiveId);
    this.setFingerprint(fingerprint);
    this.setType(type);
  }

  public ArchiveUserFingerprint() {
  }

  public enum FingerprintType {
    BACKEND_FINGERPRINT
  }

  @Override
  public void init() {
  }

  @Override
  public String managedBy() {
    return ModuleName.ACCESS;
  }

  public String getFingerprint() {
    return this.fingerprint;
  }

  public void setFingerprint(String fingerprint) {
    this.fingerprint = fingerprint;
  }

  public FingerprintType getType() {
    return this.type;
  }

  public void setType(FingerprintType type) {
    this.type = type;
  }

  public OffsetDateTime getExpiration() {
    return this.expiration;
  }

  public void setExpiration(int durationMinutes) {
    this.expiration = OffsetDateTime.now(ZoneOffset.UTC).plusMinutes(durationMinutes);
  }

  public String getArchiveId() {
    return this.archiveId;
  }

  public void setArchiveId(String archiveId) {
    this.archiveId = archiveId;
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public boolean isExpired() {
    if (this.getExpiration() == null) {
      return false;
    }
    return (this.getExpiration().isBefore(OffsetDateTime.now(ZoneOffset.UTC)));
  }
}

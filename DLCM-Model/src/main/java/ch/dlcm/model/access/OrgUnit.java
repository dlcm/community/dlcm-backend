/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrgUnit.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.access;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.OrganizationalUnitLogo;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

@Schema(description = "A organizational unit is a logical entity which could represent a research project or laboratory or all other organizational group of researchers. This entity contains public information of a organizational unit.")
public class OrgUnit extends NoSqlResource {

  @JsonIgnore
  private List<ArchiveMetadata> archives = new ArrayList<>();

  private ChangeInfo creation;

  @Schema(description = "The description of the organizational unit.")
  private String description;

  private ChangeInfo lastUpdate;

  @Schema(description = "The name of the organizational unit.")
  private String name;

  @Schema(description = "The URL of the organizational unit.")
  private URL url;

  @Schema(description = "The opening date of the organizational unit.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate openingDate;

  @Schema(description = "The closing date of the organizational unit.")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate closingDate;

  @Schema(description = "The logo of the organizational unit.")
  private OrganizationalUnitLogo logo;

  public OrgUnit() {
  }

  public OrgUnit(OrganizationalUnit orgUnit) {
    this.copyOrganizationalUnit(orgUnit);
  }

  public OrgUnit(OrganizationalUnit orgUnit, List<ArchiveMetadata> archives) {
    this.copyOrganizationalUnit(orgUnit);
    this.setArchives(archives);
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder) {
    super.addLinks(linkBuilder);
    this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.ARCHIVE).withRel(ResourceName.ARCHIVE));
    this.add(linkBuilder.slash(this.getResId()).slash(DLCMActionName.DOWNLOAD_LOGO).withRel(DLCMActionName.DOWNLOAD_LOGO));
  }

  public List<ArchiveMetadata> getArchives() {
    return this.archives;
  }

  public ChangeInfo getCreation() {
    return this.creation;
  }

  public String getDescription() {
    return this.description;
  }

  public ChangeInfo getLastUpdate() {
    return this.lastUpdate;
  }

  public String getName() {
    return this.name;
  }

  public URL getUrl() {
    return this.url;
  }

  public void setArchives(List<ArchiveMetadata> archives) {
    this.archives = archives;
  }

  public void setCreation(ChangeInfo creation) {
    this.creation = creation;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setLastUpdate(ChangeInfo lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public LocalDate getOpeningDate() {
    return this.openingDate;
  }

  public void setOpeningDate(LocalDate openingDate) {
    this.openingDate = openingDate;
  }

  public LocalDate getClosingDate() {
    return this.closingDate;
  }

  public void setClosingDate(LocalDate closingDate) {
    this.closingDate = closingDate;
  }

  public OrganizationalUnitLogo getLogo() {
    return this.logo;
  }

  public void setLogo(OrganizationalUnitLogo logo) {
    this.logo = logo;
  }

  @Override
  public boolean update(NoSqlResource item) {
    throw new UnsupportedOperationException();
  }

  private void copyOrganizationalUnit(OrganizationalUnit orgUnit) {
    this.setResId(orgUnit.getResId());
    this.setCreation(orgUnit.getCreation());
    this.setLastUpdate(orgUnit.getLastUpdate());
    this.setName(orgUnit.getName());
    this.setDescription(orgUnit.getDescription());
    this.setUrl(orgUnit.getUrl());
    this.setOpeningDate(orgUnit.getOpeningDate());
    this.setClosingDate(orgUnit.getClosingDate());
    this.setLogo(orgUnit.getLogo());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.archives, this.closingDate, this.creation, this.description, this.lastUpdate, this.logo, this.name, this.openingDate, this.url);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    OrgUnit other = (OrgUnit) obj;
    return Objects.equals(this.archives, other.archives) && Objects.equals(this.closingDate, other.closingDate) && Objects.equals(this.creation, other.creation)
            && Objects.equals(this.description, other.description) && Objects.equals(this.lastUpdate, other.lastUpdate) && Objects.equals(this.logo, other.logo)
            && Objects.equals(this.name, other.name) && Objects.equals(this.openingDate, other.openingDate) && Objects.equals(this.url, other.url);
  }

}

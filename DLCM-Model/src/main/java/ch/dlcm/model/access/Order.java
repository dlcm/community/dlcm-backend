/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - Order.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.access;

import static ch.dlcm.DLCMRestFields.AIP_PACKAGES_FIELD;
import static ch.dlcm.DLCMRestFields.DIP_PACKAGES_FIELD;
import static ch.dlcm.DLCMRestFields.METADATA_VERSION_FIELD;
import static ch.dlcm.DLCMRestFields.QUERY_TYPE_FIELD;
import static ch.dlcm.DLCMRestFields.STATUS_FIELD;
import static ch.dlcm.DLCMRestFields.STATUS_MESSAGE_FIELD;
import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.DLCMRestFields;
import ch.dlcm.listener.HistoryListener;
import ch.dlcm.listener.PackageListener;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Schema(description = "Orders allow to prepare archive download (DIP) from the archives (AIP).")
@Entity
@Table(name = "orderQuery")
@EntityListeners({ HistoryListener.class, PackageListener.class })
public class Order extends ResourceNormalized implements RemoteResourceContainer {

  public enum OrderStatus {
    DOWNLOADING, IN_DISSEMINATION_PREPARATION, IN_ERROR, IN_PREPARATION, IN_PROGRESS, READY, SUBMITTED
  }

  @Schema(description = "If the order is public, false otherwise.")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean publicOrder;

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "orderAip", joinColumns = { @JoinColumn(name = DLCMConstants.DB_ORDER_ID) }, inverseJoinColumns = {
          @JoinColumn(name = DLCMConstants.DB_AIP_ID) }, uniqueConstraints = {
                  @UniqueConstraint(columnNames = { DLCMConstants.DB_ORDER_ID, DLCMConstants.DB_AIP_ID }) })
  private List<ArchivalInfoPackage> aipPackages = new ArrayList<>();

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "orderDip", joinColumns = { @JoinColumn(name = DLCMConstants.DB_ORDER_ID) }, inverseJoinColumns = {
          @JoinColumn(name = DLCMConstants.DB_DIP_ID) }, uniqueConstraints = {
                  @UniqueConstraint(columnNames = { DLCMConstants.DB_ORDER_ID, DLCMConstants.DB_DIP_ID }) })
  private List<DisseminationInfoPackage> dipPackages = new ArrayList<>();

  @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<OrderSubsetItem> subsetItems;

  @Schema(description = "The metadata version of the order query.")
  @NotNull
  private DLCMMetadataVersion metadataVersion;

  @Schema(description = "The name of the order query.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String name;

  @Schema(description = "The query of the order query.")
  private String query;

  @Schema(description = "The type of the order query.")
  @NotNull
  @Enumerated(EnumType.STRING)
  private QueryType queryType;

  @Schema(description = "The status of the order query.")
  @Enumerated(EnumType.STRING)
  private OrderStatus status;

  @Schema(description = "The message related to the status of the order query.")
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String statusMessage;

  @Schema(description = "The dissemination policy identifier linked to the order.")
  @NotNull
  @Size(min = 1)
  @Column(name = DLCMConstants.DISSEMINATION_POLICY_ID_FIELD, length = DB_ID_LENGTH)
  private String disseminationPolicyId;

  @Schema(description = "The dissemination policy identifier linked to the orgunit.")
  @Size(min = 1)
  @Column(name = DLCMConstants.ORGANIZATIONAL_DISSEMINATION_POLICY_ID_FIELD, length = DB_ID_LENGTH)
  private String organizationalUnitDisseminationPolicyId;

  @Schema(description = "The checksum of the order subitems list.")
  private String subitemsChecksum;

  @Override
  public <T> boolean addItem(T t) {
    if (t instanceof DisseminationInfoPackage dip) {
      return this.addDip(dip);
    } else if (t instanceof ArchivalInfoPackage aip) {
      return this.addAip(aip);
    } else {
      if (t instanceof DisseminationPolicy disseminationPolicy) {
        this.setDisseminationPolicyId(disseminationPolicy.getResId());
      }
      return true;
    }
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.AIP).withRel(ResourceName.AIP));
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.VIEW).withRel(ActionName.VIEW));
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.SAVE).withRel(ActionName.SAVE));

      if (this.getDipNumber() > 0) {
        this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.DIP).withRel(ResourceName.DIP));
      }
      if (this.status == OrderStatus.READY) {
        this.add(linkBuilder.slash(this.getResId()).slash(ActionName.DOWNLOAD).withRel(ActionName.DOWNLOAD));
      }
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
    }
  }

  @Override
  public boolean equals(final Object other) {
    if (other == null) {
      return false;
    }
    if (!this.getClass().equals(other.getClass())) {
      return false;
    }
    final Order castOther = (Order) other;
    return Objects.equals(this.getName(), castOther.getName()) && Objects.equals(this.getStatus(), castOther.getStatus()) && Objects
            .equals(this.getQueryType(), castOther.getQueryType()) && Objects.equals(this.getQuery(), castOther.getQuery())
            && Objects.equals(this.getDipPackages(), castOther.getDipPackages())
            && Objects.equals(this.getDisseminationPolicyId(), castOther.disseminationPolicyId);
  }

  public Boolean isPublicOrder() {
    return this.publicOrder;
  }

  @Schema(description = "The AIP number of the order query.")
  public int getAipNumber() {
    return this.getAipPackages().size();
  }

  public List<ArchivalInfoPackage> getAipPackages() {
    return this.aipPackages;
  }

  @Schema(description = "The DIP number of the order query.")
  public int getDipNumber() {
    return this.getDipPackages().size();
  }

  public List<DisseminationInfoPackage> getDipPackages() {
    return this.dipPackages;
  }

  public List<OrderSubsetItem> getSubsetItems() {
    return this.subsetItems;
  }

  public DLCMMetadataVersion getMetadataVersion() {
    return this.metadataVersion;
  }

  public String getName() {
    return this.name;
  }

  public String getQuery() {
    return this.query;
  }

  public QueryType getQueryType() {
    return this.queryType;
  }

  public OrderStatus getStatus() {
    return this.status;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public String getDisseminationPolicyId() {
    return this.disseminationPolicyId;
  }

  public String getOrganizationalUnitDisseminationPolicyId() {
    return this.organizationalUnitDisseminationPolicyId;
  }

  public String getSubitemsChecksum() {
    return this.subitemsChecksum;
  } 

  @JsonIgnore
  public boolean hasAipInError() {
    if (this.getAipPackages().isEmpty()) {
      return false;
    }
    for (final ArchivalInfoPackage aip : this.getAipPackages()) {
      if (aip.getInfo().getStatus() == PackageStatus.IN_ERROR) {
        return true;
      }
    }
    return false;
  }

  @JsonIgnore
  public boolean hasDipInError() {
    if (this.getDipPackages().isEmpty()) {
      return false;
    }
    for (final DisseminationInfoPackage dip : this.getDipPackages()) {
      if (dip.getInfo().getStatus() == PackageStatus.IN_ERROR) {
        return true;
      }
    }
    return false;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getName(), this.getStatus(), this.getQueryType(), this.getQuery(), this.getAipPackages(), this.getDipPackages(),
            this.getDisseminationPolicyId());
  }

  @PrePersist
  @Override
  public void init() {
    if (this.status == null) {
      this.status = OrderStatus.IN_PROGRESS;
    }
    if (this.metadataVersion == null) {
      this.metadataVersion = DLCMMetadataVersion.getDefaultVersion();
    }
    if (this.publicOrder == null) {
      this.publicOrder = true;
    }
  }

  @JsonIgnore
  public boolean isCompleted() {
    if (this.getDipPackages().isEmpty()) {
      return false;
    }
    for (final DisseminationInfoPackage dip : this.getDipPackages()) {
      if (dip.getInfo().getStatus() != PackageStatus.COMPLETED) {
        return false;
      }
    }
    return true;
  }

  @JsonIgnore
  public boolean isEmptyQuery() {
    return StringTool.isNullOrEmpty(this.query);
  }

  @Override
  @JsonIgnore
  public boolean isModifiable() {
    return (this.getStatus() == OrderStatus.IN_PROGRESS || this.getStatus() == OrderStatus.IN_ERROR || this.getStatus() == OrderStatus.READY);
  }

  @JsonIgnore
  public boolean isAllAipCompleted() {
    if (this.getAipPackages().isEmpty()) {
      return false;
    }
    for (final ArchivalInfoPackage aip : this.getAipPackages()) {
      if (aip.getInfo().getStatus() != PackageStatus.COMPLETED) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String managedBy() {
    return ModuleName.ACCESS;
  }

  @Override
  public <T> boolean removeItem(T t) {
    if (t instanceof DisseminationInfoPackage dip) {
      return this.removeDip(dip);
    } else if (t instanceof ArchivalInfoPackage aip) {
      return this.removeAip(aip);
    } else {
      if (t instanceof DisseminationPolicy) {
        this.setDisseminationPolicyId(null);
      }
      return true;
    }
  }

  public void setPublicOrder(Boolean publicOrder) {
    this.publicOrder = publicOrder;
  }

  public void setAipPackages(List<ArchivalInfoPackage> aipPackages) {
    this.aipPackages = aipPackages;
  }

  public void setDipPackages(List<DisseminationInfoPackage> dipPackages) {
    this.dipPackages = dipPackages;
  }

  public void setSubsetItems(List<OrderSubsetItem> subsetItems) {
    this.subsetItems = subsetItems;
  }

  public void setErrorStatus(String desc) {
    this.status = OrderStatus.IN_ERROR;
    this.setStatusMessage(desc);
  }

  public void setMetadataVersion(DLCMMetadataVersion metadataVersion) {
    this.metadataVersion = metadataVersion;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setOrderStatus(OrderStatus status) {
    this.status = status;
    this.statusMessage = null;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public void setQueryType(QueryType queryType) {
    this.queryType = queryType;
  }

  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  public void setStatusMessage(String message) {
    this.statusMessage = StringTool.truncateWithEllipsis(message, SolidifyConstants.DB_LONG_STRING_LENGTH);
  }

  public void setDisseminationPolicyId(String disseminationPolicyId) {
    this.disseminationPolicyId = disseminationPolicyId;
  }

  public void setOrganizationalUnitDisseminationPolicyId(String id) {
    this.organizationalUnitDisseminationPolicyId = id;
  }

  public void setSubitemsChecksum(String checksum) {
    this.subitemsChecksum = checksum;
  }

  private boolean addAip(ArchivalInfoPackage aip) {
    if (!this.exist(this.aipPackages, aip)) {
      return this.aipPackages.add(aip);
    }
    return false;
  }

  private boolean addDip(DisseminationInfoPackage dip) {
    return this.dipPackages.add(dip);
  }

  private boolean removeAip(ArchivalInfoPackage aip) {
    return this.aipPackages.remove(aip);
  }

  private boolean removeDip(DisseminationInfoPackage dip) {
    return this.dipPackages.remove(dip);
  }

  @Override
  public List<String> getNonUpdatableFields() {
    return Arrays.asList(DLCMRestFields.RES_ID_FIELD, AIP_PACKAGES_FIELD, DIP_PACKAGES_FIELD, METADATA_VERSION_FIELD,
            QUERY_TYPE_FIELD, STATUS_FIELD, STATUS_MESSAGE_FIELD);
  }

  @Override
  public <T> List<String> getSubResourceIds(Class<T> type) {

    List<String> subResourceIds = Collections.emptyList();

    if (type == DisseminationPolicy.class && this.getDisseminationPolicyId() != null) {
      subResourceIds = CollectionTool.initList(this.getDisseminationPolicyId());
    }
    return subResourceIds;
  }

  @Override
  @JsonIgnore
  public List<Class<? extends Resource>> getEmbeddedResourceTypes() {
    final List<Class<? extends Resource>> embeddedResources = new ArrayList<>();

    embeddedResources.add(DisseminationPolicy.class);

    return embeddedResources;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrderArchive.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.access;

import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;

public class OrderArchive {

  private ArchivalInfoPackage archive = null;
  private ArchiveMetadata metadata = null;

  public OrderArchive(ArchivalInfoPackage aip) {
    this.archive = aip;
  }

  public OrderArchive(ArchivalInfoPackage aip, ArchiveMetadata md) {
    this.archive = aip;
    this.metadata = md;
  }

  public OrderArchive(ArchiveMetadata md) {
    this.metadata = md;
  }

  public ArchivalInfoPackage getArchive() {
    return this.archive;
  }

  public ArchiveMetadata getMetadata() {
    return this.metadata;
  }

  public void setArchive(ArchivalInfoPackage archive) {
    this.archive = archive;
  }

  public void setMetadata(ArchiveMetadata metadata) {
    this.metadata = metadata;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrderSubsetItem.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.access;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;

import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.rest.ModuleName;

@Entity
public class OrderSubsetItem extends ResourceNormalized {

  @NotNull
  @ManyToOne
  @JsonIgnore
  private Order order;

  @NotNull
  private String itemPath;

  public Order getOrder() {
    return this.order;
  }

  public String getItemPath() {
    return this.itemPath;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public void setItemPath(String itemPath) {
    this.itemPath = itemPath;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ACCESS;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    OrderSubsetItem that = (OrderSubsetItem) o;
    return Objects.equals(this.order, that.order) && Objects.equals(this.itemPath, that.itemPath);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), order, itemPath);
  }
}

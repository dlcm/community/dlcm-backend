/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrcidSynchronization.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.orcid;

import java.util.Objects;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;

import ch.unige.solidify.model.OrcidSynchronization;

import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.ModuleName;

@Entity
public class DlcmOrcidSynchronization extends OrcidSynchronization {

  /**
   * This field maps to the same database column as the 'personId' field in the parent class.
   * It is used to establish a foreign key between the orcid_synchronization and person tables in the database.
   * The @JoinColumn annotation ensures this by sharing the same name as the 'personId' field.
   * <p>
   * The column is marked as non-insertable and non-updatable because its value is
   * not directly stored in the database. Instead, the 'personId' field in the parent
   * class OrcidSynchronization is persisted.
   * <p>
   * The 'personId' field is automatically populated in the init() method with the
   * Person's resId when the object is persisted.
   */
  @Schema(description = "The person")
  @NotNull
  @ManyToOne(targetEntity = Person.class)
  @JoinColumn(name = PERSON_ID_FIELD, insertable = false, updatable = false)
  private Person person;

  public Person getPerson() {
    return this.person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  @Override
  public String getPersonId() {
    return this.person.getResId();
  }

  public void setAipId(String aipId) {
    this.objectId = aipId;
  }

  public String getAipId() {
    return this.objectId;
  }

  @Override
  public String getObjectId() {
    return this.getAipId();
  }

  @Override
  public void init() {
    if (this.person != null) {
      this.personId = this.person.getResId();
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || this.getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    DlcmOrcidSynchronization that = (DlcmOrcidSynchronization) o;
    return Objects.equals(this.person, that.person);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.person);
  }
}

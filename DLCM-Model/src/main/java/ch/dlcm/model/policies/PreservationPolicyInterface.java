/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - PreservationPolicyInterface.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.policies;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.SolidifyConstants;

import ch.dlcm.DLCMConstants;

public interface PreservationPolicyInterface {

  Boolean getDispositionApproval();

  /**
   * Default => 10 years; 0 => forever
   *
   * @return
   */
  Integer getRetention();

  @Schema(description = "The retention duration in human-readable format of the package.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  default String getSmartRetention() {
    if (this.getRetention() == null) {
      return DLCMConstants.UNKNOWN;
    }
    String unit;
    int s;
    if (this.getRetention() == 0) {
      return "forever";
    } else if (this.getRetention() < DLCMConstants.DAYS_BY_YEAR) {
      s = this.getRetention();
      unit = "days";
    } else if (this.getRetention() < (DLCMConstants.DAYS_BY_YEAR * DLCMConstants.YEARS_BY_CENTURY)) {
      s = this.getRetention() / DLCMConstants.DAYS_BY_YEAR;
      unit = "years";
    } else {
      s = this.getRetention() / (DLCMConstants.DAYS_BY_YEAR * DLCMConstants.YEARS_BY_CENTURY);
      unit = "centuries";
    }
    return String.format("%d %s", s, unit);
  }

  @JsonIgnore
  default String getRetentionDetail() {
    return DLCMConstants.RETENTION + SolidifyConstants.VALUE_SEP + this.getSmartRetention() + SolidifyConstants.FIELD_SEP
            + DLCMConstants.DISPOSITION_APPROVAL + SolidifyConstants.VALUE_SEP + this.getDispositionApproval();
  }

  default void init() {
    if (this.getRetention() == null) {
      this.setRetention(10 * DLCMConstants.DAYS_BY_YEAR);
    }

    if (this.getDispositionApproval() == null) {
      this.setDispositionApproval(true);
    }
  }

  void setDispositionApproval(Boolean approval);

  void setRetention(Integer retention);

}

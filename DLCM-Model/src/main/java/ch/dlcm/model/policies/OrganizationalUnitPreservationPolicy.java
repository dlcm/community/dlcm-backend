/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrganizationalUnitPreservationPolicy.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.policies;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.message.ResourceCacheMessage;
import ch.unige.solidify.rest.JoinResource;

import ch.dlcm.model.settings.OrganizationalUnit;

@Entity
@Table(name = "orgUnitPreservationPolicies")
public class OrganizationalUnitPreservationPolicy
        extends JoinResource<OrganizationalUnitPreservationPolicyId> {

  public static final String PATH_TO_ORG_UNIT = "compositeResId.organizationalUnit";
  public static final String PATH_TO_PRESERVATION_POLICY = "compositeResId.preservationPolicy";

  @EmbeddedId
  @JsonUnwrapped
  private OrganizationalUnitPreservationPolicyId compositeResId;

  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean defaultPolicy;

  public OrganizationalUnitPreservationPolicy() {
    this.compositeResId = new OrganizationalUnitPreservationPolicyId();
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof OrganizationalUnitPreservationPolicy)) {
      return false;
    }
    final OrganizationalUnitPreservationPolicy castOther = (OrganizationalUnitPreservationPolicy) other;
    return Objects.equals(this.getOrganizationalUnit(), castOther.getOrganizationalUnit()) && Objects
            .equals(this.getPreservationPolicy(), castOther.getPreservationPolicy());
  }

  @Override
  @Transient
  @JsonIgnore
  public CacheMessage getCacheMessage() {
    return new ResourceCacheMessage(OrganizationalUnit.class, this.getOrganizationalUnit().getResId());
  }

  @Override
  public OrganizationalUnitPreservationPolicyId getCompositeKey() {
    return this.compositeResId;
  }

  public Boolean getDefaultPolicy() {
    return this.defaultPolicy;
  }

  @Transient
  @JsonIgnore
  public OrganizationalUnit getOrganizationalUnit() {
    return this.compositeResId.getOrganizationalUnit();
  }

  @Transient
  @JsonIgnore
  public PreservationPolicy getPreservationPolicy() {
    return this.compositeResId.getPreservationPolicy();
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getOrganizationalUnit(), this.getPreservationPolicy());
  }

  @PrePersist
  @PreUpdate
  public void init() {
    if (this.defaultPolicy == null) {
      this.defaultPolicy = false;
    }
  }

  @Override
  public void setCompositeKey(OrganizationalUnitPreservationPolicyId compositeResId) {
    this.compositeResId = compositeResId;
  }

  public void setDefaultPolicy(Boolean defaultPolicy) {
    this.defaultPolicy = defaultPolicy;
  }

  @Transient
  @JsonIgnore
  public void setOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    this.compositeResId.setOrganizationalUnit(organizationalUnit);
  }

  @Transient
  @JsonIgnore
  public void setPreservationPolicy(PreservationPolicy preservationPolicy) {
    this.compositeResId.setPreservationPolicy(preservationPolicy);
  }

  @Override
  public String toString() {
    return super.toString() + " (default=" + this.getDefaultPolicy() + ")";
  }
}

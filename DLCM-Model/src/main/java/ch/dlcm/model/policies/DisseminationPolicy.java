/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DisseminationPolicy.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.policies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.rest.ModuleName;

@Schema(description = "A dissemination policy defines how to disseminate archives.")
@Entity
public class DisseminationPolicy extends ResourceNormalized {

  @Schema(description = "Type of dissemination: IIIF or WEB.")
  public enum DisseminationPolicyType {
    IIIF, HEDERA, BASIC, OAIS
  }

  @Schema(description = "Name used when downloading file: ARCHIVE_ID or ARCHIVE_NAME")
  public enum DownloadFileName {
    ARCHIVE_ID, ARCHIVE_NAME
  }

  @Schema(description = "The name of the dissemination policy.")
  @NotNull
  @Column(unique = true)
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String name;

  @JsonIgnore
  @OneToMany(mappedBy = OrganizationalUnitDisseminationPolicy.PATH_TO_DISSEMINATION_POLICY, cascade = CascadeType.ALL, orphanRemoval = true)
  protected List<OrganizationalUnitDisseminationPolicy> organizationalUnits = new ArrayList<>();

  @Schema(description = "The dissemination type of the dissemination policy.")
  @Enumerated(EnumType.STRING)
  @NotNull
  private DisseminationPolicyType type;

  @Schema(description = "The parameters of the dissemination policy.")
  @Size(min = 1, max = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String parameters;

  @Schema(description = "The number of the dissemination policy use in organizational units.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public int getUseNumber() {
    return this.organizationalUnits.size();
  }

  @Schema(description = "The prefix used when downloading a file with this dissemination policy.")
  @Size(max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  public String prefix;

  @Schema(description = "The archive name used when downloading a file with this dissemination policy.")
  @Enumerated(EnumType.STRING)
  @NotNull
  public DownloadFileName downloadFileName;

  @Schema(description = "The suffix used when downloading a file with this dissemination policy.")
  @Size(max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  public String suffix;

  @PrePersist
  @PreUpdate
  @Override
  public void init() {
    // Nothing to do
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  @Override
  public boolean isDeletable() {
    final int nbOrgUnits = this.getUseNumber();
    if (nbOrgUnits > 0) {
      throw new SolidifyUndeletableException(
              "The dissemination policy " + this.getResId() + " is used in " + nbOrgUnits + " organizational units");
    }
    return true;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setType(DisseminationPolicyType type) {
    this.type = type;
  }

  public DisseminationPolicyType getType() {
    return this.type;
  }

  public void setParameters(String parameters) {
    this.parameters = parameters;
  }

  public String getParameters() {
    return this.parameters;
  }

  public String getPrefix() {
    return prefix;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public String getSuffix() {
    return this.suffix;
  }

  public void setSuffix(String suffix) {
    this.suffix = suffix;
  }

  public DownloadFileName getDownloadFileName() {
    return this.downloadFileName;
  }

  public void setDownloadFileName(DownloadFileName downloadFilename) {
    this.downloadFileName = downloadFilename;
  }

  public String getArchiveDownloadFileName(String id, String name) {
    String str = StringTool.isNullOrEmpty(this.prefix) ? "" : this.prefix + "-";
    if (this.downloadFileName.equals(DownloadFileName.ARCHIVE_NAME)) {
      str += name;
    } else {
      str += id;
    }
    str += StringTool.isNullOrEmpty(this.suffix) ? "" : "-" + this.suffix;
    return str;
  }

  @JsonIgnore
  public void setParametersMap(Map<String, String> parametersMap) {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      this.parameters = objectMapper.writeValueAsString(parametersMap);
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("Error: cannot serialize dissemination policy parameters", e);
    }
  }

  @JsonIgnore
  public Map<String, String> getParametersMap() {
    ObjectMapper objectMapper = new ObjectMapper();
    if (this.parameters == null) {
      return Collections.emptyMap();
    }
    try {
      return objectMapper.readValue(this.parameters, new TypeReference<>() {
      });
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("Error: cannot deserialize dissemination policy parameters", e);
    }
  }

  public List<OrganizationalUnitDisseminationPolicy> getOrganizationalUnits() {
    return this.organizationalUnits;
  }

  @Override
  public String toString() {
    return "DisseminationPolicy{" +
            "name='" + name + '\'' +
            ", organizationalUnits=" + organizationalUnits +
            ", type=" + type +
            ", parameters='" + parameters + '\'' +
            '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    DisseminationPolicy that = (DisseminationPolicy) o;
    return Objects.equals(name, that.name) &&
            type == that.type &&
            Objects.equals(parameters, that.parameters) &&
            Objects.equals(organizationalUnits, that.organizationalUnits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, type, parameters, organizationalUnits);
  }
}

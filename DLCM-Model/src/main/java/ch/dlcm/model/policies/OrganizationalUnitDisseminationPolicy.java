/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrganizationalUnitDisseminationPolicy.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.policies;

import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.message.ResourceCacheMessage;
import ch.unige.solidify.rest.JoinResource;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.settings.OrganizationalUnit;

@Entity
@Table(name = "orgUnitDisseminationPolicies")
public class OrganizationalUnitDisseminationPolicy extends JoinResource<String> {

  public static final String PATH_TO_ORG_UNIT = "organizationalUnit";
  public static final String PATH_TO_DISSEMINATION_POLICY = "disseminationPolicy";

  @Id
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  @Schema(description = SolidifyConstants.RES_ID_DESCRIPTION)
  private String compositeResId;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_ORG_UNIT_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private OrganizationalUnit organizationalUnit;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_POLICY_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private DisseminationPolicy disseminationPolicy;

  @Schema(description = "The parameters of the dissemination policy.")
  @Size(min = 1, max = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String parameters;

  public OrganizationalUnitDisseminationPolicy() {
    this.compositeResId = StringTool.generateResId();
  }

  @Override
  @Transient
  @JsonIgnore
  public CacheMessage getCacheMessage() {
    return new ResourceCacheMessage(OrganizationalUnit.class, this.getOrganizationalUnit().getResId());
  }

  @Override
  public String getCompositeKey() {
    return this.compositeResId;
  }

  @Transient
  public DisseminationPolicy getDisseminationPolicy() {
    return this.disseminationPolicy;
  }

  @Transient
  public OrganizationalUnit getOrganizationalUnit() {
    return this.organizationalUnit;
  }

  public String getParameters() {
    return parameters;
  }

  @JsonIgnore
  public Map<String, String> getParametersMap() {
    ObjectMapper objectMapper = new ObjectMapper();
    if (this.parameters == null) {
      return Collections.emptyMap();
    }
    try {
      return objectMapper.readValue(this.parameters, new TypeReference<>() {
      });
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("Error: cannot deserialize dissemination policy parameters", e);
    }
  }

  @Override
  public void setCompositeKey(String compositeResId) {
    this.compositeResId = compositeResId;
  }

  @Transient
  public void setDisseminationPolicy(DisseminationPolicy disseminationPolicy) {
    this.disseminationPolicy = disseminationPolicy;
  }

  @Transient
  public void setOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    this.organizationalUnit = organizationalUnit;
  }

  public void setParameters(String parameters) {
    this.parameters = parameters;
  }

  @JsonIgnore
  public void setParametersMap(Map<String, String> parametersMap) {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      this.parameters = objectMapper.writeValueAsString(parametersMap);
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("Error: cannot serialize dissemination policy parameters", e);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    OrganizationalUnitDisseminationPolicy that = (OrganizationalUnitDisseminationPolicy) o;
    return Objects.equals(compositeResId, that.compositeResId) && Objects.equals(organizationalUnit, that.organizationalUnit)
            && Objects.equals(disseminationPolicy, that.disseminationPolicy) && Objects.equals(parameters, that.parameters);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), compositeResId, organizationalUnit, disseminationPolicy, parameters);
  }

}

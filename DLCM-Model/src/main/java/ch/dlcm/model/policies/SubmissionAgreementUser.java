/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - SubmissionAgreementUser.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.policies;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.time.OffsetDateTime;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.JoinResource;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.SubmissionAgreement;

@Entity
@Table(name = "submissionAgreementUser")
public class SubmissionAgreementUser extends JoinResource<String> {

  public static final String PATH_TO_USER = "user";
  public static final String PATH_TO_SUBMISSION_AGREEMENT = "submissionAgreement";

  @Id
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  @Schema(description = SolidifyConstants.RES_ID_DESCRIPTION)
  private String compositeResId;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_SUBMISSION_AGREEMENT_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private SubmissionAgreement submissionAgreement;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_USER_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private User user;

  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH)
  private OffsetDateTime approbationTime;

  public SubmissionAgreementUser() {
    this.compositeResId = StringTool.generateResId();
    this.approbationTime = OffsetDateTime.now();
  }

  @Override
  @JsonIgnore
  public String getCompositeKey() {
    return this.compositeResId;
  }

  @Transient
  public User getUser() {
    return this.user;
  }

  @Transient
  public SubmissionAgreement getSubmissionAgreement() {
    return this.submissionAgreement;
  }

  @Override
  public void setCompositeKey(String compositeResId) {
    this.compositeResId = compositeResId;
  }

  @Transient
  public void setUser(User user) {
    this.user = user;
  }

  @Transient
  public void setSubmissionAgreement(SubmissionAgreement submissionAgreement) {
    this.submissionAgreement = submissionAgreement;
  }

  public OffsetDateTime getApprobationTime() {
    return this.approbationTime;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    SubmissionAgreementUser that = (SubmissionAgreementUser) o;
    return Objects.equals(this.compositeResId, that.compositeResId) && Objects.equals(this.approbationTime, that.approbationTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.compositeResId, this.approbationTime);
  }
}

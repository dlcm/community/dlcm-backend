/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - PreservationPolicy.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.policies;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.rest.ModuleName;

@Schema(description = "A preservation policy defines whether approval is required for an AIP’s disposition as well as how long it should be kept.")
@Entity
public class PreservationPolicy extends ResourceNormalized implements PreservationPolicyInterface {

  @Schema(description = "If the approval step is mandatory during the AIP disposal process.")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean dispositionApproval;

  @Schema(description = "The retention duration in days of the preservation policy. O means forever.")
  // Default => 10 years; 0 => forever
  private Integer retention;

  @Schema(description = "The main storage indice in configuration of the preservation policy. O means default storage (first one).")
  // 0 => defaut storage (first one) ; x => indice of storage in configuration
  private Integer mainStorage;

  @JsonIgnore
  @OneToMany(mappedBy = OrganizationalUnitPreservationPolicy.PATH_TO_PRESERVATION_POLICY, cascade = CascadeType.ALL, orphanRemoval = true)
  protected List<OrganizationalUnitPreservationPolicy> organizationalUnits = new ArrayList<>();

  @Schema(description = "The name of the preservation policy.")
  @NotNull
  @Column(unique = true)
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String name;

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj)) {
      return false;
    }
    if (this.getClass() != obj.getClass()) {
      return false;
    }
    final PreservationPolicy other = (PreservationPolicy) obj;
    if (this.name == null) {
      return other.name == null;
    } else
      return this.name.equals(other.name);
  }

  public String getName() {
    return this.name;
  }

  @Schema(description = "The number of the preservation policy use in organizational units.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  public int getUseNumber() {
    return this.organizationalUnits.size();
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
    result = prime * result + ((this.dispositionApproval == null) ? 0 : this.dispositionApproval.hashCode());
    result = prime * result + ((this.retention == null) ? 0 : this.retention.hashCode());
    return result;
  }

  @PrePersist
  @PreUpdate
  @Override
  public void init() {
    PreservationPolicyInterface.super.init();
    if (this.getMainStorage() == null) {
      this.setMainStorage(0);
    }
  }

  @Override
  public boolean isDeletable() {
    final int nbOrgUnits = this.getUseNumber();
    if (nbOrgUnits > 0) {
      throw new SolidifyUndeletableException(
              "The preservation policy " + this.getResId() + " is used in " + nbOrgUnits + " organizational units");
    }
    return true;
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public Boolean getDispositionApproval() {
    return this.dispositionApproval;
  }

  @Override
  public Integer getRetention() {
    return this.retention;
  }

  public Integer getMainStorage() {
    return this.mainStorage;
  }

  public void setMainStorage(Integer mainStorage) {
    this.mainStorage = mainStorage;
  }

  @Override
  public void setDispositionApproval(Boolean dispositionApproval) {
    this.dispositionApproval = dispositionApproval;
  }

  @Override
  public void setRetention(Integer retention) {
    this.retention = retention;
  }

  @Override
  public String toString() {
    return super.toString() + " (name=" + this.getName() + ")";
  }

}

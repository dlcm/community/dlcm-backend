/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - SubmissionAgreementType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.policies;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        The type of submission agreement when submitting a deposit:
        - WITHOUT => No submission agreement needed.
        - ON_FIRST_DEPOSIT => For the first deposit submission, the user has to approve the submission agreement once.
        - FOR_EACH_DEPOSIT => At each deposit submission, the user has to approve the submission agreement.
        """)
public enum SubmissionAgreementType {
  // Sorted from the less restrictive to the most restrictive

  //@formatter:off
  WITHOUT(10),
  ON_FIRST_DEPOSIT(20),
  FOR_EACH_DEPOSIT(30);
  //@formatter:on

  private final int level;

  SubmissionAgreementType(int level) {
    this.level = level;
  }

  public int value() {
    return this.level;
  }
}

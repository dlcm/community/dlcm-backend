/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrganizationalUnitPreservationPolicyId.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.policies;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.settings.OrganizationalUnit;

@Embeddable
public class OrganizationalUnitPreservationPolicyId implements Serializable {
  @Serial
  private static final long serialVersionUID = -1434643940764256413L;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_ORG_UNIT_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private OrganizationalUnit organizationalUnit;

  @ManyToOne
  @JoinColumn(name = DLCMConstants.DB_POLICY_ID, referencedColumnName = DLCMConstants.DB_RES_ID)
  private PreservationPolicy preservationPolicy;

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof OrganizationalUnitPreservationPolicyId)) {
      return false;
    }
    final OrganizationalUnitPreservationPolicyId castOther = (OrganizationalUnitPreservationPolicyId) other;
    return Objects.equals(this.getOrganizationalUnit(), castOther.getOrganizationalUnit()) && Objects
            .equals(this.getPreservationPolicy(), castOther.getPreservationPolicy());
  }

  @JsonIgnore
  public OrganizationalUnit getOrganizationalUnit() {
    return this.organizationalUnit;
  }

  @JsonIgnore
  public PreservationPolicy getPreservationPolicy() {
    return this.preservationPolicy;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getOrganizationalUnit(), this.getPreservationPolicy());
  }

  @JsonIgnore
  public void setOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    this.organizationalUnit = organizationalUnit;
  }

  @JsonIgnore
  public void setPreservationPolicy(PreservationPolicy preservationPolicy) {
    this.preservationPolicy = preservationPolicy;
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName() + ": " + DLCMConstants.DB_ORG_UNIT_ID + "=" + this.getOrganizationalUnit().getResId() + " "
            + DLCMConstants.DB_POLICY_ID + "=" + this.getPreservationPolicy().getResId();
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - SubmissionPolicy.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model.policies;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.DLCMConstants;
import ch.dlcm.deserializer.SubmissionAgreementTypeDeserializer;
import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.rest.ModuleName;

@Schema(description = "A submission policy is a policy that can be associated to an SIP. It defines whether an SIP can submitted with or without approval and the amount of time it should be kept.")
@Entity
public class SubmissionPolicy extends ResourceNormalized implements RemoteResourceContainer {

  @JsonIgnore
  @OneToMany(mappedBy = OrganizationalUnitSubmissionPolicy.PATH_TO_SUBMISSION_POLICY, cascade = CascadeType.ALL, orphanRemoval = true)
  protected List<OrganizationalUnitSubmissionPolicy> organizationalUnits = new ArrayList<>();

  @Schema(description = "The name of the submission policy.")
  @NotNull
  @Column(unique = true)
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String name;

  @Schema(description = "If approval step is mandatory for submitting a deposit.")
  @Column(length = SolidifyConstants.DB_BOOLEAN_LENGTH)
  private Boolean submissionApproval;

  @Schema(description = "The time in days to keep completed deposit before purge.")
  // Default => 2 months, measured in days
  private Integer timeToKeep;

  @Schema(description = "The submission agreement type of the submission policy.")
  @Column
  @Enumerated(EnumType.STRING)
  @JsonDeserialize(using = SubmissionAgreementTypeDeserializer.class)
  private SubmissionAgreementType submissionAgreementType;

  @Schema(description = "The submission agreement of the submission policy.")
  @ManyToOne
  private SubmissionAgreement submissionAgreement;

  public String getName() {
    return this.name;
  }

  public Boolean getSubmissionApproval() {
    return this.submissionApproval;
  }

  public Integer getTimeToKeep() {
    return this.timeToKeep;
  }

  @Schema(description = "The number of the submission policy use in organizational units.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = Access.READ_ONLY)
  public int getUseNumber() {
    return this.organizationalUnits.size();
  }

  public SubmissionAgreementType getSubmissionAgreementType() {
    return this.submissionAgreementType;
  }

  public SubmissionAgreement getSubmissionAgreement() {
    return this.submissionAgreement;
  }

  @PrePersist
  @PreUpdate
  @Override
  public void init() {
    if (this.submissionApproval == null) {
      this.submissionApproval = true;
    }
    if (this.submissionAgreementType == null) {
      this.submissionAgreementType = SubmissionAgreementType.WITHOUT;
    }
    if (this.timeToKeep == null) {
      this.timeToKeep = 2 * DLCMConstants.DAYS_BY_MONTH;
    }
  }

  @Override
  public boolean isDeletable() {
    final int nbOrgUnits = this.getUseNumber();
    if (nbOrgUnits > 0) {
      throw new SolidifyUndeletableException("The submission policy " + this.getResId() + " is used in " + nbOrgUnits + " organizational units");
    }
    return true;
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSubmissionApproval(Boolean submissionApproval) {
    this.submissionApproval = submissionApproval;
  }

  public void setTimeToKeep(Integer timeToKeep) {
    this.timeToKeep = timeToKeep;
  }

  public void setSubmissionAgreementType(SubmissionAgreementType submissionAgreementType) {
    this.submissionAgreementType = submissionAgreementType;
  }

  public void setSubmissionAgreement(SubmissionAgreement submissionAgreement) {
    this.submissionAgreement = submissionAgreement;
  }

  @Override
  public <T> boolean addItem(T t) {
    if (t instanceof SubmissionAgreement sa) {
      this.setSubmissionAgreement(sa);
      return true;
    }
    throw new UnsupportedOperationException("Missing implementation 'addItem' method (" + t.getClass().getSimpleName() + ")");
  }

  @Override
  public List<Class<? extends Resource>> getEmbeddedResourceTypes() {
    return List.of(SubmissionAgreement.class);
  }

  @Override
  public <T> List<String> getSubResourceIds(Class<T> type) {
    if (SubmissionAgreement.class.equals(type) && this.submissionAgreement != null) {
      return List.of(this.submissionAgreement.getResId());
    } else if (type == null) {
      throw new IllegalArgumentException("type should not be null");
    } else if (!this.getEmbeddedResourceTypes().contains(type)) {
      throw new IllegalArgumentException("Class " + type.getCanonicalName() + " is not a subresource of SubmissionPolicy");
    }
    return List.of();
  }

  @Override
  public <T> boolean removeItem(T t) {
    if (t instanceof SubmissionAgreement) {
      this.setSubmissionAgreement(null);
      return true;
    }
    throw new UnsupportedOperationException("Missing implementation 'removeItem' method (" + t.getClass().getSimpleName() + ")");
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    SubmissionPolicy that = (SubmissionPolicy) o;
    return Objects.equals(this.organizationalUnits, that.organizationalUnits) && Objects.equals(this.name, that.name)
            && Objects.equals(this.submissionApproval, that.submissionApproval) && Objects.equals(this.timeToKeep, that.timeToKeep)
            && this.submissionAgreementType == that.submissionAgreementType
            && Objects.equals(this.submissionAgreement, that.submissionAgreement);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.organizationalUnits, this.name, this.submissionApproval, this.timeToKeep,
            this.submissionAgreementType, this.submissionAgreement);
  }

  @Override
  public String toString() {
    return super.toString() + " (name=" + this.getName() + ")";
  }

}

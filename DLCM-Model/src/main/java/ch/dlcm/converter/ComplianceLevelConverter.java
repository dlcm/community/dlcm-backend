/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ComplianceLevelConverter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import ch.dlcm.model.ComplianceLevel;

@Converter(autoApply = true)
public class ComplianceLevelConverter implements AttributeConverter<ComplianceLevel, Integer> {
  @Override
  public Integer convertToDatabaseColumn(ComplianceLevel complianceLevel) {
    if (complianceLevel != null) {
      return complianceLevel.value();
    }
    return null;
  }

  @Override
  public ComplianceLevel convertToEntityAttribute(Integer value) {
    if (value != null) {
      return ComplianceLevel.fromValue(value);
    }
    return null;
  }
}

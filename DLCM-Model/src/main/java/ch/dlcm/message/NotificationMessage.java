/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - NotificationMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.message;

import java.io.Serial;
import java.io.Serializable;

import ch.dlcm.model.notification.NotificationStatus;

public class NotificationMessage implements Serializable {

    @Serial
    private static final long serialVersionUID = 6251638532680367587L;

    private final String resId;
    private final NotificationStatus notificationStatus;

    private final String notificationTypeId;

    public NotificationMessage(String resId, NotificationStatus notificationStatus,  String notificationTypeId) {
        this.resId = resId;
        this.notificationStatus = notificationStatus;
        this.notificationTypeId = notificationTypeId;
    }

    public String getResId() {
        return this.resId;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public String getNotificationTypeId() {
        return notificationTypeId;
    }


    @Override
    public String toString() {
        return "NotificationMessage [resId=" + this.resId + "] of type : "+ notificationTypeId +" with status : " + notificationStatus;
    }

}

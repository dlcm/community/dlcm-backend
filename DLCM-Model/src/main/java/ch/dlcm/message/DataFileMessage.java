/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DataFileMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.message;

import java.io.Serial;

import ch.dlcm.model.AbstractDataFile;

public class DataFileMessage extends DlcmMessage {

  @Serial
  private static final long serialVersionUID = -15518064624284541L;

  private final Class<? extends AbstractDataFile> dataFileClass;

  public DataFileMessage(Class<? extends AbstractDataFile> clazz, String resId) {
    super(resId);
    this.dataFileClass = clazz;
  }

  public Class<? extends AbstractDataFile> getDataFileClass() {
    return this.dataFileClass;
  }

  @Override
  public String toString() {
    return "DataFileMessage [dataFileClass=" + this.dataFileClass.getSimpleName() + ", resId=" + this.resId + "]";
  }
}

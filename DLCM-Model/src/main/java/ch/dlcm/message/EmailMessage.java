/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - EmailMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.message;

import java.io.Serial;
import java.io.Serializable;
import java.util.Map;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;

import ch.dlcm.DLCMConstants;

public class EmailMessage implements Serializable {
  @Serial
  private static final long serialVersionUID = 6428936902341150988L;

  //@formatter:off
  public enum EmailTemplate {
    NEW_SUBSCRIPTION(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX + " Inscription à des notifications / Subscription to notifications"),
    REMOVE_SUBSCRIPTION(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX + " Désinscription à des notifications / Unsubscription to notifications"),
    MY_INDIRECT_DEPOSITS_COMPLETED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX + " Nouveaux Dépôts comme contributeur-trice / New deposits as contributor"),
    MY_DEPOSITS_APPROVED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX + " Dépôts approuvés / Approved deposits"),
    MY_DEPOSITS_COMPLETED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX + " Dépôts terminés / Completed deposits"),
    NEW_DEPOSITS_IN_ERROR(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX + " Nouveaux dépôts en erreur"),
    NEW_DEPOSITS_TO_APPROVE(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX + " Nouveaux dépôts à approuver"),
    MY_ARCHIVES_COMPLETED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX + " Traitement des vos nouvelles archives terminés / Processing of your new archives completed"),
    NEW_ARCHIVES_COMPLETED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX + " Traitement des nouvelles archives terminés / Processing of new archives completed"),
    NEW_ORG_UNIT_TO_APPROVE(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX + " Nouvelles unités organisationnelles à approuver / New organizational units to approve"),
    NEW_MEMBER_ORG_UNIT_TO_APPROVE(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX +  " Nouveaux membres d'unités organisationnelles à approuver /  New members of organizational units to approve"),
    NEW_REQUEST_TO_ACCESS_DATASET(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX +  " Nouvelles demandes d'autorisation d'accès aux archives /  New requests to access archives archive"),
    REQUEST_TO_ACCESS_DATASET_ACCEPTED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX +  " Demande d'accès acceptée /  Request to access granted"),
    REQUEST_TO_ACCESS_DATASET_REFUSED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX +  " Demande d'accès refusée /  Request to access refused"),
    REQUEST_TO_CREATE_ORG_UNIT_ACCEPTED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX +  " Demande de creation d'unité organisationnelle acceptée /  Request to create an organizational unit granted"),
    REQUEST_TO_CREATE_ORG_UNIT_REFUSED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX +  " Demande de creation d'unité organisationnelle refusée /  Request to create an organizational unit refused"),
    REQUEST_TO_JOIN_ORG_UNIT_REFUSED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX +  " Demande d'adhésion à une unité organisationelle refusée /  Request to add members to an organizational unit refused"),
    REQUEST_TO_JOIN_ORG_UNIT_ACCEPTED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX +  " Demande d'adhésion à une unité organisationelle acceptée /  Request to add members to an organizational unit accepted"),
    REQUEST_TO_DISPOSE_ARCHIVE_ACCEPTED(DLCMConstants.NOTIFICATION_EMAIL_DLCM_PREFIX +  " Demande de dispose un archive acceptée /  Request to dispose of a file accepted");

    private final String subject;

    EmailTemplate(String subject) {
      this.subject = subject;
    }

    public String getSubject() {
      return this.subject;
    }
  }
  //@formatter:on

  @Enumerated(EnumType.STRING)
  @NotNull
  private EmailTemplate template;

  private String to;

  private Map<String, Object> parameters;

  public EmailMessage(String toRecipient, EmailTemplate template, Map<String, Object> parameters) {
    this.template = template;
    this.to = toRecipient;
    this.parameters = parameters;
  }

  public EmailTemplate getTemplate() {
    return this.template;
  }

  public void setTemplate(EmailTemplate template) {
    this.template = template;
  }

  public String getTo() {
    return this.to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public Map<String, Object> getParameters() {
    return this.parameters;
  }

  public void setParameters(Map<String, Object> parameters) {
    this.parameters = parameters;
  }

  @Override
  public String toString() {
    return super.toString() + " [template=" + this.template + "]" + " [to=" + this.to + "]";
  }
}

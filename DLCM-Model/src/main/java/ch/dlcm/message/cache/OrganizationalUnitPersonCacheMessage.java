/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrganizationalUnitPersonCacheMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.message.cache;

import java.io.Serial;

import ch.unige.solidify.message.CacheMessage;

public final class OrganizationalUnitPersonCacheMessage extends CacheMessage {

  @Serial
  private static final long serialVersionUID = 1L;

  private final String organizationalUnitId;
  private final String personId;

  public OrganizationalUnitPersonCacheMessage(String personId, String organizationalUnitId) {
    this.personId = personId;
    this.organizationalUnitId = organizationalUnitId;
  }

  public String getOrganizationalUnitId() {
    return this.organizationalUnitId;
  }

  public String getPersonId() {
    return this.personId;
  }

  @Override
  public String toString() {
    return "OrganizationalUnitPersonCache [personId=" + this.personId + ", organizationalUnitId=" + this.organizationalUnitId + "]";
  }
}

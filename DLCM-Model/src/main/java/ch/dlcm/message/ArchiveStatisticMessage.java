/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveStatisticMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.message;

import java.io.Serial;

public class ArchiveStatisticMessage extends DlcmMessage {
  @Serial
  private static final long serialVersionUID = 4671341129721651522L;

  public enum StatisticType {
    VIEW, // View archive metadata
    DOWNLOAD // Download archive
  }

  private StatisticType statisticType;

  public ArchiveStatisticMessage(String resId, StatisticType statisticType) {
    super(resId);
    this.statisticType = statisticType;
  }

  public boolean isViewStatistic() {
    return this.statisticType.equals(StatisticType.VIEW);
  }

  public boolean isDownloadStatistic() {
    return this.statisticType.equals(StatisticType.DOWNLOAD);
  }
}

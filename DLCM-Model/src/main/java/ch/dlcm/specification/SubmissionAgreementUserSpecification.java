/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - SubmissionAgreementUserSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.Join2TiersSpecification;

import ch.dlcm.model.policies.SubmissionAgreementUser;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.SubmissionAgreement;

public class SubmissionAgreementUserSpecification extends Join2TiersSpecification<SubmissionAgreementUser> {

  @Serial
  private static final long serialVersionUID = 6743840221482228513L;

  public SubmissionAgreementUserSpecification(SubmissionAgreementUser joinCriteria) {
    super(joinCriteria, SubmissionAgreementUser.PATH_TO_SUBMISSION_AGREEMENT, SubmissionAgreementUser.PATH_TO_USER);
  }

  @Override
  protected void completeChildPredicatesList(Root<SubmissionAgreementUser> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    final User user = this.joinCriteria.getUser();
    final SubmissionAgreement submissionAgreement = this.joinCriteria.getSubmissionAgreement();
    if (user != null) {
      Path<?> userPath = this.getChildPath(root);
      if (user.getResId() != null) {
        predicatesList.add(builder.equal(userPath.get("resId"), user.getResId()));
      }
      if (user.getExternalUid() != null) {
        predicatesList.add(builder.equal(userPath.get("externalUid"), user.getExternalUid()));
      }
      if (user.getFirstName() != null) {
        predicatesList.add(builder.like(root.get("firstName"), "%" + user.getFirstName() + "%"));
      }
      if (user.getLastName() != null) {
        predicatesList.add(builder.like(root.get("lastName"), "%" + user.getLastName() + "%"));
      }
    }
    if (submissionAgreement != null) {
      Path<?> submissionAgreementPath = this.getParentPath(root);
      if (submissionAgreement.getResId() != null) {
        predicatesList.add(builder.equal(submissionAgreementPath.get("resId"), submissionAgreement.getResId()));
      }
    }
  }

  @Override
  protected void completeJoinPredicatesList(Root<SubmissionAgreementUser> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    // Do nothing
  }

  @Override
  protected String getParentId() {
    final SubmissionAgreement submissionAgreement = this.joinCriteria.getSubmissionAgreement();
    if (submissionAgreement == null) {
      return null;
    }
    return submissionAgreement.getResId();
  }

  @Override
  protected String getChildId() {
    final User user = this.joinCriteria.getUser();
    if (user == null) {
      return null;
    }
    return user.getResId();
  }

}

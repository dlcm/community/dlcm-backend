/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositContributorSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositContributor;
import ch.dlcm.model.preingest.DepositContributorId;
import ch.dlcm.model.settings.OrganizationalUnit;

public class DepositContributorSpecification
        implements Specification<DepositContributor>, FilterableByOrganizationalUnitsSpecification<DepositContributor> {
  @Serial
  private static final long serialVersionUID = 5861472447827622814L;

  private final DepositContributor criteria;

  private List<OrganizationalUnit> organizationalUnits;

  public DepositContributorSpecification(DepositContributor criteria) {
    this.criteria = criteria;
  }

  @Override
  public Path<Object> getOrganizationalUnitPath(Root<DepositContributor> root) {
    return root.get("compositeResId").get("deposit").get(DLCMConstants.ORG_UNIT_ID_FIELD);
  }

  @Override
  public void setOrganizationalUnits(List<OrganizationalUnit> organizationalUnits) {
    this.organizationalUnits = organizationalUnits;
  }

  @Override
  public Predicate toPredicate(Root<DepositContributor> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final List<Predicate> listPredicate = new ArrayList<>();
    final Path<DepositContributorId> proxy = root.get("compositeResId");

    if (this.criteria.getPersonId() != null) {
      listPredicate.add(builder.equal(proxy.get("personId"), this.criteria.getPersonId()));
    }
    if (this.criteria.getDeposit() != null) {
      this.setDeposit(root, builder, listPredicate, this.criteria.getDeposit());
    }

    /***/

    final Predicate organizationalUnitsPredicate = this.getInOrganizationalUnitsPredicate(root, this.organizationalUnits);
    if (organizationalUnitsPredicate != null) {
      listPredicate.add(organizationalUnitsPredicate);
    }

    /***/

    return builder.and(listPredicate.toArray(new Predicate[listPredicate.size()]));
  }

  private void setDeposit(Root<DepositContributor> root, CriteriaBuilder builder, List<Predicate> listPredicate, Deposit deposit) {
    final Path<Deposit> proxy = root.get("compositeResId").get("deposit");
    if (deposit.getTitle() != null) {
      listPredicate.add(builder.like(proxy.get("title"), "%" + deposit.getTitle() + "%"));
    }
    if (deposit.getStatus() != null) {
      listPredicate.add(builder.equal(proxy.get("status"), deposit.getStatus()));
    }
    if (deposit.getOrganizationalUnitId() != null) {
      listPredicate.add(builder.equal(proxy.get(DLCMConstants.ORG_UNIT_ID_FIELD), deposit.getOrganizationalUnitId()));
    }

  }
}

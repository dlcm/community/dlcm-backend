/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveStatisticsSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.model.settings.ArchiveStatistics;

public class ArchiveStatisticsSpecification extends SolidifySpecification<ArchiveStatistics> {

  @Serial
  private static final long serialVersionUID = 7731718824657917560L;

  public ArchiveStatisticsSpecification(ArchiveStatistics criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<ArchiveStatistics> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getStatistics().getViewNumber() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("statistics.viewNumber"), this.criteria.getStatistics().getViewNumber()));
    }
    if (this.criteria.getStatistics().getDownloadNumber() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("statisticdownloaNumber"), this.criteria.getStatistics().getDownloadNumber()));
    }
  }
}

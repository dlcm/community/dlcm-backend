/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - NotificationSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.time.OffsetDateTime;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;

public class NotificationSpecification extends SolidifySpecification<Notification> {

  @Serial
  private static final long serialVersionUID = -7569094645132768485L;

  private OffsetDateTime createdDateMinValue = null;
  private OffsetDateTime createdDateMaxValue = null;
  private boolean onlyWithNullSentTime = false;
  private boolean onlyWithNullReadTime = false;
  private String exceptResId = null;

  private List<NotificationStatus> notificationStatusList;

  public NotificationSpecification(Notification notification) {
    super(notification);
  }

  @Override
  protected void completePredicatesList(Root<Notification> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getNotificationType() != null) {
      this.setNotificationTypeCriteria(root, builder, predicatesList, this.criteria.getNotificationType());
    }
    if (this.criteria.getNotificationStatus() != null) {
      predicatesList.add(builder.equal(root.get("notificationStatus"), this.criteria.getNotificationStatus()));
    }
    if (this.criteria.getNotificationMark() != null) {
      predicatesList.add(builder.equal(root.get("notificationMark"), this.criteria.getNotificationMark()));
    }
    if (this.criteria.getMessage() != null) {
      predicatesList.add(builder.like(root.get("message"), "%" + this.criteria.getMessage() + "%"));
    }
    if (this.criteria.getObjectId() != null) {
      predicatesList.add(builder.equal(root.get("objectId"), this.criteria.getObjectId()));
    }
    if (this.criteria.getEmitter() != null) {
      predicatesList.add(builder.equal(root.get("emitter").get("resId"), this.criteria.getEmitter().getResId()));
    }
    if (this.criteria.getNotifiedOrgUnit() != null) {
      predicatesList.add(builder.equal(root.get("notifiedOrgUnit").get("resId"), this.criteria.getNotifiedOrgUnit().getResId()));
    }
    if (this.createdDateMinValue != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("creation").<OffsetDateTime>get("when"), this.createdDateMinValue));
    }

    if (this.createdDateMaxValue != null) {
      predicatesList.add(builder.lessThanOrEqualTo(root.get("creation").<OffsetDateTime>get("when"), this.createdDateMaxValue));
    }

    if (this.onlyWithNullSentTime) {
      predicatesList.add(builder.isNull(root.get("sentTime")));
    }

    if (this.onlyWithNullReadTime) {
      predicatesList.add(builder.isNull(root.get("readTime")));
    }

    if (this.exceptResId != null) {
      predicatesList.add(builder.notEqual(root.get("resId"), this.exceptResId));
    }

    if (this.criteria.getNotificationStatus() == null && this.notificationStatusList != null) {
      predicatesList.add(builder.and(root.get("notificationStatus").in(this.notificationStatusList)));
    }
  }

  private void setNotificationTypeCriteria(Root<Notification> root, CriteriaBuilder builder, List<Predicate> predicatesList,
          NotificationType notificationType) {
    final Path<NotificationType> proxy = root.get("notificationType");
    if (NotificationType.getAllNotificationTypes().stream().anyMatch(nt -> notificationType.getResId().equals(nt.getResId()))) {
      predicatesList.add(builder.equal(proxy.get("resId"), notificationType.getResId()));
    }
    if (notificationType.getNotificationCategory() != null) {
      predicatesList.add(builder.equal(proxy.get("notificationCategory"), notificationType.getNotificationCategory()));
    }
  }

  public OffsetDateTime getCreatedDateMinValue() {
    return this.createdDateMinValue;
  }

  public void setCreatedDateMinValue(OffsetDateTime createdDateMinValue) {
    this.createdDateMinValue = createdDateMinValue;
  }

  public OffsetDateTime getCreatedDateMaxValue() {
    return this.createdDateMaxValue;
  }

  public void setCreatedDateMaxValue(OffsetDateTime createdDateMaxValue) {
    this.createdDateMaxValue = createdDateMaxValue;
  }

  public boolean isOnlyWithNullSentTime() {
    return this.onlyWithNullSentTime;
  }

  public void setOnlyWithNullSentTime(boolean onlyWithNullSentTime) {
    this.onlyWithNullSentTime = onlyWithNullSentTime;
  }

  public boolean isOnlyWithNullReadTime() {
    return this.onlyWithNullReadTime;
  }

  public void setOnlyWithNullReadTime(boolean onlyWithNullReadTime) {
    this.onlyWithNullReadTime = onlyWithNullReadTime;
  }

  public void setExceptResId(String exceptResId) {
    this.exceptResId = exceptResId;
  }

  public void setNotificationStatusList(List<NotificationStatus> notificationStatusList) {
    this.notificationStatusList = notificationStatusList;
  }
}

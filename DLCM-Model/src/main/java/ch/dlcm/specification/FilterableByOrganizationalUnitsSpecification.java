/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - FilterableByOrganizationalUnitsSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.Access;
import ch.dlcm.model.settings.OrganizationalUnit;

public interface FilterableByOrganizationalUnitsSpecification<T> {

  /**
   * value used to get a Predicate discarding all search results
   */
  String NO_ORGANIZATIONAL_UNIT_VALUE = "-1";

  /**
   * If organizationalUnits == null ----> no filter must be applied ----> return null If
   * organizationalUnits != null -----> use the values to build a Predicate or a fake value of "-1" to
   * obtain a query returning nothing
   *
   * @param root
   * @param organizationalUnits
   * @return
   */
  default Predicate getInOrganizationalUnitsPredicate(Root<T> root, List<OrganizationalUnit> organizationalUnits) {

    if (organizationalUnits != null) {

      List<String> organizationalUnitIds = new ArrayList<>();

      if (!organizationalUnits.isEmpty()) {
        organizationalUnitIds = this.getOrganizationalUnitIds(organizationalUnits);
      } else {
        organizationalUnitIds.add(NO_ORGANIZATIONAL_UNIT_VALUE);
      }

      return this.getOrganizationalUnitPath(root).in(organizationalUnitIds);

    } else {
      return null;
    }
  }

  /**
   * Return a Predicate to search for items without embargo and public final access
   *
   * @param root
   * @param builder
   * @return
   */
  default Predicate getNoEmbargoAndPublicAccessPredicate(Root<T> root, CriteriaBuilder builder) {
    return builder.and(
            builder.isNull(this.getEmbargoAccessPath(root)),
            builder.equal(this.getFinalAccessPath(root), Access.PUBLIC));
  }

  /**
   * Get a list of ids as String with each OrganizationalUnit resId
   *
   * @param organizationalUnits
   * @return
   */
  default List<String> getOrganizationalUnitIds(List<OrganizationalUnit> organizationalUnits) {

    final List<String> organizationalUnitIds = new ArrayList<>();

    if (organizationalUnits != null) {
      for (final OrganizationalUnit unit : organizationalUnits) {
        organizationalUnitIds.add(unit.getResId());
      }
    }

    return organizationalUnitIds;
  }

  /**
   * Must return the Path to the organizationalUnitId property
   *
   * @param root
   * @return
   */
  default Path<Object> getOrganizationalUnitPath(Root<T> root) {
    return root.get("info").get(DLCMConstants.ORG_UNIT_ID_FIELD);
  }

  default Path<Object> getFinalAccessPath(Root<T> root) {
    return root.get("info").get("access");
  }

  default Path<Object> getEmbargoAccessPath(Root<T> root) {
    return root.get("info").get("embargo").get("access");
  }

  /**
   * Set a list of OrganizationalUnit that are used to build a filtering 'in' Predicate
   *
   * @param organizationalUnits
   */
  void setOrganizationalUnits(List<OrganizationalUnit> organizationalUnits);

}

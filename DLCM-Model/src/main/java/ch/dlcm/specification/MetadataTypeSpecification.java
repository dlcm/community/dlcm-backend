/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - MetadataTypeSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.model.settings.MetadataType;

public class MetadataTypeSpecification extends SolidifySpecification<MetadataType> {
  @Serial
  private static final long serialVersionUID = 1248351103736449718L;

  public MetadataTypeSpecification(MetadataType criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<MetadataType> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getMetadataFormat() != null) {
      predicatesList.add(builder.equal(root.get("metadataFormat"), this.criteria.getMetadataFormat()));
    }
    if (this.criteria.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }
    if (this.criteria.getVersion() != null) {
      predicatesList.add(builder.like(root.get("version"), "%" + this.criteria.getVersion() + "%"));
    }
    if (this.criteria.getDescription() != null) {
      predicatesList.add(builder.like(root.get("description"), "%" + this.criteria.getDescription() + "%"));
    }
    if (this.criteria.getMetadataSchema() != null) {
      predicatesList.add(builder.like(root.get("metadataSchema"), "%" + this.criteria.getMetadataSchema() + "%"));
    }
  }
}

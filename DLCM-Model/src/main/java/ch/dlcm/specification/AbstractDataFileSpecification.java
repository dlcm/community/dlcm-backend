/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AbstractDataFileSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.AbstractDataFile;

public abstract class AbstractDataFileSpecification<T extends AbstractDataFile> extends SolidifySpecification<T> {

  protected AbstractDataFileSpecification(T criteria) {
    super(criteria);
  }

  @Serial
  private static final long serialVersionUID = -3720307028195989725L;

  protected void setDataFileCriteria(Root<T> root, CriteriaBuilder builder, List<Predicate> predicatesList, AbstractDataFile df) {
    if (df.getSourceData() != null) {
      predicatesList.add(builder.like(root.get("sourceData").as(String.class), "%" + df.getSourceData() + "%"));
    }
    if (df.getFinalData() != null) {
      predicatesList.add(builder.like(root.get("finalData").as(String.class), "%" + df.getFinalData() + "%"));
    }
    if (df.getStatus() != null) {
      predicatesList.add(builder.equal(root.get("status"), df.getStatus()));
    }
    if (df.getStatusMessage() != null) {
      predicatesList.add(builder.equal(root.get("statusMessage"), df.getStatusMessage()));
    }
    if (df.getDataCategory() != null) {
      predicatesList.add(builder.equal(root.get("dataCategory"), df.getDataCategory()));
    }
    if (df.getDataType() != null) {
      predicatesList.add(builder.equal(root.get("dataType"), df.getDataType()));
    }
    if (df.getFileSize() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("fileSize"), df.getFileSize()));
    }
    if (df.getRelativeLocation() != null) {
      predicatesList.add(builder.equal(root.get("relativeLocation"), df.getRelativeLocation()));
    }
    if (this.criteria.getInfoPackage() != null) {
      predicatesList.add(builder.equal(root.get("infoPackage").get(DLCMConstants.DB_RES_ID), this.criteria.getInfoPackage().getResId()));
    }
  }
}

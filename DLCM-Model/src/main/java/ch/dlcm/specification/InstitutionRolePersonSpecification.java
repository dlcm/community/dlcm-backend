/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - InstitutionRolePersonSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.Join3TiersSpecification;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.InstitutionPersonRole;

public class InstitutionRolePersonSpecification extends Join3TiersSpecification<InstitutionPersonRole> {
  @Serial
  private static final long serialVersionUID = 1L;

  public InstitutionRolePersonSpecification(InstitutionPersonRole joinCriteria) {
    super(joinCriteria, InstitutionPersonRole.PATH_TO_INSTITUTION, InstitutionPersonRole.PATH_TO_ROLE,
            InstitutionPersonRole.PATH_TO_PERSON);
  }

  @Override
  protected String getParentId() {
    return this.joinCriteria.getInstitution().getResId();
  }

  @Override
  protected String getChildId() {
    return this.joinCriteria.getRole().getResId();
  }

  @Override
  protected String getGrandChildId() {
    return this.joinCriteria.getPerson().getResId();
  }

  @Override
  protected void completeJoinPredicatesList(Root<InstitutionPersonRole> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
  }

  /**
   * Filter on structure
   *
   * @param root
   * @param query
   * @param builder
   * @param predicatesList
   */
  @Override
  protected void completeChildPredicatesList(Root<InstitutionPersonRole> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {

    Role role = this.joinCriteria.getRole();
    Path<?> rolePath = this.getChildPath(root);

    if (role.getName() != null) {
      predicatesList.add(builder.like(rolePath.get("name"), "%" + role.getName() + "%"));
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveUserRatingSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.model.settings.ArchiveUserRating;

public class ArchiveUserRatingSpecification extends SolidifySpecification<ArchiveUserRating> {

  @Serial
  private static final long serialVersionUID = 920478099704286757L;

  public ArchiveUserRatingSpecification(ArchiveUserRating criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<ArchiveUserRating> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getArchiveId() != null) {
      predicatesList.add(builder.equal(root.get("archiveId"), this.criteria.getArchiveId()));
    }
    if (this.criteria.getRatingType() != null) {
      predicatesList.add(builder.equal(root.get("ratingType").get("resId"), this.criteria.getRatingType().getResId()));
    }
    if (this.criteria.getUser() != null) {
      predicatesList.add(builder.equal(root.get("user").get("resId"), this.criteria.getUser().getResId()));
    }
    if (this.criteria.getGrade() != null) {
      predicatesList.add(builder.lessThanOrEqualTo(root.get("grade"), this.criteria.getGrade()));
    }
  }
}

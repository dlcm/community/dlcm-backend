/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrganizationalUnitDisseminationPolicySpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.Join2TiersSpecification;

import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.model.policies.OrganizationalUnitDisseminationPolicy;

public class OrganizationalUnitDisseminationPolicySpecification extends Join2TiersSpecification<OrganizationalUnitDisseminationPolicy> {

  @Serial
  private static final long serialVersionUID = 4385379482004302361L;

  public OrganizationalUnitDisseminationPolicySpecification(OrganizationalUnitDisseminationPolicy joinCriteria) {
    super(joinCriteria, OrganizationalUnitDisseminationPolicy.PATH_TO_ORG_UNIT,
            OrganizationalUnitDisseminationPolicy.PATH_TO_DISSEMINATION_POLICY);
  }

  @Override
  protected void completeJoinPredicatesList(Root<OrganizationalUnitDisseminationPolicy> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    // Do nothing
  }

  @Override
  protected void completeChildPredicatesList(Root<OrganizationalUnitDisseminationPolicy> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    DisseminationPolicy policy = this.joinCriteria.getDisseminationPolicy();
    Path<?> policyPath = this.getChildPath(root);

    if (policy.getName() != null) {
      predicatesList.add(builder.equal(policyPath.get("name"), policy.getName()));
    }

    if (policy.getType() != null) {
      predicatesList.add(builder.equal(policyPath.get("type"), policy.getType()));
    }
  }

  @Override
  protected String getParentId() {
    return this.joinCriteria.getOrganizationalUnit().getResId();
  }

  @Override
  protected String getChildId() {
    return this.joinCriteria.getDisseminationPolicy().getResId();
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositSearchSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SearchSpecification;
import ch.unige.solidify.util.SearchCriteria;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.preingest.Deposit;

public class DepositSearchSpecification extends SearchSpecification<Deposit> {

  public DepositSearchSpecification(SearchCriteria criteria) {
    super(criteria);
  }

  @Override
  public Predicate toPredicate(Root<Deposit> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final List<Predicate> listPredicate = new ArrayList<>();
    if (this.getCriteria().getKey().equals("title")) {
      listPredicate.add(builder.like(root.get("title"), "%" + this.getCriteria().getValue() + "%"));
    }
    if (this.getCriteria().getKey().equals("status")) {
      listPredicate.add(builder.equal(root.get("status"), this.getCriteria().getValue()));
    }
    if (this.getCriteria().getKey().equals("statusMessage")) {
      listPredicate.add(builder.equal(root.get("statusMessage"), this.getCriteria().getValue()));
    }
    if (this.getCriteria().getKey().equals("publicationDate")) {
      listPredicate.add(builder.equal(root.get("publicationDate"), this.getCriteria().getValue()));
    }
    if (this.getCriteria().getKey().equals("collectionBegin")) {
      listPredicate.add(builder.greaterThanOrEqualTo(root.get("collectionBegin"), (OffsetDateTime) this.getCriteria().getValue()));
    }
    if (this.getCriteria().getKey().equals("collectionEnd")) {
      listPredicate.add(builder.greaterThanOrEqualTo(root.get("collectionEnd"), (OffsetDateTime) this.getCriteria().getValue()));
    }
    if (this.getCriteria().getKey().equals("languageId")) {
      listPredicate.add(builder.equal(root.get("languageId"), this.getCriteria().getValue()));
    }
    if (this.getCriteria().getKey().equals(DLCMConstants.ORG_UNIT_ID_FIELD)) {
      listPredicate.add(builder.equal(root.get(DLCMConstants.ORG_UNIT_ID_FIELD), this.getCriteria().getValue()));
    }
    if (this.getCriteria().getKey().equals("access")) {
      listPredicate.add(builder.equal(root.get("access"), this.getCriteria().getValue()));
    }
    if (this.getCriteria().getKey().equals("dataSensitivity")) {
      listPredicate.add(builder.equal(root.get("dataSensitivity"), this.getCriteria().getValue()));
    }
    if (this.getCriteria().getKey().equals("metadataVersion")) {
      listPredicate.add(builder.equal(root.get("metadataVersion"), this.getCriteria().getValue()));
    }

    if (this.getCriteria().getKey().equals("creation.who")) {
      listPredicate.add(builder.equal(root.get("creation").get("who"), this.getCriteria().getValue()));
    }
    return builder.and(listPredicate.toArray(new Predicate[listPredicate.size()]));
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - UserSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.model.security.User;

public class UserSpecification extends SolidifySpecification<User> {
  @Serial
  private static final long serialVersionUID = 6657807556908159308L;

  private ApplicationRole minimumApplicationRole;

  public UserSpecification(User criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
    if (this.criteria.getExternalUid() != null) {
      predicatesList.add(builder.equal(root.get("externalUid"), this.criteria.getExternalUid()));
    }

    if (this.criteria.getFirstName() != null) {
      predicatesList.add(builder.like(root.get("firstName"), "%" + this.criteria.getFirstName() + "%"));
    }

    if (this.criteria.getLastName() != null) {
      predicatesList.add(builder.like(root.get("lastName"), "%" + this.criteria.getLastName() + "%"));
    }

    if (this.criteria.getEmail() != null) {
      predicatesList.add(builder.like(root.get("email"), "%" + this.criteria.getEmail() + "%"));
    }

    if (this.criteria.getPerson() != null) {
      predicatesList.add(builder.equal(root.get("person").get("resId"), this.criteria.getPerson().getResId()));
    }

    if (this.minimumApplicationRole != null && this.minimumApplicationRole.getLevel() > 0) {
      predicatesList.add(
              builder.and(builder.lessThanOrEqualTo(root.join("applicationRole").get("level"), this.getMinimumApplicationRole().getLevel())));
    }

  }

  public ApplicationRole getMinimumApplicationRole() {
    return this.minimumApplicationRole;
  }

  public void setMinimumApplicationRole(ApplicationRole applicationRole) {
    this.minimumApplicationRole = applicationRole;
  }
}

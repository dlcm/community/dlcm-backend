/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - SubjectAreaSearchSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.model.Label;
import ch.unige.solidify.specification.SearchSpecification;
import ch.unige.solidify.util.SearchCriteria;

import ch.dlcm.model.settings.SubjectArea;

public class SubjectAreaSearchSpecification extends SearchSpecification<SubjectArea> {
  @Serial
  private static final long serialVersionUID = 6564894135210541812L;

  private String languageId;

  public SubjectAreaSearchSpecification(SearchCriteria criteria) {
    super(criteria);
  }

  @Override
  public Predicate toPredicate(Root<SubjectArea> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final List<Predicate> listPredicate = new ArrayList<>();

    if (this.getCriteria().getKey().equals("source") || this.getCriteria().getKey().equals("code")) {
      listPredicate.add(super.toPredicate(root, query, builder));
    }

    if (this.getCriteria().getKey().equals("name") && this.getLanguageId() != null) {
      Join<SubjectArea, Label> labelsJoin = root.join("labels", JoinType.LEFT);
      query.distinct(true);
      listPredicate.add(builder.or(builder.like(root.get("name"), "%" + this.getCriteria().getValue() + "%"),
              builder.and(builder.like(labelsJoin.get("text"), "%" + this.getCriteria().getValue() + "%"),
                      builder.and(labelsJoin.get("language").get("resId").in(this.getLanguageId())))));

    }
    return builder.and(listPredicate.toArray(new Predicate[listPredicate.size()]));
  }

  public String getLanguageId() {
    return this.languageId;
  }

  public void setLanguageId(String languageId) {
    this.languageId = languageId;
  }

}

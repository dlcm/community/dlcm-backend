/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.settings.OrganizationalUnit;

public class DepositSpecification extends SolidifySpecification<Deposit> implements FilterableByOrganizationalUnitsSpecification<Deposit> {
  @Serial
  private static final long serialVersionUID = -6041194178186645712L;

  private List<OrganizationalUnit> organizationalUnits;

  public DepositSpecification(Deposit criteria) {
    super(criteria);
  }

  @Override
  public Path<Object> getOrganizationalUnitPath(Root<Deposit> root) {
    return root.get(DLCMConstants.ORG_UNIT_ID_FIELD);
  }

  @Override
  public void setOrganizationalUnits(List<OrganizationalUnit> organizationalUnits) {
    this.organizationalUnits = organizationalUnits;
  }

  @Override
  protected void completePredicatesList(Root<Deposit> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
    if (this.criteria.getTitle() != null) {
      predicatesList.add(builder.like(root.get("title"), "%" + this.criteria.getTitle() + "%"));
    }
    if (this.criteria.getStatus() != null) {
      predicatesList.add(builder.equal(root.get("status"), this.criteria.getStatus()));
    }
    if (this.criteria.getStatusMessage() != null) {
      predicatesList.add(builder.equal(root.get("statusMessage"), this.criteria.getStatusMessage()));
    }
    if (this.criteria.getPublicationDate() != null) {
      predicatesList.add(builder.equal(root.get("publicationDate"), this.criteria.getPublicationDate()));
    }

    if (this.criteria.getCollectionBegin() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("collectionBegin"), this.criteria.getCollectionBegin()));
    }
    if (this.criteria.getCollectionEnd() != null) {
      predicatesList.add(builder.lessThanOrEqualTo(root.get("collectionEnd"), this.criteria.getCollectionEnd()));
    }
    if (this.criteria.getLanguageId() != null) {
      predicatesList.add(builder.equal(root.get("languageId"), this.criteria.getLanguageId()));
    }
    if (this.criteria.getOrganizationalUnitId() != null) {
      predicatesList.add(builder.equal(root.get(DLCMConstants.ORG_UNIT_ID_FIELD), this.criteria.getOrganizationalUnitId()));
    }
    if (this.criteria.getAccess() != null) {
      predicatesList.add(builder.equal(root.get("access"), this.criteria.getAccess()));
    }
    if (this.criteria.getDataSensitivity() != null) {
      predicatesList.add(builder.equal(root.get("dataSensitivity"), this.criteria.getDataSensitivity()));
    }
    if (this.criteria.getMetadataVersion() != null) {
      predicatesList.add(builder.equal(root.get("metadataVersion"), this.criteria.getMetadataVersion()));
    }
    if (!StringTool.isNullOrEmpty(this.criteria.getCreatedBy())) {
      predicatesList.add(builder.equal(root.get("creation").get("who"), this.criteria.getCreatedBy()));
    }

    /***/

    final Predicate organizationalUnitsPredicate = this.getInOrganizationalUnitsPredicate(root, this.organizationalUnits);
    if (organizationalUnitsPredicate != null) {
      // Search only for Deposits linked to the org units or that are public without embargo
      Predicate publicOrAuthorizedOrgUnitPredicate = builder.or(
              this.getNoEmbargoAndPublicAccessPredicate(root, builder),
              organizationalUnitsPredicate);
      predicatesList.add(publicOrAuthorizedOrgUnitPredicate);
    }

    /***/
  }

  @Override
  public Path<Object> getFinalAccessPath(Root<Deposit> root) {
    return root.get("access");
  }

  @Override
  public Path<Object> getEmbargoAccessPath(Root<Deposit> root) {
    return root.get("embargo").get("access");
  }

}

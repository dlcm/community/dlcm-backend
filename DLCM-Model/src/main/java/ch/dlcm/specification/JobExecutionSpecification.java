/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - JobExecutionSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.preservation.JobExecution;

public class JobExecutionSpecification extends SolidifySpecification<JobExecution> {
  @Serial
  private static final long serialVersionUID = -1117262920607646140L;

  public JobExecutionSpecification(JobExecution criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<JobExecution> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getPreservationJob() != null) {
      predicatesList.add(builder.equal(root.get("preservationJob").get(DLCMConstants.DB_RES_ID), this.criteria.getPreservationJob().getResId()));
    }

    if (this.criteria.getStatus() != null) {
      predicatesList.add(builder.equal(root.get("status"), this.criteria.getStatus()));
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - NotificationTypeSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.model.notification.NotificationType;

public class NotificationTypeSpecification extends SolidifySpecification<NotificationType> {
  @Serial
  private static final long serialVersionUID = 5108064187924281905L;

  public NotificationTypeSpecification(NotificationType criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<NotificationType> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (NotificationType.getAllNotificationTypes().stream().anyMatch(nt -> this.criteria.getResId().equals(nt.getResId()))) {
      predicatesList.add(builder.equal(root.get("resId"), this.criteria.getResId()));
    }
    if (this.criteria.getNotificationCategory() != null) {
      predicatesList.add(builder.equal(root.get("notificationCategory"), this.criteria.getNotificationCategory()));
    }
  }
}

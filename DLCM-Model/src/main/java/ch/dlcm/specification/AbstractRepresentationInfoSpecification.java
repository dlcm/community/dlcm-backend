/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AbstractRepresentationInfoSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.oais.InfoPackageInterface;
import ch.dlcm.model.oais.RepresentationInfo;

public abstract class AbstractRepresentationInfoSpecification<T extends Resource & InfoPackageInterface> extends SolidifySpecification<T> {

  @Serial
  private static final long serialVersionUID = 5300901412219062937L;

  protected AbstractRepresentationInfoSpecification(T criteria) {
    super(criteria);
  }

  protected void setRepresentationInfoCriteria(Root<T> root, CriteriaBuilder builder, List<Predicate> predicatesList, RepresentationInfo info) {
    final Path<RepresentationInfo> proxy = root.get("info");
    if (info.getName() != null) {
      predicatesList.add(builder.like(proxy.get("name"), "%" + info.getName() + "%"));
    }
    if (info.getDescription() != null) {
      predicatesList.add(builder.like(proxy.get("description"), "%" + info.getDescription() + "%"));
    }
    if (info.getStatus() != null) {
      predicatesList.add(builder.equal(proxy.get("status"), info.getStatus()));
    }
    if (info.getStatusMessage() != null) {
      predicatesList.add(builder.equal(proxy.get("statusMessage"), info.getStatusMessage()));
    }
    if (!StringTool.isNullOrEmpty(info.getOrganizationalUnitId())) {
      predicatesList.add(builder.equal(proxy.get(DLCMConstants.ORG_UNIT_ID_FIELD), info.getOrganizationalUnitId()));
    }
    if (info.getAccess() != null) {
      predicatesList.add(builder.equal(proxy.get("access"), info.getAccess()));
    }
    if (info.getDataSensitivity() != null) {
      predicatesList.add(builder.equal(proxy.get("dataSensitivity"), info.getDataSensitivity()));
    }
    if (info.getMetadataVersion() != null) {
      predicatesList.add(builder.equal(proxy.get("metadataVersion"), info.getMetadataVersion()));
    }

  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositDtoSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification.dto;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.dlcm.model.dto.DepositDto;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.specification.DepositSpecification;

public class DepositDtoSpecification extends DepositSpecification {

  @Serial
  private static final long serialVersionUID = 8211850081460450461L;

  public DepositDtoSpecification(DepositDto criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<Deposit> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {

    // Add Deposit criteria
    super.completePredicatesList(root, query, builder, predicatesList);

    // Add DepositDto criteria
    DepositDto criteriaDto = (DepositDto) this.criteria;
    if (criteriaDto.getStatusList() != null) {
      List<Predicate> statusListPredicates = new ArrayList<>();
      for (Deposit.DepositStatus status : criteriaDto.getStatusList()) {
        statusListPredicates.add(builder.equal(root.get("status"), status));
      }
      Predicate statusListPredicate = builder.or(statusListPredicates.toArray(new Predicate[statusListPredicates.size()]));
      predicatesList.add(statusListPredicate);
    }
  }
}

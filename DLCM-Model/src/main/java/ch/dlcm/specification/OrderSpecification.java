/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrderSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.model.access.Order;

public class OrderSpecification extends SolidifySpecification<Order> {
  @Serial
  private static final long serialVersionUID = 2597608881483815709L;

  public OrderSpecification(Order criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
    if (this.criteria.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }
    if (this.criteria.getStatus() != null) {
      predicatesList.add(builder.equal(root.get("status"), this.criteria.getStatus()));
    }
    if (this.criteria.getStatusMessage() != null) {
      predicatesList.add(builder.equal(root.get("statusMessage"), this.criteria.getStatusMessage()));
    }
    if (this.criteria.getQuery() != null) {
      predicatesList.add(builder.like(root.get("query"), "%" + this.criteria.getQuery() + "%"));
    }
    if (this.criteria.getQueryType() != null) {
      predicatesList.add(builder.equal(root.get("queryType"), this.criteria.getQueryType()));
    }
    if (this.criteria.isPublicOrder() != null) {
      predicatesList.add(builder.equal(root.get("publicOrder"), this.criteria.isPublicOrder()));
    }
    if (!StringTool.isNullOrEmpty(this.criteria.getCreatedBy())) {
      predicatesList.add(builder.equal(root.get("creation").get("who"), this.criteria.getCreatedBy()));
    }
  }

}

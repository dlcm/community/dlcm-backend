/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - PreservationInfoSpecificationInterface.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.dlcm.model.policies.PreservationPolicyInterface;

public interface PreservationInfoSpecificationInterface<T extends PreservationPolicyInterface> {

  default void setPreservationInfoCriteria(Root<T> root, CriteriaBuilder builder, List<Predicate> predicatesList,
          PreservationPolicyInterface info) {
    if (info.getDispositionApproval() != null) {
      predicatesList.add(builder.equal(root.get("dispositionApproval"), info.getDispositionApproval()));
    }
    if (info.getRetention() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("retention"), info.getRetention()));
    }
  }
}

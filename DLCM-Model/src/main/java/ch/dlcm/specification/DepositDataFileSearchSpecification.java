/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositDataFileSearchSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import static ch.dlcm.DLCMConstants.INFO_PACKAGE_RES_ID_KEY;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SearchSpecification;
import ch.unige.solidify.util.SearchCriteria;

import ch.dlcm.model.preingest.DepositDataFile;

public class DepositDataFileSearchSpecification extends SearchSpecification<DepositDataFile> {

  @Serial
  private static final long serialVersionUID = 2005824476567990740L;

  public DepositDataFileSearchSpecification(SearchCriteria criteria) {
    super(criteria);
  }

  @Override
  public Predicate toPredicate(Root<DepositDataFile> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final List<Predicate> listPredicate = new ArrayList<>();

    if (this.getCriteria().getKey().equals(INFO_PACKAGE_RES_ID_KEY)) {
      listPredicate.add(super.toPredicate(root, query, builder));
    } else {
      final Path path = root.get(this.getCriteria().getKey());
      listPredicate.add(super.toPredicate(path, query, builder));
    }
    return builder.and(listPredicate.toArray(new Predicate[listPredicate.size()]));
  }
}

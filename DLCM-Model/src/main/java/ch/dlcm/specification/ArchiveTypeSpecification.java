/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchiveTypeSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.model.settings.ArchiveType;

public class ArchiveTypeSpecification extends SolidifySpecification<ArchiveType> {
  @Serial
  private static final long serialVersionUID = -2755065274535080971L;

  public ArchiveTypeSpecification(ArchiveType archiveType) {
    super(archiveType);
  }

  @Override
  protected void completePredicatesList(Root<ArchiveType> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {

    if (this.criteria.getMasterType() != null && this.criteria.getMasterType().getResId() != null) {
      if (Boolean.parseBoolean(this.criteria.getMasterType().getResId())) {
        predicatesList.add(builder.isNull(root.get("masterType")));
      } else {
        predicatesList.add(builder.equal(root.get("masterType").get("resId"), this.criteria.getMasterType().getResId()));
      }
    }

    if (this.criteria.getTypeName() != null) {
      predicatesList.add(builder.like(root.get("typeName"), "%" + this.criteria.getTypeName() + "%"));
    }

  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - SubjectAreaSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.model.Label;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.model.settings.SubjectArea;

public class SubjectAreaSpecification extends SolidifySpecification<SubjectArea> {
  @Serial
  private static final long serialVersionUID = 2058331647094289096L;

  private String languageId;

  public SubjectAreaSpecification(SubjectArea subjectArea) {
    super(subjectArea);
  }

  @Override
  protected void completePredicatesList(Root<SubjectArea> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getSource() != null) {
      predicatesList.add(builder.equal(root.get("source"), this.criteria.getSource()));
    }

    if (this.criteria.getCode() != null) {
      predicatesList.add(builder.like(root.get("code"), this.criteria.getCode() + "%"));
    }

    if (this.criteria.getName() != null && this.getLanguageId() == null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }

    if (this.criteria.getName() != null && this.getLanguageId() != null) {
      Join<SubjectArea, Label> labelsJoin = root.join("labels", JoinType.LEFT);
      query.distinct(true);
      predicatesList.add(builder.or(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"),
              builder.and(builder.like(labelsJoin.get("text"), "%" + this.criteria.getName() + "%"),
                      builder.and(labelsJoin.get("language").get("resId").in(this.getLanguageId())))));
    }

  }

  public void setLanguageId(String languageId) {
    this.languageId = languageId;
  }

  public String getLanguageId() {
    return this.languageId;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ArchivalInfoPackageSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.settings.OrganizationalUnit;

public class ArchivalInfoPackageSpecification extends AbstractRepresentationInfoSpecification<ArchivalInfoPackage>
        implements FilterableByOrganizationalUnitsSpecification<ArchivalInfoPackage>,
        PreservationInfoSpecificationInterface<ArchivalInfoPackage> {
  @Serial
  private static final long serialVersionUID = -2506586572007496656L;

  private List<OrganizationalUnit> organizationalUnits;

  public ArchivalInfoPackageSpecification(ArchivalInfoPackage criteria) {
    super(criteria);
  }

  @Override
  public void setOrganizationalUnits(List<OrganizationalUnit> organizationalUnits) {
    this.organizationalUnits = organizationalUnits;
  }

  @Override
  protected void completePredicatesList(Root<ArchivalInfoPackage> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {

    if (this.criteria.getInfo() != null) {
      this.setRepresentationInfoCriteria(root, builder, predicatesList, this.criteria.getInfo());
    }

    if (this.criteria.getArchiveContainer() != null) {
      predicatesList.add(builder.equal(root.get("archiveContainer"), this.criteria.getArchiveContainer()));
    }

    if (this.criteria.getArchivalUnit() != null) {
      predicatesList.add(builder.equal(root.get("archivalUnit"), this.criteria.getArchivalUnit()));
    }

    if (this.criteria.getArchiveId() != null) {
      predicatesList.add(builder.like(root.get("archiveId"), "%" + this.criteria.getArchiveId() + "%"));
    }

    if (this.criteria.getArchiveSize() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("archiveSize"), this.criteria.getArchiveSize()));
    }

    if (this.criteria.getArchiveFileNumber() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("archiveFileNumber"), this.criteria.getArchiveFileNumber()));
    }

    if (this.criteria.getLastArchiving() != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("lastArchiving"), this.criteria.getLastArchiving()));
    }

    this.setPreservationInfoCriteria(root, builder, predicatesList, this.criteria);

    /***/

    final Predicate organizationalUnitsPredicate = this.getInOrganizationalUnitsPredicate(root, this.organizationalUnits);
    if (organizationalUnitsPredicate != null) {
      // Search only for AIP linked to the org units or that are public without embargo
      Predicate publicOrAuthorizedOrgUnitPredicate = builder.or(
              this.getNoEmbargoAndPublicAccessPredicate(root, builder),
              organizationalUnitsPredicate);
      predicatesList.add(publicOrAuthorizedOrgUnitPredicate);
    }

    /***/
  }
}

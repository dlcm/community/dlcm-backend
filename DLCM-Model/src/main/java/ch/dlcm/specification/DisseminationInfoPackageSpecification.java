/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DisseminationInfoPackageSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.settings.OrganizationalUnit;

public class DisseminationInfoPackageSpecification extends AbstractRepresentationInfoSpecification<DisseminationInfoPackage>
        implements FilterableByOrganizationalUnitsSpecification<DisseminationInfoPackage> {
  @Serial
  private static final long serialVersionUID = 2990117071718342472L;

  private List<OrganizationalUnit> organizationalUnits;

  public DisseminationInfoPackageSpecification(DisseminationInfoPackage criteria) {
    super(criteria);
  }

  @Override
  public void setOrganizationalUnits(List<OrganizationalUnit> organizationalUnits) {
    this.organizationalUnits = organizationalUnits;
  }

  @Override
  protected void completePredicatesList(Root<DisseminationInfoPackage> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getInfo() != null) {
      this.setRepresentationInfoCriteria(root, builder, predicatesList, this.criteria.getInfo());
    }

    /***/

    final Predicate organizationalUnitsPredicate = this.getInOrganizationalUnitsPredicate(root, this.organizationalUnits);
    if (organizationalUnitsPredicate != null) {
      // Search only for DIP linked to the org units or that are public without embargo
      Predicate publicOrAuthorizedOrgUnitPredicate = builder.or(
              this.getNoEmbargoAndPublicAccessPredicate(root, builder),
              organizationalUnitsPredicate);
      predicatesList.add(publicOrAuthorizedOrgUnitPredicate);
    }

    /***/
  }
}

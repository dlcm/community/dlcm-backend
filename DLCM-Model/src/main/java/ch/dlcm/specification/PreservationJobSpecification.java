/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - PreservationJobSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.model.preservation.PreservationJob;

public class PreservationJobSpecification extends SolidifySpecification<PreservationJob> {
  @Serial
  private static final long serialVersionUID = -625379431350978987L;

  public PreservationJobSpecification(PreservationJob criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<PreservationJob> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }

    if (this.criteria.getJobType() != null) {
      predicatesList.add(builder.equal(root.get("jobType"), this.criteria.getJobType()));
    }

    if (this.criteria.getJobRecurrence() != null) {
      predicatesList.add(builder.equal(root.get("jobRecurrence"), this.criteria.getJobRecurrence()));
    }

    if (this.criteria.getEnable() != null) {
      predicatesList.add(builder.equal(root.get("enable"), this.criteria.getEnable()));
    }

    if (this.criteria.getMaxItems() > 0) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("maxItems"), this.criteria.getMaxItems()));
    }

    if (this.criteria.getParameters() != null) {
      predicatesList.add(builder.like(root.get("parameters"), "%" + this.criteria.getParameters() + "%"));
    }

  }
}

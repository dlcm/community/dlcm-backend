/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DLCMDocConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm;

import ch.unige.solidify.SolidifyConstants;

public class DLCMDocConstants {

  private DLCMDocConstants() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  public static final String PACKAGE_RETENTION = "The retention duration in days of the AIP: O means forever.";
  public static final String PACKAGE_DISPOSAL_APPROVAL = "If the approval step the AIP disposal is mandatory.";
  public static final String AIP_COLLECTION_SIZE = "The number of AIPs in collection AIP.";
  public static final String ARCHIVE_UPDATED_METADATE = "If the package contains updated metadata.";
  public static final String ARCHIVE_PUBLIC_STRUCTURE = "If the archive structure content is public.";
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DLCM3XmlNamespacePrefixMapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm;

public class DLCM3XmlNamespacePrefixMapper extends DLCMXmlNamespacePrefixMapper {

  public DLCM3XmlNamespacePrefixMapper() {
    super();
    this.addSchema(DLCMConstants.METS_NAMESPACE, "mets");
    this.addSchema(DLCMConstants.DATACITE_NAMESPACE_4, "datacite");
    this.addSchema(DLCMConstants.PREMIS_NAMESPACE_3, "premis");
    this.addSchema(DLCMConstants.DLCM_NAMESPACE_3, "dlcm");
  }
}

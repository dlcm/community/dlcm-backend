/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DLCMConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm;

import java.util.Arrays;
import java.util.List;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;

import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

public class DLCMConstants {

  private DLCMConstants() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  // Modules
  public static final String DLCM = "DLCM";
  public static final String DLCM_SOLUTION = "DLCM Solution";
  public static final String DLCM_WEB_SITE = "https://www.dlcm.ch";
  public static final String DLCM_URI_FINAL_ACCESS_LEVEL = DLCM_WEB_SITE + "/access-level/final";
  public static final String DLCM_URI_EMBARGO_ACCESS_LEVEL = DLCM_WEB_SITE + "/access-level/embargo";
  public static final String DLCM_URI_DATA_TAG = DLCM_WEB_SITE + "/data-tag";
  public static final String DLCM_URI_DATA_USE_POLICY = DLCM_WEB_SITE + "/data-use-policy";

  // URLs
  // @formatter:off
  private static final String[] PUBLIC_URLS = {
          "/docs/**",
          "/DLCM.png",
          "/DLCM.svg",
          "/" + SolidifyConstants.URL_DASHBOARD,
          "/css/*.css",
          "/.well-known/jwks.json",
          "/swagger-ui.html",
          "/swagger-config",
          "/swagger-ui/**",
          "/api-docs",
          "/api-docs.yaml",
          "/api-docs/**",
          "/" + DLCMActionName.LIST_DATA_CATEGORY,
          "/" + DLCMActionName.LIST_DATA_TYPE,
          "/" + OAIConstants.OAI_RESOURCE,
          "/" + SolidifyConstants.SCHEMA,
          "/" + ResourceName.PROFILE,
          "/" + DLCMActionName.LIST_VERSION,
          "/" + SolidifyConstants.XSL,
          "/" + ModuleName.ADMIN + "/" + SolidifyConstants.ORCID + "/" + ActionName.LANDING,
          "/" + ModuleName.ADMIN + "/" + SolidifyConstants.ORCID + "/*/" + ActionName.ORCID_EXTERNAL_WEBSITES,
          "/" + ModuleName.ADMIN + "/" + ResourceName.SYSTEM_PROPERTY,
          "/" + ModuleName.ADMIN + "/" + ResourceName.GLOBAL_BANNER + "/" + DLCMActionName.GET_ACTIVE,
          "/" + ModuleName.ADMIN + "/" + ResourceName.LICENSE + "/*/" + DLCMActionName.DOWNLOAD_LOGO,
          "/" + ModuleName.ADMIN + "/" + ResourceName.LICENSE + "/*",
          "/" + ModuleName.ADMIN + "/" + ResourceName.INSTITUTION + "/*/" + DLCMActionName.DOWNLOAD_LOGO,
          "/" + ModuleName.ADMIN + "/" + ResourceName.INSTITUTION + "/*",
          "/" + ModuleName.ADMIN + "/" + ResourceName.FUNDING_AGENCY + "/*/" + DLCMActionName.DOWNLOAD_LOGO,
          "/" + ModuleName.ADMIN + "/" + ResourceName.FUNDING_AGENCY + "/*",
          "/" + ModuleName.ACCESS + "/" + ResourceName.SYSTEM_PROPERTY,
          "/" + ModuleName.ACCESS + "/" + ResourceName.ARCHIVE + "/**",
          "/" + ModuleName.ACCESS + "/" + ResourceName.ORG_UNIT + "/**",
          "/" + ModuleName.ACCESS + "/" + ResourceName.PUBLIC_METADATA + "/**",
          "/" + ModuleName.ACCESS + "/" + ResourceName.PUBLIC_METADATA + "/search",
          "/" + ModuleName.ACCESS + "/" + ResourceName.PUBLIC_METADATA + "/*/" + ResourceName.DATAFILE + "/*/" + ActionName.DOWNLOAD,
          "/" + ModuleName.ACCESS + "/" + ActionName.SITEMAP,
          "/" + ModuleName.ACCESS + "/" + ResourceName.DIP + "/*/" + ActionName.DOWNLOAD,
          "/" + ModuleName.ACCESS + "/" + ResourceName.DIP + "/*/" + ResourceName.DATAFILE + "/*/" + ActionName.DOWNLOAD,
          "/" + ModuleName.ACCESS + "/" + ResourceName.AIP + "/*/" + ActionName.DOWNLOAD,
          "/" + ModuleName.ACCESS + "/" + ResourceName.ORDER + "/*/" + ActionName.DOWNLOAD,
          "/" + ModuleName.DATAMGMT + "/" + ResourceName.PUBLIC_METADATA + "/**",
          "/" + ModuleName.DATAMGMT + "/" + ResourceName.ARCHIVE_PUBLIC_DATA + "/**",
          "/" + ModuleName.PRES_PLANNING + "/" + ResourceName.MODULE,
          "/" + ModuleName.ARCHIVALSTORAGE + "/" + ResourceName.AIP + "/*/" + ActionName.DOWNLOAD,
          "/" + ModuleName.PREINGEST + "/" + ResourceName.DEPOSIT + "/*/" + ActionName.DOWNLOAD,
          "/" + ModuleName.PREINGEST + "/" + ResourceName.DEPOSIT + "/*/" + DLCMActionName.GET_ANONYMIZED_DEPOSIT_PAGE,
          "/" + ModuleName.PREINGEST + "/" + ResourceName.DEPOSIT + "/*/" + DLCMActionName.DOWNLOAD_ANONYMIZED_DEPOSIT,
          "/" + ModuleName.PREINGEST + "/" + ResourceName.DEPOSIT + "/*/" + ResourceName.DATAFILE + "/*/" + ActionName.DOWNLOAD,
          "/" + ModuleName.INGEST + "/" + ResourceName.SIP + "/*/" + ActionName.DOWNLOAD,
          "/" + ModuleName.INGEST + "/" + ResourceName.SIP + "/*/" + ResourceName.DATAFILE + "/*/" + ActionName.DOWNLOAD,
          "/" + ActionName.DOWNLOAD,
          "/" + OAIConstants.OAI_MODULE + "/" + OAIConstants.OAI_PROVIDER,
          "/" + OAIConstants.OAI_MODULE + "/" + OAIConstants.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE,
          "/" + OAIConstants.OAI_MODULE + "/" + OAIConstants.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE + "/" + SolidifyConstants.XSL,
          "/" + OAIConstants.OAI_RESOURCE,
          "/" + OAIConstants.OAI_RESOURCE + "/" + SolidifyConstants.XSL,
          "/" + ActionName.SITEMAP_XML,
          "/" + ActionName.SITEMAP + "-*" + ActionName.XML_EXTENSION,

  };
  private static final String[] DISABLED_CSRF_URLS = {
          "/" + ModuleName.ACCESS + "/" + ResourceName.PUBLIC_METADATA + "/*/prepare-download",
          "/" + ModuleName.ACCESS + "/" + ResourceName.PUBLIC_METADATA + "/search",
          "/" + OAIConstants.OAI_RESOURCE
  };
  // @formatter:on

  // Default Values
  public static final int DAYS_BY_YEAR = 365;
  public static final int DAYS_BY_MONTH = 30;
  public static final int YEARS_BY_CENTURY = 100;
  public static final String POLICY = "policy";
  public static final String RETENTION = "retention";
  public static final String DISPOSITION_APPROVAL = "disposition approval";
  public static final int ORDER_INCREMENT = 10;

  // Database column name
  public static final String DB_RES_ID = SolidifyConstants.DB_RES_ID; // "resId"
  public static final String DB_ORG_UNIT_ID = "orgUnitId";
  public static final String DB_POLICY_ID = "policyId";
  public static final String DB_INSTITUTION_ID = "institutionId";
  public static final String DB_PERSON_ID = "personId";
  public static final String DB_ROLE_ID = "roleId";
  public static final String DB_AGENCY_ID = "agencyId";
  public static final String DB_PACKAGE_ID = "packageId";
  public static final String DB_SIP_ID = "sipId";
  public static final String DB_AIP_ID = "aipId";
  public static final String DB_DIP_ID = "dipId";
  public static final String DB_COLLECTION_ID = "collectionId";
  public static final String DB_ORDER_ID = "orderId";
  public static final String DB_FILE_ID = "fileId";
  public static final String DB_JOB_ID = "jobId";
  public static final String DB_JOB_EXECUTION_ID = "jobExecutionId";
  public static final String DB_USER_ID = "userId";
  public static final String DB_RATING_TYPE_ID = "ratingTypeId";
  public static final String DB_ARCHIVE_ID = "archiveId";
  public static final String DB_DEPOSIT_ID = "depositId";
  public static final String DB_DOMAIN_ID = "domainId";
  public static final String DB_LANGUAGE_ID = SolidifyConstants.DB_LANGUAGE_ID; // "languageId"
  public static final String CREATION_FIELD = "creation";
  public static final String DB_LOGO_ID = "logoId";
  public static final String DB_AVATAR_ID = "avatarId";
  public static final String DB_THUMBNAIL_ID = "thumbnailId";
  public static final String DB_DUA_ID = "duaId";
  public static final String SUBMISSION_AGREEMENT_FILE_ID = "submissionAgreementFileId";
  public static final String DB_INDEX_FIELD_ALIAS_ID = "indexFieldAliasId";
  public static final String DB_NOTIFICATION_TYPE_ID = "notification_type_id";

  public static final String ORG_UNIT_ID_FIELD = "organizationalUnitId";
  public static final String SUBMISSION_POLICY_ID_FIELD = "submissionPolicyId";
  public static final String PRESERVATION_POLICY_ID_FIELD = "preservationPolicyId";
  public static final String DISSEMINATION_POLICY_ID_FIELD = "disseminationPolicyId";
  public static final String DISSEMINATION_POLICY_TYPE_FIELD = "type";
  public static final String DISSEMINATION_POLICY_PARAMETERS_FIELD = "parameters";
  public static final String ORGANIZATIONAL_DISSEMINATION_POLICY_ID_FIELD = "organizationalUnitDisseminationPolicyId";
  public static final String ARCHIVE_TYPE_ID_FIELD = "archiveTypeId";
  public static final String DB_SUBJECT_AREA_ID = "subjectAreaId";
  public static final String DB_SUBMISSION_AGREEMENT_ID = "submissionAgreementId";

  // Dissemination policy
  public static final String DP_NAME_PARAM = "name";
  public static final String DISSEMINATION_POLICY_BASIC_ID = "basic-dissemination-policy";
  public static final String DISSEMINATION_POLICY_OAIS_ID = "oais-dissemination-policy";
  public static final String DISSEMINATION_POLICY_IIIF_ID = "iiif-dissemination-policy";
  public static final String DISSEMINATION_POLICY_HEDERA_ID = "hedera-dissemination-policy";
  public static final String DP_HEDERA_PROJECT_PARAM = "project";
  public static final String DP_HEDERA_SERVER_PARAM = "server";

  public static final String DP_HEDERA_RDF_DATA_FILE_SOURCE_FOLDER_PARAM = "rdfDataFileSourceDirectory";
  public static final String DP_HEDERA_SOURCE_DATASET_FILE_SOURCE_FOLDER_PARAM = "sourceDatasetFileSourceDirectory";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_IIIF_SOURCE_FOLDER_PARAM = "researchDataFilesIiifSourceDirectory";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_SOURCE_FOLDER_PARAM = "researchDataFilesManifestIiifSourceDirectory";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_TEI_SOURCE_FOLDER_PARAM = "researchDataFilesTeiSourceDirectory";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_WEB_SOURCE_FOLDER_PARAM = "researchDataFilesWebSourceDirectory";

  public static final String DP_HEDERA_RDF_DATA_FILE_TARGET_FOLDER_PARAM = "rdfDataFileTargetDirectory";
  public static final String DP_HEDERA_SOURCE_DATASET_FILE_TARGET_FOLDER_PARAM = "sourceDatasetFileTargetDirectory";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_IIIF_TARGET_FOLDER_PARAM = "researchDataFilesIiifTargetDirectory";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_TARGET_FOLDER_PARAM = "researchDataFilesManifestIiifTargetDirectory";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_TEI_TARGET_FOLDER_PARAM = "researchDataFilesTeiTargetDirectory";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_WEB_TARGET_FOLDER_PARAM = "researchDataFilesWebTargetDirectory";

  public static final String DP_HEDERA_RDF_DATA_FILE_PATTERN_PARAM = "rdfDataFilePattern";
  public static final String DP_HEDERA_SOURCE_DATASET_FILE_PATTERN_PARAM = "sourceDatasetFilePattern";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_IIIF_PATTERN_PARAM = "researchDataFilesIiifPattern";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_PATTERN_PARAM = "researchDataFilesManifestIiifPattern";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_TEI_PATTERN_PARAM = "researchDataFilesTeiPattern";
  public static final String DP_HEDERA_RESEARCH_DATA_FILES_WEB_PATTERN_PARAM = "researchDataFilesWebPattern";

  public static final String DP_IIIF_SERVER_PARAM = "server";
  public static final String DP_IIIF_CONTEXT_PARAM = "contextPath";
  public static final String DP_IIIF_MEDIA_FOLDER_PARAM = "mediaDirectory";
  public static final String DP_IIIF_MEDIA_PATTERNS_PARAM = "mediaPatterns";
  public static final String DP_IIIF_MANIFESTS_FOLDER_PARAM = "manifestDirectory";
  public static final String DP_IIIF_MANIFESTS_PATTERNS_PARAM = "manifestPatterns";

  // Index
  public static final String PRIVATE = "private";

  // Facets
  public static final String INDEXING_KEYWORD = ".keyword";
  public static final String METADATA_VERSION_FACET = "metadata-versions";
  public static final String ORG_UNIT_FACET = "organizational-units";
  public static final String ACCESS_LEVEL_FACET = "access-levels";
  public static final String DATA_TAG_FACET = "data-tags";
  public static final String DATA_USE_POLICY_FACET = "data-use-policies";
  public static final String CREATOR_FACET = "contributors";
  public static final String FORMAT_FACET = "formats";
  public static final String PATH_FACET = "paths";
  public static final String TYPE_FACET = "types";
  public static final String RETENTION_FACET = "retentions";
  public static final String ARCHIVE_FACET = "archive-types";
  public static final String INSTITUTION_FACET = "institutions";
  public static final String INSTITUTION_DESC_FACET = "institution-descriptions";
  public static final String PUBLICATION_DATE_ALIAS = "publicationDate";

  // Field

  public static final String CUSTOM_METADATA_FIELD = "custom-metadata";
  public static final String ITEMS_FIELD = "items";
  public static final String ITEM_FIELD = "item";
  public static final String ID_FIELD = "id";
  public static final String TYPE_FIELD = "type";
  public static final String ARCHIVE_ID_FIELD = "archiveId";
  public static final String CONTENT_FIELD = "content";
  public static final String CONTENT_TYPE_FIELD = "content-type";
  public static final String LANGUAGE_FIELD = "xml:lang";
  public static final String KEYWORDS = "keywords";
  public static final String FAMILY_NAME = "familyName";
  public static final String GIVEN_NAME = "givenName";
  public static final String PERSON_ORCID = "orcid";
  public static final String DATE_TYPE = "dateType";
  public static final String ALTERNATE_ID_TYPE = "alternateIdentifierType";

  // Schema
  public static final String XSL_HOME = "xslt";

  // DLCM folders
  public static final String ANONYMIZED_DEPOSITS_FOLDER = "anonymized-deposits";
  public static final String METADATA_FOLDER = "metadata";
  public static final String SCHEMA_FOLDER = SolidifyConstants.SCHEMA_HOME;
  public static final String DOC_FOLDER = "documentation";
  public static final String DATA_FOLDER = "data";
  public static final String RESEARCH_DATA_FOLDER = "researchdata";
  public static final String ADMINISTRATION_DATA_FOLDER = "administrativedata";
  public static final String SOFTWARE_FOLDER = "software";
  public static final String PACKAGE = "package";
  public static final String TOMBSTONE = "tombstone";
  public static final String NEW_PACKAGE = "new-package";
  public static final String INTERNAL_FOLDER = "dlcm";
  public static final String UPDATE_FOLDER = "update";

  // DLCM Mime-type
  public static final String DEPOSIT_MIME_TYPE = "application/dlcm-deposit";
  public static final String SIP_MIME_TYPE = "application/dlcm-sip";
  public static final String AIP_MIME_TYPE = "application/dlcm-aip";
  public static final String DIP_MIME_TYPE = "application/dlcm-dip";

  // DLCM AIP
  public static final String AIP = "aip";
  public static final String AIP_PREFIX = "aip-";
  public static final String AIP_CREATION = AIP_PREFIX + CREATION_FIELD;
  public static final String AIP_LAST_UPDATE = AIP_PREFIX + "last-update";
  public static final String AIP_CONTAINER = AIP_PREFIX + "container";
  public static final String AIP_UNIT = AIP_PREFIX + "unit";
  public static final String AIP_TYPE = AIP_PREFIX + "type";
  public static final String AIP_ORGA_UNIT = AIP_PREFIX + "organizational-unit";
  public static final String AIP_RETENTION = AIP_PREFIX + RETENTION;
  public static final String AIP_RETENTION_END = AIP_PREFIX + RETENTION + "-end";
  public static final String AIP_RETENTION_DURATION = AIP_PREFIX + RETENTION + "-duration";
  public static final String AIP_DISPOSITION_APPROVAL = AIP_PREFIX + "disposition-approval";
  public static final String AIP_SIZE = AIP_PREFIX + "size";
  public static final String AIP_ORGA_UNIT_NAME = AIP_PREFIX + "organizational-unit-name";
  public static final String AIP_SUBJECT_AREAS = AIP_PREFIX + "subject-areas";
  public static final String AIP_FILE_NUMBER = AIP_PREFIX + "file-number";
  public static final String AIP_METADATA_FILE_NUMBER = AIP_PREFIX + "metadata-file-number";
  public static final String AIP_TOTAL_FILE_NUMBER = AIP_PREFIX + "total-file-number";
  public static final String AIP_ACCESS_LEVEL = AIP_PREFIX + "access-level";
  public static final String AIP_EMBARGO_LEVEL = AIP_PREFIX + "embargo-level";
  public static final String AIP_EMBARGO_END_DATE = AIP_PREFIX + "embargo-date";
  public static final String AIP_LICENSE = AIP_PREFIX + "license";
  public static final String AIP_TOMBSTONE_SIZE = AIP_PREFIX + "tombstone-size";
  public static final String AIP_TITLE = AIP_PREFIX + "title";
  public static final String AIP_COLLECTION = AIP_PREFIX + "collection";

  public static final String AIP_CONTENT_STRUCTURE_PUBLIC = AIP_PREFIX + "content-structure-public";

  public static final String AIP_DUA = AIP_PREFIX + "dua";
  public static final String AIP_DUA_CONTENT_TYPE = AIP_DUA + "-" + CONTENT_TYPE_FIELD;
  public static final String AIP_THUMBNAIL = AIP_PREFIX + "thumbnail";
  public static final String AIP_THUMBNAIL_CONTENT_TYPE = AIP_THUMBNAIL + "-" + CONTENT_TYPE_FIELD;
  public static final String AIP_README = AIP_PREFIX + "readme";
  public static final String AIP_README_CONTENT_TYPE = AIP_README + "-" + CONTENT_TYPE_FIELD;
  public static final String AIP_DATA_TAG = AIP_PREFIX + "data-tag";

  public static final String AIP_DATA_USE_POLICY = AIP_PREFIX + "data-use-policy";
  public static final String AIP_COMPLIANCE_LEVEL = AIP_PREFIX + "compliance-level";
  public static final String AIP_INSTITUTIONS = AIP_PREFIX + INSTITUTION_FACET;
  public static final String AIP_INSTITUTION_DESCRIPTIONS = AIP_PREFIX + INSTITUTION_DESC_FACET;
  public static final String AIP_ISSUED_DATE = AIP_PREFIX + "issued-date";

  // DLCM metadata files
  public static final String METADATA_FILE = "dlcm.xml";
  public static final String REPRESENTATION_INFO_METADATA_FILE_PREFIX = "dlcm-representation-info-";
  public static final String REPRESENTATION_INFO_METADATA_FILE_PATTERN = REPRESENTATION_INFO_METADATA_FILE_PREFIX + "*";
  public static final String UPDATED_METADATA_FILE_PREFIX = "updated-metadata-";
  public static final String UPDATED_METADATA_FILE_PATTERN = UPDATED_METADATA_FILE_PREFIX + "*";
  public static final String METS_METADATA_FILE = "dlcm-mets.xml";
  public static final String UPDATE_PACKAGE_FILE = "update-package";
  public static final String UPDATED_ADMINISTRATIVE_INFO_FILE = "updated-files-administrative";
  public static final String METADATA_MIGRATION_XLS_PREFIX = "dlcm4migration";

  public static final String METS = "METS";

  public static final String TEMP_PREMIS_ID = "xxxx";

  // DLCM ARCHIVE
  public static final String DLCM_ARCHIVE = "archive";
  // Archive Thumbnails
  public static final String THUMBNAIL_EXTENSION = ".thumbnail";
  public static final String DATASET_THUMBNAIL = "dataset" + THUMBNAIL_EXTENSION;
  public static final String ARCHIVE_THUMBNAIL = DLCM_ARCHIVE + THUMBNAIL_EXTENSION;
  // Archive README
  public static final String README_EXTENSION = ".readme";
  public static final String ARCHIVE_README = DLCM_ARCHIVE + README_EXTENSION;
  // Archive DUA
  public static final String DUA_EXTENSION = ".dua";
  public static final String ARCHIVE_DUA = DLCM_ARCHIVE + DUA_EXTENSION;

  // DLCM Metadata
  public static final String FILE_ROOT = "DLCM Files Root";
  public static final String STRUCTURE_MAP = "DLCM Structural Map";
  public static final String METADATA_ROOT = "metadata";
  public static final String DOC_ROOT = "documentation";
  public static final String RESEARCH_DATA_ROOT = "researchData";
  public static final String ADMIN_DATA_ROOT = "administrativeData";
  public static final String SOFTWARE_ROOT = "software";
  public static final String INTERNAL_ROOT = "dlcm";
  public static final String AGENT_SW = "Software";

  // Search
  public static final String SEARCH_ORG_UNIT = "orgUnit";

  // Metadata field name
  public static final String DATACITE_XML_FIELD = "datacite.xml";
  public static final String DATACITE_FIELD = "datacite";
  public static final String PREMIS_FIELD = "premis";
  public static final String METADATA_VERSION_FIELD = "metadata-version";
  public static final String DOI = "DOI";
  public static final String ARK = "ARK";
  public static final String HANDLE = "Handle";
  public static final String ABSTRACT = "Abstract";
  public static final String DATACITE_FAMILY_NAME = "datacite:familyName";
  public static final String DATACITE_GIVEN_NAME = "datacite:givenName";
  public static final String DATACITE_IDENTIFIER_NAME = "datacite:nameIdentifier";
  public static final String DATACITE_RELATIONS_NAME = "datacite:relatedIdentifiers";
  public static final String DATACITE_RELATION_NAME = "datacite:relatedIdentifier";
  public static final String DATACITE_RELATION_ID_TYPE_NAME = "relatedIdentifierType";
  public static final String DATACITE_RELATION_TYPE_NAME = "relationType";
  public static final String DATACITE_DESCRIPTION_TYPE_FIELD = "descriptionType";
  public static final String DATACITE_SUBJECTS_NAME = "datacite:subjects";
  public static final String DATACITE_RESOURCE_FIELD = "datacite.datacite:resource";
  public static final String IDENTIFIER_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:identifier.content";
  public static final String IDENTIFIER_TYPE_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:identifier.identifierType";
  public static final String ALTERNATE_IDENTIFIERS_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:alternateIdentifiers";
  public static final String ALTERNATE_IDENTIFIER_FIELD = ALTERNATE_IDENTIFIERS_FIELD + ".datacite:alternateIdentifier";
  public static final String RIGHTS_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:rightsList.datacite:rights";
  public static final String DATES_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:dates.datacite:date";
  public static final String CREATORS_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:creators";
  public static final String CREATOR_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:creators.datacite:creator";
  public static final String CREATOR_NAME_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:creators.datacite:creator.datacite:creatorName.content";
  public static final String FORMAT_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:formats.datacite:format";
  public static final String RESOURCE_TYPE_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:resourceType.content";
  public static final String RESOURCE_TYPE_GENERAL_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:resourceType.resourceTypeGeneral";
  public static final String TITLE_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:titles.datacite:title.content";
  public static final String PUBLISHER_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:publisher";
  public static final String DESCRIPTIONS_FIELD = DATACITE_RESOURCE_FIELD + ".datacite:descriptions";
  public static final String DESCRIPTION_FIELD = DESCRIPTIONS_FIELD + ".datacite:description";
  public static final String RELATIONS_FIELD = DATACITE_RESOURCE_FIELD + "." + DATACITE_RELATIONS_NAME;
  public static final String RELATION_FIELD = RELATIONS_FIELD + "." + DATACITE_RELATION_NAME;
  public static final String SUBJECT_LIST_FIELD = DATACITE_RESOURCE_FIELD + "." + DATACITE_SUBJECTS_NAME;
  public static final String SUBJECT_SCHEME_FIELD = DATACITE_RESOURCE_FIELD + "." + DATACITE_SUBJECTS_NAME + ".datacite:subject.subjectScheme";
  public static final String SUBJECT_CONTENT_FIELD = DATACITE_RESOURCE_FIELD + "." + DATACITE_SUBJECTS_NAME + ".datacite:subject.content";

  public static final String DATACITE_TYPE_BOOK = "Book";
  public static final String DATACITE_TYPE_BOOK_CHAPTER = "BookChapter";
  public static final String DATACITE_TYPE_CONFERENCE_PAPER = "ConferencePaper";
  public static final String DATACITE_TYPE_DATASET = "Dataset";
  public static final String DATACITE_TYPE_JOURNAL_ARTICLE = "JournalArticle";
  public static final String DATACITE_TYPE_PEER_REVIEW = "PeerReview";
  public static final String DATACITE_TYPE_REPORT = "Report";

  public static final String DATE_AVAILABLE_ATTRIBUTE = "Available";
  public static final String DATE_ISSUED_ATTRIBUTE = "Issued";
  public static final String DATE_CREATED_ATTRIBUTE = "Created";
  public static final String DATE_UPDATED_ATTRIBUTE = "Updated";
  public static final String DATE_ACCEPTED_ATTRIBUTE = "Accepted";
  public static final String DATE_COLLECTED_START_ATTRIBUTE = "CollectedStart";
  public static final String DATE_COLLECTED_END_ATTRIBUTE = "CollectedEnd";

  // BagIt metadata
  public static final String BAGIT_PROFILE = "BagIt-Profile-DLCM.json";
  public static final String BAGIT_DATE = "Bagging-Date";
  public static final String BAGIT_SOURCE = "Source-Organization";
  public static final String BAGIT_SOURCE_ADDR = "Organization-Address";
  public static final String BAGIT_ARC_ID = "DLCM-Id";
  public static final String BAGIT_METADATA_SCHEMA = "DLCM-Metadata-Schema";
  public static final String BAGIT_ARC_NAME = "DLCM-Title";
  public static final String BAGIT_ARC_RETENTION = "DLCM-Retention";
  public static final String BAGIT_ARC_RETENTION_DURATION = "DLCM-Retention-Duration";
  public static final String BAGIT_ARC_DISPOSITION_APPROVAL = "DLCM-Disposition-Approval";
  public static final String BAGIT_ORG_UNIT_ID = "DLCM-OrgUnit-Id";
  public static final String BAGIT_ORG_UNIT_NAME = "DLCM-OrgUnit-Name";
  public static final String BAGIT_PACKAGE_SIZE = "DLCM-Package-Size";
  public static final String BAGIT_PACKAGE_FILE_NUMBER = "DLCM-Package-File-Number";
  public static final String FORMAT_XML = "text/xml";

  // OAI
  public static final String OAI_ORG_UNIT = "orgunit";
  public static final String OAI_DATACITE = "datacite";
  public static final String OAI_SCHEMA_ORG = "schema.org";
  public static final String OAI_SCHEMA_ORG_NAME = "schema.org Dataset Metadata";
  public static final String OAI_SCHEMA_ORG_SCHEMA = "https://schema.org/Dataset";

  // DataCite
  public static final String DATACITE_NAME = "DataCite Metadata";
  public static final String DATACITE_SCHEMA_4_3 = "https://schema.datacite.org/meta/kernel-4.3/metadata.xsd";

  // XML Namespace
  public static final String DATACITE_NAMESPACE_4 = "http://datacite.org/schema/kernel-4";
  public static final String PREMIS_NAMESPACE_3 = "http://www.loc.gov/premis/v3";
  public static final String METS_NAMESPACE = "http://www.loc.gov/METS/";
  public static final String DLCM_SCHEMA_1 = "dlcm-1.0.xsd";
  public static final String DLCM_NAMESPACE_1 = "http://www.dlcm.ch/dlcm/v1";
  public static final String DLCM_SCHEMA_2 = "dlcm-2.0.xsd";
  public static final String DLCM_NAMESPACE_2 = "http://www.dlcm.ch/dlcm/v2";
  public static final String DLCM_SCHEMA_3 = "dlcm-3.0.xsd";
  public static final String DLCM_NAMESPACE_3 = "http://www.dlcm.ch/dlcm/v3";

  // Preservation Job
  public static final String REPORT_ID = "{reportId}";

  // Misc.
  public static final String UNKNOWN = "unknown";
  public static final String ORCID = "ORCID";
  public static final String ROR = "ROR";
  public static final String WEB = "WEB";
  public static final String GRID = "GRID";
  public static final String ISNI = "ISNI";
  public static final String CROSSREF_FUNDER = "CrossrefFunder";
  public static final String WIKIDATA = "Wikidata";
  public static final String SPDX = "SPDX";
  public static final String NO_REPLY_PREFIX = "noreply-";
  public static final String REASON = "reason";
  public static final String GRADE = "grade";
  public static final String RATING_TYPE = "ratingType";
  public static final String ANT_ALL = "**";

  // Tests
  public static final String TEST_RES_ID = "test";

  // Message processor
  public static final String MESSAGE_PROCESSOR_NAME = "DLCM Message Processor";

  // XML
  public static final String XML_NAMESPACE_PREFIX_MAPPER = "org.glassfish.jaxb.namespacePrefixMapper";

  // JPA keys
  public static final String INFO_PACKAGE_RES_ID_KEY = "infoPackage.resId";

  // Emails
  public static final String NOTIFICATION_EMAIL_DLCM_PREFIX = "[DLCM]";
  public static final String NOTIFICATION_EMAIL_VALIDATION_PREFIX = "[s validation]";
  public static final String NOTIFICATION_IDS = "notificationIds";

  // Checksums
  public static final String FILE = "file";
  public static final String CATEGORY = "category";
  public static final String TYPE = "type";
  public static final String FOLDER = "folder";
  public static final String METADATA_TYPE = "metadataType";
  public static final String CHECKSUM_MD5 = "checksumMd5";
  public static final String CHECKSUM_MD5_ORIGIN = "checksumMd5Origin";
  public static final String CHECKSUM_SHA1 = "checksumSha1";
  public static final String CHECKSUM_SHA1_ORIGIN = "checksumSha1Origin";
  public static final String CHECKSUM_SHA256 = "checksumSha256";
  public static final String CHECKSUM_SHA256_ORIGIN = "checksumSha256Origin";
  public static final String CHECKSUM_SHA512 = "checksumSha512";
  public static final String CHECKSUM_SHA512_ORIGIN = "checksumSha512Origin";
  public static final String CHECKSUM_CRC32 = "checksumCrc32";
  public static final String CHECKSUM_CRC32_ORIGIN = "checksumCrc32Origin";

  // Web service Parameter
  public static final String ARCHIVAL_UNIT_PARAM = "archivalUnit";
  public static final String ARCHIVE_MASTER_TYPE_ID_PARAM = "masterType.resId";
  public static final String IDENTIFIER_TYPE_PARAM = "identifierType";

  // Licenses
  public static final String OPEN_LICENSE_ID_FIELD = "openLicenseId";
  public static final String DOMAIN_CONTENT_FIELD = "domainContent";
  public static final String DOMAIN_DATA_FIELD = "domainData";
  public static final String DOMAIN_SOFTWARE_FIELD = "domainSoftware";
  public static final String IS_GENERIC_FIELD = "isGeneric";
  public static final String OD_CONFORMANCE_FIELD = "odConformance";
  public static final String OSD_CONFORMANCE_FIELD = "osdConformance";
  public static final String PERCENT_ENCODING_SPACE_CHAR = "%20";
  public static final String PERCENT_ENCODING_PLUS_CHAR = "%2B";

  private static final List<String> LICENSE_DLCM_CHECK_URI_LIST = Arrays
          .asList(DLCMConstants.DLCM_URI_DATA_TAG, DLCMConstants.DLCM_URI_FINAL_ACCESS_LEVEL, DLCMConstants.DLCM_URI_EMBARGO_ACCESS_LEVEL);

  public static final List<String> BAGIT_FIXED_FILES = Arrays.asList("bag-info.txt", "bagit.txt");

  public static String[] getPublicUrls() {
    return PUBLIC_URLS.clone();
  }

  public static String[] getDisabledCsrfUrls() {
    return DISABLED_CSRF_URLS.clone();
  }

  public static List<String> getUriListWithoutLicense() {
    return LICENSE_DLCM_CHECK_URI_LIST;
  }

}

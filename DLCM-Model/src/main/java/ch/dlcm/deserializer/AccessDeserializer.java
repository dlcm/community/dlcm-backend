/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AccessDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import ch.unige.solidify.util.StringTool;

import ch.dlcm.model.Access;

/**
 * Custom deserializer allowing to accept an empty string for a null value.
 * <p>
 * Useful to support clients sending blank values instead of 'null' in JSON.
 */
public class AccessDeserializer extends JsonDeserializer<Access> {

  @Override
  public Access deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {

    final String value = jsonParser.readValueAs(String.class);
    if (StringTool.isNullOrEmpty(value)) {
      return null;
    }

    return Access.valueOf(value);
  }
}

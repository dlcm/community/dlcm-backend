/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OffsetDateTimeDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.deserializer;

import java.io.IOException;
import java.io.Serial;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Deserializer that accepts multiple formats for OffsetDateTime, such as:
 * <p>
 * - 2000-08-01T13:20:00+01:00 - 2000-08-01T13:20:00+0100 - 2000-08-01T13:20+01:00 -
 * 2000-08-01T13:20:00.200+01:00 - 2000-08-01T13:20:00Z
 */
public class OffsetDateTimeDeserializer extends StdDeserializer<OffsetDateTime> {

  @Serial
  private static final long serialVersionUID = 5207311049821632168L;

  String pattern = "[yyyy-MM-dd'T'HH:mm[:ss][.SSS][.SS][.S][XXX][XX][X][Z]]";

  public OffsetDateTimeDeserializer() {
    // empty constructor required to run in annotation
    this(null);
  }

  public OffsetDateTimeDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public OffsetDateTime deserialize(JsonParser parser, DeserializationContext context) throws IOException {

    final String dateText = parser.getText();
    if (dateText.isEmpty()) {
      return null;
    } else {
      final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(this.pattern);
      return OffsetDateTime.parse(dateText, formatter);
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - PackageListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.listener;

import jakarta.persistence.PostPersist;
import jakarta.persistence.PostUpdate;

import ch.unige.solidify.config.SolidifyEventPublisher;

import ch.dlcm.message.AipMessage;
import ch.dlcm.message.OrderMessage;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;

public class PackageListener {

  @PostPersist
  @PostUpdate
  private void publishEvent(Object object) {
    if (object instanceof Order) {
      this.publishOrderEvent((Order) object);
    }
    if (object instanceof ArchivalInfoPackage) {
      this.publishAipEvent((ArchivalInfoPackage) object);
    }

  }

  private void publishOrderEvent(Order order) {
    if (order.getStatus() == OrderStatus.SUBMITTED) {
      SolidifyEventPublisher.getPublisher().publishEvent(new OrderMessage(order.getResId()));
    }
  }

  private void publishAipEvent(ArchivalInfoPackage aip) {
    if (aip.getInfo().getStatus() == PackageStatus.IN_PREPARATION && aip.getArchiveContainer() == ArchiveContainer.UNDEFINED) {
      SolidifyEventPublisher.getPublisher().publishEvent(new AipMessage(aip.getResId()));
    }
  }

}

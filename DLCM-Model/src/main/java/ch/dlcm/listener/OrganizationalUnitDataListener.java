/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - OrganizationalUnitDataListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.listener;

import jakarta.persistence.PostPersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.config.SolidifyEventPublisher;

import ch.dlcm.message.OrganizationalUnitMessage;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.InfoPackageInterface;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.preingest.Deposit;

public class OrganizationalUnitDataListener {
  private static final Logger log = LoggerFactory.getLogger(OrganizationalUnitDataListener.class);

  public void logData(String orgUnitId) {
    log.trace("Sending message {}", orgUnitId);
    SolidifyEventPublisher.getPublisher().publishEvent(new OrganizationalUnitMessage(orgUnitId));
  }

  @PostPersist
  private void logCreation(Object object) {
    if (object instanceof ArchivalInfoPackage || object instanceof SubmissionInfoPackage) {
      this.logData((((InfoPackageInterface) object).getInfo()).getOrganizationalUnitId());
    } else if (object instanceof Deposit) {
      this.logData(((Deposit) object).getOrganizationalUnitId());
    }
  }
}

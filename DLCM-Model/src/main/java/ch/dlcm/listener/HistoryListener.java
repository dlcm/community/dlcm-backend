/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - HistoryListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.listener;

import java.time.Duration;
import java.time.OffsetDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.persistence.PostPersist;
import jakarta.persistence.PostRemove;
import jakarta.persistence.PostUpdate;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.rest.Resource;

import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.oais.InfoPackageInterface;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preservation.JobExecution;

public class HistoryListener {
  private static final Logger log = LoggerFactory.getLogger(HistoryListener.class);

  @PostPersist
  private void logCreation(Object object) {
    final Resource res = (Resource) object;

    /*
     * Send a message to store object creation in history
     */
    final StatusHistory stsHistory = new StatusHistory(res.getClass().getSimpleName(), res.getResId(),
            this.generateCreationTimeForCreationStatus(res),
            res.getCreatedBy());
    log.trace("Sending message {}", stsHistory);
    SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);

    /*
     * Send a message to store first object's status in history
     */
    this.logStatusHistory(object);
  }

  @PostUpdate
  @PostRemove
  private void logStatusHistory(Object object) {
    StatusHistory stsHistory = null;
    if (object instanceof Deposit deposit) {
      stsHistory = new StatusHistory(deposit.getClass().getSimpleName(), deposit.getResId(), deposit.getUpdateTime(),
              deposit.getStatus().toString(), deposit.getStatusMessage(), deposit.getUpdatedBy());
    } else if (object instanceof AbstractDataFile df) {
      stsHistory = new StatusHistory(df.getClass().getSimpleName(), df.getResId(), df.getUpdateTime(), df.getStatus().toString(),
              df.getStatusMessage(), df.getUpdatedBy());
    } else if (object instanceof InfoPackageInterface ip) {
      stsHistory = new StatusHistory(ip.getClass().getSimpleName(), ip.getResId(), ((Resource) ip).getUpdateTime(),
              ip.getInfo().getStatus().toString(),
              ip.getInfo().getStatusMessage(), ((Resource) ip).getUpdatedBy());
    } else if (object instanceof Order order) {
      stsHistory = new StatusHistory(order.getClass().getSimpleName(), order.getResId(), order.getUpdateTime(), order.getStatus().toString(),
              order.getStatusMessage(), order.getUpdatedBy());
    } else if (object instanceof JobExecution execution) {
      stsHistory = new StatusHistory(execution.getClass().getSimpleName(), execution.getResId(), execution.getUpdateTime(),
              execution.getStatus().toString(), execution.getStatusMessage(), execution.getUpdatedBy());
    }
    if (stsHistory != null) {
      log.trace("Sending event {}", stsHistory);
      SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);
    }
  }

  private OffsetDateTime generateCreationTimeForCreationStatus(Resource res) {
    // Subtract 1 ms to correct the ascending descending sort.
    // The event created is not exactly the same time as the first StatusHistory
    return res.getCreationTime().minus(Duration.ofMillis(1));
  }
}

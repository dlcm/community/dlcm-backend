/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DataFileListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.listener;

import java.io.IOException;
import java.net.URI;

import jakarta.persistence.PostPersist;
import jakarta.persistence.PostRemove;
import jakarta.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.message.DataFileMessage;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;

public class DataFileListener {

  private static final Logger log = LoggerFactory.getLogger(DataFileListener.class);

  @PostPersist
  @PostUpdate
  private void publishEvent(Object object) {

    final AbstractDataFile df = (AbstractDataFile) object;

    if (df.getStatus() == DataFileStatus.RECEIVED
            || df.getStatus() == DataFileStatus.CHANGE_RELATIVE_LOCATION
            || df.getStatus() == DataFileStatus.CHANGE_DATA_CATEGORY) {
      SolidifyEventPublisher.getPublisher().publishEvent(new DataFileMessage(df.getClass(), df.getResId()));
    }
  }

  /**
   * Delete files from disk when a DataFile is deleted from database.
   * Note: @PostRemove methods are called while the process is still in the JPA transaction. This means that if an exception is raised
   * within the method, the DELETE is rolled back in database.
   *
   * @param object
   */
  @PostRemove
  public void removeFile(Object object) {
    final AbstractDataFile df = (AbstractDataFile) object;

    URI sourceDataURI = df.getSourceData();
    URI finalDataURI = df.getFinalData();

    if (finalDataURI != null) {
      if (FileTool.isFile(finalDataURI)) {
        // file exists on disk --> delete it
        try {
          FileTool.deleteFile(finalDataURI);
          log.info("{} '{}' deleted from disk", df.getClass().getSimpleName(), finalDataURI);
        } catch (final IOException e) {
          throw new SolidifyFileDeleteException(finalDataURI.toString(), e);
        }
      } else if (df.getStatus() == DataFileStatus.READY) {
        // file doesn't exist on disk although Datafile is READY --> it is an error
        log.error("{} '{}' cannot be deleted from disk as it doesn't exist although its status is READY", df.getClass().getSimpleName(),
                finalDataURI);
        throw new SolidifyFileDeleteException(finalDataURI.toString());
      } else {
        // file doesn't exist on disk, but Datafile is not READY --> ĵust log a warning
        log.warn("{} '{}' cannot be deleted as it doesn't exist on disk", df.getClass().getSimpleName(), finalDataURI);
      }
    }

    // sourceData may still have a value even if the file has already been deleted from disk (this is the case on DepositDataFile upload
    // for instance) --> try to delete only if it still exists
    if (sourceDataURI != null && sourceDataURI.getScheme() != null &&
        sourceDataURI.getScheme().equals("file") && FileTool.isFile(sourceDataURI)) {
      try {
        FileTool.deleteFile(sourceDataURI);
        log.info("{} '{}' deleted from disk", df.getClass().getSimpleName(), sourceDataURI);
      } catch (final IOException e) {
        throw new SolidifyFileDeleteException(sourceDataURI.toString(), e);
      }
    }
  }
}

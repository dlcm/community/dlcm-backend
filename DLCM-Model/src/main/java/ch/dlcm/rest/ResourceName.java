/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - ResourceName.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.rest;

import ch.unige.solidify.SolidifyConstants;

public class ResourceName {

  public static final String AIP = "aip";

  public static final String AIP_FILE = "aip-package";
  public static final String ALL_METADATA = "all-metadata";
  public static final String ARCHIVE = "archives";
  public static final String AUTHORIZED_ORG_UNIT = "authorized-organizational-units";
  public static final String AUTHORIZED_INSTITUTION = "authorized-institutions";
  public static final String CONTRIBUTOR = "contributors";
  public static final String DATAFILE = "data";
  // Resources
  public static final String DEPOSIT = "deposits";
  public static final String DI = "di";
  public static final String DI_PRIVATE = "private-di";
  public static final String DI_PUBLIC = "public-di";
  public static final String DIP = "dip";
  public static final String DIS_POLICY = "dissemination-policies";
  public static final String FUNDING_AGENCY = "funding-agencies";
  public static final String ARCHIVE_ACL = "archive-acl";
  public static final String ARCHIVE_PUBLIC_DATA = "archive-public-data";
  public static final String ARCHIVE_STATISTICS = "archive-statistics";
  public static final String ARCHIVE_RATING = "archive-ratings";
  public static final String AVERAGE_RATING = "average-ratings";
  public static final String RATING_TYPE = "rating-types";

  public static final String OAUTH2_CLIENT = "oauth2-clients";

  // Miscellaneous
  public static final String HISTORY = "history";
  public static final String INSTITUTION = "institutions";
  public static final String ITEM = "items";
  public static final String LANGUAGE = "languages";
  public static final String LICENSE = "licenses";
  public static final String LOGIN = "shiblogin";
  public static final String MEMBER = "members";
  public static final String METADATA_TYPE = "metadata-types";
  public static final String MODULE = "modules";
  public static final String MONITOR = "monitor";
  public static final String NOTIFICATION = "notifications";
  public static final String NOTIFICATION_TYPES = "notification-types";
  public static final String ADDITIONAL_FIELDS_FORMS = "additional-fields-forms";
  public static final String CURRENT_VERSION = "current-version";
  public static final String ORCID = "orcid";
  public static final String ORDER = "orders";
  public static final String ORG_UNIT = "organizational-units";
  public static final String PERSON = "people";
  public static final String POLICY = "policy";
  public static final String PRES_JOB = "preservation-jobs";
  public static final String PRES_JOB_EXECUTION = "executions";
  public static final String PRES_POLICY = "preservation-policies";
  public static final String PRIVATE_METADATA = "private-metadata";
  public static final String PROFILE = "profile";
  public static final String PUBLIC_METADATA = "metadata";
  public static final String SUBJECT_AREA = "subject-areas";
  public static final String SCHEDULED_TASKS = "scheduled-tasks";
  public static final String ARCHIVE_TYPE = "archive-types";
  public static final String GLOBAL_BANNER = "global-banners";
  public static final String ORCID_SYNCHRONIZATION = "orcid-synchronizations";

  public static final String RESERVE_DOI = "reserve-doi";
  public static final String ROLE = "roles";
  public static final String ROLE_APPLICATION = "application-roles";
  public static final String SIP = "sip";
  public static final String SIP_FILE = "sip-package";
  public static final String STORED_AIP = "stored-aip";
  public static final String SUB_POLICY = "submission-policies";
  public static final String SUBMISSION_AGREEMENT = "submission-agreements";
  public static final String TOTAL_COUNT_FOR_COMPLETED_ORDER = "total-for-completed-order";
  public static final String PACKAGES = "packages";

  public static final String USER = "users";
  public static final String SYSTEM_PROPERTY = "system-properties";

  private ResourceName() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}

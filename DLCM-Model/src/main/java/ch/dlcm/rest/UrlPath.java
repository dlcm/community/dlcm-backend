/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - UrlPath.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.rest;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;

public final class UrlPath {

  // ==========
  // Access
  // ==========
  public static final String ACCESS = "/" + ModuleName.ACCESS;

  // @formatter:off

  public static final String ACCESS_AIP = ACCESS + "/" + ResourceName.AIP;
  public static final String ACCESS_ALL_METADATA = ACCESS + "/" + ResourceName.ALL_METADATA;
  public static final String ACCESS_DIP = ACCESS + "/" + ResourceName.DIP;

  public static final String ACCESS_ORDER = ACCESS + "/" + ResourceName.ORDER;
  public static final String ACCESS_ORG_UNIT= ACCESS + "/" + ResourceName.ORG_UNIT;
  public static final String ACCESS_PRIVATE_METADATA = ACCESS + "/" + ResourceName.PRIVATE_METADATA;
  public static final String ACCESS_PUBLIC_METADATA = ACCESS + "/" + ResourceName.PUBLIC_METADATA;
  public static final String ACCESS_ARCHIVE = ACCESS + "/" + ResourceName.ARCHIVE;
  // ==========
  // Admin
  // ==========
  public static final String ADMIN = "/" + ModuleName.ADMIN;

  public static final String ADMIN_AUTHORIZED_ORG_UNIT = ADMIN + "/" + ResourceName.AUTHORIZED_ORG_UNIT;
  public static final String ADMIN_AUTHORIZED_INSTITUTION = ADMIN + "/" + ResourceName.AUTHORIZED_INSTITUTION;
  public static final String ADMIN_DISSEMINATION_POLICY = ADMIN + "/" + ResourceName.DIS_POLICY;
  public static final String ADMIN_FUNDING_AGENCY = ADMIN + "/" + ResourceName.FUNDING_AGENCY;
  public static final String ADMIN_INSTITUTION = ADMIN + "/" + ResourceName.INSTITUTION;
  public static final String ADMIN_LANGUAGE = ADMIN + "/" + ResourceName.LANGUAGE;
  public static final String ADMIN_LICENSE = ADMIN + "/" + ResourceName.LICENSE;
  public static final String ADMIN_LICENSE_IMPORT_FILE = "/" + ActionName.IMPORT + "/" + ActionName.FILE;
  public static final String ADMIN_LICENSE_IMPORT_LIST = "/" + ActionName.IMPORT + "/" + ActionName.LIST;
  public static final String ADMIN_METADATA_TYPE = ADMIN + "/" + ResourceName.METADATA_TYPE;
  public static final String ADMIN_NOTIFICATION = ADMIN + "/" + ResourceName.NOTIFICATION;
  public static final String ADMIN_OPEN_LICENSE_IMPORT_FILE = "/" + ActionName.IMPORT + "/open-license/" + ActionName.FILE;
  public static final String ADMIN_PERSON = ADMIN + "/" + ResourceName.PERSON;
  public static final String ADMIN_MEMBER = ADMIN + "/" + ResourceName.MEMBER;
  public static final String ADMIN_PRESERVATION_POLICY = ADMIN + "/" + ResourceName.PRES_POLICY;
  public static final String ADMIN_SUBJECT_DOMAIN = ADMIN + "/" + ResourceName.SUBJECT_AREA;
  public static final String ADMIN_ROLE = ADMIN + "/" + ResourceName.ROLE;
  public static final String ADMIN_SUBMISSION_AGREEMENT = ADMIN + "/" + ResourceName.SUBMISSION_AGREEMENT;
  public static final String ADMIN_SUBMISSION_POLICY = ADMIN + "/" + ResourceName.SUB_POLICY;
  public static final String ADMIN_ORG_UNIT = ADMIN + "/" + ResourceName.ORG_UNIT;
  public static final String ADMIN_USER = ADMIN + "/" + ResourceName.USER;
  public static final String ADMIN_SYSTEM_PROPERTY = ADMIN + "/" + ResourceName.SYSTEM_PROPERTY;
  public static final String ADMIN_ROLE_APPLICATION = ADMIN + "/" + ResourceName.ROLE_APPLICATION;
  public static final String ADMIN_ARCHIVE_STATISTICS = ADMIN + "/" + ResourceName.ARCHIVE_STATISTICS;
  public static final String ADMIN_ARCHIVE_ACL = ADMIN + "/" + ResourceName.ARCHIVE_ACL;
  public static final String ADMIN_ARCHIVE_RATING = ADMIN + "/" + ResourceName.ARCHIVE_RATING;
  public static final String ADMIN_RATING_TYPE = ADMIN + "/" + ResourceName.RATING_TYPE;
  public static final String ADMIN_OAUTH2_CLIENT = ADMIN + "/" + ResourceName.OAUTH2_CLIENT;
  public static final String ADMIN_SCHEDULED_TASKS = ADMIN + "/" + ResourceName.SCHEDULED_TASKS;
  public static final String ADMIN_ARCHIVE_TYPE = ADMIN + "/" + ResourceName.ARCHIVE_TYPE;
  public static final String ADMIN_GLOBAL_BANNER = ADMIN + "/" + ResourceName.GLOBAL_BANNER;
  public static final String ADMIN_ORCID_SYNCHRONIZATION = ADMIN + "/" + ResourceName.ORCID_SYNCHRONIZATION;

  // ================
  // Archival Storage
  // ================
  public static final String ARCHIVAL_STORAGE = "/" + ModuleName.ARCHIVALSTORAGE;
  public static final String ARCHIVAL_STORAGE_AIP = ARCHIVAL_STORAGE + "/" + ResourceName.AIP;
  public static final String ARCHIVAL_STORAGE_STORED_AIP = ARCHIVAL_STORAGE + "/" + ResourceName.STORED_AIP;
  // =====================
  // Authorization Server
  // =====================
  public static final String AUTH = "/" + ModuleName.AUTH;
  public static final String AUTH_AUTHORIZE = AUTH + "/" + DLCMActionName.AUTHORIZE;
  public static final String AUTH_CHECK_TOKEN = AUTH + "/" + DLCMActionName.CHECK_TOKEN;
  public static final String AUTH_SHIBLOGIN = "/" + ResourceName.LOGIN;
  public static final String AUTH_TOKEN = AUTH + "/" + DLCMActionName.TOKEN;
  public static final String AUTH_USER_ID = "{" + DLCMActionName.USER_ID + "}";
  // ==========
  // Data Mgmt
  // ==========
  public static final String DATA_MGMT = "/" + ModuleName.DATAMGMT;
  public static final String DATA_MGMT_DI = DATA_MGMT + "/" + ResourceName.DI;
  public static final String DATA_MGMT_ITEM = DATA_MGMT + "/" + ResourceName.ITEM;
  public static final String DATA_MGMT_PRIVATE_DI = DATA_MGMT + "/" + ResourceName.DI_PRIVATE;
  public static final String DATA_MGMT_PUBLIC_DI = DATA_MGMT + "/" + ResourceName.DI_PUBLIC;
  public static final String DATA_MGMT_ARCHIVE_PUBLIC_DATA = DATA_MGMT + "/" + ResourceName.ARCHIVE_PUBLIC_DATA;

  // ==========
  // Ingest
  // ==========
  public static final String INGEST = "/" + ModuleName.INGEST;
  public static final String INGEST_SIP = INGEST + "/" + ResourceName.SIP;
  // ==========
  // Pre-ingest
  // ==========
  public static final String PREINGEST = "/" + ModuleName.PREINGEST;
  public static final String PREINGEST_CONTRIBUTOR = PREINGEST + "/" + ResourceName.CONTRIBUTOR;
  public static final String PREINGEST_DEPOSIT = PREINGEST + "/" + ResourceName.DEPOSIT;
  // ======================
  // Preservation Planning
  // ======================
  public static final String PRES_PLANNING = "/" + ModuleName.PRES_PLANNING;
  public static final String PRES_PLANNING_AIP = PRES_PLANNING + "/" + ResourceName.AIP;

  // @formatter on

  public static final String PRES_PLANNING_MODULE = PRES_PLANNING + "/" + ResourceName.MODULE;
  public static final String PRES_PLANNING_MONITOR = PRES_PLANNING + "/" + ResourceName.MONITOR;
  public static final String PRES_PLANNING_PRES_JOB = PRES_PLANNING + "/" + ResourceName.PRES_JOB;
  // ================
  // Resource Server
  // ================
  public static final String RES_SRV = "/" + ModuleName.RES_SRV;
  private UrlPath() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DLCMActionName.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.rest;

import ch.unige.solidify.SolidifyConstants;

public class DLCMActionName {

  public static final String APPROVE = "approve";
  public static final String ARCHIVAL_COLLECTIONS = "archivalCollections";
  public static final String ARCHIVAL_UNITS = "archivalUnits";
  public static final String AUTHENTICATED = "authenticated";
  public static final String AUTHORIZE = "authorize";

  public static final String SET_ROLE = "set-role";
  public static final String CHECK = "check";
  public static final String CHECK_FIXITY = "check-fixity";
  public static final String FIX_AIP_INFO = "fix-aip-info";
  public static final String CREATE_FROM_NOTIFICATION = "create-from-notification";
  public static final String PUT_IN_ERROR = "put-in-error";
  public static final String CHECK_TOKEN = "check_token";
  public static final String CLEAN = "clean";
  public static final String CHECK_COMPLIANCE = "check-compliance";
  public static final String CONTRIBUTOR_WITH_DEPOSITS = "with-deposits";
  public static final String APPROVE_SUBMISSION_AGREEMENT = "approve-submission-agreement";
  public static final String APPROVE_DISPOSAL = "approve-disposal";
  public static final String APPROVE_DISPOSAL_BY_ORGUNIT = "approve-disposal-by-orgunit";
  public static final String CHECK_SUBMISSION_AGREEMENT = "check-submission-agreement";
  public static final String EXTEND_RETENTION = "extend-retention";
  public static final String REPLICATE_TOMBSTONE = "replicate-tombstone";
  public static final String REPLICATE_PACKAGE = "replicate-package";
  public static final String DOWNLOAD_STATUS = "download-status";
  public static final String ENABLE_REVISION = "enable-revision";
  public static final String INBOX = "inbox";
  public static final String INIT = "initialize";
  public static final String LIST_ACCESS_TYPE = "list-accesses";

  public static final String LIST_AIP_CONTAINER = "list-aip-containers";
  public static final String LIST_AIP_WITH_STATUS = "list-aip-statuses";
  public static final String LIST_DATA_CATEGORY = "list-data-categories";
  public static final String LIST_DATA_TYPE = "list-data-types";
  public static final String LIST_DOWNLOAD_STATUS = "list-download-status";
  public static final String LIST_EXCLUDE_FILES = "list-exclude-files";

  public static final String LIST_FOLDERS = "list-folders";
  public static final String DELETE_FOLDER = "delete-folder";

  public static final String LIST_IGNORE_FILES = "list-ignore-files";
  public static final String LIST_JOB_RECURRENCE = "list-job-recurrences";
  public static final String LIST_JOB_TYPE = "list-job-types";
  public static final String LIST_METADATA_FORMAT = "list-metadata-formats";

  public static final String LIST_OAUTH2_GRANT_TYPE = "list-grant-types";
  public static final String LIST_QUERY_TYPE = "list-query-types";
  public static final String LIST_SUBJECT_AREA_SOURCES = "list-sources";

  public static final String LIST_STATUS = "list-status";
  public static final String LIST_CURRENT_STATUS = "list-current-status";
  public static final String LIST_VERSION = "list-metadata-versions";
  public static final String LIST_REFERENCE_TYPES = "list-reference-types";
  public static final String LIST_ARCHIVE_MASTER_TYPES = "list-master-types";
  public static final String LIST_ARCHIVE_TYPES_OF_MASTER_TYPE = "list-types";
  public static final String ORDER_LIST_CREATED_BY_USER = "list-created-by-user";
  public static final String ORDER_LIST_PUBLIC = "list-public";
  public static final String PREPARE_DOWNLOAD = "prepare-download";
  public static final String START_METADATA_EDITING = "start-metadata-editing";
  public static final String START_METADATA_UPGRADE = "start-metadata-upgrade";
  public static final String START_COMPLIANCE_LEVEL_UPDATE = "start-compliance-level-update";
  public static final String SET_APPROVED = "set-approved";
  public static final String SET_REFUSED = "set-refused";
  public static final String SET_PENDING = "set-pending";
  public static final String SET_READ = "set-read";
  public static final String SET_READ_ALL = "set-read-all";
  public static final String SET_UNREAD = "set-unread";
  public static final String SET_UNREAD_ALL = "set-unread-all";
  public static final String CANCEL_METADATA_EDITING = "cancel-metadata-editing";
  public static final String REINDEX = "reindex";

  public static final String RESUME_ALL = "resume-all";
  public static final String REJECT = "reject";
  public static final String REPORT = "reports";
  public static final String REPORT_LINES = "detail";
  public static final String REVOKE_ALL_TOKENS = "revoke-all-tokens";
  public static final String REVOKE_MY_TOKENS = "revoke-my-tokens";
  public static final String SEARCH_DOI = "search-doi";
  public static final String SEARCH_WITH_USER = "search-with-user";
  public static final String SENT = "sent";
  public static final String SUBMIT = "submit";
  public static final String RESUBMIT = "resubmit";
  public static final String SUBMIT_FOR_APPROVAL = "submit-for-approval";
  public static final String TOKEN = "token";
  public static final String USER_ID = "externalUid";
  public static final String NEW_USER = "new-user";
  public static final String VALIDATE = "validate";

  public static final String UPLOAD_METADATA = "upload-metadata";
  public static final String UPLOAD_LOGO = "upload-logo";
  public static final String UPLOAD_SUBMISSION_AGREEMENT = "upload-submission-agreement";
  public static final String DOWNLOAD_LOGO = "download-logo";
  public static final String DELETE_LOGO = "delete-logo";
  public static final String UPLOAD_FILE = "upload-file";
  public static final String DOWNLOAD_FILE = "download-file";
  public static final String DELETE_FILE = "delete-file";
  public static final String UPLOAD_AVATAR = "upload-avatar";
  public static final String DOWNLOAD_AVATAR = "download-avatar";
  public static final String DELETE_AVATAR = "delete-avatar";
  public static final String UPLOAD_DUA = "upload-dua";
  public static final String UPLOAD_DATASET_FILE = "upload-dataset-file";
  public static final String DOWNLOAD_DUA = "download-dua";
  public static final String DOWNLOAD_DATASET_FILE = "download-dataset-file";
  public static final String DOWNLOAD_ANONYMIZED_DEPOSIT = "download-anonymized-deposit";
  public static final String GET_ANONYMIZED_DEPOSIT_PAGE = "get-anonymized-deposit-page";
  public static final String DELETE_DUA = "delete-dua";
  public static final String DELETE_DATASET_FILE = "delete-dataset-file";
  public static final String READY_FOR_DOWNLOAD = "ready-for-download";

  public static final String THUMBNAIL = "thumbnail";
  public static final String DUA = "dua";
  public static final String README = "readme";
  public static final String STATISTICS = "statistics";
  public static final String RATING = "ratings";
  public static final String RATING_BY_USER = "list-for-user";
  public static final String BY_RATING = "by-rating";
  public static final String BIBLIOGRAPHIES = "bibliographies";
  public static final String CITATIONS = "citations";

  public static final String ADD_VIEW = "add-view";
  public static final String ADD_DOWNLOAD = "add-download";

  public static final String GET_ALL_APPROBATIONS = "get-all-approbations";
  public static final String GET_MY_ACLS = "get-my-acls";
  public static final String GET_MY_APPROBATIONS = "get-my-approbations";
  public static final String GENERATE_ANONYMIZED_DEPOSIT_PAGE = "generate-anonymized-deposit-page";
  public static final String LIST_INHERITED_ROLE = "list-inherited-role";
  public static final String LIST_INHERITED_PERSON_ROLES = "list-inherited-person-roles";

  public static final String PURGE_CONTENT_TEMP_FOLDER = "purge-temporary-folder";
  public static final String DELETE_NOTIFICATIONS_ON_DEPOSIT = "delete-notifications-on-deposit";
  public static final String REFRESH = "refresh";
  public static final String SYNCHRONIZE = "synchronize";
  public static final String SYNCHRONIZE_ALL_EXISTING = "synchronize-all-existing";

  public static final String SCHEDULE_ENABLED_TASKS = "schedule-enabled-tasks";
  public static final String DISABLE_TASKS_SCHEDULING = "disable-tasks-scheduling";
  public static final String KILL_TASK = "kill-task";

  public static final String BY_CONTRIBUTORS = "by-contributors";
  public static final String GET_BY_AIP_ID = "get-by-aip-id";

  public static final String GET_ACTIVE = "get-active";

  public static final String SYSTEM_PROPERTIES = "system-properties";
  public static final String LIST_ALL_ARCHIVE_ACL = "list-all-archive-acl";

  private DLCMActionName() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}

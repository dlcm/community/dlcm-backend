/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - AbstractDataFileTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tool;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.nio.file.Path;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.preingest.DepositDataFile;

class AbstractDataFileTest {

  @ParameterizedTest
  @ValueSource(strings = { "a + b", "漢文" })
  void fileNameFromSourceDataEncodingTest(String inputString) {
    DepositDataFile ddf = new DepositDataFile();
    ddf.setSourceData(Path.of(inputString).toUri());
    assertEquals(inputString, ddf.getFileName(), "Should be equal.");
  }

  @ParameterizedTest
  @ValueSource(strings = { "a + b", "漢文" })
  void fileNameFromFinalDataEncodingTest(String inputString) {
    DepositDataFile ddf = new DepositDataFile();
    ddf.setFinalData(Path.of(inputString).toUri());
    assertEquals(inputString, ddf.getFileName(), "Should be equal.");
  }

  @ParameterizedTest
  @CsvSource({ ", /",
  "/abc, /abc",
  "abc, /abc",
  "//abc, /abc",
  "/abc//d, /abc/d",
  "//abc//d/, /abc/d",
  "//abc//d//, /abc/d"})
  void checkRelativeLocationTest(String input, String expectedOutput) {
    final String actualOutput = AbstractDataFile.checkRelativeLocation(input);
    assertEquals(expectedOutput, actualOutput, "Should be equal.");
  }

  @ParameterizedTest
  @ValueSource(strings = { "..", "/..", "/../", "../..", "/abc/../..", "/../ab/..", "./a/../." })
  void checkRelativeLocationExceptionTest(String input) {
    assertThrows(IllegalArgumentException.class, () -> AbstractDataFile.checkRelativeLocation(input));
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - JAXBTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tool;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.transform.TransformerFactoryConfigurationError;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.xml.oai.v2.IdentifyType;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHtype;
import ch.unige.solidify.model.xml.xhtml.v1.XhtmlHtmlType;
import ch.unige.solidify.util.XMLTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.DLCMXmlNamespacePrefixMapper;
import ch.dlcm.model.xml.dlcm.v1.mets.Mets;
import ch.dlcm.model.xml.dlcm.v1.mets.PremisComplexType;
import ch.dlcm.model.xml.dlcm.v1.mets.Resource;
import ch.dlcm.model.xml.fits.Fits;

class JAXBTest extends AbstractXMLTest {
  private static final String DLCM_AUDIO_METADATA = "jaxb/dlcm-audio.xml";
  private static final String DLCM_METADATA = "jaxb/dlcm.xml";
  private static final String DLCM_DIP = "jaxb/dlcm-dip.xml";
  private static final String DLCM_VIDEO_METADATA = "jaxb/dlcm-video.xml";
  private static final Logger log = LoggerFactory.getLogger(JAXBTest.class);

  @Test
  void createOAIPMHTest() throws Exception {
    final JAXBContext jaxbContext = JAXBContext.newInstance(OAIPMHtype.class);
    final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

    log.info("Java => OAI-PMH tests");
    final OAIPMHtype oai = new OAIPMHtype();
    final IdentifyType id = new IdentifyType();
    id.setBaseURL("http://www.dlcm.ch");
    id.setRepositoryName("DLCM");
    oai.setIdentify(id);
    final QName qName = new QName(OAIConstants.OAI_PMH_NAMESPACE, OAIConstants.OAI_PMH);
    final JAXBElement<OAIPMHtype> root = new JAXBElement<>(qName, OAIPMHtype.class, oai);
    assertNotNull(root);
    jaxbMarshaller.marshal(root, System.out);
  }

  @Test
  void loadXMLTest() throws JAXBException, IOException {
    assertNotNull(this.getPath(DLCM_METADATA));
    try (FileReader fileReader = new FileReader(this.getPath(DLCM_METADATA).toFile())) {
      this.getUnmarshaller().unmarshal(fileReader);
    }
  }

  @Test
  void unloadXMLTest() throws Exception {
    final Path in = this.getPath(DLCM_METADATA);
    assertNotNull(in);
    final Path out = Paths.get(System.getProperty("java.io.tmpdir") + "/dlcm.xml");
    Mets mets;
    try (FileReader fileReader = new FileReader(in.toFile())) {
      mets = (Mets) this.getUnmarshaller().unmarshal(fileReader);
    }
    try (OutputStream outFile = new BufferedOutputStream(new FileOutputStream(out.toFile()), SolidifyConstants.BUFFER_SIZE)) {
      XMLTool.xml2Stream(this.getMarshaller(), outFile, mets);
    } catch (TransformerFactoryConfigurationError e) {
      throw new Exception(e);
    }
  }

  @Test
  void validationAudioDLCMTest() {
    final List<URL> xsdList = this.loadSchema(DLCMMetadataVersion.V1_0.getMetsSchema());
    xsdList.addAll(this.loadSchema("fits_output.xsd"));

    XMLTool.validate(xsdList, this.getPath(DLCM_AUDIO_METADATA));
  }

  @Test
  void validationDLCMTest() {
    final List<URL> xsdList = this.loadSchema(DLCMMetadataVersion.V1_0.getMetsSchema());
    xsdList.addAll(this.loadSchema("fits_output.xsd"));
    XMLTool.validate(xsdList, this.getPath(DLCM_METADATA));
  }

  @Test
  @Disabled("not used")
  void validationMETSTest() {
    final List<URL> xsdList = this.loadSchema("mets-1.11.xsd");
    XMLTool.validate(xsdList, this.getPath(DLCM_METADATA));
  }

  @Test
  @Disabled("not used")
  void validationMETSwithPREMISTest() {
    final List<URL> xsdList = this.loadSchema("mets-w-premis3-1.11.xsd");
    XMLTool.validate(xsdList, this.getPath(DLCM_METADATA));
  }

  @Test
  void validationVideoDLCMTest() {
    final List<URL> xsdList = this.loadSchema(DLCMMetadataVersion.V1_0.getMetsSchema());
    xsdList.addAll(this.loadSchema("fits_output.xsd"));

    XMLTool.validate(xsdList, this.getPath(DLCM_VIDEO_METADATA));
  }

  @Test
  void validationDipDLCMTest() {
    final List<URL> xsdList = this.loadSchema(DLCMMetadataVersion.V2_0.getMetsSchema());
    xsdList.addAll(this.loadSchema("fits_output.xsd"));
    XMLTool.validate(xsdList, this.getPath(DLCM_DIP));
  }

  private JAXBContext getJaxbContext() throws JAXBException {
    return JAXBContext.newInstance(Mets.class, Resource.class, PremisComplexType.class, Fits.class, XhtmlHtmlType.class);
  }

  private Marshaller getMarshaller() throws JAXBException {
    final Marshaller m = this.getJaxbContext().createMarshaller();
    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    m.setProperty(DLCMConstants.XML_NAMESPACE_PREFIX_MAPPER, new DLCMXmlNamespacePrefixMapper());
    return m;
  }

  private Unmarshaller getUnmarshaller() throws JAXBException {
    return this.getJaxbContext().createUnmarshaller();
  }

}

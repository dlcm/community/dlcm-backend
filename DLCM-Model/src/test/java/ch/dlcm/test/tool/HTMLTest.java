/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - HTMLTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tool;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.FileReader;
import java.nio.file.Path;

import javax.xml.namespace.QName;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.model.xml.xhtml.v1.XhtmlBodyType;
import ch.unige.solidify.model.xml.xhtml.v1.XhtmlDivType;
import ch.unige.solidify.model.xml.xhtml.v1.XhtmlHtmlType;

class HTMLTest extends AbstractXMLTest {
  private static final String DLCM_METADATA_HTML_SCHEMA_ORG = "jaxb/dlcm-html-schema.org.xml";
  private static final String DLCM_METADATA_SCHEMA_ORG = "jaxb/dlcm-schema.org.xml";
  private static final Logger log = LoggerFactory.getLogger(HTMLTest.class);

  @Test
  void createDivTest() throws Exception {
    final JAXBContext jaxbContext = JAXBContext.newInstance(XhtmlDivType.class);
    final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

    log.info("Java => Div test");
    final QName divQName = new QName("http://www.w3.org/1999/xhtml", "div");
    final XhtmlDivType div1 = new XhtmlDivType();
    div1.setId("div1");
    final XhtmlDivType div2 = new XhtmlDivType();
    div2.setId("div2");
    div1.getContent().add(new JAXBElement<>(divQName, XhtmlDivType.class, div2));

    final JAXBElement<XhtmlDivType> root = new JAXBElement<>(divQName, XhtmlDivType.class, div1);
    assertNotNull(root);
    jaxbMarshaller.marshal(root, System.out);
  }

  @Test
  void createHTMLTest() throws Exception {
    final JAXBContext jaxbContext = JAXBContext.newInstance(XhtmlHtmlType.class);
    final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

    log.info("Java => HTML test");
    final QName divQName = new QName("http://www.w3.org/1999/xhtml", "div");
    final XhtmlHtmlType xhtml = new XhtmlHtmlType();
    xhtml.setId("html");
    final XhtmlBodyType body = new XhtmlBodyType();
    body.setId("body");
    final XhtmlDivType div1 = new XhtmlDivType();
    div1.setId("div1");
    final XhtmlDivType div2 = new XhtmlDivType();
    div2.setId("div2");
    div1.getContent().add(new JAXBElement<>(divQName, XhtmlDivType.class, div2));
    body.getXhtmlBlockMix().add(div1);
    xhtml.setBody(body);
    final QName qName = new QName("http://www.w3.org/1999/xhtml", "html");
    final JAXBElement<XhtmlHtmlType> root = new JAXBElement<>(qName, XhtmlHtmlType.class, xhtml);
    assertNotNull(root);
    jaxbMarshaller.marshal(root, System.out);
  }

  @Test
  @Disabled("Cannot umarshal Div without HTML")
  void unloadDivTest() throws Exception {
    final Path in = this.getPath(DLCM_METADATA_SCHEMA_ORG);
    final JAXBContext jaxbContext = JAXBContext.newInstance(XhtmlDivType.class);

    log.info("Div => Java => Div tests");
    try (FileReader fileReader = new FileReader(in.toFile())) {
      final JAXBElement<XhtmlDivType> root = (JAXBElement<XhtmlDivType>) jaxbContext.createUnmarshaller().unmarshal(fileReader);
      assertNotNull(root);
      jaxbContext.createMarshaller().marshal(root, System.out);
    } catch (final Exception e) {
      log.error("Error [DIV]:", e);
      throw e;
    }
  }

  @Test
  void unloadXHTMLTest() throws Exception {
    final Path in = this.getPath(DLCM_METADATA_HTML_SCHEMA_ORG);
    final JAXBContext jaxbContext = JAXBContext.newInstance(XhtmlHtmlType.class);
    try (FileReader fileReader = new FileReader(in.toFile())) {
      final JAXBElement<XhtmlHtmlType> root = (JAXBElement<XhtmlHtmlType>) jaxbContext.createUnmarshaller().unmarshal(fileReader);
      assertNotNull(root);
      jaxbContext.createMarshaller().marshal(root, System.out);
    } catch (final Exception e) {
      log.error("Error [XHTML]:", e);
      throw e;
    }
  }

  private JAXBContext getJaxbContext() throws JAXBException {
    return JAXBContext.newInstance(XhtmlHtmlType.class);
  }

}

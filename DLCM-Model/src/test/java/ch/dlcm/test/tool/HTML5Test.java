/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - HTML5Test.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tool;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.FileReader;
import java.nio.file.Path;

import javax.xml.namespace.QName;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.model.xml.xhtml.v5.Body;
import ch.unige.solidify.model.xml.xhtml.v5.Html;
import ch.unige.solidify.model.xml.xhtml.v5.StyledFlowContentElement;

class HTML5Test extends AbstractXMLTest {
  private static final String DLCM_METADATA_HTML_SCHEMA_ORG = "jaxb/dlcm-html-schema.org.xml";
  private static final String DLCM_METADATA_SCHEMA_ORG = "jaxb/dlcm-schema.org.xml";
  private static final Logger log = LoggerFactory.getLogger(HTML5Test.class);

  @Test
  void createDivTest() throws Exception {
    final JAXBContext jaxbContext = JAXBContext.newInstance(Html.class);
    final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

    log.info("Java => Div test");
    final QName divQName = new QName("http://www.w3.org/1999/xhtml", "div");
    final StyledFlowContentElement div1 = new StyledFlowContentElement();
    div1.setId("div1");
    final StyledFlowContentElement div2 = new StyledFlowContentElement();
    div2.setId("div2");
    div1.getContent().add(new JAXBElement<>(divQName, StyledFlowContentElement.class, div2));

    final JAXBElement<StyledFlowContentElement> root = new JAXBElement<>(divQName, StyledFlowContentElement.class, div1);
    assertNotNull(root);
    jaxbMarshaller.marshal(root, System.out);
  }

  @Test
  void createHTMLTest() throws Exception {
    final JAXBContext jaxbContext = JAXBContext.newInstance(Html.class);
    final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

    log.info("Java => HTML test");
    final QName divQName = new QName("http://www.w3.org/1999/xhtml", "div");
    final Html html = new Html();
    html.setId("html");
    final Body body = new Body();
    body.setId("body");
    final StyledFlowContentElement div1 = new StyledFlowContentElement();
    div1.setId("div1");
    final StyledFlowContentElement div2 = new StyledFlowContentElement();
    div2.setId("div2");
    div1.getContent().add(new JAXBElement<>(divQName, StyledFlowContentElement.class, div2));
    body.getContent().add(new JAXBElement<>(divQName, StyledFlowContentElement.class, div1));
    html.setBody(body);
    final QName qName = new QName("http://www.w3.org/1999/xhtml", "html");
    final JAXBElement<Html> root = new JAXBElement<>(qName, Html.class, html);
    assertNotNull(root);
    jaxbMarshaller.marshal(root, System.out);
  }

  @Test
  void unloadDivTest() throws Exception {
    final Path in = this.getPath(DLCM_METADATA_SCHEMA_ORG);
    final JAXBContext jaxbContext = JAXBContext.newInstance(Html.class);

    log.info("Div => Java => Div tests");
    try (FileReader fileReader = new FileReader(in.toFile())) {
      final JAXBElement<Html> root = (JAXBElement<Html>) jaxbContext.createUnmarshaller().unmarshal(fileReader);
      assertNotNull(root);
      jaxbContext.createMarshaller().marshal(root, System.out);
    } catch (final Exception e) {
      log.error("Error [DIV]:", e);
      throw e;
    }
  }

  @Test
  void unloadHTMLTest() throws Exception {
    final Path in = this.getPath(DLCM_METADATA_HTML_SCHEMA_ORG);
    final JAXBContext jaxbContext = JAXBContext.newInstance(Html.class);
    try (FileReader fileReader = new FileReader(in.toFile())) {
      final Html root = (Html) jaxbContext.createUnmarshaller().unmarshal(fileReader);
      assertNotNull(root);
      jaxbContext.createMarshaller().marshal(root, System.out);
    } catch (final Exception e) {
      log.error("Error [XHTML]:", e);
      throw e;
    }
  }

  private JAXBContext getJaxbContext() throws JAXBException {
    return JAXBContext.newInstance(Html.class);
  }

}

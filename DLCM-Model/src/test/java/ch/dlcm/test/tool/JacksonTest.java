/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - JacksonTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tool;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.net.URI;

import jakarta.validation.constraints.NotNull;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.policies.PreservationPolicy;

class JacksonTest {

  private static final String CONTAINER_NAME = "My container";
  private static final String CONTAINER_DESCRIPTION = "My description";
  private static final String JSON_UNWRAPPED_DOUBLE_PROPERTIES = "{\"name\":\"My container\",\"description\":\"My description\",\"description\":\"My description\"}";
  private static final String JSON_UNWRAPPED_PREFIX = "{\"name\":\"My container\",\"metadata.description\":\"My description\",\"description\":\"My description\"}";
  private static final String FILE_URL = "http://www.unige.ch/rectorat/static/budget-2013.pdf";

  private final ObjectMapper objectMapper;

  /****************************************************************************************************/
  private static abstract class TopContainer implements MetadataAware {
    private String name;

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }
  }

  private static class Container extends TopContainer {

    @JsonUnwrapped
    private JacksonTest.Metadata metadata = new Metadata();

    @Override
    public Metadata getMetadata() {
      return this.metadata;
    }

    public void setMetadata(Metadata metadata) {
      this.metadata = metadata;
    }
  }

  private static class ChildContainer extends Container {
    public void setDescription(String description) {
      this.getMetadata().setDescription(description);
    }
  }

  private static class ContainerWithPrefix extends TopContainer {

    @JsonUnwrapped(prefix = "metadata.")
    private JacksonTest.Metadata metadata = new Metadata();

    @Override
    public Metadata getMetadata() {
      return this.metadata;
    }

    public void setMetadata(Metadata metadata) {
      this.metadata = metadata;
    }
  }

  private static class ChildContainerWithPrefix extends ContainerWithPrefix {
    public void setDescription(String description) {
      this.getMetadata().setDescription(description);
    }
  }

  private interface MetadataAware {

    Metadata getMetadata();

    default String getDescription() {
      return this.getMetadata().getDescription();
    }
  }

  private static class Metadata implements Described {
    @NotNull
    private String description;

    @Override
    public String getDescription() {
      return this.description;
    }

    @Override
    public void setDescription(String description) {
      this.description = description;
    }
  }

  private interface Described {
    String getDescription();

    void setDescription(String description);
  }

  /****************************************************************************************************/
  public JacksonTest() {
    this.objectMapper = new ObjectMapper();
    this.objectMapper.registerModule(new JavaTimeModule());
    this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    this.objectMapper.setFilterProvider(new SimpleFilterProvider().setFailOnUnknownId(false));
  }

  /****************************************************************************************************/

  @Test
  void containerTest() throws JsonProcessingException {
    Container container = new Container();
    container.setName(CONTAINER_NAME);
    container.getMetadata().setDescription(CONTAINER_DESCRIPTION);

    // serialize container and check its Json structure is flat
    String jsonContainer = this.objectMapper.writeValueAsString(container);
    assertEquals(JSON_UNWRAPPED_DOUBLE_PROPERTIES, jsonContainer);

    // deserialize from Json and check that all properties are correctly initialized
    Container newContainerInstance = this.objectMapper.readValue(jsonContainer, Container.class);
    assertEquals(CONTAINER_NAME, newContainerInstance.getName());
    assertEquals(CONTAINER_DESCRIPTION, newContainerInstance.getMetadata().getDescription());
  }

  @Test
  void childContainerTest() throws JsonProcessingException {
    ChildContainer container = new ChildContainer();
    container.setName(CONTAINER_NAME);
    container.getMetadata().setDescription(CONTAINER_DESCRIPTION);

    // serialize container and check its Json structure is flat
    String jsonContainer = this.objectMapper.writeValueAsString(container);
    assertEquals(JSON_UNWRAPPED_DOUBLE_PROPERTIES, jsonContainer);

    // deserialize from Json and check that all properties are NOT correctly initialized due to the
    // additional setter in ChildContainer:
    // there is a conflict in ChildContainer between the setDescription() method of the unwrapped
    // Metadata property
    ChildContainer newContainerInstance = this.objectMapper.readValue(jsonContainer, ChildContainer.class);
    assertEquals(CONTAINER_NAME, newContainerInstance.getName());
    assertNull(newContainerInstance.getMetadata().getDescription());
  }

  @Test
  void containerWithPrefixTest() throws JsonProcessingException {
    ContainerWithPrefix container = new ContainerWithPrefix();
    container.setName(CONTAINER_NAME);
    container.getMetadata().setDescription(CONTAINER_DESCRIPTION);

    // serialize container and check its Json structure is flat
    String jsonContainer = this.objectMapper.writeValueAsString(container);
    assertEquals(JSON_UNWRAPPED_PREFIX, jsonContainer);

    // deserialize from Json and check that all properties are correctly initialized
    ContainerWithPrefix newContainerInstance = this.objectMapper.readValue(jsonContainer, ContainerWithPrefix.class);
    assertEquals(CONTAINER_NAME, newContainerInstance.getName());
    assertEquals(CONTAINER_DESCRIPTION, newContainerInstance.getMetadata().getDescription());
  }

  @Test
  void childContainerWithPrefixTest() throws JsonProcessingException {
    ChildContainerWithPrefix container = new ChildContainerWithPrefix();
    container.setName(CONTAINER_NAME);
    container.getMetadata().setDescription(CONTAINER_DESCRIPTION);

    // serialize container and check its Json structure is flat
    String jsonContainer = this.objectMapper.writeValueAsString(container);
    assertEquals(JSON_UNWRAPPED_PREFIX, jsonContainer);

    // deserialize from Json and check that all properties are correctly initialized
    // The additional setter setDescription() in ChildContainerWithPrefix is not a problem for
    // (de)serialization as the properties have
    // different names
    ChildContainerWithPrefix newContainerInstance = this.objectMapper.readValue(jsonContainer, ChildContainerWithPrefix.class);
    assertEquals(CONTAINER_NAME, newContainerInstance.getName());
    assertEquals(CONTAINER_DESCRIPTION, newContainerInstance.getMetadata().getDescription());
  }

  @Test
  void sipDataFileTest() throws JsonProcessingException {
    SipDataFile sipDataFile = new SipDataFile();
    sipDataFile.setSourceData(URI.create(FILE_URL));
    sipDataFile.setFinalData(URI.create(FILE_URL));

    // serialize container and check its Json structure is flat
    String jsonSipDataFile = this.objectMapper.writeValueAsString(sipDataFile);

    // deserialize from Json and check that all properties are correctly initialized
    SipDataFile newSipDataFileInstance = this.objectMapper.readValue(jsonSipDataFile, SipDataFile.class);
    assertNotNull(newSipDataFileInstance.getSourceData());
    assertEquals(FILE_URL, newSipDataFileInstance.getSourceData().toString());
  }

  @Test
  void preservationPolicyTest() throws JsonProcessingException {
    PreservationPolicy preservationPolicy = new PreservationPolicy();
    preservationPolicy.setDispositionApproval(false);
    preservationPolicy.setRetention(15 * DLCMConstants.DAYS_BY_YEAR);

    // serialize container and check its Json structure is flat
    String jsonPreservationPolicy = this.objectMapper.writeValueAsString(preservationPolicy);

    // deserialize from Json and check that all properties are correctly initialized
    PreservationPolicy newPreservationPolicyInstance = this.objectMapper.readValue(jsonPreservationPolicy, PreservationPolicy.class);
    assertNotNull(newPreservationPolicyInstance.getDispositionApproval());
    assertFalse(newPreservationPolicyInstance.getDispositionApproval());
    assertEquals(15 * DLCMConstants.DAYS_BY_YEAR, newPreservationPolicyInstance.getRetention());
  }
}

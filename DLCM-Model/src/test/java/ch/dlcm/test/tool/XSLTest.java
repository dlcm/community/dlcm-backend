/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - XSLTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tool;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.tool.MetadataMigrationTool;

class XSLTest extends AbstractXMLTest {
  private static final String DLCM_METS_PREFIX = "dlcm/dlcm_mets";
  private static final String DLCM_DATACITE_METADATA_PREFIX = "dlcm/dlcm_datacite";
  private static final String DLCM_DATACITE_RANGE_DATE_PREFIX = "dlcm/dlcm_datacite_date";
  private static final String DLCM_PREMIS_PREFIX = "dlcm/dlcm_premis";
  private static final String DLCM_METS_MIGRATION = "dlcm/migration/dlcm";
  private static final Logger log = LoggerFactory.getLogger(XSLTest.class);

  @Test
  void transformDataCite() {
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      try {
        final List<URL> xsdList = this.loadSchema(version.getRepresentationInfoSchema());
        final Path xml = this.getPath(this.getInputFileName(version, DLCM_DATACITE_METADATA_PREFIX));
        final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
        for (final org.springframework.core.io.Resource xsl : resolver
                .getResources("classpath*:/" + DLCMConstants.XSL_HOME + "/" + version.getIndexingTransformation())) {
          log.info("[DataCite] Transforming with {}", xsl);
          XMLTool.transform(xml, xsl.getFile().toPath());
          final Path result = this.getPath(this.getOutputFileName(version, DLCM_DATACITE_METADATA_PREFIX));
          XMLTool.wellformed(result);
          XMLTool.validate(xsdList, xml);
        }
      } catch (final Exception e) {
        fail(e.getMessage());
      }
    }
  }

  @Test
  void transformDataCiteDateRange() {
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      try {
        final List<URL> xsdList = this.loadSchema(version.getRepresentationInfoSchema());
        final Path xml = this.getPath(this.getInputFileName(version, DLCM_DATACITE_RANGE_DATE_PREFIX));
        final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
        for (final org.springframework.core.io.Resource xsl : resolver
                .getResources("classpath*:/" + DLCMConstants.XSL_HOME + "/" + version.getIndexingTransformation())) {
          log.info("[DataCite Date Range] Transforming with {}", xsl);
          XMLTool.transform(xml, xsl.getFile().toPath());
          final Path result = this.getPath(this.getOutputFileName(version, DLCM_DATACITE_RANGE_DATE_PREFIX));
          XMLTool.wellformed(result);
          XMLTool.validate(xsdList, xml);
        }
      } catch (final Exception e) {
        fail(e.getMessage());
      }
    }
  }

  @Test
  void transformDLCMMets() {
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      try {
        final Path xml = this.getPath(this.getInputFileName(version, DLCM_METS_PREFIX));
        final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
        for (final org.springframework.core.io.Resource xsl : resolver
                .getResources("classpath*:/" + DLCMConstants.XSL_HOME + "/" + version.getIndexingTransformation())) {
          log.info("[Mets] Transforming {} with {}", xml, xsl);
          XMLTool.transform(xml, xsl.getFile().toPath());
          final Path result = this.getPath(this.getOutputFileName(version, DLCM_METS_PREFIX));
          assertNotNull(result);
          XMLTool.wellformed(result);
        }
      } catch (final Exception e) {
        fail(e.getMessage());
      }
    }
  }

  @ParameterizedTest
  @MethodSource("versions")
  void migrateMetadata(DLCMMetadataVersion version) throws IOException {
    final Path originalXml = this.getPath(this.getInputFileName(version, DLCM_METS_MIGRATION));
    Path fromXml = originalXml.getParent().resolve("dlcm.xml");
    assertTrue(FileTool.copyFile(originalXml, fromXml));
    assertDoesNotThrow(() -> MetadataMigrationTool.migrateMetadataToDefaultVersion(version, fromXml));
    assertTrue(FileTool.checkFile(fromXml));
  }

  @Test
  void transformPremis() {
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      try {
        final Path xml = this.getPath(this.getInputFileName(version, DLCM_PREMIS_PREFIX));
        final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
        for (final org.springframework.core.io.Resource xsl : resolver
                .getResources("classpath*:/" + DLCMConstants.XSL_HOME + "/" + version.getIndexingTransformation())) {
          log.info("[PREMIS] Transforming with {}", xsl);
          XMLTool.transform(xml, xsl.getFile().toPath());
          final Path result = this.getPath(this.getOutputFileName(version, DLCM_PREMIS_PREFIX));
          assertNotNull(result);
          XMLTool.wellformed(result);
        }
      } catch (final Exception e) {
        fail(e.getMessage());
      }
    }
  }

  @Test
  void transformOAI() {
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      try {
        final Path xml = this.getPath(this.getInputFileName(version, DLCM_DATACITE_METADATA_PREFIX));
        final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
        for (final org.springframework.core.io.Resource xsl : resolver
                .getResources("classpath*:/" + DLCMConstants.XSL_HOME + "/" + DLCMConstants.OAI_DATACITE + "2*.xsl")) {
          log.info("[OAI] Transforming with {}", xsl);
          XMLTool.transform(xml, xsl.getFile().toPath());
          final Path result = this.getPath(this.getOutputFileName(version, DLCM_DATACITE_METADATA_PREFIX));
          assertNotNull(result);
          XMLTool.wellformed(result);
        }
      } catch (final Exception e) {
        fail(e.getMessage());
      }
    }
  }

  @Test
  void transformSmartView() {
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      try {
        final Path xml = this.getPath(this.getInputFileName(version, DLCM_DATACITE_METADATA_PREFIX));
        final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
        for (final org.springframework.core.io.Resource xsl : resolver
                .getResources("classpath*:/" + DLCMConstants.XSL_HOME + "/dlcm_oai2.xsl")) {
          log.info("[OAI Smart View] Transforming with {}", xsl);
          XMLTool.transform(xml, xsl.getFile().toPath());
          final Path result = this.getPath(this.getOutputFileName(version, DLCM_DATACITE_METADATA_PREFIX));
          assertNotNull(result);
          // Not well formed because of XSL output method is html, xhtml not supported by browser
          // XMLTool.wellformed(result);
        }
      } catch (final Exception e) {
        fail(e.getMessage());
      }
    }
  }

  private String getInputFileName(DLCMMetadataVersion version, String prefix) {
    final StringBuilder inputFileName = new StringBuilder(prefix);
    inputFileName.append("-");
    inputFileName.append(version.getVersion());
    inputFileName.append(SolidifyConstants.XML_EXT);
    return inputFileName.toString();
  }

  private String getOutputFileName(DLCMMetadataVersion version, String prefix) {
    final StringBuilder outputFileName = new StringBuilder(prefix);
    if (version != null) {
      outputFileName.append("-");
      outputFileName.append(version.getVersion());
    }
    outputFileName.append("-out.xml");
    return outputFileName.toString();
  }

  private static Stream<Arguments> versions() {
    return Stream.of(
            arguments(DLCMMetadataVersion.V1_0),
            arguments(DLCMMetadataVersion.V1_1),
            arguments(DLCMMetadataVersion.V2_0),
            arguments(DLCMMetadataVersion.V2_1),
            arguments(DLCMMetadataVersion.V3_0),
            arguments(DLCMMetadataVersion.V3_1));
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DepositTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tool;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.unige.solidify.exception.SolidifyCheckingException;

import ch.dlcm.model.preingest.Deposit;

class DepositTest {

  private final String depositJsonTemplate = "{\"title\" : \"test\", \"metadataVersion\": \"1.1\", \"collectionBegin\" : \"%s\", \"collectionEnd\" : \"%s\"}";

  private ObjectMapper mapper = new ObjectMapper();

  @Test
  void collectionDatesFormatInvalidStringsTest() {
    final String[] invalidDates = {
            "2000-08-01T13:20:03+01:0", "2000-08-01T13:20:03:000+01:00", "2000-08-01T13:70:03+01:00", "2000-08-01T13:20:03+2500",
            "2000-08-01T13:20:03.000+25", "2000-08T13:20:03+", "2000-08-01+01:00", "2000-08-01+0100", "2000-08-01T15:25:23+1:00",
            "2000-08-01T13:20:03.0.1+0100", "2000-08-01T13:20:03"
    };
    Deposit deposit = null;
    int count = 0;
    for (final String invalidDate : invalidDates) {
      try {
        deposit = null;
        final String depositJson = String.format(this.depositJsonTemplate, invalidDate, invalidDate);
        deposit = this.mapper.readValue(depositJson, Deposit.class);
      } catch (final IOException ex) {
        count++;
      }
      assertNull(deposit);
    }
    assertEquals(invalidDates.length, count);
  }

  @Test
  void collectionDatesFormatValidStringsTest() {
    // @formatter:off
    String[] validDates = {
            "2000-08-01T13:20:03+01:00",     "2000-08-01T13:20:03+0100",     "2000-08-01T13:20:03Z",
            "2000-08-01T13:20:03.000+01:00", "2000-08-01T13:20:03.200+0100", "2000-08-01T13:20:03.200Z",
            "2000-08-01T13:20+01:00",        "2000-08-01T13:20+0100",        "2000-08-01T13:20Z",
    };
    // @formatter:on

    Deposit deposit = null;
    for (final String validDate : validDates) {
      try {
        deposit = null;
        final String depositJson = String.format(this.depositJsonTemplate, validDate, validDate);
        deposit = this.mapper.readValue(depositJson, Deposit.class);
      } catch (final IOException ex) {
        this.returnException(ex.getMessage());
      }
      assertNotNull(deposit);
    }
  }

  @Test
  void collectionDatesOutputFormatTest() {
    final String[] validDates = {
            "2000-08-01T13:20:00+01:00", "2000-08-01T13:20:00+0100", "2000-08-01T13:20:00.000+01:00", "2000-08-01T13:20:00.000+0100",
            "2000-08-01T13:20+01:00", "2000-08-01T13:20+0100"
    };
    Deposit deposit = null;
    for (final String validDate : validDates) {
      try {
        deposit = null;
        final String depositJson = String.format(this.depositJsonTemplate, validDate, validDate);
        deposit = this.mapper.readValue(depositJson, Deposit.class);
      } catch (final IOException ex) {
        this.returnException(ex.getMessage());
      }
      assertNotNull(deposit);
      try {
        this.mapper = new ObjectMapper().registerModule(new JavaTimeModule());
        final String jsonOutput = this.mapper.writeValueAsString(deposit);
        assertTrue(jsonOutput.contains("2000-08-01T13:20:00.000+0100"));
      } catch (final JsonProcessingException ex) {
        this.returnException(ex.getMessage());
      }
    }
  }

  private void returnException(String message) {
    assertThrows(SolidifyCheckingException.class, () -> {
      throw new SolidifyCheckingException(message);
    });
  }
}

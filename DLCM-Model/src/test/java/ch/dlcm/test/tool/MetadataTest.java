/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - MetadataTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tool;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.util.XMLTool;

import ch.dlcm.DLCMMetadataVersion;

class MetadataTest extends AbstractXMLTest {
  private static final String KO_SUFFIX = "-KO.xml";
  private static final String OK_SUFFIX = "-OK.xml";

  @ParameterizedTest
  @MethodSource("versions")
  void validationVersionTest(String version) throws SolidifyCheckingException {
    this.testFiles(version);
  }

  @ParameterizedTest
  @MethodSource("dataciteVersions")
  void validationDataCiteTest(String version, String dataciteVersion) throws SolidifyCheckingException {
    final List<URL> xsdList = this.loadSchema(DLCMMetadataVersion.fromVersion(version).getMetsSchema());
    final Path dataciteFile = this.getPath("dlcm/datacite/datacite-example-full-v" + dataciteVersion + ".xml");
    final Path datacite45File = this.getPath("dlcm/datacite/datacite-example-full-v4.5.xml");
    assertDoesNotThrow(() -> XMLTool.validate(xsdList, dataciteFile));
    if (DLCMMetadataVersion.fromVersion(version).getVersionNumber() < DLCMMetadataVersion.V4_0.getVersionNumber()) {
      assertThrows(SolidifyCheckingException.class, () -> XMLTool.validate(xsdList, datacite45File));
    } else {
      assertDoesNotThrow(() -> XMLTool.validate(xsdList, datacite45File));
    }
  }

  @ParameterizedTest
  @MethodSource("versions")
  void wellformedSchemasTest(String version) throws URISyntaxException, SolidifyCheckingException {
    final List<URL> xsdList = this.loadSchema(DLCMMetadataVersion.fromVersion(version).getMetsSchema());
    try {
      for (final URL xsd : xsdList) {
        assertNotNull(Paths.get(xsd.toURI()));
        XMLTool.wellformed(Paths.get(xsd.toURI()));
      }
    } catch (final SolidifyCheckingException e) {
      fail(e.getMessage());
    }
  }

  @ParameterizedTest
  @MethodSource("versions")
  void wellFormedTest(String version) throws SolidifyCheckingException {
    for (final Path file : this.loadFilesTest(version)) {
      try {
        XMLTool.wellformed(file);
      } catch (final SolidifyCheckingException e) {
        fail(e.getMessage());
      }
    }
  }

  private List<Path> loadFilesTest(String version) {
    final List<Path> list = new ArrayList<>();
    list.add(this.getPath("metadata/dlcm_mets-" + version + OK_SUFFIX));
    list.add(this.getPath("metadata/dlcm_mets-" + version + KO_SUFFIX));
    return list;
  }

  private void testFiles(String version) {
    final List<URL> xsdList = this.loadSchema(DLCMMetadataVersion.fromVersion(version).getMetsSchema());
    for (final Path file : this.loadFilesTest(version)) {
      try {
        XMLTool.validate(xsdList, file);
      } catch (final SolidifyCheckingException e) {
        if (!file.getFileName().toString().endsWith(KO_SUFFIX)) {
          fail(e.getMessage());
        }
      }
    }
  }

  private static Stream<Arguments> versions() {
    return Stream.of(
            arguments("1.0"),
            arguments("1.1"),
            arguments("2.0"),
            arguments("2.1"),
            arguments("3.0"),
            arguments("3.1"),
            arguments("4.0"));
  }

  private static Stream<Arguments> dataciteVersions() {
    return Stream.of(
            arguments("1.0", "4.0"),
            arguments("1.1", "4.0"),
            arguments("2.0", "4.3"),
            arguments("2.1", "4.3"),
            arguments("3.0", "4.4"),
            arguments("3.1", "4.4"),
            arguments("4.0", "4.5"));
  }
}

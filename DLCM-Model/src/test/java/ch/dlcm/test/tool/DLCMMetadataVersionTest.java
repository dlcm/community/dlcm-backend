/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Model - DLCMMetadataVersionTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tool;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import ch.dlcm.DLCMMetadataVersion;

class DLCMMetadataVersionTest {

  @Test
  void tesCurrentVersion() {
    final DLCMMetadataVersion currentVersion = DLCMMetadataVersion.getDefaultVersion();
    for (DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      if (currentVersion.equals(version)) {
        assertEquals(version.getVersionNumber(), currentVersion.getVersionNumber());
      } else {
        assertTrue(version.getVersionNumber() < currentVersion.getVersionNumber());
      }
    }
  }
}

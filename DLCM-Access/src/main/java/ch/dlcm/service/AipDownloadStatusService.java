/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AipDownloadStatusService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.net.URI;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.AipDownloadMessage;
import ch.dlcm.message.OrderMessage;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.citation.DLCMCitationGeneratorService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class AipDownloadStatusService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(AipDownloadStatusService.class);

  private static final String NOTIFICATION_AIP_DOWNLOAD_ERROR = "access.notification.aip.download.error";

  private final ArchivalInfoPackageService aipService;
  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteService;
  private final DLCMCitationGeneratorService citationsService;
  private final TrustedNotificationRemoteResourceService notificationRemoteService;
  private final TrustedUserRemoteResourceService userRemoteService;
  private final TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService;

  public AipDownloadStatusService(
          MessageService messageService,
          ArchivalInfoPackageService aipService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteService,
          TrustedNotificationRemoteResourceService notificationRemoteService,
          TrustedUserRemoteResourceService userRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService,
          DLCMCitationGeneratorService citationsService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.aipService = aipService;
    this.aipRemoteService = aipRemoteService;
    this.citationsService = citationsService;
    this.notificationRemoteService = notificationRemoteService;
    this.userRemoteService = userRemoteService;
    this.organizationalUnitRemoteService = organizationalUnitRemoteService;
  }

  @Transactional
  public void checkDownloadingCollection() {
    for (final ArchivalInfoPackage aip : this.aipService.findByStatus(PackageStatus.DOWNLOADING)) {
      if (aip.isCollection() && aip.isReady()) {
        this.setArchiveSizeAndFileNumber(aip);
        aip.setPackageStatus(PackageStatus.COMPLETED);
        this.saveAip(aip);
      }
    }
  }

  @Transactional
  public ArchivalInfoPackage processAip(String aipId) {
    // Load AIP
    final ArchivalInfoPackage aip = this.aipService.findOne(aipId);
    try {
      switch (aip.getInfo().getStatus()) {
        case IN_PROGRESS -> {
          if (aip.isCollection()) {
            this.getArchivalCollection(aip);
          }
          this.getArchivalInfoPackage(aip);
        }
        case DOWNLOADING -> {
          if (aip.isReady()) {
            this.setArchiveSizeAndFileNumber(aip);
            aip.setPackageStatus(PackageStatus.COMPLETED);
          }
        }
        default -> {
        }
      }
      this.saveAip(aip);
    } catch (final Exception e) {
      log.error("Error in processing AIP Download", e);
      aip.getInfo().setStatusWithMessage(PackageStatus.IN_ERROR, e.getMessage());
      this.sendErrorNotification(aip);
    }

    log.info("AIP download: resId={} status={}", aip.getResId(), aip.getInfo().getStatus());
    return aip;
  }

  private boolean getArchivalCollection(ArchivalInfoPackage aip) {
    final String storageUrl = this.aipRemoteService.findStorageForAIP(aip.getResId());

    final List<ArchivalInfoPackage> list = this.aipRemoteService.getAIPCollectionList(storageUrl, aip.getResId());
    for (final ArchivalInfoPackage a : list) {
      if (a.getInfo().getStatus() != PackageStatus.COMPLETED) {
        log.warn("AIP ({}) not ready for downloading", a.getResId());
        continue;
      }
      if (!this.aipService.existsById(a.getResId())) {
        a.reset();
        this.aipService.saveForOrders(a);
        SolidifyEventPublisher.getPublisher().publishEvent(new AipDownloadMessage(a.getResId()));
      }
      if (!this.belongsToList(a, aip.getCollection()) && !aip.addItem(a)) {
        log.error("AIP ({}) cannot be add to the collection", a.getResId());
        return false;
      }
    }
    aip.setPackageStatus(PackageStatus.DOWNLOADING);
    return true;

  }

  private void getArchivalInfoPackage(ArchivalInfoPackage aip) {
    if (aip.getDataFiles().isEmpty()) {
      final AipDataFile df = new AipDataFile(aip);
      final String storageUrl = this.aipRemoteService.findStorageForAIP(aip.getResId());
      df.setSourceData(URI.create(storageUrl + "/" + ResourceName.AIP + "/" + aip.getResId() + "/" + ActionName.DOWNLOAD));
      df.setDataCategory(DataCategory.Package);
      df.setDataType(DataCategory.InformationPackage);
      aip.addItem(df);
      aip.setPackageStatus(PackageStatus.DOWNLOADING);
    } else {
      if (aip.isReady()) {
        aip.setPackageStatus(PackageStatus.COMPLETED);
      }
    }
  }

  private void saveAip(ArchivalInfoPackage aip) {
    this.aipService.saveForOrders(aip);
    // Notify Order queue and collection in-progress
    if (aip.getInfo().getStatus() == PackageStatus.COMPLETED) {
      // messages are sent without resId to notify collections
      SolidifyEventPublisher.getPublisher().publishEvent(new OrderMessage());
      SolidifyEventPublisher.getPublisher().publishEvent(new AipDownloadMessage());

      // Delete associated citations if they already exist (in case of AIP update)
      this.deleteAssociatedCitations(aip);
    }
  }

  private void setArchiveSizeAndFileNumber(ArchivalInfoPackage aip) {
    long size = aip.getDataFiles().get(0).getFileSize();
    long fileNumber = this.getPackageFileNumber(aip.getArchiveContainer(), aip.getDataFiles().get(0).getFinalData());
    // For collection, add others AIPs
    if (aip.isCollection()) {
      for (final ArchivalInfoPackage aiu : aip.getCollection()) {
        size += aiu.getArchiveSize();
        fileNumber += aiu.getArchiveFileNumber();
      }
    }
    aip.setArchiveSize(size);
    aip.setArchiveFileNumber(fileNumber);
  }

  private void deleteAssociatedCitations(ArchivalInfoPackage aip) {
    this.citationsService.deleteAllStoredCitationsForItem(aip.getResId());
  }

  private void sendErrorNotification(ArchivalInfoPackage aip) {
    try {
      // Send a notification
      String message = this.messageService.get(NOTIFICATION_AIP_DOWNLOAD_ERROR, new Object[] { aip.getResId() });
      User updaterUser = this.userRemoteService.findByExternalUid(aip.getLastUpdate().getWho());
      OrganizationalUnit organizationalUnit = this.organizationalUnitRemoteService.findOne(aip.getInfo().getOrganizationalUnitId());
      this.notificationRemoteService.createNotification(updaterUser, message, NotificationType.IN_ERROR_DOWNLOADED_AIP_INFO, organizationalUnit,
              aip.getResId());
    } catch (Exception e) {
      log.error("Cannot send notification for AIP {} in error: {}", aip.getResId(), e.getMessage());
    }
  }
}

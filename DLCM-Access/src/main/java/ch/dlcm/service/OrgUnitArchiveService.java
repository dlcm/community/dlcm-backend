/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrgUnitArchiveService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.service.CompositeNoSqlResourceService;
import ch.unige.solidify.util.SearchConditionTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.access.OrgUnit;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;

@Service("orgUnitArchiveService")
@ConditionalOnBean(AccessController.class)
public class OrgUnitArchiveService extends CompositeNoSqlResourceService<OrgUnit> {

  private final FallbackOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService;

  private final SearchService searchService;

  public OrgUnitArchiveService(FallbackOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService,
          SearchService searchService) {
    this.organizationalUnitRemoteResourceService = organizationalUnitRemoteResourceService;
    this.searchService = searchService;
  }

  @Override
  public boolean delete(OrgUnit orgUnit) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<OrgUnit> findAll(OrgUnit search) {
    return Collections.emptyList();
  }

  @Override
  @SuppressWarnings({ "squid:S3776", "unchecked" })
  public <V> Page<V> findByParentId(String parentId, Class<?> parentClass, Class<V> childClass, Pageable pageable) {
    final OrgUnit orgUnit = this.findOne(parentId);
    if (childClass.equals(ArchiveMetadata.class)) {
      final RestCollection<ArchiveMetadata> archives = this.searchService.search(this.getSearchConditions(orgUnit.getResId()), pageable);
      return new PageImpl<>(((List<V>) archives.getData()), pageable, archives.getPage().getTotalItems());
    }
    throw new UnsupportedOperationException(
            "Missing implementation 'findByParentId' method (" + parentClass.getSimpleName() + "->" + childClass.getSimpleName() + ")");
  }

  @Override
  @SuppressWarnings({ "squid:S3776", "unchecked" })
  public <V> V findByParentIdAndChildId(String parentId, Class<?> parentClass, Class<V> childClass, String id) {
    return (V) this.searchService.findOne(id);
  }

  @Override
  public OrgUnit findOne(String id) {
    final RestCollection<ArchiveMetadata> archives = this.searchService.search(this.getSearchConditions(id),
            PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE));
    return new OrgUnit(this.organizationalUnitRemoteResourceService.getOrgUnit(id), archives.getData());
  }

  @Override
  public OrgUnit save(OrgUnit orgUnit) {
    throw new UnsupportedOperationException();
  }

  @Override
  public OrgUnit update(OrgUnit orgUnit) {
    throw new UnsupportedOperationException();
  }

  private List<SearchCondition> getSearchConditions(String orgUnitId) {
    List<SearchCondition> searchConditions = new ArrayList<>();
    SearchConditionTool.addQueryCondition(searchConditions, DLCMConstants.AIP_ORGA_UNIT + ":" + orgUnitId);
    return searchConditions;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrgUnitService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.NoSqlResourceService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.controller.AccessController;
import ch.dlcm.model.access.OrgUnit;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;

@Service("orgUnitRemoteService")
@ConditionalOnBean(AccessController.class)
public class OrgUnitService extends NoSqlResourceService<OrgUnit> {

  private final FallbackOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService;

  public OrgUnitService(FallbackOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService) {
    this.organizationalUnitRemoteResourceService = organizationalUnitRemoteResourceService;
  }

  @Override
  public boolean delete(OrgUnit t) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<OrgUnit> findAll(OrgUnit search) {
    final RestCollection<OrganizationalUnit> result = this.organizationalUnitRemoteResourceService
            .getOrgUnitList(StringTool.convertToQueryString(search), PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE));
    final List<OrgUnit> list = new ArrayList<>();
    for (final OrganizationalUnit orgUnit : result.getData()) {
      list.add(new OrgUnit(orgUnit));
    }
    return list;
  }

  @Override
  public Page<OrgUnit> findAll(OrgUnit search, Pageable pageable) {
    final RestCollection<OrganizationalUnit> result = this.organizationalUnitRemoteResourceService.getOrgUnitList(
            StringTool.convertToQueryString(search), pageable);
    final List<OrgUnit> list = new ArrayList<>();
    for (final OrganizationalUnit orgUnit : result.getData()) {
      list.add(new OrgUnit(orgUnit));
    }
    return new PageImpl<>(list, pageable, result.getPage().getTotalItems());
  }

  @Override
  public OrgUnit findOne(String id) {
    return new OrgUnit(this.organizationalUnitRemoteResourceService.getOrgUnit(id));
  }

  @Override
  public OrgUnit save(OrgUnit t) {
    throw new UnsupportedOperationException();
  }

  @Override
  public OrgUnit update(OrgUnit t) {
    throw new UnsupportedOperationException();
  }

}

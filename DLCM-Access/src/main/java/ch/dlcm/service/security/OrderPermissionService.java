/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrderPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import java.text.ParseException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import com.nimbusds.jwt.SignedJWT;

import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.service.HttpRequestInfoProvider;

import ch.dlcm.business.OrderService;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.access.Order;

@Service
@ConditionalOnBean(AccessController.class)
public class OrderPermissionService implements ApplicationRoleListService {

  private final HttpRequestInfoProvider httpRequestInfoProvider;
  private final OrderService orderService;

  public OrderPermissionService(OrderService orderService, HttpRequestInfoProvider httpRequestInfoProvider) {
    this.orderService = orderService;
    this.httpRequestInfoProvider = httpRequestInfoProvider;
  }

  public boolean isAllowedToGet(String orderId) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    final Order order = this.orderService.findOne(orderId);
    if (order.isPublicOrder().booleanValue()) {
      return true;
    }
    return this.isOrderOfCurrentUser(order);
  }

  public boolean isAllowedToUpdate(String orderId) {
    return this.canBypassOrIsOrderOfCurrentUser(orderId);
  }

  public boolean isAllowedToDelete(String orderId) {
    return this.canBypassOrIsOrderOfCurrentUser(orderId);
  }

  public boolean isAllowedToDeleteList(String[] listOrderIds) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    for (final String orderId : listOrderIds) {
      if (!this.canBypassOrIsOrderOfCurrentUser(orderId)) {
        return false;
      }
    }
    return true;
  }

  private boolean canBypassOrIsOrderOfCurrentUser(String orderId) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    final Order order = this.orderService.findOne(orderId);
    return this.isOrderOfCurrentUser(order);
  }

  private boolean isOrderOfCurrentUser(Order order) {
    try {
      final String token = this.httpRequestInfoProvider.getIncomingToken();
      final String externalUid = SignedJWT.parse(token).getJWTClaimsSet().getClaim("user_name").toString();
      return order.getCreatedBy().equals(externalUid);
    } catch (ParseException ex) {
      return false;
    }
  }
}

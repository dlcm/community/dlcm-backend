/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - MetadataPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyMfaNeededException;

import ch.dlcm.controller.AccessController;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.security.Role;
import ch.dlcm.service.SearchService;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class MetadataPermissionService extends AbstractPermissionWithOrgUnitService {
  private final SearchService searchService;
  private final MfaPermissionService mfaPermissionService;

  public MetadataPermissionService(SearchService searchService,
          TrustedPersonRemoteResourceService personRemoteResourceService,
          MfaPermissionService mfaPermissionService) {
    super(personRemoteResourceService);
    this.searchService = searchService;
    this.mfaPermissionService = mfaPermissionService;
  }

  @Override
  public boolean isAllowed(String targetResId, String actionString) {
    final ArchiveMetadata archiveMetadata = this.getExistingResource(targetResId);
    if (archiveMetadata == null) {
      throw new NoSuchElementException("Archive not found");
    }
    boolean isAllowed = super.isAllowed(targetResId, actionString);

    DLCMControllerAction action = this.getControllerAction(actionString);

    // CRIMSON and RED archives need MFA to be downloaded
    if (isAllowed && !action.equals(DLCMControllerAction.LIST_FILES)
            && (archiveMetadata.getDataTag().equals(DataTag.CRIMSON) || archiveMetadata.getDataTag().equals(DataTag.RED))
            && !this.mfaPermissionService.isMfaAuthenticated()) {
      throw new SolidifyMfaNeededException();
    }
    return isAllowed;
  }

  @Override
  protected boolean isAllowedToPerformActionOnResource(String personId, OrganizationalUnitAwareResource existingResource,
          DLCMControllerAction action) {

    if (existingResource != null) {
      if (((ArchiveMetadata) existingResource).getCurrentAccess() == Access.PUBLIC) {
        if (action == DLCMControllerAction.DOWNLOAD_ARCHIVE || action == DLCMControllerAction.PREPARE_DOWNLOAD_ARCHIVE
                || action == DLCMControllerAction.LIST_FILES) {
          return true;
        }
      } else if (Boolean.TRUE.equals(((ArchiveMetadata) existingResource).isStructureContentPublic())
              && action == DLCMControllerAction.LIST_FILES) {
        return true;
      } else if (((ArchiveMetadata) existingResource).getCurrentAccess() == Access.RESTRICTED) {
        return this.isAllowedActionWithRestrictedAccess(personId, existingResource.getOrganizationalUnitId(), action);
      } else if (((ArchiveMetadata) existingResource).getCurrentAccess() == Access.CLOSED) {
        return this.isAllowedActionWithClosedAccess(personId, existingResource, action);
      }
    }
    return super.isAllowedToPerformActionOnResource(personId, existingResource, action);
  }

  @Override
  protected boolean isApproverAllowed(OrganizationalUnitAwareResource existingDeposit, DLCMControllerAction action) {
    return true;
  }

  @Override
  protected boolean isCreatorAllowed(OrganizationalUnitAwareResource existingDeposit, DLCMControllerAction action) {
    return true;
  }

  @Override
  protected boolean isManagerAllowed(OrganizationalUnitAwareResource existingDeposit, DLCMControllerAction action) {
    return true;
  }

  @Override
  protected boolean isRoleAllowed(Role role, OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    if (action != DLCMControllerAction.DOWNLOAD_ARCHIVE &&
            action != DLCMControllerAction.PREPARE_DOWNLOAD_ARCHIVE &&
            action != DLCMControllerAction.GET) {
      return false;
    }
    return super.checkActionByRole(role, existingResource, action);
  }

  @Override
  protected boolean isStewardAllowed(OrganizationalUnitAwareResource existingDeposit, DLCMControllerAction action) {
    return true;
  }

  @Override
  protected boolean isVisitorAllowed(OrganizationalUnitAwareResource existingDeposit, DLCMControllerAction action) {
    return true;
  }

  @Override
  public ArchiveMetadata getExistingResource(String id) {
    return this.searchService.findOne(id);
  }

  public boolean isAllowedActionWithRestrictedAccess(String personId, String orgId, DLCMControllerAction action) {
    if (action == DLCMControllerAction.DOWNLOAD_ARCHIVE || action == DLCMControllerAction.PREPARE_DOWNLOAD_ARCHIVE) {
      final Optional<Role> orgUnitRole = this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(personId, orgId);
      final Optional<Role> inheritedRole = this.trustedPersonRemoteResourceService.findInheritedOrganizationalRole(personId, orgId);
      final Optional<Role> role = Role.maxRole(orgUnitRole, inheritedRole);
      return role.isPresent();
    }
    return false;
  }

  private boolean isAllowedActionWithClosedAccess(String personId, OrganizationalUnitAwareResource existingResource,
          DLCMControllerAction action) {
    if (action == DLCMControllerAction.DOWNLOAD_ARCHIVE || action == DLCMControllerAction.PREPARE_DOWNLOAD_ARCHIVE
            || action == DLCMControllerAction.LIST_FILES) {
      final Optional<Role> orgUnitRole = this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(personId,
              existingResource.getOrganizationalUnitId());
      final Optional<Role> inheritedRole = this.trustedPersonRemoteResourceService.findInheritedOrganizationalRole(personId,
              existingResource.getOrganizationalUnitId());
      final Optional<Role> role = Role.maxRole(orgUnitRole, inheritedRole);
      return role.filter(value -> value.getResId().equals(Role.MANAGER_ID) || value.getResId().equals(Role.STEWARD_ID)).isPresent();
    }
    return false;
  }

}

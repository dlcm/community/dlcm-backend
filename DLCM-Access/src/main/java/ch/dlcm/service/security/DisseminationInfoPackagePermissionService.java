/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DisseminationInfoPackagePermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.dlcm.business.DisseminationInfoPackageService;
import ch.dlcm.controller.AccessController;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class DisseminationInfoPackagePermissionService extends AbstractPermissionWithOrgUnitService {

  private final MetadataPermissionService metadataPermissionService;
  private final DownloadWithAclPermissionService downloadWithAclPermissionService;
  private final DisseminationInfoPackageService dipService;

  public DisseminationInfoPackagePermissionService(DisseminationInfoPackageService dipService,
          MetadataPermissionService metadataPermissionService,
          DownloadWithAclPermissionService downloadWithAclPermissionService,
          TrustedPersonRemoteResourceService personRemoteResourceService) {
    super(personRemoteResourceService);
    this.metadataPermissionService = metadataPermissionService;
    this.downloadWithAclPermissionService = downloadWithAclPermissionService;
    this.dipService = dipService;
  }

  @Override
  protected boolean isApproverAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    /**
     * So far, use same rights as Creator
     */
    return this.isCreatorAllowed(existingResource, action);
  }

  @Override
  protected boolean isCreatorAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    if (existingResource instanceof DisseminationInfoPackage) {

      final PackageStatus existingValue = ((DisseminationInfoPackage) existingResource).getInfo().getStatus();

      return (existingValue == PackageStatus.IN_ERROR || existingValue == PackageStatus.IN_PREPARATION) && action == DLCMControllerAction.RESUME;
    }

    return false;
  }

  @Override
  protected boolean isManagerAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return true;
  }

  @Override
  protected boolean isStewardAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    /**
     * So far, use same rights as Creator
     */
    return this.isCreatorAllowed(existingResource, action);
  }

  @Override
  protected boolean isAllowedToPerformActionOnResource(String personId, OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    if (action == DLCMControllerAction.GET && existingResource != null &&
            ((DisseminationInfoPackage) existingResource).getInfo().isAccessCurrentlyPublic()) {
      return true;
    }

    return super.isAllowedToPerformActionOnResource(personId, existingResource, action);
  }
  public boolean isAllowedToDownload(String dipId) {
    final DisseminationInfoPackage dip = this.dipService.findOne(dipId);
    boolean isDipAllowed = true;
    for(String aipId : dip.getAipIds()) {
      boolean isAipAllowed = metadataPermissionService.isAllowed(aipId, "DOWNLOAD_ARCHIVE") || downloadWithAclPermissionService.isAllowed(aipId);
      isDipAllowed = isDipAllowed && isAipAllowed;
    }
    return isDipAllowed;
  }

  @Override
  OrganizationalUnitAwareResource getExistingResource(String resId) {
    return this.dipService.findOne(resId);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AccessRefreshMgmt.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.AccessRefreshMessage;

@Service
@ConditionalOnBean(AccessController.class)
public class AccessRefreshMgmt extends MessageProcessorBySize<AccessRefreshMessage> {
  private static final Logger log = LoggerFactory.getLogger(AccessRefreshMgmt.class);

  protected final AccessRefreshService accessRefreshService;
  protected final ArchivalInfoPackageService aipService;

  public AccessRefreshMgmt(AccessRefreshService accessRefreshService, ArchivalInfoPackageService aipService, DLCMProperties dlcmProperties) {
    super(dlcmProperties);
    this.accessRefreshService = accessRefreshService;
    this.aipService = aipService;
  }

  @JmsListener(destination = "${dlcm.queue.aip-refresh}")
  @Override
  public void receiveMessage(AccessRefreshMessage aipMessage) {
    if (aipMessage.getResId() != null && this.isBigPackageSize(this.aipService.getSize(aipMessage.getResId()))) {
      aipMessage.setBigArchive(true);
    }
    this.sendForParallelProcessing(aipMessage);
  }

  @Override
  public void processMessage(AccessRefreshMessage aipMessage) {
    this.setSecurityContext();
    log.info("Reading message {}", aipMessage);
    try {
      this.accessRefreshService.refreshAip(aipMessage.getResId());
    } catch (final Exception e) {
      log.error("Cannot refresh AIP ({})", aipMessage.getResId(), e);
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AccessUpdateService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import static ch.unige.solidify.rest.RestCollectionPage.MAX_SIZE_PAGE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.business.DisseminationInfoPackageService;
import ch.dlcm.business.OrderService;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.QueryType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DisseminationInfoPackage;

@Service
@ConditionalOnBean(AccessController.class)
public class AccessRefreshService {
  private static final Logger log = LoggerFactory.getLogger(AccessRefreshService.class);

  private static final int MAXIMUM_COLLECTION_LEVEL = 30;

  private final OrderService orderService;
  private final DisseminationInfoPackageService dipService;
  private final ArchivalInfoPackageService aipService;

  public AccessRefreshService(OrderService orderService,
          DisseminationInfoPackageService dipService,
          ArchivalInfoPackageService aipService) {
    this.orderService = orderService;
    this.dipService = dipService;
    this.aipService = aipService;
  }

  public void refreshAip(String aipId) {
    log.info("Refresh AIP {}", aipId);

    // Add enclosing collections
    List<ArchivalInfoPackage> updatedAipList = this.aipService.findCollectionsContainingAip(aipId);
    updatedAipList = this.addEnclosingCollections(updatedAipList, 0);
    List<String> updatedAipIdList = updatedAipList.stream()
            .map(ResourceNormalized::getResId)
            .collect(Collectors.toList());

    // Reverse list to have most enclosing collections first, needed for deletion
    Collections.reverse(updatedAipIdList);

    // Remove duplicates, keep first occurrence, delete others
    updatedAipIdList = new ArrayList<>(new LinkedHashSet<>(updatedAipIdList));

    // And the AIP itself
    updatedAipIdList.add(aipId);

    // Create a list of all orders of impacted AIPs
    final List<Order> orderList = new ArrayList<>();
    for (String updatedAipId : updatedAipIdList) {
      orderList.addAll(this.orderService.findByAip(updatedAipId));
    }

    // Delete associated orders
    for (Order order : orderList) {
      log.info("Delete order {}", order.getResId());
      this.orderService.delete(order.getResId());
    }

    // Delete associated DIP
    List<DisseminationInfoPackage> dipList = new ArrayList<>();
    for (String updatedAipId : updatedAipIdList) {
      try {
        dipList.addAll(this.dipService.findDipContainingAip(updatedAipId, PageRequest.of(0, MAX_SIZE_PAGE)).getContent());
      } catch (SolidifyResourceNotFoundException e) {
        log.info("No DIP associated to AIP {} to delete", updatedAipId);
      }
    }
    for (DisseminationInfoPackage dip : dipList) {
      log.info("Delete DIP {}", dip.getResId());
      this.dipService.delete(dip.getResId());
    }

    // Delete associated AipDownload
    for (String updatedAipId : updatedAipIdList) {
      try {
        log.info("Delete AIPdownload {}", updatedAipId);
        this.aipService.delete(updatedAipId);
      } catch (SolidifyResourceNotFoundException e) {
        log.info("No AipDownload associated to AIP {} to delete", updatedAipId);
      }
    }

    // Recreate direct order
    for (Order order : orderList) {
      if (order.getQueryType().equals(QueryType.DIRECT)) {
        // Allow the recreated order to be submitted again
        log.info("Recreate order {}", order.getResId());
        this.orderService.recreateOrder(order);
      }
    }
  }

  private List<ArchivalInfoPackage> addEnclosingCollections(List<ArchivalInfoPackage> aipList, int recursiveCounter) {
    if (recursiveCounter > MAXIMUM_COLLECTION_LEVEL) {
      throw new SolidifyRuntimeException("No more than " + MAXIMUM_COLLECTION_LEVEL + " collection level is accepted");
    }
    recursiveCounter++;
    List<ArchivalInfoPackage> newList = new ArrayList<>(aipList);
    for (ArchivalInfoPackage aip : aipList) {
      if (aip.isCollection()) {
        final List<ArchivalInfoPackage> collectionsContainingAip = this.aipService.findCollectionsContainingAip(aip.getResId());
        newList.addAll(this.addEnclosingCollections(collectionsContainingAip, recursiveCounter));
      }
    }
    return newList;
  }

}

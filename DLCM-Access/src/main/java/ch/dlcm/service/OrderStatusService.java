/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrderStatusService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.assembling.AssemblingService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.OrderMessage;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.repository.OrderRepository;

@Service
@ConditionalOnBean(AccessController.class)
public class OrderStatusService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(OrderStatusService.class);

  private final AssemblingService assemblingService;
  private final OrderRepository orderRepository;

  public OrderStatusService(MessageService messageService, OrderRepository orderRepository, AssemblingService assemblingService,
          DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.orderRepository = orderRepository;
    this.assemblingService = assemblingService;
  }

  @Transactional
  public void checkOrderProgress() {
    final List<OrderStatus> statusToCheck = Arrays.asList(OrderStatus.IN_DISSEMINATION_PREPARATION, OrderStatus.DOWNLOADING);
    for (Order order : this.orderRepository.findByStatusIn(statusToCheck)) {
      switch (order.getStatus()) {
        case IN_DISSEMINATION_PREPARATION -> {
          order = this.orderRepository.findOneWithDip(order.getResId());
          if (order.isCompleted()) {
            order.setOrderStatus(OrderStatus.READY);
          } else if (order.hasDipInError()) {
            order.setErrorStatus(this.messageService.get("access.order.error.dip"));
          }
        }
        case DOWNLOADING -> {
          order = this.orderRepository.findOneWithAip(order.getResId());
          if (order.isAllAipCompleted()) {
            order.setOrderStatus(OrderStatus.SUBMITTED);
          } else if (order.hasAipInError()) {
            order.setErrorStatus(this.messageService.get("access.order.error.aip"));
          }
        }
        default -> {
          continue;
        }
      }
      this.orderRepository.save(order);
      if (order.getStatus() == OrderStatus.SUBMITTED) {
        SolidifyEventPublisher.getPublisher().publishEvent(new OrderMessage(order.getResId()));
      }
      this.logOrderInfo("Order processed", order);
    }
  }

  @Transactional
  public Order processOrder(String orderId) {
    // Load Order
    Order order = this.orderRepository.findById(orderId).orElseThrow(() -> new NoSuchElementException("Order " + orderId + " not found"));
    try {
      this.logOrderInfo("Order", order);
      switch (order.getStatus()) {
        case SUBMITTED -> this.processSubmittedOrder(order);
        case IN_PREPARATION -> this.processInPreparationOrder(order);
        default -> this.logOrderInfo("The order has a status that does not require any processing", order);
      }
    } catch (final Exception e) {
      log.error("Error in processing order (" + order.getResId() + ")", e);
      order.setErrorStatus(e.getMessage());
    }
    this.orderRepository.save(order);

    this.logOrderInfo("Order processed", order);
    return order;
  }

  private void processInPreparationOrder(Order order) throws JAXBException, IOException {
    if (this.assemblingService.getStatus(order.getResId(), null) == OrderStatus.IN_PROGRESS) {
      if (this.assemblingService.submitOrder(order) == null) {
        order.setErrorStatus(this.messageService.get("access.order.error.submission"));
      }
    } else {
      final List<DisseminationInfoPackage> dips = this.assemblingService.createDipPackages(order);
      if (!dips.isEmpty()) {
        order = this.orderRepository.findOneWithDip(order.getResId());
        order.getDipPackages().addAll(dips);
        order.setOrderStatus(OrderStatus.IN_DISSEMINATION_PREPARATION);
      } else {
        order.setErrorStatus(this.messageService.get("access.order.error.sending"));
      }
    }
  }

  private void processSubmittedOrder(Order order) {
    if (order.getAipPackages().isEmpty()) {
      order.setErrorStatus(this.messageService.get("access.order.datafile.noaip"));
    } else {
      if (order.isAllAipCompleted()) {
        order.setOrderStatus(OrderStatus.IN_PREPARATION);
        if (this.assemblingService.generateDipPackages(order) == null) {
          order.setErrorStatus(this.messageService.get("access.order.error.preparation"));
        }
      } else {
        order.setOrderStatus(OrderStatus.DOWNLOADING);
      }
    }
  }

  private void logOrderInfo(String message, Order order) {
    log.info("{}: {} [resId={} status={}]", message, order.getName(), order.getResId(), order.getStatus());
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - HederaDipBuilderService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.dissemination;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.OrderService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.MetadataService;

public class HederaDipBuilderService extends DipBuilderService {

  private static final Logger log = LoggerFactory.getLogger(HederaDipBuilderService.class);

  private final String server;
  private final String project;

  // Files
  private final String sourceDatasetsFilesSourceFolder;
  private final String rdfFilesSourceFolder;
  private final String researchFilesIiifSourceFolder;
  private final String researchFilesManifestIiifSourceFolder;
  private final String researchFilesTeiSourceFolder;
  private final String researchFilesWebSourceFolder;

  private final String sourceDatasetsFilesTargetFolder;
  private final String rdfFilesTargetFolder;
  private final String researchFilesIiifTargetFolder;
  private final String researchFilesManifestIiifTargetFolder;
  private final String researchFilesTeiTargetFolder;
  private final String researchFilesWebTargetFolder;

  private final List<String> sourceDatasetsFilesPatterns;
  private final List<String> rdfFilesPatterns;
  private final List<String> researchFilesIiifPatterns;
  private final List<String> researchFilesManifestIiifPatterns;
  private final List<String> researchFilesTeiPatterns;
  private final List<String> researchFilesWebPatterns;

  public HederaDipBuilderService(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repository,
          MessageService messageService,
          MetadataService metadataService,
          OrderService orderService,
          Map<String, String> parametersMap) {
    super(dlcmProperties, repository, messageService, metadataService, orderService);
    this.project = parametersMap.get(DLCMConstants.DP_HEDERA_PROJECT_PARAM);
    this.server = parametersMap.get(DLCMConstants.DP_HEDERA_SERVER_PARAM);

    this.sourceDatasetsFilesSourceFolder = parametersMap.get(DLCMConstants.DP_HEDERA_SOURCE_DATASET_FILE_SOURCE_FOLDER_PARAM);
    this.rdfFilesSourceFolder = parametersMap.get(DLCMConstants.DP_HEDERA_RDF_DATA_FILE_SOURCE_FOLDER_PARAM);
    this.researchFilesIiifSourceFolder = parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_IIIF_SOURCE_FOLDER_PARAM);
    this.researchFilesManifestIiifSourceFolder = parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_SOURCE_FOLDER_PARAM);
    this.researchFilesTeiSourceFolder = parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_TEI_SOURCE_FOLDER_PARAM);
    this.researchFilesWebSourceFolder = parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_WEB_SOURCE_FOLDER_PARAM);

    this.sourceDatasetsFilesTargetFolder = parametersMap.get(DLCMConstants.DP_HEDERA_SOURCE_DATASET_FILE_TARGET_FOLDER_PARAM);
    this.rdfFilesTargetFolder = parametersMap.get(DLCMConstants.DP_HEDERA_RDF_DATA_FILE_TARGET_FOLDER_PARAM);
    this.researchFilesIiifTargetFolder = parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_IIIF_TARGET_FOLDER_PARAM);
    this.researchFilesManifestIiifTargetFolder = parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_TARGET_FOLDER_PARAM);
    this.researchFilesTeiTargetFolder = parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_TEI_TARGET_FOLDER_PARAM);
    this.researchFilesWebTargetFolder = parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_WEB_TARGET_FOLDER_PARAM);

    this.sourceDatasetsFilesPatterns = this.getPatternList(parametersMap.get(DLCMConstants.DP_HEDERA_SOURCE_DATASET_FILE_PATTERN_PARAM));
    this.rdfFilesPatterns = this.getPatternList(parametersMap.get(DLCMConstants.DP_HEDERA_RDF_DATA_FILE_PATTERN_PARAM));
    this.researchFilesIiifPatterns = this.getPatternList(parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_IIIF_PATTERN_PARAM));
    this.researchFilesManifestIiifPatterns = this.getPatternList(parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_PATTERN_PARAM));
    this.researchFilesTeiPatterns = this.getPatternList(parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_TEI_PATTERN_PARAM));
    this.researchFilesWebPatterns = this.getPatternList(parametersMap.get(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_WEB_PATTERN_PARAM));
  }

  @Override
  public Path getBasePathToCreateDipZip(Order order, Path accessPath) {
    if (order.getAipPackages().size() == 1) {
      return accessPath.resolve(order.getAipPackages().get(0).getResId());
    } else {
      return accessPath;
    }
  }

  @Override
  public void buildDisseminationPolicyDip(Order order, ArchivalInfoPackage aip, Path workingFolder) {
    this.extractDataciteMetadata(workingFolder, aip.getMetadataVersion());

    try {
      this.createDirectories(workingFolder);
      List<Path> targetFoldersToIgnores = List.of(
              workingFolder.resolve(this.sourceDatasetsFilesTargetFolder),
              workingFolder.resolve(this.rdfFilesTargetFolder),
              workingFolder.resolve(this.researchFilesIiifTargetFolder),
              workingFolder.resolve(this.researchFilesManifestIiifTargetFolder),
              workingFolder.resolve(this.researchFilesTeiTargetFolder),
              workingFolder.resolve(this.researchFilesWebTargetFolder)
      );

      Path researchDataFolder = workingFolder.resolve(DLCMConstants.RESEARCH_DATA_FOLDER);
      List<String> allFilesPatters = List.of(DLCMConstants.ANT_ALL);
      this.filterWorkingDirectory(researchDataFolder.resolve(this.sourceDatasetsFilesSourceFolder),
              workingFolder.resolve(this.sourceDatasetsFilesTargetFolder), targetFoldersToIgnores, allFilesPatters, true);
      this.filterWorkingDirectory(researchDataFolder.resolve(this.rdfFilesSourceFolder),
              workingFolder.resolve(this.rdfFilesTargetFolder), targetFoldersToIgnores, allFilesPatters, true);
      this.filterWorkingDirectory(researchDataFolder.resolve(this.researchFilesIiifSourceFolder),
              workingFolder.resolve(this.researchFilesIiifTargetFolder), targetFoldersToIgnores, allFilesPatters, true);
      this.filterWorkingDirectory(researchDataFolder.resolve(this.researchFilesManifestIiifSourceFolder),
              workingFolder.resolve(this.researchFilesManifestIiifTargetFolder), targetFoldersToIgnores, allFilesPatters, true);
      this.filterWorkingDirectory(researchDataFolder.resolve(this.researchFilesTeiSourceFolder),
              workingFolder.resolve(this.researchFilesTeiTargetFolder), targetFoldersToIgnores, allFilesPatters, true);
      this.filterWorkingDirectory(researchDataFolder.resolve(this.researchFilesWebSourceFolder),
              workingFolder.resolve(this.researchFilesWebTargetFolder), targetFoldersToIgnores, allFilesPatters, true);

    } catch (IOException e) {
      log.error("Failed to create/write subdirectory: {}, {}, {}, {}, {}, {}", this.sourceDatasetsFilesTargetFolder, this.rdfFilesTargetFolder, this.researchFilesIiifTargetFolder, this.researchFilesManifestIiifTargetFolder, this.researchFilesTeiTargetFolder, this.researchFilesWebTargetFolder);
      throw new SolidifyRuntimeException("Failed to create/write subdirectory: " + this.sourceDatasetsFilesTargetFolder + ", " + this.rdfFilesTargetFolder + ", " + this.researchFilesIiifTargetFolder + ", " + this.researchFilesManifestIiifTargetFolder + ", " + this.researchFilesTeiTargetFolder + " and " + this.researchFilesWebTargetFolder, e);
    }

    FileTool.deleteFolder(workingFolder.resolve(DLCMConstants.RESEARCH_DATA_FOLDER));
  }

  @Override
  protected List<String> getFilesToExtract(Order order) {
    List<String> fileList = new ArrayList<>();
    fileList.add(DLCMConstants.METADATA_FILE);
    fileList.addAll(this.sourceDatasetsFilesPatterns);
    fileList.addAll(this.rdfFilesPatterns);
    fileList.addAll(this.researchFilesIiifPatterns);
    fileList.addAll(this.researchFilesManifestIiifPatterns);
    fileList.addAll(this.researchFilesTeiPatterns);
    fileList.addAll(this.researchFilesWebPatterns);
    return fileList;
  }

  private void createDirectories(Path workingFolder) throws IOException {
    Files.createDirectories(workingFolder.resolve(this.sourceDatasetsFilesTargetFolder));
    Files.createDirectories(workingFolder.resolve(this.rdfFilesTargetFolder));
    Files.createDirectories(workingFolder.resolve(this.researchFilesIiifTargetFolder));
    Files.createDirectories(workingFolder.resolve(this.researchFilesManifestIiifTargetFolder));
    Files.createDirectories(workingFolder.resolve(this.researchFilesTeiTargetFolder));
    Files.createDirectories(workingFolder.resolve(this.researchFilesWebTargetFolder));
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - BasicDipBuilderService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.dissemination;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.OrderService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.Package;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.OrderSubsetItem;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.MetadataService;

@Service
@ConditionalOnBean(AccessController.class)
public class BasicDipBuilderService extends DipBuilderService {

  public BasicDipBuilderService(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repository,
          MessageService messageService,
          MetadataService metadataService,
          OrderService orderService) {
    super(dlcmProperties, repository, messageService, metadataService, orderService);
  }

  @Override
  public void buildDisseminationPolicyDip(Order order, ArchivalInfoPackage aip, Path workingFolder) {
    // Extract datacite part from dlcm.xml to a new file and delete it
    this.extractDataciteMetadata(workingFolder, aip.getMetadataVersion());
  }

  @Override
  public Path getBasePathToCreateDipZip(Order order, Path accessPath) {
    if (order.getAipPackages().size() == 1) {
      return accessPath.resolve(order.getAipPackages().get(0).getResId());
    } else {
      return accessPath;
    }
  }

  @Override
  protected List<String> getFilesToExtract(Order order) {
    List<String> fileList = new ArrayList<>();
    fileList.add(Package.METADATA_FILE.getName());
    if (order.getSubsetItems() != null
            && !order.getSubsetItems().isEmpty()) {
      for (OrderSubsetItem subsetItem : order.getSubsetItems()) {
        this.addCommonFolders(fileList, subsetItem.getItemPath());
      }
    } else {
      this.addCommonFolders(fileList, "/**");
      fileList.add(Package.METADATA.getName().substring(1) + "/**");
      fileList.add(Package.INTERNAL.getName().substring(1) + "/" + Package.ARCHIVE_THUMBNAIL.getName());
      fileList.add(Package.INTERNAL.getName().substring(1) + "/" + Package.ARCHIVE_DUA.getName());
      fileList.add(Package.INTERNAL.getName().substring(1) + "/" + Package.ARCHIVE_README.getName());
      fileList.add(Package.INTERNAL.getName().substring(1) + "/" + Package.DATASET_THUMBNAIL.getName());
    }
    return fileList;
  }

}

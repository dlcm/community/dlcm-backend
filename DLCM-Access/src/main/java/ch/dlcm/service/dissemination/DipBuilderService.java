/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DipBuilderService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.dissemination;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.AntPathMatcher;

import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.OrderService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.model.Access;
import ch.dlcm.model.Package;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.DLCMService;
import ch.dlcm.service.MetadataService;

public abstract class DipBuilderService extends DLCMService {

  private static final Logger log = LoggerFactory.getLogger(DipBuilderService.class);

  protected final MetadataService metadataService;
  protected final OrderService orderService;
  protected final String repositoryPrefix;

  protected DipBuilderService(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repository,
          MessageService messageService,
          MetadataService metadataService,
          OrderService orderService) {
    super(messageService, dlcmProperties);
    this.repositoryPrefix = repository.getName();
    this.metadataService = metadataService;
    this.orderService = orderService;

  }

  public abstract void buildDisseminationPolicyDip(Order order, ArchivalInfoPackage aip, Path workingFolder);

  public abstract Path getBasePathToCreateDipZip(Order order, Path accessPath);

  protected abstract List<String> getFilesToExtract(Order order);

  public boolean createDipArchive(String orderLocation, Order order, String dipId, String orgUnitId, Access accessLevel, Path accessPath,
          Path dipFolder) {
    final ZipTool zipTool;

    // Create order archive (i.e. DIP) & Purge working files for each orgUnit
    zipTool = new ZipTool(this.getDipArchiveUri(orderLocation, order.getResId(), orgUnitId, accessLevel, dipId));
    if (!zipTool.zipFiles(this.getBasePathToCreateDipZip(order, accessPath))) {
      return false;
    }
    order.getAipPackages().forEach(aip -> FileTool.deleteFolder(accessPath.resolve(aip.getResId())));
    return true;
  }

  public Path getDestinationFolderToUnzipAip(ArchivalInfoPackage aip, Path rootFolder) {
    return rootFolder.resolve(aip.getResId());
  }

  public URI getDipArchiveUri(String rootPath, String orderId, String orgUnitId, Access accessLevel, String dipId) {
    return Paths.get(rootPath).resolve(orderId).resolve(orgUnitId).resolve(accessLevel.name())
            .resolve(dipId + SolidifyConstants.ZIP_EXT).toUri();
  }

  public boolean buildDip(Path rootFolder, List<ArchivalInfoPackage> aips, Order order) {
    for (final ArchivalInfoPackage aip : aips) {
      final Path destinationFolder = this.getDestinationFolderToUnzipAip(aip, rootFolder);
      final List<String> filesToExtract = this.getFilesToExtract(order);
      if (!this.extractArchive(aip, filesToExtract, destinationFolder)) {
        return false;
      }

      // Check that there is at least one data file apart from the internal folder
      if (!isThereAtLeastOneFileInArchiveExcludingMetadataFolder(destinationFolder)) {
        throw new SolidifyCheckingException("Error when checking if there is at least one file in the archive");
      }

      this.buildDisseminationPolicyDip(order, aip, destinationFolder);

      if (aip.isCollection()) {
        return this.buildDip(destinationFolder, aip.getCollection(), order);
      }
    }
    return true;

  }

  protected void extractDataciteMetadata(Path workingFolder, DLCMMetadataVersion metadataVersion) {
    try {
      this.metadataService.extractDataciteMetadataFromFile(workingFolder.resolve(DLCMConstants.METADATA_FILE),
              Files.newOutputStream(workingFolder.resolve(this.repositoryPrefix + "-" + DLCMConstants.DATACITE_XML_FIELD)), metadataVersion);
      Files.delete(workingFolder.resolve(DLCMConstants.METADATA_FILE));
    } catch (IOException e) {
      throw new SolidifyRuntimeException("IOException when reading metadata file", e);
    } catch (JAXBException e) {
      throw new SolidifyRuntimeException("JAXBException when extracting datacite meta", e);
    }
  }

  public static boolean isThereAtLeastOneFileInArchiveExcludingMetadataFolder(Path rootFolder) {
    final boolean[] fileExists = new boolean[1];
    String folderToExclude = DLCMConstants.INTERNAL_FOLDER;

    try {
      Files.walkFileTree(rootFolder, new SimpleFileVisitor<>() {
        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
          // If there is a folder with the same name of the folderToExclude but it is not a root level, it should be checked and not skipped.
          if (dir.getFileName().toString().equals(folderToExclude) && dir.getParent().toString().equals(rootFolder.toString())) {
            return FileVisitResult.SKIP_SUBTREE;
          } else {
            return FileVisitResult.CONTINUE;
          }
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
          // Found a file in the directory
          if (!file.getFileName().endsWith(DLCMConstants.METADATA_FILE)) {
            fileExists[0] = true;
            return FileVisitResult.TERMINATE;
          }
          return FileVisitResult.CONTINUE;
        }
      });
    } catch (IOException e) {
      log.error("IOException when checking if there is at least one file in the archive: {}", rootFolder);
      throw new SolidifyRuntimeException("IOException when checking if there is at least one file in the archive", e);
    }

    return fileExists[0];
  }

  protected void addCommonFolders(List<String> fileList, String fileAntPattern) {
    // Add common folders (~ data categories)
    fileList.addAll(Package.getCommonFoldersAntPath(fileAntPattern));
  }

  // Extract an AIU
  private boolean extractArchive(ArchivalInfoPackage aip, List<String> filesToExtract, Path destination) {
    // Adapt item path list to extract to archive container
    this.normalizePathForArchiveContainer(aip, filesToExtract);
    // Unzip data
    final ZipTool zipAip = new ZipTool(aip.getDownloadUriForAccess());
    return switch (aip.getArchiveContainer()) {
      case BAG_IT -> zipAip.unzipFiles(destination, filesToExtract, DLCMConstants.DATA_FOLDER + File.separator, false);
      case ZIP -> zipAip.unzipFiles(destination, filesToExtract, false);
      default -> throw new SolidifyCheckingException(
              this.messageService.get("archival.aip.error.container", new Object[] { aip.getArchiveContainer() }));
    };
  }

  private void normalizePathForArchiveContainer(ArchivalInfoPackage aip, List<String> filesToExtract) {
    switch (aip.getArchiveContainer()) {
      case BAG_IT -> {
        for (int i = 0; i < filesToExtract.size(); i++) {
          final String initialValue = filesToExtract.get(i);
          filesToExtract.set(i, DLCMConstants.DATA_FOLDER + "/" + initialValue);
        }
      }
      case ZIP -> {
        // Do nothing
      }
      default -> throw new SolidifyCheckingException(
              this.messageService.get("archival.aip.error.container", new Object[] { aip.getArchiveContainer() }));
    }
  }

  protected void filterWorkingDirectory(Path sourceDir, Path targetDir, List<Path> foldersToIgnores, List<String> patterns,
          boolean keepRelativeLocation) {
    if (!Files.isDirectory(sourceDir)) {
      log.warn("Source directory {} is not a valid directory.", sourceDir);
      return;
    }

    try (Stream<Path> pathStream = Files.walk(sourceDir)) {
      pathStream.filter(path -> !Files.isDirectory(path) && !this.isChild(path, foldersToIgnores))
              .forEach(file -> {
                this.processFile(file, sourceDir, targetDir, patterns, keepRelativeLocation);
              });
    } catch (IOException e) {
      log.error("Error when walking through directory {}.", sourceDir);
      throw new SolidifyProcessingException("Error when walking through directory: " + sourceDir, e);
    }
  }

  private void processFile(Path file, Path sourceDir, Path targetDir, List<String> patterns, boolean keepRelativeLocation) {
    Path relativePath = sourceDir.relativize(file);
    if (this.matchesAny(relativePath, patterns)) {
      try {
        Path targetFilePath = keepRelativeLocation ? targetDir.resolve(relativePath) : targetDir.resolve(file.getFileName());
        FileTool.moveFile(file, targetFilePath);
      } catch (IOException e) {
        log.error("Failed to copy file: {} to destination: {}", file, targetDir);
        throw new SolidifyProcessingException("Failed to copy file: " + file + " to destination: " + targetDir, e);
      }
    }
  }

  private boolean matchesAny(Path entryName, List<String> patterns) {
    AntPathMatcher matcher = new AntPathMatcher();
    String entryNameNormalized = entryName.toString().replace(File.separatorChar, '/');
    return patterns.stream().anyMatch(pattern -> matcher.match(pattern, entryNameNormalized));
  }

  private boolean isChild(Path child, List<Path> parentsFolders) {
    Path finalChild = child.normalize();
    return parentsFolders.stream()
            .map(Path::normalize)
            .anyMatch(finalChild::startsWith);
  }

  private static boolean isValidAntPattern(String pattern) {
    return new AntPathMatcher().isPattern(pattern);
  }

  protected List<String> getPatternList(String patternList) {
    return Stream.of(patternList.split(","))
            .map(String::trim)
            .filter(DipBuilderService::isValidAntPattern)
            .toList();
  }

}

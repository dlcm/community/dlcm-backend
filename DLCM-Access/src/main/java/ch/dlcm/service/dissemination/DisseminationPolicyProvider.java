/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DisseminationPolicyProvider.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.dissemination;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.OrderService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.display.OrganizationalUnitDisseminationPolicyDTO;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.model.tool.CleanTool;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.abstractservice.DisseminationPolicyRemoteResourceService;
import ch.dlcm.service.rest.abstractservice.OrganizationalDisseminationPolicyRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDisseminationPolicyRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationnalUnitDisseminationPolicyRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class DisseminationPolicyProvider {

  private final DisseminationPolicyRemoteResourceService disseminationPolicyRemoteResourceService;
  private final OrganizationalDisseminationPolicyRemoteResourceService orgUnitDisseminationPolicyRemoteService;
  private final DLCMProperties dlcmProperties;
  private final DLCMRepositoryDescription repository;
  private final MessageService messageService;
  private final MetadataService metadataService;
  private final OrderService orderService;

  private final BasicDipBuilderService basicDipBuilderService;
  private final OaisDipBuilderService oaisDipBuilderService;

  public DisseminationPolicyProvider(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repository,
          MessageService messageService,
          MetadataService metadataService,
          OrderService orderService,
          BasicDipBuilderService basicDipBuilderService,
          OaisDipBuilderService oaisDipBuilderService,
          TrustedDisseminationPolicyRemoteResourceService disseminationPolicyRemoteResourceService,
          TrustedOrganizationnalUnitDisseminationPolicyRemoteResourceService orgUnitDisseminationPolicyRemoteService) {
    this.dlcmProperties = dlcmProperties;
    this.repository = repository;
    this.messageService = messageService;
    this.metadataService = metadataService;
    this.orderService = orderService;
    this.basicDipBuilderService = basicDipBuilderService;
    this.oaisDipBuilderService = oaisDipBuilderService;
    this.disseminationPolicyRemoteResourceService = disseminationPolicyRemoteResourceService;
    this.orgUnitDisseminationPolicyRemoteService = orgUnitDisseminationPolicyRemoteService;
  }

  public String calculateFinalFileName(DisseminationPolicy disseminationPolicy, String archiveName, String dipName) {
    return disseminationPolicy.getArchiveDownloadFileName(dipName, CleanTool.cleanTitle(archiveName));
  }

  public DipBuilderService getDisseminationPolicyEnforcementService(Order order, String orgUnitId) {
    final Map<String, String> parameters;
    DisseminationPolicy disseminationPolicy;
    if (order.getOrganizationalUnitDisseminationPolicyId() == null) {
      disseminationPolicy = this.disseminationPolicyRemoteResourceService.findOne(order.getDisseminationPolicyId());
      parameters = disseminationPolicy.getParametersMap();
    } else {
      OrganizationalUnitDisseminationPolicyDTO disseminationPolicyDTO = this.orgUnitDisseminationPolicyRemoteService.getOrganizationalUnitDisseminationPolicy(orgUnitId,
              order.getDisseminationPolicyId(), order.getOrganizationalUnitDisseminationPolicyId());
      parameters = disseminationPolicyDTO.getJoinResource().getParametersMap();
      disseminationPolicy = disseminationPolicyDTO;
    }

    return switch (disseminationPolicy.getType()) {
      case BASIC -> this.basicDipBuilderService;
      case OAIS -> this.oaisDipBuilderService;
      case IIIF -> new IiifDipBuilderService(
              this.dlcmProperties,
              this.repository,
              this.messageService,
              this.metadataService,
              this.orderService,
              parameters);
      case HEDERA -> new HederaDipBuilderService(
              this.dlcmProperties,
              this.repository,
              this.messageService,
              this.metadataService,
              this.orderService,
              parameters);
      default -> throw new IllegalArgumentException("Unexpected value: " + disseminationPolicy.getType());
    };
  }

}

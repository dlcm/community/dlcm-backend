/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OaisDipBuilderService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.dissemination;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.JAXBException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.OrderService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.Access;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.MetadataService;

@Service
@ConditionalOnBean(AccessController.class)
public class OaisDipBuilderService extends DipBuilderService {

  public OaisDipBuilderService(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repository,
          MessageService messageService,
          MetadataService metadataService,
          OrderService orderService) {
    super(dlcmProperties, repository, messageService, metadataService, orderService);
  }

  @Override
  public void buildDisseminationPolicyDip(Order order, ArchivalInfoPackage metadataVersion, Path workingFolder) {
    // No specific action to do for this dissemination policy, DIP should be the same as the AIP
  }

  @Override
  public Path getBasePathToCreateDipZip(Order order, Path accessPath) {
    return accessPath;
  }

  @Override
  public Path getDestinationFolderToUnzipAip(ArchivalInfoPackage aip, Path rootFolder) {
    return rootFolder.resolve(DLCMConstants.DATA_FOLDER).resolve(aip.getResId());
  }

  @Override
  protected List<String> getFilesToExtract(Order order) {
    List<String> fileList = new ArrayList<>();
    fileList.add(DLCMConstants.ANT_ALL);
    return fileList;
  }

  @Override
  public boolean createDipArchive(String orderLocation, Order order, String dipId, String orgUnitId, Access accessLevel, Path accessPath,
          Path dipFolder) {
    try {
      // For the OAIS dissemination policy, it is needed to generate the dlcm.xml metadata file
      this.metadataService.generateMetadata(order, dipId, orgUnitId, accessLevel, dipFolder);
      // Create order archive (i.e. DIP) & Purge working files for each orgUnit
      final ZipTool zipTool = new ZipTool(this.getDipArchiveUri(orderLocation, order.getResId(), orgUnitId, accessLevel, dipId));
      if (!zipTool.zipFiles(accessPath) || !FileTool.deleteFolder(accessPath.resolve(DLCMConstants.DATA_FOLDER))) {
        return false;
      }
      FileTool.deleteFile(accessPath.resolve(DLCMConstants.METADATA_FILE));
      return true;
    } catch (JAXBException | IOException e) {
      throw new SolidifyProcessingException(e.getMessage(), e);
    }
  }

}

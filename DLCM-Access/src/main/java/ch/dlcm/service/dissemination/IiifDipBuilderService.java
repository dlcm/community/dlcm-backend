/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - IiifDipBuilderService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.dissemination;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.OrderService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.MetadataService;

public class IiifDipBuilderService extends DipBuilderService {

  private static final Logger log = LoggerFactory.getLogger(IiifDipBuilderService.class);

  private final String mediaFolder;
  private final List<String> mediaPatterns;
  private final String manifestFolder;
  private final List<String> manifestPatterns;

  public IiifDipBuilderService(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repository,
          MessageService messageService,
          MetadataService metadataService,
          OrderService orderService,
          Map<String, String> parametersMap) {
    super(dlcmProperties, repository, messageService, metadataService, orderService);
    this.mediaFolder = parametersMap.get(DLCMConstants.DP_IIIF_MEDIA_FOLDER_PARAM);
    this.mediaPatterns = this.getPatternList(parametersMap.get(DLCMConstants.DP_IIIF_MEDIA_PATTERNS_PARAM));
    this.manifestFolder = parametersMap.get(DLCMConstants.DP_IIIF_MANIFESTS_FOLDER_PARAM);
    this.manifestPatterns = this.getPatternList(parametersMap.get(DLCMConstants.DP_IIIF_MANIFESTS_PATTERNS_PARAM));
  }

  @Override
  public Path getBasePathToCreateDipZip(Order order, Path accessPath) {
    if (order.getAipPackages().size() == 1) {
      return accessPath.resolve(order.getAipPackages().get(0).getResId());
    } else {
      return accessPath;
    }
  }

  @Override
  public void buildDisseminationPolicyDip(Order order, ArchivalInfoPackage aip, Path workingFolder) {
    this.extractDataciteMetadata(workingFolder, aip.getMetadataVersion());

    // create directories for "medias" and "manifests"
    try {
      this.createDirectories(workingFolder);
      List<Path> targetFoldersToIgnores = List.of(workingFolder.resolve(this.mediaFolder), workingFolder.resolve(this.manifestFolder));
      // filter for media pattern
      this.filterWorkingDirectory(workingFolder, workingFolder.resolve(this.mediaFolder), targetFoldersToIgnores, this.mediaPatterns, false);
      // filter for manifest pattern
      this.filterWorkingDirectory(workingFolder, workingFolder.resolve(this.manifestFolder), targetFoldersToIgnores, this.manifestPatterns, false);
    } catch (IOException e) {
      log.error("Failed to create/write subdirectory: {}, {}", this.mediaFolder, this.manifestFolder);
      throw new SolidifyRuntimeException("Failed to create/write subdirectory: " + this.mediaFolder + " and " + this.manifestFolder, e);
    }

    // clean working directories ===> delete researchData folder and keep only media/manifest folders
    FileTool.deleteFolder(workingFolder.resolve(DLCMConstants.RESEARCH_DATA_FOLDER));

  }

  @Override
  protected List<String> getFilesToExtract(Order order) {
    List<String> fileList = new ArrayList<>();
    fileList.add(DLCMConstants.METADATA_FILE);
    fileList.addAll(this.mediaPatterns);
    fileList.addAll(this.manifestPatterns);
    return fileList;
  }

  private void createDirectories(Path workingFolder) throws IOException {
    Files.createDirectories(workingFolder.resolve(this.mediaFolder));
    Files.createDirectories(workingFolder.resolve(this.manifestFolder));
  }
}

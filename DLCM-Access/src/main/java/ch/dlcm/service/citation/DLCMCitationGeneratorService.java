/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DataCitationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.citation;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.business.CitationService;
import ch.unige.solidify.config.CitationProperties;
import ch.unige.solidify.model.config.FormatsConfig;
import ch.unige.solidify.service.CitationGenerationService;

import ch.dlcm.controller.AccessController;
import ch.dlcm.service.SearchService;

@Service
@ConditionalOnBean(AccessController.class)
public class DLCMCitationGeneratorService extends CitationGenerationService {

  public DLCMCitationGeneratorService(CitationProperties citationProperties, SearchService searchService, CitationService citationService) {
    super(citationService, new ArchiveMetadataDataProvider(searchService),
            new FormatsConfig(citationProperties.getBibliographies().getStyles(),
                    citationProperties.getBibliographies().getLanguages(),
                    citationProperties.getBibliographies().getOutputFormats()),
            new FormatsConfig(citationProperties.getCitations().getStyles(),
                    citationProperties.getCitations().getLanguages(),
                    citationProperties.getCitations().getOutputFormats()));
  }
}

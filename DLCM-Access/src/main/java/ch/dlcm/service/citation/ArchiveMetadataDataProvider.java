/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - ArchiveMetadataDataProvider.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.citation;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.undercouch.citeproc.csl.CSLItemData;
import de.undercouch.citeproc.csl.CSLItemDataBuilder;
import de.undercouch.citeproc.csl.CSLName;
import de.undercouch.citeproc.csl.CSLNameBuilder;
import de.undercouch.citeproc.csl.CSLType;

import ch.unige.solidify.model.Citation;
import ch.unige.solidify.service.SolidifyItemDataProvider;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.service.SearchService;

public class ArchiveMetadataDataProvider implements SolidifyItemDataProvider {

  private SearchService searchService;

  public ArchiveMetadataDataProvider(SearchService searchService) {
    this.searchService = searchService;
  }

  /**************************************************************************************/

  @Override
  public CSLItemData retrieveItem(String archiveId, String style, String language, Citation.OutputFormat outputFormat) {

    final ArchiveMetadata archiveMetadata = this.searchService.findOne(archiveId);

    CSLType cslType = this.getCSLTypeFromDataciteType(archiveMetadata.getResourceTypeGeneral());
    CSLName[] authors = this.getAuthors(archiveMetadata.getCreators());

    CSLItemDataBuilder builder = new CSLItemDataBuilder().id(archiveId);
    builder.type(cslType)
            .title(archiveMetadata.getTitle())
            .author(authors)
            .DOI(archiveMetadata.getDoi())
            .publisher(archiveMetadata.getPublisher())
            .abstrct(archiveMetadata.getAbstract());

    if (archiveMetadata.getIssuedDate() != null) {
      OffsetDateTime issuedDate = archiveMetadata.getIssuedDate();
      builder.issued(issuedDate.getYear(), issuedDate.getMonthValue(), issuedDate.getDayOfMonth());
    }

    if (archiveMetadata.getAcceptedDate() != null) {
      OffsetDateTime submittedDate = archiveMetadata.getAcceptedDate();
      builder.submitted(submittedDate.getYear(), submittedDate.getMonthValue(), submittedDate.getDayOfMonth());
    }

    return builder.build();
  }

  /**************************************************************************************/

  private CSLName[] getAuthors(List<Map<String, String>> creators) {
    List<CSLName> cslNames = new ArrayList<>();
    for (Map<String, String> creator : creators) {
      CSLName cslName = new CSLNameBuilder().given(creator.get(DLCMConstants.GIVEN_NAME)).family(creator.get(DLCMConstants.FAMILY_NAME)).build();
      cslNames.add(cslName);
    }
    return cslNames.toArray(new CSLName[0]);
  }

  private CSLType getCSLTypeFromDataciteType(String resourceType) {
    if (!StringTool.isNullOrEmpty(resourceType)) {
      return switch (resourceType) {
        case DLCMConstants.DATACITE_TYPE_BOOK -> CSLType.BOOK;
        case DLCMConstants.DATACITE_TYPE_BOOK_CHAPTER -> CSLType.CHAPTER;
        case DLCMConstants.DATACITE_TYPE_CONFERENCE_PAPER -> CSLType.PAPER_CONFERENCE;
        case DLCMConstants.DATACITE_TYPE_DATASET -> CSLType.DATASET;
        case DLCMConstants.DATACITE_TYPE_JOURNAL_ARTICLE -> CSLType.ARTICLE_JOURNAL;
        case DLCMConstants.DATACITE_TYPE_PEER_REVIEW -> CSLType.REVIEW;
        case DLCMConstants.DATACITE_TYPE_REPORT -> CSLType.REPORT;
        default -> null;
      };
    }
    return null;
  }

}

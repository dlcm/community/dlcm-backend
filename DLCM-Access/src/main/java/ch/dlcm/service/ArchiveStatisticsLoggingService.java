/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - ArchiveStatisticsLoggingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.ArchiveStatisticMessage;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.service.rest.fallback.FallbackArchiveStatisticsRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class ArchiveStatisticsLoggingService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(ArchiveStatisticsLoggingService.class);

  private final FallbackArchiveStatisticsRemoteResourceService archiveStatisticsRemoteResourceService;

  public ArchiveStatisticsLoggingService(MessageService messageService,
          FallbackArchiveStatisticsRemoteResourceService archiveStatisticsRemoteResourceService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.archiveStatisticsRemoteResourceService = archiveStatisticsRemoteResourceService;
  }

  @JmsListener(destination = "${dlcm.queue.archive}")
  public void logStatistic(ArchiveStatisticMessage statistic) {
    if (log.isTraceEnabled()) {
      log.trace("Reading message {}", statistic);
    }
    try {
      // Check archive statistic exists
      if (!this.archiveStatisticsRemoteResourceService.checkArchiveStatistics(statistic.getResId())) {
        this.archiveStatisticsRemoteResourceService.createArchiveStatistics(statistic.getResId());
      }
      // Log Action
      if (statistic.isViewStatistic() || statistic.isDownloadStatistic()) {
        Result result = this.archiveStatisticsRemoteResourceService.logArchiveStatistics(statistic.getResId(),
                (statistic.isViewStatistic() ? DLCMActionName.ADD_VIEW : DLCMActionName.ADD_DOWNLOAD));
        if (result.getStatus().equals(ActionStatus.EXECUTED)) {
          log.info("Archive statistics logged: {}", result.getMessage());
        } else {
          log.warn("Archive statistics not logged: {}", result.getMessage());
        }
      } else {
        log.warn("No archive statistic {}", statistic);
      }

    } catch (final Exception e) {
      log.error("Error in message {}", statistic, e);
    }
  }
}

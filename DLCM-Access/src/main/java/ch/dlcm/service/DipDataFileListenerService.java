/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DipDataFileListenerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.DipDataFileService;
import ch.dlcm.business.DisseminationInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.DataFileMessage;
import ch.dlcm.message.DipMessage;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.DipDataFile;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class DipDataFileListenerService extends AbstractDataFileListenerService<DipDataFile> {
  private static final Logger log = LoggerFactory.getLogger(DipDataFileListenerService.class);

  private static final String NOTIFICATION_DIP_DATAFILE_ERROR = "access.notification.dip.datafile.error";

  private final String accessLocation;
  private final DisseminationInfoPackageService dipService;

  public DipDataFileListenerService(DLCMProperties dlcmProperties,
          MessageService messageService,
          DataFileStatusService dfStatusService,
          TrustedUserRemoteResourceService userRemoteService,
          TrustedNotificationRemoteResourceService notificationRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService,
          DipDataFileService dipDataFileService,
          DisseminationInfoPackageService dipService) {
    super(dlcmProperties,
            messageService,
            dfStatusService,
            userRemoteService,
            notificationRemoteService,
            organizationalUnitRemoteService,
            dipDataFileService);
    this.accessLocation = dlcmProperties.getAccessLocation();
    this.dipService = dipService;
  }

  @JmsListener(destination = "${dlcm.queue.dip-datafile}")
  @Override
  public void receiveDataFileMessage(DataFileMessage dataFileMessage) {
    super.receiveDataFileMessage(dataFileMessage);
  }

  @Override
  protected void processDataFile(DataFileMessage dataFileMessage) {
    log.info("Reading Deposit DIP Data File message {}", dataFileMessage);
    try {
      final DipDataFile dipDf = this
              .processDataFile(this.accessLocation, dataFileMessage.getResId());
      if (dipDf.getStatus() == DataFileStatus.READY) {
        boolean isBigPackage = this.dipService.getSize(dipDf.getInfoPackage().getResId()) > this.fileSizeLimit;
        SolidifyEventPublisher.getPublisher().publishEvent(new DipMessage(dipDf.getInfoPackage().getResId(), isBigPackage));
      } else if (dipDf.getStatus() == DataFileStatus.IN_ERROR) {
        this.sendDatafilePackageInErrorNotification(dipDf);
      }
    } catch (final NoSuchElementException e) {
      log.error(e.getMessage());
    }
  }

  private void sendDatafilePackageInErrorNotification(DipDataFile dataFile) {
    DisseminationInfoPackage dip = this.dipService.findOne(dataFile.getInfoPackage().getResId());
    String lastModifierId = dip.getLastUpdate().getWho();
    String orgUnitId = dip.getInfo().getOrganizationalUnitId();
    String message = this.messageService.get(NOTIFICATION_DIP_DATAFILE_ERROR, new Object[] { dataFile.getResId() });
    NotificationType notificationType = NotificationType.IN_ERROR_DIP_INFO;
    this.sendDatafilePackageInErrorNotification(lastModifierId, message, notificationType, orgUnitId, dataFile.getInfoPackage().getResId());
  }

}

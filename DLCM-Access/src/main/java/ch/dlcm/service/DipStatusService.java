/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DipStatusService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.net.URI;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DipDataFile;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.oais.RepresentationInfo;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.repository.DisseminationInfoPackageRepository;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class DipStatusService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(DipStatusService.class);
  private static final String NOTIFICATION_DIP_ERROR = "access.notification.dip.error";

  protected DisseminationInfoPackageRepository dipRepository;
  private final TrustedNotificationRemoteResourceService notificationRemoteService;
  private final TrustedUserRemoteResourceService userRemoteService;
  private final TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService;

  public DipStatusService(MessageService messageService, DisseminationInfoPackageRepository dipRepository,
          TrustedNotificationRemoteResourceService notificationRemoteService, TrustedUserRemoteResourceService userRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.dipRepository = dipRepository;
    this.notificationRemoteService = notificationRemoteService;
    this.userRemoteService = userRemoteService;
    this.organizationalUnitRemoteService = organizationalUnitRemoteService;
  }

  public DisseminationInfoPackage createDIP(String id, String orgUnitId, String name, Access access, List<ArchivalInfoPackage> aips,
          URI orderUri) {

    try {
      // Create DIP
      final DisseminationInfoPackage dip = new DisseminationInfoPackage();
      dip.setResId(id);
      final RepresentationInfo info = new RepresentationInfo();
      info.setName(this.messageService.get("access.dip.name", new Object[] { name }));
      info.setDescription(this.messageService.get("access.dip.description"));
      info.setOrganizationalUnitId(orgUnitId);
      info.setAccess(access);
      dip.setInfo(info);
      // AIP links
      for (final ArchivalInfoPackage aip : aips) {
        dip.addItem(aip);
      }
      // DIP data file
      final DipDataFile dipDataFile = new DipDataFile(dip);
      dipDataFile.setSourceData(orderUri);
      dipDataFile.setDataCategory(DataCategory.Package);
      dipDataFile.setDataType(DataCategory.InformationPackage);
      dip.getDataFiles().add(dipDataFile);

      // Save DIP
      return this.dipRepository.save(dip);
    } catch (final Exception e) {
      log.error("Error in creating DIP for order ({})", id, e);
    }
    return null;
  }

  @Transactional
  public DisseminationInfoPackage processDipStatus(String dipId) {
    // Load DIP
    final DisseminationInfoPackage dip = this.dipRepository.findById(dipId)
            .orElseThrow(() -> new NoSuchElementException("Dip " + dipId + " not found"));
    try {
      if (dip.getInfo().getStatus() == PackageStatus.IN_PREPARATION) {
        if (dip.isReady()) {
          dip.setPackageStatus(PackageStatus.COMPLETED);
        }
      }
    } catch (final Exception e) {
      log.error("Error in  processing DIP ({})", dip.getResId(), e);
      this.inError(dip, e.getMessage());
      this.sendErrorNotification(dip);
    }
    return this.dipRepository.save(dip);
  }

  private void sendErrorNotification(DisseminationInfoPackage dip) {
    try {
      // Send a notification
      String message = this.messageService.get(NOTIFICATION_DIP_ERROR, new Object[] { dip.getResId() });
      User updaterUser = this.userRemoteService.findByExternalUid(dip.getLastUpdate().getWho());
      OrganizationalUnit organizationalUnit = this.organizationalUnitRemoteService.findOne(dip.getInfo().getOrganizationalUnitId());
      this.notificationRemoteService.createNotification(updaterUser, message, NotificationType.IN_ERROR_DIP_INFO, organizationalUnit,
              dip.getResId());
    } catch (Exception e) {
      log.error("Cannot send notification for DIP {} in error: {}", dip.getResId(), e.getMessage());
    }
  }
}

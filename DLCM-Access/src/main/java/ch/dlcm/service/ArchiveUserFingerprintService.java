/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - ArchiveUserFingerprintService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import jakarta.servlet.http.HttpServletRequest;

import ch.unige.solidify.util.HashTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.access.ArchiveUserFingerprint;
import ch.dlcm.repository.ArchiveUserFingerprintRepository;

@Service
@ConditionalOnBean(AccessController.class)
public class ArchiveUserFingerprintService {
  private final HttpServletRequest httpServletRequest;
  private final int archiveUserFingerprintLifeTimeMinutes;
  private final ArchiveUserFingerprintRepository archiveUserFingerprintRepository;

  private static final long CLEANING_INTERVAL_MILLISECONDS = 600000;  // ten minutes

  public ArchiveUserFingerprintService(ArchiveUserFingerprintRepository archiveUserFingerprintRepository,
          HttpServletRequest httpServletRequest,
          DLCMProperties dlcmProperties) {
    this.archiveUserFingerprintRepository = archiveUserFingerprintRepository;
    this.httpServletRequest = httpServletRequest;
    this.archiveUserFingerprintLifeTimeMinutes = dlcmProperties.getParameters().getArchiveUserFingerprintDurationMinutes();
  }

  public boolean existsUnexpiredArchiveUserFingerprint(String archiveId) {
    final String userAgent = this.httpServletRequest.getHeader(HttpHeaders.USER_AGENT);

    final String headerXFF = this.httpServletRequest.getHeader("X-FORWARDED-FOR");
    final String ipAddress = headerXFF == null ? this.httpServletRequest.getRemoteAddr()
            : this.httpServletRequest.getRemoteAddr() + " XFF(" + headerXFF + ")";

    String fingerprint = HashTool.hash(userAgent + "-" + ipAddress);

    ArchiveUserFingerprint archiveUserFingerprint = this.archiveUserFingerprintRepository.findByArchiveIdAndFingerprint(archiveId, fingerprint);

    if (archiveUserFingerprint != null && !archiveUserFingerprint.isExpired()) {
      return true;
    }

    if (archiveUserFingerprint != null) {
      this.archiveUserFingerprintRepository.delete(archiveUserFingerprint);
    }

    archiveUserFingerprint = new ArchiveUserFingerprint(archiveId, fingerprint,
            ArchiveUserFingerprint.FingerprintType.BACKEND_FINGERPRINT);
    archiveUserFingerprint.setExpiration(this.archiveUserFingerprintLifeTimeMinutes);
    this.archiveUserFingerprintRepository.save(archiveUserFingerprint);

    return false;
  }

  @Scheduled(fixedDelay = CLEANING_INTERVAL_MILLISECONDS)
  public void removeExpiredArchiveUserFingerprint() {
    final OffsetDateTime limitTime = OffsetDateTime.now().minusMinutes(this.archiveUserFingerprintLifeTimeMinutes);
    List<ArchiveUserFingerprint> listExpiredArchiveUserFingerprint = this.archiveUserFingerprintRepository.findExpiredArchiveUserFingerprint(
            limitTime);
    this.archiveUserFingerprintRepository.deleteAll(listExpiredArchiveUserFingerprint);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DipMgmt.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.DipMessage;
import ch.dlcm.message.OrderMessage;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.repository.DisseminationInfoPackageRepository;

@Service
@ConditionalOnBean(AccessController.class)
public class DipMgmt extends MessageProcessorBySize<DipMessage> {
  private static final Logger log = LoggerFactory.getLogger(DipMgmt.class);

  protected final DipStatusService dipStatusService;
  protected final DisseminationInfoPackageRepository dipRepository;

  public DipMgmt(DipStatusService dipStatusService, DisseminationInfoPackageRepository dipRepository, DLCMProperties dlcmProperties) {
    super(dlcmProperties);
    this.dipStatusService = dipStatusService;
    this.dipRepository = dipRepository;
  }

  @JmsListener(destination = "${dlcm.queue.dip}")
  @Override
  public void receiveMessage(final DipMessage dipMessage) {
    this.sendForParallelProcessing(dipMessage);
  }

  @Override
  public void processMessage(final DipMessage dipPayload) {
    this.setSecurityContext();
    DipMgmt.log.info("Reading message {}", dipPayload);
    Optional<DisseminationInfoPackage> dipObject = this.dipRepository.findById(dipPayload.getResId());
    if (dipObject.isEmpty()) {
      DipMgmt.log.error("Cannot find DIP ({})", dipPayload.getResId());
      return;
    }
    DisseminationInfoPackage dip = dipObject.get();
    if (dip == null) {
      DipMgmt.log.error("Cannot find DIP ({})", dipPayload.getResId());
      return;
    }
    while (dip.isInProgress()) {
      dip = this.dipStatusService.processDipStatus(dip.getResId());
    }
    // Notify Order queue
    if (dip.getInfo().getStatus() == PackageStatus.COMPLETED) {
      SolidifyEventPublisher.getPublisher().publishEvent(new OrderMessage());
    }
    DipMgmt.log.info("DIP processed: resId={} status={}", dip.getResId(), dip.getInfo().getStatus());
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrderMgmt.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.dlcm.business.OrderService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.OrderMessage;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;

@Service
@ConditionalOnBean(AccessController.class)
public class OrderMgmt extends MessageProcessorBySize<OrderMessage> {
  private static final Logger log = LoggerFactory.getLogger(OrderMgmt.class);

  protected OrderService orderService;
  protected OrderStatusService orderStatusService;

  public OrderMgmt(OrderStatusService orderStatusService, OrderService orderService, DLCMProperties dlcmProperties) {
    super(dlcmProperties);
    this.orderStatusService = orderStatusService;
    this.orderService = orderService;
  }

  @JmsListener(destination = "${dlcm.queue.order}")
  @Override
  public void receiveMessage(final OrderMessage orderMessage) {
    if (orderMessage.getResId() != null && this.isBigPackageSize(this.orderService.getSize(orderMessage.getResId()))) {
      orderMessage.setBigArchive(true);
    }
    this.sendForParallelProcessing(orderMessage);
  }

  @Override
  public void processMessage(final OrderMessage orderMessage) {
    log.info("Reading message {}", orderMessage);
    if (orderMessage.getResId() != null) {
      try {
        Order order = this.orderService.findOne(orderMessage.getResId());
        while (order.getStatus() != OrderStatus.IN_ERROR && order.getStatus() != OrderStatus.READY
                && order.getStatus() != OrderStatus.DOWNLOADING
                && order.getStatus() != OrderStatus.IN_DISSEMINATION_PREPARATION) {
          order = this.orderStatusService.processOrder(order.getResId());
        }
      } catch (NoSuchElementException e) {
        log.error("Cannot find order ({})", orderMessage.getResId());
      }
    } else {
      this.orderStatusService.checkOrderProgress();
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - ArchiveSitemapService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.sitemap;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.sitemap.SolidifySitemapService;

import ch.dlcm.controller.AccessController;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.SearchService;

@Service
@ConditionalOnBean(AccessController.class)
public class ArchiveSitemapService extends SolidifySitemapService {

  private final SearchService searchService;

  public ArchiveSitemapService(
          SolidifyProperties solidifyConfig,
          SearchService searchService) {
    super(solidifyConfig, "browse/project/detail/", "");
    this.searchService = searchService;
  }

  @Override
  public String getName() {
    return ResourceName.ARCHIVE;
  }

  @Override
  public long getItemsTotal() {
    Page<ArchiveMetadata> result = this.getArchives(PageRequest.of(0, 1));
    return result.getTotalElements();

  }

  @Override
  public List<String> getItemsFrom(int from) {
    int page = from / this.getPageSize();
    Pageable pageable = PageRequest.of(page, this.getPageSize());
    Page<ArchiveMetadata> result = this.getArchives(pageable);
    return result.getContent().stream().map(p -> this.getItemPartialUrl(p.getResId())).toList();
  }

  private Page<ArchiveMetadata> getArchives(Pageable pageable) {
    return this.searchService.findAll(new ArchiveMetadata(), pageable);
  }
}

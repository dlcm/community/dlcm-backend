/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AipDownloadMgmt.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.AipDownloadMessage;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;

@Service
@ConditionalOnBean(AccessController.class)
public class AipDownloadMgmt extends MessageProcessorBySize<AipDownloadMessage> {
  private static final Logger log = LoggerFactory.getLogger(AipDownloadMgmt.class);

  protected final AipDownloadStatusService aipDownloadStatusService;
  protected final ArchivalInfoPackageService aipService;

  public AipDownloadMgmt(AipDownloadStatusService aipDownloadStatusService, ArchivalInfoPackageService aipService,
          DLCMProperties dlcmProperties) {
    super(dlcmProperties);
    this.aipDownloadStatusService = aipDownloadStatusService;
    this.aipService = aipService;
  }

  @JmsListener(destination = "${dlcm.queue.aip-download}")
  @Override
  public void receiveMessage(AipDownloadMessage aipMessage) {
    if (aipMessage.getResId() != null && this.isBigPackageSize(this.aipService.getSize(aipMessage.getResId()))) {
      aipMessage.setBigArchive(true);
    }
    this.sendForParallelProcessing(aipMessage);
  }

  @Override
  public void processMessage(AipDownloadMessage aipMessage) {
    this.setSecurityContext();
    log.info("Reading message {}", aipMessage);
    if (aipMessage.getResId() != null) {
      try {
        ArchivalInfoPackage aip = this.aipService.findOne(aipMessage.getResId());
        while (aip.getInfo().getStatus() != PackageStatus.IN_ERROR && aip.getInfo().getStatus() != PackageStatus.COMPLETED) {
          aip = this.aipDownloadStatusService.processAip(aip.getResId());
          // Downloading in-progress: wait for working AIP package is ready
          if (aip.getInfo().getStatus() == PackageStatus.DOWNLOADING) {
            break;
          }
        }
      } catch (NoSuchElementException e) {
        log.error("Cannot find AIPDownload ({})", aipMessage.getResId());
      }
    } else {
      this.aipDownloadStatusService.checkDownloadingCollection();
    }
  }
}

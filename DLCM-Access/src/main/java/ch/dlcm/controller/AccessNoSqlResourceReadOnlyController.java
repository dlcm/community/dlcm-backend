/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AccessNoSqlResourceReadOnlyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.NoSqlResourceReadOnlyController;
import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.service.NoSqlResourceService;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.config.DLCMProperties;

public abstract class AccessNoSqlResourceReadOnlyController<T extends NoSqlResource> extends NoSqlResourceReadOnlyController<T> {

  private final String accessTempLocation;

  protected AccessNoSqlResourceReadOnlyController(NoSqlResourceService<T> noSqlResourceService, DLCMProperties dlcmProperties) {
    super(noSqlResourceService);
    this.accessTempLocation = dlcmProperties.getTempLocation(dlcmProperties.getAccessLocation());
  }

  protected HttpEntity<StreamingResponseBody> getThumbnail(Path thumbnailFile) {
    try {
      final InputStream is = new BufferedInputStream(new FileInputStream(thumbnailFile.toFile()), SolidifyConstants.BUFFER_SIZE);
      return this.buildDownloadResponseEntity(is, thumbnailFile.getFileName().toString(), FileTool.getContentType(thumbnailFile).toString(),
              FileTool.getSize(thumbnailFile));
    } catch (final IOException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  protected Path getThumbnailPath(String resId) {
    return Paths.get(this.accessTempLocation, resId);
  }

  protected void downloadThumbnailLocally(ByteArrayOutputStream downloadArchiveThumbnail, Path thumbnailPath)
          throws IOException {
    try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(thumbnailPath.toFile()), SolidifyConstants.BUFFER_SIZE)) {
      downloadArchiveThumbnail.writeTo(outputStream);
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AccessIndexResourceReadOnlyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.index.IndexDataSearchRemoteController;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.service.rest.abstractservice.IndexMetadataRemoteResourceService;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;

public abstract class AccessIndexResourceReadOnlyController<T extends IndexMetadata> extends IndexDataSearchRemoteController<T> {

  private static final Logger log = LoggerFactory.getLogger(AccessIndexResourceReadOnlyController.class);

  protected final String cacheLocation;
  protected final String tempLocation;

  protected AccessIndexResourceReadOnlyController(
          IndexMetadataRemoteResourceService<T> indexResourceRemoteService,
          DLCMProperties dlcmProperties) {
    super(indexResourceRemoteService);
    this.cacheLocation = dlcmProperties.getAccessLocation();
    this.tempLocation = dlcmProperties.getTempLocation(dlcmProperties.getAccessLocation());
  }

  protected HttpEntity<StreamingResponseBody> getFile(Path path, String targetName, String contentType) {
    try {
      return this.buildDownloadResponseEntity(FileTool.getInputStream(path), targetName, contentType, FileTool.getSize(path));
    } catch (IOException e) {
      log.error("Cannot find file", e);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  protected Path getThumbnailPath(String resId) {
    return this.checkIfParentFolderExist(Paths.get(this.cacheLocation, resId, DLCMConstants.ARCHIVE_THUMBNAIL));
  }

  protected Path getDuaPath(String resId) {
    return this.checkIfParentFolderExist(Paths.get(this.cacheLocation, resId, DLCMConstants.ARCHIVE_DUA));
  }

  protected Path getReadmePath(String resId) {
    return this.checkIfParentFolderExist(Paths.get(this.cacheLocation, resId, DLCMConstants.ARCHIVE_README));
  }

  protected Path getDataFilePath(String resId, String fileId) {
    return this.checkIfParentFolderExist(Paths.get(this.cacheLocation, resId, fileId));
  }

  protected Path getTempPath(String resId) {
    return this.checkIfParentFolderExist(Paths.get(this.tempLocation, resId));
  }

  protected void downloadThumbnailLocally(ByteArrayOutputStream downloadArchiveThumbnail, Path thumbnailPath)
          throws IOException {
    try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(thumbnailPath.toFile()), SolidifyConstants.BUFFER_SIZE)) {
      downloadArchiveThumbnail.writeTo(outputStream);
    }
  }

  private Path checkIfParentFolderExist(Path path) {
    if (!FileTool.ensureFolderExists(path.getParent())) {
      throw new SolidifyCheckingException("Cannot create parent folder: " + path.toString());
    }
    return path;
  }
}

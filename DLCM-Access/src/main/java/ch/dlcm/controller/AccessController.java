/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AccessController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.business.OAIMetadataPrefixService;
import ch.unige.solidify.business.OAISetService;
import ch.unige.solidify.config.CitationProperties;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.message.AccessRefreshMessage;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.rest.abstractservice.OrganizationalUnitRemoteResourceService;

@RestController
@ConditionalOnProperty(prefix = "dlcm.module.access", name = "enable")
@RequestMapping(UrlPath.ACCESS)
public class AccessController extends ModuleController {

  private static final Logger log = LoggerFactory.getLogger(AccessController.class);

  private final SystemProperties systemProperties;
  private final DLCMProperties config;

  AccessController(DLCMProperties dlcmProperties, OAIMetadataPrefixService oaiMetadataPrefixService, OAISetService oaiSetService,
          CitationProperties citationProperties, DLCMProperties config) {
    super(ModuleName.ACCESS);
    this.config = config;
    if (dlcmProperties.getData().isInit()) {
      try {
        // Init OAI Metadata Prefix
        oaiMetadataPrefixService.initDefaultData();
        // OAI Metadata Prefix: DataCite
        oaiMetadataPrefixService.createIfNotExists(
                DLCMConstants.OAI_DATACITE,
                true,
                DLCMConstants.DATACITE_NAME,
                DLCMConstants.DATACITE_NAME + " 4.3",
                DLCMMetadataVersion.getDefaultVersion().getRepresentationInfoSchema(),
                null,
                DLCMConstants.DATACITE_SCHEMA_4_3,
                DLCMConstants.DATACITE_NAMESPACE_4,
                null);

        // OAI Metadata Prefix: schema.org
        oaiMetadataPrefixService.createIfNotExists(
                DLCMConstants.OAI_SCHEMA_ORG,
                false,
                DLCMConstants.OAI_SCHEMA_ORG_NAME,
                DLCMConstants.OAI_SCHEMA_ORG_NAME,
                null,
                "datacite2schema.org.xsl",
                DLCMConstants.OAI_SCHEMA_ORG_SCHEMA,
                DLCMConstants.OAI_SCHEMA_ORG_SCHEMA,
                null);
        // Update OAI DC
        oaiMetadataPrefixService.updateOaiDc("datacite2oai_dc.xsl");

      } catch (IOException e) {
        throw new SolidifyRuntimeException(e.getMessage(), e);
      }
      // OAI Set for all OrgUnits
      oaiSetService.createIfNotExists(DLCMConstants.OAI_ORG_UNIT, "Organizational Units", "Set for all organizational units", "*");
    }
    this.systemProperties = new SystemProperties(citationProperties);
    this.cleanTmpDirectory();
  }

  @TrustedUserPermissions
  @PostMapping("/" + DLCMActionName.REFRESH + "/{aipId}")
  public ResponseEntity<Void> refresh(@PathVariable String aipId) {
    SolidifyEventPublisher.getPublisher().publishEvent(new AccessRefreshMessage(aipId));
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping(DLCMActionName.SYSTEM_PROPERTIES)
  public HttpEntity<SystemProperties> systemProperties() {
    this.systemProperties.removeLinks();
    this.systemProperties.add(linkTo(this.getClass()).withSelfRel());
    return new ResponseEntity<>(this.systemProperties, HttpStatus.OK);
  }

  private void cleanTmpDirectory() {
    Path tmpDipLocation = Paths.get(this.config.getTempLocation(this.config.getAccessLocation()));
    Path tmpOrderLocation = Paths.get(this.config.getTempLocation(this.config.getOrderLocation()));

    try {
      FileTool.deleteFolderContent(tmpDipLocation);
      FileTool.deleteFolderContent(tmpOrderLocation);
    } catch (IOException e) {
      log.error("Unable to clean temporary directory", e);
    }
  }

  private static class SystemProperties extends RepresentationModel<SystemProperties> {
    Map<String, String[]> citations = new HashMap<>();
    Map<String, String[]> bibliographies = new HashMap<>();

    public SystemProperties(CitationProperties citationProperties) {
      this.citations.put("languages", citationProperties.getCitations().getLanguages());
      this.citations.put("outputFormats", citationProperties.getCitations().getOutputFormats());
      this.citations.put("styles", citationProperties.getCitations().getStyles());

      this.bibliographies.put("languages", citationProperties.getBibliographies().getLanguages());
      this.bibliographies.put("outputFormats", citationProperties.getBibliographies().getOutputFormats());
      this.bibliographies.put("styles", citationProperties.getBibliographies().getStyles());
    }

    public Map<String, String[]> getCitations() {
      return this.citations;
    }

    public Map<String, String[]> getBibliographies() {
      return this.bibliographies;
    }
  }
}

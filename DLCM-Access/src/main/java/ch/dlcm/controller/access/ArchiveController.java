/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - ArchiveController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.security.EveryonePermissions;

import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.IdentifierType;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.SearchService;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_ARCHIVE)
public class ArchiveController extends SolidifyController implements ControllerWithHateoasHome {
  private static final Logger log = LoggerFactory.getLogger(ArchiveController.class);

  private SearchService searchService;
  private String archiveHomePage;

  public ArchiveController(DLCMRepositoryDescription repository, SearchService searchService) {
    super();
    this.archiveHomePage = repository.getArchiveHomePage();
    this.searchService = searchService;
  }

  @GetMapping("/**")
  public ModelAndView redirectArchive(HttpServletRequest request) {
    try {
      final String id = this.extractEndingPath(request);
      final ArchiveMetadata archive = this.getArchiveMetadata(id);
      return new ModelAndView("redirect:" + this.archiveHomePage + archive.getResId());
    } catch (final SolidifyResourceNotFoundException e) {
      log.error("Error", e);
      ModelAndView mav = new ModelAndView();
      mav.addObject("exception", e);
      mav.addObject("url", request.getRequestURL());
      mav.setViewName("error");
      return mav;
    }
  }

  @GetMapping("/" + ResourceName.PUBLIC_METADATA + "/**")
  public HttpEntity<ArchiveMetadata> get(HttpServletRequest request) {
    final String id = this.extractEndingPath(request);
    try {
      ArchiveMetadata archive = this.getArchiveMetadata(id);
      archive.removeLinks();
      archive.addLinksForAccess(linkTo(MetadataController.class));
      return new ResponseEntity<>(archive, HttpStatus.OK);
    } catch (final SolidifyResourceNotFoundException e) {
      log.error("Error", e);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (final SolidifyRuntimeException e) {
      log.error("Error", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private ArchiveMetadata getArchiveMetadata(String id) {
    if (this.isIdentifierType(id, IdentifierType.DOI)) {
      return this.getByIdentifier(IdentifierType.DOI, id);
    } else if (this.isIdentifierType(id, IdentifierType.ARK)) {
      return this.getByIdentifier(IdentifierType.ARK, id);
    }
    return this.searchService.findOne(id);
  }

  private boolean isIdentifierType(String id, IdentifierType idType) {
    return id.startsWith(idType.getDescription().toLowerCase());
  }

  private ArchiveMetadata getByIdentifier(IdentifierType idType, String id) {
    final String realId = this.extractIdentifier(idType, id);
    return this.searchService.getByIdentifier(idType, realId);
  }

  private String extractIdentifier(IdentifierType idType, String id) {
    if (IdentifierType.DOI.equals(idType)) {
      return id.substring(IdentifierType.DOI.getDescription().length() + 1);
    }
    return id;
  }

}

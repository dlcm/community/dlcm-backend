/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DipAipController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;

import ch.dlcm.controller.AccessController;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_DIP + SolidifyConstants.URL_PARENT_ID + ResourceName.AIP)
public class DipAipController extends AssociationController<DisseminationInfoPackage, ArchivalInfoPackage> {

  @GetMapping("/" + ActionName.STATUS)
  public HttpEntity<PackageStatus[]> listStatus(@PathVariable String parentid) {
    return new ResponseEntity<>(PackageStatus.values(), HttpStatus.OK);
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowed(#parentid, 'UPDATE')")
  public HttpEntity<List<ArchivalInfoPackage>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowed(#parentid, 'GET')")
  public HttpEntity<ArchivalInfoPackage> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowed(#parentid, 'GET')")
  public HttpEntity<RestCollection<ArchivalInfoPackage>> list(@PathVariable String parentid, @ModelAttribute ArchivalInfoPackage filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowed(#parentid, 'UPDATE')")
  public HttpEntity<ArchivalInfoPackage> update(@PathVariable String parentid, @PathVariable String id, @RequestBody ArchivalInfoPackage v2) {
    return super.update(parentid, id, v2);
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowed(#parentid, 'UPDATE')")
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowed(#parentid, 'UPDATE')")
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @Override
  protected void addLinks(String parentid, ArchivalInfoPackage aip) {
    aip.removeLinks();
    aip.addLinksForAccess(linkTo(methodOn(this.getClass()).list(parentid, null, null)), false, false);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(String parentid, W w) {
    w.add(linkTo(methodOn(this.getClass()).listStatus(parentid)).withRel(ActionName.VALUES));
  }

  @Override
  protected String getParentFieldName() {
    return "dipPackages";
  }

  @Override
  public ArchivalInfoPackage getEmptyChildResourceObject() {
    return new ArchivalInfoPackage();
  }

  @Override
  protected boolean addChildOnParent(DisseminationInfoPackage dip, ArchivalInfoPackage aip) {
    return dip.addItem(aip);
  }

  @Override
  protected boolean removeChildFromParent(DisseminationInfoPackage dip, ArchivalInfoPackage aip) {
    return dip.removeItem(aip);
  }

}

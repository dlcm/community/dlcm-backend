/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrderAipController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.unit.DataSize;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.EveryonePermissions;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.AipDownloadMessage;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.Role;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.SearchMgmt;
import ch.dlcm.service.rest.propagate.PropagatePersonRemoteResourceService;

@RestController
@EveryonePermissions
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_ORDER + SolidifyConstants.URL_PARENT_ID + ResourceName.AIP)
public class OrderAipController extends AssociationController<Order, ArchivalInfoPackage> {

  private static final Logger log = LoggerFactory.getLogger(OrderAipController.class);

  private final ArchivalInfoPackageService aipService;
  private final SearchMgmt searchMgmt;
  private final PropagatePersonRemoteResourceService personRemoteResourceService;
  private final DataSize fileSizeLimit;

  public OrderAipController(ArchivalInfoPackageService aipService,
          SearchMgmt searchMgmt,
          PropagatePersonRemoteResourceService personRemoteResourceService,
          DLCMProperties dlcmProperties) {
    this.aipService = aipService;
    this.searchMgmt = searchMgmt;
    this.personRemoteResourceService = personRemoteResourceService;
    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit();
  }

  /**
   * For each AIP to add to Order: - check that the authenticated User can read it - make sure that
   * the AIP copy exists on access side
   */
  @Override
  public HttpEntity<List<ArchivalInfoPackage>> create(@PathVariable String parentid, @RequestBody String[] aipIds) {

    final List<String> filteredAips = new ArrayList<>();

    for (final String aipId : aipIds) {

      final ArchivalInfoPackage aip = this.searchMgmt.getAIP(aipId);

      if (aip.getInfo().getStatus() == PackageStatus.COMPLETED) {

        if (this.isReadableByAuthenticatedUser(aip)) {

          filteredAips.add(aipId);
          this.ensureAIPcopyExistsOnAccess(aip);

        } else {
          log.warn("AIP '{}' is not readable by the authenticated user", aipId);
        }

      } else if (aip.getInfo().getStatus() != PackageStatus.COMPLETED) {
        log.warn("AIP '{}' is not COMPLETED and thus can not be copied on access", aipId);
      }
    }

    return super.create(parentid, filteredAips.toArray(new String[0]));
  }

  @Override
  public HttpEntity<ArchivalInfoPackage> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  public HttpEntity<RestCollection<ArchivalInfoPackage>> list(@PathVariable String parentid, @ModelAttribute ArchivalInfoPackage filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  public HttpEntity<ArchivalInfoPackage> update(@PathVariable String parentid, @PathVariable String id, @RequestBody ArchivalInfoPackage v2) {
    return super.update(parentid, id, v2);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @Override
  protected void addLinks(String parentid, ArchivalInfoPackage aip) {
    aip.removeLinks();
    final WebMvcLinkBuilder linkBuilder = linkTo(methodOn(this.getClass()).list(parentid, null, null));
    aip.add(linkBuilder.slash(aip.getResId()).withSelfRel());
    aip.add((Tool.referenceLink(linkBuilder.slash(aip.getResId()).toUriComponentsBuilder(), ModuleName.ACCESS)).withSelfRel());
    aip.add(linkBuilder.withRel(ActionName.LIST));
    aip.add((Tool.parentLink(linkBuilder.toUriComponentsBuilder())).withRel(ActionName.PARENT));
  }

  private void ensureAIPcopyExistsOnAccess(ArchivalInfoPackage aip) {

    if (!this.aipService.existsById(aip.getResId())) {

      boolean isBigPackage = aip.getArchiveSize() != null && aip.getArchiveSize() > this.fileSizeLimit.toBytes();

      /*
       * Save AIP on access
       */
      aip.reset();
      this.aipService.save(aip);

      /*
       * init transfer of AIP file from storage to access
       */

      SolidifyEventPublisher.getPublisher().publishEvent(new AipDownloadMessage(aip.getResId(), isBigPackage));
    }
  }

  private boolean isReadableByAuthenticatedUser(ArchivalInfoPackage aip) {

    boolean authorized = false;

    if (aip.getInfo().isAccessCurrentlyPublic()) {
      /*
       * AIP is readable if access is public
       */
      authorized = true;
    } else {

      /*
       * AIP is readable if user has any role on the AIP's organizational unit
       */
      final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      final String personId = this.personRemoteResourceService.getLinkedPersonId(authentication);
      final Optional<Role> role = this.personRemoteResourceService.findOrganizationalUnitRole(personId, aip.getInfo().getOrganizationalUnitId());

      authorized = role.isPresent();
    }

    return authorized;
  }

  @Override
  protected String getParentFieldName() {
    return "orders";
  }

  @Override
  public ArchivalInfoPackage getEmptyChildResourceObject() {
    return new ArchivalInfoPackage();
  }

  @Override
  protected boolean addChildOnParent(Order parentResource, ArchivalInfoPackage childResource) {
    return parentResource.addItem(childResource);
  }

  @Override
  protected boolean removeChildFromParent(Order parentResource, ArchivalInfoPackage childResource) {
    return parentResource.removeItem(childResource);
  }
}

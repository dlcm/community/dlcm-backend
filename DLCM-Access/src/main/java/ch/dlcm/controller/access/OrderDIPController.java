/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrderDIPController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.EveryonePermissions;

import ch.dlcm.controller.AccessController;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_ORDER + SolidifyConstants.URL_PARENT_ID + ResourceName.DIP)
public class OrderDIPController extends AssociationController<Order, DisseminationInfoPackage> {

  @Override
  public HttpEntity<List<DisseminationInfoPackage>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @Override
  public HttpEntity<DisseminationInfoPackage> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  public HttpEntity<RestCollection<DisseminationInfoPackage>> list(@PathVariable String parentid,
          @ModelAttribute DisseminationInfoPackage filterItem, Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  public HttpEntity<DisseminationInfoPackage> update(@PathVariable String parentid, @PathVariable String id,
          @RequestBody DisseminationInfoPackage v2) {
    return super.update(parentid, id, v2);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @Override
  protected String getParentFieldName() {
    return "orders";
  }

  @Override
  public DisseminationInfoPackage getEmptyChildResourceObject() {
    return new DisseminationInfoPackage();
  }

  @Override
  protected boolean addChildOnParent(Order parentResource, DisseminationInfoPackage childResource) {
    return parentResource.addItem(childResource);
  }

  @Override
  protected boolean removeChildFromParent(Order parentResource, DisseminationInfoPackage childResource) {
    return parentResource.removeItem(childResource);
  }

}

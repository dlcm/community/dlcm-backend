/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - MetadataController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.dto.CitationDto;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.FacetResult;
import ch.unige.solidify.rest.FacetResultValue;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.DownloadTokenService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SearchConditionTool;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.OrderService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.Package;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.access.OrderSubsetItem;
import ch.dlcm.model.dto.ArchiveStatisticsDto;
import ch.dlcm.model.dto.DisseminationPolicyDto;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.index.ArchivePublicData.PublicDataFileType;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.settings.RatingType;
import ch.dlcm.repository.DisseminationInfoPackageRepository;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.ArchiveUserFingerprintService;
import ch.dlcm.service.citation.DLCMCitationGeneratorService;
import ch.dlcm.service.dissemination.DisseminationPolicyProvider;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicDataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchiveStatisticsRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchiveRatingRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_PUBLIC_METADATA)
public class MetadataController extends SearchController {
  private static final Logger log = LoggerFactory.getLogger(MetadataController.class);

  private final int waitMaxTries = 60;
  private final int waitMilliseconds = 1000;

  private final FallbackOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService;
  private final FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteResourceService;
  private final FallbackArchivePublicDataRemoteResourceService archivePublicDataRemoteResourceService;

  private final FallbackArchiveStatisticsRemoteResourceService archiveStatisticsRemoteResourceService;
  private final TrustedArchiveRatingRemoteResourceService archiveRatingRemoteResourceService;

  public MetadataController(
          TrustedArchivePublicMetadataRemoteResourceService searchService,
          DLCMProperties dlcmProperties,
          DisseminationInfoPackageRepository dipRepository,
          FallbackOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService,
          OrderService orderService,
          FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteResourceService,
          FallbackArchivePublicDataRemoteResourceService archivePublicDataRemoteResourceService,
          FallbackArchiveStatisticsRemoteResourceService archiveStatisticsRemoteResourceService,
          TrustedArchiveRatingRemoteResourceService archiveRatingRemoteResource,
          ArchiveUserFingerprintService archiveUserFingerprintService,
          DLCMCitationGeneratorService dlcmCitationGeneratorService,
          DownloadTokenService downloadTokenService,
          TrustedUserRemoteResourceService trustedUserRemoteResourceService,
          DisseminationPolicyProvider disseminationPolicyProvider) {
    super(searchService, dlcmProperties, dipRepository, orderService, archiveUserFingerprintService, dlcmCitationGeneratorService,
            downloadTokenService,
            trustedUserRemoteResourceService, disseminationPolicyProvider);
    this.organizationalUnitRemoteResourceService = organizationalUnitRemoteResourceService;
    this.archivePrivateMetadataRemoteResourceService = archivePrivateMetadataRemoteResourceService;
    this.archivePublicDataRemoteResourceService = archivePublicDataRemoteResourceService;
    this.archiveStatisticsRemoteResourceService = archiveStatisticsRemoteResourceService;
    this.archiveRatingRemoteResourceService = archiveRatingRemoteResource;
  }

  @Override
  public HttpEntity<ArchiveMetadata> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<List<CitationDto>> bibliographies(@PathVariable String id) {
    return super.bibliographies(id);
  }

  @Override
  public HttpEntity<List<CitationDto>> citations(@PathVariable String id) {
    return super.citations(id);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> list(@ModelAttribute ArchiveMetadata search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> search(
          @RequestParam(required = false) String query,
          Pageable pageable) {
    return super.search(query, pageable);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> searchPost(@RequestBody MultiValueMap<String, String> params) {
    return super.searchPost(params);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> searchPost(
          @RequestBody(required = false) List<SearchCondition> searchConditions,
          Pageable pageable) {
    return super.searchPost(searchConditions, pageable);
  }

  @Override
  public HttpEntity<OrderStatus> getDownloadStatus(@PathVariable String orderId) {
    return super.getDownloadStatus(orderId);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.THUMBNAIL)
  public HttpEntity<StreamingResponseBody> downloadThumbnail(@PathVariable String id) {
    // Check if archive exist
    final ArchiveMetadata archive = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (archive == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    try {
      // Check if thumbnail exists
      Path thumbnailPath;
      String thumbnailContentType;
      if (archive.isPublicDataPresent(PublicDataFileType.ARCHIVE_THUMBNAIL).equals(Boolean.TRUE)) {
        thumbnailPath = this.getThumbnailPath(id);
        thumbnailContentType = archive.getPublicDataContentType(PublicDataFileType.ARCHIVE_THUMBNAIL);
        // Check if thumbnail already download
        this.archivePublicDataRemoteResourceService.downloadArchivePublicData(id, PublicDataFileType.ARCHIVE_THUMBNAIL, thumbnailPath);
      } else {
        // use orgUnit thumbnail
        String orgUnitId = archive.getMetadata().get(DLCMConstants.AIP_ORGA_UNIT).toString();
        thumbnailPath = this.getThumbnailPath(orgUnitId);
        // Check if thumbnail already download
        if (!FileTool.checkFile(thumbnailPath)) {
          this.organizationalUnitRemoteResourceService.downloadOrgUnitLogo(orgUnitId, thumbnailPath);
        }
        thumbnailContentType = FileTool.getContentType(thumbnailPath).toString();
      }
      return this.getFile(
              thumbnailPath,
              id + "." + DLCMConstants.ARCHIVE_THUMBNAIL,
              thumbnailContentType);
    } catch (HttpClientErrorException.NotFound e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DUA)
  public HttpEntity<StreamingResponseBody> downloadDua(@PathVariable String id) {
    // Check if archive exist
    final ArchiveMetadata archive = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (archive != null) {
      try {
        // Check if DUA exists
        if (archive.isPublicDataPresent(PublicDataFileType.ARCHIVE_DUA).equals(Boolean.TRUE)
                && !archive.getMetadata().get(DLCMConstants.AIP_DATA_USE_POLICY).equals(DataUsePolicy.EXTERNAL_DUA)) {
          Path duaPath = this.getDuaPath(id);
          // Download DUA if not already done
          if (!FileTool.checkFile(duaPath)) {
            this.archivePublicDataRemoteResourceService.downloadArchivePublicData(id, PublicDataFileType.ARCHIVE_DUA, duaPath);
          }
          return this.getFile(
                  duaPath,
                  id + "." + DLCMConstants.ARCHIVE_DUA,
                  archive.getPublicDataContentType(PublicDataFileType.ARCHIVE_DUA));
        }
      } catch (HttpClientErrorException.NotFound e) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.README)
  public HttpEntity<StreamingResponseBody> downloadReadme(@PathVariable String id) {
    // Check if archive exist
    final ArchiveMetadata archive = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (archive != null) {
      try {
        // Check if README exists
        Path readmePath;
        if (archive.isPublicDataPresent(PublicDataFileType.ARCHIVE_README).equals(Boolean.TRUE)) {
          readmePath = this.getReadmePath(id);
          // Check if README already download
          if (!FileTool.checkFile(readmePath)) {
            this.archivePublicDataRemoteResourceService.downloadArchivePublicData(id, PublicDataFileType.ARCHIVE_README, readmePath);
          }
          return this.getFile(
                  readmePath,
                  id + "." + DLCMConstants.ARCHIVE_README,
                  archive.getPublicDataContentType(PublicDataFileType.ARCHIVE_README));
        }
      } catch (HttpClientErrorException.NotFound e) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.STATISTICS)
  public HttpEntity<ArchiveStatisticsDto> getStatistics(@PathVariable String id) {
    // Check if archive exist
    final ArchiveMetadata archive = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (archive == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    try {
      return new ResponseEntity<>(this.archiveStatisticsRemoteResourceService.getArchiveStatistics(id), HttpStatus.OK);
    } catch (SolidifyResourceNotFoundException e) {
      // No Statistics yet
      RestCollection<RatingType> ratingTypeList = this.archiveRatingRemoteResourceService.getAllRatingType();

      ArchiveStatisticsDto stat = new ArchiveStatisticsDto();
      stat.init(ratingTypeList.getData());
      return new ResponseEntity<>(stat, HttpStatus.OK);
    } catch (SolidifyRuntimeException e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/" + DLCMActionName.SEARCH_DOI)
  public HttpEntity<ArchiveMetadata> searchDOI(@RequestParam("doi") String doiValue) {
    final TrustedArchivePublicMetadataRemoteResourceService searchService = (TrustedArchivePublicMetadataRemoteResourceService) this
            .getIndexResourceRemoteService();
    try {
      return new ResponseEntity<>(searchService.getMetadataByDoi(doiValue), HttpStatus.OK);
    } catch (final SolidifyResourceNotFoundException e) {
      log.error("DOI Error", e);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (final SolidifyRuntimeException e) {
      log.error("DOI Error", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.PACKAGES)
  @PreAuthorize("@metadataPermissionService.isAllowed(#id, 'DOWNLOAD_ARCHIVE') "
          + "|| @downloadWithAclPermissionService.isAllowed(#id)")
  public HttpEntity<ArchiveMetadata> getPackages(@PathVariable String id) {
    // Check if archive exist
    final ArchiveMetadata archive = this.archivePrivateMetadataRemoteResourceService.getIndexMetadata(id);
    if (archive == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(archive, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.DATAFILE)
  @PreAuthorize("@metadataPermissionService.isAllowed(#id, 'LIST_FILES') "
          + "|| @downloadWithAclPermissionService.isAllowed(#id)")
  public HttpEntity<RestCollection<ArchiveMetadata>> getDataFiles(@PathVariable String id, @RequestParam(required = false) String path,
          @RequestParam(defaultValue = "*") String file, @RequestParam(defaultValue = "0") String minSize,
          @RequestParam(required = false) String maxSize, Pageable pageable) {
    // Check if archive exist
    final ArchiveMetadata archive = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (archive == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // Condition list
    List<SearchCondition> conditionList = new ArrayList<>();
    // Filter on archive ID AND path AND file name
    SearchConditionTool.addQueryCondition(conditionList, DLCMConstants.ARCHIVE_ID_FIELD + ":" + id + " AND file.name:\"" + file + "\"");
    // Condition on type
    SearchConditionTool.addTermCondition(conditionList, "type.keyword", "dataFile");
    // Condition on folder
    if (!StringTool.isNullOrEmpty(path)) {
      SearchConditionTool.addTermCondition(conditionList, "file.path.keyword", path);
    }
    // Condition on file size
    SearchConditionTool.addRangeCondition(conditionList, "file.size", minSize, maxSize);
    final RestCollection<ArchiveMetadata> itemList = this.archivePrivateMetadataRemoteResourceService.search(
            conditionList,
            pageable);
    return new ResponseEntity<>(itemList, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.AIP)
  @PreAuthorize("@metadataPermissionService.isAllowed(#id, 'DOWNLOAD_ARCHIVE') "
          + "|| @downloadWithAclPermissionService.isAllowed(#id)")
  public HttpEntity<RestCollection<ArchiveMetadata>> getAip(@PathVariable String id, @RequestParam(defaultValue = "0") String minSize,
          @RequestParam(required = false) String maxSize, Pageable pageable) {
    // Check if archive exist
    final ArchiveMetadata archive = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (archive == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // Condition list
    List<SearchCondition> conditionList = new ArrayList<>();
    // Filter on archive ID
    SearchConditionTool.addQueryCondition(conditionList, DLCMConstants.ARCHIVE_ID_FIELD + ":" + id);
    // Condition on type
    SearchConditionTool.addTermCondition(conditionList, "type.keyword", "aip");
    // Condition on file size
    SearchConditionTool.addRangeCondition(conditionList, "aip.size", minSize, maxSize);

    final RestCollection<ArchiveMetadata> itemList = this.archivePrivateMetadataRemoteResourceService.search(
            conditionList,
            pageable);
    return new ResponseEntity<>(itemList, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.LIST_FOLDERS)
  @PreAuthorize("@metadataPermissionService.isAllowed(#id, 'LIST_FILES') "
          + "|| @downloadWithAclPermissionService.isAllowed(#id)")
  public HttpEntity<List<String>> listFolders(@PathVariable String id) {
    // Check if archive exist
    final ArchiveMetadata archive = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (archive == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // Condition list
    List<SearchCondition> conditionList = new ArrayList<>();
    // Filter on archive ID
    SearchConditionTool.addQueryCondition(conditionList, DLCMConstants.ARCHIVE_ID_FIELD + ":" + id);
    final RestCollection<ArchiveMetadata> itemList = this.archivePrivateMetadataRemoteResourceService.search(
            conditionList,
            PageRequest.of(0, 1));
    // Extract folders from the facet PATH_FACET
    final List<String> folderList = new ArrayList<>();
    for (FacetResult facet : itemList.getFacets()) {
      if (facet.getName().equals(DLCMConstants.PATH_FACET)) {
        for (FacetResultValue value : facet.getValues()) {
          folderList.add(value.getValue());
        }
        break;
      }
    }

    return new ResponseEntity<>(folderList, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_PARENT_ID + ResourceName.DATAFILE + SolidifyConstants.URL_ID)
  @PreAuthorize("@metadataPermissionService.isAllowed(#parentid, 'LIST_FILES') "
          + "|| @downloadWithAclPermissionService.isAllowed(#parentid)")
  public HttpEntity<ArchiveMetadata> getDataFile(@PathVariable String parentid, @PathVariable String id) {
    // Check if archive exist
    final ArchiveMetadata archive = this.getIndexResourceRemoteService().getIndexMetadata(parentid);
    if (archive == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // Check if archive exist
    final ArchiveMetadata datafile = this.archivePrivateMetadataRemoteResourceService.getIndexMetadata(id);
    if (datafile == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(datafile, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_PARENT_ID + ResourceName.DATAFILE + SolidifyConstants.URL_ID + "/" + DLCMActionName.READY_FOR_DOWNLOAD)
  @PreAuthorize("@metadataPermissionService.isAllowed(#parentid, 'DOWNLOAD_ARCHIVE') "
          + "|| @downloadWithAclPermissionService.isAllowed(#parentid)")
  public ResponseEntity<Void> checkDataFileIfIsReadyDownload(@PathVariable String parentid, @PathVariable String id) {
    // Check if data file is already present
    final Path dataFilePath = this.getDataFilePath(parentid, id);
    if (!FileTool.checkFile(dataFilePath)) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_PARENT_ID + ResourceName.DATAFILE + SolidifyConstants.URL_ID + "/" + ActionName.DOWNLOAD)
  @PreAuthorize("@metadataPermissionService.isAllowed(#parentid, 'DOWNLOAD_ARCHIVE') "
          + "|| @downloadWithAclPermissionService.isAllowed(#parentid)"
          + "|| @downloadTokenPermissionService.isAllowed(#id, T(ch.dlcm.model.security.DownloadTokenType).DIP_DATAFILE_ONLY)")
  public HttpEntity<StreamingResponseBody> downloadDataFile(@PathVariable String parentid, @PathVariable String id) throws IOException {
    // Check if archive exist
    final ArchiveMetadata archive = this.getIndexResourceRemoteService().getIndexMetadata(parentid);
    if (archive == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // Check if archive exist
    final ArchiveMetadata dataFile = this.archivePrivateMetadataRemoteResourceService.getIndexMetadata(id);
    if (dataFile == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // Check if data file is already present
    final Path dataFilePath = this.getDataFilePath(parentid, id);
    if (!FileTool.checkFile(dataFilePath)) {
      final Path tempAipPath = this.getTempPath(parentid);
      final String fileFullName = (String) dataFile.getMetadata("file", "fullName");
      // Find order of the subset with one file
      final DisseminationPolicyDto disseminationPolicyDto = new DisseminationPolicyDto();
      disseminationPolicyDto.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
      disseminationPolicyDto.setSubsetItemList(new ArrayList<>());
      final OrderSubsetItem dataFileToDownload = new OrderSubsetItem();
      dataFileToDownload.setResId(dataFile.getResId());
      dataFileToDownload.setItemPath(fileFullName);
      disseminationPolicyDto.getSubsetItemList().add(dataFileToDownload);
      final String orderId = this.getOrderIdentifier(parentid, disseminationPolicyDto);
      // Get DIP
      DisseminationInfoPackage disseminationInfoPackage = this.checkDipReady(orderId);
      if (disseminationInfoPackage == null) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      final URI fileUri = disseminationInfoPackage.getDownloadUri();
      // Extract file
      final ZipTool zipTool = new ZipTool(fileUri);
      if (!zipTool.unzipFiles(tempAipPath, Package.getCommonFoldersAntPath(fileFullName), false)) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
      // Copy file
      final Path extractedFile = this.getExtractedFileName(tempAipPath, dataFile);
      if (!FileTool.moveFile(extractedFile, dataFilePath)) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
      }
    }
    // Donwload data file
    return this.getFile(dataFilePath, (String) dataFile.getMetadata("file", "name"), (String) dataFile.getMetadata("file", "mimeType"));

  }

  private DisseminationInfoPackage checkDipReady(String orderId) {
    int tries = 0;
    while (tries < this.waitMaxTries) {
      final List<DisseminationInfoPackage> dipList = this.dipRepository.findByOrderId(orderId);
      if (!dipList.isEmpty()) {
        DisseminationInfoPackage dip = dipList.get(0);
        if (dip.isReady()) {
          return dip;
        } else {
          log.info("DIP not ready for the moment for order '{}'", orderId);
        }
      } else {
        log.info("No DIP found at the moment for order '{}'", orderId);
      }
      SolidifyTime.waitInMilliSeconds(this.waitMilliseconds);
      tries++;
    }
    log.info("No DIP ready found for order '{}'", orderId);
    return null;
  }

  private Path getExtractedFileName(Path tempAipPath, ArchiveMetadata dataFile) {
    final List<Path> extractedFiles = FileTool.findFiles(tempAipPath);
    if (extractedFiles.size() == 1) {
      return extractedFiles.get(0);
    }
    final String fileCategory = (String) dataFile.getMetadata("dlcm", "category");
    final String folder = switch (fileCategory) {
      case "PrimaryData" -> Package.RESEARCH.getName();
      case "SecondaryData" -> Package.DOC.getName();
      case "AdministrativeData" -> Package.ADMINISTRATION.getName();
      case "Software" -> Package.SOFTWARE.getName();
      default -> throw new SolidifyCheckingException("Unsupported value: " + fileCategory);
    };
    final List<Path> filterdList = extractedFiles
            .stream()
            .filter(p -> p.startsWith(folder))
            .toList();
    if (filterdList.size() != 1) {
      throw new SolidifyProcessingException("Cannot find file");
    }
    return filterdList.get(0);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    super.addOthersLinks(w);
    w.add(linkTo(methodOn(this.getClass()).searchDOI("doiValue")).withRel(DLCMActionName.SEARCH_DOI));
  }

}

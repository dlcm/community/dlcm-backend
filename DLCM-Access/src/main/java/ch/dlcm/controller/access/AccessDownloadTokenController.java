/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AccessDownloadTokenController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.model.security.DownloadToken;
import ch.unige.solidify.repository.DownloadTokenRepository;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.RootPermissions;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.controller.DLCMDownloadTokenController;
import ch.dlcm.model.security.DownloadTokenType;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@RestController
@ConditionalOnBean(AccessController.class)
public class AccessDownloadTokenController extends DLCMDownloadTokenController {

  private final String accessModulePath;

  public AccessDownloadTokenController(
          DownloadTokenRepository downloadTokenRepository,
          SolidifyProperties solidifyConfig,
          DLCMProperties dlcmProperties) throws URISyntaxException {
    super(solidifyConfig, downloadTokenRepository);
    this.accessModulePath = new URI(dlcmProperties.getModule().getAccess().getPublicUrl()).getPath();
  }

  @GetMapping(UrlPath.ACCESS_PUBLIC_METADATA + "/{dipName}/" + ActionName.DOWNLOAD_TOKEN)
  @PreAuthorize("@metadataPermissionService.isAllowed(#dipName, 'DOWNLOAD_ARCHIVE') || @downloadWithAclPermissionService.isAllowed(#dipName)")
  public ResponseEntity<DownloadToken> getTokenForArchive(@PathVariable String dipName) {
    final String cookiePath = this.accessModulePath
            + "/" + ResourceName.PUBLIC_METADATA
            + "/" + dipName
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(dipName, DownloadTokenType.ARCHIVE, cookiePath);
  }

  @GetMapping(UrlPath.ACCESS_AIP + SolidifyConstants.URL_PARENT_ID + ResourceName.DATAFILE
          + SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD_TOKEN)
  @RootPermissions
  public ResponseEntity<DownloadToken> getTokenForAipDownloadedDataFile(@PathVariable String parentid, @PathVariable String id) {
    final String cookiePath = this.accessModulePath
            + "/" + ResourceName.AIP
            + "/" + parentid
            + "/" + ResourceName.DATAFILE
            + "/" + id
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(id, DownloadTokenType.AIP_DOWNLOAD_DATAFILE, cookiePath);
  }

  @GetMapping(UrlPath.ACCESS_DIP + SolidifyConstants.URL_PARENT_ID + ResourceName.DATAFILE
          + SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD_TOKEN)
  @RootPermissions
  public ResponseEntity<DownloadToken> getTokenForDipDataFile(@PathVariable String parentid, @PathVariable String id) {
    final String cookiePath = this.accessModulePath
            + "/" + ResourceName.DIP
            + "/" + parentid
            + "/" + ResourceName.DATAFILE
            + "/" + id
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(id, DownloadTokenType.DIP_DATAFILE, cookiePath);
  }

  @GetMapping(UrlPath.ACCESS_PUBLIC_METADATA + SolidifyConstants.URL_PARENT_ID + ResourceName.DATAFILE + SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD_TOKEN)
  @PreAuthorize("@metadataPermissionService.isAllowed(#parentid, 'DOWNLOAD_ARCHIVE') "
          + "|| @downloadWithAclPermissionService.isAllowed(#parentid)")
  public ResponseEntity<DownloadToken> getTokenForDataFile(@PathVariable String parentid, @PathVariable String id) {
    final String cookiePath = this.accessModulePath
            + "/" + ResourceName.PUBLIC_METADATA
            + "/" + parentid
            + "/" + ResourceName.DATAFILE
            + "/" + id
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(id, DownloadTokenType.DIP_DATAFILE_ONLY, cookiePath);
  }


  @GetMapping(UrlPath.ACCESS_AIP + SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD_TOKEN)
  @PreAuthorize("@metadataPermissionService.isAllowed(#aipId, 'DOWNLOAD_ARCHIVE') || @downloadWithAclPermissionService.isAllowed(#aipId)")
  public ResponseEntity<DownloadToken> getTokenAip(@PathVariable("id") String aipId) {
    final String cookiePath = this.accessModulePath
            + "/" + ResourceName.AIP
            + "/" + aipId
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(aipId, DownloadTokenType.AIP_DOWNLOAD, cookiePath);
  }

  @GetMapping(UrlPath.ACCESS_DIP + SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD_TOKEN)
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowedToDownload(#dipId)")
  public ResponseEntity<DownloadToken> getTokenDip(@PathVariable("id") String dipId) {
    final String cookiePath = this.accessModulePath
            + "/" + ResourceName.DIP
            + "/" + dipId
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(dipId, DownloadTokenType.DIP, cookiePath);
  }

  @GetMapping(UrlPath.ACCESS_ORDER + SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD_TOKEN)
  @NoOnePermissions // Not yet implemented
  public ResponseEntity<DownloadToken> getTokenForOrder(@PathVariable("id") String orderId) {
    final String cookiePath = this.accessModulePath
            + "/" + ResourceName.ORDER
            + "/" + orderId
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(orderId, DownloadTokenType.ORDER, cookiePath);
  }
}

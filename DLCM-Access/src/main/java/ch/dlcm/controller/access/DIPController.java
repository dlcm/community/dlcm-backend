/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DIPController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.DataFileService;
import ch.dlcm.business.DisseminationInfoPackageService;
import ch.dlcm.controller.AbstractPackageController;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.ArchiveStatisticMessage;
import ch.dlcm.message.ArchiveStatisticMessage.StatisticType;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.oais.DipDataFile;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.repository.OrderRepository;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.DataCategoryService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.propagate.PropagateMetadataTypeRemoteResourceService;

@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_DIP)
public class DIPController extends AbstractPackageController<DipDataFile, DisseminationInfoPackage> implements ApplicationRoleListService {

  private final OrderRepository orderRepository;

  public DIPController(HistoryService historyService, MetadataService metadataService,
          PropagateMetadataTypeRemoteResourceService metadataTypeService, DataFileService<DipDataFile> dataFileService,
          DataCategoryService dataCategoryService, OrderRepository orderRepository) {
    super(historyService, metadataService, metadataTypeService, dataFileService, dataCategoryService);
    this.orderRepository = orderRepository;
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowedToCreate(#dip)")
  public HttpEntity<DisseminationInfoPackage> create(@RequestBody DisseminationInfoPackage dip) {
    return super.create(dip);
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowed(#dipId, 'GET')")
  public HttpEntity<DisseminationInfoPackage> get(@PathVariable("id") String dipId) {
    return super.get(dipId);
  }

  @Override
  public HttpEntity<RestCollection<DisseminationInfoPackage>> list(@ModelAttribute DisseminationInfoPackage search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowedToUpdate(#dipId)")
  public HttpEntity<DisseminationInfoPackage> update(@PathVariable("id") String dipId, @RequestBody Map<String, Object> updateMap) {
    return super.update(dipId, updateMap);
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowed(#dipId, 'DELETE')")
  public ResponseEntity<Void> delete(@PathVariable("id") String dipId) {
    // Check if DIP is used by an order
    if (!this.orderRepository.findByDip(dipId).isEmpty()) {
      throw new SolidifyUndeletableException("DIP referenced in an order");
    }
    return super.delete(dipId);
  }

  @Override
  @RootPermissions
  public ResponseEntity<Void> deleteList(@RequestBody String[] dipIds) {
    for (String id : dipIds) {
      // Check if DIP is used by an order
      if (!this.orderRepository.findByDip(id).isEmpty()) {
        throw new SolidifyUndeletableException("DIP referenced in an order");
      }
    }
    return super.deleteList(dipIds);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD)
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowedToDownload(#dipId)"
          + " || @downloadTokenPermissionService.isAllowed(#dipId, T(ch.dlcm.model.security.DownloadTokenType).DIP)")
  public HttpEntity<FileSystemResource> getFiles(@PathVariable("id") String dipId) {
    final DisseminationInfoPackage dip = this.itemService.findOne(dipId);
    if (!this.isRootOrTrustedOrAdminRole()) {
      // Log DL Statistics for each AIP
      for (String aipId : dip.getAipIds()) {
        SolidifyEventPublisher.getPublisher().publishEvent(new ArchiveStatisticMessage(aipId, StatisticType.DOWNLOAD));
      }
    }
    final URI fileUri = dip.getDownloadUri();
    return this.buildDownloadResponseEntity(fileUri.getPath(), dipId + SolidifyConstants.ZIP_EXT, DLCMConstants.DIP_MIME_TYPE,
            FileTool.getSize(fileUri));
  }

  @GetMapping("/" + ResourceName.TOTAL_COUNT_FOR_COMPLETED_ORDER)
  @EveryonePermissions
  public HttpEntity<Integer> getCountForCompletedOrders() {
    return new ResponseEntity<>(((DisseminationInfoPackageService) this.itemService).countDIPOfCompletedOrders(), HttpStatus.OK);
  }

  @Override
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowed(#dipId, 'HISTORY')")
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable("id") String dipId, Pageable pageable) {
    return super.history(dipId, pageable);
  }

  @GetMapping("/" + DLCMActionName.LIST_STATUS)
  @EveryonePermissions
  public HttpEntity<PackageStatus[]> listStatus() {
    return new ResponseEntity<>(PackageStatus.values(), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.RESUME)
  @PreAuthorize("@disseminationInfoPackagePermissionService.isAllowed(#dipId, 'RESUME')")
  public HttpEntity<Result> resume(@PathVariable("id") String dipId) {
    final Result res = ((DisseminationInfoPackageService) this.itemService).resumePackage(dipId);
    res.add(linkTo(this.getClass()).slash(dipId).slash(ActionName.RESUME).withSelfRel());
    res.add(linkTo(this.getClass()).slash(dipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.PUT_IN_ERROR)
  @RootPermissions
  public HttpEntity<Result> putInError(@PathVariable("id") String dipId) {
    final DisseminationInfoPackage dip = this.itemService.findOne(dipId);
    final Result result = new Result(dip.getResId());
    if (dip.isCompleted()) {
      result.setMesssage(this.messageService.get("message.package.completed", new Object[] { dipId }));
    } else {
      dip.setPackageStatus(PackageStatus.IN_ERROR);
      this.itemService.save(dip);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.package.in_error", new Object[] { dipId }));
    }
    result.add(linkTo(this.getClass()).slash(dipId).slash(DLCMActionName.PUT_IN_ERROR).withSelfRel());
    result.add(linkTo(this.getClass()).slash(dipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @GetMapping("/" + DLCMActionName.GET_BY_AIP_ID + SolidifyConstants.URL_ID)
  @TrustedUserPermissions
  public HttpEntity<RestCollection<DisseminationInfoPackage>> getDipContainingAip(@PathVariable("id") String aipId, Pageable pageable) {
    final Page<DisseminationInfoPackage> dipPage = ((DisseminationInfoPackageService) this.itemService).findDipContainingAip(aipId, pageable);
    return new ResponseEntity<>(new RestCollection<>(dipPage, pageable), HttpStatus.OK);
  }

  @GetMapping("/" + SolidifyConstants.SCHEMA)
  @EveryonePermissions
  public HttpEntity<StreamingResponseBody> schema(@RequestParam(value = "version", required = false) String version,
          @RequestParam(value = "disseminationPolicy", defaultValue = "PUBLIC", required = false) DisseminationPolicy.DisseminationPolicyType disseminationPolicy) {
    if (disseminationPolicy == DisseminationPolicy.DisseminationPolicyType.BASIC) {
      return this.getRepresentationInfoSchema(version);
    } else {
      return this.getSchemaStream(version, new DipDataFile());
    }
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listStatus()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listVersions()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).schema(DLCMMetadataVersion.getDefaultVersion().getVersion(),
            DisseminationPolicy.DisseminationPolicyType.BASIC)).withRel(SolidifyConstants.SCHEMA));
    w.add(linkTo(methodOn(this.getClass()).profile(DLCMMetadataVersion.getDefaultVersion().getVersion())).withRel(ResourceName.PROFILE));
  }
}

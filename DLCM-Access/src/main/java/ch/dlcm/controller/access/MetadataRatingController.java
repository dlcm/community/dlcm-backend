/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - MetadataRatingController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.DLCMConstants;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveUserRating;
import ch.dlcm.model.settings.RatingType;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.SearchService;
import ch.dlcm.service.rest.propagate.PropagateArchiveRatingRemoteResourceService;
import ch.dlcm.service.rest.propagate.PropagateUserRemoteResourceService;

@UserPermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_PUBLIC_METADATA + SolidifyConstants.URL_PARENT_ID + DLCMActionName.RATING)
public class MetadataRatingController extends SolidifyController {

  private final SearchService searchService;
  private final PropagateUserRemoteResourceService userRemoteResourceService;
  private final PropagateArchiveRatingRemoteResourceService archiveRatingRemoteResourceService;

  public MetadataRatingController(
          SearchService searchService,
          PropagateUserRemoteResourceService userRemoteResourceService,
          PropagateArchiveRatingRemoteResourceService archiveRatingRemoteResourceService) {
    this.searchService = searchService;
    this.userRemoteResourceService = userRemoteResourceService;
    this.archiveRatingRemoteResourceService = archiveRatingRemoteResourceService;
  }

  @GetMapping
  public HttpEntity<RestCollection<ArchiveUserRating>> getArchiveUserRating(@PathVariable String parentid, Pageable pageable) {
    try {
      return new ResponseEntity<>(this.archiveRatingRemoteResourceService.getArchiveUserRating(parentid, pageable), HttpStatus.OK);
    } catch (SolidifyResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/" + DLCMActionName.RATING_BY_USER)
  public HttpEntity<RestCollection<ArchiveUserRating>> getUserRating(@PathVariable String parentid) {
    try {
      User user = this.userRemoteResourceService.getUserAuthenticated();
      return new ResponseEntity<>(this.archiveRatingRemoteResourceService.getAllRatingByUser(parentid, user.getResId()), HttpStatus.OK);
    } catch (SolidifyResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PostMapping
  public HttpEntity<ArchiveUserRating> create(@PathVariable String parentid,
          @RequestParam(value = DLCMConstants.RATING_TYPE) String ratingType,
          @RequestParam(value = DLCMConstants.GRADE) String grade) {
    try {
      ArchiveUserRating archiveUserRating = this.createArchiveUserRating(parentid, ratingType, grade);
      if (archiveUserRating == null) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
      return new ResponseEntity<>(this.archiveRatingRemoteResourceService.createArchiveUserRating(archiveUserRating), HttpStatus.OK);
    } catch (SolidifyResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (SolidifyRuntimeException e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PatchMapping
  @SuppressWarnings("squid:S4684")
  public HttpEntity<ArchiveUserRating> update(@PathVariable String parentid, @RequestBody ArchiveUserRating archiveUserRating) {
    if (archiveUserRating == null) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    try {
      final ArchiveMetadata archive = this.searchService.findOne(parentid);
      if (archive == null) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
      // Check that the authenticated User is the one used in the resource
      User user = this.userRemoteResourceService.getUserAuthenticated();
      if (!user.getResId().equals(archiveUserRating.getUser().getResId())) {
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
      }
      // find the resId of the ArchiveUserRating to change
      RestCollection<ArchiveUserRating> ratingCollection = this.archiveRatingRemoteResourceService.getArchiveByUserAndRating(
              archiveUserRating.getArchiveId(),
              archiveUserRating.getUser().getResId(), archiveUserRating.getRatingType().getResId());
      if (ratingCollection.getData() == null || ratingCollection.getData().isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
      ArchiveUserRating ratingToModify = ratingCollection.getData().get(0);
      if (ratingToModify == null) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
      return new ResponseEntity<>(this.archiveRatingRemoteResourceService.updateArchiveUserRating(ratingToModify.getResId(), archiveUserRating),
              HttpStatus.OK);
    } catch (SolidifyResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (SolidifyRuntimeException e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @DeleteMapping("/" + DLCMActionName.BY_RATING)
  public ResponseEntity<Void> delete(@PathVariable String parentid, @RequestBody String ratingId) {
    try {
      // Check authenticated User
      User user = this.userRemoteResourceService.getUserAuthenticated();

      RestCollection<ArchiveUserRating> listRating = this.archiveRatingRemoteResourceService.getAllRatingByUser(parentid, user.getResId());
      // find the rating selected
      Optional<ArchiveUserRating> aurToDelete = listRating.getData().stream()
              .filter(aur -> aur.getRatingType().getResId().equals(ratingId)).findFirst();
      if (aurToDelete.isPresent()) {
        this.archiveRatingRemoteResourceService.deleteArchiveUserRating(aurToDelete.get().getResId());
      }
    } catch (SolidifyResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (SolidifyRuntimeException e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @DeleteMapping
  public ResponseEntity<Void> delete(@PathVariable String parentid) {
    try {
      // Check authenticated User
      User user = this.userRemoteResourceService.getUserAuthenticated();

      RestCollection<ArchiveUserRating> listRating = this.archiveRatingRemoteResourceService.getAllRatingByUser(parentid, user.getResId());
      for (ArchiveUserRating aur : listRating.getData()) {
        this.archiveRatingRemoteResourceService.deleteArchiveUserRating(aur.getResId());
      }
    } catch (SolidifyResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (SolidifyRuntimeException e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  private ArchiveUserRating createArchiveUserRating(String archiveId, String ratingTypeId, String grade) {
    final ArchiveMetadata archive = this.searchService.findOne(archiveId);
    if (archive == null) {
      return null;
    }
    final User user = this.userRemoteResourceService.getUserAuthenticated();
    if (user == null) {
      return null;
    }
    final RatingType ratingType = this.archiveRatingRemoteResourceService.getRatingType(ratingTypeId);
    if (ratingType == null) {
      return null;
    }

    ArchiveUserRating archiveUserRating = new ArchiveUserRating();
    archiveUserRating.setArchiveId(archive.getResId());
    archiveUserRating.setUser(user);
    archiveUserRating.setRatingType(ratingType);
    archiveUserRating.setGrade(Integer.parseInt(grade));
    return archiveUserRating;
  }

}

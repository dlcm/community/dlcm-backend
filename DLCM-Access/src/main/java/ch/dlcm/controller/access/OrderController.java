/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrderController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import static ch.dlcm.model.access.Order.OrderStatus.IN_PROGRESS;
import static ch.dlcm.model.access.Order.OrderStatus.SUBMITTED;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.business.OrderService;
import ch.dlcm.controller.AccessController;
import ch.dlcm.controller.ResourceWithHistoryController;
import ch.dlcm.model.MetadataVisibility;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.access.OrderArchive;
import ch.dlcm.model.access.QueryType;
import ch.dlcm.repository.OrderRepository;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.SearchMgmt;

@UserPermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_ORDER)
public class OrderController extends ResourceWithHistoryController<Order> {

  final ArchivalInfoPackageService aipService;
  final SearchMgmt searchMgmt;

  public OrderController(HistoryService historyService, ArchivalInfoPackageService aipService, SearchMgmt searchMgmt) {
    super(historyService);
    this.aipService = aipService;
    this.searchMgmt = searchMgmt;
  }

  @Override
  public HttpEntity<Order> create(@RequestBody Order order) {
    return super.create(order);
  }

  @Override
  @PreAuthorize("@orderPermissionService.isAllowedToGet(#orderId)")
  public HttpEntity<Order> get(@PathVariable("id") String orderId) {
    return super.get(orderId);
  }

  @Override
  @AdminPermissions
  public HttpEntity<RestCollection<Order>> list(@ModelAttribute Order search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @PreAuthorize("@orderPermissionService.isAllowedToUpdate(#orderId)")
  public HttpEntity<Order> update(@PathVariable("id") String orderId, @RequestBody Map<String, Object> updateMap) {
    return super.update(orderId, updateMap);
  }

  @Override
  @PreAuthorize("@orderPermissionService.isAllowedToDelete(#orderId)")
  public ResponseEntity<Void> delete(@PathVariable("id") String orderId) {
    return super.delete(orderId);
  }

  @Override
  @PreAuthorize("@orderPermissionService.isAllowedToDeleteList(#orderIds)")
  public ResponseEntity<Void> deleteList(@RequestBody String[] orderIds) {
    return super.deleteList(orderIds);
  }

  @NoOnePermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD)
  public HttpEntity<InputStreamResource> getFiles(@PathVariable("id") String orderId) {
    final Order order = ((OrderRepository) this.itemService).findOneWithDip(orderId);
    if (order == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    if (order.getStatus() != OrderStatus.READY) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    // TODO download all DIPs from item.getDipPackages()
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @SuppressWarnings("squid:S4684")
  @GetMapping("/" + DLCMActionName.ORDER_LIST_CREATED_BY_USER)
  public HttpEntity<RestCollection<Order>> listCreatedByUser(@ModelAttribute Order order, Pageable pageable) {
    final String externalUid = this.getAuthenticatedUserExternalUid();
    order.setCreatedBy(externalUid);
    return super.list(order, pageable);
  }

  @SuppressWarnings("squid:S4684")
  @GetMapping("/" + DLCMActionName.ORDER_LIST_PUBLIC)
  public HttpEntity<RestCollection<Order>> listPublic(@ModelAttribute Order order, Pageable pageable) {
    order.setPublicOrder(Boolean.TRUE);
    return super.list(order, pageable);
  }

  @GetMapping("/" + DLCMActionName.LIST_QUERY_TYPE)
  public HttpEntity<QueryType[]> listQueryType() {
    return new ResponseEntity<>(QueryType.values(), HttpStatus.OK);
  }

  @GetMapping("/" + DLCMActionName.LIST_STATUS)
  public HttpEntity<OrderStatus[]> listStatus() {
    return new ResponseEntity<>(OrderStatus.values(), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.RESUME)
  public HttpEntity<Result> resume(@PathVariable("id") String orderId) {
    return ((OrderService) this.itemService).changeOrderStatus(IN_PROGRESS, orderId);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.SAVE)
  public HttpEntity<Order> save(@PathVariable("id") String orderId) {
    Order order = this.itemService.findOne(orderId);
    if (order.getQuery().isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    order = ((OrderService) this.itemService).saveOrder(order);
    return new ResponseEntity<>(order, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.SUBMIT)
  public HttpEntity<Result> submit(@PathVariable("id") String orderId) {
    return ((OrderService) this.itemService).changeOrderStatus(SUBMITTED, orderId);
  }

  @RootPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.PUT_IN_ERROR)
  public HttpEntity<Result> putInError(@PathVariable("id") String orderId) {
    final Order order = this.itemService.findOne(orderId);
    final Result result = new Result(order.getResId());
    if (order.getStatus() == OrderStatus.READY) {
      result.setMesssage(this.messageService.get("message.order.completed", new Object[] { orderId }));
    } else {
      order.setStatus(OrderStatus.IN_ERROR);
      this.itemService.save(order);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.order.in_error", new Object[] { orderId }));
    }
    result.add(linkTo(this.getClass()).slash(orderId).slash(DLCMActionName.PUT_IN_ERROR).withSelfRel());
    result.add(linkTo(this.getClass()).slash(orderId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.VIEW)
  public HttpEntity<RestCollection<OrderArchive>> view(@PathVariable("id") String orderId, @RequestParam(defaultValue = "false") boolean allInfo,
          Pageable pageable) {
    final Order order = this.itemService.findOne(orderId);
    if (order.isEmptyQuery()) {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    final RestCollection<OrderArchive> orderArchiveCollection = this.searchMgmt.search(MetadataVisibility.PUBLIC, order, allInfo, pageable);
    orderArchiveCollection.add(linkTo(methodOn(this.getClass()).view(orderId, allInfo, pageable)).withSelfRel());
    orderArchiveCollection.add(linkTo(methodOn(this.getClass()).get(orderId)).withRel(ActionName.PARENT));
    this.addPageLinks(linkTo(methodOn(this.getClass()).view(orderId, allInfo, pageable)), orderArchiveCollection, pageable);
    return new ResponseEntity<>(orderArchiveCollection, HttpStatus.OK);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listStatus()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listQueryType()).withRel(ActionName.VALUES));
  }

}

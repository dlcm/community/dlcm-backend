/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AIPDownloadController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.unit.DataSize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.business.DataFileService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AbstractPackageController;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.AipDownloadMessage;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.repository.OrderRepository;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.DataCategoryService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.SearchMgmt;
import ch.dlcm.service.rest.propagate.PropagateMetadataTypeRemoteResourceService;

@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_AIP)
public class AIPDownloadController extends AbstractPackageController<AipDataFile, ArchivalInfoPackage> {
  private static final Logger log = LoggerFactory.getLogger(AIPDownloadController.class);

  private final OrderRepository orderRepository;
  private final SearchMgmt searchMgmt;

  private final DataSize fileSizeLimit;

  public AIPDownloadController(DLCMProperties dlcmProperties, HistoryService historyService,
          MetadataService metadataService, PropagateMetadataTypeRemoteResourceService metadataTypeService,
          DataFileService<AipDataFile> dataFileService, DataCategoryService dataCategoryService, OrderRepository orderRepository,
          SearchMgmt searchMgmt) {
    super(historyService, metadataService, metadataTypeService, dataFileService, dataCategoryService);
    this.orderRepository = orderRepository;
    this.searchMgmt = searchMgmt;

    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit();
  }

  @Override
  @SuppressWarnings("squid:S4684")
  @TrustedUserPermissions
  public HttpEntity<ArchivalInfoPackage> create(@RequestBody ArchivalInfoPackage aipToGet) {
    ArchivalInfoPackage aip;
    try {
      aip = this.searchMgmt.getAIP(aipToGet.getResId());
    } catch (final SolidifyResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } catch (final SolidifyRestException e) {
      log.error("REST error:", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    if (aip.getInfo().getStatus() != PackageStatus.COMPLETED) {
      throw new IllegalStateException("Aip " + aip.getResId() + " is not completed yet");
    }
    if (!this.itemService.existsById(aip.getResId())) {
      boolean isBigPackage = aip.getArchiveSize() != null && aip.getArchiveSize() > this.fileSizeLimit.toBytes();
      aip.reset();
      this.itemService.save(aip);
      SolidifyEventPublisher.getPublisher().publishEvent(new AipDownloadMessage(aip.getResId(), isBigPackage));
      this.addLinks(aip);
    }
    return new ResponseEntity<>(aip, HttpStatus.CREATED);
  }

  @Override
  @PreAuthorize("@archivalInfoPackagePermissionService.isAllowed(#id, 'GET')")
  public HttpEntity<ArchivalInfoPackage> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<ArchivalInfoPackage>> list(@ModelAttribute ArchivalInfoPackage search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @PreAuthorize("@archivalInfoPackagePermissionService.isAllowedToUpdate(#id)")
  public HttpEntity<ArchivalInfoPackage> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  @PreAuthorize("@archivalInfoPackagePermissionService.isAllowed(#id, 'DELETE')")
  public ResponseEntity<Void> delete(@PathVariable String id) {
    // Check if AIP is used by an order
    if (!this.orderRepository.findByAip(id).isEmpty()) {
      throw new SolidifyUndeletableException("AIP referenced in an order");
    }
    this.itemService.delete(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PreAuthorize("@metadataPermissionService.isAllowed(#aipId, 'DOWNLOAD_ARCHIVE') || @downloadWithAclPermissionService.isAllowed(#aipId)"
          + "|| @downloadTokenPermissionService.isAllowed(#aipId, T(ch.dlcm.model.security.DownloadTokenType).AIP_DOWNLOAD)")
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD)
  public HttpEntity<FileSystemResource> getFiles(@PathVariable("id") String aipId) {
    final ArchivalInfoPackage item = this.itemService.findOne(aipId);
    final URI fileUri = item.getDownloadUriForAccess();
    return this.buildDownloadResponseEntity(fileUri.getPath(), aipId + SolidifyConstants.ZIP_EXT, DLCMConstants.AIP_MIME_TYPE,
            FileTool.getSize(fileUri));
  }

  @GetMapping("/" + DLCMActionName.LIST_STATUS)
  public HttpEntity<PackageStatus[]> listStatus() {
    return new ResponseEntity<>(PackageStatus.values(), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.RESUME)
  public HttpEntity<Result> resume(@PathVariable String id) {
    final ArchivalInfoPackage item = this.itemService.findOne(id);
    final Result res = new Result(item.getResId());
    res.setStatus(ActionStatus.NOT_EXECUTED);
    if (item.getInfo().getStatus() == PackageStatus.COMPLETED) {
      res.setStatus(ActionStatus.NON_APPLICABLE);
      res.setMesssage(this.messageService.get("message.package.completed", new Object[] { id }));
    } else if (item.getInfo().getStatus() == PackageStatus.DOWNLOADING) {
      res.setMesssage(this.messageService.get("message.package.inprogress", new Object[] { id }));
    } else {
      item.setPackageStatus(PackageStatus.IN_PROGRESS);
      this.itemService.save(item);
      res.setStatus(ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.package.download", new Object[] { id }));
      SolidifyEventPublisher.getPublisher().publishEvent(new AipDownloadMessage(id));
    }
    res.add(linkTo(this.getClass()).slash(id).slash(ActionName.RESUME).withSelfRel());
    res.add(linkTo(this.getClass()).slash(id).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @RootPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.PUT_IN_ERROR)
  public HttpEntity<Result> putInError(@PathVariable String id) {
    final ArchivalInfoPackage aip = this.itemService.findOne(id);
    final Result result = new Result(aip.getResId());
    if (aip.isCompleted()) {
      result.setMesssage(this.messageService.get("message.package.completed", new Object[] { id }));
    } else {
      // Set to status to trigger check process
      aip.setPackageStatus(PackageStatus.IN_ERROR);
      this.itemService.save(aip);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.package.in_error", new Object[] { id }));
    }
    result.add(linkTo(this.getClass()).slash(id).slash(DLCMActionName.PUT_IN_ERROR).withSelfRel());
    result.add(linkTo(this.getClass()).slash(id).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping("/" + ResourceName.TOTAL_COUNT_FOR_COMPLETED_ORDER)
  public HttpEntity<Integer> getCountForCompletedOrders() {
    return new ResponseEntity<>(((ArchivalInfoPackageService) this.itemService).countAIPDownloadOfCompletedOrders(), HttpStatus.OK);
  }

  @Override
  protected void addLinks(ArchivalInfoPackage aip) {
    aip.removeLinks();
    aip.addLinksForAccess(linkTo(this.getClass()), true, true);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listStatus()).withRel(ActionName.VALUES));
  }
}

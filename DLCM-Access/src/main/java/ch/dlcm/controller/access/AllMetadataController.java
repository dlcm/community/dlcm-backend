/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AllMetadataController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.model.dto.CitationDto;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.service.DownloadTokenService;

import ch.dlcm.business.OrderService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.repository.DisseminationInfoPackageRepository;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.ArchiveUserFingerprintService;
import ch.dlcm.service.citation.DLCMCitationGeneratorService;
import ch.dlcm.service.dissemination.DisseminationPolicyProvider;
import ch.dlcm.service.rest.trusted.TrustedArchiveAllMetadataRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

@UserPermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_ALL_METADATA)
public class AllMetadataController extends SearchController {

  public AllMetadataController(
          TrustedArchiveAllMetadataRemoteResourceService searchService,
          DLCMProperties dlcmProperties,
          DisseminationInfoPackageRepository dipRepository,
          OrderService orderService,
          ArchiveUserFingerprintService archiveUserFingerprintService,
          DLCMCitationGeneratorService dlcmCitationGeneratorService,
          DownloadTokenService downloadTokenService,
          TrustedUserRemoteResourceService trustedUserRemoteResourceService,
          DisseminationPolicyProvider disseminationPolicyProvider) {
    super(searchService, dlcmProperties, dipRepository, orderService, archiveUserFingerprintService,
            dlcmCitationGeneratorService, downloadTokenService, trustedUserRemoteResourceService, disseminationPolicyProvider);
  }

  @Override
  public HttpEntity<ArchiveMetadata> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<List<CitationDto>> bibliographies(@PathVariable String id) {
    return super.bibliographies(id);
  }

  @Override
  public HttpEntity<List<CitationDto>> citations(@PathVariable String id) {
    return super.citations(id);
  }

  @TrustedUserPermissions
  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> list(@ModelAttribute ArchiveMetadata search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> search(
          @RequestParam(required = false) String query,
          Pageable pageable) {
    return super.search(query, pageable);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> searchPost(@RequestBody MultiValueMap<String, String> params) {
    return super.searchPost(params);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> searchPost(
          @RequestBody(required = false) List<SearchCondition> searchConditions,
          Pageable pageable) {
    return super.searchPost(searchConditions, pageable);
  }

  @Override
  public HttpEntity<OrderStatus> getDownloadStatus(@PathVariable String orderId) {
    return super.getDownloadStatus(orderId);
  }

}

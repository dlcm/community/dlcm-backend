/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - SearchController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import static ch.dlcm.DLCMConstants.DISSEMINATION_POLICY_BASIC_ID;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.dto.CitationDto;
import ch.unige.solidify.model.security.DownloadToken;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.DownloadTokenService;
import ch.unige.solidify.service.rest.abstractservice.IndexMetadataRemoteResourceService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.OrderService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessIndexResourceReadOnlyController;
import ch.dlcm.message.ArchiveStatisticMessage;
import ch.dlcm.message.ArchiveStatisticMessage.StatisticType;
import ch.dlcm.model.MetadataVisibility;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.dto.DisseminationPolicyDto;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.model.security.DownloadTokenType;
import ch.dlcm.model.security.User;
import ch.dlcm.repository.DisseminationInfoPackageRepository;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.service.ArchiveUserFingerprintService;
import ch.dlcm.service.citation.DLCMCitationGeneratorService;
import ch.dlcm.service.dissemination.DisseminationPolicyProvider;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

public abstract class SearchController extends AccessIndexResourceReadOnlyController<ArchiveMetadata>
        implements ApplicationRoleListService {
  private static final Logger log = LoggerFactory.getLogger(SearchController.class);

  protected final DisseminationInfoPackageRepository dipRepository;
  private final OrderService orderService;
  private final ArchiveUserFingerprintService archiveUserFingerprintService;
  private final DLCMCitationGeneratorService dlcmCitationGeneratorService;
  private final DownloadTokenService downloadTokenService;
  private final TrustedUserRemoteResourceService trustedUserRemoteResourceService;
  private final DisseminationPolicyProvider disseminationPolicyProvider;

  protected SearchController(
          IndexMetadataRemoteResourceService<ArchiveMetadata> indexResourceRemoteService,
          DLCMProperties dlcmProperties,
          DisseminationInfoPackageRepository dipRepository,
          OrderService orderService,
          ArchiveUserFingerprintService archiveUserFingerprintService,
          DLCMCitationGeneratorService dlcmCitationGeneratorService,
          DownloadTokenService downloadTokenService,
          TrustedUserRemoteResourceService trustedUserRemoteResourceService,
          DisseminationPolicyProvider disseminationPolicyProvider) {
    super(indexResourceRemoteService, dlcmProperties);
    this.dipRepository = dipRepository;
    this.orderService = orderService;
    this.archiveUserFingerprintService = archiveUserFingerprintService;
    this.dlcmCitationGeneratorService = dlcmCitationGeneratorService;
    this.downloadTokenService = downloadTokenService;
    this.trustedUserRemoteResourceService = trustedUserRemoteResourceService;
    this.disseminationPolicyProvider = disseminationPolicyProvider;
  }

  @Override
  public HttpEntity<ArchiveMetadata> get(@PathVariable String id) {
    final ArchiveMetadata metadata = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (metadata != null) {
      if (!this.isRootOrTrustedOrAdminRole()) {
        // Log View Statistics
        this.logViewStatistic(metadata);
      }
      this.addLinks(metadata);
      return new ResponseEntity<>(metadata, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.BIBLIOGRAPHIES)
  public HttpEntity<List<CitationDto>> bibliographies(@PathVariable String id) {
    final ArchiveMetadata metadata = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (metadata != null) {
      List<CitationDto> bibliographies = this.dlcmCitationGeneratorService.getBibliographiesDtos(metadata.getResId());
      return new ResponseEntity<>(bibliographies, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.CITATIONS)
  public HttpEntity<List<CitationDto>> citations(@PathVariable String id) {
    final ArchiveMetadata metadata = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (metadata != null) {
      List<CitationDto> citations = this.dlcmCitationGeneratorService.getCitationsDtos(metadata.getResId());
      return new ResponseEntity<>(citations, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @GetMapping("/{dipName}/" + ActionName.DOWNLOAD)
  @PreAuthorize("@metadataPermissionService.isAllowed(#dipName, 'DOWNLOAD_ARCHIVE') "
          + "|| @downloadWithAclPermissionService.isAllowed(#dipName) "
          + "|| @downloadTokenPermissionService.isAllowed(#dipName, T(ch.dlcm.model.security.DownloadTokenType).ARCHIVE)")
  public HttpEntity<FileSystemResource> download(@PathVariable String dipName, @RequestParam(required = false) String orderId) {
    // Find the linked DIP of the archive for direct download: DIP Name = AIP Id
    final List<DisseminationInfoPackage> dipList;
    DisseminationPolicy disseminationPolicy;
    if (orderId == null) {
      // If order is not specified, check a direct order with the aipId as dipName and disseminationPolicyPublic and not itemList selected.
      DisseminationPolicyDto dpDTO = new DisseminationPolicyDto();
      dpDTO.setDisseminationPolicyId(DISSEMINATION_POLICY_BASIC_ID);
      List<Order> orderList = this.orderService.findByArchiveAndDisseminationPolicy(dipName, dpDTO);
      if (orderList.isEmpty()) {
        throw new IllegalStateException("Order for DIP " + dipName + " is missing");
      }
      if (orderList.size() > 1) {
        throw new SolidifyRuntimeException("More than one Full Order DIRECT for the aip " + dipName);
      }
      // In case of full public direct order, the name of the file should be the name of the archive
      orderId = orderList.get(0).getResId();
    }

    dipList = this.dipRepository.findByOrderId(orderId);
    disseminationPolicy = this.orderService.findDisseminationPolicyByOrderId(orderId);

    if (dipList.isEmpty()) {
      throw new IllegalStateException("DIP for AIP " + dipName + " is missing");
    } else if (dipList.size() > 1) {
      throw new SolidifyRuntimeException("More than one DIP is present for AIP " + dipName);
    }

    // Take fileName according what is defined in the dissemination policy
    final String fileName = this.calculateFinalFileName(dipName, disseminationPolicy);

    // Download DIP of AIP
    final URI fileUri = dipList.get(0).getDownloadUri();
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null || (!this.checkUserInDownloadTokenIsAdminOrRootOrTrusted(dipName) && !this.isRootOrTrustedOrAdminRole())) {
      // Log DL Statistics
      SolidifyEventPublisher.getPublisher().publishEvent(new ArchiveStatisticMessage(dipName, StatisticType.DOWNLOAD));
    }
    return this.buildDownloadResponseEntity(fileUri.getPath(), fileName + SolidifyConstants.ZIP_EXT, DLCMConstants.DIP_MIME_TYPE,
            FileTool.getSize(fileUri));
  }

  @GetMapping("/{orderId}/" + DLCMActionName.DOWNLOAD_STATUS)
  public HttpEntity<OrderStatus> getDownloadStatus(@PathVariable String orderId) {
    final Order order = this.orderService.findOne(orderId);
    if (order == null) {
      throw new NoSuchElementException("Order not found");
    }
    return new ResponseEntity<>(order.getStatus(), HttpStatus.OK);
  }

  @GetMapping("/" + DLCMActionName.LIST_DOWNLOAD_STATUS)
  @EveryonePermissions
  public HttpEntity<OrderStatus[]> listDownloadStatus() {
    return new ResponseEntity<>(OrderStatus.values(), HttpStatus.OK);
  }

  @PostMapping("/{aipId}/" + DLCMActionName.PREPARE_DOWNLOAD)
  @PreAuthorize("@metadataPermissionService.isAllowed(#aipId, 'PREPARE_DOWNLOAD_ARCHIVE')"
          + "|| @downloadWithAclPermissionService.isAllowed(#aipId)")
  public HttpEntity<String> prepareDownload(@PathVariable String aipId,
          @RequestBody(required = false) DisseminationPolicyDto disseminationPolicyDto) {

    if (disseminationPolicyDto == null) {
      disseminationPolicyDto = new DisseminationPolicyDto();
    }

    if (disseminationPolicyDto.getDisseminationPolicyId() == null) {
      disseminationPolicyDto.setDisseminationPolicyId(DISSEMINATION_POLICY_BASIC_ID);
    }
    try {
      final String orderId = this.getOrderIdentifier(aipId, disseminationPolicyDto);
      return new ResponseEntity<>(orderId, HttpStatus.ACCEPTED);
    } catch (SolidifyCheckingException e) {
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  protected String getOrderIdentifier(String aipId, DisseminationPolicyDto disseminationPolicyDto) {
    Order order = null;
    List<Order> orders = this.orderService.findByArchiveAndDisseminationPolicy(aipId, disseminationPolicyDto);
    if (orders.isEmpty()) {
      // create a new order
      order = this.orderService.createOrderForAIP(aipId, disseminationPolicyDto);
      this.orderService.submitOrder(order);
      return order.getResId();
    } else if (orders.size() == 1) {
      order = orders.get(0);
      switch (order.getStatus()) {
        case IN_PROGRESS -> this.orderService.submitOrder(order);
        case IN_ERROR -> {
          log.error("AIP download: order in error");
          throw new SolidifyCheckingException("Order is in error");
        }
        default -> {
          // do nothing
        }
      }

    } else {
      log.error("AIP download: too many orders");
      throw new SolidifyCheckingException("Too many orders found");
    }
    return order.getResId();
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> search(@RequestParam(required = false) String query, Pageable pageable) {
    // transform sort parameters from the portal to the corresponding name in the index
    if (!pageable.getSort().isEmpty() && pageable.getSort().isSorted()) {
      pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), this.adaptSortProperties(pageable.getSort()));
    }

    return super.search(query, pageable);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> searchPost(
          @RequestBody(required = false) List<SearchCondition> searchConditions,
          Pageable pageable) {
    // transform sort parameters from the portal to the corresponding name in the index
    if (!pageable.getSort().isEmpty() && pageable.getSort().isSorted()) {
      pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), this.adaptSortProperties(pageable.getSort()));
    }
    return super.searchPost(searchConditions, pageable);
  }

  protected Sort adaptSortProperties(Sort sort) {
    // create a copy from the sort since the list of orders are not modifiable.
    List<org.springframework.data.domain.Sort.Order> newOrderList = new ArrayList<>(sort.toList());
    for (Sort.Order order : sort.toList()) {
      String newNameProperty = null;
      switch (order.getProperty()) {
        case DLCMConstants.ORG_UNIT_ID_FIELD -> newNameProperty = DLCMConstants.AIP_ORGA_UNIT_NAME + DLCMConstants.INDEXING_KEYWORD;
        case "title" -> newNameProperty = DLCMConstants.AIP_TITLE + DLCMConstants.INDEXING_KEYWORD;
        case "accessLevel" -> newNameProperty = DLCMConstants.AIP_ACCESS_LEVEL + DLCMConstants.INDEXING_KEYWORD;
        case "dataSensitivity" -> newNameProperty = DLCMConstants.AIP_DATA_TAG + DLCMConstants.INDEXING_KEYWORD;
        case "size" -> newNameProperty = DLCMConstants.AIP_SIZE;
        case "files" -> newNameProperty = DLCMConstants.AIP_FILE_NUMBER;
        default -> {
        }
      }
      if (!StringTool.isNullOrEmpty(newNameProperty)) {
        newOrderList.remove(order);
        newOrderList.add(new Sort.Order(order.getDirection(), newNameProperty));
      }
    }
    return Sort.by(newOrderList);
  }

  @Override
  protected void addLinks(ArchiveMetadata archiveMetadata) {
    archiveMetadata.removeLinks();
    archiveMetadata.addLinksForAccess(linkTo(this.getClass()));
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).search("*", null)).withRel(ActionName.SEARCH));
    w.add(linkTo(methodOn(this.getClass()).listDownloadStatus()).withRel(ActionName.VALUES));
  }

  private void logViewStatistic(ArchiveMetadata metadata) {
    String resId = metadata.getResId();

    // Check if data file metadata
    if (metadata.getIndex().equals(MetadataVisibility.RESTRICTED.toString())
            && !metadata.getMetadata().get(DLCMConstants.TYPE_FIELD).equals(DLCMConstants.PACKAGE)) {
      resId = (String) (metadata.getMetadata().get(DLCMConstants.ARCHIVE_ID_FIELD));
    }

    if (!this.archiveUserFingerprintService.existsUnexpiredArchiveUserFingerprint(resId)) {
      SolidifyEventPublisher.getPublisher().publishEvent(new ArchiveStatisticMessage(resId, StatisticType.VIEW));
    }
  }

  private boolean checkUserInDownloadTokenIsAdminOrRootOrTrusted(String archiveId) {
    String tokenHash = this.downloadTokenService.getTokenHashFromRequest(archiveId, DownloadTokenType.ARCHIVE);
    if (!StringTool.isNullOrEmpty(tokenHash)) {
      DownloadToken downloadToken = this.downloadTokenService.findByTokenHash(tokenHash);
      if (downloadToken != null) {
        try {
          User user = this.trustedUserRemoteResourceService.findByExternalUid(downloadToken.getUserId());
          return user != null && (user.getApplicationRole().getResId().equals(AuthApplicationRole.ADMIN_ID) || user.getApplicationRole()
                  .getResId()
                  .equals(AuthApplicationRole.ROOT_ID)
                  || user.getApplicationRole().getResId().equals(AuthApplicationRole.TRUSTED_CLIENT_ID));
        } catch (SolidifyRestException e) {
          // If the user does not exist, an exception will be thrown meaning the user has no role ADMIN, ROOT or TRUSTED
        }
      }
    }
    return false;
  }

  private String calculateFinalFileName(String dipName, DisseminationPolicy disseminationPolicy) {
    final ArchiveMetadata archive = this.getIndexResourceRemoteService().getIndexMetadata(dipName);
    return this.disseminationPolicyProvider.calculateFinalFileName(disseminationPolicy, archive.getTitle(), dipName);
  }
}

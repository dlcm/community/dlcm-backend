/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrgUnitController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.NoSqlResourceService;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.controller.AccessNoSqlResourceReadOnlyController;
import ch.dlcm.model.access.OrgUnit;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_ORG_UNIT)
public class OrgUnitController extends AccessNoSqlResourceReadOnlyController<OrgUnit> {

  private final FallbackOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService;

  public OrgUnitController(@Qualifier("orgUnitRemoteService") NoSqlResourceService<OrgUnit> noSqlResourceService,
          DLCMProperties dlcmProperties, FallbackOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService) {
    super(noSqlResourceService, dlcmProperties);
    this.organizationalUnitRemoteResourceService = organizationalUnitRemoteResourceService;
  }

  @Override
  public HttpEntity<OrgUnit> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<OrgUnit>> list(@ModelAttribute OrgUnit search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DOWNLOAD_LOGO)
  @ResponseBody
  public HttpEntity<StreamingResponseBody> downloadLogo(@PathVariable String id) {
    final OrgUnit orgUnit = this.noSqlResourceService.findOne(id);
    if (orgUnit == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    try {
      // Check if thumbnail exists
      Path thumbnailPath = this.getThumbnailPath(id);
      // Check if thumbnail already download
      if (!FileTool.checkFile(thumbnailPath)) {
        this.organizationalUnitRemoteResourceService.downloadOrgUnitLogo(id, thumbnailPath);
      }
      return this.getThumbnail(thumbnailPath);
    } catch (HttpClientErrorException.NotFound e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

}

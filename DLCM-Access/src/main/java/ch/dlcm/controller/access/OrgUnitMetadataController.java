/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrgUnitMetadataController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.access;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.index.AssociationIndexReadOnlyRemoteController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.CompositeNoSqlResourceService;

import ch.dlcm.controller.AccessController;
import ch.dlcm.model.MetadataVisibility;
import ch.dlcm.model.access.OrgUnit;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.SearchService;

@EveryonePermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_ORG_UNIT + SolidifyConstants.URL_PARENT_ID + ResourceName.ARCHIVE)
public class OrgUnitMetadataController extends AssociationIndexReadOnlyRemoteController<OrgUnit, ArchiveMetadata> {

  public OrgUnitMetadataController(
          @Qualifier("orgUnitArchiveService") CompositeNoSqlResourceService<OrgUnit> resourceServiceNoSql,
          SearchService searchService) {
    super(resourceServiceNoSql, searchService);
  }

  @Override
  public HttpEntity<ArchiveMetadata> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> list(@PathVariable String parentid,
          @ModelAttribute ArchiveMetadata filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  protected String getIndex() {
    return MetadataVisibility.PUBLIC.toString();
  }

  @Override
  protected Class<OrgUnit> getMainResourceClass() {
    return OrgUnit.class;
  }

  @Override
  protected Class<ArchiveMetadata> getSubResourceClass() {
    return ArchiveMetadata.class;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DisseminationInfoPackageRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.dlcm.controller.AccessController;
import ch.dlcm.model.oais.DisseminationInfoPackage;

@Repository
@ConditionalOnBean(AccessController.class)
public interface DisseminationInfoPackageRepository extends SolidifyRepository<DisseminationInfoPackage> {

  List<DisseminationInfoPackage> findByInfoName(String name);

  @Query("SELECT COALESCE(SUM(df.fileSize), 0) "
          + "FROM DipDataFile df "
          + "WHERE df.infoPackage.resId = :dipId")
  long getSize(String dipId);

  @Query("SELECT COUNT(DISTINCT d.resId) "
          + "FROM DisseminationInfoPackage d "
          + "JOIN d.orders o "
          + "WHERE o.queryType = 'SIMPLE' "
          + "AND o.status = 'READY'")
  int countDIPOfCompletedOrders();

  @Query("SELECT dip "
          + "FROM DisseminationInfoPackage dip "
          + "JOIN dip.aips aip "
          + "WHERE aip.resId = :aipId")
  Page<DisseminationInfoPackage> findDipContainingAip(String aipId, Pageable pageable);

  @Query("SELECT dip "
          + "FROM DisseminationInfoPackage dip "
          + "JOIN dip.orders order "
          + "WHERE order.resId = :orderId")
  List<DisseminationInfoPackage> findByOrderId(String orderId);

}

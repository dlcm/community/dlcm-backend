/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrderRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.dlcm.controller.AccessController;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.access.QueryType;

@Repository
@ConditionalOnBean(AccessController.class)
public interface OrderRepository extends SolidifyRepository<Order> {

  @Query("SELECT o FROM Order o LEFT JOIN FETCH o.dipPackages where o.resId=:id")
  Order findOneWithDip(String id);

  @Query("SELECT DISTINCT o FROM Order o JOIN o.dipPackages dip  WHERE dip.resId=:dipId")
  List<Order> findByDip(String dipId);

  @Query("SELECT o FROM Order o LEFT JOIN FETCH o.aipPackages where o.resId=:orderId")
  Order findOneWithAip(String orderId);

  @Query("SELECT DISTINCT o FROM Order o JOIN o.aipPackages aip  WHERE aip.resId=:aipId")
  List<Order> findByAip(String aipId);

  List<Order> findByQueryTypeAndQueryAndDisseminationPolicyIdAndOrganizationalUnitDisseminationPolicyIdAndSubitemsChecksum(
          QueryType queryType, String query, String disseminiationPolicyId, String organizationalUnitDisseminationPolicyId, String subItemHash);

  List<Order> findByStatusIn(List<OrderStatus> statuses);

  @Query("SELECT COALESCE(SUM(df.fileSize), 0) FROM Order o JOIN o.aipPackages aip JOIN aip.dataFiles df WHERE o.resId = :orderId")
  long getSize(String orderId);

  /**
   * Find orders that have the same checksum
   *
   * @param checksum the checksum to search for
   * @param orderId the order ID to exclude from search
   * @return list of orders with matching checksums
   */
  @Query("SELECT o FROM Order o WHERE o.subitemsChecksum = :checksum AND o.resId != :orderId ORDER BY o.creation.when DESC")
  List<Order> findDifferentOrdersWithSameChecksum(String checksum, String orderId);

}

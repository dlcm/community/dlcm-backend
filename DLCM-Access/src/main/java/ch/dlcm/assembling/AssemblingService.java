/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AssemblingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.assembling;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.Access;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.service.DLCMService;

public abstract class AssemblingService extends DLCMService {

  protected final String orderLocation;

  protected AssemblingService(DLCMProperties dlcmProperties, MessageService messageService) {
    super(messageService, dlcmProperties);
    this.orderLocation = dlcmProperties.getOrderLocation();
  }

  // **************************
  // ** Methods to implement **
  // **************************

  // Verify Order
  public abstract void checkOrder(String orderId);

  // Create DIP Object by orgUnit
  public abstract List<DisseminationInfoPackage> createDipPackages(Order order);

  // Transform AIPs => DIPs
  public abstract String generateDipPackages(Order order);

  // Get status of Information Package
  public abstract OrderStatus getStatus(String orderId, URI infoPackage);

  // Create DIP packages
  public abstract String submitOrder(Order order) throws JAXBException, IOException;

  // *********************
  // ** Default Methods **
  // *********************

  // Get working directory for Order
  public Path getOrderFolder(String orderId) {
    return Paths.get(this.orderLocation).resolve(orderId);
  }

  // Get working directory for DIP
  public Path getDipFolder(String orderId, String orgUnitId, Access accessLevel) {
    return Paths.get(this.orderLocation).resolve(orderId).resolve(orgUnitId).resolve(accessLevel.name());
  }

  // Get URI of DIP
  public URI getDipUri(String orderId, String orgUnitId, Access accessLevel, String dipId) {
    return Paths.get(this.orderLocation).resolve(orderId).resolve(orgUnitId).resolve(accessLevel.name())
            .resolve(dipId + SolidifyConstants.ZIP_EXT).toUri();
  }

  // Get AIP list sorted by OrgUnit
  protected Map<String, List<ArchivalInfoPackage>> getAipByOrgUnit(Order order) {
    List<ArchivalInfoPackage> aips = order.getAipPackages();
    return aips.stream().collect(Collectors.groupingBy(a -> a.getInfo().getOrganizationalUnitId()));
  }

  // Find DIP id on file system
  protected String findDipId(String orderId, String orgUnitId, Access accessLevel) {
    List<Path> dipList = FileTool.scanFolder(this.getOrderFolder(orderId).resolve(orgUnitId).resolve(accessLevel.name()), Files::isRegularFile);
    if (dipList.size() != 1) {
      throw new SolidifyCheckingException(this.messageService.get("checking.order.dipnumber", new Object[] { dipList.size() }));
    }
    String dipFileName = dipList.get(0).getFileName().toString();
    return dipFileName.substring(0, dipFileName.lastIndexOf(SolidifyConstants.ZIP_EXT));
  }

}

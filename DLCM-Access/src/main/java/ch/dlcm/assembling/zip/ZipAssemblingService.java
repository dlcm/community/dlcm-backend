/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - ZipAssemblingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.assembling.zip;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import jakarta.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.assembling.AssemblingService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.model.Access;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.access.QueryType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.service.DipStatusService;
import ch.dlcm.service.dissemination.DipBuilderService;
import ch.dlcm.service.dissemination.DisseminationPolicyProvider;
import ch.dlcm.service.rest.trusted.TrustedDisseminationPolicyRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class ZipAssemblingService extends AssemblingService {

  private static final Logger log = LoggerFactory.getLogger(ZipAssemblingService.class);

  private final TrustedDisseminationPolicyRemoteResourceService disseminationPolicyRemoteResourceService;

  private final DipStatusService dipStatusService;

  private final DisseminationPolicyProvider disseminationPolicyProvider;

  public ZipAssemblingService(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          DipStatusService dipStatusService,
          DisseminationPolicyProvider disseminationPolicyProvider,
          TrustedDisseminationPolicyRemoteResourceService disseminationPolicyRemoteResourceService) {
    super(dlcmProperties, messageService);
    this.dipStatusService = dipStatusService;
    this.disseminationPolicyProvider = disseminationPolicyProvider;
    this.disseminationPolicyRemoteResourceService = disseminationPolicyRemoteResourceService;
  }

  @Override
  public void checkOrder(String orderId) {
    // Check if Order exists
    if (!FileTool.checkFile(this.getOrderFolder(orderId))) {
      throw new SolidifyCheckingException(
              this.messageService.get("checking.order.notfound", new Object[] { this.getOrderFolder(orderId).toString() }));
    }
    // List Organizational Units
    for (final Path orgUnitPath : FileTool.scanFolder(this.getOrderFolder(orderId), Files::isDirectory)) {
      String orgUnitId = orgUnitPath.getFileName().toString();
      // List access levels
      for (final Path accessPath : FileTool.scanFolder(orgUnitPath, Files::isDirectory)) {
        Access accessLevel = Access.valueOf(accessPath.getFileName().toString());
        String dipId = this.findDipId(orderId, orgUnitId, accessLevel);
        URI dipURI = this.getDipUri(orderId, orgUnitId, accessLevel, dipId);
        // Check if Order archive exists
        if (!FileTool.checkFile(dipURI)) {
          throw new SolidifyCheckingException(this.messageService.get("checking.order.noarchive", new Object[] { dipURI }));
        }
        // Check order archive
        final ZipTool zipTool = new ZipTool(dipURI);
        if (!zipTool.checkZip()) {
          throw new SolidifyCheckingException(this.messageService.get("checking.zip.notvalid", new Object[] { dipURI }));
        }
      }
    }
  }

  @Override
  public List<DisseminationInfoPackage> createDipPackages(Order order) {
    final List<DisseminationInfoPackage> dipList = new ArrayList<>();
    int count = 0;
    // Sort AIP by orgUnit
    final Map<String, List<ArchivalInfoPackage>> aipsByOrgUnit = this.getAipByOrgUnit(order);
    // For orgUnits
    for (final Entry<String, List<ArchivalInfoPackage>> entryByOrgUnit : aipsByOrgUnit.entrySet()) {
      final String orgUnitId = entryByOrgUnit.getKey();
      final List<ArchivalInfoPackage> aipList = entryByOrgUnit.getValue();
      // Sort AIP by access
      final Map<Access, List<ArchivalInfoPackage>> aipsByAccess = aipList.stream().collect(Collectors.groupingBy(a -> a.getInfo().getAccess()));
      for (final Entry<Access, List<ArchivalInfoPackage>> entryByAccess : aipsByAccess.entrySet()) {
        count++;
        final Access access = entryByAccess.getKey();
        final String dipId = this.findDipId(order.getResId(), orgUnitId, access);
        final List<ArchivalInfoPackage> aip = entryByAccess.getValue();
        // Determinate DIP name
        String name = order.getName();
        if (!order.getQueryType().equals(QueryType.DIRECT)) {
          name += " #" + count;
        }
        final DisseminationInfoPackage dip = this.dipStatusService.createDIP(dipId, orgUnitId, name, access, aip,
                this.getDipUri(order.getResId(), orgUnitId, access, dipId));
        if (dip != null) {
          dipList.add(dip);
        }
      }
    }
    return dipList;
  }

  @Override
  public String generateDipPackages(Order order) {
    if (order.getDisseminationPolicyId() == null) {
      throw new SolidifyCheckingException(this.messageService.get("access.order.error.dissemination_policy_notexist"));
    }
    // check if order compliance ex: for IIIF,HEDERA don't allow multiple orgunits
    this.checkOrderCompliance(order);

    // Sort AIP by orgUnit
    final Map<String, List<ArchivalInfoPackage>> aipsByOrgUnit = this.getAipByOrgUnit(order);

    for (final Entry<String, List<ArchivalInfoPackage>> entryByOrgUnit : aipsByOrgUnit.entrySet()) {
      final String orgUnitId = entryByOrgUnit.getKey();
      final DipBuilderService dipBuilderService = this.disseminationPolicyProvider.getDisseminationPolicyEnforcementService(order, orgUnitId);
      final List<ArchivalInfoPackage> aipList = entryByOrgUnit.getValue();
      // Sort AIP by access
      final Map<Access, List<ArchivalInfoPackage>> aipsByAccess = aipList.stream().collect(Collectors.groupingBy(a -> a.getInfo().getAccess()));
      for (final Entry<Access, List<ArchivalInfoPackage>> entryByAccess : aipsByAccess.entrySet()) {
        final Access access = entryByAccess.getKey();
        final List<ArchivalInfoPackage> aip = entryByAccess.getValue();
        if (!dipBuilderService.buildDip(this.getDipFolder(order.getResId(), orgUnitId, access), aip, order)) {
          return null;
        }
      }
    }
    return order.getResId();
  }

  @Override
  public OrderStatus getStatus(String orderId, URI infoPackage) {
    try {
      this.checkOrder(orderId);
    } catch (final SolidifyCheckingException e) {
      return OrderStatus.IN_PROGRESS;
    }
    return OrderStatus.READY;
  }

  @Override
  public String submitOrder(Order order) throws JAXBException, IOException {
    // List Organizational Unit
    for (final Path orgUnitPath : FileTool.scanFolder(this.getOrderFolder(order.getResId()), Files::isDirectory)) {
      String orgUnitId = orgUnitPath.getFileName().toString();
      final DipBuilderService dipBuilderService = this.disseminationPolicyProvider.getDisseminationPolicyEnforcementService(order, orgUnitId);
      // List access level
      for (Path accessPath : FileTool.scanFolder(orgUnitPath, Files::isDirectory)) {
        Access accessLevel = Access.valueOf(accessPath.getFileName().toString());
        // Generate DIP iD
        String dipId = StringTool.generateResId();
        // Generate DIP zip
        if (!dipBuilderService.createDipArchive(this.orderLocation, order, dipId, orgUnitId, accessLevel, accessPath,
                this.getDipFolder(order.getResId(), orgUnitId, accessLevel))) {
          return null;
        }
      }
    }
    return order.getResId();
  }

  private void checkOrderCompliance(Order order) {
    DisseminationPolicy disseminationPolicy = this.disseminationPolicyRemoteResourceService.findOne(order.getDisseminationPolicyId());
    if ((disseminationPolicy.getType() == DisseminationPolicy.DisseminationPolicyType.IIIF
            || disseminationPolicy.getType() == DisseminationPolicy.DisseminationPolicyType.HEDERA)
            && !this.aipsAreFromSameOrgUnit(order.getAipPackages())) {
      log.error("Downloading multiple AIPs from different organization units is not allowed for IIIF and HEDERA, order: {}", order.getName());
      throw new SolidifyRuntimeException("Downloading multiple AIPs from different organization units is not allowed for IIIF and HEDERA");
    }
  }

  private boolean aipsAreFromSameOrgUnit(List<ArchivalInfoPackage> aips) {
    if (aips == null || aips.isEmpty()) {
      return true;
    }
    // Use the first element's ID as the reference.
    String commonId = aips.get(0).getOrganizationalUnitId();
    // allMatch stops checking as soon as it finds a mismatch.
    return aips.stream().allMatch(aip -> aip.getOrganizationalUnitId().equals(commonId));
  }

}

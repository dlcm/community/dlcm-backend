/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OrderService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import static ch.dlcm.model.access.Order.OrderStatus.SUBMITTED;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.transform.TransformerFactoryConfigurationError;

import jakarta.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.unit.DataSize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.HashTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.AipDownloadMessage;
import ch.dlcm.model.MetadataVisibility;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.access.OrderArchive;
import ch.dlcm.model.access.OrderSubsetItem;
import ch.dlcm.model.access.QueryType;
import ch.dlcm.model.dto.DisseminationPolicyDto;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.repository.OrderRepository;
import ch.dlcm.repository.OrderSubsetItemRepository;
import ch.dlcm.service.SearchMgmt;
import ch.dlcm.service.rest.trusted.TrustedDisseminationPolicyRemoteResourceService;
import ch.dlcm.specification.OrderSpecification;

@Service
@ConditionalOnBean(AccessController.class)
public class OrderService extends ResourceService<Order> {
  private static final Logger log = LoggerFactory.getLogger(OrderService.class);

  private final DataSize fileSizeLimit;
  private final String orderLocation;
  private final SearchMgmt searchMgmt;
  private final ChecksumAlgorithm defaultChecksumAlgorithm;
  private final ArchivalInfoPackageService aipService;
  private final OrderSubsetItemRepository orderSubsetItemRepository;
  private final TrustedDisseminationPolicyRemoteResourceService disseminationPolicyRemoteResourceService;

  public OrderService(DLCMProperties dlcmProperties,
          SearchMgmt searchMgmt,
          ArchivalInfoPackageService aipService,
          OrderSubsetItemRepository orderSubsetItemRepository,
          TrustedDisseminationPolicyRemoteResourceService disseminationPolicyRemoteResourceService) {
    this.searchMgmt = searchMgmt;
    this.aipService = aipService;
    this.orderSubsetItemRepository = orderSubsetItemRepository;
    this.disseminationPolicyRemoteResourceService = disseminationPolicyRemoteResourceService;
    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit();
    this.orderLocation = dlcmProperties.getOrderLocation();
    this.defaultChecksumAlgorithm = ChecksumAlgorithm.valueOf(dlcmProperties.getParameters().getDefaultChecksum());
  }

  public Order saveOrder(Order order) {
    RestCollection<OrderArchive> orderArchiveCollection;
    Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    do {
      orderArchiveCollection = this.searchMgmt.search(MetadataVisibility.PUBLIC, order, true, pageable);
      for (final OrderArchive orderArchive : orderArchiveCollection.getData()) {
        final ArchivalInfoPackage archive = orderArchive.getArchive();
        if (archive.getInfo().getStatus() != PackageStatus.COMPLETED &&
                archive.getInfo().getStatus() != PackageStatus.DISPOSED) {
          log.warn("AIP ({}) not ready for Access module", archive.getResId());
          continue;
        }
        if (!this.aipService.existsById(archive.getResId())) {

          boolean isBigPackage = archive.getArchiveSize() != null && archive.getArchiveSize() > this.fileSizeLimit.toBytes();

          archive.reset();
          this.aipService.saveForOrders(archive);
          SolidifyEventPublisher.getPublisher().publishEvent(new AipDownloadMessage(archive.getResId(), isBigPackage));
        }
        order.addItem(archive);
      }
      pageable = pageable.next();
    } while (orderArchiveCollection.getPage().hasNext());
    order = this.save(order);

    order.add(linkTo(this.getClass()).slash(order.getResId()).slash(ActionName.SAVE).withSelfRel());
    order.add(linkTo(this.getClass()).slash(order.getResId()).withRel(ActionName.PARENT));
    return order;
  }

  public HttpEntity<Result> changeOrderStatus(OrderStatus newStatus, String orderId) {

    final Order order = this.findOne(orderId);

    final Result res = new Result(orderId);
    if (this.statusUpdateIsValid(order, newStatus)) {
      order.setStatus(newStatus);
      this.save(order);
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.order.status"));
      return new ResponseEntity<>(res, HttpStatus.OK);
    } else {
      res.setStatus(Result.ActionStatus.NON_APPLICABLE);
      res.setMesssage(this.messageService.get("message.order.status.error"));
      return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
    }
  }

  @Override
  public void delete(String orderId) {
    super.delete(orderId);
    final Path orderPath = Paths.get(this.orderLocation).resolve(orderId);
    if (FileTool.isFolder(orderPath) && !FileTool.deleteFolder(orderPath)) {
      throw new SolidifyFileDeleteException(orderPath.toString());
    }
  }

  /**
   * Check if an order status can be manually updated to the new given status, according to the order's management workflow.
   *
   * @param order The order to check
   * @param newStatus The new status to apply
   * @return True if the status update is valid
   */
  public boolean statusUpdateIsValid(Order order, Order.OrderStatus newStatus) {

    final Order.OrderStatus currentStatus = order.getStatus();

    return switch (currentStatus) {
      case IN_PROGRESS -> newStatus == OrderStatus.SUBMITTED;
      case IN_ERROR -> newStatus == OrderStatus.IN_PROGRESS;
      default -> false;
    };
  }

  public Order createOrderForAIP(String aipId, DisseminationPolicyDto disseminationPolicy) {
    final Order order = new Order();
    List<OrderSubsetItem> subsetItemList = disseminationPolicy.getSubsetItemList();
    order.setName(aipId);
    order.setQueryType(QueryType.DIRECT);
    order.setQuery(aipId);
    order.setDisseminationPolicyId(disseminationPolicy.getDisseminationPolicyId());
    order.setOrganizationalUnitDisseminationPolicyId(disseminationPolicy.getOrganizationalUnitDisseminationPolicyId());
    if (subsetItemList == null) { // if no subsetItemList is provided, set it to an empty list
      subsetItemList = List.of();
    }
    order.setSubitemsChecksum(this.calculateChecksum(aipId, subsetItemList)); // calculate and set checksum
    final Order savedOrder = this.save(order);
    subsetItemList.forEach(subsetItem -> {
      subsetItem.setOrder(savedOrder);
      subsetItem.setCreatedBy(savedOrder.getCreatedBy());
      subsetItem.setUpdatedBy(savedOrder.getUpdatedBy());
    });
    this.orderSubsetItemRepository.saveAll(subsetItemList);

    return savedOrder;
  }

  public void submitOrder(Order order) {
    this.saveOrder(order);
    this.changeOrderStatus(SUBMITTED, order.getResId());
  }

  @Transactional
  public void recreateOrder(Order order) {
    DisseminationPolicyDto dto = new DisseminationPolicyDto();
    dto.setDisseminationPolicyId(order.getDisseminationPolicyId());
    if (order.getOrganizationalUnitDisseminationPolicyId() != null) {
      dto.setOrganizationalUnitDisseminationPolicyId(order.getOrganizationalUnitDisseminationPolicyId());
    }
    dto.setSubsetItemList(List.of());
    Order order1 = this.createOrderForAIP(order.getName(), dto);
    this.submitOrder(order1);
  }

  public List<Order> findByAip(String aipId) {
    return ((OrderRepository) this.itemRepository).findByAip(aipId);
  }

  public DisseminationPolicy findDisseminationPolicyByOrderId(String orderId) {
    Order order = this.findOne(orderId);
    return this.disseminationPolicyRemoteResourceService.findOne(order.getDisseminationPolicyId());
  }

  @Override
  protected void validateItemSpecificRules(Order order, BindingResult errors) {

    if (StringTool.isNullOrEmpty(order.getQuery())) {
      errors.addError(new FieldError(order.getClass().getSimpleName(), "query", this.messageService.get("validation.order.query.empty")));
    }

    // Check disseminationPolicyId exists
    try {
      this.disseminationPolicyRemoteResourceService.findOne(order.getDisseminationPolicyId());
    } catch (SolidifyRuntimeException e) {
      errors.addError(new FieldError(order.getClass().getSimpleName(), "disseminationPolicyId",
              this.messageService.get("validation.order.disseminationPolicyIdNotValid")));
    }

    switch (order.getQueryType()) {
      case ADVANCED -> {
        // TODO: implement advanced query
      }
      case DIRECT -> {
        // Check if AIP number
        this.checkAipNumber(order, errors, 1);
        // Check order integrity
        if (!StringTool.isNullOrEmpty(order.getQuery())) {
          if (!order.getQuery().equals(order.getName())) {
            errors.addError(new FieldError(order.getClass().getSimpleName(), "query",
                    this.messageService.get("validation.order.nameAndQuery.notMatch")));
          }
          if (order.getStatus().equals(OrderStatus.SUBMITTED)) {
            if (order.getAipPackages().isEmpty()) {
              errors.addError(new FieldError(order.getClass().getSimpleName(), "aipPackages",
                      this.messageService.get("validation.order.submittedWithoutAip")));
            } else if (!order.getQuery().equals(order.getAipPackages().get(0).getResId())) {
              errors.addError(new FieldError(order.getClass().getSimpleName(), "query",
                      this.messageService.get("validation.order.direct.invalidName", new Object[] { order.getName(), order.getQuery() })));
            }
          }
        }
        // Check sub items list is empty for OAIS
        if (order.getDisseminationPolicyId().equals(DLCMConstants.DISSEMINATION_POLICY_OAIS_ID)
                && (order.getSubsetItems() != null && !order.getSubsetItems().isEmpty())) {
          errors.addError(new FieldError(order.getClass().getSimpleName(), "subsetItems",
                  this.messageService.get("validation.order.direct.subsetItems.notSupported")));
        }
        // Check
        if (!order.getDisseminationPolicyId().equals(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID)
                && !order.getDisseminationPolicyId().equals(DLCMConstants.DISSEMINATION_POLICY_OAIS_ID)
                && order.getOrganizationalUnitDisseminationPolicyId() == null) {
          errors.addError(new FieldError(order.getClass().getSimpleName(), "organizationalUnitDisseminationPolicyId",
                  this.messageService.get("validation.order.direct.specificDissemincationPolicy.missing")));
        }
      }
      case SIMPLE -> {
        // Check if AIP number
        this.checkAipNumber(order, errors, order.getQuery().split(SolidifyConstants.FIELD_SEP).length);
        // Check dissemination policy
        if (!order.getDisseminationPolicyId().equals(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID)
                && !order.getDisseminationPolicyId().equals(DLCMConstants.DISSEMINATION_POLICY_OAIS_ID)) {
          errors.addError(new FieldError(order.getClass().getSimpleName(), "disseminationPolicyId",
                  this.messageService.get("validation.order.simple.disseminationPolicy.notCompatible",
                          new Object[] { order.getDisseminationPolicyId() })));
        }
      }
      default -> {
        errors.addError(new FieldError(order.getClass().getSimpleName(), "queryType",
                this.messageService.get("validation.order.invalidType", new Object[] { order.getQueryType() })));
      }
    }
  }

  private void checkAipNumber(Order order, BindingResult errors, int expectedNumber) {
    if (!StringTool.isNullOrEmpty(order.getQuery())) {
      int aipNumber = order.getQuery().split(SolidifyConstants.FIELD_SEP).length;
      if (aipNumber != expectedNumber) {
        errors.addError(new FieldError(order.getClass().getSimpleName(), "query",
                this.messageService.get("validation.order.wrongArchiveNumber", new Object[] { expectedNumber, aipNumber })));
      }
      if (order.getStatus().equals(OrderStatus.SUBMITTED) && order.getAipPackages().size() != expectedNumber) {
        errors.addError(new FieldError(order.getClass().getSimpleName(), "query",
                this.messageService.get("validation.order.wrongArchiveNumber", new Object[] { expectedNumber, order.getAipPackages().size() })));
      }
    }
  }

  /**
   * @return the cumulated size of all AIP contained in the order
   */
  public long getSize(String resId) {
    return ((OrderRepository) this.itemRepository).getSize(resId);
  }

  @Override
  public OrderSpecification getSpecification(Order resource) {
    return new OrderSpecification(resource);
  }

  public List<Order> findByArchiveAndDisseminationPolicy(String aipId, DisseminationPolicyDto disseminationPolicyDto) {
    return ((OrderRepository) this.itemRepository)
            .findByQueryTypeAndQueryAndDisseminationPolicyIdAndOrganizationalUnitDisseminationPolicyIdAndSubitemsChecksum(
                    QueryType.DIRECT,
                    aipId,
                    disseminationPolicyDto.getDisseminationPolicyId(),
                    disseminationPolicyDto.getOrganizationalUnitDisseminationPolicyId(),
                    this.calculateChecksum(aipId, disseminationPolicyDto.getSubsetItemList()));
  }

  private String calculateChecksum(String aipId, List<OrderSubsetItem> subsetItems) {
    String content = aipId;
    if (subsetItems != null && !subsetItems.isEmpty()) {
      // Create a string combining all paths, sorted by path to ensure consistent checksum
      content += subsetItems.stream()
              .map(OrderSubsetItem::getItemPath)
              .sorted()
              .collect(Collectors.joining(""));
    }
    try {
      return HashTool.hash(this.defaultChecksumAlgorithm.toString(), content.getBytes(StandardCharsets.UTF_8));
    } catch (TransformerFactoryConfigurationError | NoSuchAlgorithmException e) {
      throw new SolidifyProcessingException(e.getMessage(), e);
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - DisseminationInfoPackageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.Objects;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.HttpRequestInfoProvider;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AccessController;
import ch.dlcm.message.DipMessage;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.repository.DisseminationInfoPackageRepository;
import ch.dlcm.service.HistoryService;
import ch.dlcm.specification.DisseminationInfoPackageSpecification;

@Service
@ConditionalOnBean(AccessController.class)
public class DisseminationInfoPackageService extends InfoPackageService<DisseminationInfoPackage> {

  public DisseminationInfoPackageService(DLCMProperties dlcmProperties,
          HttpRequestInfoProvider httpRequestInfoProvider,
          SpecificationPermissionsFilter<DisseminationInfoPackage> specificationPermissionFilter, HistoryService historyService) {
    super(dlcmProperties, httpRequestInfoProvider, specificationPermissionFilter, historyService);
  }

  @Override
  protected Specification<DisseminationInfoPackage> addSpecificationFilter(Specification<DisseminationInfoPackage> spec) {
    return this.specificationPermissionFilter.addPermissionsFilter(spec, this.httpRequestInfoProvider.getPrincipal());
  }

  public Page<DisseminationInfoPackage> findDipContainingAip(String aipId, Pageable pageable) {
    return ((DisseminationInfoPackageRepository) this.itemRepository).findDipContainingAip(aipId, pageable);
  }

  public boolean putPackageInProcessingQueue(DisseminationInfoPackage dip) {
    if (Objects.requireNonNull(dip.getInfo().getStatus()) == PackageStatus.IN_PREPARATION) {
      boolean isBigPackage = this.getSize(dip.getResId()) > this.fileSizeLimit.toBytes();
      SolidifyEventPublisher.getPublisher().publishEvent(new DipMessage(dip.getResId(), isBigPackage));
      return true;
    }
    return false;
  }

  public long getSize(String dipId) {
    return ((DisseminationInfoPackageRepository) this.itemRepository).getSize(dipId);
  }

  public Result resumePackage(String dipId) {
    DisseminationInfoPackage dip = this.findOne(dipId);
    Result result = super.savePackageResumeStatus(dip);
    if (result.getStatus() == Result.ActionStatus.EXECUTED) {
      this.putPackageInProcessingQueue(dip);
    }
    return result;
  }

  public int countDIPOfCompletedOrders() {
    return ((DisseminationInfoPackageRepository) this.itemRepository).countDIPOfCompletedOrders();
  }

  @Override
  public DisseminationInfoPackageSpecification getSpecification(DisseminationInfoPackage resource) {
    return new DisseminationInfoPackageSpecification(resource);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - AccessPermissionEvaluatorTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test;

import static ch.dlcm.controller.DLCMControllerAction.DELETE;
import static ch.dlcm.controller.DLCMControllerAction.DOWNLOAD_ARCHIVE;
import static ch.dlcm.controller.DLCMControllerAction.GET;
import static ch.dlcm.controller.DLCMControllerAction.LIST_FILES;
import static ch.dlcm.controller.DLCMControllerAction.PREPARE_DOWNLOAD_ARCHIVE;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.service.security.MetadataPermissionService;

@ExtendWith(SpringExtension.class)
class AccessPermissionEvaluatorTest extends AbstractPermissionEvaluatorTest {

  private final Map<Authentication, List<DLCMControllerAction>> mapAllowedActionsMetadataPublic = new LinkedHashMap<>();

  private MetadataPermissionService metadataPermissionService;


  @Override
  @BeforeEach
  void setUp() {
    super.setUp();

    // Set the map of allowed actions, valid with a Metadata
    this.mapAllowedActionsMetadataPublic.put(this.authManager, Arrays.asList(DOWNLOAD_ARCHIVE, PREPARE_DOWNLOAD_ARCHIVE, LIST_FILES, GET));
    this.mapAllowedActionsMetadataPublic.put(this.authApprover, Arrays.asList(DOWNLOAD_ARCHIVE, PREPARE_DOWNLOAD_ARCHIVE, LIST_FILES, GET));
    this.mapAllowedActionsMetadataPublic.put(this.authSteward, Arrays.asList(DOWNLOAD_ARCHIVE, PREPARE_DOWNLOAD_ARCHIVE, LIST_FILES, GET));
    this.mapAllowedActionsMetadataPublic.put(this.authCreator, Arrays.asList(DOWNLOAD_ARCHIVE, PREPARE_DOWNLOAD_ARCHIVE, LIST_FILES, GET));
    this.mapAllowedActionsMetadataPublic.put(this.authVisitor, Arrays.asList(DOWNLOAD_ARCHIVE, PREPARE_DOWNLOAD_ARCHIVE, LIST_FILES, GET));

    // Construct the permissions service
    this.metadataPermissionService = new MetadataPermissionService(this.searchService, this.trustedPersonRemoteResourceService,
            this.mfaPermissionService);
  }

  @Test
  void metadataAccessLevelPublicAllowedActions() {
    this.allowedActionsTest(this.mapAllowedActionsMetadataPublic, this.metadataPermissionService);
  }

  @Test
  void metadataAccessLevelPublicForbiddenActions() {
    this.forbiddenActionsTest(this.mapAllowedActionsMetadataPublic, this.metadataPermissionService);
  }

  @Test
  void wrongAction() {
    this.setSecurityContextAuthentication(this.authManager);
    assertThrows(SolidifyRuntimeException.class, () -> this.metadataPermissionService.isAllowed(AbstractPermissionEvaluatorTest.EXISTENT_ID,
            AbstractPermissionEvaluatorTest.INEXISTENT_ACTION));
  }

  @Test
  void wrongResId() {
    this.setSecurityContextAuthentication(this.authManager);
    final String deleteActionString = DELETE.toString();
    assertThrows(NoSuchElementException.class, () -> this.metadataPermissionService.isAllowed(AbstractPermissionEvaluatorTest.INEXISTENT_ID,
            deleteActionString));
  }

  @Test
  void metadataAccessLevelClosedForbiddenActions() {
    // Not authorized actions for closed metadata
    for (final Map.Entry<Authentication, List<DLCMControllerAction>> allowedActions : this.mapAllowedActionsMetadataPublic.entrySet()) {
      this.setSecurityContextAuthentication(allowedActions.getKey());
      for (final DLCMControllerAction action : this.getActionsNotInList(allowedActions.getValue())) {
        Assertions.assertFalse(
                this.metadataPermissionService.isAllowed(AbstractPermissionEvaluatorTest.CLOSED_ARCHIVE_METADATA_ID, action.toString()));
      }
    }
  }

  @Test
  void metadataAccessLevelRestrictedForbiddenActions() {
    // Not authorized actions for closed metadata
    for (final Map.Entry<Authentication, List<DLCMControllerAction>> allowedActions : this.mapAllowedActionsMetadataPublic.entrySet()) {
      this.setSecurityContextAuthentication(allowedActions.getKey());
      for (final DLCMControllerAction action : this.getActionsNotInList(allowedActions.getValue())) {
        Assertions.assertFalse(
                this.metadataPermissionService.isAllowed(AbstractPermissionEvaluatorTest.RESTRICTED_ARCHIVE_METADATA_ID, action.toString()));
      }
    }
  }

}

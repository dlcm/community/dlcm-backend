/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - OaiPmhTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.access;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.JAXBException;

import org.mockito.Mock;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.oai.OAIMetadataPrefix;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.OAIService;
import ch.unige.solidify.test.oai.OAIServiceTest;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.IdentifierService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.SearchMgmt;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchiveTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLanguageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLicenseRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackMetadataTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPersonRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPreservationPolicyRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;

@ActiveProfiles({ "sec-noauth" })
public class OaiPmhTest extends OAIServiceTest {

  protected DLCMProperties dlcmProperties;
  protected DLCMRepositoryDescription repositoryDescription;
  protected MetadataService metadataService;
  protected SearchMgmt searchMgmt;
  protected IdentifierService identifierService;

  @Mock
  protected HistoryService historyService;
  @Mock
  protected FallbackLanguageRemoteResourceService languageResourceService;
  @Mock
  protected FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService;
  @Mock
  protected FallbackArchivalInfoPackageRemoteResourceService fallbackAipRemoteResourceService;
  @Mock
  protected FallbackMetadataTypeRemoteResourceService metadataTypeResourceService;
  @Mock
  protected FallbackPersonRemoteResourceService personRemoteResourceService;
  @Mock
  protected FallbackLicenseRemoteResourceService licenseRemoteResourceService;
  @Mock
  protected FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteService;
  @Mock
  protected FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteService;
  @Mock
  protected FallbackSubmissionInfoPackageRemoteResourceService fallbackSubmissionInfoPackageRemoteResourceService;
  @Mock
  protected FallbackPreservationPolicyRemoteResourceService preservationPolicyRemoteResourceService;
  @Mock
  protected FallbackArchiveTypeRemoteResourceService fallbackArchiveTypeResourceService;
  @Mock
  protected TrustedArchivalInfoPackageRemoteResourceService trustedAipRemoteResourceService;
  @Mock
  protected FileFormatService fileFormatService;
  @Mock
  protected TrustedDepositRemoteResourceService trustedDepositRemoteService;

  @Override
  protected void localSetup() {
    this.dlcmProperties = new DLCMProperties(new SolidifyProperties(new GitInfoProperties()), new MultipartProperties());
    this.repositoryDescription = new DLCMRepositoryDescription(this.gitInfoProperties);
    this.repositoryDescription.setArchiveHomePage(HOST_URL + "/");
    // Metadata Service
    this.metadataService = new MetadataService(
            this.languageResourceService,
            this.orgUnitResourceService,
            this.fallbackAipRemoteResourceService,
            this.historyService,
            this.dlcmProperties,
            this.repositoryDescription,
            this.messageService,
            this.gitInfoProperties,
            this.fileFormatService,
            this.metadataTypeResourceService,
            this.personRemoteResourceService,
            this.licenseRemoteResourceService,
            this.archivePublicMetadataRemoteService,
            this.archivePrivateMetadataRemoteService,
            this.preservationPolicyRemoteResourceService,
            this.fallbackArchiveTypeResourceService,
            this.trustedDepositRemoteService);
    // Search Service
    this.searchMgmt = new SearchMgmt(
            this.messageService,
            this.trustedAipRemoteResourceService,
            this.archivePublicMetadataRemoteService,
            this.archivePrivateMetadataRemoteService,
            this.dlcmProperties);
    // Git Info Service
    this.gitInfoProperties.getBuild().setVersion("1.x");
    this.identifierService = new IdentifierService(this.repositoryDescription);
    // OAI Service
    this.oaiService = new OAIService(
            this.oaiConfig,
            this.repositoryDescription,
            this.oaiMetadataPrefixService,
            this.oaiSetService,
            this.searchMgmt,
            this.archivePublicMetadataRemoteService,
            this.metadataService);

    // Mock DLCMRestClientService
    try {
      for (DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
        ArchiveMetadata archiveMetadata = this.getMetadata(version);
        when(this.archivePublicMetadataRemoteService.getRecord(version.getVersion())).thenReturn(archiveMetadata);
      }
      RestCollection<ArchiveMetadata> archiveMetadataList = this.getArchiveMetadataList();
      when(this.archivePublicMetadataRemoteService.getIndexMetadataList(any(String.class), any(String.class),
              any(Pageable.class))).thenReturn(archiveMetadataList);
      when(this.archivePublicMetadataRemoteService.getMetadataList(any(), any(String.class), any(), any(),
              any(), any(), any(Pageable.class)))
                      .thenReturn(archiveMetadataList);
    } catch (IOException | JAXBException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  @Override
  protected String[] getMetadataList() {
    return new String[] { OAIConstants.OAI_DC, DLCMConstants.OAI_DATACITE };
  }

  private ArchiveMetadata getMetadata(DLCMMetadataVersion version) throws IOException, JAXBException {
    ArchiveMetadata archiveMetadata = new ArchiveMetadata();
    archiveMetadata.setIndex(this.dlcmProperties.getIndexing().getIndexName());
    archiveMetadata.setResId(version.getVersion());
    archiveMetadata.getMetadata().put(DLCMConstants.AIP_LAST_UPDATE, OffsetDateTime.now().toString());
    Path xml = this.getPath("dlcm_mets-" + version.getVersion() + SolidifyConstants.XML_EXT);
    this.metadataService.completeArchiveIndex(version, archiveMetadata, xml);
    return archiveMetadata;
  }

  private RestCollection<ArchiveMetadata> getArchiveMetadataList() throws IOException, JAXBException {
    List<ArchiveMetadata> list = new ArrayList<>();
    for (DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      ArchiveMetadata archiveMetadata = this.getMetadata(version);
      list.add(archiveMetadata);
    }
    return new RestCollection<>(list);
  }

  @Override
  protected List<String> getIdentifierList() {
    List<String> list = new ArrayList<>();
    for (DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      list.add(version.getVersion());
    }
    return list;
  }

  @Override
  protected String getMetadataXmlTransformation() throws IOException {
    ClassPathResource xslResource = new ClassPathResource(SolidifyConstants.XSL_HOME + "/datacite2oai_dc.xsl");
    return FileTool.toString(xslResource.getInputStream());
  }

  @Override
  protected String getReferenceOAIMetadataPrefixName() {
    return DLCMConstants.OAI_DATACITE;
  }

  @Override
  protected OAIMetadataPrefix getReferenceOAIMetadataPrefix() throws MalformedURLException {
    OAIMetadataPrefix oaiMetadataPrefix = new OAIMetadataPrefix();
    oaiMetadataPrefix.setPrefix(DLCMConstants.OAI_DATACITE);
    oaiMetadataPrefix.setName(DLCMConstants.DATACITE_NAME);
    oaiMetadataPrefix.setReference(true);
    oaiMetadataPrefix.setSchemaUrl(new URL(DLCMConstants.DATACITE_SCHEMA_4_3));
    oaiMetadataPrefix.setSchemaNamespace(DLCMConstants.DATACITE_NAMESPACE_4);
    oaiMetadataPrefix.setEnabled(true);
    return oaiMetadataPrefix;
  }

}

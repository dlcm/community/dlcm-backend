/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Access - IiifDipBuilderServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test;

import ch.unige.solidify.exception.SolidifyCheckingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class IiifDipBuilderServiceTest {

    private Map<String, String> parametersMap;

    @BeforeEach
    void setUp() {
        parametersMap = new HashMap<>();
    }

    @Test
    void testPatternSplittingWithCommas() {
        parametersMap.put("mediaPattern", "*.jpg,*.png,*.txt, */filename.jpg");
        parametersMap.put("manifestPattern", "*/manifest.json,*/temp.json, */filename.xml");

        List<String> mediaPatterns = parsePatterns("mediaPattern");
        List<String> manifestPatterns = parsePatterns("manifestPattern");

        assertEquals(List.of("*.jpg", "*.png", "*.txt", "*/filename.jpg"), mediaPatterns);
        assertEquals(List.of("*/manifest.json", "*/temp.json", "*/filename.xml"), manifestPatterns);
    }

    @Test
    void testInvalidPatterns() {
        parametersMap.put("mediaPattern", "*.jpg,*.png,invalid[pattern");
        parametersMap.put("manifestPattern", "manifest.json,!temp.json,invalid[pattern");

        assertThrows(SolidifyCheckingException.class, () -> parsePatterns("mediaPattern"));
    }

    @Test
    void testEmptyPatterns() {
        parametersMap.put("mediaPattern", "");
        parametersMap.put("manifestPattern", "");

        assertThrows(SolidifyCheckingException.class, () -> parsePatterns("mediaPattern"));
    }

    private List<String> parsePatterns(String patternKey) {
        String mediaPattern = parametersMap.get(patternKey);
        return mediaPattern == null ? List.of() :
                Stream.of(mediaPattern.split(","))
                        .map(String::trim)
                        .peek(pattern -> {
                            if (!new org.springframework.util.AntPathMatcher().isPattern(pattern)) {
                                throw new SolidifyCheckingException("Invalid pattern: " + pattern);
                            }
                        })
                        .toList();
    }


}

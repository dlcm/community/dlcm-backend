/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - BasicReplicationFromMainStorageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.replication.basic;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.exception.DLCMReplicationException;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.AipService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;

@Profile("replication-basic-from-all-storage")
@Service
@ConditionalOnBean(PreservationPlanningController.class)
public class BasicReplicationFromAllStorageService extends BasicReplicationStorageService {

  private static final Logger log = LoggerFactory.getLogger(BasicReplicationFromAllStorageService.class);

  public BasicReplicationFromAllStorageService(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          AipService aipService,
          FallbackArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(dlcmProperties, messageService, aipService, aipRemoteResourceService);
  }

  /**
   * Find the golden replica from AIP list
   * A golden replica or copy is the most recent AIP. It could be a disposed AIP or an updated AIP by
   * extending retention.
   *
   * @param aipList
   * @param aipId
   * @return
   */
  @Override
  protected Entry<String, ArchivalInfoPackage> findGoldenCopy(Map<String, ArchivalInfoPackage> aipList, String aipId) {
    // Find more recent first copy ~ original copy
    final Optional<Entry<String, ArchivalInfoPackage>> sourceAip = aipList.entrySet()
            .stream()
            .filter(entry -> entry.getValue() != null)
            .sorted((e1, e2) -> (e2.getValue().getLastArchiving().compareTo(e1.getValue().getLastArchiving())))
            .findFirst();
    if (!sourceAip.isPresent()) {
      final String msg = "Could not find first copy of AIP (" + aipId + ")";
      log.error(msg);
      throw new DLCMReplicationException(msg);
    } else {
      return sourceAip.get();
    }
  }

}

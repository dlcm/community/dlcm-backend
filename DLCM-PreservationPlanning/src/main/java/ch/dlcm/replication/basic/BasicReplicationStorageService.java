/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - BasicReplicationStorageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.replication.basic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.exception.DLCMCompareException;
import ch.dlcm.exception.DLCMReplicationException;
import ch.dlcm.model.ChecksumCheck;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.replication.ReplicationService;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.AipService;
import ch.dlcm.service.DLCMService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;

public abstract class BasicReplicationStorageService extends DLCMService implements ReplicationService {
  private static final Logger log = LoggerFactory.getLogger(BasicReplicationStorageService.class);

  protected final String[] archivalStorageNodes;

  protected final AipService aipService;

  protected final FallbackArchivalInfoPackageRemoteResourceService aipRemoteResourceService;

  protected BasicReplicationStorageService(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          AipService aipService,
          FallbackArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(messageService, dlcmProperties);
    this.archivalStorageNodes = dlcmProperties.getArchivalStorageUrls();
    this.aipRemoteResourceService = aipRemoteResourceService;
    this.aipService = aipService;
  }

  protected abstract Entry<String, ArchivalInfoPackage> findGoldenCopy(Map<String, ArchivalInfoPackage> aipList, String aipId);

  /**
   * Check if a AIP is correctly replicated on all storage nodes
   */
  @Override
  public void checkAIP(String aipId) {
    // Load AIPs
    final Map<String, ArchivalInfoPackage> aipList = this.loadAIPs(aipId);
    // Find golden copy: latest AIP on main storage
    final Entry<String, ArchivalInfoPackage> sourceAip = this.findGoldenCopy(aipList, aipId);

    // Check if all copies are available
    aipList.entrySet()
            .stream()
            .filter(entry -> entry.getValue() == null)
            .forEach(entry -> {
              final String msg = "Missing copy of AIP (" + sourceAip.getValue().getResId() + ") on " + entry.getKey();
              throw new DLCMReplicationException(msg);
            });

    // Check if all copies are the same
    aipList.entrySet()
            .stream()
            .filter(entry -> entry.getValue() != null)
            .forEach(entry -> this.aipService.compare(sourceAip.getValue(), entry.getValue()));
  }

  /**
   * Replicate a AIP on all storage nodes
   */
  @Override
  public void replicateAIP(String aipId) {
    // Load AIPs
    final Map<String, ArchivalInfoPackage> aipList = this.loadAIPs(aipId);
    // Find golden copy: latest AIP
    final Entry<String, ArchivalInfoPackage> sourceAip = this.findGoldenCopy(aipList, aipId);
    // Test if missing a copy
    if (aipList.entrySet().stream().filter(entry -> entry.getValue() == null).count() == 0) {
      // Test if all copies already are up-to-date
      try {
        aipList.entrySet()
                .stream()
                .filter(entry -> entry.getValue() != null)
                .forEach(entry -> this.aipService.compare(sourceAip.getValue(), entry.getValue()));
        throw new SolidifyCheckingException("All AIP copies are up-to-date for " + aipId);
      } catch (DLCMCompareException e) {
        log.info("AIP replication: starting for {} because of {}", aipId, e.getMessage());
      }
    } else {
      log.info("AIP replication: starting for {} because of missing copies", aipId);
    }

    // Replace archive for all existing AIP
    aipList.entrySet()
            .stream()
            .filter(entry -> entry.getValue() != null)
            .forEach(entry -> {
              try {
                // Compare AIP with source
                this.aipService.compare(sourceAip.getValue(), entry.getValue());
              } catch (DLCMCompareException e) {
                // Check golden copy
                if (sourceAip.getValue().getLastArchiving().isBefore(entry.getValue().getLastArchiving())) {
                  final String msg = "Wrong golden copy of the AIP (" + sourceAip.getValue().getResId() + ")";
                  throw new DLCMReplicationException(msg, e);
                }
                // Replace AIP
                this.replaceAIPArchive(sourceAip.getValue(), sourceAip.getKey(), entry.getKey());
              } catch (SolidifyRestException | SolidifyResourceNotFoundException e) {
                final String msg = "Error in replicating tombstone AIP (" + sourceAip.getValue().getResId() + ") from " + sourceAip.getKey()
                        + " to " + entry.getKey();
                throw new DLCMReplicationException(msg, e);
              }
            });

    // Create a copy for nodes without a copy
    aipList.entrySet()
            .stream()
            .filter(entry -> entry.getValue() == null)
            .forEach(entry -> {
              try {
                this.copyAIP(sourceAip.getValue(), sourceAip.getKey(), entry.getKey());
              } catch (SolidifyRestException | SolidifyResourceNotFoundException e) {
                final String msg = "Error in replicating AIP (" + sourceAip.getValue().getResId() + ") from " + sourceAip.getKey() + " to "
                        + entry.getKey();
                throw new DLCMReplicationException(msg, e);
              }
            });
    log.info("AIP replication: completed for {}", aipId);
  }

  /**
   * Create a replica AIP on 'archivalStorage' node by copying AIP from 'masterNode' node
   *
   * @param aip
   * @param masterNode
   * @param archivalStorage
   */
  private void copyAIP(ArchivalInfoPackage aip, String masterNode, String archivalStorage) {
    final String downloadUrl = masterNode + "/" + ResourceName.AIP + "/" + aip.getResId() + "/" + ActionName.DOWNLOAD;
    // Reset AIP status
    aip.resetForReplication();
    // Re-initialize the AIP status and checksum checking infos for re-storing
    aip.getInfo().setStatus(PackageStatus.IN_PREPARATION);
    aip.setChecksumCheck(new ChecksumCheck());
    if (Boolean.FALSE.equals(aip.getArchivalUnit())) {
      final List<ArchivalInfoPackage> collection = this.aipRemoteResourceService.getAIPCollectionList(masterNode, aip.getResId());
      aip.setCollection(collection);
    }
    // Create replicated AIP on secondary node
    this.aipRemoteResourceService.createAIP(aip, downloadUrl, aip.getCollectionIds(), archivalStorage);
  }

  /**
   * Update a replica AIP on 'archivalStorage' node by replicating AIP packages from 'masterNode' node
   *
   * @param aip
   * @param masterNode
   * @param archivalStorage
   */
  private void replaceAIPArchive(ArchivalInfoPackage aip, String masterNode, String archivalStorage) {
    final String downloadUrl = masterNode + "/" + ResourceName.AIP + "/" + aip.getResId()
            + "/" + ActionName.DOWNLOAD;
    switch (aip.getInfo().getStatus()) {
      // Send AIP new package on secondary node
      case COMPLETED -> this.aipRemoteResourceService.replicateAIP(aip, downloadUrl, archivalStorage, DLCMActionName.REPLICATE_PACKAGE);
      // Send AIP tombstone on secondary node
      case DISPOSED -> this.aipRemoteResourceService.replicateAIP(aip, downloadUrl, archivalStorage, DLCMActionName.REPLICATE_TOMBSTONE);
      default -> throw new DLCMReplicationException("Wrong AIP status for replication: " + aip.getInfo().getStatus());
    }
  }

  /**
   * Load AIP replicas form all archival storage nodes
   *
   * @param aipId
   * @param allAip
   * @return
   */
  private Map<String, ArchivalInfoPackage> loadAIPs(String aipId) {
    // Check if replication enable
    if (this.archivalStorageNodes.length == 0) {
      throw new DLCMReplicationException(this.messageService.get("replication.disable"));
    }
    // Load AIP for each node
    final Map<String, ArchivalInfoPackage> aipList = new HashMap<>();
    for (final String archivalStorage : this.archivalStorageNodes) {
      try {
        // Load AIP
        final ArchivalInfoPackage aip = this.aipRemoteResourceService.getAIP(archivalStorage, aipId);
        aipList.put(archivalStorage, aip);
      } catch (final SolidifyResourceNotFoundException e) {
        aipList.put(archivalStorage, null);
      }
    }
    if (aipList.isEmpty()) {
      throw new DLCMReplicationException(this.messageService.get("replication.noaip", new Object[] { aipId }));
    }
    return aipList;
  }

}

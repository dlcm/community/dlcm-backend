/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - NoReplicationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.replication.none;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.replication.ReplicationService;
import ch.dlcm.service.DLCMService;

@Profile("replication-none")
@Service
@ConditionalOnBean(PreservationPlanningController.class)
public class NoReplicationService extends DLCMService implements ReplicationService {

  protected NoReplicationService(MessageService messageService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
  }

  @Override
  public void checkAIP(String aipId) {
    this.noReplication();
  }

  @Override
  public void replicateAIP(String aipId) {
    this.noReplication();
  }

  private void noReplication() {
    throw new SolidifyProcessingException("No replication configured");
  }
}

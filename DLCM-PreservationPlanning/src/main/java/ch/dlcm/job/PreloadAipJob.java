/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - PreloadAipJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedArchiveDownloadRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePublicMetadataRemoteResourceService;

public class PreloadAipJob extends AbstractJob {
  private final TrustedArchivePublicMetadataRemoteResourceService archiveMetadataRemoteResourceService;
  private final TrustedArchiveDownloadRemoteResourceService archiveDownloadRemoteResourceService;

  public PreloadAipJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          TrustedArchivePublicMetadataRemoteResourceService archiveMetadataRemoteResourceService,
          TrustedArchiveDownloadRemoteResourceService archiveDownloadRemoteResourceService) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.archiveMetadataRemoteResourceService = archiveMetadataRemoteResourceService;
    this.archiveDownloadRemoteResourceService = archiveDownloadRemoteResourceService;
  }

  @Override
  public void execute(PreservationJob job, String executionId) {
    Pageable pageable = this.getPageRequest(job.getMaxItems());
    RestCollection<ArchiveMetadata> collection;
    do {
      // List existing archives
      collection = this.archiveMetadataRemoteResourceService.getIndexMetadataList(pageable);
      pageable = pageable.next();
      for (final ArchiveMetadata archiveMetadata : collection.getData()) {
        try {
          Result res = switch (job.getJobType()) {
            case ARCHIVE_PRELOAD_SMALL -> this.preloadArchive(0L, this.fileSizeLimit.toBytes(), archiveMetadata);
            case ARCHIVE_PRELOAD_BIG -> this.preloadArchive(this.fileSizeLimit.toBytes(), Long.MAX_VALUE, archiveMetadata);
            default -> throw new SolidifyProcessingException("Job type " + job.getJobType().getLabel() + " not applicable");
          };
          job = this.logResult(res, job, executionId, archiveMetadata.getResId(), this.getArchivePublicUrl(archiveMetadata.getResId()));
        } catch (final Exception e) {
          job = this.addInErrorItem(job, executionId, archiveMetadata.getResId(), this.getArchivePublicUrl(archiveMetadata.getResId()),
                  e.getMessage());
        }
        // Save intermediate step by 10%
        job = this.jobProcessingService.saveStep(job, executionId, collection.getPage().getTotalItems());
      }
    } while (collection.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));
  }

  private Result preloadArchive(long minSize, long maxSize, ArchiveMetadata archiveMetadata) {
    Result result = new Result(archiveMetadata.getResId());
    // Check archive size
    final long archiveSize = archiveMetadata.getArchiveSize();
    if (archiveSize < minSize || archiveSize > maxSize) {
      result.setMesssage(this.messageService.get("job.aip.wrong_size",
              new Object[] { StringTool.formatSmartSize(archiveSize), StringTool.formatSmartSize(minSize),
                      StringTool.formatSmartSize(maxSize) }));
      return result;
    }
    // Check if AIP already downloaded
    try {
      try {
        this.archiveDownloadRemoteResourceService.getArchiveDownloadStatus(archiveMetadata.getResId());
        result.setMesssage(this.messageService.get("job.aip.already_preload"));
      } catch (SolidifyResourceNotFoundException e) {
        // Trigger download
        this.archiveDownloadRemoteResourceService.prepareArchiveDownload(archiveMetadata.getResId());
        result.setStatus(ActionStatus.EXECUTED);
      }
    } catch (Exception e) {
      result.setStatus(ActionStatus.NOT_EXECUTED);
      result.setMesssage(e.getMessage());
    }

    return result;
  }

}

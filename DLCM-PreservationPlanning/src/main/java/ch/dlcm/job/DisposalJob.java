/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - DisposalJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePublicMetadataRemoteResourceService;

public class DisposalJob extends AbstractJob {

  private final TrustedArchivePublicMetadataRemoteResourceService archiveMetadataRemoteResourceService;
  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;

  public DisposalJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          TrustedArchivePublicMetadataRemoteResourceService archiveMetadataRemoteResourceService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.archiveMetadataRemoteResourceService = archiveMetadataRemoteResourceService;
    this.aipRemoteResourceService = aipRemoteResourceService;
  }

  @Override
  public void execute(PreservationJob job, String executionId) {
    // Compute item total
    final long total = this.getTotal();
    // Archive Unit Disposal
    job = this.runDisposalProcess(job, executionId, total);
  }

  private long getTotal() {
    final RestCollection<ArchiveMetadata> collection = this.archiveMetadataRemoteResourceService
            .getIndexMetadataList(this.getPageRequestForTotal());
    return collection.getPage().getTotalItems();
  }

  private PreservationJob runDisposalProcess(PreservationJob job, String executionId, long total) {
    Pageable pageable = this.getPageRequest(job.getMaxItems());
    RestCollection<ArchiveMetadata> collection;
    do {
      // List archives
      collection = this.archiveMetadataRemoteResourceService.getIndexMetadataList(pageable);
      pageable = pageable.next();
      for (final ArchiveMetadata archive : collection.getData()) {
        try {
          this.aipRemoteResourceService.deleteAip(archive.getResId());
          job = this.addProcessedItem(job, executionId, archive.getResId(), this.getArchivePublicUrl(archive.getResId()));
        } catch (SolidifyRestException e) {
          job = this.addIgnoredItem(job, executionId, archive.getResId(), this.getArchivePublicUrl(archive.getResId()),
                  e.getCause() != null ? e.getCause().getMessage() : e.getMessage());
        } catch (SolidifyResourceNotFoundException e) {
          // extract message from parent exception
          job = this.addInErrorItem(job, executionId, archive.getResId(), this.getArchivePublicUrl(archive.getResId()), e.getMessage());
        }
        job = this.jobProcessingService.saveStep(job, executionId, total);
      }
    } while (collection.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));
    return job;
  }
}

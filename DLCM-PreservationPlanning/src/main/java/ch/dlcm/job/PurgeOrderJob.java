/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - PurgeOrderJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.QueryType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.model.settings.ArchiveStatistics;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedAIPDownloadRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchiveStatisticsRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDisseminationInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrderRemoteResourceService;

public class PurgeOrderJob extends AbstractJob {

  private final TrustedOrderRemoteResourceService orderRemoteResourceService;
  private final TrustedDisseminationInfoPackageRemoteResourceService disseminationInfoPackageRemoteResourceService;
  private final TrustedAIPDownloadRemoteResourceService aipDownloadRemoteResourceService;
  private final TrustedArchiveStatisticsRemoteResourceService archiveStatisticsRemoteResourceService;
  private ArrayList<String> dipIds;
  private ArrayList<String> aipDownloadIds;
  private final int timeToKeep;

  public PurgeOrderJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          TrustedOrderRemoteResourceService orderRemoteResourceService,
          TrustedDisseminationInfoPackageRemoteResourceService disseminationInfoPackageRemoteResourceService,
          TrustedAIPDownloadRemoteResourceService aipDownloadRemoteResourceService,
          TrustedArchiveStatisticsRemoteResourceService archiveStatisticsRemoteResourceService) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.orderRemoteResourceService = orderRemoteResourceService;
    this.disseminationInfoPackageRemoteResourceService = disseminationInfoPackageRemoteResourceService;
    this.aipDownloadRemoteResourceService = aipDownloadRemoteResourceService;
    this.archiveStatisticsRemoteResourceService = archiveStatisticsRemoteResourceService;
    this.timeToKeep = dlcmProperties.getPreservationPlanning().getTimeToKeepOrders();
  }

  @Override
  protected void execute(PreservationJob job, String executionId) {
    // Compute item total
    final long total = this.getTotal();
    this.dipIds = new ArrayList<>();
    this.aipDownloadIds = new ArrayList<>();
    this.deleteCompletedOrder(job, executionId, total);
  }

  private void deleteCompletedOrder(PreservationJob job, String executionId, long total) {
    Pageable orderPageable = this.getPageRequest(job.getMaxItems());
    RestCollection<Order> collection;
    do {
      if (StringTool.isNullOrEmpty(job.getParameters())) {
        collection = this.orderRemoteResourceService.getCompletedOrder(orderPageable);
      } else {
        collection = this.orderRemoteResourceService.getCompletedOrderWithParameters(job.getParameters(), orderPageable);
      }
      // check time to keep
      orderPageable = orderPageable.next();
      for (final Order order : collection.getData()) {
        try {
          // Check if order is candidate for purge
          if (this.isCandidateToBePurged(order)) {
            // list DIP of the order
            this.listOrderDIP(order);
            // list Downloaded AIP of the order
            this.listOrderAIPDownload(order);
            // Delete the order

            this.orderRemoteResourceService.deleteOrder(order.getResId());
            job = this.addProcessedItem(job, executionId, order.getResId(), null);
          } else {
            job = this.addIgnoredItem(job, executionId, order.getResId(), this.getOrderPublicUrl(order.getResId()),
                    "The order is not ready to be purged");
          }
        } catch (final Exception e) {
          job = this.addInErrorItem(job, executionId, order.getResId(), this.getOrderPublicUrl(order.getResId()), e.getMessage());
        }
        this.jobProcessingService.saveStep(job, executionId, total);
      }
    } while (collection.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));

    // Delete identified DIP and downloaded AIP
    this.deleteDIPAndAIPLinkedToOrders(job, executionId, total);

  }

  private boolean isCandidateToBePurged(Order order) {
    if (order.getQueryType() == QueryType.DIRECT) {
      try {
        final ArchiveStatistics archiveStatistics = this.archiveStatisticsRemoteResourceService.findOne(order.getQuery());
        return archiveStatistics.getLastUpdate().getWhen().plusDays(this.timeToKeep).isBefore(OffsetDateTime.now(ZoneOffset.UTC));
      } catch (SolidifyResourceNotFoundException e) {
        // check order last update
      } catch (Exception e) {
        throw new SolidifyCheckingException("Error in getting archive statistics", e);
      }
    }
    return order.getLastUpdate().getWhen().plusDays(this.timeToKeep).isBefore(OffsetDateTime.now(ZoneOffset.UTC));
  }

  private void listOrderAIPDownload(Order order) {
    Pageable aipPageable = this.getPageRequest(0);
    RestCollection<ArchivalInfoPackage> aipList;
    do {
      aipList = this.aipDownloadRemoteResourceService.getAipsByOrder(order.getResId(), aipPageable);
      aipPageable = aipPageable.next();
      aipList.getData().forEach(d -> {
        if (!this.aipDownloadIds.contains(d.getResId())) {
          this.aipDownloadIds.add(d.getResId());
        }
      });
    } while (aipList.getPage().hasNext());
  }

  private void listOrderDIP(Order order) {
    // Get all dip and aip ids to preserve them since it is needed to delete first the order
    Pageable dipPageable = this.getPageRequest(0);
    RestCollection<DisseminationInfoPackage> dipList;
    do {
      dipList = this.disseminationInfoPackageRemoteResourceService.getDIPByOrder(order.getResId(), dipPageable);
      dipPageable = dipPageable.next();
      dipList.getData().forEach(d -> {
        if (!this.dipIds.contains(d.getResId())) {
          this.dipIds.add(d.getResId());
        }
      });
    } while (dipList.getPage().hasNext());
  }

  private void deleteDIPAndAIPLinkedToOrders(PreservationJob job, String executionId, long total) {
    for (String dipId : this.dipIds) {
      try {
        this.disseminationInfoPackageRemoteResourceService.deleteDIP(dipId);
        job = this.addProcessedItem(job, executionId, dipId, null);
      } catch (final Exception e) {
        job = this.addInErrorItem(job, executionId, dipId, this.getDIPPublicUrl(dipId), e.getMessage());
      }
      this.jobProcessingService.saveStep(job, executionId, total);
    }

    // Once all the orders have been removed, we can delete all AIP Download
    for (String aipId : this.aipDownloadIds) {
      try {
        this.aipDownloadRemoteResourceService.deleteAIPDownload(aipId);
        job = this.addProcessedItem(job, executionId, aipId, null);
      } catch (final SolidifyRestException e) {
        // Add as ignore all the AIPs that cannot be removed since there are linked to an existing order not
        // removed by this job
        // (i.e. the ones created by direct download)
        job = this.addIgnoredItem(job, executionId, aipId, this.getAIPDownloadPublicUrl(aipId), e.getMessage());
      } catch (final Exception e) {
        job = this.addInErrorItem(job, executionId, aipId, this.getAIPDownloadPublicUrl(aipId), e.getMessage());
      }
      this.jobProcessingService.saveStep(job, executionId, total);
    }
  }

  private long getTotal() {
    final RestCollection<Order> orderList = this.orderRemoteResourceService.getCompletedOrder(this.getPageRequestForTotal());
    final int totalDIP = this.disseminationInfoPackageRemoteResourceService.getCountDIPOfCompletedOrders();
    final int totalAip = this.aipDownloadRemoteResourceService.getCountAIPDownloadOfCompletedOrders();
    return orderList.getPage().getTotalItems() + totalAip + totalDIP;
  }
}

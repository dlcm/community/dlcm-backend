/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - ApplyAipActionJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;

public class ApplyAipActionJob extends AbstractJob {

  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;

  public ApplyAipActionJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.aipRemoteResourceService = aipRemoteResourceService;
  }

  @Override
  public void execute(PreservationJob job, String executionId) {
    // Determine if job applicable on all node or not
    final String[] storageList = this.defineStorageList(job);
    // Compute item total on all storage node
    final long total = this.getTotal(storageList);
    // For each storage node, run action for each aip
    for (final String storageUrl : storageList) {
      job = this.applyAction(job, executionId, total, storageUrl);
    }
  }

  private PreservationJob applyAction(PreservationJob job, String executionId, long total, String storageUrl) {
    Pageable pageable = this.getPageRequest(job.getMaxItems());
    RestCollection<ArchivalInfoPackage> collection;
    do {
      // List AIPs
      if (StringTool.isNullOrEmpty(job.getParameters())) {
        collection = this.aipRemoteResourceService.getAipList(storageUrl, pageable);
      } else {
        collection = this.aipRemoteResourceService.getAipListWithParameters(storageUrl, job.getParameters(), pageable);
      }
      pageable = pageable.next();
      for (final ArchivalInfoPackage aip : collection.getData()) {
        try {
          Result res = switch (job.getJobType()) {
            case ARCHIVE_CHECK -> this.aipRemoteResourceService.checkAIP(storageUrl, aip.getResId());
            case FIXITY -> this.aipRemoteResourceService.checkAIPFixity(storageUrl, aip.getResId());
            case REINDEX, REINDEX_ALL -> this.aipRemoteResourceService.reindexAIP(storageUrl, aip.getResId());
            default -> throw new SolidifyProcessingException("Job type " + job.getJobType().getLabel() + " not applicable");
          };
          job = this.logResult(res, job, executionId, aip.getResId(), this.getAipPublicUrl(storageUrl, aip.getResId()));
        } catch (final Exception e) {
          job = this.addInErrorItem(job, executionId, aip.getResId(), this.getAipPublicUrl(storageUrl, aip.getResId()), e.getMessage());
        }
        // Save intermediate step by 10%
        job = this.jobProcessingService.saveStep(job, executionId, total);
      }
    } while (collection.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));
    return job;

  }

  private String[] defineStorageList(PreservationJob job) {
    return switch (job.getJobType()) {
      case ARCHIVE_CHECK, FIXITY, REINDEX_ALL -> this.internalArchivalStorageUrls;
      case REINDEX -> new String[] { this.defaultArchivalStorageUrl };
      default -> throw new SolidifyProcessingException("Job type " + job.getJobType().getLabel() + " not applicable");
    };
  }

  private long getTotal(String[] storageList) {
    long total = 0;
    for (final String storageUrl : storageList) {
      final RestCollection<ArchivalInfoPackage> collection = this.aipRemoteResourceService.getAipList(storageUrl, this.getPageRequestForTotal());
      total += collection.getPage().getTotalItems();
    }
    return total;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - AbstractJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import java.util.Arrays;

import org.springframework.data.domain.PageRequest;
import org.springframework.util.unit.DataSize;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.JobProcessingService;

public abstract class AbstractJob {

  protected JobProcessingService jobProcessingService;
  protected MessageService messageService;

  protected final String accessModulePublicUrl;
  protected final String preingestModulePublicUrl;
  protected final String ingestModulePublicUrl;
  protected final String[] internalArchivalStorageUrls;
  protected final String[] publicArchivalStorageUrls;

  protected final String defaultArchivalStorageUrl;
  protected final String defaultArchivalStoragePublicUrl;

  protected final DataSize fileSizeLimit;

  private final long jobPageSize;

  protected AbstractJob(DLCMProperties dlcmProperties, MessageService messageService, JobProcessingService jobProcessingService) {
    super();
    this.jobProcessingService = jobProcessingService;
    this.messageService = messageService;

    this.accessModulePublicUrl = dlcmProperties.getModule().getAccess().getPublicUrl();
    this.ingestModulePublicUrl = dlcmProperties.getModule().getIngest().getPublicUrl();
    this.preingestModulePublicUrl = dlcmProperties.getModule().getPreingest().getPublicUrl();
    this.internalArchivalStorageUrls = dlcmProperties.getArchivalStorageUrls();
    this.publicArchivalStorageUrls = dlcmProperties.getArchivalStoragePublicUrls();
    this.defaultArchivalStorageUrl = dlcmProperties.getDefaultArchivalStorageUrl();
    this.defaultArchivalStoragePublicUrl = dlcmProperties.getDefaultArchivalStoragePublicUrl();
    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit();
    this.jobPageSize = dlcmProperties.getPreservationPlanning().getJobPageSize();
  }

  public void run(String jobId, String executionId) {
    // Save starting info
    PreservationJob job;
    if (executionId == null) {
      job = this.jobProcessingService.createNewExecution(jobId);
      executionId = job.getLastExecution().getResId();
    } else {
      job = this.jobProcessingService.resumeExecution(jobId, executionId);
    }
    try {
      this.execute(job, executionId);
      // Save ending info
      this.jobProcessingService.completeJob(job, executionId);
    } catch (final Exception e) {
      // Save ending info
      this.jobProcessingService.completeInErrorJob(job, executionId, e);
    }
  }

  protected PreservationJob addIgnoredItem(PreservationJob job, String executionId, String aipId, String url, String errorMessage) {
    return this.jobProcessingService.addIgnoredItem(job, executionId, aipId, url, errorMessage);
  }

  protected PreservationJob addInErrorItem(PreservationJob job, String executionId, String aipId, String url, String errorMessage) {
    return this.jobProcessingService.addInErrorItem(job, executionId, aipId, url, errorMessage);
  }

  protected PreservationJob addProcessedItem(PreservationJob job, String executionId, String aipId, String url) {
    return this.jobProcessingService.addProcessedItem(job, executionId, aipId, url);
  }

  protected abstract void execute(PreservationJob job, String executionId);

  protected PageRequest getPageRequest(long itemNumber) {
    long pageSize = Math.min(this.jobPageSize, RestCollectionPage.MAX_SIZE_PAGE);
    if (itemNumber > 0) {
      pageSize = Math.min(itemNumber, pageSize);
    }
    return PageRequest.of(0, (int) pageSize);
  }

  protected PageRequest getPageRequestForTotal() {
    return PageRequest.of(0, 1);
  }

  protected String getSipPublicUrl(String sipId) {
    return this.ingestModulePublicUrl + "/" + ResourceName.SIP + "/" + sipId;
  }

  protected String getDepositPublicUrl(String depositId) {
    return this.preingestModulePublicUrl + "/" + ResourceName.DEPOSIT + "/" + depositId;
  }

  protected String getAipPublicUrl(String storageUrl, String aipId) {
    String publicStorageUrl = this.getPublicArchivalStorageUrlFromInternalUrl(storageUrl);
    return publicStorageUrl + "/" + ResourceName.AIP + "/" + aipId;
  }

  protected String getArchivePublicUrl(String archiveId) {
    return this.accessModulePublicUrl + "/" + ResourceName.PUBLIC_METADATA + "/" + archiveId;
  }

  protected String getOrderPublicUrl(String orderId) {
    return this.accessModulePublicUrl + "/" + ResourceName.ORDER + "/" + orderId;
  }

  protected String getDIPPublicUrl(String orderId) {
    return this.accessModulePublicUrl + "/" + ResourceName.DIP + "/" + orderId;
  }

  protected String getAIPDownloadPublicUrl(String orderId) {
    return this.accessModulePublicUrl + "/" + ResourceName.AIP + "/" + orderId;
  }

  private String getPublicArchivalStorageUrlFromInternalUrl(String url) {

    if (Arrays.asList(this.publicArchivalStorageUrls).contains(url)) {
      // url is already public
      return url;
    }

    for (int i = 0; i < this.internalArchivalStorageUrls.length; i++) {
      if (this.internalArchivalStorageUrls[i].equals(url)) {
        return this.publicArchivalStorageUrls[i];
      }
    }

    throw new SolidifyRuntimeException("No public storage URL found for internal storage URL '" + url + "'");
  }

  protected PreservationJob logResult(Result result, PreservationJob job, String executionId, String resId, String resUrl) {
    return switch (result.getStatus()) {
      case EXECUTED -> this.addProcessedItem(job, executionId, resId, resUrl);
      case NON_APPLICABLE -> this.addIgnoredItem(job, executionId, resId, resUrl, result.getMessage());
      default -> this.addInErrorItem(job, executionId, resId, resUrl, result.getMessage());
    };
  }
}

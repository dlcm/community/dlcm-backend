/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - ComplianceLevelUpdateJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;

public class ComplianceLevelUpdateJob extends AbstractJob {

  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;

  private final String defaultStorageUrl;

  public ComplianceLevelUpdateJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.aipRemoteResourceService = aipRemoteResourceService;
    this.defaultStorageUrl = dlcmProperties.getDefaultArchivalStorageUrl();
  }

  @Override
  protected void execute(PreservationJob job, String executionId) {
    final long total = this.getTotal(this.defaultStorageUrl);
    Pageable pageable = this.getPageRequest(job.getMaxItems());
    RestCollection<ArchivalInfoPackage> collection;
    do {
      // List AIPs
      if (StringTool.isNullOrEmpty(job.getParameters())) {
        collection = this.aipRemoteResourceService.getAipList(this.defaultStorageUrl, pageable);
      } else {
        collection = this.aipRemoteResourceService.getAipListWithParameters(this.defaultStorageUrl, job.getParameters(), pageable);
      }
      pageable = pageable.next();
      for (final ArchivalInfoPackage aip : collection.getData()) {
        try {
          if (aip.getInfo().getStatus() == PackageStatus.COMPLETED) {
            // Update compliance level
            final Result aipResult = this.aipRemoteResourceService.updateComplianceLevel(this.defaultStorageUrl, aip.getResId());
            job = this.logResult(aipResult, job, executionId, aip.getResId(), this.getAipPublicUrl(this.defaultStorageUrl, aip.getResId()));
          } else {
            job = this.addIgnoredItem(job, executionId, aip.getResId(),
                    this.getAipPublicUrl(this.defaultStorageUrl, aip.getResId()),
                    "Archive not ready for compliance level update");
          }
        } catch (final Exception e) {
          job = this.addInErrorItem(job, executionId, aip.getResId(), this.getAipPublicUrl(this.defaultStorageUrl, aip.getResId()),
                  e.getMessage());
        }
        // Save intermediate step by 10%
        job = this.jobProcessingService.saveStep(job, executionId, total);
      }
    } while (collection.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));
  }

  private long getTotal(String storageUrl) {
    final RestCollection<ArchivalInfoPackage> collection = this.aipRemoteResourceService.getAipList(storageUrl, this.getPageRequestForTotal());
    return collection.getPage().getTotalItems();
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - CheckReplicationJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.exception.DLCMCompareException;
import ch.dlcm.exception.DLCMReplicationException;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.replication.ReplicationService;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedArchivePublicMetadataRemoteResourceService;

public class CheckReplicationJob extends AbstractJob {

  private final ReplicationService replicationService;
  private final TrustedArchivePublicMetadataRemoteResourceService archiveMetadataRemoteResourceService;

  public CheckReplicationJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          ReplicationService replicationService,
          TrustedArchivePublicMetadataRemoteResourceService archiveMetadataRemoteResourceService) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.replicationService = replicationService;
    this.archiveMetadataRemoteResourceService = archiveMetadataRemoteResourceService;
  }

  @Override
  public void execute(PreservationJob job, String executionId) {
    // Compute item total
    final long total = this.getTotal();
    // Archive Unit Replication
    this.runReplicationCheck(job, executionId, total);
  }

  private long getTotal() {
    final RestCollection<ArchiveMetadata> collection = this.archiveMetadataRemoteResourceService.getIndexMetadataList(null, null,
            this.getPageRequestForTotal());
    return collection.getPage().getTotalItems();
  }

  private PreservationJob runReplicationCheck(PreservationJob job, String executionId, long total) {
    Pageable pageable = this.getPageRequest(job.getMaxItems());
    RestCollection<ArchiveMetadata> collection;
    do {
      // List AIPs from index
      collection = this.archiveMetadataRemoteResourceService.getMetadataList(pageable);
      pageable = pageable.next();
      for (final ArchiveMetadata md : collection.getData()) {
        try {
          this.replicationService.checkAIP(md.getResId());
          job = this.addProcessedItem(job, executionId, md.getResId(),
                  this.getAipPublicUrl(this.defaultArchivalStoragePublicUrl, md.getResId()));
        } catch (final SolidifyCheckingException e) {
          job = this.addIgnoredItem(job, executionId, md.getResId(),
                  this.getAipPublicUrl(this.defaultArchivalStoragePublicUrl, md.getResId()), e.getMessage());
        } catch (SolidifyProcessingException | SolidifyRestException | DLCMReplicationException | DLCMCompareException e) {
          job = this.addInErrorItem(job, executionId, md.getResId(),
                  this.getAipPublicUrl(this.defaultArchivalStoragePublicUrl, md.getResId()), e.getMessage());
        }
        job = this.jobProcessingService.saveStep(job, executionId, total);
      }
    } while (collection.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));
    return job;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - CleanSubmissionJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.exception.DLCMJobItemIgnoredException;
import ch.dlcm.exception.DLCMJobItemInErrorException;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedSubmissionPolicyRemoteResourceService;

public class CleanSubmissionJob extends AbstractJob {

  private final boolean complianceMode;
  private final TrustedSubmissionPolicyRemoteResourceService submissionPolicyRemoteResourceService;
  private final TrustedDepositRemoteResourceService depositRemoteResourceService;
  private final TrustedSubmissionInfoPackageRemoteResourceService sipRemoteResourceService;
  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;

  public CleanSubmissionJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          TrustedSubmissionPolicyRemoteResourceService submissionPolicyRemoteResourceService,
          TrustedDepositRemoteResourceService depositRemoteResourceService,
          TrustedSubmissionInfoPackageRemoteResourceService sipRemoteResourceService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService,
          boolean complianceMode) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.submissionPolicyRemoteResourceService = submissionPolicyRemoteResourceService;
    this.depositRemoteResourceService = depositRemoteResourceService;
    this.sipRemoteResourceService = sipRemoteResourceService;
    this.aipRemoteResourceService = aipRemoteResourceService;
    this.complianceMode = complianceMode;
  }

  @Override
  public void execute(PreservationJob job, String executionId) {
    // Loop on completed SIP to delete working SIP associated to deposit
    Pageable sipPageable = this.getPageRequest(job.getMaxItems());
    RestCollection<SubmissionInfoPackage> sipList;
    do {
      // Find all completed SIP
      if (StringTool.isNullOrEmpty(job.getParameters())) {
        sipList = this.sipRemoteResourceService.getSIPList(sipPageable);
      } else {
        sipList = this.sipRemoteResourceService.getSIPListWithParameters(job.getParameters(), sipPageable);
      }
      // Go to next page
      sipPageable = sipPageable.next();
      for (final SubmissionInfoPackage sipPackage : sipList.getData()) {
        try {
          // Check SIP
          this.checkIfSipReadyToClean(sipPackage);
          // Check replicas
          if (this.complianceMode) {
            this.hasAllReplicas(sipPackage);
          }
          // Clean SIP
          if (sipPackage.getInfo().getStatus().equals(PackageStatus.CLEANED)) {
            job = this.addIgnoredItem(job, executionId, sipPackage.getResId(), this.getSipPublicUrl(sipPackage.getResId()),
                    "SIP already cleaned");
          } else {
            try {
              // update deposit with the CLEANED status
              this.sipRemoteResourceService.cleanSIPDatafile(sipPackage);
              job = this.addProcessedItem(job, executionId, sipPackage.getResId(), this.getSipPublicUrl(sipPackage.getResId()));
            } catch (final Exception e) {
              job = this.addInErrorItem(job, executionId, sipPackage.getResId(), this.getSipPublicUrl(sipPackage.getResId()), e.getMessage());
            }
          }
          // Clean deposit
          Deposit deposit = this.cleanDeposit(sipPackage.getDepositId());
          job = this.addProcessedItem(job, executionId, deposit.getResId(), this.getDepositPublicUrl(deposit.getResId()));
        } catch (final DLCMJobItemIgnoredException e) {
          job = this.addIgnoredItem(job, executionId, e.getPackageId(), e.getPackageUrl(), e.getMessage());
        } catch (final DLCMJobItemInErrorException e) {
          job = this.addInErrorItem(job, executionId, e.getPackageId(), e.getPackageUrl(), e.getMessage());
        }
        this.jobProcessingService.saveStep(job, executionId, sipList.getPage().getTotalItems());
      }
    } while (sipList.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));
  }

  private Deposit cleanDeposit(String depositId) throws DLCMJobItemIgnoredException, DLCMJobItemInErrorException {
    Deposit deposit = null;
    try {
      deposit = this.depositRemoteResourceService.getDeposit(depositId);
      if (deposit.getStatus().equals(DepositStatus.CLEANED)) {
        throw new DLCMJobItemIgnoredException(deposit.getResId(), this.getDepositPublicUrl(depositId), "Deposit already cleaned");
      }
      this.depositRemoteResourceService.cleanDepositDatafile(deposit);
    } catch (DLCMJobItemIgnoredException e) {
      throw e;
    } catch (final SolidifyResourceNotFoundException e) {
      throw new DLCMJobItemInErrorException(depositId, this.getDepositPublicUrl(depositId), "Deposit not found");
    } catch (final Exception e) {
      throw new DLCMJobItemInErrorException(depositId, this.getDepositPublicUrl(depositId), e.getMessage());
    }
    return deposit;
  }

  private void checkIfSipReadyToClean(SubmissionInfoPackage sipPackage) throws DLCMJobItemIgnoredException {
    // Check if SIP completed
    if (!sipPackage.isCompleted()) {
      // SIP not completed
      throw new DLCMJobItemIgnoredException(sipPackage.getResId(), this.getSipPublicUrl(sipPackage.getResId()), "Package in-progress");
    }
    if (sipPackage.getSubmissionPolicyId() == null) {
      // Check if submission policy is defined
      throw new DLCMJobItemIgnoredException(sipPackage.getResId(), this.getSipPublicUrl(sipPackage.getResId()), "Missing submission policy");
    }
    // Check submission policy retention time
    final OffsetDateTime endTimeToKeep = sipPackage.getCreation()
            .getWhen()
            .plusDays(
                    this.submissionPolicyRemoteResourceService.getSubmissionPolicy(
                            sipPackage.getSubmissionPolicyId())
                            .getTimeToKeep());

    if (endTimeToKeep.isAfter(OffsetDateTime.now(ZoneOffset.UTC))) {
      throw new DLCMJobItemIgnoredException(sipPackage.getResId(), this.getSipPublicUrl(sipPackage.getResId()), "Too early to clean package");
    }
  }

  private void hasAllReplicas(SubmissionInfoPackage sipPackage) throws DLCMJobItemIgnoredException {
    int replicaNumber = 0;
    for (final String archivalStorageUrl : this.internalArchivalStorageUrls) {
      try {
        final ArchivalInfoPackage aip = this.aipRemoteResourceService.getAIP(archivalStorageUrl, sipPackage.getAipId());
        if (aip.isCompleted()) {
          replicaNumber++;
        }
      } catch (final SolidifyResourceNotFoundException e) {
        // Do not count => missing a replica
      } catch (final Exception e) {
        throw new SolidifyRuntimeException("Error in submission cleaning: " + e.getMessage(), e);
      }
    }
    if (replicaNumber != this.internalArchivalStorageUrls.length) {
      throw new DLCMJobItemIgnoredException(sipPackage.getResId(), this.getSipPublicUrl(sipPackage.getResId()), "Missing a copy in replication");
    }
  }
}

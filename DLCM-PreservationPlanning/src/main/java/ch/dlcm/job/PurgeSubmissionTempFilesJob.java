/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - PurgeSubmissionTempFilesJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedSubmissionInfoPackageRemoteResourceService;

public class PurgeSubmissionTempFilesJob extends AbstractJob {
  private static final Logger log = LoggerFactory.getLogger(PurgeSubmissionTempFilesJob.class);

  private final TrustedDepositRemoteResourceService depositRemoteResourceService;
  private final TrustedSubmissionInfoPackageRemoteResourceService sipRemoteResourceService;
  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;
  private final TrustedArchivePublicMetadataRemoteResourceService archiveMetadataRemoteResourceService;

  public PurgeSubmissionTempFilesJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          TrustedDepositRemoteResourceService depositRemoteResourceService,
          TrustedSubmissionInfoPackageRemoteResourceService sipRemoteResourceService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService,
          TrustedArchivePublicMetadataRemoteResourceService archiveMetadataRemoteResourceService) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.depositRemoteResourceService = depositRemoteResourceService;
    this.sipRemoteResourceService = sipRemoteResourceService;
    this.aipRemoteResourceService = aipRemoteResourceService;
    this.archiveMetadataRemoteResourceService = archiveMetadataRemoteResourceService;
  }

  @Override
  public void execute(PreservationJob job, String executionId) {
    // Compute item total
    final long total = this.getTotal();
    job = this.deleteWorkingSIP(job, executionId, total);
    this.deleteWorkingAIP(job, executionId, total);
  }

  /**
   * Loop on completed AIP to delete working AIP associated with a SIP
   *
   * @param job
   * @param executionId
   * @param total
   */
  private PreservationJob deleteWorkingAIP(PreservationJob job, String executionId, long total) {
    Pageable archivePageable = this.getPageRequest(job.getMaxItems());
    RestCollection<ArchiveMetadata> archiveList;
    do {
      // Find all completed AIP on default archival storage module
      archiveList = this.archiveMetadataRemoteResourceService.getMetadataList(archivePageable);
      // Go to next page
      archivePageable = archivePageable.next();
      // For each AIP
      for (final ArchiveMetadata archive : archiveList.getData()) {
        // For all SIP of the AIP: today always 1 only SIP
        final ArchivalInfoPackage aip = this.aipRemoteResourceService.findOne(archive.getResId());
        for (final String sipId : aip.getSipIds()) {
          try {
            // Check if the working AIP associated with the SIP is already deleted
            final HttpStatusCode httpStatus = this.sipRemoteResourceService.checkSipWorkingAip(sipId);
            switch (HttpStatus.valueOf(httpStatus.value())) {
              case OK -> {
                // Delete the working AIP associated with the SIP
                if (this.sipRemoteResourceService.deleteWorkingAip(sipId)) {
                  job = this.addProcessedItem(job, executionId, sipId, this.getSipPublicUrl(sipId));
                } else {
                  job = this.addInErrorItem(job, executionId, sipId, this.getSipPublicUrl(sipId), "Error in purging working AIP");
                }
              }
              case NOT_FOUND -> job = this.addIgnoredItem(job, executionId, sipId, this.getSipPublicUrl(sipId), "SIP does not exist");
              case GONE -> job = this.addIgnoredItem(job, executionId, sipId, this.getSipPublicUrl(sipId), "Working AIP does not exist");
              default -> job = this.addInErrorItem(job, executionId, sipId, this.getSipPublicUrl(sipId), "Unknown error");
            }
          } catch (final Exception e) {
            job = this.addInErrorItem(job, executionId, sipId, this.getSipPublicUrl(sipId), e.getMessage());
            log.error("Error in purging temporary AIP for SIP {} : {}", sipId, e.getMessage());
          }
        }
        job = this.jobProcessingService.saveStep(job, executionId, total);
      }
    } while (archiveList.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));

    return job;
  }

  /**
   * Loop on completed SIP to delete working SIP associated with a deposit
   *
   * @param job
   * @param executionId
   * @param total
   * @return
   */
  private PreservationJob deleteWorkingSIP(PreservationJob job, String executionId, long total) {
    Pageable sipPageable = this.getPageRequest(job.getMaxItems());
    RestCollection<SubmissionInfoPackage> sipList;
    do {
      // Find all completed SIP
      sipList = this.sipRemoteResourceService.getCompletedSIPList(sipPageable);
      // Go to next page
      sipPageable = sipPageable.next();
      // For each SIP
      for (final SubmissionInfoPackage sipPackage : sipList.getData()) {
        final String depositId = sipPackage.getDepositId();
        try {
          final HttpStatusCode httpStatus = this.depositRemoteResourceService.checkDepositWorkingSip(depositId);
          switch (HttpStatus.valueOf(httpStatus.value())) {
            case OK -> {
              // Deleting working SIP associated with the deposit
              if (this.depositRemoteResourceService.deleteWorkingSip(depositId)) {
                job = this.addProcessedItem(job, executionId, depositId, this.getDepositPublicUrl(depositId));
              } else {
                job = this.addInErrorItem(job, executionId, depositId, this.getDepositPublicUrl(depositId), "Error in purging working SIP");
              }
            }
            case NOT_FOUND -> job = this.addIgnoredItem(job, executionId, depositId, this.getDepositPublicUrl(depositId),
                    "Deposit does not exist");
            case GONE -> job = this.addIgnoredItem(job, executionId, depositId, this.getDepositPublicUrl(depositId),
                    "Working SIP does not exist");
            default -> job = this.addInErrorItem(job, executionId, depositId, this.getDepositPublicUrl(depositId), "Unknown error");
          }
        } catch (final Exception e) {
          job = this.addInErrorItem(job, executionId, sipPackage.getResId(), this.getDepositPublicUrl(depositId), e.getMessage());
          log.error("Error in purging temporary SIP for deposit {} : {}", depositId, e.getMessage());
        }
        job = this.jobProcessingService.saveStep(job, executionId, total);
      }
    } while (sipList.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));

    return job;
  }

  private long getTotal() {
    final RestCollection<SubmissionInfoPackage> sipList = this.sipRemoteResourceService.getCompletedSIPList(this.getPageRequestForTotal());
    final RestCollection<ArchiveMetadata> aipList = this.archiveMetadataRemoteResourceService.getMetadataList(this.getPageRequestForTotal());
    return sipList.getPage().getTotalItems() + aipList.getPage().getTotalItems();
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - ReloadAipJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;

public class ReloadAipJob extends AbstractJob {

  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;

  public ReloadAipJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.aipRemoteResourceService = aipRemoteResourceService;
  }

  @Override
  public void execute(PreservationJob job, String executionId) {
    Pageable pageable = this.getPageRequest(job.getMaxItems());
    RestCollection<StoredAIP> collection;
    do {
      // List existing AIPs (Stored AIP)
      collection = this.aipRemoteResourceService.getStoredAipList(pageable);
      pageable = pageable.next();
      for (final StoredAIP storedAip : collection.getData()) {
        try {
          // Check if AIP already exists
          this.aipRemoteResourceService.getAIPOnDefaultStorage(storedAip.getAipId());
          job = this.addIgnoredItem(job, executionId, storedAip.getAipId(),
                  this.getAipPublicUrl(this.defaultArchivalStoragePublicUrl, storedAip.getAipId()), "Existing AIP");
        } catch (final SolidifyResourceNotFoundException e) {
          // Reload it if not
          try {
            this.aipRemoteResourceService.createAIP(storedAip);
            job = this.addProcessedItem(job, executionId, storedAip.getAipId(),
                    this.getAipPublicUrl(this.defaultArchivalStoragePublicUrl, storedAip.getAipId()));
          } catch (final SolidifyProcessingException ex) {
            job = this.addInErrorItem(job, executionId, storedAip.getAipId(),
                    this.getAipPublicUrl(this.defaultArchivalStoragePublicUrl, storedAip.getAipId()), ex.getMessage());
          }
        }
        // Save intermediate step by 10%
        job = this.jobProcessingService.saveStep(job, executionId, collection.getPage().getTotalItems());
      }
    } while (collection.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));
  }
}

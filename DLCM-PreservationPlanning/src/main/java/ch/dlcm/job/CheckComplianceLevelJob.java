/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - CheckComplianceLevelJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;

public class CheckComplianceLevelJob extends AbstractJob {

  private final TrustedDepositRemoteResourceService depositRemoteResourceService;

  public CheckComplianceLevelJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          TrustedDepositRemoteResourceService depositRemoteResourceService) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.depositRemoteResourceService = depositRemoteResourceService;
  }

  @Override
  protected void execute(PreservationJob job, String executionId) {
    Pageable pageable = this.getPageRequest(job.getMaxItems());
    RestCollection<Deposit> depositList;
    do {
      // Find all Deposits
      if (StringTool.isNullOrEmpty(job.getParameters())) {
        depositList = this.depositRemoteResourceService.getDepositList(pageable);
      } else {
        depositList = this.depositRemoteResourceService.getDepositListWithParameters(job.getParameters(), pageable);
      }
      // Go to next page
      pageable = pageable.next();
      for (final Deposit deposit : depositList.getData()) {
        try {

          final Result result = this.depositRemoteResourceService.checkComplianceLevel(deposit);
          if (result.getStatus() == Result.ActionStatus.EXECUTED) {
            job = this.addProcessedItem(job, executionId, deposit.getResId(),
                    this.getDepositPublicUrl(deposit.getResId()));
          } else if (result.getStatus() == Result.ActionStatus.NOT_EXECUTED) {
            job = this
                    .addIgnoredItem(job, executionId, deposit.getResId(),
                            this.getDepositPublicUrl(deposit.getResId()), "Action not executed");
          }
        } catch (final Exception e) {
          job = this.addInErrorItem(job, executionId, deposit.getResId(),
                  this.getDepositPublicUrl(deposit.getResId()), e.getMessage());
        }
      }

      job = this.jobProcessingService.saveStep(job, executionId, depositList.getPage().getTotalItems());

    } while (depositList.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - MetadataMigrationJob.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.job;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.JobProcessingService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedSubmissionInfoPackageRemoteResourceService;

public class MetadataMigrationJob extends AbstractJob {

  private static final String ARCHIVE_NOT_UPGRADABLE = "Archive not upgradable because it is already in default version or the data are not cleaned";

  private final TrustedDepositRemoteResourceService depositRemoteResourceService;
  private final TrustedSubmissionInfoPackageRemoteResourceService sipRemoteResourceService;
  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;

  private final String defaultStorageUrl;

  public MetadataMigrationJob(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          JobProcessingService jobProcessingService,
          TrustedDepositRemoteResourceService depositRemoteResourceService,
          TrustedSubmissionInfoPackageRemoteResourceService sipRemoteResourceService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(dlcmProperties, messageService, jobProcessingService);
    this.depositRemoteResourceService = depositRemoteResourceService;
    this.sipRemoteResourceService = sipRemoteResourceService;
    this.aipRemoteResourceService = aipRemoteResourceService;
    this.defaultStorageUrl = dlcmProperties.getDefaultArchivalStorageUrl();
  }

  @Override
  protected void execute(PreservationJob job, String executionId) {
    final long total = this.getTotal(this.defaultStorageUrl);
    Pageable pageable = this.getPageRequest(job.getMaxItems());
    RestCollection<ArchivalInfoPackage> collection;
    do {
      // List AIPs
      if (StringTool.isNullOrEmpty(job.getParameters())) {
        collection = this.aipRemoteResourceService.getAipList(this.defaultStorageUrl, pageable);
      } else {
        collection = this.aipRemoteResourceService.getAipListWithParameters(this.defaultStorageUrl, job.getParameters(), pageable);
      }
      pageable = pageable.next();
      for (final ArchivalInfoPackage aip : collection.getData()) {
        final List<SubmissionInfoPackage> sipList = new ArrayList<>();
        final List<Deposit> depositList = new ArrayList<>();
        try {
          // Preload SIP and Deposit
          for (String sipId : aip.getSipIds()) {
            final SubmissionInfoPackage sip = this.sipRemoteResourceService.findOne(sipId);
            sipList.add(sip);
            depositList.add(this.depositRemoteResourceService.findOne(sip.getDepositId()));
          }
          // Check if archve is candidate to upgrade
          if (this.checkIfArchiveIsUpgrable(aip, sipList, depositList)) {
            job = this.upgradeArchive(job, executionId, this.defaultStorageUrl, aip, sipList, depositList);
          } else {
            job = this.ignoreArchive(job, executionId, this.defaultStorageUrl, aip, sipList, depositList);
          }
        } catch (final Exception e) {
          job = this.addInErrorItem(job, executionId, aip.getResId(), this.getAipPublicUrl(this.defaultStorageUrl, aip.getResId()),
                  e.getMessage());
        }
        // Save intermediate step by 10%
        job = this.jobProcessingService.saveStep(job, executionId, total);
      }
    } while (collection.getPage().hasNext() && this.jobProcessingService.checkIfMaxItems(job, executionId));
  }

  private PreservationJob ignoreArchive(PreservationJob job, String executionId, String storageUrl, ArchivalInfoPackage aip,
          List<SubmissionInfoPackage> sipList, List<Deposit> depositList) {
    job = this.addIgnoredItem(job, executionId, aip.getResId(), this.getAipPublicUrl(storageUrl, aip.getResId()),
            ARCHIVE_NOT_UPGRADABLE);
    for (final SubmissionInfoPackage sip : sipList) {
      job = this.addIgnoredItem(job, executionId, sip.getResId(), this.getSipPublicUrl(sip.getResId()),
              ARCHIVE_NOT_UPGRADABLE);
    }
    for (final Deposit deposit : depositList) {
      job = this.addIgnoredItem(job, executionId, deposit.getResId(), this.getDepositPublicUrl(deposit.getResId()),
              ARCHIVE_NOT_UPGRADABLE);
    }
    return job;
  }

  private PreservationJob upgradeArchive(PreservationJob job, String executionId, String storageUrl, ArchivalInfoPackage aip,
          List<SubmissionInfoPackage> sipList,
          List<Deposit> depositList) {
    // Upgrade AIP
    final Result aipResult = this.aipRemoteResourceService.upgradeMetadata(storageUrl, aip.getResId());
    job = this.logResult(aipResult, job, executionId, aip.getResId(), this.getAipPublicUrl(storageUrl, aip.getResId()));
    // Upgrade SIP
    for (final SubmissionInfoPackage sip : sipList) {
      final Result sipResult = this.sipRemoteResourceService.upgradeMetadata(sip.getResId());
      job = this.logResult(sipResult, job, executionId, sip.getResId(), this.getSipPublicUrl(sip.getResId()));
    }
    // Upgrade Deposit
    for (final Deposit deposit : depositList) {
      final Result depositResult = this.depositRemoteResourceService.upgradeMetadata(deposit);
      job = this.logResult(depositResult, job, executionId, deposit.getResId(), this.getDepositPublicUrl(deposit.getResId()));
    }
    return job;
  }

  private boolean checkIfArchiveIsUpgrable(ArchivalInfoPackage aip, List<SubmissionInfoPackage> sipList, List<Deposit> depositList) {
    // AIP not in default version
    if (aip.getInfo().getStatus() != PackageStatus.COMPLETED
            || aip.getMetadataVersion() == DLCMMetadataVersion.getDefaultVersion()) {
      return false;
    }
    // SIP not with CLEANED status
    for (final SubmissionInfoPackage sip : sipList) {
      if (sip.getInfo().getStatus() != PackageStatus.CLEANED) {
        return false;
      }
    }
    // Deposit not with CLEANED status
    for (final Deposit deposit : depositList) {
      if (deposit.getStatus() != DepositStatus.CLEANED) {
        return false;
      }
    }
    return true;
  }

  private long getTotal(String storageUrl) {
    final RestCollection<ArchivalInfoPackage> collection = this.aipRemoteResourceService.getAipList(storageUrl, this.getPageRequestForTotal());
    return collection.getPage().getTotalItems() * 3; // AIP + SIP + Deposit
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - JobProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.model.preservation.JobExecution;
import ch.dlcm.model.preservation.JobExecutionReportLine;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.model.preservation.PreservationJob.JobStatus;
import ch.dlcm.repository.PreservationJobRepository;

@Service
@ConditionalOnBean(PreservationPlanningController.class)
public class JobProcessingService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(JobProcessingService.class);

  protected PreservationJobRepository jobRepository;

  public JobProcessingService(MessageService messageService, PreservationJobRepository preservationJobRepository,
          DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.jobRepository = preservationJobRepository;
  }

  // Log ignored items
  public PreservationJob addIgnoredItem(PreservationJob job, String executionId, String resId, String url, String errorMessage) {
    final JobExecution execution = this.getExecutionReportIgnored(job, executionId);
    execution.getLastExecutionReport()
            .addReportLine(new JobExecutionReportLine(resId, JobExecutionReportLine.ItemStatus.IGNORED, url, errorMessage));
    return this.saveJob(job);
  }

  // Log in-error items
  public PreservationJob addInErrorItem(PreservationJob job, String executionId, String resId, String url, String errorMessage) {
    final JobExecution execution = this.getExecutionReportError(job, executionId);
    execution.getLastExecutionReport()
            .addReportLine(new JobExecutionReportLine(resId, JobExecutionReportLine.ItemStatus.ERROR, url, errorMessage));
    return this.saveJob(job);
  }

  // Log processed items
  public PreservationJob addProcessedItem(PreservationJob job, String executionId, String resId, String url) {
    final JobExecution execution = this.getExecutionReportProcessed(job, executionId);
    execution.getLastExecutionReport().addReportLine(new JobExecutionReportLine(resId, JobExecutionReportLine.ItemStatus.PROCESSED, url));
    return this.saveJob(job);
  }

  /**
   * Save ending info
   *
   * @param job
   */
  @Transactional
  public void completeInErrorJob(PreservationJob job, String executionId, Exception e) {
    final Optional<PreservationJob> jobValue = this.jobRepository.findById(job.getResId());
    final PreservationJob jobReload = jobValue.orElseThrow(() -> new NoSuchElementException("Preservation Job " + job.getResId()));
    final JobExecution execution = this.getJobExecution(jobReload, executionId);
    execution.setStatus(JobStatus.IN_ERROR);
    execution.setStatusMessage(e.getMessage());
    execution.setCompletionStatus(100);
    execution.setEndDate(OffsetDateTime.now(ZoneOffset.UTC));
    this.saveJob(jobReload);
    log.error("Job completed in error: {} {} {}", jobReload.getResId(), jobReload.getJobType().name(), jobReload.getName(), e);
  }

  /**
   * Save ending info
   *
   * @param job
   */
  @Transactional
  public void completeJob(PreservationJob job, String executionId) {
    final Optional<PreservationJob> jobValue = this.jobRepository.findById(job.getResId());
    final PreservationJob jobReload = jobValue.orElseThrow(() -> new NoSuchElementException("Preservation Job " + job.getResId()));
    final JobExecution execution = this.getJobExecution(jobReload, executionId);
    execution.setStatus(JobStatus.COMPLETED);
    execution.setCompletionStatus(100);
    execution.setEndDate(OffsetDateTime.now(ZoneOffset.UTC));
    this.saveJob(jobReload);
    log.info("Job completed: {} {} {}", jobReload.getResId(), jobReload.getJobType().name(), jobReload.getName());
  }

  /**
   * Start a new execution of the job
   *
   * @param jobId
   * @return
   */
  @Transactional
  public PreservationJob createNewExecution(String jobId) {
    final PreservationJob job = this.jobRepository.findById(jobId)
            .orElseThrow(() -> new NoSuchElementException("Preservation Job " + jobId + " not found"));
    log.info("Job starting: {} {} {}", job.getResId(), job.getJobType().name(), job.getName());
    job.getLastExecution().setStatus(JobStatus.IN_PROGRESS);
    job.getLastExecution().setStatusMessage(null);
    job.getLastExecution().setCompletionStatus(0);
    job.getLastExecution().setStartDate(OffsetDateTime.now(ZoneOffset.UTC));
    job.getLastExecution().addExecutionReport();
    this.saveJob(job);
    return job;
  }

  @Transactional
  public PreservationJob resumeExecution(String jobId, String executionId) {
    final PreservationJob job = this.jobRepository.findById(jobId)
            .orElseThrow(() -> new NoSuchElementException("Preservation Job " + jobId + " not found"));
    final JobExecution execution = this.getJobExecution(job, executionId);
    log.info("Job {} {} starting execution {}", job.getResId(), job.getJobType().name(), execution.getResId());
    execution.setStatus(JobStatus.IN_PROGRESS);
    job.getLastExecution().setStatusMessage(null);
    execution.setCompletionStatus(0);
    execution.setStartDate(OffsetDateTime.now(ZoneOffset.UTC));
    execution.addExecutionReport();
    this.saveJob(job);
    return job;
  }

  /**
   * Save intermediate step by 10%
   *
   * @param job
   * @param num
   * @param totalItems
   */
  @Transactional
  public PreservationJob saveStep(PreservationJob job, String executionId, long totalItems) {
    // Get Job Execution
    final JobExecution execution = this.getJobExecution(job, executionId);

    final long currentNumber = execution.getProcessedItems() + execution.getIgnoredItems() + execution.getInErrorItems();
    final int percent = this.calculatePercentRoundTo10(currentNumber, totalItems);

    if (execution.getCompletionStatus() != percent) {
      execution.setCompletionStatus(percent);
      return this.saveJob(job);
    }

    return job;
  }

  public boolean checkIfMaxItems(PreservationJob job, String executionId) {
    if (job.getMaxItems() == 0) {
      return true;
    }
    final JobExecution jobExecution = this.getJobExecution(job, executionId);
    return jobExecution.getProcessedItems() < job.getMaxItems();
  }

  /**
   * Save Job in database
   *
   * @param job
   */
  protected PreservationJob saveJob(PreservationJob job) {
    return this.jobRepository.save(job);
  }

  private int calculatePercentRoundTo10(long num, long totalItems) {
    final long percent = num * 100 / totalItems;
    final long modulo10 = percent % 10;
    long roundPercent = (modulo10 >= 5) ? percent + (10 - modulo10) : percent - modulo10;
    if (roundPercent == 100 && num != totalItems) {
      roundPercent = 90; // return 100% only if all items have been treated
    }
    return (int) roundPercent;
  }

  // Get execution report
  private JobExecution getExecutionReportError(PreservationJob job, String executionId) {
    final JobExecution execution = this.getJobExecution(job, executionId);
    execution.getLastExecutionReport().addInErrorItem();
    return execution;
  }

  private JobExecution getExecutionReportIgnored(PreservationJob job, String executionId) {
    final JobExecution execution = this.getJobExecution(job, executionId);
    execution.getLastExecutionReport().addIgnoredItem();
    return execution;
  }

  private JobExecution getExecutionReportProcessed(PreservationJob job, String executionId) {
    final JobExecution execution = this.getJobExecution(job, executionId);
    execution.getLastExecutionReport().addProcessedItem();
    return execution;
  }

  private JobExecution getJobExecution(PreservationJob job, String executionId) {
    return job.getExecutions().stream().filter(item -> item.getResId().equals(executionId)).findFirst()
            .orElseThrow(() -> new NoSuchElementException(
                    "Preservation Job " + job.getResId() + " does not contain an execution with " + executionId));
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - MonitorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.service.FallbackRestClientService;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreservationPlanningController;

@Service
@ConditionalOnBean(PreservationPlanningController.class)
public class MonitorService extends DLCMService {

  private final FallbackRestClientService restClientService;

  private final String authServerUrl;
  private final String adminUrl;
  private final String dataMgmtUrl;
  private final String preingestUrl;
  private final String ingestUrl;
  private final String accessUrl;
  private final String[] archivalStorageUrls;

  public MonitorService(DLCMProperties dlcmProperties, AuthorizationClientProperties authClientProperties,
          MessageService messageService, FallbackRestClientService restClientService) {
    super(messageService, dlcmProperties);
    this.restClientService = restClientService;

    this.authServerUrl = authClientProperties.getAuthorizationServerUrl();
    this.adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
    this.dataMgmtUrl = dlcmProperties.getModule().getDataMgmt().getUrl();
    this.preingestUrl = dlcmProperties.getModule().getPreingest().getUrl();
    this.ingestUrl = dlcmProperties.getModule().getIngest().getUrl();
    this.accessUrl = dlcmProperties.getModule().getAccess().getUrl();
    this.archivalStorageUrls = dlcmProperties.getArchivalStorageUrls();
  }

  public JSONObject getModuleStatuses() {
    final JSONObject moduleStatus = new JSONObject();
    // Modules with unique instance
    moduleStatus.put("auth", this.getModuleStatus(this.authServerUrl));
    moduleStatus.put("admin", this.getModuleStatus(this.adminUrl));
    moduleStatus.put("dataManagement", this.getModuleStatus(this.dataMgmtUrl));
    moduleStatus.put("preingest", this.getModuleStatus(this.preingestUrl));
    moduleStatus.put("ingest", this.getModuleStatus(this.ingestUrl));
    moduleStatus.put("access", this.getModuleStatus(this.accessUrl));
    // Modules with multiple instances
    moduleStatus.put("archivalStorage", this.getModuleList(this.archivalStorageUrls));
    return moduleStatus;
  }

  private JSONArray getModuleList(String[] urls) {
    final JSONArray moduleList = new JSONArray();
    for (final String url : urls) {
      moduleList.put(this.getModuleStatus(url));
    }
    return moduleList;
  }

  private JSONObject getModuleStatus(String url) {
    try {
      return new JSONObject(this.restClientService.getResource(url)).put("status", "UP");
    } catch (final Exception e) {
      return new JSONObject().put("status", "DOWN");
    }
  }

}

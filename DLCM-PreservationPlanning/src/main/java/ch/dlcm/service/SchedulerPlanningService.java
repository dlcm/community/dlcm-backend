/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - SchedulerPlanningService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.PreservationJobService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.model.preservation.JobRecurrence;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.rest.fallback.FallbackPreservationJobRemoteResourceService;

@Component
@ConditionalOnBean(PreservationPlanningController.class)
public class SchedulerPlanningService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(SchedulerPlanningService.class);

  private final int jobPageSize;

  private final PreservationJobService preservationJobService;
  private final FallbackPreservationJobRemoteResourceService preservationJobRemoteResourceService;

  public SchedulerPlanningService(DLCMProperties dlcmProperties, MessageService messageService,
          FallbackPreservationJobRemoteResourceService preservationJobRemoteResourceService,
          PreservationJobService preservationJobService) {
    super(messageService, dlcmProperties);
    this.jobPageSize = dlcmProperties.getPreservationPlanning().getJobPageSize();
    this.preservationJobService = preservationJobService;
    this.preservationJobRemoteResourceService = preservationJobRemoteResourceService;
  }

  @Scheduled(fixedRate = SolidifyConstants.MILLISECONDS_IN_HOUR, initialDelay = SolidifyConstants.MILLISECONDS_IN_MINUTE)
  public void scheduledRecurrenceJob() {
    final Pageable pageable = PageRequest.of(0, this.jobPageSize);

    final Page<PreservationJob> jobList = this.preservationJobService.findAll(pageable);
    for (final PreservationJob job : jobList) {
      if (job.isEnable() && job.getJobRecurrence() != JobRecurrence.ONCE && this.checkFrequency(job)) {
        try {
          this.preservationJobRemoteResourceService.startPreservationJob(job);
          log.info("Preservation Job {} ({}, {}) started", job.getResId(), job.getJobType(),
                  job.getName());
        } catch (final Exception e) {
          log.info("Preservation Job {} ({}, {}) cannot be started: {}", job.getResId(),
                  job.getJobType(), job.getName(), e.getMessage());
        }
      }
    }
  }

  /**
   * Compare current datetime with Preservation job scheduling, and return true if the job must be
   * started now. Depending on the PreservationJob recurrence type, the following scheduling
   * properties are compared:
   * <p>
   * DAILY:
   * <ul>
   * <li>hour</li>
   * </ul>
   * <p>
   * WEEKLY:
   * <ul>
   * <li>day of week</li>
   * <li>hour</li>
   * </ul>
   * <p>
   * MONTHLY:
   * <ul>
   * <li>day of month</li>
   * <li>hour</li>
   * </ul>
   * <p>
   * YEARLY:
   * <ul>
   * <li>month</li>
   * <li>day of month</li>
   * <li>hour</li>
   * </ul>
   *
   * @param job The preservation job to check
   * @return true if the job must be executed now
   */
  private boolean checkFrequency(PreservationJob job) {
    switch (job.getJobRecurrence()) {
      case DAILY -> {
        if (OffsetDateTime.now(ZoneOffset.UTC).getHour() == job.getScheduling().getHour()) {
          return true;
        }
      }
      case WEEKLY -> {
        if (OffsetDateTime.now(ZoneOffset.UTC).getDayOfWeek().getValue() == job.getScheduling().getWeekDay()
                && OffsetDateTime.now(ZoneOffset.UTC).getHour() == job.getScheduling().getHour()) {
          return true;
        }
      }
      case MONTHLY -> {
        if (OffsetDateTime.now(ZoneOffset.UTC).getDayOfMonth() == job.getScheduling().getMonthDay()
                && OffsetDateTime.now(ZoneOffset.UTC).getHour() == job.getScheduling().getHour()) {
          return true;
        }
      }
      case YEARLY -> {
        if (OffsetDateTime.now(ZoneOffset.UTC).getMonth().getValue() == job.getScheduling().getMonth()
                && OffsetDateTime.now(ZoneOffset.UTC).getDayOfMonth() == job.getScheduling().getMonthDay()
                && OffsetDateTime.now(ZoneOffset.UTC).getHour() == job.getScheduling().getHour()) {
          return true;
        }
      }
      default -> {
        return false;
      }
    }
    return false;
  }
}

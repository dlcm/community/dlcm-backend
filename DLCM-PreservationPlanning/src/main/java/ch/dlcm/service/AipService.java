/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - AipService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.exception.DLCMCompareException;
import ch.dlcm.exception.DLCMReplicationException;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.AipStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preservation.AipCopy;
import ch.dlcm.model.preservation.AipCopyList;
import ch.dlcm.model.preservation.AipCopySearchType;

@Service
@ConditionalOnBean(PreservationPlanningController.class)
public class AipService extends DLCMService {

  private static final Logger log = LoggerFactory.getLogger(AipService.class);

  private final AipCachedService aipCachedService;

  public AipService(MessageService messageService, AipCachedService aipCachedService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.aipCachedService = aipCachedService;
  }

  public void deleteCache() {
    this.aipCachedService.deleteCache();
  }

  public Page<AipCopyList> findAll(AipCopySearchType searchType, Pageable pageable, boolean returnPublicUrls) {
    try {
      return switch (searchType) {
        case ALL_COPIES -> this.findAllAipCopies(pageable, returnPublicUrls);
        case ALL_COPIES_COMPLETED -> this.findAllAipCopiesCompleted(pageable, returnPublicUrls);
        case SOME_COPY_MISSING -> this.findAllAipCopiesMissing(pageable, returnPublicUrls);
        case SOME_COPY_IN_ERROR -> this.findAllAipCopiesWithError(pageable, returnPublicUrls);
        case SOME_COPY_MISC_STATUS -> this.findAllAipCopiesWithMiscStatus(pageable, returnPublicUrls);
        case SOME_COPY_NOT_UPDATED -> this.findAllAipCopiesNotUpdated(pageable, returnPublicUrls);
        default -> throw new UnsupportedOperationException("Not implemented");
      };
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return Page.empty();
    }
  }

  /**
   * Return the first AIP found copy that has a COMPLETED status It may be stored on any storage node.
   *
   * @param aipId
   * @param returnPublicUrls Indicate if public storage node urls must be used to build the AIP urls
   * @return
   */
  public Map.Entry<String, ArchivalInfoPackage> findFirstCompletedCopy(String aipId, boolean returnPublicUrls) {

    final Map<String, ArchivalInfoPackage> aipList = this.aipCachedService.loadAips(aipId, returnPublicUrls);

    final Optional<Map.Entry<String, ArchivalInfoPackage>> sourceAip = aipList.entrySet().stream()
            .filter(entry -> entry.getValue() != null)
            .filter(entry -> entry.getValue().isCompleted())
            .findFirst();

    if (sourceAip.isEmpty()) {
      final String msg = "Could not find first copy of AIP (" + aipId + ")";
      log.error(msg);
      throw new DLCMReplicationException(msg);
    }

    return sourceAip.get();
  }

  /**
   * Return a map of each storage node url, associated with the corresponding completed AIP,
   *
   * @param aipId
   * @param returnPublicUrls Indicate if public storage node urls must be used to build the AIP urls
   * @return
   */
  public Map<String, ArchivalInfoPackage> loadCompletedAips(String aipId, boolean returnPublicUrls) {

    final Map<String, ArchivalInfoPackage> aipList = this.aipCachedService.loadAips(aipId, returnPublicUrls);
    return aipList.entrySet().stream().filter(entry -> entry.getValue() != null)
            .filter(entry -> entry.getValue().isCompleted())
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  private Page<AipCopyList> findAllAipCopies(Pageable pageable, boolean returnPublicUrls) {
    final List<String> aipIdsToReturn = this.aipCachedService.findAllAipIdsFromAllStorageNodes(pageable.getSort());
    return this.getPagedAipCopyList(aipIdsToReturn, pageable, returnPublicUrls);
  }

  private Page<AipCopyList> findAllAipCopiesCompleted(Pageable pageable, boolean returnPublicUrls) {
    final Map<String, RestCollection<AipStatus>> allAipByNode = this.aipCachedService.fetchAllAipStatusFromAllStorageNodes(pageable.getSort());

    List<String> aipIdsToReturn = new ArrayList<>();

    for (final RestCollection<AipStatus> aipCollectionOnNode : allAipByNode.values()) {

      final List<String> completedAipIdsOnStorageNode = StreamSupport.stream(aipCollectionOnNode.getData().spliterator(), false)
              .filter(aipStatus -> PackageStatus.isCompletedProcess(aipStatus.getStatus()))
              .map(AipStatus::getResId)
              .toList();

      if (aipIdsToReturn.isEmpty()) {
        aipIdsToReturn.addAll(completedAipIdsOnStorageNode);
      } else {
        // keep only AIP that are already in the list as we want the ones that are everywhere
        aipIdsToReturn.retainAll(completedAipIdsOnStorageNode);
      }
    }

    return this.getPagedAipCopyList(aipIdsToReturn, pageable, returnPublicUrls);
  }

  private Page<AipCopyList> findAllAipCopiesMissing(Pageable pageable, boolean returnPublicUrls) {
    final List<String> allCombinedAipIds = this.aipCachedService.findAllAipIdsFromAllStorageNodes(pageable.getSort());

    final Map<String, RestCollection<AipStatus>> allAipByNode = this.aipCachedService.fetchAllAipStatusFromAllStorageNodes(pageable.getSort());

    final List<String> aipIdsToReturn = new ArrayList<>();

    for (final RestCollection<AipStatus> aipCollectionOnNode : allAipByNode.values()) {
      final List<String> aipIdsOnStorageNode = StreamSupport.stream(aipCollectionOnNode.getData().spliterator(), false)
              .map(AipStatus::getResId)
              .toList();

      final List<String> missingAipIds = allCombinedAipIds.stream().filter(aipId -> !aipIdsOnStorageNode.contains(aipId))
              .filter(aipId -> !aipIdsToReturn.contains(aipId)).toList();

      aipIdsToReturn.addAll(missingAipIds);
    }

    return this.getPagedAipCopyList(aipIdsToReturn, pageable, returnPublicUrls);
  }

  private Page<AipCopyList> findAllAipCopiesWithError(Pageable pageable, boolean returnPublicUrls) {
    final Map<String, RestCollection<AipStatus>> allAIPsByNode = this.aipCachedService.fetchAllAipStatusFromAllStorageNodes(pageable.getSort());

    final List<String> aipIdsToReturn = new ArrayList<>();

    for (final RestCollection<AipStatus> aipCollectionOnNode : allAIPsByNode.values()) {

      final List<String> errorAipIds = StreamSupport.stream(aipCollectionOnNode.getData().spliterator(), false)
              .filter(aipStatus -> PackageStatus.isInError(aipStatus.getStatus()))
              .map(AipStatus::getResId)
              .toList();

      aipIdsToReturn.addAll(errorAipIds);
    }

    return this.getPagedAipCopyList(aipIdsToReturn, pageable, returnPublicUrls);
  }

  private Page<AipCopyList> findAllAipCopiesWithMiscStatus(Pageable pageable, boolean returnPublicUrls) {
    final Map<String, RestCollection<AipStatus>> allAIPsByNode = this.aipCachedService.fetchAllAipStatusFromAllStorageNodes(pageable.getSort());

    final List<String> aipIdsToReturn = new ArrayList<>();

    for (final RestCollection<AipStatus> aipCollectionOnNode : allAIPsByNode.values()) {

      final List<String> miscStatusAipIds = StreamSupport.stream(aipCollectionOnNode.getData().spliterator(), false)
              .filter(aipStatus -> !PackageStatus.isInError(aipStatus.getStatus()) && !PackageStatus.isCompletedProcess(aipStatus.getStatus()))
              .map(AipStatus::getResId)
              .toList();

      aipIdsToReturn.addAll(miscStatusAipIds);
    }

    return this.getPagedAipCopyList(aipIdsToReturn, pageable, returnPublicUrls);
  }

  private Page<AipCopyList> findAllAipCopiesNotUpdated(Pageable pageable, boolean returnPublicUrls) {
    final List<String> allCombinedAipIds = this.aipCachedService.findAllAipIdsFromAllStorageNodes(pageable.getSort());

    // Map that contains for each aip(key), the list of nodes where the aip is not updated
    final Map<String, List<String>> aipIdsMapToReturn = new HashMap<>();

    // for each aip, check all nodes and compare each of them
    for (final String aipId : allCombinedAipIds) {
      final Map<String, ArchivalInfoPackage> aipMap = this.aipCachedService.loadAips(aipId, returnPublicUrls);
      // Find most recent copy -> last updated copy
      final Optional<Map.Entry<String, ArchivalInfoPackage>> lastUpdateAipCopyOpt = aipMap
              .entrySet()
              .stream()
              .filter(entry -> entry.getValue() != null)
              .max(Comparator.comparing(e -> e.getValue().getLastUpdate().getWhen()));

      if (lastUpdateAipCopyOpt.isPresent()) {
        // Compare each aip with the most recent copy to determine if there is any copy not updated
        aipMap.entrySet()
                .stream()
                .filter(entry -> entry.getValue() != null)
                .filter(entry -> !entry.getValue().isInProgress())
                .forEach(entry -> {
                  try {
                    // Compare AIP with source
                    this.compare(lastUpdateAipCopyOpt.get().getValue(), entry.getValue());
                  } catch (SolidifyCheckingException | SolidifyProcessingException | DLCMCompareException e) {
                    log.error("AIP Comparison Error [{}] => {}", entry.getValue().getResId(), e.getMessage());
                    if (!aipIdsMapToReturn.containsKey(entry.getValue().getResId())) {
                      aipIdsMapToReturn.put(entry.getValue().getResId(), new ArrayList<>());
                    }
                    aipIdsMapToReturn.get(entry.getValue().getResId()).add(entry.getKey());
                  }
                });
      }
    }
    return this.getPagedAipCopyList(aipIdsMapToReturn, pageable, returnPublicUrls);
  }

  private Page<AipCopyList> getPagedAipCopyList(List<String> aipIds, Pageable pageable, boolean returnPublicUrls) {

    /*
     * Sort aipIds the same way as allAipIdsFromAllStorageNodes
     */
    final List<String> allAipIdsFromAllStorageNodes = this.aipCachedService.findAllAipIdsFromAllStorageNodes(pageable.getSort());
    final List<String> resultAipIds = new ArrayList<>();
    for (final String aipId : allAipIdsFromAllStorageNodes) {
      if (aipIds.contains(aipId)) {
        resultAipIds.add(aipId);
      }
    }

    final int start = (int) pageable.getOffset();
    final int end = Math.min((start + pageable.getPageSize()), resultAipIds.size());

    final List<String> pagedAipIdsToReturn = resultAipIds.subList(start, end);

    final List<AipCopyList> aipCopyLists = new ArrayList<>();
    for (final String aipId : pagedAipIdsToReturn) {
      final Map<String, ArchivalInfoPackage> aipMap = this.aipCachedService.loadAips(aipId, returnPublicUrls);
      final AipCopyList aipCopyList = new AipCopyList(aipId);

      for (final Map.Entry<String, ArchivalInfoPackage> entry : aipMap.entrySet()) {
        final AipCopy item = new AipCopy(entry.getKey(), entry.getValue());
        aipCopyList.getCopies().add(item);
      }

      aipCopyLists.add(aipCopyList);
    }

    return new PageImpl<>(aipCopyLists, pageable, resultAipIds.size());
  }

  private Page<AipCopyList> getPagedAipCopyList(Map<String, List<String>> aipWithNodesNotUpdatedMap, Pageable pageable,
          boolean returnPublicUrls) {

    /*
     * Sort aipIds the same way as allAipIdsFromAllStorageNodes
     */
    final List<String> allAipIdsFromAllStorageNodes = this.aipCachedService.findAllAipIdsFromAllStorageNodes(pageable.getSort());
    final List<String> resultAipIds = new ArrayList<>();
    for (final String aipId : allAipIdsFromAllStorageNodes) {
      if (aipWithNodesNotUpdatedMap.containsKey(aipId)) {
        resultAipIds.add(aipId);
      }

    }

    final int start = (int) pageable.getOffset();
    final int end = Math.min((start + pageable.getPageSize()), resultAipIds.size());

    final List<String> pagedAipIdsToReturn = resultAipIds.subList(start, end);

    final List<AipCopyList> aipCopyLists = new ArrayList<>();
    for (final String aipId : pagedAipIdsToReturn) {
      final Map<String, ArchivalInfoPackage> aipMap = this.aipCachedService.loadAips(aipId, returnPublicUrls);
      final AipCopyList aipCopyList = new AipCopyList(aipId);
      for (final Map.Entry<String, ArchivalInfoPackage> entry : aipMap.entrySet()) {
        // Check if there is an entry for this node and aip in the map that contains the non-updated
        AipCopy item;
        if (aipWithNodesNotUpdatedMap.containsKey(entry.getValue().getResId())
                && aipWithNodesNotUpdatedMap.get(entry.getValue().getResId()).contains(entry.getKey())) {
          item = new AipCopy(entry.getKey(), entry.getValue(), true);
        } else {
          item = new AipCopy(entry.getKey(), entry.getValue());
        }
        aipCopyList.getCopies().add(item);
      }

      aipCopyLists.add(aipCopyList);
    }

    return new PageImpl<>(aipCopyLists, pageable, resultAipIds.size());
  }

  /**
   * Compare a AIP to another by check 1) the status 2) the archive size 3) the archive checksums 4)
   * if the archive is disposed 4a) the tombstone size 4b) the tombstone checksums
   *
   * @param aip1
   * @param aip2
   */
  public void compare(ArchivalInfoPackage aip1, ArchivalInfoPackage aip2) {
    // Check if AIP are ready to be compared
    if (PackageStatus.isInError(aip1.getInfo().getStatus())
            || PackageStatus.isInError(aip2.getInfo().getStatus())) {
      throw new SolidifyProcessingException("AIP in error : " + aip1.getInfo().getStatus() + " or " + aip2.getInfo().getStatus());
    }
    if (PackageStatus.isInProgress(aip1.getInfo().getStatus())
            || PackageStatus.isInProgress(aip2.getInfo().getStatus())) {
      throw new SolidifyCheckingException("AIP not ready : " + aip1.getInfo().getStatus() + " or " + aip2.getInfo().getStatus());
    }
    if (PackageStatus.isDisposalInProgress(aip1.getInfo().getStatus())
            || PackageStatus.isDisposalInProgress(aip2.getInfo().getStatus())) {
      throw new SolidifyCheckingException("AIP disposable : " + aip1.getInfo().getStatus() + " or " + aip2.getInfo().getStatus());
    }
    // Archive Status
    if (!aip1.getInfo().getStatus().equals(aip2.getInfo().getStatus())) {
      throw new DLCMCompareException("Different status: " + aip1.getInfo().getStatus() + " <> " + aip2.getInfo().getStatus());
    }

    // Archive size
    if (aip1.getArchiveSize().compareTo(aip2.getArchiveSize()) != 0) {
      throw new DLCMCompareException("Different size: " + aip1.getArchiveSize() + " <> " + aip2.getArchiveSize());
    }
    // Archive checksum
    this.checkChecksums(aip1, aip2, DataFileChecksum.ChecksumOrigin.DLCM);

    // Tombstone
    if (aip1.getInfo().getStatus().equals(PackageStatus.DISPOSED)) {
      this.checkChecksums(aip1, aip2, DataFileChecksum.ChecksumOrigin.DLCM_TOMBSTONE);
      if (aip1.getTombstoneSize().compareTo(aip2.getTombstoneSize()) != 0) {
        throw new DLCMCompareException("Different tombstone size: " + aip1.getTombstoneSize() + " <> " + aip2.getTombstoneSize());
      }
    } else if (aip1.getTombstoneSize() != null
            && aip1.getTombstoneSize().compareTo(0L) != 0
            && aip1.getTombstoneSize().compareTo(aip2.getTombstoneSize()) != 0) {
      throw new DLCMCompareException("Tombstone size must be zero: " + aip1.getTombstoneSize() + " <> " + aip2.getTombstoneSize());
    }
    // Retention
    if (!aip1.getRetention().equals(aip2.getRetention())) {
      throw new DLCMCompareException("Different retention: " + aip1.getRetention() + " <> " + aip2.getRetention());
    }
    // Last archiving
    if (!aip1.getLastArchiving().equals(aip2.getLastArchiving())) {
      throw new DLCMCompareException("Different last archiving: " + aip1.getLastArchiving() + " <> " + aip2.getLastArchiving());
    }
  }

  /**
   * Checksum check by origin
   *
   * @param aip1
   * @param aip2
   * @param checksumOrigin
   */
  private void checkChecksums(ArchivalInfoPackage aip1, ArchivalInfoPackage aip2, DataFileChecksum.ChecksumOrigin checksumOrigin) {
    // Filtering on origin
    List<DataFileChecksum> checksumList1 = aip1.getChecksums(checksumOrigin);
    List<DataFileChecksum> checksumList2 = aip2.getChecksums(checksumOrigin);
    // Check
    for (final DataFileChecksum checksum1 : checksumList1) {
      for (final DataFileChecksum checksum2 : checksumList2) {
        if (checksum1.getChecksumAlgo() == checksum2.getChecksumAlgo() && checksum1.getChecksumType() == checksum2.getChecksumType()
                && !checksum1.getChecksum().equals(checksum2.getChecksum())) {
          throw new DLCMCompareException(
                  "Different " + checksum1.getChecksumAlgo() + " checksum: " + checksum1.getChecksum() + " <> " + checksum2.getChecksum());
        }
      }
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - AipCachedService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import static java.util.stream.Collectors.toList;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.model.oais.AipStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;

@Service
@ConditionalOnBean(PreservationPlanningController.class)
public class AipCachedService extends DLCMService {

  private static final Logger log = LoggerFactory.getLogger(AipCachedService.class);

  private final FallbackArchivalInfoPackageRemoteResourceService aipRemoteResourceService;
  private final String[] archivalStorageNodes;
  private final String[] archivalStoragePublicUrls;

  public AipCachedService(DLCMProperties dlcmProperties, MessageService messageService,
          FallbackArchivalInfoPackageRemoteResourceService trustedAipRemoteResourceService) {
    super(messageService, dlcmProperties);
    this.archivalStorageNodes = dlcmProperties.getArchivalStorageUrls();
    this.archivalStoragePublicUrls = dlcmProperties.getArchivalStoragePublicUrls();
    this.aipRemoteResourceService = trustedAipRemoteResourceService;
  }

  @CacheEvict(cacheNames = { DLCMCacheNames.AIP_BY_STORAGE_NODE, DLCMCacheNames.AIP_BY_STORAGE_NODE + "_ids" }, allEntries = true)
  public void deleteCache() {
    log.info("AIPCachedService: {} cache deleted", DLCMCacheNames.AIP_BY_STORAGE_NODE);
  }

  /**
   * Return a list of existing AIP for each storage node
   *
   * @return
   */
  @Cacheable(DLCMCacheNames.AIP_BY_STORAGE_NODE)
  public Map<String, RestCollection<AipStatus>> fetchAllAipStatusFromAllStorageNodes(Sort sort) {

    final Map<String, RestCollection<AipStatus>> allAips = new HashMap<>();

    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, sort);

    for (final String archivalStorageUrl : this.archivalStorageNodes) {
      final RestCollection<AipStatus> aips = this.aipRemoteResourceService.getAipStatusList(archivalStorageUrl, pageable);
      allAips.put(archivalStorageUrl, aips);
    }

    return allAips;
  }

  /**
   * Return a list of all AIP ids combined with results of all storage nodes
   *
   * @return
   */
  @Cacheable(DLCMCacheNames.AIP_BY_STORAGE_NODE + "_ids")
  public List<String> findAllAipIdsFromAllStorageNodes(Sort sort) {
    final Map<String, List<AipStatus>> allAipStatusesByAipIds = this.getAllAipStatusByAipIds(sort);
    final Map<OffsetDateTime, String> allAipIdsByCreationDate = this.getAllAipIdsByCreationDate(allAipStatusesByAipIds);
    return this.getSortedAIPids(allAipIdsByCreationDate, sort);
  }

  /**
   * Return a map of each storage node url, associated with the corresponding stored AIP
   *
   * @param aipId
   * @param returnPublicUrls Indicate if public storage node urls must be used to build the AIP urls
   * @return
   */
  @Cacheable(DLCMCacheNames.AIP_BY_STORAGE_NODE)
  public Map<String, ArchivalInfoPackage> loadAips(String aipId, boolean returnPublicUrls) {
    // Try to load AIP from each node
    final Map<String, ArchivalInfoPackage> aipList = new HashMap<>();
    for (final String archivalStorageUrl : this.archivalStorageNodes) {
      try {
        final ArchivalInfoPackage aip = this.aipRemoteResourceService.getAIP(archivalStorageUrl, aipId);
        aipList.put(archivalStorageUrl, aip);
      } catch (final SolidifyResourceNotFoundException e) {
        aipList.put(archivalStorageUrl, null);
      }
    }

    if (returnPublicUrls) {
      this.replaceInternalStorageUrlsByPublicUrls(aipList);
    }

    return aipList;
  }

  private Map<OffsetDateTime, String> getAllAipIdsByCreationDate(Map<String, List<AipStatus>> allAipStatusesByAipIds) {
    // for each AIP, get the creation date (= the lowest creation date of all copies)
    final Map<OffsetDateTime, String> allAipIdsByCreationDate = new HashMap<>();
    for (final Map.Entry<String, List<AipStatus>> entry : allAipStatusesByAipIds.entrySet()) {
      OffsetDateTime creationDate = null;
      for (final AipStatus aipStatus : entry.getValue()) {
        if (creationDate == null || aipStatus.getCreationTime().isBefore(creationDate)) {
          creationDate = aipStatus.getCreationTime();
        }
      }
      allAipIdsByCreationDate.put(creationDate, entry.getKey());
    }
    return allAipIdsByCreationDate;
  }

  private Map<String, List<AipStatus>> getAllAipStatusByAipIds(Sort sort) {
    final Map<String, List<AipStatus>> allAipStatusesByAipIds = new HashMap<>();

    for (final RestCollection<AipStatus> aipStatusesOnNode : this.fetchAllAipStatusFromAllStorageNodes(sort).values()) {

      for (final AipStatus aipStatus : aipStatusesOnNode.getData()) {
        // ensure list exists for AIP id
        allAipStatusesByAipIds.computeIfAbsent(aipStatus.getResId(), k -> new ArrayList<>());
        // add AIP copy to list
        allAipStatusesByAipIds.get(aipStatus.getResId()).add(aipStatus);
      }

    }

    return allAipStatusesByAipIds;
  }

  private List<String> getSortedAIPids(Map<OffsetDateTime, String> allAipIdsByCreationDate, Sort sort) {
    // Sort all unique AIP on their creation date, according to given Sort
    // if no sort is given it is sorted descending to get last created AIP first
    final String sortProperty = "creation.when";
    SortedSet<OffsetDateTime> keys;
    Sort.Order order = sort.getOrderFor(sortProperty);
    if (order != null && order.isAscending()) {
      keys = new TreeSet<>(allAipIdsByCreationDate.keySet());
    } else {
      keys = new TreeSet<>(Collections.reverseOrder());
      keys.addAll(allAipIdsByCreationDate.keySet());
    }
    return keys.stream().map(allAipIdsByCreationDate::get).collect(toList());
  }

  private void replaceInternalStorageUrlsByPublicUrls(Map<String, ArchivalInfoPackage> urlIndexedMap) {

    final String[] storagePublicUrls = this.archivalStoragePublicUrls;

    if (this.archivalStorageNodes.length == storagePublicUrls.length) {
      for (int i = 0; i < this.archivalStorageNodes.length; i++) {
        if (!this.archivalStorageNodes[i].equals(storagePublicUrls[i])) {
          final String oldKeyUrl = this.archivalStorageNodes[i];
          if (urlIndexedMap.containsKey(oldKeyUrl)) {
            final String newKeyUrl = storagePublicUrls[i];
            final ArchivalInfoPackage value = urlIndexedMap.remove(oldKeyUrl);
            urlIndexedMap.put(newKeyUrl, value);
          }
        }
      }
    } else {
      throw new SolidifyRuntimeException(
              "internal archival-storage urls number is different from the number of public archival-storage urls in application config");
    }
  }
}

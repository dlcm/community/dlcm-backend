/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - JobService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.PreservationJobService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.job.AbstractJob;
import ch.dlcm.job.ApplyAipActionJob;
import ch.dlcm.job.CheckComplianceLevelJob;
import ch.dlcm.job.CheckReplicationJob;
import ch.dlcm.job.CleanSubmissionJob;
import ch.dlcm.job.ComplianceLevelUpdateJob;
import ch.dlcm.job.DisposalJob;
import ch.dlcm.job.MetadataMigrationJob;
import ch.dlcm.job.PreloadAipJob;
import ch.dlcm.job.PurgeOrderJob;
import ch.dlcm.job.PurgeSubmissionTempFilesJob;
import ch.dlcm.job.ReloadAipJob;
import ch.dlcm.job.ReplicationJob;
import ch.dlcm.message.JobMessage;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.replication.ReplicationService;
import ch.dlcm.service.rest.trusted.TrustedAIPDownloadRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchiveDownloadRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchiveStatisticsRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDisseminationInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrderRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedSubmissionPolicyRemoteResourceService;

@Service
@ConditionalOnBean(PreservationPlanningController.class)
public class JobService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(JobService.class);

  private final JobProcessingService jobProcessingService;
  private final PreservationJobService preservationJobService;
  private final ReplicationService replicationService;
  private final TrustedSubmissionPolicyRemoteResourceService submissionPolicyRemoteResourceService;
  private final TrustedDepositRemoteResourceService depositRemoteResourceService;
  private final TrustedSubmissionInfoPackageRemoteResourceService sipRemoteResourceService;
  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;
  private final TrustedArchivePublicMetadataRemoteResourceService archiveMetadataRemoteResourceService;
  private final TrustedArchiveDownloadRemoteResourceService archiveDownloadRemoteResourceService;
  private final TrustedOrderRemoteResourceService orderRemoteResourceService;
  private final TrustedDisseminationInfoPackageRemoteResourceService disseminationInfoPackageRemoteResourceService;
  private final TrustedAIPDownloadRemoteResourceService aipDownloadRemoteResourceService;
  private final TrustedArchiveStatisticsRemoteResourceService archiveStatisticsRemoteResourceService;
  private final DLCMProperties dlcmProperties;

  private final int nbProcessors = Runtime.getRuntime().availableProcessors();
  private final ExecutorService executorService = Executors.newFixedThreadPool(this.nbProcessors);

  public JobService(DLCMProperties dlcmProperties,
          MessageService messageService,
          PreservationJobService preservationJobService,
          JobProcessingService jobProcessingService,
          ReplicationService replicationService,
          TrustedSubmissionPolicyRemoteResourceService submissionPolicyRemoteResourceService,
          TrustedDepositRemoteResourceService depositRemoteResourceService,
          TrustedSubmissionInfoPackageRemoteResourceService sipRemoteResourceService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService,
          TrustedArchivePublicMetadataRemoteResourceService archiveMetadataRemoteResourceService,
          TrustedArchiveDownloadRemoteResourceService archiveDownloadRemoteResourceService,
          TrustedOrderRemoteResourceService orderRemoteResourceService,
          TrustedDisseminationInfoPackageRemoteResourceService disseminationInfoPackageRemoteResourceService,
          TrustedAIPDownloadRemoteResourceService aipDownloadRemoteResourceService,
          TrustedArchiveStatisticsRemoteResourceService archiveStatisticsRemoteResourceService) {
    super(messageService, dlcmProperties);
    this.preservationJobService = preservationJobService;
    this.replicationService = replicationService;
    this.jobProcessingService = jobProcessingService;
    this.submissionPolicyRemoteResourceService = submissionPolicyRemoteResourceService;
    this.depositRemoteResourceService = depositRemoteResourceService;
    this.sipRemoteResourceService = sipRemoteResourceService;
    this.aipRemoteResourceService = aipRemoteResourceService;
    this.archiveMetadataRemoteResourceService = archiveMetadataRemoteResourceService;
    this.archiveDownloadRemoteResourceService = archiveDownloadRemoteResourceService;
    this.orderRemoteResourceService = orderRemoteResourceService;
    this.disseminationInfoPackageRemoteResourceService = disseminationInfoPackageRemoteResourceService;
    this.aipDownloadRemoteResourceService = aipDownloadRemoteResourceService;
    this.archiveStatisticsRemoteResourceService = archiveStatisticsRemoteResourceService;
    this.dlcmProperties = dlcmProperties;
  }

  @JmsListener(destination = "${dlcm.queue.job}")
  public void receivedJobMessage(JobMessage jobMessage) {
    this.executorService.submit(() -> {
      try {
        this.processJob(jobMessage);
      } catch (RuntimeException t) {
        log.error("Cannot run preservation job thread", t);
      }
    });
  }

  private void processJob(JobMessage jobMessage) {
    log.info("Reading {}", jobMessage);

    final PreservationJob job = this.preservationJobService.findOne(jobMessage.getResId());

    AbstractJob jobProcess = switch (job.getJobType()) {
      case METADATA_MIGRATION -> new MetadataMigrationJob(this.dlcmProperties, this.messageService, this.jobProcessingService,
              this.depositRemoteResourceService, this.sipRemoteResourceService, this.aipRemoteResourceService);
      case COMPLIANCE_LEVEL_UPDATE -> new ComplianceLevelUpdateJob(this.dlcmProperties, this.messageService, this.jobProcessingService,
              this.aipRemoteResourceService);
      case RELOAD -> new ReloadAipJob(this.dlcmProperties, this.messageService, this.jobProcessingService, this.aipRemoteResourceService);
      case REPLICATION -> new ReplicationJob(this.dlcmProperties, this.messageService, this.jobProcessingService, this.replicationService,
              this.archiveMetadataRemoteResourceService);
      case REPLICATION_CHECK -> new CheckReplicationJob(this.dlcmProperties, this.messageService, this.jobProcessingService,
              this.replicationService, this.archiveMetadataRemoteResourceService);
      case ARCHIVE_CHECK, FIXITY, REINDEX, REINDEX_ALL -> new ApplyAipActionJob(this.dlcmProperties, this.messageService,
              this.jobProcessingService, this.aipRemoteResourceService);
      case ARCHIVE_PRELOAD_SMALL, ARCHIVE_PRELOAD_BIG -> new PreloadAipJob(this.dlcmProperties, this.messageService, this.jobProcessingService,
              this.archiveMetadataRemoteResourceService, this.archiveDownloadRemoteResourceService);
      case CLEAN_SUBMISSION -> new CleanSubmissionJob(this.dlcmProperties, this.messageService, this.jobProcessingService,
              this.submissionPolicyRemoteResourceService, this.depositRemoteResourceService, this.sipRemoteResourceService,
              this.aipRemoteResourceService, true);
      case SIMPLE_CLEAN_SUBMISSION -> new CleanSubmissionJob(this.dlcmProperties, this.messageService, this.jobProcessingService,
              this.submissionPolicyRemoteResourceService, this.depositRemoteResourceService, this.sipRemoteResourceService,
              this.aipRemoteResourceService, false);
      case PURGE_SUBMISSION_TEMP_FILES -> new PurgeSubmissionTempFilesJob(this.dlcmProperties, this.messageService, this.jobProcessingService,
              this.depositRemoteResourceService, this.sipRemoteResourceService, this.aipRemoteResourceService,
              this.archiveMetadataRemoteResourceService);
      case PURGE_ORDER -> new PurgeOrderJob(this.dlcmProperties, this.messageService, this.jobProcessingService, this.orderRemoteResourceService,
              this.disseminationInfoPackageRemoteResourceService, this.aipDownloadRemoteResourceService,
              this.archiveStatisticsRemoteResourceService);
      case DISPOSAL -> new DisposalJob(this.dlcmProperties, this.messageService, this.jobProcessingService,
              this.archiveMetadataRemoteResourceService, this.aipRemoteResourceService);
      case CHECK_COMPLIANCE_LEVEL -> new CheckComplianceLevelJob(this.dlcmProperties, this.messageService, this.jobProcessingService,
              this.depositRemoteResourceService);
      default -> throw new UnsupportedOperationException("Unsupported job type " + job.getJobType());
    };
    jobProcess.run(job.getResId(), jobMessage.getExecutionId());

    log.info("Preservation Job processed: resId={}[{}]", job.getResId(), job.getJobType().getLabel());
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - ModuleList.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.model;

import java.util.Arrays;
import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.OAIProperties;
import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;

@Schema(description = "The functional module list.")
public class ModuleList extends RepresentationModel<ModuleList> {

  @Schema(description = "The URL of Authoriration module.")
  private final String authorization;
  @Schema(description = "The URL of Admin module.")
  private final String admin;
  @Schema(description = "The URL of Data Management module.")
  private final String dataManagement;
  @Schema(description = "The URL of Index module.")
  private final String index;
  @Schema(description = "The URL of Pre-Ingest module.")
  private final String preingest;
  @Schema(description = "The URL of Ingest module.")
  private final String ingest;
  @Schema(description = "The URL of Access module.")
  private final String access;
  @Schema(description = "The URL of OAI-PMH module.")
  private final String oaiInfo;
  @Schema(description = "The URL of Preservation Planning module.")
  private final String preservationPlanning;
  @Schema(description = "The URL of OAI-PMH provider.")
  private final String oaiPmh;
  @Schema(description = "The URL list of Archival Storage module.")
  private final String[] archivalStorage;

  public ModuleList(DLCMProperties dlcmProperties, AuthorizationClientProperties authClientProperties, OAIProperties oaiProperties) {
    // Modules with unique instance
    this.authorization = authClientProperties.getPublicAuthorizationServerUrl();
    this.admin = dlcmProperties.getModule().getAdmin().getPublicUrl();
    this.dataManagement = dlcmProperties.getModule().getDataMgmt().getPublicUrl();
    this.index = this.getLinkedModuleUrl(this.dataManagement, IndexConstants.INDEX_MODULE);
    this.preingest = dlcmProperties.getModule().getPreingest().getPublicUrl();
    this.ingest = dlcmProperties.getModule().getIngest().getPublicUrl();
    this.access = dlcmProperties.getModule().getAccess().getPublicUrl();
    this.preservationPlanning = dlcmProperties.getModule().getPreservationPlanning().getPublicUrl();
    this.oaiInfo = this.getLinkedModuleUrl(this.access, OAIConstants.OAI_MODULE);
    if (StringTool.isNullOrEmpty(oaiProperties.getPublicUrl())) {
      this.oaiPmh = this.oaiInfo + "/" + OAIConstants.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE;
    } else {
      this.oaiPmh = oaiProperties.getPublicUrl();
    }
    // Modules with multiple instances
    this.archivalStorage = dlcmProperties.getArchivalStoragePublicUrls();
  }

  public String getAuthorization() {
    return this.authorization;
  }

  public String getAdmin() {
    return this.admin;
  }

  public String getDataManagement() {
    return this.dataManagement;
  }

  public String getIndex() {
    return this.index;
  }

  public String getPreingest() {
    return this.preingest;
  }

  public String getIngest() {
    return this.ingest;
  }

  public String getAccess() {
    return this.access;
  }

  public String getPreservationPlanning() {
    return this.preservationPlanning;
  }

  public String getOaiInfo() {
    return this.oaiInfo;
  }

  public String getOaiPmh() {
    return this.oaiPmh;
  }

  public String[] getArchivalStorage() {
    return this.archivalStorage;
  }

  private String getLinkedModuleUrl(String mainModuleUrl, String linkedModuleName) {
    return mainModuleUrl.substring(0, mainModuleUrl.lastIndexOf("/") + 1) + linkedModuleName;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Arrays.hashCode(this.archivalStorage);
    result = prime * result + Objects.hash(this.access, this.admin, this.authorization, this.dataManagement, this.index, this.ingest, this.preservationPlanning, this.oaiInfo, this.oaiPmh, this.preingest);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    ModuleList other = (ModuleList) obj;
    return Objects.equals(this.access, other.access) && Objects.equals(this.admin, other.admin) && Arrays.equals(this.archivalStorage, other.archivalStorage)
            && Objects.equals(this.authorization, other.authorization) && Objects.equals(this.dataManagement, other.dataManagement) && Objects.equals(this.index, other.index)
            && Objects.equals(this.ingest, other.ingest) && Objects.equals(this.preservationPlanning, other.preservationPlanning) && Objects.equals(this.oaiInfo, other.oaiInfo)
            && Objects.equals(this.oaiPmh, other.oaiPmh) && Objects.equals(this.preingest, other.preingest);
  }

}

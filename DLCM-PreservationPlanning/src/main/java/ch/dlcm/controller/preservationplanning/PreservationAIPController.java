/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - PreservationAIPController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preservationplanning;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.AdminPermissions;

import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.model.preservation.AipCopyList;
import ch.dlcm.model.preservation.AipCopySearchType;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.AipService;

@AdminPermissions
@RestController
@ConditionalOnBean(PreservationPlanningController.class)
@RequestMapping(UrlPath.PRES_PLANNING_AIP)
public class PreservationAIPController extends SolidifyController implements ControllerWithHateoasHome {

  private final AipService aipService;

  public PreservationAIPController(AipService aipService) {
    this.aipService = aipService;
  }

  @PostMapping("/" + ActionName.DELETE_CACHE)
  public HttpEntity<String> evictCache() {
    this.aipService.deleteCache();
    return new HttpEntity<>("OK");
  }

  @GetMapping
  public HttpEntity<RestCollection<AipCopyList>> list(
          @RequestParam(name = "searchType", required = false, defaultValue = "ALL_COPIES") AipCopySearchType searchType, Pageable pageable) {

    final Page<AipCopyList> listItem = this.aipService.findAll(searchType, pageable, true);
    return this.page2Collection(listItem, pageable);
  }

  protected void addLinks(AipCopyList aipCopyList) {
    aipCopyList.removeLinks();
    aipCopyList.addLinks(linkTo(this.getClass()));
  }

  protected HttpEntity<RestCollection<AipCopyList>> page2Collection(Page<AipCopyList> listItem, Pageable pageable) {
    for (final AipCopyList aipCopyList : listItem) {
      this.addLinks(aipCopyList);
    }
    final RestCollection<AipCopyList> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(this.getClass(), pageable).withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass(), pageable)).toUriComponentsBuilder()).withRel(ActionName.MODULE));
    this.addSortLinks(linkTo(this.getClass(), pageable), collection);
    this.addPageLinks(linkTo(this.getClass(), pageable), collection, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }
}

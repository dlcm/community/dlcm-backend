/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - ExecutionReportController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preservationplanning;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.AdminPermissions;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.JobExecutionReportService;
import ch.dlcm.controller.DLCMCompositionController;
import ch.dlcm.model.preservation.JobExecutionReport;
import ch.dlcm.model.preservation.JobExecutionReportLine;
import ch.dlcm.repository.JobExecutionLineReportRepository;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.service.HistoryService;

@AdminPermissions
public abstract class ExecutionReportController<T extends Resource & RemoteResourceContainer, V extends Resource>
        extends DLCMCompositionController<T, V> {

  protected JobExecutionLineReportRepository reportLineRepository;

  protected JobExecutionReportService reportService;

  public ExecutionReportController(HistoryService historyService, JobExecutionLineReportRepository reportLineRepository,
          JobExecutionReportService reportService) {
    super(historyService);
    this.reportLineRepository = reportLineRepository;
    this.reportService = reportService;
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.REPORT + "/" + DLCMConstants.REPORT_ID)
  public HttpEntity<JobExecutionReport> report(@PathVariable String parentid, @PathVariable String id, @PathVariable String reportId) {
    if (this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, false) == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    final JobExecutionReport report = this.reportService.findOne(reportId);
    report.add(linkTo(methodOn(this.getClass()).report(parentid, id, reportId)).withSelfRel());
    report.add(Tool.parentLink((linkTo(methodOn(this.getClass())
            .reports(parentid, id, null))).toUriComponentsBuilder()).withRel(ActionName.PARENT));
    report.add(linkTo(methodOn(this.getClass()).reportLines(parentid, id, reportId, null)).withRel(DLCMActionName.REPORT_LINES));
    return new ResponseEntity<>(report, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.REPORT + "/" + DLCMConstants.REPORT_ID
          + "/" + DLCMActionName.REPORT_LINES)
  public HttpEntity<RestCollection<JobExecutionReportLine>> reportLines(@PathVariable String parentid, @PathVariable String id,
          @PathVariable String reportId, Pageable pageable) {
    if (this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, false) == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    final Page<JobExecutionReportLine> reportLines = this.reportLineRepository.findByReportId(reportId, pageable);
    final RestCollection<JobExecutionReportLine> collection = new RestCollection<>(reportLines, pageable);
    collection.add(linkTo(methodOn(this.getClass()).reportLines(parentid, id, reportId, pageable)).withSelfRel());
    collection.add(Tool.parentLink((linkTo(methodOn(this.getClass())
            .reportLines(parentid, id, reportId, null))).toUriComponentsBuilder()).withRel(ActionName.PARENT));
    this.addPageLinks(linkTo(methodOn(this.getClass()).reportLines(parentid, id, reportId, pageable)), collection, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.REPORT)
  public HttpEntity<RestCollection<JobExecutionReport>> reports(@PathVariable String parentid, @PathVariable String id, Pageable pageable) {
    if (this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, false) == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    final Page<JobExecutionReport> listItem = this.reportService.findByJobExecutionId(id, pageable);
    final RestCollection<JobExecutionReport> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(methodOn(this.getClass()).reports(parentid, id, pageable)).withSelfRel());
    collection.add(Tool.parentLink((linkTo(methodOn(this.getClass())
            .reports(parentid, id, pageable))).toUriComponentsBuilder()).withRel(ActionName.PARENT));
    this.addPageLinks(linkTo(methodOn(this.getClass()).reports(parentid, id, pageable)), collection, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

}

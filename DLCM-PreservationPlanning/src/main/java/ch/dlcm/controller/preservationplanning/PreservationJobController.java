/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - PreservationJobController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preservationplanning;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;

import ch.dlcm.business.PreservationJobService;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.message.JobMessage;
import ch.dlcm.model.preservation.JobRecurrence;
import ch.dlcm.model.preservation.JobType;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.model.preservation.PreservationJob.JobStatus;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(PreservationPlanningController.class)
@RequestMapping(UrlPath.PRES_PLANNING_PRES_JOB)
public class PreservationJobController extends ResourceController<PreservationJob> {
  private static final Logger log = LoggerFactory.getLogger(PreservationJobController.class);

  @Override
  public HttpEntity<PreservationJob> create(@RequestBody PreservationJob t) {
    return super.create(t);
  }

  @Override
  public HttpEntity<PreservationJob> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<PreservationJob>> list(@ModelAttribute PreservationJob search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<PreservationJob> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @PostMapping("/" + DLCMActionName.INIT)
  public HttpEntity<Result> init() {
    final Result res = new Result();
    try {
      final int created = this.createDefaultJobs();
      res.setStatus(ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.job.init.success", new Object[] { created }));
    } catch (final Exception e) {
      res.setStatus(ActionStatus.NOT_EXECUTED);
      res.setMesssage(this.messageService.get("message.job.init.error", new Object[] { e.getMessage() }));
    }
    res.add(linkTo(this.getClass()).slash(DLCMActionName.INIT).withSelfRel());
    res.add(linkTo(this.getClass()).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_JOB_RECURRENCE)
  public HttpEntity<JobRecurrence[]> listJobRecurrences() {
    return new ResponseEntity<>(JobRecurrence.values(), HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_JOB_TYPE)
  public HttpEntity<JobType[]> listJobTypes() {
    return new ResponseEntity<>(JobType.values(), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.RESUME)
  public HttpEntity<Result> resume(@PathVariable String id) {
    final PreservationJob job = this.itemService.findOne(id);

    final Result res = new Result(job.getResId());
    if (!job.isEnable()) {
      res.setMesssage(this.messageService.get("message.job.disabled", new Object[] { id }));
    } else if (job.getLastExecutionStatus() == JobStatus.IN_PROGRESS) {
      res.setMesssage(this.messageService.get("message.job.inprogress", new Object[] { id }));
    } else if (job.getLastExecutionStatus() != JobStatus.IN_ERROR) {
      res.setMesssage(this.messageService.get("message.job.notresumable", new Object[] { id }));
    } else {
      res.setStatus(ActionStatus.NOT_EXECUTED);
      if (job.resume()) {
        this.itemService.save(job);
        res.setStatus(ActionStatus.EXECUTED);
        res.setMesssage(this.messageService.get("message.job.readytoresume", new Object[] { id }));
      } else {
        res.setMesssage(this.messageService.get("message.job.notreadytoresume", new Object[] { id }));
      }
    }
    if (res.getStatus() == ActionStatus.EXECUTED) {
      SolidifyEventPublisher.getPublisher().publishEvent(new JobMessage(id));
    }
    res.add(linkTo(this.getClass()).slash(id).slash(ActionName.RESUME).withSelfRel());
    res.add(linkTo(this.getClass()).slash(id).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.START)
  public HttpEntity<Result> start(@PathVariable String id) {
    final PreservationJob job = this.itemService.findOne(id);

    final Result res = new Result(job.getResId());
    if (!job.isEnable()) {
      res.setMesssage(this.messageService.get("message.job.disabled", new Object[] { id }));
    } else if (job.getLastExecutionStatus() == JobStatus.IN_PROGRESS) {
      res.setMesssage(this.messageService.get("message.job.inprogress", new Object[] { id }));
    } else {
      res.setStatus(ActionStatus.NOT_EXECUTED);
      if (job.start()) {
        this.itemService.save(job);
        res.setStatus(ActionStatus.EXECUTED);
        res.setMesssage(this.messageService.get("message.job.readytostart", new Object[] { id }));
      } else {
        res.setMesssage(this.messageService.get("message.job.notreadytostart", new Object[] { id }));
      }
    }
    if (res.getStatus() == ActionStatus.EXECUTED) {
      SolidifyEventPublisher.getPublisher().publishEvent(new JobMessage(id));
    }
    res.add(linkTo(this.getClass()).slash(id).slash(ActionName.START).withSelfRel());
    res.add(linkTo(this.getClass()).slash(id).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listJobTypes()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listJobRecurrences()).withRel(ActionName.VALUES));
  }

  private int createDefaultJobs() {
    int count = 0;
    if (this.createPreservationJob(JobType.PURGE_SUBMISSION_TEMP_FILES, JobRecurrence.DAILY)) {
      count++;
    }
    if (this.createPreservationJob(JobType.REPLICATION, JobRecurrence.DAILY)) {
      count++;
    }
    if (this.createPreservationJob(JobType.CLEAN_SUBMISSION, JobRecurrence.WEEKLY)) {
      count++;
    }
    if (this.createPreservationJob(JobType.SIMPLE_CLEAN_SUBMISSION, JobRecurrence.WEEKLY)) {
      count++;
    }
    if (this.createPreservationJob(JobType.FIXITY, JobRecurrence.WEEKLY)) {
      count++;
    }
    if (this.createPreservationJob(JobType.REPLICATION_CHECK, JobRecurrence.MONTHLY)) {
      count++;
    }
    if (this.createPreservationJob(JobType.ARCHIVE_CHECK, JobRecurrence.YEARLY)) {
      count++;
    }
    if (this.createPreservationJob(JobType.DISPOSAL, JobRecurrence.MONTHLY)) {
      count++;
    }
    if (this.createPreservationJob(JobType.ARCHIVE_PRELOAD_SMALL, JobRecurrence.DAILY)) {
      count++;
    }
    if (this.createPreservationJob(JobType.PURGE_ORDER, JobRecurrence.WEEKLY)) {
      count++;
    }
    return count;
  }

  private boolean createPreservationJob(JobType jobType, JobRecurrence jobFrequency) {
    if (((PreservationJobService) this.itemService).findByName(jobType.getLabel()) == null) {
      final PreservationJob presJob = new PreservationJob();
      presJob.setName(jobType.getLabel());
      presJob.setJobType(jobType);
      presJob.setJobRecurrence(jobFrequency);
      presJob.setEnable(false);
      // Specific parameters
      switch (jobType) {
        case FIXITY -> {
          presJob.setMaxItems(100);
          presJob.setParameters("sort=lastUpdate.when,asc");
        }
        case PURGE_ORDER -> presJob.setParameters("sort=creation.when,asc");
        default -> {
          // no specific parameter
        }
      }
      this.itemService.save(presJob);
      log.info("Preservation Job '{}' created", jobType.getLabel());
      return true;
    }
    return false;
  }
}

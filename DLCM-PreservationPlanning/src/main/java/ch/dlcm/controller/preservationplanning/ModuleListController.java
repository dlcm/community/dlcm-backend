/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - ModuleListController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preservationplanning;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.OAIProperties;
import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.security.EveryonePermissions;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.model.ModuleList;
import ch.dlcm.rest.UrlPath;

@EveryonePermissions
@RestController
@ConditionalOnBean(PreservationPlanningController.class)
@RequestMapping(UrlPath.PRES_PLANNING_MODULE)
public class ModuleListController implements ControllerWithHateoasHome {

  private final ModuleList moduleList;

  public ModuleListController(DLCMProperties dlcmProperties, AuthorizationClientProperties authClientProperties, OAIProperties oaiProperties) {
    this.moduleList = new ModuleList(dlcmProperties, authClientProperties, oaiProperties);
  }

  @GetMapping
  public HttpEntity<ModuleList> moduleList() {
    return new ResponseEntity<>(this.moduleList, HttpStatus.OK);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - MonitorController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preservationplanning;

import org.json.JSONException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.security.AdminPermissions;

import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.MonitorService;

@AdminPermissions
@RestController
@ConditionalOnBean(PreservationPlanningController.class)
@RequestMapping(UrlPath.PRES_PLANNING_MONITOR)
public class MonitorController implements ControllerWithHateoasHome {

  private MonitorService monitorService;

  public MonitorController(MonitorService monitorService) {
    this.monitorService = monitorService;
  }

  @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
  public HttpEntity<String> status() {
    try {
      return new ResponseEntity<>(this.monitorService.getModuleStatuses().toString(), HttpStatus.OK);
    } catch (JSONException | SolidifyRestException | SolidifyResourceNotFoundException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

}

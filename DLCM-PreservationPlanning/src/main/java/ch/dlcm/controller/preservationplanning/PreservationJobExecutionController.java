/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - PreservationJobExecutionController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preservationplanning;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.lang.reflect.Method;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;

import ch.dlcm.business.JobExecutionReportService;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.message.JobMessage;
import ch.dlcm.model.preservation.JobExecution;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.model.preservation.PreservationJob.JobStatus;
import ch.dlcm.repository.JobExecutionLineReportRepository;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.HistoryService;

@AdminPermissions
@RestController
@ConditionalOnBean(PreservationPlanningController.class)
@RequestMapping(UrlPath.PRES_PLANNING_PRES_JOB + SolidifyConstants.URL_PARENT_ID + ResourceName.PRES_JOB_EXECUTION)
public class PreservationJobExecutionController extends ExecutionReportController<PreservationJob, JobExecution> {

  public PreservationJobExecutionController(HistoryService historyService, JobExecutionLineReportRepository reportLineRepository,
          JobExecutionReportService reportService) {
    super(historyService, reportLineRepository, reportService);
  }

  @Override
  public HttpEntity<JobExecution> create(@PathVariable final String parentid, final @Valid @RequestBody JobExecution childItem) {
    return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
  }

  @Override
  public HttpEntity<JobExecution> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  public HttpEntity<RestCollection<JobExecution>> list(@PathVariable String parentid, @ModelAttribute JobExecution filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  public HttpEntity<JobExecution> update(@PathVariable final String parentid, @PathVariable final String id,
          @RequestBody final Map<String, Object> newChildItem) {
    return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable final String parentid, @PathVariable final String id) {
    return super.delete(parentid, id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable final String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_STATUS)
  public HttpEntity<JobStatus[]> listStatus(@PathVariable String parentid) {
    return new ResponseEntity<>(JobStatus.values(), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.RESUME)
  public HttpEntity<Result> resume(@PathVariable String parentid, @PathVariable String id) {
    final JobExecution jobExecution = this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, true);
    if (jobExecution == null) {
      throw new SolidifyResourceNotFoundException(parentid + "-" + id);
    }
    final Result res = new Result(parentid);
    if (jobExecution.getStatus() == JobStatus.COMPLETED) {
      res.setMesssage(this.messageService.get("message.job.execution.completed", new Object[] { id, parentid }));
    } else if (jobExecution.getStatus() != JobStatus.IN_ERROR) {
      res.setMesssage(this.messageService.get("message.job.execution.notinerror", new Object[] { id, parentid }));
    } else {
      res.setStatus(ActionStatus.NOT_EXECUTED);
      jobExecution.setStatus(JobStatus.READY);
      this.subResourceService.save(jobExecution);
      res.setStatus(ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.job.execution.readytoresume", new Object[] { id, parentid }));
    }
    if (res.getStatus() == ActionStatus.EXECUTED) {
      SolidifyEventPublisher.getPublisher().publishEvent(new JobMessage(parentid, id));
    }
    try {
      final Method resumeMtd = PreservationJobExecutionController.class.getMethod(ActionName.RESUME, String.class, String.class);
      res.add(linkTo(resumeMtd, parentid, id).withSelfRel());
      PreservationJobExecutionController.class.getMethod(ActionName.READ, String.class, String.class);
      res.add(linkTo(methodOn(this.getClass()).get(parentid, id)).withRel(ActionName.PARENT));
    } catch (final NoSuchMethodException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(String parentid, W w) {
    w.add(linkTo(methodOn(this.getClass()).listStatus(parentid)).withRel(ActionName.VALUES));
  }

  @Override
  protected PreservationJob getParentResourceProperty(JobExecution subResource) {
    return subResource.getPreservationJob();
  }

  @Override
  protected void setParentResourceProperty(JobExecution subResource, PreservationJob parentResource) {
    subResource.setPreservationJob(parentResource);
  }
}

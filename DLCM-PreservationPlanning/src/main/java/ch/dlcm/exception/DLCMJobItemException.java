/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - DLCMJobItemException.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.exception;

import ch.unige.solidify.exception.SolidifyException;

public abstract class DLCMJobItemException extends SolidifyException {

  private static final long serialVersionUID = -6229130509461818650L;

  private final String packageId;

  private final String packageUrl;

  protected DLCMJobItemException(String packageId, String packageUrl, String message) {
    super(message);
    this.packageId = packageId;
    this.packageUrl = packageUrl;
  }

  public String getPackageId() {
    return this.packageId;
  }

  public String getPackageUrl() {
    return this.packageUrl;
  }

}

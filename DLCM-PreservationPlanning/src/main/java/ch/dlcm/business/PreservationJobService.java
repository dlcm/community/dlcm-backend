/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Preservation Planning - PreservationJobService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.ResourceService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreservationPlanningController;
import ch.dlcm.model.preservation.JobRecurrence;
import ch.dlcm.model.preservation.JobScheduling;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.repository.PreservationJobRepository;
import ch.dlcm.specification.PreservationJobSpecification;

@Service
@ConditionalOnBean(PreservationPlanningController.class)
public class PreservationJobService extends ResourceService<PreservationJob> {

  private static final String SCHEDULING_FIELD = "scheduling";
  private static final String INVALID_SCHEDULING_FOR_RECURRENCE = "validation.preservationjob.invalidSchedulingForRecurrence";
  private static final String INVALID_SCHEDULING_VALUE = "validation.preservationjob.invalidSchedulingValue";
  private static final String RECURRENCE_NOT_APPLICABLE = "validation.preservationjob.recurrenceNotApplicable";

  private final PreservationJobRepository preservationJobRepository;
  private final int preservationPlanningMonth;
  private final int preservationPlanningMonthDay;
  private final int preservationPlanningHour;
  private final int preservationPlanningWeekDay;

  public PreservationJobService(PreservationJobRepository preservationJobRepository, DLCMProperties dlcmProperties) {
    this.preservationJobRepository = preservationJobRepository;

    this.preservationPlanningMonth = dlcmProperties.getPreservationPlanning().getMonth();
    this.preservationPlanningMonthDay = dlcmProperties.getPreservationPlanning().getMonthDay();
    this.preservationPlanningHour = dlcmProperties.getPreservationPlanning().getHour();
    this.preservationPlanningWeekDay = dlcmProperties.getPreservationPlanning().getWeekDay();
  }

  public PreservationJob findByName(String resName) {
    return this.preservationJobRepository.findByName(resName);
  }

  @Override
  public void beforeValidate(PreservationJob job) {
    this.cleanScheduling(job);
  }

  @Override
  public void validateItemSpecificRules(PreservationJob job, BindingResult errors) {
    /**
     * Check recurrence level according to jobType, since recurrent job can only be created for these
     * types of jobs: FIXITY, REPLICATION, REPLICATION_COLLECTION, CLEAN_SUBMISSION
     */
    if (job.getJobRecurrence() != JobRecurrence.ONCE && !job.getJobType().isRecurring()) {
      errors.addError(new FieldError(job.getClass().getSimpleName(), "jobRecurrence",
              this.messageService.get(RECURRENCE_NOT_APPLICABLE,
                      new Object[] { job.getJobRecurrence(), job.getJobType() })));
    }

    /**
     * Check scheduling values according to recurrence level
     */
    // @formatter:off
    if (    (job.getJobRecurrence() == JobRecurrence.DAILY
                    && (job.getScheduling().getHour() == null || job.getScheduling().getWeekDay() != null
                    || job.getScheduling().getMonthDay() != null || job.getScheduling().getMonth() != null))
            ||
            (job.getJobRecurrence() == JobRecurrence.WEEKLY
                    && (job.getScheduling().getHour() == null || job.getScheduling().getWeekDay() == null
                    || job.getScheduling().getMonthDay() != null || job.getScheduling().getMonth() != null))
            ||
            (job.getJobRecurrence() == JobRecurrence.MONTHLY
                    && (job.getScheduling().getHour() == null || job.getScheduling().getWeekDay() != null
                    || job.getScheduling().getMonthDay() == null || job.getScheduling().getMonth() != null))
            ||
            (job.getJobRecurrence() == JobRecurrence.YEARLY
                    && (job.getScheduling().getHour() == null || job.getScheduling().getWeekDay() != null
                    || job.getScheduling().getMonthDay() == null || job.getScheduling().getMonth() == null))
    ) {
      // @formatter:on
      errors.addError(new FieldError(job.getClass().getSimpleName(), SCHEDULING_FIELD,
              this.messageService.get(INVALID_SCHEDULING_FOR_RECURRENCE, new Object[] { job.getJobRecurrence().getLabel() })));
    }

    /**
     * Check scheduling values
     */
    if (job.getScheduling() != null) {
      final JobScheduling scheduling = job.getScheduling();

      /**
       * hour value
       */
      if (scheduling.getHour() != null && (scheduling.getHour() < 0 || scheduling.getHour() > 23)) {
        errors.addError(new FieldError(job.getClass().getSimpleName(), SCHEDULING_FIELD,
                this.messageService.get(INVALID_SCHEDULING_VALUE, new Object[] { "hour: " + scheduling.getHour() })));
      }

      /**
       * day of week value
       */
      if (scheduling.getWeekDay() != null && (scheduling.getWeekDay() < 1 || scheduling.getWeekDay() > 7)) {
        errors.addError(new FieldError(job.getClass().getSimpleName(), SCHEDULING_FIELD,
                this.messageService.get(INVALID_SCHEDULING_VALUE, new Object[] { "day of week: " + scheduling.getWeekDay() })));
      }

      /**
       * day of month
       */
      if (scheduling.getMonthDay() != null && (scheduling.getMonthDay() < 1 || scheduling.getMonthDay() > 31)) {
        errors.addError(new FieldError(job.getClass().getSimpleName(), SCHEDULING_FIELD,
                this.messageService.get(INVALID_SCHEDULING_VALUE, new Object[] { "day of month: " + scheduling.getMonthDay() })));
      }

      /**
       * month
       */
      if (scheduling.getMonth() != null && (scheduling.getMonth() < 1 || scheduling.getMonth() > 12)) {
        errors.addError(new FieldError(job.getClass().getSimpleName(), SCHEDULING_FIELD,
                this.messageService.get(INVALID_SCHEDULING_VALUE, new Object[] { "month: " + scheduling.getMonth() })));
      }

      /**
       * day of month according to month
       */
      if (scheduling.getMonthDay() != null && scheduling.getMonth() != null) {
        final int day = scheduling.getMonthDay();
        final int month = scheduling.getMonth();

        if ((month == 2 && day > 28)
                ||
                ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31)) {
          errors.addError(new FieldError(job.getClass().getSimpleName(), SCHEDULING_FIELD,
                  this.messageService.get(INVALID_SCHEDULING_VALUE, new Object[] { "day: " + day + " not valid for month: " + month })));
        }
      }
    }
  }

  private void cleanScheduling(PreservationJob job) {

    if (job.getJobRecurrence() == null || job.getJobRecurrence() == JobRecurrence.ONCE) {

      /**
       * Job is not recurrent --> remove scheduling as scheduling values wouldn't be used
       */
      job.setScheduling(null);

    } else {
      if (job.getScheduling() == null) {

        /**
         * Job scheduling is required but is missing. Set default values according to recurrence level.
         */
        final JobScheduling scheduling = new JobScheduling();
        switch (job.getJobRecurrence()) {
          case YEARLY -> {
            scheduling.setMonth(this.preservationPlanningMonth);
            scheduling.setMonthDay(this.preservationPlanningMonthDay);
            scheduling.setHour(this.preservationPlanningHour);
          }
          case MONTHLY -> {
            scheduling.setMonthDay(this.preservationPlanningMonthDay);
            scheduling.setHour(this.preservationPlanningHour);
          }
          case WEEKLY -> {
            scheduling.setWeekDay(this.preservationPlanningWeekDay);
            scheduling.setHour(this.preservationPlanningHour);
          }
          case DAILY -> scheduling.setHour(this.preservationPlanningHour);
          default -> throw new SolidifyRuntimeException("Unknown job recurrence: " + job.getJobRecurrence());
        }
        job.setScheduling(scheduling);
      } else {
        /**
         * Job scheduling given -> clean values according to recurrence level
         */
        final JobScheduling scheduling = job.getScheduling();
        switch (job.getJobRecurrence()) {
          case DAILY -> {
            scheduling.setWeekDay(null);
            scheduling.setMonthDay(null);
            scheduling.setMonth(null);
          }
          case WEEKLY -> {
            scheduling.setMonthDay(null);
            scheduling.setMonth(null);
          }
          case MONTHLY -> {
            scheduling.setWeekDay(null);
            scheduling.setMonth(null);
          }
          case YEARLY -> scheduling.setWeekDay(null);
          default -> throw new SolidifyRuntimeException("Unknown job scheduling: " + job.getScheduling());
        }
      }
    }
  }

  @Override
  public PreservationJobSpecification getSpecification(PreservationJob resource) {
    return new PreservationJobSpecification(resource);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ResourceWithHistoryController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.model.StatusHistory;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.HistoryService;

@UserPermissions
public abstract class ResourceWithHistoryController<T extends Resource> extends ResourceController<T> {

  protected HistoryService historyService;

  protected ResourceWithHistoryController(HistoryService historyService) {
    this.historyService = historyService;
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.HISTORY)
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable String id, Pageable pageable) {
    if (!this.itemService.existsById(id)) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    final Page<StatusHistory> listItem = this.historyService.findByResId(id, pageable);
    final RestCollection<StatusHistory> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(methodOn(this.getClass()).history(id, pageable)).withSelfRel());
    collection
            .add(Tool.parentLink((linkTo(methodOn(this.getClass()).history(id, pageable))).toUriComponentsBuilder()).withRel(ActionName.PARENT));
    this.addPageLinks(linkTo(methodOn(this.getClass()).history(id, pageable)), collection, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

}

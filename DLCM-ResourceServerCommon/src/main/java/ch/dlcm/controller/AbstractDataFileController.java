/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - AbstractDataFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import jakarta.validation.Valid;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.Result;

import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.DataFileOwnerInterface;
import ch.dlcm.model.oais.MetadataVersionAware;
import ch.dlcm.service.HistoryService;

public abstract class AbstractDataFileController<T extends Resource & RemoteResourceContainer & MetadataVersionAware & DataFileOwnerInterface<V>, V extends AbstractDataFile<T, V>>
        extends DLCMCompositionController<T, V> {

  public AbstractDataFileController(HistoryService historyService) {
    super(historyService);
  }

  @Override
  @PostMapping
  public HttpEntity<V> create(@PathVariable final String parentid, final @Valid @RequestBody V childResource) {
    final T parentResource = this.resourceService.findOne(parentid);
    // Check
    if (!parentResource.checkDataFileToAdd(childResource.getDataCategory(), childResource.getDataType())) {
      throw new IllegalStateException(this.messageService.get("validation.dataFile.dataCategory.invalid",
              new Object[] { childResource.getDataCategory(), childResource.getDataType() }));
    }
    return super.create(parentid, childResource);
  }

  protected <D extends AbstractDataFile> Result resumeDataFile(String parentid, String id) {
    final V item = this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, true);
    final D itemDataFile = (D) item;
    if (item == null) {
      throw new SolidifyResourceNotFoundException(parentid + "-" + id);
    }
    final Result res = new Result(item.getResId());
    if (itemDataFile.getStatus() == DataFileStatus.READY
            || itemDataFile.getStatus() == DataFileStatus.CLEANED) {
      res.setMesssage(this.messageService.get("message.datafile.completed", new Object[] { id, parentid }));
    } else if (itemDataFile.getStatus() == DataFileStatus.TO_PROCESS
            || itemDataFile.getStatus() == DataFileStatus.CLEANING) {
      res.setMesssage(this.messageService.get("message.datafile.inprogress", new Object[] { id, parentid }));
    } else {
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      itemDataFile.setFileStatus(DataFileStatus.RECEIVED);
      this.subResourceService.save(item);
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.datafile.resume", new Object[] { id, parentid }));
    }
    return res;
  }

  protected <D extends AbstractDataFile> Result putInErrorDataFile(String parentid, String id) {
    final V item = this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, true);
    final D itemDataFile = (D) item;
    if (item == null) {
      throw new SolidifyResourceNotFoundException(parentid + "-" + id);
    }
    final Result res = new Result(item.getResId());
    if (itemDataFile.getStatus() == DataFileStatus.READY
            || itemDataFile.getStatus() == DataFileStatus.CLEANED) {
      res.setMesssage(this.messageService.get("message.datafile.completed", new Object[] { id, parentid }));
    } else {
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      itemDataFile.setFileStatus(DataFileStatus.IN_ERROR);
      this.subResourceService.save(item);
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.datafile.in_error", new Object[] { id, parentid }));
    }
    return res;
  }

}

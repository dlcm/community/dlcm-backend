/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - AbstractPackageController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.DataFileService;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.DataFileOwnerInterface;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.oais.MetadataVersionAware;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.DataCategoryService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.propagate.PropagateMetadataTypeRemoteResourceService;

/**
 * Abstract controller for packages (Deposit, SIP, AIP, ...) with data file(s)
 *
 * @param <S> The type of the data file (DepositDataFile, SIPDataFile, AipDataFile, ...)
 * @param <T> The type of the package (Deposit, SubmissionInfoPackage, ArchivalInfoPackage, ...)
 */
@UserPermissions
public abstract class AbstractPackageController<S extends AbstractDataFile<T, ?>, T extends Resource & MetadataVersionAware & DataFileOwnerInterface<S>>
        extends ResourceWithHistoryController<T> {

  public enum UploadMode {
    STANDARD, UPLOAD_METADATA
  }

  protected final MetadataService metadataService;
  protected final PropagateMetadataTypeRemoteResourceService metadataTypeService;
  protected final DataFileService<S> dataFileService;
  protected final DataCategoryService dataCategoryService;

  protected AbstractPackageController(HistoryService historyService, MetadataService metadataService,
          PropagateMetadataTypeRemoteResourceService metadataTypeService, DataFileService<S> dataFileService,
          DataCategoryService dataCategoryService) {
    super(historyService);
    this.metadataService = metadataService;
    this.metadataTypeService = metadataTypeService;
    this.dataFileService = dataFileService;
    this.dataCategoryService = dataCategoryService;
  }

  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_VERSION)
  public HttpEntity<List<String>> listVersions() {
    return new ResponseEntity<>(
            Stream.of(DLCMMetadataVersion.values())
                    .map(DLCMMetadataVersion::getVersion)
                    .toList(),
            HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_DATA_CATEGORY)
  public HttpEntity<List<DataCategory>> listDataCategory() {
    return new ResponseEntity<>(this.dataCategoryService.listPackageCategories(), HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_DATA_TYPE)
  public HttpEntity<List<DataCategory>> listDataType(@RequestParam(required = true) String category) {
    return new ResponseEntity<>(this.dataCategoryService.listPackageTypes(category), HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping("/" + ResourceName.PROFILE)
  public HttpEntity<StreamingResponseBody> profile(@RequestParam(value = "version", required = false) String version) {
    final DLCMMetadataVersion metadataVersion = this.getVersion(version);
    final ClassPathResource profile = new ClassPathResource(
            SolidifyConstants.SCHEMA_HOME + "/" + metadataVersion.getMetsProfile());
    return this.getXsdAsStreamingResponse(profile);
  }

  protected HttpEntity<StreamingResponseBody> getRepresentationInfoSchema(String version) {
    final DLCMMetadataVersion metadataVersion = this.getVersion(version);
    final ClassPathResource xsd = this.metadataService.getRepresentationInfoSchema(metadataVersion);
    return this.getXsdAsStreamingResponse(xsd);
  }

  protected HttpEntity<StreamingResponseBody> getSchemaStream(String version, AbstractDataFile<?, ?> df) {
    final DLCMMetadataVersion metadataVersion = this.getVersion(version);
    final ClassPathResource xsd = this.metadataService.getResourceSchema(metadataVersion, df);
    return this.getXsdAsStreamingResponse(xsd);
  }

  protected HttpEntity<S> upload(String id, FileUploadDto<S> fileUploadDto, String tmpLocation, UploadMode uploadMode) throws IOException {
    // Check parent resource
    final T item = this.itemService.findOne(id);
    if (!(item.isModifiable() || uploadMode.equals(UploadMode.UPLOAD_METADATA))) {
      throw new IllegalStateException(this.messageService.get("validation.resource.unmodifiable",
              new Object[] { item.getClass().getSimpleName() + ": " + item.getResId() }));
    }

    // Check
    if (!item.checkDataFileToAdd(fileUploadDto.getDataCategory(), fileUploadDto.getDataType())) {
      throw new IllegalStateException(this.messageService.get("validation.dataFile.dataCategory.invalid",
              new Object[] { fileUploadDto.getDataCategory(), fileUploadDto.getDataType() }));
    }

    // Check temporary folder
    final Path tmpDir = Paths.get(tmpLocation + "/" + id);
    Path tmpDirWithRelativeLocation = tmpDir;
    if (!FileTool.ensureFolderExists(tmpDir)) {
      throw new IOException(this.messageService.get("message.upload.folder.notcreated", new Object[] { tmpDir }));
    }

    // Check relative location
    String relativeLocation = fileUploadDto.getRelLocation();

    if (!StringTool.isNullOrEmpty(relativeLocation)) {
      // Check format
      relativeLocation = AbstractDataFile.checkRelativeLocation(relativeLocation);
      // Check temporary folder
      tmpDirWithRelativeLocation = Path.of(tmpDir + relativeLocation);
      if (!tmpDirWithRelativeLocation.normalize().startsWith(tmpDir)) {
        throw new IllegalArgumentException("Path traversal attempt");
      }
      if (!FileTool.ensureFolderExists(tmpDirWithRelativeLocation)) {
        throw new IOException(this.messageService.get("message.upload.folder.notcreated", new Object[] { tmpDirWithRelativeLocation }));
      }
    } else {
      relativeLocation = "/";
    }

    final String fileName = switch (uploadMode) {
      case STANDARD -> fileUploadDto.getOriginalFileName();
      case UPLOAD_METADATA -> DLCMConstants.UPDATED_METADATA_FILE_PREFIX;
      default -> throw new IllegalStateException(this.messageService.get("message.upload.mode.unknown", new Object[] { uploadMode }));
    };

    S dataFile = this.getDataFile(item, fileUploadDto);
    // Temporary path to copy
    final Path tempTarget = tmpDirWithRelativeLocation.resolve(fileName);

    // check if duplicate data files
    if (this.dataFileService
            .findByPackageIdAndRelativeLocationAndSourceData(item.getResId(), tempTarget.toUri())
            .isPresent()) {
      String relativeFullName = relativeLocation + "/" + fileName;
      if (relativeLocation.equals("/")) {
        relativeFullName = "/" + fileName;
      }
      throw new IOException(this.messageService.get("message.upload.file.duplicate", new Object[] { relativeFullName }));
    }
    // Copy file
    Files.copy(fileUploadDto.getInputStream(), tempTarget);
    // Set in data file
    dataFile.setSourceData(tempTarget.toUri());
    dataFile.setFileSize(Long.valueOf(FileTool.getSize(tempTarget)));
    // Save it
    this.dataFileService.save(dataFile);

    return new ResponseEntity<>(dataFile, HttpStatus.OK);
  }

  protected void setAuthenticatedUserProperties(AbstractDataFile<?, ?> resource) {
    final String externalUid = this.getAuthenticatedUserExternalUid();
    if (externalUid != null) {
      resource.setUpdatedBy(externalUid);
      if (resource.getCreatedBy() == null) {
        resource.setCreatedBy(externalUid);
      }
    }
  }

  private S getDataFile(T dataFileOwner, FileUploadDto<S> fileUploadDto) {
    final S dataFile = fileUploadDto.getDataFile();

    // Set data category
    dataFile.setDataCategory(fileUploadDto.getDataCategory());
    // Set data type
    dataFile.setDataType(fileUploadDto.getDataType());
    // Set relative location
    dataFile.setRelativeLocation(fileUploadDto.getRelLocation());
    // Load custom metadata definition
    if (fileUploadDto.getMetadataType() != null) {
      dataFile.setMetadataType(this.metadataTypeService.findOneWithCache(fileUploadDto.getMetadataType()));
    }
    // Set package
    dataFile.setInfoPackage(dataFileOwner);
    // Set checksum
    this.generateDataFileChecksums(fileUploadDto, dataFile);
    // Set user info
    this.setAuthenticatedUserProperties(dataFile);
    // Initialize empty field
    dataFile.init();
    return dataFile;
  }

  private DLCMMetadataVersion getVersion(String version) {
    if (!StringTool.isNullOrEmpty(version)) {
      return DLCMMetadataVersion.fromVersion(version);
    }
    return DLCMMetadataVersion.getDefaultVersion();
  }

  private void generateDataFileChecksums(FileUploadDto<S> fileUploadDto, S dataFile) {
    List<DataFileChecksum> listDataFileChecksum = dataFile.getChecksums();
    if (fileUploadDto.getChecksums() != null) {
      for (Map.Entry<ChecksumAlgorithm, String> checksumEntry : fileUploadDto.getChecksums().entrySet()) {
        var dataFileChecksum = new DataFileChecksum();
        dataFileChecksum.init();
        dataFileChecksum.setChecksum(checksumEntry.getValue());
        dataFileChecksum.setChecksumType(DataFileChecksum.ChecksumType.COMPLETE);
        dataFileChecksum.setChecksumAlgo(checksumEntry.getKey());

        DataFileChecksum.ChecksumOrigin checksumOrigin = fileUploadDto.getChecksumsOrigin().get(checksumEntry.getKey());
        if (!DataFileChecksum.ChecksumOrigin.PORTAL.equals(checksumOrigin)) {
          checksumOrigin = DataFileChecksum.ChecksumOrigin.USER;
        }
        dataFileChecksum.setChecksumOrigin(checksumOrigin);

        listDataFileChecksum.add(dataFileChecksum);
      }
    }
  }

  private ResponseEntity<StreamingResponseBody> getXsdAsStreamingResponse(ClassPathResource xsd) {
    try {
      final InputStream is = xsd.getInputStream();
      return this.buildDownloadResponseEntity(is, xsd.getFilename(), SolidifyConstants.XML_MIME_TYPE, xsd.contentLength());
    } catch (final IOException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

}

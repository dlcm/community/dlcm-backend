/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DLCMModuleController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.Module;

import ch.dlcm.service.ToolList;
import ch.dlcm.service.ToolService;

public abstract class DLCMModuleController extends ModuleController {

  ToolList toolList;

  public DLCMModuleController(String name, ToolList toolList) {
    super(name);
    this.toolList = toolList;
  }

  @GetMapping(value = "/checkTool", produces = { MediaType.APPLICATION_JSON_VALUE })
  public HttpEntity<String> checkTool(@RequestParam String toolName) {
    for (final ToolService tool : this.toolList.getToolList()) {
      if (tool.getToolName().equals(toolName)) {
        final JSONObject toolDetails = new JSONObject();
        toolDetails.put("name", tool.getToolName());
        toolDetails.put("version", tool.getToolVersion());
        toolDetails.put("description", tool.getToolDescription());
        toolDetails.put("status", tool.check());
        return new ResponseEntity<>(toolDetails.toString(), HttpStatus.OK);
      }
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @Override
  public HttpEntity<Module> home() {
    final Module module = super.home().getBody();
    if (module == null) {
      throw new SolidifyRuntimeException("Response contains no body");
    }
    for (final ToolService tool : this.toolList.getToolList()) {
      module.add(linkTo(methodOn(this.getClass()).checkTool(tool.getToolName())).withRel("tools"));
    }
    return new ResponseEntity<>(module, HttpStatus.OK);
  }

}

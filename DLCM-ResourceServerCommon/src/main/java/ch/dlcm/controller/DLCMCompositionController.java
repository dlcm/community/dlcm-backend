/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DLCMCompositionController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.net.URI;
import java.util.NoSuchElementException;

import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.CompositionController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.model.StatusHistory;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.HistoryService;

@UserPermissions
public abstract class DLCMCompositionController<T extends Resource, V extends Resource> extends CompositionController<T, V> {

  protected HistoryService historyService;

  public DLCMCompositionController(HistoryService historyService) {
    this.historyService = historyService;
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.HISTORY)
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable String parentid, @PathVariable String id, Pageable pageable) {
    if (this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, false) == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // TODO check if it's the "id" or "parentid" instead in parameter. It's should never found
    // because it is a history for deposit status
    final Page<StatusHistory> listItem = this.historyService.findByResId(id, pageable);
    final RestCollection<StatusHistory> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(this.getClass(), parentid, id, pageable).withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass(), parentid, id, pageable)).toUriComponentsBuilder()).withRel(ActionName.PARENT));
    this.addPageLinks(linkTo(this.getClass(), parentid, id, pageable), collection, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD)
  @ResponseBody
  public HttpEntity<FileSystemResource> download(@PathVariable String parentid, @PathVariable String id) {
    if (!this.resourceService.existsById(parentid)) {
      throw new NoSuchElementException("No parent with id " + parentid);
    }
    final V item = this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, false);
    if (item == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    final URI fileUri = item.getDownloadUri();
    final String contentTypeFile = (item.getContentType() == null) ? FileTool.getContentType(fileUri).toString() : item.getContentType();
    return this.buildDownloadResponseEntity(fileUri.getPath(), fileUri.getPath().substring(fileUri.getPath().lastIndexOf('/') + 1),
            contentTypeFile, FileTool.getSize(fileUri));
  }
}

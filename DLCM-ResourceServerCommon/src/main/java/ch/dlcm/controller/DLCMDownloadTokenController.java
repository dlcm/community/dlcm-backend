/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DLCMDownloadTokenController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.controller.SolidifyDownloadTokenController;
import ch.unige.solidify.repository.DownloadTokenRepository;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.model.security.DownloadTokenType;

@UserPermissions
public abstract class DLCMDownloadTokenController extends SolidifyDownloadTokenController<DownloadTokenType> {

  protected DLCMDownloadTokenController(SolidifyProperties config,
          DownloadTokenRepository downloadTokenRepository) {
    super(config, downloadTokenRepository);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DLCMIgnoreException.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.exception;

import java.io.Serial;

import ch.unige.solidify.exception.SolidifyRuntimeException;

/**
 * Exception when a data file belongs to ignore list (need validation to be preserved)
 */
public class DLCMIgnoreException extends SolidifyRuntimeException {
  @Serial
  private static final long serialVersionUID = -5784984260724094679L;

  public DLCMIgnoreException(String message) {
    super(message);
  }

  public DLCMIgnoreException(String message, Throwable exception) {
    super(message, exception);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - VirusCheckService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.viruscheck;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.Tool;
import ch.dlcm.model.VirusCheck;
import ch.dlcm.service.ToolService;

public abstract class VirusCheckService extends ToolService {

  protected VirusCheckService(MessageService messageService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
  }

  // **************************
  // ** Methods to implement **
  // **************************

  public void checkVirus(String resId, AbstractDataFile file, InputStream inputStream) {
    // Init data file
    final VirusCheck virusCheck = this.initVirusCheck(file);
    // Run tool & Set details
    virusCheck.setDetails(this.runTool(file, inputStream));

    // Log Virus check identification
    this.logHistory(file.getClass().getSimpleName(), resId, virusCheck.getTool());
  }

  // *********************
  // ** Default Methods **
  // *********************

  public abstract void logHistory(String type, String resId, Tool tool);

  @Override
  public String runTool(AbstractDataFile file) throws IOException {
    try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file.getPath().toFile()),
            SolidifyConstants.BUFFER_SIZE)) {
      return this.runTool(file, inputStream);
    }
  }

  private VirusCheck initVirusCheck(AbstractDataFile file) {
    if (file.getVirusCheck() == null) {
      file.setVirusCheck(new VirusCheck());
    }
    final VirusCheck virusTool = file.getVirusCheck();
    this.setTool(virusTool);
    return virusTool;
  }

}

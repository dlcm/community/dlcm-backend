/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ClamAVClient.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.viruscheck.clamav;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.exception.SolidifyToolException;

public class ClamAVClient {
  public static final int READ_SIZE = 2048;

  public static final String TOOL_PREFIX = "{ClamAV} ";
  private static final Logger log = LoggerFactory.getLogger(ClamAVClient.class);

  private final String hostName;
  private final int port;
  private int timeout = 60000;

  public ClamAVClient(String hostName, int port) {
    super();
    this.hostName = hostName;
    this.port = port;
  }

  public ClamAVClient(String hostName, int port, int timeout) {
    super();
    this.timeout = timeout;
    this.hostName = hostName;
    this.port = port;
  }

  public String getHostName() {
    return this.hostName;
  }

  public int getPort() {
    return this.port;
  }

  public int getTimeout() {
    return this.timeout;
  }

  public String ping() {
    return this.executeCommand("zPING\0");
  }

  public ClamAVResult scan(InputStream in) {
    String response = null;
    try (Socket socket = new Socket()) {
      this.configureSocket(socket);
      try (DataOutputStream dos = new DataOutputStream(socket.getOutputStream())) {

        dos.write("zINSTREAM\0".getBytes());
        int read = READ_SIZE;
        final byte[] buffer = new byte[READ_SIZE];
        while (read == READ_SIZE) {
          read = in.read(buffer);
          if (read > 0) { // if previous read exhausted the stream
            try {
              dos.writeInt(read);
              dos.write(buffer, 0, read);
            } catch (final IOException e) {
              log.debug(TOOL_PREFIX + "Error in writing data to socket", e);
              break;
            }
          }
        }
        dos.writeInt(0);
        dos.flush();
        try {
          read = socket.getInputStream().read(buffer);
        } catch (final IOException e) {
          log.debug(TOOL_PREFIX + "Error in reading result from socket", e);
          read = 0;
        }

        if (read > 0) {
          response = new String(buffer, 0, read);
        }
      }
    } catch (final IOException e) {
      log.error(TOOL_PREFIX + "Error", e);
      return new ClamAVResult(e);
    }
    if (response != null) {
      log.debug("{} Response: {}", TOOL_PREFIX, response);
      return new ClamAVResult(response.trim());
    }

    return new ClamAVResult();
  }

  public String stats() {
    return this.executeCommand("zSTATS\n");
  }

  public String version() {
    return this.executeCommand("zVERSION\0");
  }

  public String versionNumber() {
    final String v = this.executeCommand("zVERSION\0");
    if (v != null) {
      return v.substring(v.indexOf(' ') + 1, v.indexOf('/'));
    }
    throw new SolidifyToolException("Cannot get version number");
  }

  private Socket configureSocket(Socket socket) throws IOException {
    socket.connect(new InetSocketAddress(this.getHostName(), this.getPort()));
    socket.setSoTimeout(this.getTimeout());
    return socket;
  }

  private String executeCommand(String cmd) {
    String response = null;
    try (Socket socket = new Socket()) {
      this.configureSocket(socket);
      try (DataOutputStream dos = new DataOutputStream(socket.getOutputStream())) {
        dos.write(cmd.getBytes());
        dos.flush();
        response = this.receiveResult(socket);
      }
    } catch (final IOException e) {
      throw new SolidifyToolException(TOOL_PREFIX + "Error", e);
    }
    if (response != null) {
      log.debug("{} Response: {}", TOOL_PREFIX, response);
    }
    return response;
  }

  private String receiveResult(Socket socket) throws IOException {
    final StringBuilder response = new StringBuilder();

    try (InputStream is = socket.getInputStream()) {
      int read = READ_SIZE;
      final byte[] buffer = new byte[READ_SIZE];

      while (read == READ_SIZE) {
        read = is.read(buffer);
        response.append(new String(buffer, 0, read));
      }
    }
    return response.toString();
  }

}

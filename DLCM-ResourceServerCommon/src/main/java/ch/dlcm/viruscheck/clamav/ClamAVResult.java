/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ClamAVResult.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.viruscheck.clamav;

import ch.dlcm.exception.DLCMVirusCheckException;

public class ClamAVResult {

  public enum Status {
    ERROR, FAILED, PASSED
  }

  public static final String ERROR_SUFFIX = "ERROR";
  public static final String FOUND_SUFFIX = "FOUND";
  public static final String RESPONSE_OK = "stream: OK";

  public static final String RESPONSE_SIZE_EXCEEDED = "INSTREAM size limit exceeded. ERROR";

  public static final String STREAM_PREFIX = "stream: ";
  private Exception exception;
  private String result = "";
  private String signature = "";

  private Status status = Status.ERROR;

  public ClamAVResult() {
  }

  public ClamAVResult(Exception ex) {
    this.setException(ex);
  }

  public ClamAVResult(String result) {
    this.setResult(result);
  }

  public Exception getException() {
    return this.exception;
  }

  public String getResult() {
    return this.result;
  }

  public String getSignature() {
    return this.signature;
  }

  public Status getStatus() {
    return this.status;
  }

  public void setException(Exception exception) {
    this.exception = exception;
  }

  public void setResult(String result) {
    this.result = result;

    if (result == null) {
      this.setStatus(Status.ERROR);
      this.setException(new DLCMVirusCheckException("No result"));
    } else if (RESPONSE_OK.equals(result)) {
      this.setStatus(Status.PASSED);
    } else if (result.endsWith(FOUND_SUFFIX)) {
      this.setStatus(Status.FAILED);
      this.setSignature(result.substring(STREAM_PREFIX.length(), result.lastIndexOf(FOUND_SUFFIX) - 1));
    } else if (result.endsWith(ERROR_SUFFIX)) {
      this.setStatus(Status.ERROR);
      this.setException(new DLCMVirusCheckException(result));
    } else {
      this.setStatus(Status.ERROR);
      this.setException(new DLCMVirusCheckException("Unknow error"));
    }
  }

  public void setSignature(String signature) {
    this.signature = signature;
  }

  public void setStatus(Status status) {
    this.status = status;
  }
}

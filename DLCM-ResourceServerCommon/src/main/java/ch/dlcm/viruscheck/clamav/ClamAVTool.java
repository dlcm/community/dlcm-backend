/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ClamAVTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.viruscheck.clamav;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.json.JSONObject;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyToolException;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.Tool;
import ch.dlcm.viruscheck.VirusCheckService;

@Profile("vir-clamav")
@Service
public class ClamAVTool extends VirusCheckService {

  private final String virusCheckTool;
  private final ClamAVClient clamClt;

  public ClamAVTool(DLCMProperties dlcmProperties, MessageService messageService) {
    super(messageService, dlcmProperties);
    this.virusCheckTool = dlcmProperties.getVirusCheck().getTool();
    final URI uri = URI.create(dlcmProperties.getVirusCheck().getTool());
    this.clamClt = new ClamAVClient(uri.getHost(), uri.getPort(), dlcmProperties.getVirusCheck().getTimeout());
  }

  @Override
  public JSONObject check() {
    final JSONObject check = new JSONObject();
    check.put("url", this.virusCheckTool);
    // Ping
    check.put("ping", this.clamClt.ping());
    // Stats
    check.put("stats", this.clamClt.stats());
    // No Virus
    final String inputString = "0123456789@$#¬abcdefghijklmnopqrstuvwxyz".repeat(1000);
    final InputStream is1 = new ByteArrayInputStream(inputString.getBytes());
    ClamAVResult res = this.clamClt.scan(is1);
    if (res.getStatus() == ClamAVResult.Status.PASSED) {
      check.put("noVirus", res.getResult());
    } else {
      check.put("noVirus", "KO");
    }
    // Virus Eicar-Test
    final InputStream is2 = new ByteArrayInputStream("X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*".getBytes());

    res = this.clamClt.scan(is2);
    if (res.getStatus() == ClamAVResult.Status.FAILED) {
      check.put("virus", res.getSignature());
    } else {
      check.put("virus", "KO");
    }
    return check;
  }

  @Override
  public String getToolDescription() {
    return "ClamAV® is an open source antivirus engine";
  }

  @Override
  public String getToolName() {
    return "ClamAV";
  }

  @Override
  public String getToolPuid() {
    return null;
  }

  @Override
  public String getToolVersion() {
    try {
      return this.clamClt.versionNumber();
    } catch (final SolidifyToolException e) {
      return "unknown";
    }
  }

  @Override
  public void logHistory(String type, String resId, Tool tool) {
    final StatusHistory stsHistory = new StatusHistory(type, resId, DataFileStatus.VIRUS_CHECKED.toString());
    stsHistory.setDescription(this.messageService
            .get("datafile.status." + stsHistory.getStatus().toLowerCase(), new Object[] { tool.getName() + " " + tool.getVersion() }));
    SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);
  }

  @Override
  public String runTool(AbstractDataFile file, InputStream inputStream) {
    file.getVirusCheck().setCheckDate(OffsetDateTime.now(ZoneOffset.UTC));
    return this.getResultDetails(this.clamClt.scan(inputStream));
  }

  private String getResultDetails(ClamAVResult clamResult) {
    String resultWithSignatureMessage = "result = " + clamResult.getResult() + ", signature = " + clamResult.getSignature();
    switch (clamResult.getStatus()) {
      case ERROR -> throw new SolidifyToolException("Error during virus scan : " + resultWithSignatureMessage, clamResult.getException());
      case FAILED -> throw new SolidifyCheckingException("Virus found: " + resultWithSignatureMessage);
      case PASSED -> {
        return clamResult.getResult();
      }
      default -> {
      }
    }
    return null;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - NoVirusCheckTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.viruscheck.notool;

import java.io.InputStream;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.json.JSONObject;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.Tool;
import ch.dlcm.viruscheck.VirusCheckService;

@Profile("vir-notool")
@Service
public class NoVirusCheckTool extends VirusCheckService {

  private final String applicationName;
  private final String applicationVersion;

  public NoVirusCheckTool(DLCMProperties dlcmProperties, MessageService messageService) {
    super(messageService, dlcmProperties);
    this.applicationName = dlcmProperties.getAppConfig().getApplicationName();
    this.applicationVersion = dlcmProperties.getAppConfig().getApplicationVersion();
  }

  @Override
  public JSONObject check() {
    final JSONObject check = new JSONObject();
    check.put("info", "No virus check");
    return check;
  }

  @Override
  public String getToolDescription() {
    return "DLCM tool of " + this.applicationName;
  }

  @Override
  public String getToolName() {
    return "DLCM Virus Check";
  }

  @Override
  public String getToolPuid() {
    return null;
  }

  @Override
  public String getToolVersion() {
    return this.applicationVersion;
  }

  @Override
  public void logHistory(String type, String resId, Tool tool) {
    // Log nothing
  }

  @Override
  public String runTool(AbstractDataFile file, InputStream inputStream) {
    file.getVirusCheck().setCheckDate(OffsetDateTime.now(ZoneOffset.UTC));
    return "No virus check";
  }

}

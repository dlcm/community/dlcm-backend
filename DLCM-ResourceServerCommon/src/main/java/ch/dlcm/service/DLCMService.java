/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DLCMService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import static ch.dlcm.DLCMConstants.BAGIT_FIXED_FILES;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;

import gov.loc.repository.bagit.hash.StandardSupportedAlgorithms;
import gov.loc.repository.bagit.hash.SupportedAlgorithm;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.oais.InfoPackageInterface;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.preingest.Deposit;

public abstract class DLCMService {

  private static final Logger log = LoggerFactory.getLogger(DLCMService.class);

  protected static final String ERROR_AIP_CONTAINER = "archival.aip.error.container";
  protected static final String PROCESSED_MESSAGE = "processed";
  protected static final String PROCESSED_MESSAGE_WITH_STATUS = "processed (statusMessage: ";
  protected static final String LOG_OBJECT_WITH_STATUS_TEMPLATE = "{} '{}' ({}) (status: {}): {}";

  protected final long batItInternalFileNumber;
  protected List<SupportedAlgorithm> supportedChecksumAlgo = new ArrayList<>();

  protected final MessageService messageService;

  protected DLCMService(MessageService messageService, DLCMProperties dlcmProperties) {
    this.messageService = messageService;
    Arrays.asList(dlcmProperties.getParameters().getChecksumList())
            .forEach(el -> this.supportedChecksumAlgo.add(StandardSupportedAlgorithms.valueOf(el)));
    this.batItInternalFileNumber = (dlcmProperties.getParameters().getChecksumList().length * 2L) + BAGIT_FIXED_FILES.size();
  }

  protected <T extends Resource> boolean belongsToList(T item, List<T> list) {
    return list.stream().anyMatch(o -> item.getResId().equals(o.getResId()));
  }

  protected long getPackageFileNumber(ArchiveContainer aipContainer, URI packageUri) {
    final ZipTool zip = new ZipTool(packageUri);
    return switch (aipContainer) {
      case BAG_IT -> zip.getFileNumber() - this.batItInternalFileNumber;
      case ZIP -> zip.getFileNumber();
      default -> throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { aipContainer }));
    };
  }

  protected long getPackageUpdateNumber(ArchiveContainer aipContainer, Path packagePath) {
    List<Path> metadataFiles = switch (aipContainer) {
      case BAG_IT -> FileTool.findFiles(packagePath.resolve(DLCMConstants.DATA_FOLDER));
      case ZIP -> FileTool.findFiles(packagePath);
      default -> throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { aipContainer }));
    };
    return metadataFiles.stream()
            .filter(f -> f.getFileName().toString().startsWith(DLCMConstants.REPRESENTATION_INFO_METADATA_FILE_PREFIX)).count();
  }

  protected void inError(InfoPackageInterface<?> infoPackage, String msg) {
    this.inError(PackageStatus.IN_ERROR, infoPackage, msg);
  }

  protected void inError(PackageStatus errorStatus, InfoPackageInterface<?> infoPackage, String msg) {
    infoPackage.getInfo().setStatusWithMessage(errorStatus, msg);
    if (log.isErrorEnabled()) {
      log.error("{} for {} {}", msg, this.getInfoPackageType(infoPackage), infoPackage.getResId());
    }
  }

  protected void inError(InfoPackageInterface<?> infoPackage, String msg, String cause) {
    this.inError(PackageStatus.IN_ERROR, infoPackage, msg, cause);
  }

  protected void inError(PackageStatus errorStatus, InfoPackageInterface<?> infoPackage, String msg, String cause) {
    infoPackage.getInfo().setStatusWithMessage(errorStatus, msg + " : " + cause);
    if (log.isErrorEnabled()) {
      log.error("{} ({}) for {} {}", msg, cause, this.getInfoPackageType(infoPackage), infoPackage.getResId());
    }
  }

  protected void deleteFile(URI fileToDelete) {
    try {
      FileTool.deleteFile(fileToDelete);
    } catch (final IOException e) {
      throw new SolidifyFileDeleteException(fileToDelete.toString(), e);
    }
  }

  private String getInfoPackageType(InfoPackageInterface<?> infoPackage) {
    if (infoPackage instanceof SubmissionInfoPackage) {
      return "SIP";
    } else if (infoPackage instanceof ArchivalInfoPackage) {
      return "AIP";
    } else if (infoPackage instanceof DisseminationInfoPackage) {
      return "DIP";
    }
    return "[unknown information package type]";
  }

  protected void logWillBeProcessedPackage(InfoPackageInterface<?> infoPackage) {
    this.logPackageMessage(LogLevel.INFO, infoPackage, "will be processed");
  }

  protected void logProcessedPackage(InfoPackageInterface<?> infoPackage) {
    if (infoPackage.getInfo().getStatus() == PackageStatus.COMPLETED) {
      this.logPackageMessage(LogLevel.INFO, infoPackage, PROCESSED_MESSAGE);
    } else if (infoPackage.getInfo().getStatus() == PackageStatus.IN_ERROR) {
      this.logPackageMessage(LogLevel.WARN, infoPackage, PROCESSED_MESSAGE_WITH_STATUS + infoPackage.getInfo().getStatusMessage() + ")");
    } else {
      this.logPackageMessage(LogLevel.DEBUG, infoPackage, PROCESSED_MESSAGE);
    }
  }

  protected void logPackageMessage(LogLevel logLevel, InfoPackageInterface<?> infoPackage, String message, Exception... e) {
    this.logObjectWithStatusMessage(logLevel, this.getInfoPackageType(infoPackage), infoPackage.getResId(),
            infoPackage.getInfo().getStatus().toString(), infoPackage.getInfo().getName(), message, e);
  }

  protected void logDepositMessage(LogLevel logLevel, Deposit deposit, String message, Exception... e) {
    this.logObjectWithStatusMessage(logLevel, "Deposit", deposit.getResId(), deposit.getStatus().toString(), deposit.getTitle(), message, e);
  }

  protected void logProcessedDeposit(Deposit deposit) {
    if (deposit.getStatus() == Deposit.DepositStatus.COMPLETED) {
      this.logDepositMessage(LogLevel.INFO, deposit, PROCESSED_MESSAGE);
    } else if (deposit.getStatus() == Deposit.DepositStatus.IN_ERROR) {
      this.logDepositMessage(LogLevel.WARN, deposit, PROCESSED_MESSAGE_WITH_STATUS + deposit.getStatusMessage() + ")");
    } else {
      this.logDepositMessage(LogLevel.DEBUG, deposit, PROCESSED_MESSAGE);
    }
  }

  protected void logDataFileMessage(LogLevel logLevel, AbstractDataFile<?, ?> df, String message, Exception... e) {
    this.logObjectWithStatusMessage(logLevel, df.getClass().getSimpleName(), df.getResId(), df.getStatus().toString(), df.getFileName(), message,
            e);
  }

  protected void logProcessedDataFile(AbstractDataFile<?, ?> df) {
    if (df.getStatus() == DataFileStatus.READY) {
      this.logDataFileMessage(LogLevel.INFO, df, PROCESSED_MESSAGE);
    } else if (df.getStatus() == DataFileStatus.IN_ERROR) {
      this.logDataFileMessage(LogLevel.WARN, df, PROCESSED_MESSAGE_WITH_STATUS + df.getStatusMessage() + ")");
    } else {
      this.logDataFileMessage(LogLevel.DEBUG, df, PROCESSED_MESSAGE);
    }
  }

  private void logObjectWithStatusMessage(LogLevel logLevel, String className, String resId, String status, String name, String message,
          Exception... exceptions) {
    switch (logLevel) {
      case TRACE -> log.trace(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
      case DEBUG -> log.debug(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
      case WARN -> log.warn(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
      case ERROR -> {
        Exception e = (exceptions.length > 0) ? exceptions[0] : null;
        log.error(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message, e);
      }
      default -> log.info(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
    }
  }
}

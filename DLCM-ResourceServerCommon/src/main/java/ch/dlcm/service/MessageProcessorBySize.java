/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - MessageProcessorBySize.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.io.Serial;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.unit.DataSize;

import ch.unige.solidify.SolidifyConstants;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.message.PackageMessage;

public abstract class MessageProcessorBySize<T extends PackageMessage> {
  private static final Logger log = LoggerFactory.getLogger(MessageProcessorBySize.class);

  final ExecutorService bigSizeExecutor;
  final ExecutorService smallSizeExecutor;
  private final DataSize fileSizeLimit;

  protected MessageProcessorBySize(DLCMProperties dlcmProperties) {
    this.bigSizeExecutor = Executors.newFixedThreadPool(1);
    this.smallSizeExecutor = Executors.newFixedThreadPool(1);
    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit();
  }

  public abstract void processMessage(T message);

  public abstract void receiveMessage(T message);

  void sendForParallelProcessing(T message) {
    if (message.isBigPackage()) {
      log.info("Package '{}' sent in thread for big size packages ({})", message.getResId(), this.getClass().getSimpleName());
      this.bigSizeExecutor.submit(this.runnableWithStacktrace(() -> this.processMessage(message)));
    } else {
      log.info("Package '{}' sent in thread for small size packages ({})", message.getResId(), this.getClass().getSimpleName());
      this.smallSizeExecutor.submit(this.runnableWithStacktrace(() -> this.processMessage(message)));
    }
  }

  void setSecurityContext() {
    UserDetails messageProcessorUserDetails = new MessageProcessorUserDetails();
    SecurityContext securityContext = new SecurityContextImpl();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(messageProcessorUserDetails, null,
            messageProcessorUserDetails.getAuthorities()));
    SecurityContextHolder.setContext(securityContext);
  }

  private static class MessageProcessorUserDetails implements UserDetails {

    @Serial
    private static final long serialVersionUID = 1289831691500128412L;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
      return Arrays.asList(new SimpleGrantedAuthority(SolidifyConstants.MESSAGE_PROCESSOR_ROLE));
    }

    @Override
    public String getPassword() {
      return null;
    }

    @Override
    public String getUsername() {
      return DLCMConstants.MESSAGE_PROCESSOR_NAME;
    }

    @Override
    public boolean isAccountNonExpired() {
      return false;
    }

    @Override
    public boolean isAccountNonLocked() {
      return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
      return true;
    }

    @Override
    public boolean isEnabled() {
      return true;
    }
  }

  protected boolean isBigPackageSize(long bytes) {
    return bytes > this.fileSizeLimit.toBytes();
  }

  private Runnable runnableWithStacktrace(Runnable runnable) {
    return () -> {
      try {
        runnable.run();
      } catch (Throwable t) {
        log.error("Cannot run thread", t);
      }
    };
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - HistoryService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.time.OffsetDateTime;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.security.User;
import ch.dlcm.repository.HistoryRepository;
import ch.dlcm.service.rest.fallback.FallbackUserRemoteResourceService;

@Service
public class HistoryService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(HistoryService.class);

  private final HistoryRepository historyRepository;
  private final FallbackUserRemoteResourceService userResourceService;

  public HistoryService(MessageService messageService, HistoryRepository historyRepository,
          FallbackUserRemoteResourceService userResourceService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.historyRepository = historyRepository;
    this.userResourceService = userResourceService;
  }

  public Page<StatusHistory> findByResId(String resId, Pageable pageable) {
    final Page<StatusHistory> listItem = this.historyRepository.findByResId(resId, pageable);

    listItem.forEach(statusHistory -> {
      final User user = this.userResourceService.findByExternalUid(statusHistory.getCreatedBy());
      if (user != null && user.getPerson() != null) {
        statusHistory.setCreatorName(user.getPerson().getFullName());
      }
    });

    return listItem;
  }

  public List<StatusHistory> getHistory(String resId) {
    return this.historyRepository.findByResId(resId);
  }

  @EventListener
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void saveEvent(StatusHistory event) {
    if (log.isTraceEnabled()) {
      log.trace("Received StatusHistory event {}", event);
    }
    // Read last event
    final List<StatusHistory> last = this.historyRepository.findListMostRecentFirst(event.getResId());
    if (last.isEmpty() || !last.get(0).getStatus().equals(event.getStatus())) {
      // Add new event
      this.checkEvent(event);
      this.historyRepository.save(event);
    } else if (!StringTool.isNullOrEmpty(event.getDescription())) {
      // Update description of last event
      last.get(0).setDescription(event.getDescription());
      this.historyRepository.save(last.get(0));
    }
  }

  public void checkLastEvents(String resId, String errorMessage, String... statusList) {
    // Sort history by date
    final List<StatusHistory> sortedList = this.sortStatusList(resId);

    for (int i = 0; i < statusList.length; i++) {
      // Check status
      if (!sortedList.get(sortedList.size() - 1 - i).getStatus().equals(statusList[i])) {
        throw new SolidifyCheckingException(errorMessage);
      }
    }
  }

  public String getLastStatus(String resId) {
    return this.getLastStatusHistory(resId).getStatus();
  }

  public String getStatusBeforeError(String resId, String errorStatus) {
    // Sort history by date
    final List<StatusHistory> sortedList = this.sortStatusList(resId);

    // Check if error
    if (sortedList.size() < 2 || !sortedList.get(sortedList.size() - 1).getStatus().equals(errorStatus)) {
      throw new SolidifyCheckingException("Entity " + resId + " not in error");
    }
    return sortedList.get(sortedList.size() - 2).getStatus();
  }

  public boolean hasBeenUpdatingMetadata(String resId) {
    final StatusHistory lastStatusHistory = this.getLastStatusHistory(resId);
    if (lastStatusHistory.getStatus().equals(Deposit.DepositStatus.CANCEL_EDITING_METADATA.toString())) {
      return false;
    }
    final OffsetDateTime lastEditionTime = this.findLastStatusTime(resId, PackageStatus.EDITING_METADATA.toString());

    if (lastEditionTime == null) {
      return false;
    }
    // check if there is a completed status after, if so, it is from a previous editing_metadata
    List<StatusHistory> statusHistoryList = this.historyRepository
            .findListWithStatusAfterTimeOldestFirst(resId, PackageStatus.COMPLETED.toString(), lastEditionTime);
    return statusHistoryList.isEmpty();
  }

  public boolean hasBeenDisposableAndNotApproved(String resId) {
    final StatusHistory lastStatusHistory = this.getLastStatusHistory(resId);
    if (PackageStatus.isDisposalApprovedProcess(lastStatusHistory.getStatus())) {
      return false;
    }
    final OffsetDateTime lastDisposableTime = this.findLastStatusTime(resId, PackageStatus.DISPOSABLE.toString());

    if (lastDisposableTime == null) {
      return false;
    }
    // check if there is a completed status after, if so, it is from a retention extension of the archive
    List<StatusHistory> statusHistoryList = this.historyRepository
            .findListWithStatusAfterTimeOldestFirst(resId, PackageStatus.UPDATING_RETENTION.toString(), lastDisposableTime);
    return statusHistoryList.isEmpty();
  }

  public boolean hasBeenCleaned(String resId) {
    return this.historyRepository.hasBeenInStatus(resId, Deposit.DepositStatus.CLEANED.name());
  }

  public boolean hasBeenCompleted(String resId) {
    return this.historyRepository.hasBeenInStatus(resId, Deposit.DepositStatus.COMPLETED.name());
  }

  public boolean hasSubmissionAgreementApproved(String resId) {
    return this.historyRepository.hasBeenInStatus(resId, Deposit.DepositStatus.SUBMISSION_AGREEMENT_APPROVED.name());
  }

  /**
   * Return true if the object has been in EDITING_METADATA status since the last time it was in
   * COMPLETED status. It allows discarding updates from other operations (redindexing, checking,
   * etc.)
   *
   * @param resId
   * @return
   */
  public boolean hasBeenUpdatedSinceLastCompletedStatus(String resId) {
    final OffsetDateTime lastCompleted = this.findLastStatusTime(resId, Deposit.DepositStatus.COMPLETED.toString());
    final OffsetDateTime updateAfterLastCompleted = this.findFirstStatusTimeAfter(resId, PackageStatus.EDITING_METADATA.toString(),
            lastCompleted);
    return updateAfterLastCompleted != null;
  }

  public OffsetDateTime getLastCompletedUpdate(String resId) {
    final OffsetDateTime lastCompleted = this.findLastStatusTime(resId, PackageStatus.COMPLETED.toString());
    final OffsetDateTime lastEdition = this.findLastStatusTimeBefore(resId, PackageStatus.EDITING_METADATA.toString(), lastCompleted);
    final OffsetDateTime firstCompleted = this.findFirstStatusTime(resId, PackageStatus.COMPLETED.toString());

    if (lastEdition == null) {
      return firstCompleted;
    } else {
      return this.findFirstStatusTimeAfter(resId, PackageStatus.COMPLETED.toString(), lastEdition);
    }
  }

  public OffsetDateTime findLastCompletedOrCleanedTime(String resId) {
    final OffsetDateTime lastCompleted = this.findLastStatusTime(resId, Deposit.DepositStatus.COMPLETED.toString());
    final OffsetDateTime lastCleaned = this.findLastStatusTime(resId, Deposit.DepositStatus.CLEANED.toString());
    if (lastCompleted == null) {
      return null;
    } else if (lastCleaned == null) {
      return lastCompleted;
    } else {
      if (lastCompleted.isAfter(lastCleaned)) {
        return lastCompleted;
      } else {
        return lastCleaned;
      }
    }
  }

  public StatusHistory findLastCompletedOrCleaned(String resId) {
    final List<StatusHistory> statusList = this.historyRepository.findListCompletedOrCleanedStatusMostRecentFirst(resId);
    if (statusList.isEmpty()) {
      return null;
    } else {
      return statusList.get(0);
    }
  }

  private OffsetDateTime findLastStatusTime(String resId, String status) {
    List<StatusHistory> statusHistoryList = this.historyRepository.findListWithStatusMostRecentFirst(resId, status);
    if (statusHistoryList.isEmpty()) {
      return null;
    } else {
      return statusHistoryList.get(0).getChangeTime();
    }
  }

  private OffsetDateTime findFirstStatusTime(String resId, String status) {
    List<StatusHistory> statusHistoryList = this.historyRepository.findListWithStatusOldestFirst(resId, status);
    if (statusHistoryList.isEmpty()) {
      return null;
    } else {
      return statusHistoryList.get(0).getChangeTime();
    }
  }

  private OffsetDateTime findFirstStatusTimeAfter(String resId, String status, OffsetDateTime after) {
    List<StatusHistory> statusHistoryList = this.historyRepository.findListWithStatusAfterTimeOldestFirst(resId, status, after);
    if (statusHistoryList.isEmpty()) {
      return null;
    } else {
      return statusHistoryList.get(0).getChangeTime();
    }
  }

  private OffsetDateTime findLastStatusTimeBefore(String resId, String status, OffsetDateTime before) {
    List<StatusHistory> statusHistoryList = this.historyRepository.findListWithStatusBeforeTimeMostRecentFirst(resId, status, before);
    if (statusHistoryList.isEmpty()) {
      return null;
    } else {
      return statusHistoryList.get(0).getChangeTime();
    }
  }

  private StatusHistory getLastStatusHistory(String resId) {
    // Sort history by date
    final List<StatusHistory> sortedList = this.sortStatusList(resId);
    return sortedList.get(sortedList.size() - 1);
  }

  private List<StatusHistory> sortStatusList(String resId) {
    return this.getHistory(resId).stream()
            .sorted(Comparator.comparing(StatusHistory::getSeq))
            .toList();
  }

  public List<StatusHistory> getLastProcess(String resId) {
    List<StatusHistory> fullHistory = this.sortStatusList(resId);
    int firstLast = 0;
    int secondLast = 0;
    // Check last status is completed or error
    String lastStatus = fullHistory.get(fullHistory.size() - 1).getStatus();
    if (PackageStatus.isCompletedProcess(PackageStatus.valueOf(lastStatus))
            || PackageStatus.isInError(PackageStatus.valueOf(lastStatus))) {
      firstLast = 1;
    }
    // Find last completed or last error
    for (int i = 0; i < fullHistory.size() - firstLast; i++) {
      String currentStatus = fullHistory.get(i).getStatus();
      if (!currentStatus.equals(StatusHistory.CREATED_STATUS)
              && (PackageStatus.isCompletedProcess(PackageStatus.valueOf(currentStatus))
              || PackageStatus.isInError(PackageStatus.valueOf(currentStatus)))) {
        secondLast = i;
      }
    }
    return fullHistory.subList(secondLast + 1, fullHistory.size() - firstLast);
  }

  public List<StatusHistory> getDepositLastSubmission(String resId) {
    List<StatusHistory> fullHistory = this.sortStatusList(resId);
    int firstLast = 0;
    int secondLast = 0;
    // Check last status is completed or error
    String lastStatus = fullHistory.get(fullHistory.size() - 1).getStatus();
    if (DepositStatus.isCompletedProcess(DepositStatus.valueOf(lastStatus))
            || DepositStatus.isInError(DepositStatus.valueOf(lastStatus))) {
      firstLast = 1;
    }
    // Find last completed or last error
    for (int i = 0; i < fullHistory.size() - firstLast; i++) {
      String currentStatus = fullHistory.get(i).getStatus();
      if (!currentStatus.equals(StatusHistory.CREATED_STATUS)
              && (DepositStatus.isCompletedProcess(DepositStatus.valueOf(currentStatus))
              || DepositStatus.isInError(DepositStatus.valueOf(currentStatus)))) {
        secondLast = i;
      }
    }
    return fullHistory.subList(secondLast + 1, fullHistory.size() - firstLast);
  }

  private void checkEvent(StatusHistory event) {
    if (event.getDescription() == null) {
      String msg = "status.history";
      if (event.getType().equals("Deposit")) {
        msg = "preingest.deposit.status." + event.getStatus().toLowerCase();
      } else if (event.getType().endsWith("Order")) {
        msg = "access.order.status." + event.getStatus().toLowerCase();
      } else if (event.getType().endsWith("DataFile")) {
        msg = "datafile.status." + event.getStatus().toLowerCase();
      } else if (event.getType().endsWith("InfoPackage")) {
        msg = "package.status." + event.getStatus().toLowerCase();
      } else if (event.getType().equals("PreservationJob") || event.getType().equals("JobExecution")) {
        msg = "pres-planning.job.status." + event.getStatus().toLowerCase();
      } else if (event.getType().equals("Notification")) {
        msg = "notification.status." + event.getStatus().toLowerCase();
      }
      event.setDescription(this.messageService.get(msg));
    }
  }

}

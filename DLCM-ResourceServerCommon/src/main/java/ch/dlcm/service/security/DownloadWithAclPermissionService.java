/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Security - DownloadWithAclPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import java.security.Principal;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyMfaNeededException;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.service.security.SolidifyPermissionService;

import ch.dlcm.model.DataTag;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.service.SearchService;
import ch.dlcm.service.rest.fallback.FallbackArchiveACLRemoteResourceService;

@Service
public final class DownloadWithAclPermissionService implements SolidifyPermissionService {

  private final HttpRequestInfoProvider httpRequestInfoProvider;
  private final FallbackArchiveACLRemoteResourceService archiveACLResourceService;
  private final MfaPermissionService mfaPermissionService;
  private final SearchService searchService;

  public DownloadWithAclPermissionService(HttpRequestInfoProvider httpRequestInfoProvider,
          FallbackArchiveACLRemoteResourceService archiveACLResourceService,
          MfaPermissionService mfaPermissionService,
          SearchService searchService) {
    this.httpRequestInfoProvider = httpRequestInfoProvider;
    this.archiveACLResourceService = archiveACLResourceService;
    this.mfaPermissionService = mfaPermissionService;
    this.searchService = searchService;
  }

  public boolean isAllowed(String aipId) {
    final Principal principal = this.httpRequestInfoProvider.getPrincipal();
    boolean isAllowed = false;
    if (principal != null) {
      List<ArchiveACL> acls = this.archiveACLResourceService.findMyAcl();
      for (ArchiveACL acl : acls) {
        if (acl.getAipId().equals(aipId) && !acl.isExpired() && Boolean.FALSE.equals(acl.isDeleted())) {
          isAllowed = true;
        }
      }
    }

    // CRIMSON and RED archives need MFA to be downloaded
    final ArchiveMetadata archiveMetadata = this.searchService.findOne(aipId);
    if (archiveMetadata == null) {
      throw new NoSuchElementException("Archive not found");
    }
    if (isAllowed
            && (archiveMetadata.getDataTag().equals(DataTag.CRIMSON) || archiveMetadata.getDataTag().equals(DataTag.RED))
            && !this.mfaPermissionService.isMfaAuthenticated()) {
      throw new SolidifyMfaNeededException();
    }
    return isAllowed;
  }
}

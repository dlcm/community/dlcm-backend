/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Security - MfaPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.stereotype.Service;

import net.minidev.json.JSONArray;

@Service
public class MfaPermissionService {

  private static final String MFA_SCOPE = "auth-mfa";
  public boolean isMfaAuthenticated() {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (!(authentication instanceof BearerTokenAuthentication)) {
      return false;
    }
    BearerTokenAuthentication bearer = (BearerTokenAuthentication) authentication;
    JSONArray scopeArray = (JSONArray) bearer.getTokenAttributes().get("scope");
    if (scopeArray == null || scopeArray.isEmpty()) {
      throw new BadCredentialsException("Token is missing scope");
    }
    for(Object scope : scopeArray) {
      if (!(scope instanceof String)) {
        throw new BadCredentialsException("Scope should be a String");
      }
      if (scope.equals(MFA_SCOPE)) {
        return true;
      }
    }
    return false;
  }
}

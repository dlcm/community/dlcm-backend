/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - AbstractPermissionWithOrgUnitService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.security.SolidifyPermissionService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.security.Role;
import ch.dlcm.service.rest.abstractservice.PersonRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;

public abstract class AbstractPermissionWithOrgUnitService implements SolidifyPermissionService {

  private static final Logger logger = LoggerFactory.getLogger(AbstractPermissionWithOrgUnitService.class);

  protected final PersonRemoteResourceService trustedPersonRemoteResourceService;

  protected AbstractPermissionWithOrgUnitService(TrustedPersonRemoteResourceService trustedPersonRemoteResourceService) {
    this.trustedPersonRemoteResourceService = trustedPersonRemoteResourceService;
  }

  public boolean isAllowed(String targetResId, String actionString) {

    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    OrganizationalUnitAwareResource existingResource = this.getExistingResource(targetResId);
    if (existingResource == null) {
      throw new NoSuchElementException(targetResId);
    }
    DLCMControllerAction action = this.getControllerAction(actionString);
    final String personId = this.getPersonId();
    return this.isAllowedToPerformActionOnResource(personId, existingResource, action);

  }

  public boolean isAllowedToCreate(OrganizationalUnitAwareResource resource) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    String personId = this.getPersonId();
    return this.isAllowedToPerformActionOnResource(personId, resource, DLCMControllerAction.CREATE);
  }

  public boolean isAllowedToUpdate(String targetResId) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    String personId = this.getPersonId();
    OrganizationalUnitAwareResource existingResource = this.getExistingResource(targetResId);
    return this.isAllowedToPerformActionOnResource(personId, existingResource, DLCMControllerAction.UPDATE);
  }

  protected boolean isVisitorAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return false;
  }

  protected boolean isApproverAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return false;
  }

  protected boolean isCreatorAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return false;
  }

  protected boolean isManagerAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return false;
  }

  protected boolean isStewardAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return false;
  }

  protected String getResourceSimpleName(OrganizationalUnitAwareResource existingResource) {

    if (existingResource != null) {
      return existingResource.getClass().getSimpleName();
    } else {
      throw new SolidifyRuntimeException("error while checking user permissions on organizational unit: no given resource to check");
    }
  }

  protected boolean checkActionByRole(Role role, OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    /*
     * Check role specific permissions
     */
    return switch (role.getResId()) {
      case Role.MANAGER_ID -> this.isManagerAllowed(existingResource, action);
      case Role.VISITOR_ID -> this.isVisitorAllowed(existingResource, action);
      case Role.CREATOR_ID -> this.isCreatorAllowed(existingResource, action);
      case Role.STEWARD_ID -> this.isStewardAllowed(existingResource, action);
      case Role.APPROVER_ID -> this.isApproverAllowed(existingResource, action);
      default -> false;
    };
  }

  String getOrganizationalUnitIdToCheckPermission(OrganizationalUnitAwareResource existingResource) {
    if (existingResource != null) {
      return existingResource.getOrganizationalUnitId();
    } else {
      throw new SolidifyRuntimeException("error while checking user permissions on organizational unit: no given resource to check");
    }
  }

  String getPersonId() {
    return this.trustedPersonRemoteResourceService.getLinkedPersonId(this.getAuthentication());
  }

  Authentication getAuthentication() {
    return SecurityContextHolder.getContext().getAuthentication();
  }

  DLCMControllerAction getControllerAction(String actionString) {
    try {
      return DLCMControllerAction.valueOf(actionString);
    } catch (final IllegalArgumentException e) {
      final String errorMessage = "Unable to check for access permission: the action name '" + actionString + "' is wrong";
      logger.error(errorMessage);
      throw new SolidifyRuntimeException(errorMessage, e);
    }
  }

  boolean isRoleAllowed(Role role, OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    /*
     * All roles can perform these actions
     */
    if (action == DLCMControllerAction.GET ||
            action == DLCMControllerAction.GET_FILE ||
            action == DLCMControllerAction.LIST_FILES ||
            action == DLCMControllerAction.DOWNLOAD_FILE ||
            action == DLCMControllerAction.HISTORY) {
      return true;
    }
    return this.checkActionByRole(role, existingResource, action);
  }

  protected boolean isAllowedToPerformActionOnResource(String personId, OrganizationalUnitAwareResource existingResource,
          DLCMControllerAction action) {

    try {
      final String organizationalUnitId = this.getOrganizationalUnitIdToCheckPermission(existingResource);
      final String resourceSimpleName = this.getResourceSimpleName(existingResource);
      String existingResourceId = null;
      if (existingResource != null) {
        existingResourceId = existingResource.getResId();
      }

      if (StringTool.isNullOrEmpty(organizationalUnitId)) {
        logger.error("Unable to check permissions on {}: organizationalUnitId is not set", resourceSimpleName,
                new SolidifyRuntimeException("Unable to check permissions"));
        return false;
      }

      if (StringTool.isNullOrEmpty(personId)) {
        logger.warn("Unable to check permissions on {} ({}): personId is not set", resourceSimpleName, existingResourceId);
        return false;
      }

      final Optional<Role> orgUnitRole = this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(personId, organizationalUnitId);
      final Optional<Role> inheritedRole = this.trustedPersonRemoteResourceService.findInheritedOrganizationalRole(personId,
              organizationalUnitId);
      final Optional<Role> role = Role.maxRole(orgUnitRole, inheritedRole);

      if (role.isEmpty()) {
        return false;
      }

      if (this.isRoleAllowed(role.get(), existingResource, action)) {

        logger.trace("The person '{}' with role '{}' is allowed to perform the action '{}' on {} for organizational unit '{}'", personId,
                role.get().getName(), action, resourceSimpleName, organizationalUnitId);

        return true;
      }

      logger.warn("The person '{}' is not allowed to perform the action '{}' on {} '{}' for organizational unit '{}'", personId, action,
              resourceSimpleName, existingResourceId, organizationalUnitId);

      return false;

    } catch (final RuntimeException e) {
      logger.error("Error in checking permissions", e);
      return false;
    }
  }

  abstract OrganizationalUnitAwareResource getExistingResource(String resId);

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - AbstractAipDataFileListenerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.AipDataFileService;
import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.message.DataFileMessage;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

public abstract class AbstractAipDataFileListenerService extends AbstractDataFileListenerService<AipDataFile> {
  private static final Logger log = LoggerFactory.getLogger(AbstractAipDataFileListenerService.class);

  private final String archivalLocation;

  private final ArchivalInfoPackageService aipService;

  protected AbstractAipDataFileListenerService(DLCMProperties dlcmProperties,
          MessageService messageService,
          DataFileStatusService dfStatusService,
          TrustedUserRemoteResourceService userRemoteService,
          TrustedNotificationRemoteResourceService notificationRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService,
          AipDataFileService aipDataFileService,
          ArchivalInfoPackageService aipService) {
    super(dlcmProperties,
            messageService,
            dfStatusService,
            userRemoteService,
            notificationRemoteService,
            organizationalUnitRemoteService,
            aipDataFileService);
    this.archivalLocation = dlcmProperties.getArchivalLocation();
    this.aipService = aipService;
  }

  @JmsListener(destination = "${dlcm.queue.aip-datafile}")
  @Override
  public void receiveDataFileMessage(DataFileMessage dataFileMessage) {
    super.receiveDataFileMessage(dataFileMessage);
  }

  @Override
  protected void processDataFile(DataFileMessage dataFileMessage) {
    log.info("Reading AIP Data File message {}", dataFileMessage);
    try {
      final AipDataFile aipDf = this.processDataFile(this.archivalLocation, dataFileMessage.getResId());
      if (aipDf.getStatus() == DataFileStatus.READY) {
        boolean isBigPackage = this.aipService.getSize(aipDf.getInfoPackage().getResId()) > this.fileSizeLimit;
        this.publishEvent(aipDf.getInfoPackage().getResId(), isBigPackage);
      } else if (aipDf.getStatus() == DataFileStatus.IN_ERROR) {
        this.sendDatafilePackageInErrorNotification(aipDf);
      }
    } catch (final NoSuchElementException e) {
      log.error(e.getMessage());
    }
  }

  private void sendDatafilePackageInErrorNotification(AbstractDataFile dataFile) {
    ArchivalInfoPackage aip = this.aipService.findOne(dataFile.getInfoPackage().getResId());
    String lastModifierId = aip.getLastUpdate().getWho();
    String orgUnitId = aip.getInfo().getOrganizationalUnitId();
    String message = this.getErrorMessage(dataFile.getResId());
    NotificationType notificationType = this.getNotificationType();
    this.sendDatafilePackageInErrorNotification(lastModifierId, message, notificationType, orgUnitId, dataFile.getInfoPackage().getResId());
  }

  protected abstract void publishEvent(String aipDatafileId, boolean isBigPackage);

  protected abstract NotificationType getNotificationType();

  protected abstract String getErrorMessage(String datafileId);

}

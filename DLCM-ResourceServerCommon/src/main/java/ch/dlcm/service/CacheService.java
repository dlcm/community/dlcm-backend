/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - CacheService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import static ch.unige.solidify.SolidifyConstants.EXTERNAL_UID_KEY;
import static ch.unige.solidify.SolidifyConstants.SEARCH_KEY;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.message.ResourceCacheMessage;
import ch.unige.solidify.service.AbstractCacheService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.message.cache.InstitutionPersonCacheMessage;
import ch.dlcm.message.cache.OrganizationalUnitPersonCacheMessage;
import ch.dlcm.model.dto.AverageRating;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;

@Service
public class CacheService extends AbstractCacheService {

  private static final Logger logger = LoggerFactory.getLogger(CacheService.class);

  /**
   * Autowiring itself allows cache to work on a method inside its own class
   */
  protected CacheService self;

  CacheManager cacheManager;

  private final Map<String, List<OrganizationalUnit>> cachedOrganizationalUnits = new ConcurrentHashMap<>();

  private final Map<String, Date> cachedOrganizationalUnitsDates = new ConcurrentHashMap<>();

  /**
   * Cache for ArchiveUserRating where is stored: archiveId and list of AverageRating: ratingType and
   * value(average).
   */
  private final Map<String, List<AverageRating>> cachedArchiveAverageRating = new ConcurrentHashMap<>();

  private final Map<String, Date> cachedArchiveAverageRatingDates = new ConcurrentHashMap<>();

  private final int authorizedUnitsCacheTime;
  private final int archiveRatingCacheTime;

  public CacheService(DLCMProperties dlcmProperties, @Lazy CacheService self, CacheManager cacheManager) {
    this.self = self;
    this.cacheManager = cacheManager;
    this.authorizedUnitsCacheTime = dlcmProperties.getParameters().getAuthorizedUnitsCacheTime();
    this.archiveRatingCacheTime = dlcmProperties.getParameters().getArchiveRatingCacheTime();
  }

  /**
   * Clear entry in OrganizationalUnitRoles cache identified by a personId and an organizational unit id
   *
   * @param personId             The personId
   * @param organizationalUnitId The organizationalUnitId
   */
  @CacheEvict(cacheNames = DLCMCacheNames.ORGANIZATIONAL_UNIT_ROLES)
  public void cleanCacheOrganizationalUnitRoles(String personId, String organizationalUnitId) {
    CacheService.logger
            .trace("'OrganizationalUnitRoles' cache cleared for: personId={}, organizationalUnitId={}", personId, organizationalUnitId);
    this.cachedOrganizationalUnits.clear();
    this.cachedOrganizationalUnitsDates.clear();
  }

  /**
   * Clear the whole InheritedOrganizationalUnitRoles cache
   */
  @CacheEvict(cacheNames = DLCMCacheNames.INHERITED_ORGANIZATIONAL_UNIT_ROLE)
  public void cleanCacheInheritedOrganizationalUnitRoles() {
    // Clean all cache because:
    // 1) Changing institutional roles happens rarely
    // 2) The JMS message InstitutionPersonCacheMessage is sent when an IPR relation is changed and the cache
    // INHERITED_ORGANIZATIONAL_UNIT_ROLE store role for personId and orgUnitId which belongs to an institution, clearing individual entry
    // implies to send REST calls to obtain all org units belonging to an institution
    CacheService.logger.trace("'InheritedOrganizationalUnitRoles' cache cleared");
  }

  /**
   * Clear entries in INSTITUTIONS_ROLES_BY_PERSON cache that contains institutions with roles for a person.
   * All cache content is cleaned because changing institutional roles happens rarely.
   */
  @CacheEvict(cacheNames = { DLCMCacheNames.INSTITUTIONS_ROLES_BY_PERSON })
  public void clearCachedInstitutionsRolesByPerson() {
    CacheService.logger.trace("'{}' cache cleared", DLCMCacheNames.INSTITUTIONS_ROLES_BY_PERSON);
  }

  /**
   * Clear entry in search cache identified by query
   */
  @CacheEvict(cacheNames = DLCMCacheNames.SEARCH, allEntries = true)
  public void cleanCacheSearch() {
    CacheService.logger.trace("'search' cache cleared ");
  }

  /**
   * Clear entry in UserExternalUid cache identified by externalUid
   *
   * @param externalUid The externalUid of the user
   */
  @CacheEvict(cacheNames = DLCMCacheNames.USER_EXTERNAL_UID)
  public void cleanCacheUserExternalUid(String externalUid) {
    CacheService.logger.trace("'UserExternalUid' cache cleared for externalUid: {}", externalUid);
  }

  public List<OrganizationalUnit> getCachedAuthorizedOrganizationalUnits(String cacheKey) {

    if (!StringTool.isNullOrEmpty(cacheKey)) {
      final Date cacheDate = this.cachedOrganizationalUnitsDates.get(cacheKey);

      if (cacheDate != null && (System.currentTimeMillis() - cacheDate.getTime()) < this.authorizedUnitsCacheTime * 1000) {
        /*
         * organizationalUnits taken from the cache
         */
        CacheService.logger.trace("organizational units taken from cache for {}", cacheKey);

        return this.cachedOrganizationalUnits.get(cacheKey);
      } else {
        /*
         * organizationalUnits have not been cached yet or cache is too old
         */
        this.cachedOrganizationalUnits.remove(cacheKey);
        return Collections.emptyList();
      }
    } else {
      return Collections.emptyList();
    }
  }

  public List<AverageRating> getCachedArchiveAverageRating(String cacheKey) {
    if (!StringTool.isNullOrEmpty(cacheKey)) {
      final Date cacheDate = this.cachedArchiveAverageRatingDates.get(cacheKey);

      if (cacheDate != null && (System.currentTimeMillis() - cacheDate.getTime()) < this.archiveRatingCacheTime * 1000) {
        /*
         * AverageRating for this Archive taken from the cache
         */
        CacheService.logger.trace("Average Rating taken from cache for {}", cacheKey);

        return this.cachedArchiveAverageRating.get(cacheKey);
      } else {
        /*
         * RatingGlobals for this Archive have not been cached yet or cache is too old
         */
        this.cachedArchiveAverageRating.remove(cacheKey);
        return Collections.emptyList();
      }
    } else {
      return Collections.emptyList();
    }
  }

  @JmsListener(destination = "${dlcm.topic.cache}", containerFactory = "topicListenerFactory")
  public void processCacheMessage(CacheMessage message) {

    if (message instanceof ResourceCacheMessage resourceCacheMessage) {

      Set<String> resourceIdsToRemoveFromCache = this.getResourceIdsToClean(resourceCacheMessage.getResId());
      for (String resourceIdToClean : resourceIdsToRemoveFromCache) {
        this.self.clearCachedResource(resourceIdToClean);
      }

      if (message.hasOtherProperty(SEARCH_KEY)) {
        this.self.cleanCacheSearch();
      }

      if (((ResourceCacheMessage) message).getResourceClass() == User.class) {
        this.self.cleanCacheUserExternalUid(message.getOtherProperty(EXTERNAL_UID_KEY).toString());
      }

    } else if (message instanceof OrganizationalUnitPersonCacheMessage oupCacheMessage) {
      this.self.cleanCacheOrganizationalUnitRoles(oupCacheMessage.getPersonId(), oupCacheMessage.getOrganizationalUnitId());
    } else if (message instanceof InstitutionPersonCacheMessage) {
      this.self.cleanCacheInheritedOrganizationalUnitRoles();
      this.self.clearCachedInstitutionsRolesByPerson();
    }
  }

  public void storeAuthorizedOrganizationalUnits(String cacheKey, List<OrganizationalUnit> organizationalUnits) {
    if (!StringTool.isNullOrEmpty(cacheKey)) {
      CacheService.logger.trace("organizational units put in cache for {}", cacheKey);
      this.cachedOrganizationalUnits.put(cacheKey, organizationalUnits);
      this.cachedOrganizationalUnitsDates.put(cacheKey, new Date());
    }
  }

  public void storeArchiveAverageRating(String cacheKey, List<AverageRating> averageRatings) {
    if (!StringTool.isNullOrEmpty(cacheKey)) {
      CacheService.logger.trace("Average Rating put in cache for {}", cacheKey);
      this.cachedArchiveAverageRating.put(cacheKey, averageRatings);
      this.cachedArchiveAverageRatingDates.put(cacheKey, new Date());
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - AbstractDataFileListenerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.ResourceService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.message.DataFileMessage;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

public abstract class AbstractDataFileListenerService<T extends AbstractDataFile<?, ?>> extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(AbstractDataFileListenerService.class);

  protected final long fileSizeLimit;

  private final DataFileStatusService dfStatusService;
  private final TrustedUserRemoteResourceService userRemoteService;
  private final TrustedNotificationRemoteResourceService notificationRemoteService;
  private final TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService;
  private final ResourceService<T> specificDataFileService;
  private final ExecutorService executorService;

  protected AbstractDataFileListenerService(DLCMProperties dlcmProperties,
          MessageService messageService,
          DataFileStatusService dfStatusService,
          TrustedUserRemoteResourceService userRemoteService,
          TrustedNotificationRemoteResourceService notificationRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService,
          ResourceService<T> specificDataFileService) {
    super(messageService, dlcmProperties);
    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit().toBytes();
    this.dfStatusService = dfStatusService;
    this.userRemoteService = userRemoteService;
    this.notificationRemoteService = notificationRemoteService;
    this.organizationalUnitRemoteService = organizationalUnitRemoteService;
    this.specificDataFileService = specificDataFileService;
    this.executorService = Executors.newFixedThreadPool(dlcmProperties.getParameters().getDataFileThreadPoolSize());
  }

  protected void receiveDataFileMessage(DataFileMessage dataFileMessage) {
    this.executorService.submit(() -> {
      try {
        this.processDataFile(dataFileMessage);
      } catch (Throwable t) {
        log.error("Cannot run data file thread", t);
      }
    });
  }

  protected abstract void processDataFile(DataFileMessage dataFileMessage);

  protected T processDataFile(String location, String id) {
    T df = this.specificDataFileService.findOne(id);

    log.trace("Processing Data File '{}': type={} resId={} status={}", df.getFileName(), df.getClass().getSimpleName(), df.getResId(),
            df.getStatus());

    while (df.isInProgress()) {
      df = this.dfStatusService.processDataFile(location, this.specificDataFileService, id);
    }

    log.trace("Data file '{}' processed: type={} resId={} status={}", df.getFileName(), df.getClass().getSimpleName(), df.getResId(),
            df.getStatus());

    return df;
  }

  protected void sendDatafilePackageInErrorNotification(String lastModifierId, String message, NotificationType notificationType,
          String orgUnitId, String dataFileId) {
    OrganizationalUnit organizationalUnit = this.organizationalUnitRemoteService.findOne(orgUnitId);
    User updaterUser = this.userRemoteService.findByExternalUid(lastModifierId);
    this.notificationRemoteService.createNotification(updaterUser, message, notificationType, organizationalUnit, dataFileId);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitListenerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.message.OrganizationalUnitMessage;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;

@Service
public class OrganizationalUnitListenerService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(OrganizationalUnitListenerService.class);

  private final FallbackOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService;

  public OrganizationalUnitListenerService(MessageService messageService,
          FallbackOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.organizationalUnitRemoteResourceService = organizationalUnitRemoteResourceService;
  }

  @JmsListener(destination = "${dlcm.queue.orgunit}")
  public void processOrganizationalUnitData(OrganizationalUnitMessage orgUnit) {
    log.trace("Reading message {}", orgUnit);

    final OrganizationalUnit organizationalUnit = this.organizationalUnitRemoteResourceService.getOrgUnit(orgUnit.getResId());

    // OrgUnit is empty?
    if (organizationalUnit.getIsEmpty()) {
      organizationalUnit.setIsEmpty(false);
      this.organizationalUnitRemoteResourceService.updateOrgUnit(orgUnit.getResId(), organizationalUnit);
    }
  }

}

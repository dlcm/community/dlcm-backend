/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - OrganizationalUnitRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.nio.file.Path;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.display.OrgUnitPersonRoleListDTO;
import ch.dlcm.model.dto.AuthorizedOrganizationalUnitDto;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.SubjectArea;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.CacheService;

public abstract class OrganizationalUnitRemoteResourceService extends RemoteResourceService<OrganizationalUnit> {

  private static final Logger log = LoggerFactory.getLogger(OrganizationalUnitRemoteResourceService.class);

  private final CacheService cacheService;

  private final String adminUrl;

  protected OrganizationalUnitRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService,
          CacheService cacheService) {
    super(restClientService);
    this.cacheService = cacheService;
    this.adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  public RestCollection<OrganizationalUnit> getOrgUnitList(String queryString, Pageable pageable) {
    final StringBuilder url = new StringBuilder(this.adminUrl);
    url.append("/");
    url.append(ResourceName.ORG_UNIT);
    if (!StringTool.isNullOrEmpty(queryString)) {
      url.append("?");
      url.append(queryString);
    }
    String jsonString = this.restClientService.getResource(url.toString(), pageable);
    return new RestCollection<>(jsonString, OrganizationalUnit.class);
  }

  public RestCollection<AuthorizedOrganizationalUnitDto> getAuthorizedOrgUnitWithVisitorRoleList(Pageable pageable) {
    return this.getAuthorizedOrgUnitWithRoleList("openOnly=false&roleSuperiorToVisitor=true", pageable);
  }

  public RestCollection<AuthorizedOrganizationalUnitDto> getAuthorizedOrgUnitWithRoleList(String queryString, Pageable pageable) {
    final StringBuilder url = new StringBuilder(this.adminUrl);
    url.append("/");
    url.append(ResourceName.AUTHORIZED_ORG_UNIT);
    if (!StringTool.isNullOrEmpty(queryString)) {
      url.append("?");
      url.append(queryString);
    }
    String jsonString = this.restClientService.getResource(url.toString(), pageable);
    return new RestCollection<>(jsonString, AuthorizedOrganizationalUnitDto.class);
  }

  public Optional<AuthorizedOrganizationalUnitDto> getAuthorizedOrganizationalUnit(String orgUnitId) {
    String url = this.adminUrl + "/" + ResourceName.AUTHORIZED_ORG_UNIT + "/" + orgUnitId;
    return Optional.of(this.restClientService.getResource(url, AuthorizedOrganizationalUnitDto.class));
  }

  public OrganizationalUnit getOrgUnit(String resId) {
    this.restClientService.checkResId(resId);
    String url = this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + resId;
    return this.restClientService.getResource(url, OrganizationalUnit.class);
  }

  public OrganizationalUnit updateOrgUnit(String orgUnitId, OrganizationalUnit orgUnit) {
    String url = this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + orgUnitId;
    return this.restClientService.patchResource(url, orgUnit, OrganizationalUnit.class);
  }

  public void downloadOrgUnitLogo(String resId, Path path) {
    String url = this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + resId + "/" + DLCMActionName.DOWNLOAD_LOGO;
    this.restClientService.downloadContent(url, path, TokenUsage.WITH_TOKEN);
  }

  public List<OrganizationalUnit> getAuthorizedOrganizationalUnit(Principal principal) {

    List<OrganizationalUnit> cachedAuthorizedOrganizationalUnits = null;
    String cacheKey = null;

    try {
      cacheKey = principal.getName();
      cachedAuthorizedOrganizationalUnits = this.cacheService.getCachedAuthorizedOrganizationalUnits(cacheKey);
    } catch (final SolidifyRuntimeException e) {
      log.debug("Error getting authorized organizational unit", e);
    }

    if (cachedAuthorizedOrganizationalUnits != null && !cachedAuthorizedOrganizationalUnits.isEmpty()) {
      return cachedAuthorizedOrganizationalUnits;
    } else {

      final String url = this.adminUrl + "/" + ResourceName.AUTHORIZED_ORG_UNIT;

      try {
        final List<OrganizationalUnit> units = new ArrayList<>();
        Pageable page = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
        RestCollection<OrganizationalUnit> organizationalUnits;
        // List all orgUnits
        do {
          organizationalUnits = this.restClientService.getResourceList(url, OrganizationalUnit.class, page);
          page = page.next();
          units.addAll(organizationalUnits.getData());
        } while (organizationalUnits.getPage().hasNext());

        this.cacheService.storeAuthorizedOrganizationalUnits(cacheKey, units);

        return units;

      } catch (final SolidifyRuntimeException e) {
        log.error("Error getting resource class", e);
      }

      return Collections.emptyList();
    }
  }

  public List<SubjectArea> getSubjectAreas(String orgUnitId) {
    return this.restClientService.getAllResources(this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + orgUnitId + "/" +
            ResourceName.SUBJECT_AREA, SubjectArea.class);
  }

  public List<Institution> getInstitutions(String orgUnitId) {
    return this.restClientService.getAllResources(this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + orgUnitId + "/" +
            ResourceName.INSTITUTION, Institution.class);
  }

  public List<FundingAgency> getFundingAgencies(String orgUnitId) {
    return this.restClientService.getAllResources(this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + orgUnitId + "/" +
            ResourceName.FUNDING_AGENCY, FundingAgency.class);
  }

  public List<SubmissionPolicy> getSubmissionPolicies(String orgUnitId) {
    return this.restClientService.getAllResources(this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + orgUnitId + "/" +
            ResourceName.SUB_POLICY, SubmissionPolicy.class);
  }

  public List<PreservationPolicy> getPreservationPolicies(String orgUnitId) {
    return this.restClientService.getAllResources(this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + orgUnitId + "/" +
            ResourceName.PRES_POLICY, PreservationPolicy.class);
  }

  public List<OrgUnitPersonRoleListDTO> getMembers(String orgUnitId) {
    return this.restClientService.getAllResources(this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + orgUnitId + "/" + ResourceName.PERSON,
            OrgUnitPersonRoleListDTO.class);
  }

  @Override
  protected Class<OrganizationalUnit> getResourceClass() {
    return OrganizationalUnit.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.ORG_UNIT;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DisseminationPolicyRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.display.OrganizationalUnitDisseminationPolicyDTO;
import ch.dlcm.rest.ResourceName;

public abstract class OrganizationalDisseminationPolicyRemoteResourceService {

  private static final Object SIZE_PARAM = "?" + ActionName.SIZE + "=";
  private final String adminUrl;

  protected final DLCMProperties config;
  protected final SolidifyRestClientService restClientService;

  protected OrganizationalDisseminationPolicyRemoteResourceService(DLCMProperties config, SolidifyRestClientService restClientService) {
    this.config = config;
    this.restClientService = restClientService;
    this.adminUrl = this.config.getModule().getAdmin().getUrl();
  }

  public OrganizationalUnitDisseminationPolicyDTO getOrganizationalUnitDisseminationPolicy(String orgUnitId, String disseminatonPolicyId,
          String compositeKey) {
    String url = this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + orgUnitId + "/" + ResourceName.DIS_POLICY + "/" + disseminatonPolicyId
            + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE + "&" + SolidifyConstants.JOIN_RESOURCE_ID_PARAM + "=" + compositeKey;
    return this.restClientService.getResource(url, OrganizationalUnitDisseminationPolicyDTO.class);
  }

}

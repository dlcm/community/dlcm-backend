/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - AIPDownloadRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.rest.ResourceName;

public abstract class AIPDownloadRemoteResourceService extends RemoteResourceService<ArchivalInfoPackage> {

  private final String accessUrl;

  protected AIPDownloadRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.accessUrl = dlcmProperties.getModule().getAccess().getUrl();
  }

  public RestCollection<ArchivalInfoPackage> getAipsByOrder(String orderId, Pageable pageable) {
    String jsonString = this.restClientService.getResource(this.accessUrl + "/" + ResourceName.ORDER + "/" + orderId + "/"
            + ResourceName.AIP, pageable);
    return new RestCollection<>(jsonString, ArchivalInfoPackage.class);
  }

  @Override
  protected Class<ArchivalInfoPackage> getResourceClass() {
    return ArchivalInfoPackage.class;
  }

  public void deleteAIPDownload(String aipId) {
    String url = this.accessUrl + "/" + ResourceName.AIP + "/" + aipId;
    this.restClientService.deleteResource(url);
  }

  public int getCountAIPDownloadOfCompletedOrders() {
    return this.restClientService.getResource(this.accessUrl + "/" + ResourceName.AIP + "/" + ResourceName.TOTAL_COUNT_FOR_COMPLETED_ORDER,
            Integer.class);
  }

}

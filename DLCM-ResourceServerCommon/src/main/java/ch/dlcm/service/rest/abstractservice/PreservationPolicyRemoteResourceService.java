/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - PreservationPolicyRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.rest.ResourceName;

public abstract class PreservationPolicyRemoteResourceService extends RemoteResourceService<PreservationPolicy> {

  private final String adminUrl;

  protected PreservationPolicyRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  @Override
  protected Class<PreservationPolicy> getResourceClass() {
    return PreservationPolicy.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.PRES_POLICY;
  }

  public List<PreservationPolicy> getPreservationPoliciesByRetention(String retention) {
    final Pageable page = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    String url = this.adminUrl + "/" + ResourceName.PRES_POLICY;
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.set("retention", retention);
    uriBuilder.queryParams(params);
    url = uriBuilder.toUriString();
    // Get PreservationPolicy RestCollection
    RestCollection<PreservationPolicy> policiesCollection = this.restClientService.getResourceList(url, PreservationPolicy.class, page);
    // Return preservationPolicies list
    return policiesCollection.getData();
  }
}

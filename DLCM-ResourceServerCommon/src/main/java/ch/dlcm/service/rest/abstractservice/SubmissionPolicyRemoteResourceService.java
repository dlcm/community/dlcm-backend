/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - SubmissionPolicyRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.rest.ResourceName;

public abstract class SubmissionPolicyRemoteResourceService extends RemoteResourceService<SubmissionPolicy> {

  private final String adminUrl;

  protected SubmissionPolicyRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  public SubmissionPolicy getSubmissionPolicy(String resId) {
    this.restClientService.checkResId(resId);
    String url = this.adminUrl + "/" + ResourceName.SUB_POLICY + "/" + resId;
    return this.restClientService.getResource(url, SubmissionPolicy.class);
  }

  @Override
  protected Class<SubmissionPolicy> getResourceClass() {
    return SubmissionPolicy.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.SUB_POLICY;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchiveACLRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.util.List;

import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.CollectionTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

public abstract class ArchiveACLRemoteResourceService extends RemoteResourceService<ArchiveACL> {

  private final String adminUrl;

  protected ArchiveACLRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  public List<ArchiveACL> findMyAcl() {
    String url = adminUrl + "/" + ResourceName.ARCHIVE_ACL + "/" + DLCMActionName.GET_MY_ACLS;
    // Get List
    final String jsonResponse = this.restClientService.getResource(url);
    return CollectionTool.getList(jsonResponse, ArchiveACL.class);
  }

  @Override
  protected Class<ArchiveACL> getResourceClass() {
    return ArchiveACL.class;
  }

  @Override
  protected String getResourceUrl() {
    return adminUrl + "/" + ResourceName.ARCHIVE_ACL;
  }

}

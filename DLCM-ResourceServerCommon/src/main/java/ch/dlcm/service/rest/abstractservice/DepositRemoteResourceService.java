/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DepositRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

public abstract class DepositRemoteResourceService extends RemoteResourceService<Deposit> {
  private static final Logger log = LoggerFactory.getLogger(DepositRemoteResourceService.class);

  private final String preingestUrl;

  protected DepositRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.preingestUrl = dlcmProperties.getModule().getPreingest().getUrl();
  }

  public RestCollection<Deposit> getDepositList(Pageable pageable) {
    String jsonString = this.restClientService.getResource(this.preingestUrl + "/" + ResourceName.DEPOSIT, pageable);
    return new RestCollection<>(jsonString, Deposit.class);
  }

  public RestCollection<Deposit> getDepositListWithParameters(String queryParameters, Pageable pageable) {
    if (StringTool.isNullOrEmpty(queryParameters)) {
      throw new SolidifyCheckingException("The parameter list is missing");
    }
    String jsonString = this.restClientService.getResource(this.preingestUrl + "/" + ResourceName.DEPOSIT + "?" + queryParameters, pageable);
    return new RestCollection<>(jsonString, Deposit.class);
  }

  public RestCollection<DepositDataFile> getDepositDataFileList(String depositId, Pageable pageable) {
    String jsonString = this.restClientService.getResource(this.preingestUrl + "/" + ResourceName.DEPOSIT + "/" + depositId + "/" +
            ResourceName.DATAFILE, pageable);
    return new RestCollection<>(jsonString, DepositDataFile.class);
  }

  public DepositDataFile getDepositDataFile(String depositId, String depositDataFileId) {
    this.restClientService.checkResId(depositId);
    String url = this.preingestUrl + "/" + ResourceName.DEPOSIT + "/" + depositId + "/" + ResourceName.DATAFILE + "/" + depositDataFileId;
    return this.restClientService.getResource(url, DepositDataFile.class);
  }

  public Deposit getDeposit(String resId) {
    this.restClientService.checkResId(resId);
    String url = this.preingestUrl + "/" + ResourceName.DEPOSIT + "/" + resId;
    return this.restClientService.getResource(url, Deposit.class);
  }

  public void cleanDepositDatafile(Deposit deposit) {
    String url = this.preingestUrl + "/" + ResourceName.DEPOSIT + "/" + deposit.getResId() + "/" + DLCMActionName.CLEAN;
    this.restClientService.postResourceAction(url);
  }

  public Result checkComplianceLevel(Deposit deposit) {
    String url = this.preingestUrl + "/" + ResourceName.DEPOSIT + "/" + deposit.getResId() + "/" + DLCMActionName.CHECK_COMPLIANCE;
    return this.restClientService.postResourceAction(url);
  }

  public Result upgradeMetadata(Deposit deposit) {
    String url = this.preingestUrl + "/" + ResourceName.DEPOSIT + "/" + deposit.getResId() + "/" + DLCMActionName.START_METADATA_UPGRADE;
    return this.restClientService.postResourceAction(url);
  }

  public HttpStatusCode checkDepositWorkingSip(String depositId) {
    // Check deposit SIP status
    String url = this.preingestUrl + "/" + ResourceName.DEPOSIT + "/" + depositId + "/" + ResourceName.SIP_FILE;
    return this.restClientService.headResource(url);
  }

  public boolean deleteWorkingSip(String depositId) {
    String url = this.preingestUrl + "/" + ResourceName.DEPOSIT + "/" + depositId + "/" + ResourceName.SIP_FILE;
    try {
      this.restClientService.deleteResource(url);
    } catch (HttpClientErrorException e) {
      log.error("Error in deleting working SIP of deposit " + depositId, e);
      return false;
    }
    return true;
  }

  @Override
  protected String getResourceUrl() {
    return this.preingestUrl + "/" + ResourceName.DEPOSIT;
  }

  @Override
  protected Class<Deposit> getResourceClass() {
    return Deposit.class;
  }
}

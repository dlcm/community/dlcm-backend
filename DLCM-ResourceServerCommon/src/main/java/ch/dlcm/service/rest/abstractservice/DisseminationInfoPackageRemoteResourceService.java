/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DisseminationInfoPackageRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

public abstract class DisseminationInfoPackageRemoteResourceService extends RemoteResourceService<DisseminationInfoPackage> {
  private static final Logger log = LoggerFactory.getLogger(DisseminationInfoPackageRemoteResourceService.class);

  private final String accessUrl;

  protected DisseminationInfoPackageRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.accessUrl = dlcmProperties.getModule().getAccess().getUrl();
  }

  @Override
  protected Class<DisseminationInfoPackage> getResourceClass() {
    return DisseminationInfoPackage.class;
  }

  public RestCollection<DisseminationInfoPackage> getDipByAip(String aipId, Pageable pageable) {
    final String url = this.accessUrl + "/" + ResourceName.DIP + "/" + DLCMActionName.GET_BY_AIP_ID + "/" + aipId;
    final String jsonString = this.restClientService.getResource(url, pageable);
    return new RestCollection<>(jsonString, DisseminationInfoPackage.class);
  }

  public boolean deleteDIP(String dipId) {
    String url = this.accessUrl + "/" + ResourceName.DIP + "/" + dipId;
    try {
      this.restClientService.deleteResource(url);
    } catch (HttpClientErrorException e) {
      log.error("Error in deleting DIP : " + dipId, e);
      return false;
    }
    return true;
  }

  public RestCollection<DisseminationInfoPackage> getDIPByOrder(String orderId, Pageable pageable) {
    final String url = this.accessUrl + "/" + ResourceName.ORDER + "/" + orderId + "/" + ResourceName.DIP;
    final String jsonString = this.restClientService.getResource(url, pageable);
    return new RestCollection<>(jsonString, DisseminationInfoPackage.class);
  }

  public int getCountDIPOfCompletedOrders() {
    return this.restClientService.getResource(this.accessUrl + "/" + ResourceName.DIP + "/" + ResourceName.TOTAL_COUNT_FOR_COMPLETED_ORDER,
            Integer.class);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchiveRatingRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.settings.ArchiveUserRating;
import ch.dlcm.model.settings.RatingType;
import ch.dlcm.rest.ResourceName;

public abstract class ArchiveRatingRemoteResourceService {

  protected final SolidifyRestClientService restClientService;
  private final String adminModuleUrl;

  protected ArchiveRatingRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    this.adminModuleUrl = dlcmProperties.getModule().getAdmin().getUrl();
    this.restClientService = restClientService;
  }

  public ArchiveUserRating createArchiveUserRating(ArchiveUserRating archiveUserRating) {
    String url = this.adminModuleUrl + "/" + ResourceName.ARCHIVE_RATING;
    return this.restClientService.postResource(url, archiveUserRating, ArchiveUserRating.class);
  }

  public ArchiveUserRating updateArchiveUserRating(String archiveId, ArchiveUserRating archiveUserRating) {
    String url = this.adminModuleUrl + "/" + ResourceName.ARCHIVE_RATING + "/" + archiveId;
    return this.restClientService.patchResource(url, archiveUserRating, ArchiveUserRating.class);
  }

  public RestCollection<ArchiveUserRating> getArchiveUserRating(String archiveId, Pageable pageable) {
    String url = this.adminModuleUrl + "/" + ResourceName.ARCHIVE_RATING + "?" + DLCMConstants.DB_ARCHIVE_ID + "=" + archiveId;
    String jsonString = this.restClientService.getResource(url, pageable);
    return new RestCollection<>(jsonString, ArchiveUserRating.class);
  }

  public RestCollection<ArchiveUserRating> getArchiveByUserAndRating(String archiveId, String userId, String ratingId) {
    String url = this.adminModuleUrl + "/" + ResourceName.ARCHIVE_RATING + "?" + DLCMConstants.DB_ARCHIVE_ID + "=" + archiveId
            + "&user.resId=" + userId + "&ratingType.resId=" + ratingId;
    String jsonString = this.restClientService.getResource(url);
    return new RestCollection<>(jsonString, ArchiveUserRating.class);
  }

  public RestCollection<ArchiveUserRating> getAllRatingByUser(String resId, String userId) {
    String url =
            this.adminModuleUrl + "/" + ResourceName.ARCHIVE_RATING + "?" + DLCMConstants.DB_ARCHIVE_ID + "=" + resId + "&user.resId=" + userId;

    String jsonString = this.restClientService.getResource(url);
    return new RestCollection<>(jsonString, ArchiveUserRating.class);
  }

  public void deleteArchiveUserRating(String resId) {
    String url = this.adminModuleUrl + "/" + ResourceName.ARCHIVE_RATING + "/" + resId;
    this.restClientService.deleteResource(url);
  }

  public RatingType getRatingType(String ratingTypeId) {
    String url = this.adminModuleUrl + "/" + ResourceName.RATING_TYPE + "/" + ratingTypeId;
    return this.restClientService.getResource(url, RatingType.class);
  }

  public RestCollection<RatingType> getAllRatingType() {
    String url = this.adminModuleUrl + "/" + ResourceName.RATING_TYPE;
    String jsonString = this.restClientService.getResource(url);
    return new RestCollection<>(jsonString, RatingType.class);
  }

}

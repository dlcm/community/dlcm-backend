/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - NotificationRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.util.ArrayList;
import java.util.List;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.dto.NotificationContributorDto;
import ch.dlcm.model.dto.NotificationsContributorDto;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

public abstract class NotificationRemoteResourceService extends RemoteResourceService<Notification> {

  private final String adminUrl;
  private final UserRemoteResourceService userRemoteResourceService;

  protected NotificationRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService,
          UserRemoteResourceService userRemoteResourceService) {
    super(restClientService);
    this.adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
    this.userRemoteResourceService = userRemoteResourceService;
  }

  public void createNotification(Notification notification) {
    // Build URL
    String url = this.adminUrl + "/" + ResourceName.NOTIFICATION;
    // Create object
    restClientService.postResource(url, notification, Notification.class);
  }

  private void createNotificationForContributors(NotificationsContributorDto notificationsContributorsDto) {
    // Build URL
    String url = this.adminUrl + "/" + ResourceName.NOTIFICATION + "/" + DLCMActionName.BY_CONTRIBUTORS;
    // Create object
    restClientService.postResourceOther(url, notificationsContributorsDto, String.class);
  }

  public RestCollection<Notification> findByObjectIdAndNotificationType(String objectId, String notificationTypeId) {
    java.lang.String url = this.adminUrl + "/" + ResourceName.NOTIFICATION + "?objectId=" + objectId + "&notificationType.resId=" + notificationTypeId;
    java.lang.String jsonString = this.restClientService.getResource(url);
    return new RestCollection<>(jsonString, Notification.class);  }

  public void updateNotification(Notification notification) {
    // Build URL
    java.lang.String url = this.adminUrl + "/" + ResourceName.NOTIFICATION + "/" + notification.getResId();
    this.restClientService.patchResource(url, notification, Notification.class);
  }

  public void deleteNotificationsOnDeposit(java.lang.String depositId) {
    final java.lang.String url = this.adminUrl + "/" + ResourceName.NOTIFICATION + "/" + DLCMActionName.DELETE_NOTIFICATIONS_ON_DEPOSIT + "/" + depositId;
    restClientService.deleteResource(url);
  }

  @Override
  protected Class<Notification> getResourceClass() {
    return Notification.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.NOTIFICATION;
  }

  public void createNotification(User emitter, String message, NotificationType notificationType,
          OrganizationalUnit notifiedOrgUnit, String objectID) {
    Notification notification = new Notification();
    notification.setEmitter(emitter);
    notification.setMessage(message);
    notification.setObjectId(objectID);
    notification.setNotificationType(notificationType);
    notification.setNotifiedOrgUnit(notifiedOrgUnit);
    notification.setNotificationStatus(NotificationStatus.PENDING);

    this.createNotification(notification);
  }

  public void createNotification(User emitter, String message, NotificationType notificationType,
          OrganizationalUnit notifiedOrgUnit, String objectID, Person recipient) {
    Notification notification = new Notification();
    notification.setEmitter(emitter);
    notification.setMessage(message);
    notification.setObjectId(objectID);
    notification.setNotificationType(notificationType);
    notification.setNotifiedOrgUnit(notifiedOrgUnit);
    notification.setNotificationStatus(NotificationStatus.PENDING);
    notification.setRecipient(recipient);

    this.createNotification(notification);
  }

  public void createDepositCompletedNotifications(Deposit deposit, User emitter, String message) {
    this.createdDepositCompletedNotificationForCreators(deposit, emitter, message);
    this.createdDepositCompletedNotificationForContributors(deposit, emitter, message);
    this.createDepositCompletedNotificationsForStewards(deposit, emitter, message);
  }

  private void createDepositCompletedNotificationsForStewards(Deposit deposit, User emitter, String message) {
    this.createNotification(emitter, message, NotificationType.CREATED_DEPOSIT_INFO,
            deposit.getOrganizationalUnit(), deposit.getResId());
  }

  private void createdDepositCompletedNotificationForContributors(Deposit deposit, User emitter, String message) {
    List<NotificationContributorDto> notificationsContributorDto = new ArrayList<>();

    for (String contributor : deposit.getContributorIds()) {
      Notification notification = new Notification();
      notification.setEmitter(emitter);
      notification.setMessage(message);
      notification.setObjectId(deposit.getResId());
      notification.setNotificationType(NotificationType.MY_INDIRECT_COMPLETED_DEPOSIT_INFO);
      notification.setNotifiedOrgUnit(deposit.getOrganizationalUnit());
      notification.setNotificationStatus(NotificationStatus.PENDING);

      NotificationContributorDto notificationContributorDto = new NotificationContributorDto(notification, contributor);

      notificationsContributorDto.add(notificationContributorDto);
    }

    NotificationsContributorDto notificationsContributorsDto = new NotificationsContributorDto(notificationsContributorDto);
    this.createNotificationForContributors(notificationsContributorsDto);
  }

  private void createdDepositCompletedNotificationForCreators(Deposit deposit, User emitter, String message) {
    Person creator = this.userRemoteResourceService.findByExternalUid(deposit.getCreatedBy()).getPerson();
    this.createNotification(emitter, message, NotificationType.MY_COMPLETED_DEPOSIT_INFO, deposit.getOrganizationalUnit(), deposit.getResId(),
            creator);
  }

  public void createdDepositApprovedNotificationForCreators(Deposit deposit, User emitter, String message) {
    //find the creator of the deposit
    Person creator = this.userRemoteResourceService.findByExternalUid(deposit.getCreatedBy()).getPerson();
    this.createNotification(emitter, message, NotificationType.MY_APPROVED_DEPOSIT_INFO, deposit.getOrganizationalUnit(), deposit.getResId(),
            creator);
  }

  public void createArchiveCompletedNotifications(ArchivalInfoPackage aip, User emitter, String message, OrganizationalUnit organizationalUnit) {
    this.createdArchiveCompletedNotificationForCreators(aip, emitter, message, organizationalUnit);
    this.createArchiveCompletedNotificationsForApprovers(aip, emitter, message, organizationalUnit);
  }

  private void createArchiveCompletedNotificationsForApprovers(ArchivalInfoPackage aip, User emitter, String message,
          OrganizationalUnit organizationalUnit) {
    this.createNotification(emitter, message, NotificationType.COMPLETED_ARCHIVE_INFO,
            organizationalUnit, aip.getResId());
  }

  private void createdArchiveCompletedNotificationForCreators(ArchivalInfoPackage aip, User emitter, String message,
          OrganizationalUnit organizationalUnit) {
    Person creator = this.userRemoteResourceService.findByExternalUid(aip.getCreatedBy()).getPerson();
    this.createNotification(emitter, message, NotificationType.MY_COMPLETED_ARCHIVE_INFO, organizationalUnit, aip.getResId(),
            creator);
  }
}

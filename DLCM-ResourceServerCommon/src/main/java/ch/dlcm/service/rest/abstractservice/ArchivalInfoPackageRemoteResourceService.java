/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchivalInfoPackageRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.net.URI;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.AipStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.oais.RepresentationInfo;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

public abstract class ArchivalInfoPackageRemoteResourceService extends RemoteResourceService<ArchivalInfoPackage> {
  private static final Logger log = LoggerFactory.getLogger(ArchivalInfoPackageRemoteResourceService.class);

  private final MessageService messageService;

  private final String ingestUrl;
  private final String[] archivalStorageList;

  protected ArchivalInfoPackageRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService,
          MessageService messageService) {
    super(restClientService);
    this.messageService = messageService;
    this.ingestUrl = dlcmProperties.getModule().getIngest().getUrl();
    this.archivalStorageList = dlcmProperties.getModule().getArchivalStorage().getUrls();
  }

  protected String getDefaultArchivalStorage() {
    return this.archivalStorageList[0];
  }

  public String sendAIP(SubmissionInfoPackage sip, ArchiveContainer aipContainer, String aipId) {
    // Create AIP object
    ArchivalInfoPackage aip = new ArchivalInfoPackage();
    aip.setResId(aipId);

    RepresentationInfo info = new RepresentationInfo(sip.getInfo());
    info.setStatus(PackageStatus.IN_PREPARATION);
    info.setName(this.messageService.get("ingest.aip.name", new Object[] { sip.getInfo().getName() }));
    info.setDescription(StringTool.truncateOnSpaceWithEllipsis(
            this.messageService.get("ingest.aip.description", new Object[] { sip.getInfo().getDescription() }),
            SolidifyConstants.DB_LONG_STRING_LENGTH));
    aip.setPublicationDate(sip.getPublicationDate());
    aip.setInfo(info);
    aip.setArchiveContainer(aipContainer);
    aip.addSip(sip.getResId());
    aip.setCreatedBy(sip.getCreatedBy());
    aip.setUpdatedBy(sip.getUpdatedBy());
    aip.setDispositionApproval(sip.getDispositionApproval());
    aip.setRetention(sip.getRetention());
    aip.setLastArchiving(OffsetDateTime.now());

    if (sip.isCollection()) {
      aip.setArchivalUnit(false);
    }

    // Link for downloading AIP package
    String downloadUrl = this.ingestUrl + "/" + ResourceName.SIP + "/" + sip.getResId() + "/" + ResourceName.AIP_FILE;
    // Create AIP
    return this.createAIP(aip, downloadUrl, sip.getCollection(), this.archivalStorageList[sip.getMainStorage()]);
  }

  public String createAIP(ArchivalInfoPackage aip, String sourceUrl, List<String> collection, String targetUrl) {
    try {
      // Send AIP object
      ArchivalInfoPackage createdAip = this.restClientService.postResource(targetUrl + "/" + ResourceName.AIP, aip,
              ArchivalInfoPackage.class);

      log.info("AIP '{}' created on {}/{}", createdAip.getResId(), targetUrl, ResourceName.AIP);

      // Send AIP collection
      if (aip.isCollection()) {
        this.restClientService.postResourceOther(targetUrl + "/" + ResourceName.AIP + "/" + createdAip.getResId() + "/" + ResourceName.AIP,
                collection, String.class);
        log.info("AIP collection '{}' created on {}/{}/{}/{}", aip.getResId(), targetUrl, ResourceName.AIP, createdAip.getResId(),
                ResourceName.AIP);
      }

      // Send AIP package
      AipDataFile aipDataFile = new AipDataFile(createdAip);
      aipDataFile.setCreatedBy(aip.getCreatedBy());
      aipDataFile.setUpdatedBy(aip.getUpdatedBy());
      aipDataFile.setSourceData(URI.create(sourceUrl));
      aipDataFile.setDataCategory(DataCategory.Package);
      aipDataFile.setDataType(DataCategory.InformationPackage);

      this.restClientService.postResource(
              targetUrl + "/" + ResourceName.AIP + "/"
                      + createdAip.getResId() + "/" + ResourceName.DATAFILE,
              aipDataFile, AipDataFile.class);

      log.info("AipDataFile '{}' created on {}/{}/{}/{}", createdAip.getResId(), targetUrl, ResourceName.AIP, createdAip.getResId(),
              ResourceName.DATAFILE);

      return createdAip.getResId();
    } catch (RuntimeException e) {

      String message = e.getMessage();

      // for nested exception messages
      String msgSubStr = "message\":\"";
      if (message.contains(msgSubStr)) {
        message = StringTool.substringBetween(message, msgSubStr, ",");
      }

      throw new SolidifyProcessingException(message);
    }
  }

  public String replicateAIP(ArchivalInfoPackage aip, String sourceUrl, String targetUrl, String action) {
    try {
      // Check action
      if (!action.equals(DLCMActionName.REPLICATE_TOMBSTONE) && !action.equals(DLCMActionName.REPLICATE_PACKAGE)) {
        throw new SolidifyProcessingException("Wrong AIP action: " + action);
      }
      // Start replicating tombstone/new package
      String lastArchiving = aip.getLastArchiving().format(DateTimeFormatter.ISO_DATE_TIME);
      Result result = this.restClientService.postResourceAction(
              targetUrl
                      + "/" + ResourceName.AIP
                      + "/" + aip.getResId()
                      + "/" + action
                      + "?lastArchiving=" + lastArchiving);
      if (result.getStatus().equals(Result.ActionStatus.EXECUTED)) {
        // Update the AIP metadata
        final Map<String, Object> updateMap = Map.of(
                "info", aip.getInfo().toUpdateMap(),
                "retention", aip.getRetention());
        this.restClientService.patchResource(
                targetUrl
                        + "/" + ResourceName.AIP
                        + "/" + aip.getResId(),
                updateMap,
                ArchivalInfoPackage.class);
        // Send AIP tombstone/new package
        AipDataFile aipDataFile = new AipDataFile(aip);
        aipDataFile.setCreatedBy(aip.getCreatedBy());
        aipDataFile.setUpdatedBy(aip.getUpdatedBy());
        aipDataFile.setSourceData(URI.create(sourceUrl));
        aipDataFile.setDataCategory(DataCategory.Package);
        aipDataFile.setDataType(DataCategory.InformationPackage);

        AipDataFile resPackage = this.restClientService.postResource(
                targetUrl
                        + "/" + ResourceName.AIP
                        + "/" + aip.getResId()
                        + "/" + ResourceName.DATAFILE,
                aipDataFile,
                AipDataFile.class);
        if (resPackage.getResId() != null) {
          log.info("AipDataFile '{}' created on {}/{}/{}/{}", aip.getResId(), targetUrl, ResourceName.AIP, aip.getResId(),
                  ResourceName.DATAFILE);
        }
        return aip.getResId();
      } else {
        throw new SolidifyProcessingException("Cannot execute 'replace-tombstone' action");
      }
    } catch (RuntimeException e) {
      String message = e.getMessage();

      // for nested exception messages
      String msgSubStr = "message\":\"";
      if (message.contains(msgSubStr)) {
        message = StringTool.substringBetween(message, msgSubStr, ",");
      }
      throw new SolidifyProcessingException(message);
    }
  }

  public String createAIP(StoredAIP storedAip) {
    // Create AIP
    ArchivalInfoPackage aip = storedAip.getAIP();
    aip.getInfo().setName(
            this.messageService.get("pres-planning.aip.name", new Object[] { storedAip.getResId() }));
    aip.getInfo().setDescription(this.messageService.get("pres-planning.aip.description"));

    try {
      // Send AIP object
      ArchivalInfoPackage resAip = this.restClientService.postResource(this.getDefaultArchivalStorage() + "/" + ResourceName.AIP, aip,
              ArchivalInfoPackage.class);
      if (resAip != null && resAip.getResId() != null) {
        return resAip.getResId();
      } else {
        throw new SolidifyProcessingException(this.messageService.get("archival.aip.error.creation", new Object[] { storedAip.getAipId() }));
      }
    } catch (RuntimeException e) {
      throw new SolidifyProcessingException(this.messageService.get("archival.aip.error.reloading", new Object[] { storedAip.getAipId() }));
    }
  }

  @Override
  public ArchivalInfoPackage findOne(String aipId) {
    return this.findOne(aipId, null);
  }

  public ArchivalInfoPackage findOne(String aipId, Integer preferredStorage) {
    return this.getAIPOnStorage(aipId, preferredStorage);
  }

  public ArchivalInfoPackage getAIPOnDefaultStorage(String aipId) {
    return this.getAIP(this.getDefaultArchivalStorage(), aipId);
  }

  public ArchivalInfoPackage getAIPOnStorage(String aipId) {
    return this.getAIPOnStorage(aipId, null);
  }

  public ArchivalInfoPackage getAIPOnStorage(String aipId, Integer preferredStorage) {
    final String storage = this.findStorageForAIP(aipId, preferredStorage);
    return this.getAIP(storage, aipId);
  }

  public String findStorageForAIP(String aipId) {
    return this.findStorageForAIP(aipId, null);
  }

  public String findStorageForAIP(String aipId, Integer preferredStorage) {
    for (String storage : this.getArchivalStorageListByPreference(preferredStorage)) {
      if (this.isAipPresent(storage, aipId)) {
        return storage;
      }
    }
    return this.getDefaultArchivalStorage();
  }

  public String[] getArchivalStorageListByPreference(Integer preferredStorageIndex) {
    if (preferredStorageIndex == null) {
      return this.archivalStorageList;
    }
    List<String> list = new ArrayList<>(Arrays.asList(this.archivalStorageList));
    String preferredStorage = list.remove(preferredStorageIndex.intValue());
    list.add(0, preferredStorage);
    return list.toArray(new String[0]);
  }

  public ArchivalInfoPackage getAIP(String storageUrl, String aipId) {
    this.restClientService.checkResId(aipId);
    return this.restClientService.getResource(this.getArchivalStorageUrlWithId(storageUrl, aipId), ArchivalInfoPackage.class);
  }

  public List<ArchivalInfoPackage> getAIPCollectionList(String archivalStorage, String id) {
    String url = archivalStorage + "/" + ResourceName.AIP + "/" + id + "/" + ResourceName.AIP;
    Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    List<ArchivalInfoPackage> aipList = new ArrayList<>();
    RestCollection<ArchivalInfoPackage> collection;
    do {
      // Get AIPs
      collection = this.restClientService.getResourceList(url, ArchivalInfoPackage.class, pageable);
      // Next Page
      pageable = pageable.next();
      // Save AIPs
      aipList.addAll(collection.getData());
    } while (collection.getPage().hasNext());
    return aipList;
  }

  public Result reindexAIP(String storageUrl, String aipId) {
    String url = this.getArchivalStorageUrlWithId(storageUrl, aipId) + "/" + DLCMActionName.REINDEX;
    return this.restClientService.postResourceAction(url);
  }

  public Result checkAIP(String storageUrl, String aipId) {
    String url = this.getArchivalStorageUrlWithId(storageUrl, aipId) + "/" + DLCMActionName.CHECK;
    return this.restClientService.postResourceAction(url);
  }

  public Result checkAIPFixity(String storageUrl, String aipId) {
    String url = this.getArchivalStorageUrlWithId(storageUrl, aipId) + "/" + DLCMActionName.CHECK_FIXITY;
    return this.restClientService.postResourceAction(url);
  }

  public Result upgradeMetadata(String storageUrl, String aipId) {
    String url = this.getArchivalStorageUrlWithId(storageUrl, aipId) + "/" + DLCMActionName.START_METADATA_UPGRADE;
    return this.restClientService.postResourceAction(url);
  }

  public Result updateComplianceLevel(String storageUrl, String aipId) {
    String url = this.getArchivalStorageUrlWithId(storageUrl, aipId) + "/" + DLCMActionName.START_COMPLIANCE_LEVEL_UPDATE;
    return this.restClientService.postResourceAction(url);
  }

  public RestCollection<ArchivalInfoPackage> getAipList(String storageUrl, Pageable pageable) {
    String jsonString = this.restClientService.getResource(storageUrl + "/" + ResourceName.AIP, pageable);
    return new RestCollection<>(jsonString, ArchivalInfoPackage.class);
  }

  public RestCollection<ArchivalInfoPackage> getAipListWithParameters(String storageUrl, String queryParameters, Pageable pageable) {
    if (StringTool.isNullOrEmpty(queryParameters)) {
      throw new SolidifyCheckingException("The parameter list is missing");
    }
    String jsonString = this.restClientService.getResource(storageUrl + "/" + ResourceName.AIP + "?" + queryParameters, pageable);
    return new RestCollection<>(jsonString, ArchivalInfoPackage.class);
  }

  public RestCollection<AipDataFile> getAipDataFileList(String storageUrl, String aipId,
          Pageable pageable) {
    String jsonString = this.restClientService.getResource(this.getArchivalStorageUrlWithId(storageUrl, aipId) + "/" + ResourceName.DATAFILE,
            pageable);
    return new RestCollection<>(jsonString, AipDataFile.class);
  }

  public RestCollection<AipStatus> getAipStatusList(String storageUrl, Pageable pageable) {
    String jsonString = this.restClientService.getResource(storageUrl + "/" + ResourceName.AIP + "/" + DLCMActionName.LIST_AIP_WITH_STATUS,
            pageable);
    return new RestCollection<>(jsonString, AipStatus.class);
  }

  public void deleteAip(String aipId) {
    final String storageUrl = this.findStorageForAIP(aipId);
    this.restClientService.deleteResource(this.getArchivalStorageUrlWithId(storageUrl, aipId));
  }

  public RestCollection<StoredAIP> getStoredAipList(Pageable pageable) {
    String jsonString = this.restClientService.getResource(this.getDefaultArchivalStorage() + "/" + ResourceName.STORED_AIP, pageable);
    return new RestCollection<>(jsonString, StoredAIP.class);
  }

  public void sendAIPForUpdate(SubmissionInfoPackage sip, String updatedMetadataId) {
    final String aipId = sip.getAipId();
    final String storageUrl = this.archivalStorageList[sip.getMainStorage()];

    try {
      // Check AIP
      final String aipUrl = storageUrl + "/" + ResourceName.AIP;
      ArchivalInfoPackage aip = this.restClientService.getResource(aipUrl + "/" + aipId, ArchivalInfoPackage.class);
      // Check if ready for metadata edition
      if (!PackageStatus.isCompletedProcess(aip.getInfo().getStatus())) {
        throw new SolidifyRuntimeException("AIP in wrong status: " + aip.getInfo().getStatus());
      }
      if (aip.getInfo().getStatus() != PackageStatus.EDITING_METADATA) {
        // Put AIP in status EDITING METADATA
        final String aipMetadataEditingUrl = aipUrl + "/" + aipId + "/" + DLCMActionName.START_METADATA_EDITING;
        final Result changeStatusResult = this.restClientService.postResource(aipMetadataEditingUrl, Result.class);
        if (changeStatusResult.getStatus() != Result.ActionStatus.EXECUTED) {
          throw new SolidifyRuntimeException("AIP cannot be put in metadata editing");
        }
      }

      // Update the AIP metadata
      Map<String, Object> updateMap = new HashMap<>();
      updateMap.put("info", sip.getInfo().toUpdateMap());
      updateMap.put("publicationDate", sip.getPublicationDate());
      this.restClientService.patchResource(aipUrl + "/" + aipId, updateMap, ArchivalInfoPackage.class);

      // Update AIP AIP collection
      if (aip.isCollection()) {
        final String collectionAipUrl = storageUrl + "/" + ResourceName.AIP + "/" + aip.getResId() + "/" + ResourceName.AIP;
        final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
        final List<ArchivalInfoPackage> aipList = this.restClientService.getResourceList(collectionAipUrl, ArchivalInfoPackage.class, pageable)
                .getData();
        if (!aipList.isEmpty()) {
          final List<String> currentAipIdList = aipList.stream().map(ArchivalInfoPackage::getResId).toList();
          final List<String> newAipIdList = sip.getCollection();
          // Delete existing AIPs
          this.restClientService.deleteResourceList(collectionAipUrl, currentAipIdList);
          // Create AIPs from the ones defines in the deposit
          this.restClientService.postResourceOther(collectionAipUrl, newAipIdList, String.class);
        }
      }

      // Send updated metadata as a AIP data file
      final AipDataFile aipDataFile = new AipDataFile(aipId);
      final String sipDataFileUrl = this.ingestUrl + "/" + ResourceName.SIP + "/" + sip.getResId() + "/" + ResourceName.DATAFILE + "/" +
              updatedMetadataId + "/" + ActionName.DOWNLOAD;
      aipDataFile.setSourceData(URI.create(sipDataFileUrl));
      aipDataFile.setDataCategory(DataCategory.Package);
      aipDataFile.setDataType(DataCategory.UpdatePackage);
      aipDataFile.setCreatedBy(sip.getUpdatedBy());
      final String aipDataFileUrl = aipUrl + "/" + aipId + "/" + ResourceName.DATAFILE;
      this.restClientService.postResource(aipDataFileUrl, aipDataFile, AipDataFile.class);

    } catch (Exception e) {
      throw new SolidifyRuntimeException("Error in updating AIP of SIP (" + sip.getResId() + ")", e);
    }

  }

  @Override
  protected Class<ArchivalInfoPackage> getResourceClass() {
    return ArchivalInfoPackage.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.getDefaultArchivalStorage() + "/" + ResourceName.AIP;
  }

  protected String getArchivalStorageUrlWithId(String storageUrl, String aipId) {
    return storageUrl + "/" + ResourceName.AIP + "/" + aipId;
  }

  protected boolean isAipPresent(String storageUrl, String aipId) {
    try {
      return HttpStatus.OK.equals(this.restClientService.headResource(this.getArchivalStorageUrlWithId(storageUrl, aipId)));
    } catch (Exception e) {
      return false;
    }
  }
}

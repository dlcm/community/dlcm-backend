/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchiveStatisticsRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import org.springframework.http.HttpStatus;

import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.dto.ArchiveStatisticsDto;
import ch.dlcm.model.settings.ArchiveStatistics;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

public abstract class ArchiveStatisticsRemoteResourceService extends RemoteResourceService<ArchiveStatistics> {

  private final String adminUrl;

  protected ArchiveStatisticsRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  public boolean checkArchiveStatistics(String resId) {
    String url = this.adminUrl + "/" + ResourceName.ARCHIVE_STATISTICS + "/" + resId;
    return (this.restClientService.headResource(url) == HttpStatus.OK);
  }

  public boolean createArchiveStatistics(String resId) {
    // Create ArchiveStatistics object
    ArchiveStatistics archive = new ArchiveStatistics();
    archive.setResId(resId);

    // Send ArchiveStatistics object
    ArchiveStatistics resArchive = this.restClientService.postResource(this.adminUrl + "/" + ResourceName.ARCHIVE_STATISTICS, archive,
            ArchiveStatistics.class);
    return (resArchive.getResId().equals(resId));
  }

  public ArchiveStatisticsDto getArchiveStatistics(String resId) {
    String url = this.adminUrl + "/" + ResourceName.ARCHIVE_STATISTICS + "/" + resId + "/" + DLCMActionName.STATISTICS;
    return this.restClientService.getResource(url, ArchiveStatisticsDto.class);
  }

  public Result logArchiveStatistics(String resId, String action) {
    String url = this.adminUrl + "/" + ResourceName.ARCHIVE_STATISTICS + "/" + resId + "/" + action;
    return this.restClientService.postResourceAction(url);
  }

  @Override
  protected Class<ArchiveStatistics> getResourceClass() {
    return ArchiveStatistics.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.ARCHIVE_STATISTICS;
  }

}

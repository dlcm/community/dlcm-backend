/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - LicenseRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.settings.License;
import ch.dlcm.rest.ResourceName;

public abstract class LicenseRemoteResourceService extends RemoteResourceService<License> {

  private final String adminUrl;

  protected LicenseRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  @Override
  protected Class<License> getResourceClass() {
    return License.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.LICENSE;
  }

  public List<License> getLicensesByTitle(String title) {
    final Pageable page = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    String url = this.adminUrl + "/" + ResourceName.LICENSE;
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    if (!StringTool.isNullOrEmpty(title)) {
      params.set("title",  URLEncoder.encode(title, StandardCharsets.UTF_8));
      uriBuilder.queryParams(params);
    }
    url = uriBuilder.toUriString();
    // Get Licenses RestCollection
    RestCollection<License> licenseCollection = this.restClientService.getResourceList(url, License.class, page);
    // Return license list
    return licenseCollection.getData();
  }

  public License getByOpenLicenseId(String openLicenseId) {
    String url = this.adminUrl + "/" + ResourceName.LICENSE + "/" + openLicenseId;
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.set(DLCMConstants.IDENTIFIER_TYPE_PARAM, ResourceIdentifierType.SPDX_ID.name());
    uriBuilder.queryParams(params);
    url = uriBuilder.toUriString();
    return this.restClientService.getResource(url, License.class);
  }
}

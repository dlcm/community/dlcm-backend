/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchiveTypeRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.settings.ArchiveType;
import ch.dlcm.rest.ResourceName;

public abstract class ArchiveTypeRemoteResourceService extends RemoteResourceService<ArchiveType> {

  private final String adminUrl;

  protected ArchiveTypeRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  @Override
  protected Class<ArchiveType> getResourceClass() {
    return ArchiveType.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.ARCHIVE_TYPE;
  }

  public RestCollection<ArchiveType> get(String masterTypeId, String typeName) {
    final StringBuilder url = new StringBuilder(this.getResourceUrl());
    url.append("?");
    url.append(DLCMConstants.ARCHIVE_MASTER_TYPE_ID_PARAM + "=" + masterTypeId);
    url.append("&");
    url.append("typeName=" + typeName);
    String jsonString = this.restClientService.getResource(url.toString());
    return new RestCollection<>(jsonString, ArchiveType.class);
  }

  public RestCollection<ArchiveType> get(String masterTypeId) {
    final StringBuilder url = new StringBuilder(this.getResourceUrl());
    url.append("?");
    url.append(DLCMConstants.ARCHIVE_MASTER_TYPE_ID_PARAM + "=" + masterTypeId);
    String jsonString = this.restClientService.getResource(url.toString());
    return new RestCollection<>(jsonString, ArchiveType.class);
  }
}

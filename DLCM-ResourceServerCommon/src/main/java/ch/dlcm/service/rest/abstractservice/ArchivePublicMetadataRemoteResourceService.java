/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchivePublicMetadataRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.SearchConditionTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.IdentifierType;
import ch.dlcm.model.MetadataVisibility;
import ch.dlcm.model.index.ArchiveMetadata;

public abstract class ArchivePublicMetadataRemoteResourceService extends ArchiveMetadataRemoteResourceService {
  private static final Logger log = LoggerFactory.getLogger(ArchivePublicMetadataRemoteResourceService.class);

  protected ArchivePublicMetadataRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService dlcmRestClientService) {
    super(dlcmProperties, dlcmRestClientService, MetadataVisibility.PUBLIC);
  }

  public ArchiveMetadata getMetadataByDoi(String aipDoi) {
    String valueField = DLCMConstants.IDENTIFIER_FIELD;
    String identifierTypeField = DLCMConstants.IDENTIFIER_TYPE_FIELD;

    valueField = this.formatField(valueField);
    identifierTypeField = this.formatField(identifierTypeField);

    final String query = valueField + ":" + "\"" + aipDoi + "\"" + " AND " + identifierTypeField + ":DOI";

    final RestCollection<ArchiveMetadata> collection = this.getMetadataList(query, null, null, null, null, null, null);
    if (collection.getPage().getTotalItems() == 1) {
      return collection.getData().get(0);
    } else if (collection.getPage().getTotalItems() == 0) {
      throw new SolidifyResourceNotFoundException("Archive with DOI '" + aipDoi + "' not found");
    } else {
      throw new SolidifyRuntimeException("more than one archive correspond to the same DOI '" + aipDoi + "'");
    }
  }

  public ArchiveMetadata getByIdentifier(IdentifierType idType, String aipId) {

    String fieldName = null;
    switch (idType) {
      case DOI -> fieldName = "doi.keyword";
      case ARK -> fieldName = "ark.keyword";
      default -> throw new SolidifyRuntimeException("ID type known: " + idType);
    }

    SearchCondition searchCondition = SearchConditionTool.createTermSearchCondition(fieldName, aipId);
    final RestCollection<ArchiveMetadata> collection = this.search(List.of(searchCondition), Pageable.ofSize(2));

    if (collection.getPage().getTotalItems() == 1) {
      return collection.getData().get(0);
    } else if (collection.getPage().getTotalItems() == 0) {
      throw new SolidifyResourceNotFoundException("Archive with " + idType.getDescription() + " identifier '" + aipId + "' not found");
    } else {
      throw new SolidifyRuntimeException("more than one archive correspond to the " + idType.getDescription() + " identifier '" + aipId + "'");
    }
  }

  public boolean deleteIndex(String aipId) {
    try {
      this.deleteIndexMetadata(aipId);
    } catch (SolidifyResourceNotFoundException | SolidifyRestException e) {
      log.error("Error in deleting metadata index (" + aipId + ")", e);
      return false;
    }
    return true;
  }

}

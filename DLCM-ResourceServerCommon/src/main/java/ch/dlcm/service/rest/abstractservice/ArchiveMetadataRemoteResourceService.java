/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchiveMetadataRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.service.rest.abstractservice.IndexMetadataRemoteResourceService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.MetadataVisibility;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.rest.ResourceName;

public abstract class ArchiveMetadataRemoteResourceService extends IndexMetadataRemoteResourceService<ArchiveMetadata> {

  private final String dataMgmtIndexUrl;

  protected ArchiveMetadataRemoteResourceService(
          DLCMProperties dlcmProperties,
          SolidifyRestClientService dlcmRestClientService,
          MetadataVisibility metadataVisibility) {
    super(ArchiveMetadata.class, dlcmRestClientService, dlcmProperties.getIndexing().getIndexName(metadataVisibility));
    switch (metadataVisibility) {
      case ALL -> this.dataMgmtIndexUrl = dlcmProperties.getModule().getDataMgmt().getUrl() + "/" + ResourceName.DI;
      case RESTRICTED -> this.dataMgmtIndexUrl = dlcmProperties.getModule().getDataMgmt().getUrl() + "/" + ResourceName.DI_PRIVATE;
      default -> this.dataMgmtIndexUrl = dlcmProperties.getModule().getDataMgmt().getUrl() + "/" + ResourceName.DI_PUBLIC;
    }
  }

  public RestCollection<ArchiveMetadata> getMetadataList(Map<String, Object> parameters, Pageable pageable) {
    String url = this.buildUrl(parameters);
    return this.getIndexMetadataListWithUrl(url, pageable);
  }

  public RestCollection<ArchiveMetadata> getMetadataList(Pageable pageable) {
    return this.getMetadataList(Collections.emptyMap(), pageable);
  }

  public RestCollection<ArchiveMetadata> getMetadataList(String query, String field, String from, String to,
          String set, String orgUnit, Pageable pageable) {

    HashMap<String, Object> parameters = this.createParameterMap(query, field, from, to, set);
    parameters.put(DLCMConstants.SEARCH_ORG_UNIT, orgUnit);

    return this.getMetadataList(parameters, pageable);
  }

  @Override
  protected String buildUrl(Map<String, Object> parameters) {
    String url = this.getIndexUrl() + "/" + ActionName.SEARCH;
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);

    if (parameters != null) {

      this.completeUri(uriBuilder, parameters);

      if (!StringTool.isNullOrEmpty((String) parameters.get(DLCMConstants.SEARCH_ORG_UNIT))) {
        uriBuilder.queryParam(DLCMConstants.SEARCH_ORG_UNIT, parameters.get(DLCMConstants.SEARCH_ORG_UNIT));
      }
    }

    return uriBuilder.toUriString();
  }

  // Build index web service URL
  @Override
  protected String getIndexUrl() {
    return this.dataMgmtIndexUrl;
  }

  protected String formatField(String query) {
    return query.replace(":", "\\:");
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - SubmissionInfoPackageRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.RepresentationInfo;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

public abstract class SubmissionInfoPackageRemoteResourceService extends RemoteResourceService<SubmissionInfoPackage> {
  private static final Logger log = LoggerFactory.getLogger(SubmissionInfoPackageRemoteResourceService.class);

  private final MessageService messageService;

  private final String ingestUrl;
  private final String preingestUrl;

  protected SubmissionInfoPackageRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService,
          MessageService messageService) {
    super(restClientService);
    this.messageService = messageService;
    this.ingestUrl = dlcmProperties.getModule().getIngest().getUrl();
    this.preingestUrl = dlcmProperties.getModule().getPreingest().getUrl();
  }

  public RestCollection<SubmissionInfoPackage> getCompletedSIPList(Pageable pageable) {
    String jsonString = this.restClientService.getResource(this.ingestUrl + "/" + ResourceName.SIP + "?info.status=" +
            PackageStatus.COMPLETED, pageable);
    return new RestCollection<>(jsonString, SubmissionInfoPackage.class);
  }

  public RestCollection<SubmissionInfoPackage> getSIPList(Pageable pageable) {
    String jsonString = this.restClientService.getResource(this.ingestUrl + "/" + ResourceName.SIP, pageable);
    return new RestCollection<>(jsonString, SubmissionInfoPackage.class);
  }

  public RestCollection<SubmissionInfoPackage> getSIPListWithParameters(String queryParameters, Pageable pageable) {
    if (StringTool.isNullOrEmpty(queryParameters)) {
      throw new SolidifyCheckingException("The parameter list is missing");
    }
    String jsonString = this.restClientService.getResource(this.ingestUrl + "/" + ResourceName.SIP + "?" + queryParameters, pageable);
    return new RestCollection<>(jsonString, SubmissionInfoPackage.class);
  }

  public RestCollection<SipDataFile> getSipDataFileList(String sipId, Pageable pageable) {
    String jsonString = this.restClientService.getResource(this.ingestUrl + "/" + ResourceName.SIP + "/" + sipId + "/" +
            ResourceName.DATAFILE, pageable);
    return new RestCollection<>(jsonString, SipDataFile.class);
  }

  public void cleanSIPDatafile(SubmissionInfoPackage sip) {
    String url = this.ingestUrl + "/" + ResourceName.SIP + "/" + sip.getResId() + "/" + DLCMActionName.CLEAN;
    this.restClientService.postResourceAction(url);
  }

  public HttpStatusCode checkSipWorkingAip(String sipId) {
    // Check deposit SIP status
    String url = this.ingestUrl + "/" + ResourceName.SIP + "/" + sipId + "/" + ResourceName.AIP_FILE;
    return this.restClientService.headResource(url);
  }

  public boolean deleteWorkingAip(String sipId) {
    String url = this.ingestUrl + "/" + ResourceName.SIP + "/" + sipId + "/" + ResourceName.AIP_FILE;
    try {
      this.restClientService.deleteResource(url);
    } catch (HttpClientErrorException e) {
      log.error("Error in deleting working AIP of SIP " + sipId, e);
      return false;
    }
    return true;
  }

  public String sendSIP(Deposit deposit) {
    // Create SIP
    SubmissionInfoPackage sip = new SubmissionInfoPackage();
    sip.setPublicationDate(deposit.getPublicationDate());
    sip.setInfo(this.getDepositInfo(deposit));
    sip.setDepositId(deposit.getResId());
    sip.setCreatedBy(deposit.getCreatedBy());
    sip.setUpdatedBy(deposit.getUpdatedBy());
    sip.setDispositionApproval(deposit.getPreservationPolicy().getDispositionApproval());
    sip.setRetention(deposit.getPreservationPolicy().getRetention());
    sip.setMainStorage(deposit.getPreservationPolicy().getMainStorage());
    sip.setSubmissionPolicyId(deposit.getSubmissionPolicy().getResId());

    try {
      // Send SIP object
      SubmissionInfoPackage resSip = this.restClientService.postResource(this.ingestUrl + "/" + ResourceName.SIP, sip,
              SubmissionInfoPackage.class);

      if (resSip.getResId() != null) {

        log.trace("SIP created: resId={}", resSip.getResId());
        // Send SIP package
        SipDataFile sipDataFile = new SipDataFile(resSip);
        sipDataFile.setSourceData(URI.create(this.preingestUrl + "/" + ResourceName.DEPOSIT + "/" + deposit.getResId() + "/" +
                ResourceName.SIP_FILE));
        sipDataFile.setDataCategory(DataCategory.Package);
        sipDataFile.setDataType(DataCategory.InformationPackage);
        sipDataFile.setCreatedBy(deposit.getUpdatedBy());

        SipDataFile resPackage = this.restClientService.postResource(this.ingestUrl + "/" + ResourceName.SIP + "/" + resSip.getResId() +
                "/" + ResourceName.DATAFILE, sipDataFile, SipDataFile.class);

        if (resPackage.getResId() != null) {
          log.trace("SIP package created: resId = {}", resSip.getResId());
          // Send AIP collection
          if (deposit.isCollection()) {
            String resCollection = this.restClientService.postResourceOther(this.ingestUrl + "/" + ResourceName.SIP + "/" +
                    resSip.getResId() + "/" + ResourceName.AIP, deposit.getCollection(), String.class);
            if (resCollection == null) {
              log.error("Cannot create the AIP collection info for SIP ({})", resSip.getResId());
              return null;
            }
          }
          return resSip.getResId();
        } else {
          log.error("Cannot create the package info for SIP ({})", resSip.getResId());
        }

      } else {
        log.error("Cannot create SIP for deposit ({})", deposit.getResId());
      }
    } catch (Exception e) {
      log.error("Error in sending SIP ({})", deposit.getResId(), e);
    }
    return null;
  }

  public void sendSIPForUpdate(Deposit deposit) {
    final String sipId = deposit.getSipId();

    try {
      // Check SIP
      final String sipUrl = this.ingestUrl + "/" + ResourceName.SIP;
      final SubmissionInfoPackage sip = this.restClientService.getResource(sipUrl + "/" + sipId, SubmissionInfoPackage.class);
      // Check if ready for metadata edition
      if (!PackageStatus.isCompletedProcess(sip.getInfo().getStatus())) {
        throw new SolidifyCheckingException("SIP in wrong status: " + sip.getInfo().getStatus());
      }
      // Check if the deposit and sip must have the same version
      if (!sip.getMetadataVersion().equals(deposit.getMetadataVersion())) {
        throw new SolidifyCheckingException(
                "Cannot change metadata version: " + sip.getMetadataVersion().getVersion() + " -> " + deposit.getMetadataVersion().getVersion());
      }
      // Check if editing metadata
      if (sip.getInfo().getStatus() != PackageStatus.EDITING_METADATA) {
        // Put SIP in status EDITING_METADATA
        final String sipMetadataEditingUrl = sipUrl + "/" + sipId + "/" + DLCMActionName.START_METADATA_EDITING;
        final Result changeStatusResult = this.restClientService.postResource(sipMetadataEditingUrl, Result.class);
        if (changeStatusResult.getStatus() != Result.ActionStatus.EXECUTED) {
          throw new SolidifyRuntimeException("SIP cannot be put in metadata editing");
        }
      }

      // Update the SIP metadata
      final RepresentationInfo info = this.getDepositInfo(deposit);
      Map<String, Object> updateMap = new HashMap<>();
      updateMap.put("info", info.toUpdateMap());
      updateMap.put("publicationDate", deposit.getPublicationDate());
      this.restClientService.patchResource(sipUrl + "/" + sipId, updateMap, SubmissionInfoPackage.class);

      // Update SIP AIP collection
      final String collectionSipUrl = this.ingestUrl + "/" + ResourceName.SIP + "/" + sip.getResId() + "/" + ResourceName.AIP;
      final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
      final List<ArchivalInfoPackage> aipList = this.restClientService.getResourceList(collectionSipUrl, ArchivalInfoPackage.class, pageable)
              .getData();
      if (!aipList.isEmpty()) {
        final List<String> currentAipIdList = aipList.stream().map(ArchivalInfoPackage::getResId).toList();
        final List<String> newAipIdList = deposit.getCollection();
        // Delete existing AIPs
        this.restClientService.deleteResourceList(collectionSipUrl, currentAipIdList);
        // Create AIPs from the ones defines in the deposit
        this.restClientService.postResourceOther(collectionSipUrl, newAipIdList, String.class);
      }

      // Send updated metadata as a SIP data file
      final SipDataFile sipDataFile = new SipDataFile(sipId);
      final String depositSipUrl = this.preingestUrl + "/" + ResourceName.DEPOSIT + "/" + deposit.getResId() + "/" + ResourceName.SIP_FILE;
      sipDataFile.setSourceData(URI.create(depositSipUrl));
      sipDataFile.setDataCategory(DataCategory.Package);
      sipDataFile.setDataType(DataCategory.UpdatePackage);
      sipDataFile.setCreatedBy(deposit.getUpdatedBy());
      final String sipDatafileUrl = sipUrl + "/" + sipId + "/" + ResourceName.DATAFILE;
      this.restClientService.postResource(sipDatafileUrl, sipDataFile, SipDataFile.class);
    } catch (Exception e) {
      throw new SolidifyRuntimeException("Error in updating SIP of deposit (" + deposit.getResId() + ")", e);
    }

  }

  public Result upgradeMetadata(String sipId) {
    return this.restClientService
            .postResourceAction(this.ingestUrl + "/" + ResourceName.SIP + "/" + sipId + "/" + DLCMActionName.START_METADATA_UPGRADE);
  }

  @Override
  protected String getResourceUrl() {
    return this.ingestUrl + "/" + ResourceName.SIP;
  }

  @Override
  protected Class<SubmissionInfoPackage> getResourceClass() {
    return SubmissionInfoPackage.class;
  }

  private RepresentationInfo getDepositInfo(Deposit deposit) {
    RepresentationInfo info = new RepresentationInfo();
    info.setMetadataVersion(deposit.getMetadataVersion());
    info.setName(this.messageService.get("preingest.sip.name", new Object[] { deposit.getTitle() }));
    info.setDescription(StringTool.truncateOnSpaceWithEllipsis(
            this.messageService.get("preingest.sip.description", new Object[] { deposit.getDescription() }),
            SolidifyConstants.DB_LONG_STRING_LENGTH));
    info.setOrganizationalUnitId(deposit.getOrganizationalUnitId());
    info.setAccess(deposit.getAccess());
    info.setEmbargo(deposit.getEmbargo());
    info.setDataSensitivity(deposit.getDataSensitivity());
    info.setDataUsePolicy(deposit.getDataUsePolicy());
    info.setLicenseId(deposit.getLicenseId());
    info.setComplianceLevel(deposit.getComplianceLevel());
    info.setContainsUpdatedMetadata(deposit.getContainsUpdatedMetadata());
    info.setDataUsePolicy(deposit.getDataUsePolicy());
    info.setContentStructurePublic(deposit.getContentStructurePublic());
    return info;
  }

}

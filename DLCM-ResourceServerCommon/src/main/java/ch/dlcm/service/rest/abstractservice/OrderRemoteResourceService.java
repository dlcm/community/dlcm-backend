/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - OrderRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.access.Order;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

public abstract class OrderRemoteResourceService extends RemoteResourceService<Order> {

  private final String accessUrl;

  protected OrderRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.accessUrl = dlcmProperties.getModule().getAccess().getUrl();
  }

  public Order createOrder(Order order) {
    final String url = this.accessUrl + "/" + ResourceName.ORDER;
    return this.restClientService.postResource(url, order, Order.class);
  }

  public RestCollection<Order> getOrder(String aipId) {
    this.restClientService.checkResId(aipId);
    // Send Order object
    String jsonString = this.restClientService.getResource(this.accessUrl + "/" + ResourceName.ORDER + "?query=" + aipId, String.class);
    return new RestCollection<>(jsonString, Order.class);
  }

  public Order saveOrder(String orderId) {
    String url = this.accessUrl + "/" + ResourceName.ORDER + "/" + orderId + "/" + ActionName.SAVE;
    // Send Order object
    return this.restClientService.postResource(url, Order.class);
  }

  public Result submitOrder(String orderId) {
    String url = this.accessUrl + "/" + ResourceName.ORDER + "/" + orderId + "/" + DLCMActionName.SUBMIT;
    // Send Order object
    return this.restClientService.postResourceAction(url);
  }

  public RestCollection<Order> getCompletedOrder(Pageable pageable) {
    String url = this.accessUrl + "/" + ResourceName.ORDER + "?status=" + PackageStatus.READY;
    String jsonString = this.restClientService.getResource(url, pageable);
    return new RestCollection<>(jsonString, Order.class);
  }

  public RestCollection<Order> getCompletedOrderWithParameters(String queryParameters, Pageable pageable) {
    if (StringTool.isNullOrEmpty(queryParameters)) {
      throw new SolidifyCheckingException("The parameter list is missing");
    }
    String url = this.accessUrl + "/" + ResourceName.ORDER + "?status=" + PackageStatus.READY + "&" + queryParameters;
    String jsonString = this.restClientService.getResource(url, pageable);
    return new RestCollection<>(jsonString, Order.class);
  }

  public void deleteOrder(String orderId) {
    String url = this.accessUrl + "/" + ResourceName.ORDER + "/" + orderId;
    this.restClientService.deleteResource(url);
  }

  @Override
  protected Class<Order> getResourceClass() {
    return Order.class;
  }
}

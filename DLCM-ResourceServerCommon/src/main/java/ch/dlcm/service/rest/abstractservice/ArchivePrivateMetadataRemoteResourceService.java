/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchivePrivateMetadataRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.MetadataVisibility;
import ch.dlcm.model.index.ArchiveMetadata;

public abstract class ArchivePrivateMetadataRemoteResourceService extends ArchiveMetadataRemoteResourceService {
  private static final Logger log = LoggerFactory.getLogger(ArchivePrivateMetadataRemoteResourceService.class);

  private static final String DEPOSIT_ID = "packages.deposits.id";
  private static final String SIP_ID = "packages.sip.id";

  protected ArchivePrivateMetadataRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService dlcmRestClientService) {
    super(dlcmProperties, dlcmRestClientService, MetadataVisibility.RESTRICTED);
  }

  public String getDepositId(String aipId) {
    ArchiveMetadata aip = this.getIndexMetadata(aipId);
    return (String) aip.getMetadata(DEPOSIT_ID.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
  }

  public String getSipId(String aipId) {
    ArchiveMetadata aip = this.getIndexMetadata(aipId);
    return (String) aip.getMetadata(SIP_ID.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
  }

  public String getAipIdFromDepositId(String depositId) {
    ArchiveMetadata aip = this.getPackage(DEPOSIT_ID, depositId);
    return aip.getResId();
  }

  public String getAipIdFromSipId(String sipId) {
    ArchiveMetadata aip = this.getPackage(SIP_ID, sipId);
    return aip.getResId();
  }

  public List<String> getCollections(String aipId, boolean allParents) {
    List<String> aipList = new ArrayList<>();
    Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    RestCollection<ArchiveMetadata> archiveMetadataCollection;
    do {
      // List collection of the AIP
      archiveMetadataCollection = this.getArchiveMetadataCollections(aipId, pageable);
      // If no collection, return
      if (archiveMetadataCollection.getData().isEmpty()) {
        return aipList;
      }
      pageable = pageable.next();
      for (final ArchiveMetadata archive : archiveMetadataCollection.getData()) {
        final String collectionId = (String) archive.getMetadata("archiveId");
        aipList.add(collectionId);
        // Retrieve all parent collections recursively
        if (allParents) {
          aipList.addAll(this.getCollections(collectionId, allParents));
        }
      }
    } while (archiveMetadataCollection.getPage().hasNext());
    return aipList;
  }

  public boolean deleteIndex(String aipId) {
    try {
      this.deleteIndexMetadata(aipId);
      this.deleteDataFileIndex(aipId);
    } catch (SolidifyResourceNotFoundException | SolidifyRestException e) {
      log.error("Error in deleting metadata index (" + aipId + ")", e);
      return false;
    }
    return true;
  }

  protected ArchiveMetadata getPackage(String fieldToSearch, String valueToSearch) {
    final String query = "type:package AND " + fieldToSearch + ":\"" + valueToSearch + "\"";
    RestCollection<ArchiveMetadata> collection = this.getIndexMetadataList(query, null, null, null, null, null);
    if (collection.getPage().getTotalItems() == 1) {
      return collection.getData().get(0);
    } else if (collection.getPage().getTotalItems() == 0) {
      throw new SolidifyResourceNotFoundException("Archive not found: " + fieldToSearch + " = " + valueToSearch);
    } else {
      throw new SolidifyRuntimeException("Too many archives: " + query);
    }
  }

  protected RestCollection<ArchiveMetadata> getArchiveMetadataCollections(String aipId, Pageable pageable) {
    final String query = "type:aip AND aip.id:\"" + aipId + "\"";
    return this.getIndexMetadataList(query, null, null, null, null, pageable);
  }

  private void deleteDataFileIndex(String aipId) {
    final Pageable page = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String url = this.getIndexUrl() + "/" + ActionName.SEARCH + "?query=" + DLCMConstants.ARCHIVE_ID_FIELD +
            ":" + aipId;
    RestCollection<ArchiveMetadata> dataFiles = this.restClientService.getResourceList(url, ArchiveMetadata.class, page);
    while (dataFiles.getPage().getTotalItems() > 0) {
      // List IDs
      final List<String> dataFilesIds = dataFiles.getData().stream().map(NoSqlResource::getResId).toList();
      if (!dataFilesIds.isEmpty()) {
        // Delete IDs
        this.restClientService.deleteResourceList(this.getIndexUrl(), dataFilesIds);
      }
      // List all data files => always first page because of deletion
      dataFiles = this.restClientService.getResourceList(url, ArchiveMetadata.class, page);
    }
  }
}

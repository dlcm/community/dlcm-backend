/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - AdditionalFieldsFormRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import org.springframework.cache.annotation.Cacheable;

import ch.unige.solidify.rest.CacheNames;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.settings.AdditionalFieldsForm;
import ch.dlcm.rest.ResourceName;

public abstract class AdditionalFieldsFormRemoteResourceService extends RemoteResourceService<AdditionalFieldsForm> {

  private final String adminUrl;

  protected AdditionalFieldsFormRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  public AdditionalFieldsForm getAdditionalFieldsForm(String orgUnitId, String additionalFieldsFormId) {
    this.restClientService.checkResId(orgUnitId);
    this.restClientService.checkResId(additionalFieldsFormId);
    String url = this.adminUrl + "/" + ResourceName.ORG_UNIT + "/" + orgUnitId + "/" + ResourceName.ADDITIONAL_FIELDS_FORMS + "/"
            + additionalFieldsFormId;
    return this.restClientService.getResource(url, AdditionalFieldsForm.class);
  }

  public AdditionalFieldsForm findOne(String orgUnitId, String formId) {
    return this.getAdditionalFieldsForm(orgUnitId, formId);
  }

  @Cacheable(CacheNames.RESOURCES)
  public AdditionalFieldsForm findOneWithCache(String orgUnitId, String formId) {
    return this.findOne(orgUnitId, formId);
  }

  @Override
  protected Class<AdditionalFieldsForm> getResourceClass() {
    return AdditionalFieldsForm.class;
  }
}

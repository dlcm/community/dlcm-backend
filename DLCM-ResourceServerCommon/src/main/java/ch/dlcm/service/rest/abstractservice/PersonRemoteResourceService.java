/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - PersonRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.DLCMCacheNames;

public abstract class PersonRemoteResourceService extends RemoteResourceService<Person> {

  private final ObjectMapper mapper = new ObjectMapper();
  private final UserRemoteResourceService userRemoteResourceService;

  private final String adminUrl;

  protected PersonRemoteResourceService(UserRemoteResourceService userRemoteResourceService, DLCMProperties dlcmProperties,
          SolidifyRestClientService restClientService) {
    super(restClientService);
    this.userRemoteResourceService = userRemoteResourceService;
    this.adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  @Cacheable(DLCMCacheNames.INSTITUTIONS_ROLES_BY_PERSON)
  public List<Institution> findInstitutions(String personId) {
    return this.getPersonInstitutions(personId, PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE)).getData();
  }

  @Cacheable(DLCMCacheNames.ORGANIZATIONAL_UNIT_ROLES)
  public Optional<Role> findOrganizationalUnitRole(String personId, String orgUnitId) {
    final String url = this.adminUrl + "/" + ResourceName.PERSON + "/" + personId + "/" + ResourceName.ORG_UNIT + "/" + orgUnitId;
    final String jsonString;
    try {
      jsonString = this.restClientService.getResource(url);
    } catch (SolidifyResourceNotFoundException e) {
      return Optional.empty();
    }
    List<Role> roleList;
    try {
      final JsonNode jsonRootNode = this.mapper.readTree(jsonString);
      final JsonNode jsonRolesNode = jsonRootNode.get("roles");
      final String jsonRolesStr = jsonRolesNode.toString();
      this.mapper.registerModule(new com.fasterxml.jackson.datatype.jsr310.JavaTimeModule());
      this.mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
      this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      final JavaType listRoleType = this.mapper.getTypeFactory().constructParametricType(List.class, Role.class);
      roleList = this.mapper.readValue(jsonRolesStr, listRoleType);
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("Error finding roles for person " + personId + " on organizational unit " + orgUnitId, e);
    }
    if (roleList.isEmpty()) {
      return Optional.empty();
    } else if (roleList.size() == 1) {
      return Optional.of(roleList.get(0));
    } else {
      throw new SolidifyRuntimeException("Person " + personId + " has multiple roles in organizational unit " + orgUnitId);
    }
  }

  @Cacheable(DLCMCacheNames.INHERITED_ORGANIZATIONAL_UNIT_ROLE)
  public Optional<Role> findInheritedOrganizationalRole(String personId, String orgUnitId) {
    final String url = this.adminUrl + "/" + ResourceName.PERSON + "/" + personId + "/" + DLCMActionName.LIST_INHERITED_ROLE + "/" +
            ResourceName.ORG_UNIT + "/" + orgUnitId;
    try {
      return Optional.ofNullable(this.restClientService.postResource(url, Role.class));
    } catch (SolidifyResourceNotFoundException e) {
      return Optional.empty();
    } catch (final SolidifyRuntimeException e) {
      throw new SolidifyRuntimeException("Error finding inherited role for person " + personId + " on organizational unit " + orgUnitId, e);
    }
  }

  public RestCollection<Institution> getPersonInstitutions(String personId, Pageable pageable) {
    String url = this.adminUrl + "/" + ResourceName.PERSON + "/" + personId + "/" + ResourceName.INSTITUTION;
    String jsonString = this.restClientService.getResource(url, pageable);
    return new RestCollection<>(jsonString, Institution.class);
  }

  public RestCollection<Person> getPersonList(String queryString, Pageable pageable) {
    final StringBuilder url = new StringBuilder(this.adminUrl);
    url.append("/");
    url.append(ResourceName.PERSON);
    if (!StringTool.isNullOrEmpty(queryString)) {
      url.append("?");
      url.append(queryString);
    }
    String jsonString = this.restClientService.getResource(url.toString(), pageable);
    return new RestCollection<>(jsonString, Person.class);
  }

  public Person getPerson(String resId) {
    this.restClientService.checkResId(resId);
    String url = this.adminUrl + "/" + ResourceName.PERSON + "/" + resId;
    return this.restClientService.getResource(url, Person.class);
  }

  public Person getByOrcid(String orcid) {
    String url = this.adminUrl + "/" + ResourceName.PERSON + "/" + orcid;
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.set(DLCMConstants.IDENTIFIER_TYPE_PARAM, ResourceIdentifierType.ORCID.getName());
    uriBuilder.queryParams(params);
    url = uriBuilder.toUriString();
    return this.restClientService.getResource(url, Person.class);
  }

  /**
   * Return the Person id linked to the Authentication.
   * <p>
   * - It uses REST HTTP requests to get the Person linked to OAuth2 token (call is made on Admin)
   *
   * @param authentication the authentication object of the linked person
   * @return String the personId (existing in the Admin module)
   */
  public String getLinkedPersonId(Authentication authentication) {

    final String authUserId = authentication.getName();
    String personId = null;

    /*
     * Get Person linked to user identified by its OAuth2 token
     */
    final User user = this.userRemoteResourceService.findByExternalUid(authUserId);
    if (user != null && user.getPerson() != null) {
      personId = user.getPerson().getResId();
    }

    return personId;
  }

  @Override
  protected Class<Person> getResourceClass() {
    return Person.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.PERSON;
  }
}

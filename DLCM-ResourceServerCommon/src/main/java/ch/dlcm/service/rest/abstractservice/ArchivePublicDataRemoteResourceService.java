/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchivePublicDataRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.index.ArchivePublicData;
import ch.dlcm.model.index.ArchivePublicData.PublicDataFileType;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

public abstract class ArchivePublicDataRemoteResourceService extends RemoteResourceService<ArchivePublicData> {
  private static final Logger log = LoggerFactory.getLogger(ArchivePublicDataRemoteResourceService.class);

  private final String dataMgmtUrl;

  protected ArchivePublicDataRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.dataMgmtUrl = dlcmProperties.getModule().getDataMgmt().getUrl();
  }

  public void createArchivePublicData(String aipId, PublicDataFileType dataFileType, Path dataFile, String mimeType) {
    ArchivePublicData archivePublicData = new ArchivePublicData();
    archivePublicData.setAipId(aipId);
    archivePublicData.setDataFileType(dataFileType);

    try {
      String url = this.dataMgmtUrl + "/" + ResourceName.ARCHIVE_PUBLIC_DATA;
      ArchivePublicData newArchivePublicData = this.restClientService.postResource(url, archivePublicData, ArchivePublicData.class);
      this.restClientService.uploadContent(
              url
                      + "/" + newArchivePublicData.getResId()
                      + "/" + DLCMActionName.UPLOAD_DATASET_FILE
                      + "?" + MIME_TYPE_PARAM + "=" + mimeType,
              new FileSystemResource(dataFile),
              "file",
              ArchivePublicData.class);
    } catch (Exception e) {
      log.error("Error in creating archive data file ({}-{})", aipId, dataFileType);
      throw e;
    }
  }

  public boolean deleteArchivePublicData(String aipId, PublicDataFileType dataFileType) {
    try {
      this.restClientService.deleteResource(
              this.dataMgmtUrl
                      + "/" + ResourceName.ARCHIVE_PUBLIC_DATA
                      + "/" + this.getArchivePublicDataId(aipId, dataFileType));
    } catch (SolidifyResourceNotFoundException | SolidifyRestException e) {
      log.error("Error in deleting archive data file(" + aipId + ")", e);
      return false;
    }
    return true;
  }

  public boolean checkArchivePublicData(String aipId, PublicDataFileType dataFileType) {
    String url = this.getArchivePublicDataUrlByArchive(aipId, dataFileType);
    return (this.restClientService.headResource(url) == HttpStatus.OK);
  }

  public void downloadArchivePublicData(String aipId, PublicDataFileType dataFileType, Path path) {
    String url = this.dataMgmtUrl
            + "/" + ResourceName.ARCHIVE_PUBLIC_DATA +
            "/" + this.getArchivePublicDataId(aipId, dataFileType)
            + "/" + DLCMActionName.DOWNLOAD_DATASET_FILE;
    this.restClientService.downloadContent(url, path, TokenUsage.WITH_TOKEN);
  }

  @Override
  protected Class<ArchivePublicData> getResourceClass() {
    return ArchivePublicData.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.dataMgmtUrl + "/" + ResourceName.ARCHIVE_PUBLIC_DATA;
  }

  private String getArchivePublicDataUrlByArchive(String aipId, PublicDataFileType dataFileType) {
    return this.dataMgmtUrl
            + "/" + ResourceName.ARCHIVE_PUBLIC_DATA
            + "/" + aipId
            + "?dataFileType=" + dataFileType;
  }

  private String getArchivePublicDataId(String aipId, PublicDataFileType dataFileType) {
    String url = this.getArchivePublicDataUrlByArchive(aipId, dataFileType);
    ArchivePublicData archivePublicData = this.restClientService.getResource(url, this.getResourceClass());
    return archivePublicData.getResId();
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - UserRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.rest.abstractservice;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.security.User;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.DLCMCacheNames;

public abstract class UserRemoteResourceService extends RemoteResourceService<User> {
  private static final Logger log = LoggerFactory.getLogger(UserRemoteResourceService.class);

  private final String adminUrl;

  protected UserRemoteResourceService(DLCMProperties dlcmProperties, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.adminUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  public User getUser(String resId) {
    this.restClientService.checkResId(resId);
    String url = this.adminUrl + "/" + ResourceName.USER + "/" + resId;
    return this.restClientService.getResource(url, User.class);
  }

  public User getUserAuthenticated() {
    String url = this.adminUrl + "/" + ResourceName.USER + "/" + DLCMActionName.AUTHENTICATED;
    return this.restClientService.getResource(url, User.class);
  }

  public List<User> getUsersByExternalUid(String externalUid) {
    final Pageable page = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    // Build URL
    String url = this.adminUrl + "/" + ResourceName.USER;
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.set("externalUid", externalUid);
    uriBuilder.queryParams(params);
    url = uriBuilder.toUriString();
    // Get User RestCollection
    RestCollection<User> userCollection = this.restClientService.getResourceList(url, User.class, page);
    // Return user list
    return userCollection.getData();
  }

  public List<User> findByPersonResId(String personId) {
    final Pageable page = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    // Build URL
    String url = this.adminUrl + "/" + ResourceName.USER;
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.set("person.resId", personId);
    uriBuilder.queryParams(params);
    url = uriBuilder.toUriString();
    // Get User RestCollection
    RestCollection<User> userCollection = this.restClientService.getResourceList(url, User.class, page);
    // Return user list
    return userCollection.getData();
  }

  @Cacheable(DLCMCacheNames.USER_EXTERNAL_UID)
  public User findByExternalUid(String externalUid) {
    try {
      final List<User> users = this.getUsersByExternalUid(externalUid);

      if (users.size() == 1) {
        return users.get(0);
      } else if (users.size() > 1) {
        throw new SolidifyRuntimeException("more than one user correspond to the externalUid");
      }

    } catch (final Exception e) {
      log.error("Error finding external UID " + externalUid, e);
    }

    return null;
  }

  @Override
  protected Class<User> getResourceClass() {
    return User.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminUrl + "/" + ResourceName.USER;
  }
}

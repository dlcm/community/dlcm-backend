/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DataCategoryService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.DataCategory;

@Service
public class DataCategoryService extends DLCMService {

  private final boolean researchDataEnabled;
  private final boolean administrativeDataEnabled;

  private final List<DataCategory> dataFileCategoryList;
  private final List<DataCategory> packageCategoryList;

  public DataCategoryService(MessageService messageService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    // Configuration
    this.researchDataEnabled = dlcmProperties.getParameters().isResearchDataEnabled();
    this.administrativeDataEnabled = dlcmProperties.getParameters().isAdministrativeDataEnabled();
    // Data File list
    this.dataFileCategoryList = DataCategory.listCategories();
    if (!this.researchDataEnabled) {
      this.removeResearchCategories(this.dataFileCategoryList);
    }
    if (!this.administrativeDataEnabled) {
      this.removeAdministrativeCategories(this.dataFileCategoryList);
    }
    // Package list
    this.packageCategoryList = DataCategory.listCategories();
    this.removeResearchCategories(this.packageCategoryList);
    this.removeAdministrativeCategories(this.packageCategoryList);
  }

  public DataCategory getDataFileDefaultCategories() {
    if (this.researchDataEnabled) {
      return DataCategory.Primary;
    }
    if (this.administrativeDataEnabled) {
      return DataCategory.Administrative;
    }
    throw new SolidifyRuntimeException("Missing default data file category");
  }

  public List<DataCategory> listDataFileCategories() {
    return this.dataFileCategoryList;
  }

  public List<DataCategory> listDataFileTypes(String category) {
    final DataCategory dataCategory = DataCategory.valueOf(category);
    if (this.listDataFileCategories().contains(dataCategory)) {
      return DataCategory.listSubCategories(dataCategory);
    }
    return new ArrayList<>();
  }

  public DataCategory getPackageDefaultCategories() {
    return DataCategory.Package;
  }

  public List<DataCategory> listPackageCategories() {
    return this.packageCategoryList;
  }

  public List<DataCategory> listPackageTypes(String category) {
    final DataCategory dataCategory = DataCategory.valueOf(category);
    if (this.listPackageCategories().contains(dataCategory)) {
      return DataCategory.listSubCategories(dataCategory);
    }
    return new ArrayList<>();
  }

  public boolean checkDataFileCategory(DataCategory dataCategory) {
    return this.listDataFileCategories().contains(dataCategory);
  }

  public boolean checkDataFileCategoryAndType(DataCategory dataCategory, DataCategory dataType) {
    return this.checkDataFileCategory(dataCategory) && this.listDataFileTypes(dataCategory.toString()).contains(dataType);
  }

  public boolean checkPackageCategory(DataCategory dataCategory) {
    return this.listPackageCategories().contains(dataCategory);
  }

  public boolean checkPackageCategoryAndType(DataCategory dataCategory, DataCategory dataType) {
    return this.checkPackageCategory(dataCategory) && this.listPackageTypes(dataCategory.toString()).contains(dataType);
  }

  private void removeResearchCategories(List<DataCategory> categoryList) {
    categoryList.remove(DataCategory.Primary);
    categoryList.remove(DataCategory.Secondary);
    categoryList.remove(DataCategory.Software);
  }

  private void removeAdministrativeCategories(List<DataCategory> categoryList) {
    categoryList.remove(DataCategory.Administrative);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - MetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.model.xml.xhtml.v5.Html;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.OAIMetadataService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.Access;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.FileInfoUpdate;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DipDataFile;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.settings.MetadataType;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.metadata.MetadataGenerator;
import ch.dlcm.service.metadata.MetadataVersion1;
import ch.dlcm.service.metadata.MetadataVersion2;
import ch.dlcm.service.metadata.MetadataVersion3;
import ch.dlcm.service.metadata.MetadataVersion4;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchiveTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLanguageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLicenseRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackMetadataTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPersonRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPreservationPolicyRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;

@Service
public class MetadataService extends DLCMService implements OAIMetadataService {

  private static final Logger log = LoggerFactory.getLogger(MetadataService.class);

  private final String preingestTempLocation;
  private final DLCMProperties dlcmProperties;
  private final DLCMRepositoryDescription repositoryDescription;
  private final FallbackArchivalInfoPackageRemoteResourceService aipRemoteService;
  private final GitInfoProperties gitInfoProperties;
  private final HistoryService historyService;
  private final FallbackLanguageRemoteResourceService languageRemoteService;
  private final FallbackMetadataTypeRemoteResourceService metadataTypeRemoteService;
  private final FallbackOrganizationalUnitRemoteResourceService orgUnitRemoteService;
  private final FallbackPersonRemoteResourceService personRemoteService;
  private final FallbackLicenseRemoteResourceService licenseRemoteService;
  private final FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteService;
  private final FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteService;
  private final FallbackPreservationPolicyRemoteResourceService preservationPolicyRemoteService;
  private final FallbackArchiveTypeRemoteResourceService archiveTypeResourceService;
  private final FileFormatService fileFormatService;
  private final TrustedDepositRemoteResourceService depositRemoteService;

  public MetadataService(
          FallbackLanguageRemoteResourceService languageRemoteService,
          FallbackOrganizationalUnitRemoteResourceService orgUnitRemoteService,
          FallbackArchivalInfoPackageRemoteResourceService aipRemoteService,
          HistoryService historyService,
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repositoryDescription,
          MessageService messageService,
          GitInfoProperties gitInfoProperties,
          FileFormatService fileFormatService,
          FallbackMetadataTypeRemoteResourceService metadataTypeRemoteService,
          FallbackPersonRemoteResourceService personRemoteService,
          FallbackLicenseRemoteResourceService licenseRemoteService,
          FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteService,
          FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteService,
          FallbackPreservationPolicyRemoteResourceService preservationPolicyRemoteService,
          FallbackArchiveTypeRemoteResourceService archiveTypeResourceService,
          TrustedDepositRemoteResourceService depositRemoteService) {
    super(messageService, dlcmProperties);
    this.preingestTempLocation = dlcmProperties.getTempLocation(dlcmProperties.getPreingestLocation());
    this.dlcmProperties = dlcmProperties;
    this.repositoryDescription = repositoryDescription;
    this.languageRemoteService = languageRemoteService;
    this.orgUnitRemoteService = orgUnitRemoteService;
    this.aipRemoteService = aipRemoteService;
    this.historyService = historyService;
    this.gitInfoProperties = gitInfoProperties;
    this.metadataTypeRemoteService = metadataTypeRemoteService;
    this.personRemoteService = personRemoteService;
    this.licenseRemoteService = licenseRemoteService;
    this.archivePublicMetadataRemoteService = archivePublicMetadataRemoteService;
    this.archivePrivateMetadataRemoteService = archivePrivateMetadataRemoteService;
    this.preservationPolicyRemoteService = preservationPolicyRemoteService;
    this.archiveTypeResourceService = archiveTypeResourceService;
    this.fileFormatService = fileFormatService;
    this.depositRemoteService = depositRemoteService;
  }

  // ********************
  // ** Metadata Check **
  // ********************
  public void checkCustomMetadata(AbstractDataFile<?, ?> df) {
    try {
      if (StringTool.isNullOrEmpty(df.getMetadataTypeId())) {
        df.setComplianceLevel(ComplianceLevel.NO_COMPLIANCE);
      } else {
        final Path mdFile = Paths.get(df.getFinalData());
        final MetadataType metadataType = this.metadataTypeRemoteService.findOne(df.getMetadataTypeId());
        switch (metadataType.getMetadataFormat()) {
          case SCHEMA_LESS -> {
            if (FileTool.checkFile(mdFile)) {
              df.setComplianceLevel(ComplianceLevel.AVERAGE_COMPLIANCE);
              if (FileTool.getSize(mdFile) > 0) {
                df.setComplianceLevel(ComplianceLevel.FULL_COMPLIANCE);
              }
            }
          }
          case JSON -> {
            JSONTool.wellformed(mdFile);
            df.setComplianceLevel(ComplianceLevel.AVERAGE_COMPLIANCE);
            if (!StringTool.isNullOrEmpty(metadataType.getMetadataSchema())) {
              JSONTool.validate(metadataType.getMetadataSchema(), FileTool.toString(mdFile));
              df.setComplianceLevel(ComplianceLevel.FULL_COMPLIANCE);
            }
          }
          case XML -> {
            XMLTool.wellformed(mdFile);
            df.setComplianceLevel(ComplianceLevel.AVERAGE_COMPLIANCE);
            if (!StringTool.isNullOrEmpty(metadataType.getMetadataSchema())) {
              XMLTool.validate(metadataType.getMetadataSchema(), FileTool.toString(mdFile));
              df.setComplianceLevel(ComplianceLevel.FULL_COMPLIANCE);
            }
          }
          default -> df.setComplianceLevel(ComplianceLevel.WEAK_COMPLIANCE);
        }
      }
    } catch (final IOException e) {
      throw new SolidifyCheckingException(e.getMessage(), e);
    }
  }

  public void checkDlcmMetadata(DLCMMetadataVersion version, AbstractDataFile<?, ?> df) throws IOException {
    final Path mdFile = Paths.get(df.getFinalData());
    XMLTool.wellformed(mdFile);
    df.setComplianceLevel(ComplianceLevel.AVERAGE_COMPLIANCE);
    XMLTool.validate(this.getDlcmSchema(version, df), mdFile);
    df.setComplianceLevel(ComplianceLevel.FULL_COMPLIANCE);
  }

  public void checkMetadataFile(DLCMMetadataVersion version, Path mdFile, AbstractDataFile<?, ?> df) throws IOException {
    // Check XML: well-formed & valid
    this.checkXML(version, mdFile, df);

    try {
      // Get metadata generator for the version
      final MetadataGenerator metadataGenerator = this.getMetadataGenerator(version);
      metadataGenerator.checkMetadataFile(version, mdFile, this.getPackage(df));
    } catch (final JAXBException e) {
      throw new SolidifyCheckingException(this.messageService.get("checking.metadata.notvalid", new Object[] { e.getMessage() }));
    }
  }

  private void checkXML(DLCMMetadataVersion version, Path mdFile, AbstractDataFile<?, ?> dfType) throws IOException {
    if (!FileTool.checkFile(mdFile)) {
      throw new SolidifyCheckingException(this.messageService.get("checking.metadata.nofile", new Object[] { mdFile.toString() }));
    }
    try {
      XMLTool.wellformed(mdFile);
    } catch (final SolidifyCheckingException e) {
      throw new SolidifyCheckingException(this.messageService.get("checking.metadata.notwellformed", new Object[] { mdFile.toString() }));
    }
    try {
      XMLTool.validate(this.getDlcmSchema(version, dfType), mdFile);
    } catch (final SolidifyCheckingException e) {
      throw new SolidifyCheckingException(this.messageService.get("checking.metadata.notvalid", new Object[] { e.getMessage() }));
    }
  }

  // *************************
  // ** Metadata Generation **
  // *************************

  // ** Archive Metadata **
  public ArchiveMetadata completeArchiveIndex(DLCMMetadataVersion version, ArchiveMetadata archiveMetadata, Path xml)
          throws IOException, JAXBException {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(version);
    metadataGenerator.completeArchiveIndex(archiveMetadata, xml);
    this.fixDataciteMapCreatorContent(archiveMetadata);
    return archiveMetadata;
  }

  /**
   * Fix the creator property: if there is only one creator, it is created as a Map instead of a list of Maps (with each Map containing one
   * contributor's properties)
   *
   * @param archiveMetadata
   */
  private void fixDataciteMapCreatorContent(ArchiveMetadata archiveMetadata) {
    Object dataciteCreator = archiveMetadata.getMetadata(DLCMConstants.CREATOR_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
    if (dataciteCreator instanceof Map) {
      List<Map<String, Object>> creatorsList = new ArrayList<>();
      creatorsList.add((Map<String, Object>) dataciteCreator);
      Map<String, Object> dataciteCreators = (Map<String, Object>) archiveMetadata
              .getMetadata(DLCMConstants.CREATORS_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
      dataciteCreators.put("datacite:creator", creatorsList);
    }
  }

  // ** Archive Reload **
  public Map<String, String> extractInfoFromMetadata(DLCMMetadataVersion version, Path xml)
          throws IOException, JAXBException {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(version);
    return metadataGenerator.extractInfoFromMetadata(xml);
  }

  public void extractDataciteMetadataFromFile(Path workingFolder, OutputStream outputStream, DLCMMetadataVersion metadataVersion)
          throws IOException, JAXBException {
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(metadataVersion);
    metadataGenerator.extractDataciteMetadataFromFile(workingFolder, outputStream);
  }

  // ** Update Datacite metadata on Deposit **
  public void writeDataciteXmlFromIndexCompletedWithDeposit(DLCMMetadataVersion version, Deposit deposit, OutputStream outputStream)
          throws JAXBException {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(version);
    metadataGenerator.writeDataciteXmlFromIndexCompletedWithDeposit(deposit, outputStream);
  }

  // ** Premis File from updated file on Deposit **
  public void writePremisXmlOfUpdatedFile(DLCMMetadataVersion version, Deposit deposit, List<DepositDataFile> dataFiles, String outputLocation)
          throws JAXBException, IOException {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(version);
    metadataGenerator.writePremisXmlOfUpdatedFile(deposit, dataFiles, outputLocation);

  }

  // ** Update Deposit from Index **
  public Deposit writeDepositFromIndex(Deposit deposit) {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(deposit.getMetadataVersion());
    log.trace("metadataGenerator for version {} found: {}", deposit.getMetadataVersion(), metadataGenerator.getClass().getSimpleName());
    return metadataGenerator.writeDepositFromIndex(deposit);
  }

  // ** OAI-PMH ** //

  @Override
  public Object getOAIMetadata(String dataCite) throws JAXBException {
    // Get metadata generator for the default version
    // Always the default version for OAI-PMH
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(DLCMMetadataVersion.getDefaultVersion());
    return metadataGenerator.getDataCiteMetadata(dataCite);
  }

  @Override
  public List<Class<?>> getOaiXmlClasses() {
    return Arrays.asList(
            ch.dlcm.model.xml.dlcm.v2.dlcm.RepositoryInfo.class,
            ch.dlcm.model.xml.dlcm.v4.mets.Resource.class,
            Html.class);
  }

  @Override
  public Marshaller getMarshaller(JAXBContext jaxbContext) throws JAXBException {
    return MetadataGenerator.getMarshaller(jaxbContext, DLCMMetadataVersion.getDefaultVersion());
  }
  // ** Deposit **

  /**
   * Generate a basic DataCite metadata file and use it to add a new DepositDataFile on the deposit.
   * <p>
   * Metadata are based on the deposit properties
   *
   * @param deposit
   * @throws JAXBException
   * @throws IOException
   */
  public void generateDefaultDataCiteFromDeposit(Deposit deposit) throws JAXBException, IOException {

    final Path tmpFolderPath = Paths.get(this.preingestTempLocation)
            .resolve(deposit.getResId());
    final Path tmpFilePath = tmpFolderPath.resolve(DLCMConstants.METADATA_FILE);

    if (!FileTool.ensureFolderExists(tmpFolderPath)) {
      throw new IOException(this.messageService.get("datafile.error.folder.creation"));
    }

    /*
     * Create DataCite XML based on deposit properties
     */
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(deposit.getMetadataVersion());
    metadataGenerator.completeDataciteMetadataWithDeposit(deposit, tmpFilePath);

    final DepositDataFile depositDataFile = new DepositDataFile(deposit);

    depositDataFile.setDataCategory(DataCategory.Package);
    depositDataFile.setDataType(DataCategory.Metadata);
    depositDataFile.setSourceData(tmpFilePath.toUri());

    deposit.getDataFiles().add(depositDataFile);
  }

  public void generateMetadata(Deposit deposit, Path outputLocation) throws JAXBException, IOException {
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(deposit.getMetadataVersion());

    // Merge descriptive metadata
    for (final DepositDataFile df : deposit.getDataFiles()) {
      if (df.getDataCategory() == DataCategory.Package && df.getDataType() == DataCategory.Metadata) {
        this.logDepositMessage(LogLevel.INFO, deposit, "will complete Datacite metadata with datafile " + df.getFileName());
        metadataGenerator.completeDataciteMetadataFileWithDeposit(deposit, df);
        this.logDepositMessage(LogLevel.DEBUG, deposit, "Datacite metadata completed with datafile " + df.getFileName());
      }
    }

    // Load AIP
    final List<ArchivalInfoPackage> aips = new ArrayList<>();
    if (deposit.isCollection()) {
      for (final String aipId : deposit.getCollection()) {
        final ArchivalInfoPackage aip = this.aipRemoteService.findOne(aipId);
        aips.add(aip);
      }
    }

    // Generate METS metadata
    this.logDepositMessage(LogLevel.INFO, deposit, "will generate METS metadata");
    metadataGenerator.generateMetadata(deposit.getResId(), deposit.getTitle(), deposit.getCreationTime(), deposit.getUpdateTime(),
            ResourceName.SIP, ModuleName.PREINGEST, ResourceName.DEPOSIT, deposit.getDataFiles(), aips, outputLocation);
    this.logDepositMessage(LogLevel.DEBUG, deposit, "METS metadata generated");
  }

  // ** DIP(Order) **

  public void generateMetadata(Order order, String dipId, String orgUnitId, Access accessLevel, Path outputLocation)
          throws JAXBException, IOException {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(order.getMetadataVersion());
    // Generate descriptive metadata
    final List<DipDataFile> dataFiles = new ArrayList<>();
    final DipDataFile dipMetadata = new DipDataFile();
    dipMetadata.setDataCategory(DataCategory.Package);
    dipMetadata.setDataType(DataCategory.Metadata);
    dipMetadata.setFinalData(metadataGenerator.generateDefaultDataCite(order, dipId, orgUnitId, accessLevel,
            outputLocation.resolve(DLCMConstants.METADATA_FILE)));
    dipMetadata.setFileStatus(DataFileStatus.RECEIVED);
    dataFiles.add(dipMetadata);

    // Generate METS metadata
    metadataGenerator.generateMetadata(order.getResId(), order.getName(), order.getCreationTime(), order.getUpdateTime(), ResourceName.DIP,
            ModuleName.ACCESS, ResourceName.ORDER, dataFiles, order.getAipPackages(), outputLocation);
  }

  // ** SIP **

  public void updateMetadata(SubmissionInfoPackage sip, Path metadataFile) throws JAXBException, IOException {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(sip.getInfo().getMetadataVersion());
    metadataGenerator.updateMetadata(sip, metadataFile);
  }

  // ** AIP **

  public void updateMetadata(ArchivalInfoPackage aip, Path metadataFile, Path newMetadataFile, Path newPremisFile,
          List<FileInfoUpdate> fileInfoUpdates) throws JAXBException, IOException {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(aip.getInfo().getMetadataVersion());
    metadataGenerator.updateMetadata(aip, metadataFile, newMetadataFile, newPremisFile, fileInfoUpdates);
  }

  public void upgradeMetadata(ArchivalInfoPackage aip, Path metadataFile) throws JAXBException, IOException {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(aip.getInfo().getMetadataVersion());
    metadataGenerator.upgradeMetadata(aip, metadataFile);
  }

  public int updateComplianceLevel(ArchivalInfoPackage aip, Path metadataFile) throws JAXBException, IOException {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(aip.getInfo().getMetadataVersion());
    return metadataGenerator.updateCompliance(aip, metadataFile);
  }

  // ** Disposal **

  public void updateMetadataForDisposal(ArchivalInfoPackage aip, Path metadataFile) throws JAXBException, IOException {
    // Get metadata generator for the version
    final MetadataGenerator metadataGenerator = this.getMetadataGenerator(aip.getInfo().getMetadataVersion());
    metadataGenerator.updateMetadataForDisposal(aip, metadataFile);
  }

  // ********************
  // ** Misc. methods **
  // ********************
  public ClassPathResource getRepresentationInfoSchema(DLCMMetadataVersion version) {
    return new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + version.getRepresentationInfoSchema());
  }

  public ClassPathResource getResourceSchema(DLCMMetadataVersion version, AbstractDataFile<?, ?> df) {
    ClassPathResource xsd;
    if (df instanceof DipDataFile) {
      xsd = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + version.getMetsSchema());
    } else if (df instanceof AipDataFile && df.getDataType() != DataCategory.UpdatePackage) {
      xsd = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + version.getMetsSchema());
    } else if (df instanceof SipDataFile) {
      xsd = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + version.getMetsSchema());
    } else {
      if (df.getStatus() != null
              && (df.getStatus().equals(DataFileStatus.CHECK_COMPLIANCE)
                      || df.getStatus().equals(DataFileStatus.CHECK_COMPLIANCE_CLEANED))) {
        xsd = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + version.getMetsSchema());
      } else {
        xsd = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + version.getRepresentationInfoSchema());
      }
    }
    return xsd;
  }

  private List<URL> getDlcmSchema(DLCMMetadataVersion version, AbstractDataFile<?, ?> df) throws IOException {
    final List<URL> list = new ArrayList<>();
    final ClassPathResource xsd = this.getResourceSchema(version, df);
    list.add(xsd.getURL());
    // Add FITS schema
    if (!(df instanceof DepositDataFile)
            || (df.getStatus() != null
                    && (df.getStatus().equals(DataFileStatus.CHECK_COMPLIANCE)
                            || df.getStatus().equals(DataFileStatus.CHECK_COMPLIANCE_CLEANED)))) {
      list.add(new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/fits_output.xsd").getURL());
    }
    return list;
  }

  private MetadataGenerator getMetadataGenerator(DLCMMetadataVersion version) {
    return switch (version) {
      case V1_0, V1_1 -> new MetadataVersion1(
              version,
              this.dlcmProperties,
              this.repositoryDescription,
              this.messageService,
              this.archivePublicMetadataRemoteService,
              this.archivePrivateMetadataRemoteService,
              this.metadataTypeRemoteService,
              this.orgUnitRemoteService,
              this.licenseRemoteService,
              this.languageRemoteService,
              this.personRemoteService,
              this.preservationPolicyRemoteService,
              this.archiveTypeResourceService,
              this.fileFormatService,
              this.historyService,
              this.gitInfoProperties);
      case V2_0, V2_1 -> new MetadataVersion2(
              version,
              this.dlcmProperties,
              this.repositoryDescription,
              this.messageService,
              this.archivePublicMetadataRemoteService,
              this.archivePrivateMetadataRemoteService,
              this.metadataTypeRemoteService,
              this.orgUnitRemoteService,
              this.licenseRemoteService,
              this.languageRemoteService,
              this.personRemoteService,
              this.preservationPolicyRemoteService,
              this.archiveTypeResourceService,
              this.fileFormatService,
              this.historyService,
              this.gitInfoProperties);
      case V3_0, V3_1 -> new MetadataVersion3(
              version,
              this.dlcmProperties,
              this.repositoryDescription,
              this.messageService,
              this.archivePublicMetadataRemoteService,
              this.archivePrivateMetadataRemoteService,
              this.metadataTypeRemoteService,
              this.orgUnitRemoteService,
              this.licenseRemoteService,
              this.languageRemoteService,
              this.personRemoteService,
              this.preservationPolicyRemoteService,
              this.archiveTypeResourceService,
              this.fileFormatService,
              this.historyService,
              this.gitInfoProperties);
      case V4_0 -> new MetadataVersion4(
              version,
              this.dlcmProperties,
              this.repositoryDescription,
              this.messageService,
              this.archivePublicMetadataRemoteService,
              this.archivePrivateMetadataRemoteService,
              this.metadataTypeRemoteService,
              this.orgUnitRemoteService,
              this.licenseRemoteService,
              this.languageRemoteService,
              this.personRemoteService,
              this.preservationPolicyRemoteService,
              this.archiveTypeResourceService,
              this.fileFormatService,
              this.historyService,
              this.gitInfoProperties,
              this.depositRemoteService);
      default -> throw new SolidifyProcessingException(this.messageService.get("metadata.error.version", new Object[] { version }));
    };
  }

  private String getPackage(AbstractDataFile<?, ?> df) {
    if (df instanceof DipDataFile) {
      return ResourceName.DIP;
    } else if (df instanceof AipDataFile) {
      return ResourceName.AIP;
    } else if (df instanceof SipDataFile) {
      return ResourceName.SIP;
    }
    return ResourceName.DEPOSIT;
  }

}

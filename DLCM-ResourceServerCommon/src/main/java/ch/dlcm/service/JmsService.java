/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - JmsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.message.AccessRefreshMessage;
import ch.dlcm.message.AipCheckMessage;
import ch.dlcm.message.AipDownloadMessage;
import ch.dlcm.message.AipMessage;
import ch.dlcm.message.ArchiveStatisticMessage;
import ch.dlcm.message.DataFileMessage;
import ch.dlcm.message.DepositMessage;
import ch.dlcm.message.DipMessage;
import ch.dlcm.message.EmailMessage;
import ch.dlcm.message.JobMessage;
import ch.dlcm.message.NotificationMessage;
import ch.dlcm.message.OrderMessage;
import ch.dlcm.message.OrganizationalUnitMessage;
import ch.dlcm.message.SipMessage;
import ch.dlcm.message.cache.ArchiveReindexedMessage;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.DipDataFile;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.preingest.DepositDataFile;

@Service
public class JmsService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(JmsService.class);

  private final String aipDownloadQueue;
  private final String aipRefreshQueue;
  private final String aipFixityQueue;
  private final String aipQueue;
  private final String depositDataFileQueue;
  private final String sipDataFileQueue;
  private final String aipDataFileQueue;
  private final String dipDataFileQueue;
  private final String depositQueue;
  private final String dipQueue;
  private final String jobQueue;
  private final String orderQueue;
  private final String orgUnitQueue;
  private final String sipQueue;
  private final String archiveQueue;
  private final String emailsQueue;
  private final String cacheTopic;
  private final String archiveReindexedTopic;
  private final String notificationQueue;
  private JmsTemplate queueJmsTemplate;
  private JmsTemplate topicJmsTemplate;

  private final JsonSerializerMessagePostCreator jsonSerializerMessagePostCreator;

  public JmsService(JmsTemplate queueJmsTemplate, @Qualifier("topicJmsTemplate") JmsTemplate topicJmsTemplate, DLCMProperties dlcmProperties,
          MessageService messageService) {
    super(messageService, dlcmProperties);
    this.queueJmsTemplate = queueJmsTemplate;
    this.topicJmsTemplate = topicJmsTemplate;
    this.jsonSerializerMessagePostCreator = new JsonSerializerMessagePostCreator();
    this.aipDownloadQueue = dlcmProperties.getQueue().getAipDownload();
    this.aipRefreshQueue = dlcmProperties.getQueue().getAipRefresh();
    this.aipFixityQueue = dlcmProperties.getQueue().getAipFixity();
    this.aipQueue = dlcmProperties.getQueue().getAip();
    this.depositDataFileQueue = dlcmProperties.getQueue().getDepositDataFile();
    this.sipDataFileQueue = dlcmProperties.getQueue().getSipDataFile();
    this.aipDataFileQueue = dlcmProperties.getQueue().getAipDataFile();
    this.dipDataFileQueue = dlcmProperties.getQueue().getDipDataFile();
    this.depositQueue = dlcmProperties.getQueue().getDeposit();
    this.dipQueue = dlcmProperties.getQueue().getDip();
    this.jobQueue = dlcmProperties.getQueue().getJob();
    this.orderQueue = dlcmProperties.getQueue().getOrder();
    this.orgUnitQueue = dlcmProperties.getQueue().getOrgunit();
    this.sipQueue = dlcmProperties.getQueue().getSip();
    this.archiveQueue = dlcmProperties.getQueue().getArchive();
    this.notificationQueue = dlcmProperties.getQueue().getNotification();
    this.cacheTopic = dlcmProperties.getTopic().getCache();
    this.emailsQueue = dlcmProperties.getTopic().getEmails();
    this.archiveReindexedTopic = dlcmProperties.getTopic().getArchiveReindexed();
  }

  private void convertAndSendToQueue(String queue, Object event) {
    this.convertAndSendToQueue(queue, event, false);
  }

  private void convertAndSendToQueue(String queue, Object event, boolean withJsonSerialization) {
    if (log.isTraceEnabled()) {
      log.trace("Sending message to queue {} => {}", queue, event);
    }
    if (withJsonSerialization) {
      this.queueJmsTemplate.convertAndSend(queue, event, this.jsonSerializerMessagePostCreator);
    } else {
      this.queueJmsTemplate.convertAndSend(queue, event);
    }
  }

  private void convertAndSendToTopic(String topic, Object event) {
    this.convertAndSendToTopic(topic, event, false);
  }

  private void convertAndSendToTopic(String topic, Object event, boolean withJsonSerialization) {
    if (log.isTraceEnabled()) {
      log.trace("Sending message to topic {} => {}", topic, event);
    }
    if (withJsonSerialization) {
      this.topicJmsTemplate.convertAndSend(topic, event, this.jsonSerializerMessagePostCreator);
    } else {
      this.topicJmsTemplate.convertAndSend(topic, event);
    }
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(AipDownloadMessage event) {
    this.convertAndSendToQueue(this.aipDownloadQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(AccessRefreshMessage event) {
    this.convertAndSendToQueue(this.aipRefreshQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(AipCheckMessage event) {
    this.convertAndSendToQueue(this.aipFixityQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(AipMessage event) {
    this.convertAndSendToQueue(this.aipQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(CacheMessage event) {
    this.convertAndSendToTopic(this.cacheTopic, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(ArchiveReindexedMessage event) {
    this.convertAndSendToTopic(this.archiveReindexedTopic, event, true);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(DataFileMessage event) {
    Class<? extends AbstractDataFile> dataFileClass = event.getDataFileClass();
    if (dataFileClass.equals(DepositDataFile.class)) {
      this.convertAndSendToQueue(this.depositDataFileQueue, event);
    } else if (dataFileClass.equals(SipDataFile.class)) {
      this.convertAndSendToQueue(this.sipDataFileQueue, event);
    } else if (dataFileClass.equals(AipDataFile.class)) {
      this.convertAndSendToQueue(this.aipDataFileQueue, event);
    } else if (dataFileClass.equals(DipDataFile.class)) {
      this.convertAndSendToQueue(this.dipDataFileQueue, event);
    } else {
      throw new SolidifyRuntimeException("Unknown DataFile class " + dataFileClass);
    }
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(DepositMessage event) {
    this.convertAndSendToQueue(this.depositQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(DipMessage event) {
    this.convertAndSendToQueue(this.dipQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(JobMessage event) {
    this.convertAndSendToQueue(this.jobQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(OrderMessage event) {
    this.convertAndSendToQueue(this.orderQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(OrganizationalUnitMessage event) {
    this.convertAndSendToQueue(this.orgUnitQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(SipMessage event) {
    this.convertAndSendToQueue(this.sipQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(ArchiveStatisticMessage event) {
    this.convertAndSendToQueue(this.archiveQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(EmailMessage event) {
    this.convertAndSendToTopic(this.emailsQueue, event);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(NotificationMessage event) {
    this.convertAndSendToQueue(this.notificationQueue, event);
  }
}

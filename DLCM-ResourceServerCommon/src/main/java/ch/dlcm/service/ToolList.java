/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ToolList.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service
public class ToolList implements ApplicationContextAware {
  private static final Logger log = LoggerFactory.getLogger(ToolList.class);
  private final List<ToolService> toolServiceList = new ArrayList<>();

  public List<ToolService> getToolList() {
    return this.toolServiceList;
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) {

    final String[] toolList = applicationContext.getBeanNamesForType(ToolService.class);
    log.info("Tool found : {}", toolList.length);
    for (final String t : toolList) {
      log.info("Finding tool: {}", t);
      this.toolServiceList.add((ToolService) applicationContext.getBean(t));
    }
  }

}

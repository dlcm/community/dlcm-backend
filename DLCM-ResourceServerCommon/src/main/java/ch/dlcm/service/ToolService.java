/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ToolService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.io.IOException;
import java.io.InputStream;

import org.json.JSONObject;
import org.slf4j.Logger;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.Tool;
import ch.dlcm.model.ToolContainer;

public abstract class ToolService extends DLCMService {

  protected ToolService(MessageService messageService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);

  }

  // **************************
  // ** Methods to implement **
  // **************************

  public abstract JSONObject check();

  public abstract String getToolDescription();

  public abstract String getToolName();

  public abstract String getToolPuid();

  public abstract String getToolVersion();

  public void inError(Logger log, String message, AbstractDataFile<?, ?> dataFile) {
    log.error("{} for {}", message, dataFile.getFinalData());
    dataFile.setStatusMessage(message);
  }

  public abstract String runTool(AbstractDataFile<?, ?> file) throws IOException;

  // *********************
  // ** Default Methods **
  // *********************

  public abstract String runTool(AbstractDataFile<?, ?> file, InputStream inputStream);

  public void setTool(ToolContainer infoTool) {
    final Tool tool = new Tool();
    tool.setName(this.getToolName());
    tool.setDescription(this.getToolDescription());
    tool.setVersion(this.getToolVersion());
    tool.setPuid(this.getToolPuid());
    infoTool.setTool(tool);
  }

}

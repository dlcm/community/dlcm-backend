/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ObjectIdentifierComplexTypeWrapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.metadata.wrapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;

public class ObjectIdentifierComplexTypeWrapper {

  private final ch.dlcm.model.xml.dlcm.v1.mets.ObjectIdentifierComplexType objectIdentifierComplexTypeV1;
  private final ch.dlcm.model.xml.dlcm.v2.mets.ObjectIdentifierComplexType objectIdentifierComplexTypeV2;
  private final ch.dlcm.model.xml.dlcm.v3.mets.ObjectIdentifierComplexType objectIdentifierComplexTypeV3;
  private final ch.dlcm.model.xml.dlcm.v4.mets.ObjectIdentifierComplexType objectIdentifierComplexTypeV4;


  public ObjectIdentifierComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v1.mets.ObjectIdentifierComplexType objectIdentifierComplexType) {
    this.objectIdentifierComplexTypeV1 = objectIdentifierComplexType;
    this.objectIdentifierComplexTypeV2 = null;
    this.objectIdentifierComplexTypeV3 = null;
    this.objectIdentifierComplexTypeV4 = null;
  }

  public ObjectIdentifierComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v2.mets.ObjectIdentifierComplexType objectIdentifierComplexType) {
    this.objectIdentifierComplexTypeV1 = null;
    this.objectIdentifierComplexTypeV2 = objectIdentifierComplexType;
    this.objectIdentifierComplexTypeV3 = null;
    this.objectIdentifierComplexTypeV4 = null;
  }

  public ObjectIdentifierComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v3.mets.ObjectIdentifierComplexType objectIdentifierComplexType) {
    this.objectIdentifierComplexTypeV1 = null;
    this.objectIdentifierComplexTypeV2 = null;
    this.objectIdentifierComplexTypeV3 = objectIdentifierComplexType;
    this.objectIdentifierComplexTypeV4 = null;
  }

  public ObjectIdentifierComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v4.mets.ObjectIdentifierComplexType objectIdentifierComplexType) {
    this.objectIdentifierComplexTypeV1 = null;
    this.objectIdentifierComplexTypeV2 = null;
    this.objectIdentifierComplexTypeV3 = null;
    this.objectIdentifierComplexTypeV4 = objectIdentifierComplexType;
  }

  public StringPlusAuthorityWrapper getObjectIdentifierType() {
    if (this.objectIdentifierComplexTypeV1 != null) {
      return new StringPlusAuthorityWrapper(this.objectIdentifierComplexTypeV1.getObjectIdentifierType());
    }
    if (this.objectIdentifierComplexTypeV2 != null) {
      return new StringPlusAuthorityWrapper(this.objectIdentifierComplexTypeV2.getObjectIdentifierType());
    }
    if (this.objectIdentifierComplexTypeV3 != null) {
      return new StringPlusAuthorityWrapper(this.objectIdentifierComplexTypeV3.getObjectIdentifierType());
    }
    if (this.objectIdentifierComplexTypeV4 != null) {
      return new StringPlusAuthorityWrapper(this.objectIdentifierComplexTypeV4.getObjectIdentifierType());
    }
    throw new SolidifyRuntimeException("ObjectIdentifierComplexTypeWrapper object is empty");
  }

  public String getObjectIdentifierValue() {
    if (this.objectIdentifierComplexTypeV1 != null) {
      return this.objectIdentifierComplexTypeV1.getObjectIdentifierValue();
    }
    if (this.objectIdentifierComplexTypeV2 != null) {
      return this.objectIdentifierComplexTypeV2.getObjectIdentifierValue();
    }
    if (this.objectIdentifierComplexTypeV3 != null) {
      return this.objectIdentifierComplexTypeV3.getObjectIdentifierValue();
    }
    if (this.objectIdentifierComplexTypeV4 != null) {
      return this.objectIdentifierComplexTypeV4.getObjectIdentifierValue();
    }
    throw new SolidifyRuntimeException("ObjectIdentifierComplexTypeWrapper object is empty");
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - PremisFileWrapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.metadata.wrapper;

import java.util.ArrayList;
import java.util.List;

public class PremisFileWrapper extends ObjectComplexTypeWrapper {
  private final ch.dlcm.model.xml.dlcm.v1.mets.PremisFile premisFileV1;
  private final ch.dlcm.model.xml.dlcm.v2.mets.PremisFile premisFileV2;
  private final ch.dlcm.model.xml.dlcm.v3.mets.PremisFile premisFileV3;
  private final ch.dlcm.model.xml.dlcm.v4.mets.PremisFile premisFileV4;

  public PremisFileWrapper(ch.dlcm.model.xml.dlcm.v1.mets.PremisFile premisFile) {
    super(premisFile);
    this.premisFileV1 = premisFile;
    this.premisFileV2 = null;
    this.premisFileV3 = null;
    this.premisFileV4 = null;
  }

  public PremisFileWrapper(ch.dlcm.model.xml.dlcm.v2.mets.PremisFile premisFile) {
    super(premisFile);
    this.premisFileV1 = null;
    this.premisFileV2 = premisFile;
    this.premisFileV3 = null;
    this.premisFileV4 = null;
  }

  public PremisFileWrapper(ch.dlcm.model.xml.dlcm.v3.mets.PremisFile premisFile) {
    super(premisFile);
    this.premisFileV1 = null;
    this.premisFileV2 = null;
    this.premisFileV3 = premisFile;
    this.premisFileV4 = null;
  }

  public PremisFileWrapper(ch.dlcm.model.xml.dlcm.v4.mets.PremisFile premisFile) {
    super(premisFile);
    this.premisFileV1 = null;
    this.premisFileV2 = null;
    this.premisFileV3 = null;
    this.premisFileV4 = premisFile;
  }

  public List<ObjectIdentifierComplexTypeWrapper> getObjectIdentifier() {
    final List<ObjectIdentifierComplexTypeWrapper> objectIdentifierList = new ArrayList<>();
    if (this.premisFileV1 != null) {
      for (ch.dlcm.model.xml.dlcm.v1.mets.ObjectIdentifierComplexType objectIdentifier : this.premisFileV1.getObjectIdentifier()) {
        objectIdentifierList.add(new ObjectIdentifierComplexTypeWrapper(objectIdentifier));
      }
    }
    if (this.premisFileV2 != null) {
      for (ch.dlcm.model.xml.dlcm.v2.mets.ObjectIdentifierComplexType objectIdentifier : this.premisFileV2.getObjectIdentifier()) {
        objectIdentifierList.add(new ObjectIdentifierComplexTypeWrapper(objectIdentifier));
      }
    }
    if (this.premisFileV3 != null) {
      for (ch.dlcm.model.xml.dlcm.v3.mets.ObjectIdentifierComplexType objectIdentifier : this.premisFileV3.getObjectIdentifier()) {
        objectIdentifierList.add(new ObjectIdentifierComplexTypeWrapper(objectIdentifier));
      }
    }
    if (this.premisFileV4 != null) {
      for (ch.dlcm.model.xml.dlcm.v4.mets.ObjectIdentifierComplexType objectIdentifier : this.premisFileV4.getObjectIdentifier()) {
        objectIdentifierList.add(new ObjectIdentifierComplexTypeWrapper(objectIdentifier));
      }
    }
    return objectIdentifierList;
  }
}

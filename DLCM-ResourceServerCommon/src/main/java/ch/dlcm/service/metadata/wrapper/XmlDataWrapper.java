/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - XmlDataWrapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.metadata.wrapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;

public class XmlDataWrapper {

  private final ch.dlcm.model.xml.dlcm.v1.mets.XmlData xmlDataV1;
  private final ch.dlcm.model.xml.dlcm.v2.mets.XmlData xmlDataV2;
  private final ch.dlcm.model.xml.dlcm.v3.mets.XmlData xmlDataV3;
  private final ch.dlcm.model.xml.dlcm.v4.mets.XmlData xmlDataV4;

  public XmlDataWrapper(ch.dlcm.model.xml.dlcm.v1.mets.XmlData xmlData) {
    this.xmlDataV1 = xmlData;
    this.xmlDataV2 = null;
    this.xmlDataV3 = null;
    this.xmlDataV4 = null;
  }

  public XmlDataWrapper(ch.dlcm.model.xml.dlcm.v2.mets.XmlData xmlData) {
    this.xmlDataV1 = null;
    this.xmlDataV2 = xmlData;
    this.xmlDataV3 = null;
    this.xmlDataV4 = null;
  }

  public XmlDataWrapper(ch.dlcm.model.xml.dlcm.v3.mets.XmlData xmlData) {
    this.xmlDataV1 = null;
    this.xmlDataV2 = null;
    this.xmlDataV3 = xmlData;
    this.xmlDataV4 = null;
  }

  public XmlDataWrapper(ch.dlcm.model.xml.dlcm.v4.mets.XmlData xmlData) {
    this.xmlDataV1 = null;
    this.xmlDataV2 = null;
    this.xmlDataV3 = null;
    this.xmlDataV4 = xmlData;
  }

  public PremisComplexTypeWrapper getPremis() {
    if (this.xmlDataV1 != null) {
      return new PremisComplexTypeWrapper(this.xmlDataV1.getPremis());
    }
    if (this.xmlDataV2 != null) {
      return new PremisComplexTypeWrapper(this.xmlDataV2.getPremis());
    }
    if (this.xmlDataV3 != null) {
      return new PremisComplexTypeWrapper(this.xmlDataV3.getPremis());
    }
    if (this.xmlDataV4 != null) {
      return new PremisComplexTypeWrapper(this.xmlDataV4.getPremis());
    }
    throw new SolidifyRuntimeException("XmlData object is empty");
  }

}

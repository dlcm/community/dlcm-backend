/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - MdWrapWrapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.metadata.wrapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;

public class MdWrapWrapper {

  private final ch.dlcm.model.xml.dlcm.v1.mets.MdWrap mdWrapV1;
  private final ch.dlcm.model.xml.dlcm.v2.mets.MdWrap mdWrapV2;
  private final ch.dlcm.model.xml.dlcm.v3.mets.MdWrap mdWrapV3;
  private final ch.dlcm.model.xml.dlcm.v4.mets.MdWrap mdWrapV4;


  public MdWrapWrapper(ch.dlcm.model.xml.dlcm.v1.mets.MdWrap mdWrap) {
    this.mdWrapV1 = mdWrap;
    this.mdWrapV2 = null;
    this.mdWrapV3 = null;
    this.mdWrapV4 = null;
  }

  public MdWrapWrapper(ch.dlcm.model.xml.dlcm.v2.mets.MdWrap mdWrap) {
    this.mdWrapV1 = null;
    this.mdWrapV2 = mdWrap;
    this.mdWrapV3 = null;
    this.mdWrapV4 = null;
  }

  public MdWrapWrapper(ch.dlcm.model.xml.dlcm.v3.mets.MdWrap mdWrap) {
    this.mdWrapV1 = null;
    this.mdWrapV2 = null;
    this.mdWrapV3 = mdWrap;
    this.mdWrapV4 = null;
  }

  public MdWrapWrapper(ch.dlcm.model.xml.dlcm.v4.mets.MdWrap mdWrap) {
    this.mdWrapV1 = null;
    this.mdWrapV2 = null;
    this.mdWrapV3 = null;
    this.mdWrapV4 = mdWrap;
  }

  public XmlDataWrapper getXmlData() {
    if (this.mdWrapV1 != null) {
      return new XmlDataWrapper(this.mdWrapV1.getXmlData());
    }
    if (this.mdWrapV2 != null) {
      return new XmlDataWrapper(this.mdWrapV2.getXmlData());
    }
    if (this.mdWrapV3 != null) {
      return new XmlDataWrapper(this.mdWrapV3.getXmlData());
    }
    if (this.mdWrapV4 != null) {
      return new XmlDataWrapper(this.mdWrapV4.getXmlData());
    }
    throw new SolidifyRuntimeException("MdWrapWrapper object is empty");
  }
}

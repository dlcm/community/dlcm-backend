/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - StringPlusAuthorityWrapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.metadata.wrapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;

public class StringPlusAuthorityWrapper {
  private final ch.dlcm.model.xml.dlcm.v1.mets.StringPlusAuthority stringPlusAuthorityV1;
  private final ch.dlcm.model.xml.dlcm.v2.mets.StringPlusAuthority stringPlusAuthorityV2;
  private final ch.dlcm.model.xml.dlcm.v3.mets.StringPlusAuthority stringPlusAuthorityV3;
  private final ch.dlcm.model.xml.dlcm.v4.mets.StringPlusAuthority stringPlusAuthorityV4;

  public StringPlusAuthorityWrapper(ch.dlcm.model.xml.dlcm.v1.mets.StringPlusAuthority stringPlusAuthority) {
    this.stringPlusAuthorityV1 = stringPlusAuthority;
    this.stringPlusAuthorityV2 = null;
    this.stringPlusAuthorityV3 = null;
    this.stringPlusAuthorityV4 = null;
  }

  public StringPlusAuthorityWrapper(ch.dlcm.model.xml.dlcm.v2.mets.StringPlusAuthority stringPlusAuthority) {
    this.stringPlusAuthorityV1 = null;
    this.stringPlusAuthorityV2 = stringPlusAuthority;
    this.stringPlusAuthorityV3 = null;
    this.stringPlusAuthorityV4 = null;
  }

  public StringPlusAuthorityWrapper(ch.dlcm.model.xml.dlcm.v3.mets.StringPlusAuthority stringPlusAuthority) {
    this.stringPlusAuthorityV1 = null;
    this.stringPlusAuthorityV2 = null;
    this.stringPlusAuthorityV3 = stringPlusAuthority;
    this.stringPlusAuthorityV4 = null;
  }

  public StringPlusAuthorityWrapper(ch.dlcm.model.xml.dlcm.v4.mets.StringPlusAuthority stringPlusAuthority) {
    this.stringPlusAuthorityV1 = null;
    this.stringPlusAuthorityV2 = null;
    this.stringPlusAuthorityV3 = null;
    this.stringPlusAuthorityV4 = stringPlusAuthority;
  }

  public String getValue() {
    if (this.stringPlusAuthorityV1 != null) {
      return this.stringPlusAuthorityV1.getValue();
    }
    if (this.stringPlusAuthorityV2 != null) {
      return this.stringPlusAuthorityV2.getValue();
    }
    if (this.stringPlusAuthorityV3 != null) {
      return this.stringPlusAuthorityV3.getValue();
    }
    if (this.stringPlusAuthorityV4 != null) {
      return this.stringPlusAuthorityV4.getValue();
    }
    throw new SolidifyRuntimeException("StringPlusAuthorityWrapper object is empty");
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.metadata;

import static ch.dlcm.model.xml.dlcm.v4.mets.FileType.ARCHIVE_THUMBNAIL;
import static ch.dlcm.model.xml.dlcm.v4.mets.FileType.DATA_USE_AGREEMENT;
import static ch.dlcm.model.xml.dlcm.v4.mets.FileType.README;

import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.Language;
import ch.unige.solidify.model.xml.oai.v2.oai_dc.OaiDcType;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.HashTool;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ValidationTool;
import ch.unige.solidify.util.XMLTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.Access;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.FileFormat;
import ch.dlcm.model.FileInfoUpdate;
import ch.dlcm.model.IdentifierType;
import ch.dlcm.model.Package;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.Tool;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.display.OrgUnitPersonRoleListDTO;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.policies.PreservationPolicyInterface;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.License;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.model.xml.dlcm.v4.mets.Affiliation;
import ch.dlcm.model.xml.dlcm.v4.mets.Agent;
import ch.dlcm.model.xml.dlcm.v4.mets.AgentComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.AgentIdentifierComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.AmdSec;
import ch.dlcm.model.xml.dlcm.v4.mets.CompositionLevelComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.ContributorType;
import ch.dlcm.model.xml.dlcm.v4.mets.DataCategory;
import ch.dlcm.model.xml.dlcm.v4.mets.DataClassification;
import ch.dlcm.model.xml.dlcm.v4.mets.DateType;
import ch.dlcm.model.xml.dlcm.v4.mets.DescriptionType;
import ch.dlcm.model.xml.dlcm.v4.mets.DigiprovMD;
import ch.dlcm.model.xml.dlcm.v4.mets.Div;
import ch.dlcm.model.xml.dlcm.v4.mets.DlcmInfo;
import ch.dlcm.model.xml.dlcm.v4.mets.DmdSec;
import ch.dlcm.model.xml.dlcm.v4.mets.EventComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.EventDetailInformationComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.EventIdentifierComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.ExtensionComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.FLocat;
import ch.dlcm.model.xml.dlcm.v4.mets.File;
import ch.dlcm.model.xml.dlcm.v4.mets.FileGrp;
import ch.dlcm.model.xml.dlcm.v4.mets.FileSec;
import ch.dlcm.model.xml.dlcm.v4.mets.FixityComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.FormatComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.FormatDesignationComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.FormatRegistryComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.Fptr;
import ch.dlcm.model.xml.dlcm.v4.mets.FunderIdentifierType;
import ch.dlcm.model.xml.dlcm.v4.mets.IntellectualEntity;
import ch.dlcm.model.xml.dlcm.v4.mets.LinkingObjectIdentifierComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.MdRef;
import ch.dlcm.model.xml.dlcm.v4.mets.MdWrap;
import ch.dlcm.model.xml.dlcm.v4.mets.MetadataFile;
import ch.dlcm.model.xml.dlcm.v4.mets.Mets;
import ch.dlcm.model.xml.dlcm.v4.mets.MetsHdr;
import ch.dlcm.model.xml.dlcm.v4.mets.NameIdentifier;
import ch.dlcm.model.xml.dlcm.v4.mets.NameType;
import ch.dlcm.model.xml.dlcm.v4.mets.ObjectCharacteristicsComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.ObjectComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.ObjectIdentifierComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.OriginalNameComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.PremisComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.PremisFile;
import ch.dlcm.model.xml.dlcm.v4.mets.PreservationLevelComplexType;
import ch.dlcm.model.xml.dlcm.v4.mets.RelatedIdentifierType;
import ch.dlcm.model.xml.dlcm.v4.mets.RelationType;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.AlternateIdentifiers.AlternateIdentifier;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.Contributors.Contributor;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.Contributors.Contributor.ContributorName;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.Dates;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.FundingReferences.FundingReference;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.FundingReferences.FundingReference.FunderIdentifier;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.RelatedIdentifiers;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.RelatedIdentifiers.RelatedIdentifier;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.RightsList;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.Subjects;
import ch.dlcm.model.xml.dlcm.v4.mets.Resource.Subjects.Subject;
import ch.dlcm.model.xml.dlcm.v4.mets.ResourceType;
import ch.dlcm.model.xml.dlcm.v4.mets.StringPlusAuthority;
import ch.dlcm.model.xml.dlcm.v4.mets.StructMap;
import ch.dlcm.model.xml.dlcm.v4.mets.XmlData;
import ch.dlcm.model.xml.fits.Fits;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.metadata.wrapper.MdWrapWrapper;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchiveTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLanguageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLicenseRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackMetadataTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPersonRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPreservationPolicyRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;

public class MetadataVersion4 extends MetadataGenerator {
  private static final Logger log = LoggerFactory.getLogger(MetadataVersion4.class);

  private TrustedDepositRemoteResourceService depositRemoteService;

  public MetadataVersion4(
          DLCMMetadataVersion version,
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repositoryDescription,
          MessageService messageService,
          FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataResourceService,
          FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataResourceService,
          FallbackMetadataTypeRemoteResourceService metadataTypeService,
          FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService,
          FallbackLicenseRemoteResourceService licenseRemoteResourceService,
          FallbackLanguageRemoteResourceService languageResourceService,
          FallbackPersonRemoteResourceService personRemoteResourceService,
          FallbackPreservationPolicyRemoteResourceService preservationPolicyRemoteService,
          FallbackArchiveTypeRemoteResourceService archiveTypeResourceService,
          FileFormatService fileFormatService,
          HistoryService historyService,
          GitInfoProperties gitInfoProperties,
          TrustedDepositRemoteResourceService depositRemoteService) {
    super(version, dlcmProperties, repositoryDescription, messageService, archivePublicMetadataResourceService,
            archivePrivateMetadataResourceService,
            metadataTypeService, orgUnitResourceService, licenseRemoteResourceService, languageResourceService, personRemoteResourceService,
            preservationPolicyRemoteService, archiveTypeResourceService, fileFormatService, historyService, gitInfoProperties);

    this.depositRemoteService = depositRemoteService;

    // JAXB context
    try {
      this.jaxbContext = JAXBContext.newInstance(Mets.class, Resource.class, PremisComplexType.class, Fits.class);
    } catch (final JAXBException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  @Override
  public void checkMetadataFile(DLCMMetadataVersion version, Path metadata, String packageType) throws IOException, JAXBException {
    final Mets mets = this.loadMetadataFile(metadata);
    // Check Metadata version
    if (!mets.getPROFILE().contains(version.getVersion())) {
      throw new SolidifyCheckingException(
              this.messageService.get("checking.metadata.wrongversion", new Object[] { version.getVersion(), mets.getPROFILE() }));
    }
    // Check package Type
    if (!mets.getTYPE().equals(packageType)) {
      throw new SolidifyCheckingException(
              this.messageService.get("checking.metadata.wrongtype", new Object[] { mets.getTYPE(), packageType }));
    }
  }

  @Override
  public void checkDescriptionMetadataFile(Path metadata) throws IOException, JAXBException {
    this.loadDataciteMetadataFile(metadata);
  }

  @Override
  public void completeArchiveIndex(ArchiveMetadata archiveMetadata, Path xml) throws IOException, JAXBException {
    // Load xml file in object
    final Mets mets = this.loadMetadataFile(xml);
    // Metadata version
    archiveMetadata.getMetadata().put(DLCMConstants.METADATA_VERSION_FIELD, this.metadataVersion.getVersion());
    // Creation
    archiveMetadata.getMetadata().put(DLCMConstants.CREATION_FIELD,
            mets.getDmdSec().getMdWrap().getCREATED().toString());
    // DataCite
    final String datacite = this.getDataCiteMetadata(mets.getDmdSec().getMdWrap().getXmlData().getResource());
    archiveMetadata.getMetadata().put(DLCMConstants.DATACITE_XML_FIELD, datacite);
    archiveMetadata.getMetadata().put(DLCMConstants.DATACITE_FIELD,
            JSONTool.convertJson2Map(XMLTool.xml2Json(this.tranform4Indexing(datacite)).toString()));
    // Premis
    final String premis = this.getPremisMetadata(mets.getAmdSec().getDigiprovMD().getMdWrap().getXmlData().getPremis());
    archiveMetadata.getMetadata().put(DLCMConstants.PREMIS_FIELD, JSONTool.convertJson2Map(
            XMLTool.xml2Json(this.tranform4Indexing(premis)).toString()));
    // Custom Metadata
    if (!mets.getDmdSec().getMdRef().isEmpty()) {
      final String customMetadata = this.getCustomMetadata(mets.getDmdSec().getMdRef());
      archiveMetadata.getMetadata().put(DLCMConstants.CUSTOM_METADATA_FIELD, JSONTool.convertJson2Map(
              XMLTool.xml2Json(this.tranform4Indexing(customMetadata)).toString()));
    }
  }

  @Override
  public void completeDataciteMetadataFileWithDeposit(Deposit deposit, DepositDataFile df) throws JAXBException, IOException {
    Resource datacite = this.loadDataciteMetadataFile(Paths.get(df.getFinalData()));
    this.completeDataciteMetadataWithDeposit(datacite, deposit);
    try (FileWriter fileWriter = new FileWriter(Paths.get(df.getFinalData()).toFile())) {
      MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion).marshal(datacite, fileWriter);
    }
  }

  @Override
  public void completeDataciteMetadataWithDeposit(Deposit deposit, Path tmpFilePath) throws JAXBException, IOException {
    final Resource datacite = this.completeDataciteMetadataWithDeposit(new Resource(), deposit);

    /*
     * Save the xml on disk and add datafile to deposit datafiles list (to get the same situation as if
     * it were uploaded by user)
     */

    final Marshaller marchal = MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion);
    try (FileWriter fileWriter = new FileWriter(tmpFilePath.toFile())) {
      marchal.marshal(datacite, fileWriter);
    }
  }

  @Override
  public URI generateDefaultDataCite(Order order, String packageId, String orgUnitId, Access accessLevel, Path metadataFile)
          throws JAXBException, IOException {
    // Create DataCite metadata
    final Resource datacite = new Resource();
    // Identifier
    final Resource.Identifier identifier = new Resource.Identifier();
    identifier.setIdentifierType("DLCM");
    identifier.setValue(packageId);
    datacite.setIdentifier(identifier);
    // Create title
    datacite.setTitles(new Resource.Titles());
    final Resource.Titles.Title title = new Resource.Titles.Title();
    title.setValue(order.getName());
    datacite.getTitles().getTitle().add(title);
    // Create Description
    datacite.setDescriptions(new Resource.Descriptions());
    final Resource.Descriptions.Description desc = new Resource.Descriptions.Description();
    desc.setDescriptionType(DescriptionType.ABSTRACT);
    desc.getContent().add(this.getDetailedLabel(order.getQueryType().toString(), order.getQuery()));
    datacite.getDescriptions().getDescription().add(desc);
    datacite.setPublicationYear(Integer.toString(order.getCreationTime().getYear()));
    // Creator
    datacite.setCreators(new Resource.Creators());
    final Resource.Creators.Creator creator = new Resource.Creators.Creator();
    creator.setCreatorName(this.createPersonalName(order.getCreatedBy()));
    datacite.getCreators().getCreator().add(creator);
    // Publisher
    this.setPublisher(datacite);
    // resourceType
    final Resource.ResourceType resourceType = new Resource.ResourceType();
    resourceType.setValue(ResourceType.DATASET.toString());
    resourceType.setResourceTypeGeneral(ResourceType.DATASET);
    datacite.setResourceType(resourceType);
    // Creation date
    datacite.setDates(new Resource.Dates());
    final Resource.Dates.Date date = new Resource.Dates.Date();
    date.setDateType(DateType.CREATED);
    date.setValue(order.getCreationTime().format(DateTimeFormatter.ISO_DATE_TIME));
    datacite.getDates().getDate().add(date);
    // Format
    datacite.setFormats(new Resource.Formats());
    datacite.getFormats().getFormat().add(SolidifyConstants.ZIP_MIME_TYPE);
    // Right List
    final RightsList rightsList = new RightsList();
    final RightsList.Rights rights = new RightsList.Rights();
    rights.setValue(accessLevel.name());
    rights.setRightsURI(DLCMConstants.DLCM_URI_FINAL_ACCESS_LEVEL);
    rightsList.getRights().add(rights);
    datacite.setRightsList(rightsList);
    // OrgUnit
    this.completeDataCiteWithOrgUnit(datacite, orgUnitId);
    // AIPs
    final List<String> aipIdList = order.getAipPackages().stream().map(ResourceNormalized::getResId).toList();
    this.completeDataCiteHasPart(datacite, aipIdList);
    // Save DataCite metadata in file
    try (FileWriter fileWriter = new FileWriter(metadataFile.toFile())) {
      MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion).marshal(datacite, fileWriter);
    }
    return metadataFile.toUri();
  }

  @Override
  public <T extends AbstractDataFile<?, ?>> void generateMetadata(String resId, String name, OffsetDateTime creationTime,
          OffsetDateTime updateTime, String packageType, String module, String request, List<T> dataFiles, List<ArchivalInfoPackage> aips,
          Path outputLocation) throws JAXBException, IOException {
    // Create Mets object
    final Mets mets = this.createMets(resId, name, packageType);
    // Mets Header
    mets.setMetsHdr(this.createMetsHeader(creationTime, updateTime, module));
    // Create Descriptive Section
    mets.setDmdSec(this.createDescriptiveSection());
    // Create Administrative Section
    mets.setAmdSec(this.createAdministrativeSection());
    // Create File Section
    mets.setFileSec(this.createFileSection());
    // Create Structure Map
    mets.setStructMap(this.createStructureMap());
    // Create Premis object
    final PremisComplexType premis = this.createPremis(resId, name, request);
    // Process data files
    this.referenceDataFile(mets, premis, dataFiles, aips, outputLocation);
    // Create Digital Provenance
    mets.getAmdSec().setDigiprovMD(this.createDigitalProvenance(premis));
    // Update embedded metadata checksum
    this.updateChecksum(mets.getDmdSec().getMdWrap());
    this.updateChecksum(mets.getAmdSec().getDigiprovMD().getMdWrap());
    // Save in file
    this.saveXmlInFile(mets, outputLocation + "/" + DLCMConstants.METS_METADATA_FILE);
    // Rename metadata
    this.renameMetadataFile(outputLocation);
  }

  @Override
  public void updateMetadata(SubmissionInfoPackage sip, Path metadataFile) throws JAXBException, IOException {
    final Mets mets = this.loadMetadataFile(metadataFile);
    // Update Mets
    mets.setOBJID(sip.getAipId());
    mets.setTYPE(ResourceName.AIP);
    // Get Premis object
    final PremisComplexType premis = mets.getAmdSec().getDigiprovMD().getMdWrap().getXmlData().getPremis();
    // Add premis object for OrgUnit
    final OrganizationalUnit orgUnit = this.getOrganizationUnit(sip.getInfo().getOrganizationalUnitId());
    if (orgUnit != null) {
      premis.getObject().add(this.createPremisObject(orgUnit.getResId(), orgUnit.getName(), ResourceName.ORG_UNIT));
    } else {
      throw new SolidifyProcessingException("Cannot find organizational unit (" + sip.getInfo().getOrganizationalUnitId() + ")");
    }
    // Add premis object for SIP
    premis.getObject().add(this.createPremisObject(sip.getResId(), sip.getInfo().getName(), ResourceName.SIP));
    // Add premis object for AIP
    final IntellectualEntity premisAip = this
            .createPremisObject(sip.getAipId(), this.messageService.get("ingest.aip.name", new Object[] { sip.getInfo().getName() }),
                    ResourceName.AIP);
    premisAip.getPreservationLevel().add(this.createPremisPreservationLevel(sip));
    premis.getObject().add(premisAip);
    this.updateChecksum(mets.getAmdSec().getDigiprovMD().getMdWrap());
    // Save in file
    this.saveXmlInFile(mets, metadataFile.toString());
  }

  @Override
  public Object getDataCiteMetadata(String dataCiteString) throws JAXBException {
    return this.loadDataciteMetadata(dataCiteString);
  }

  @Override
  public JAXBElement<OaiDcType> getOaiDcMetadata(ArchiveMetadata archiveMetadata) throws JAXBException {
    final Resource dataCite = (Resource) this.getDataCiteMetadata(archiveMetadata.getOAIMetadata());
    final OaiDcType oaiDc = this.oaiDcFactory.createOaiDcType();
    // IDs
    if (dataCite.getIdentifier() != null) {
      oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createIdentifier(
              this.getOaiDcEntry(dataCite.getIdentifier().getIdentifierType().toLowerCase() + ":" + dataCite.getIdentifier().getValue(), null)));
    }
    // Titles
    for (final Resource.Titles.Title t : dataCite.getTitles().getTitle()) {
      oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createTitle(this.getOaiDcEntry(t.getValue(), t.getLang())));
    }
    // Descriptions
    for (final Resource.Descriptions.Description d : dataCite.getDescriptions().getDescription()) {
      oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createDescription(this.getOaiDcEntry(d.getContent().toString(), d.getLang())));
    }
    // Contributors
    if (dataCite.getContributors() != null) {
      for (final Resource.Contributors.Contributor c : dataCite.getContributors().getContributor()) {
        oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createCreator(this.getOaiDcEntry(c.getContributorName().getValue(), null)));
      }
    }
    // Creators
    for (final Resource.Creators.Creator c : dataCite.getCreators().getCreator()) {
      oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createCreator(this.getOaiDcEntry(c.getCreatorName().getValue(), null)));
    }
    // Subjects
    if (dataCite.getSubjects() != null) {
      for (final Resource.Subjects.Subject s : dataCite.getSubjects().getSubject()) {
        oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createSubject(this.getOaiDcEntry(s.getValue(), s.getLang())));
      }
    }
    // Rights
    for (final Resource.RightsList.Rights r : dataCite.getRightsList().getRights()) {
      oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createRights(this.getOaiDcEntry(r.getValue(), null)));
    }
    // Publisher
    oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createPublisher(this.getOaiDcEntry(dataCite.getPublisher().getValue(), null)));

    // Formats
    for (final String f : dataCite.getFormats().getFormat()) {
      oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createFormat(this.getOaiDcEntry(f, null)));
    }
    // Dates
    for (final Resource.Dates.Date d : dataCite.getDates().getDate()) {
      oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createDate(this.getOaiDcEntry(d.getValue(), null)));
    }
    // Language
    oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createLanguage(this.getOaiDcEntry(dataCite.getLanguage(), null)));
    // Types
    final QName qName = new QName(OAIConstants.OAI_DC_NAMESPACE, OAIConstants.OAI_DC);
    return new JAXBElement<>(qName, OaiDcType.class, oaiDc);
  }

  @Override
  public void updateMetadataForDisposal(ArchivalInfoPackage aip, Path metadataFile) throws IOException, JAXBException {
    // Load Mets metadata from file
    final Mets mets = this.loadMetadataFile(metadataFile);
    // Get Premis object
    final PremisComplexType premis = mets.getAmdSec().getDigiprovMD().getMdWrap().getXmlData().getPremis();
    final IntellectualEntity premisAip = this.findObject(premis.getObject(), ResourceName.AIP);
    // add history event
    this.addHistoryEvents(ResourceName.AIP, aip.getResId(), premis.getEvent(), premisAip.getObjectIdentifier());
    // Save in file
    this.updateChecksum(mets.getAmdSec().getDigiprovMD().getMdWrap());
    this.saveXmlInFile(mets, metadataFile.toString());
  }

  protected void addFileInfoUpdateIntoEventDetailInfo(ArchivalInfoPackage aip, List<FileInfoUpdate> fileInfoUpdates,
          List<EventComplexType> events) {
    // Check if the deposit is being updated
    if (this.historyService.hasBeenUpdatingMetadata(aip.getResId())) {
      // Get last premis event to add info from updates files
      log.debug("Add info from files updated in the editing metadata event");
      Optional<EventComplexType> lastEventOpt = events.stream()
              .max(Comparator.comparing(e -> Instant.parse(e.getEventDateTime())));
      if (lastEventOpt.isPresent()) {
        // Add an extension which each of the files that have been updated
        List<Object> metadataFiles = new ArrayList<>();
        for (FileInfoUpdate fileInfoUpdate : fileInfoUpdates) {

          MetadataFile metadataFile = new MetadataFile();
          switch (fileInfoUpdate.getFileType()) {
            case DLCMConstants.ARCHIVE_THUMBNAIL -> metadataFile.setType(ARCHIVE_THUMBNAIL);
            case DLCMConstants.ARCHIVE_DUA -> metadataFile.setType(DATA_USE_AGREEMENT);
            case DLCMConstants.ARCHIVE_README -> metadataFile.setType(README);
            default -> throw new SolidifyCheckingException("Wrong file type: " + fileInfoUpdate.getFileType());
          }
          metadataFile.setValue(fileInfoUpdate.getAction().toString());
          metadataFiles.add(metadataFile);

        }
        lastEventOpt.get().getEventDetailInformation().get(0).getEventDetailExtension()
                .add(this.createObjectExtension("dlcm", metadataFiles));
      }
    }
  }

  @Override
  public void updateMetadata(ArchivalInfoPackage aip, Path metadataFile, Path newMetadataFile, Path newPremisFile,
          List<FileInfoUpdate> fileInfoUpdates)
          throws IOException, JAXBException {
    // Load Mets metadata from file
    final Mets mets = this.loadMetadataFile(metadataFile);
    // Update Mets label
    mets.setLABEL(aip.getInfo().getName());
    // Update DataCite metadata
    if (newMetadataFile != null && FileTool.checkFile(newMetadataFile)) {
      // Save previous version
      Resource dataciteOldVersion = mets.getDmdSec().getMdWrap().getXmlData().getResource();
      this.saveXmlInFile(dataciteOldVersion, metadataFile.getParent().resolve(this.getMetadataVersionFileName(aip)).toString());
      // Add new version
      Resource dataciteNewVersion = this.loadDataciteMetadataFile(newMetadataFile);
      mets.getDmdSec().setMdWrap(this.createMetadataWrap(dataciteNewVersion, null, null, Collections.emptyList()));
      // Update DataCite metadata checksum
      this.updateChecksum(mets.getDmdSec().getMdWrap());
    }
    // Get Premis object
    PremisComplexType premis = mets.getAmdSec().getDigiprovMD().getMdWrap().getXmlData().getPremis();
    final IntellectualEntity premisAip = this.findObject(premis.getObject(), ResourceName.AIP);
    // Update preservation policy
    this.updatePreservationPolicy(aip, premisAip);
    // add history event
    this.addHistoryEvents(ResourceName.AIP, aip.getResId(), premis.getEvent(), premisAip.getObjectIdentifier());
    if (!fileInfoUpdates.isEmpty()) {
      // add premis info from the updated files
      premis = this.addPremisInfoFromUpdatedFiles(mets, premis, newPremisFile, fileInfoUpdates);
      // add file info update in premis event
      this.addFileInfoUpdateIntoEventDetailInfo(aip, fileInfoUpdates, premis.getEvent());
    }
    // Update PREMIS metadata checksum
    this.updateChecksum(mets.getAmdSec().getDigiprovMD().getMdWrap());
    // Update PREMIS objects for collection
    if (aip.isCollection()) {
      this.updatePremisObjectsForCollection(new MdWrapWrapper(mets.getAmdSec().getDigiprovMD().getMdWrap()), aip);
    }
    // Save in file
    this.saveXmlInFile(mets, metadataFile.toString());
  }

  @Override
  public void upgradeMetadata(ArchivalInfoPackage aip, Path metadataFile) throws IOException, JAXBException {
    // Load Mets metadata from file
    final Mets mets = this.loadMetadataFile(metadataFile);
    // Get Premis object
    final PremisComplexType premis = mets.getAmdSec().getDigiprovMD().getMdWrap().getXmlData().getPremis();
    final IntellectualEntity premisAip = this.findObject(premis.getObject(), ResourceName.AIP);
    // add history event
    this.addHistoryEvents(ResourceName.AIP, aip.getResId(), premis.getEvent(), premisAip.getObjectIdentifier());
    // Update compliance level
    this.updateComplianceLevel(premis);
    // Update PREMIS metadata checksum
    this.updateChecksum(mets.getAmdSec().getDigiprovMD().getMdWrap());
    // Update PREMIS objects for collection
    if (aip.isCollection()) {
      this.updatePremisObjectsForCollection(new MdWrapWrapper(mets.getAmdSec().getDigiprovMD().getMdWrap()), aip);
    }
    // Save in file
    this.saveXmlInFile(mets, metadataFile.toString());
  }

  @Override
  public int updateCompliance(ArchivalInfoPackage aip, Path metadataFile) throws IOException, JAXBException {
    int count = 0;
    // Load Mets metadata from file
    final Mets mets = this.loadMetadataFile(metadataFile);
    // Get Premis object
    final PremisComplexType premis = mets.getAmdSec().getDigiprovMD().getMdWrap().getXmlData().getPremis();
    // Update compliance level
    if ((count = this.updateComplianceLevel(premis)) == 0) {
      return count;
    }
    final IntellectualEntity premisAip = this.findObject(premis.getObject(), ResourceName.AIP);
    // add history event
    this.addHistoryEvents(ResourceName.AIP, aip.getResId(), premis.getEvent(), premisAip.getObjectIdentifier());
    // Update PREMIS metadata checksum
    this.updateChecksum(mets.getAmdSec().getDigiprovMD().getMdWrap());
    // Update PREMIS objects for collection
    if (aip.isCollection()) {
      this.updatePremisObjectsForCollection(new MdWrapWrapper(mets.getAmdSec().getDigiprovMD().getMdWrap()), aip);
    }
    // Save in file
    this.saveXmlInFile(mets, metadataFile.toString());
    return count;
  }

  private int updateComplianceLevel(PremisComplexType premis) throws JAXBException {
    int count = 0;
    // Check if file identifiacation tool is compatible
    if (this.checkIfFileFormatToolCompatible(premis)) {
      // Get deposit ID
      final IntellectualEntity premisDeposit = this.findObject(premis.getObject(), ResourceName.DEPOSIT);
      // Create Deposit object
      final Deposit deposit = new Deposit();
      deposit.setResId(premisDeposit.getObjectIdentifier().get(0).getObjectIdentifierValue());
      // For each data file
      for (ObjectComplexType objectComplexType : premis.getObject().stream().filter(PremisFile.class::isInstance).toList()) {
        final PremisFile premisFile = (PremisFile) objectComplexType;
        Optional<ObjectIdentifierComplexType> fileId = this.getPremisObjectIdentifier(premisFile, this.getPackageType(ResourceName.DATAFILE));
        if (fileId.isPresent()) {
          // Create Deposit Data File object
          final DepositDataFile depositDataFile = new DepositDataFile();
          // Data File ID
          depositDataFile.setResId(fileId.get().getObjectIdentifierValue());
          // Data File deposit
          depositDataFile.setInfoPackage(deposit);
          // Data File size
          depositDataFile.setFileSize(premisFile.getObjectCharacteristics().get(0).getSize());
          // Get FITS & DLCM info
          final Optional<ExtensionComplexType> fitsExtension = this.getPremisObjectExtension(premisFile, Fits.class);
          final Optional<ExtensionComplexType> dlcmExtension = this.getPremisObjectExtension(premisFile, DlcmInfo.class);
          if (fitsExtension.isPresent() && dlcmExtension.isPresent()) {
            final Fits fitsInfo = (Fits) fitsExtension.get().getAny().get(0);
            final DlcmInfo dlcmInfo = (DlcmInfo) dlcmExtension.get().getAny().get(0);
            // Data File File Format info (FITS)
            depositDataFile.getFileFormat().setDetails(this.getFitsInfo(fitsInfo));
            final int originalComplianceLevel = dlcmInfo.getComplianceLevel();
            final int newComplianceLevel = this.fileFormatService.assessComplianceLevel(depositDataFile.getResId(), depositDataFile).value();
            // Change compliance level
            if (newComplianceLevel != originalComplianceLevel) {
              count++;
              dlcmInfo.setComplianceLevel(newComplianceLevel);
            }
            if (dlcmInfo.getDataClassification().getValue().equals("Unknown")) {
              count++;
              final DepositDataFile df = this.depositRemoteService.getDepositDataFile(deposit.getResId(), depositDataFile.getResId());
              dlcmInfo.setDataClassification(this.createDataClassification(df));
            }
          }
        }
      }
    }
    return count;
  }

  private boolean checkIfFileFormatToolCompatible(PremisComplexType premis) {
    for (final AgentComplexType agent : premis.getAgent()) {
      final AgentIdentifierComplexType agentIdentifier = agent.getAgentIdentifier().get(0);
      if (agentIdentifier.getAgentIdentifierType().getValue().equals(DLCMConstants.DLCM)
              && this.fileFormatService.getToolName().equals(agentIdentifier.getAgentIdentifierValue())) {
        if (!this.fileFormatService.getToolVersion().equals(agent.getAgentVersion())) {
          log.warn("File identification tool ({}) versions are different: {} vs {}", this.fileFormatService.getToolName(),
                  this.fileFormatService.getToolVersion(), agent.getAgentVersion());
        }
        return true;
      }
    }
    log.warn("File identification tool not compatible : {} - {}", this.fileFormatService.getToolName(), this.fileFormatService.getToolVersion());
    return false;
  }

  private PremisComplexType addPremisInfoFromUpdatedFiles(Mets mets, PremisComplexType premis,
          Path newPremisFile, List<FileInfoUpdate> fileInfoUpdates)
          throws JAXBException, IOException {
    // Set the next id available for adding files into structmap
    this.initMetsIdFromFileIdAlreadyUsed(mets);
    FileGrp fg = this.getParentGroup(mets.getFileSec().getFileGrp().get(0), DLCMConstants.INTERNAL_ROOT);

    // Read updated premis file and for each of the updated files, take premis object and added to the current premis
    final Mets metsFileToAdd = this.loadMetadataFile(newPremisFile);
    List<PremisFile> premisObjectList = metsFileToAdd.getAmdSec().getDigiprovMD().getMdWrap().getXmlData().getPremis().getObject().stream()
            .filter(PremisFile.class::isInstance).map(po -> (PremisFile) po).toList();

    for (FileInfoUpdate fileInfoUpdate : fileInfoUpdates) {
      log.debug("Add premis object of file {} of type {}", newPremisFile, fileInfoUpdate.getFileType());
      // Find in Premis file the premis object according to the type to add.
      Optional<PremisFile> premisFileToAddOpt = premisObjectList
              .stream()
              .filter(po -> po.getOriginalName().getValue()
                      .endsWith(fileInfoUpdate.getFileType().substring(fileInfoUpdate.getFileType().lastIndexOf("."))))
              .findFirst();
      if (premisFileToAddOpt.isPresent()) {
        final PremisFile premisFileToAdd = premisFileToAddOpt.get();
        // Find premis object of the same type of the updated file it's being added and change the name of the current reference file
        Optional<PremisFile> currentPremisObjectFromUpdatedFileTypeOpt = premis.getObject()
                .stream()
                .filter(PremisFile.class::isInstance)
                .map(po -> (PremisFile) po).filter(po -> po.getOriginalName().getValue().startsWith(fileInfoUpdate.getFileType()))
                .findFirst();

        if (currentPremisObjectFromUpdatedFileTypeOpt.isPresent()) {
          PremisFile currentPremisFileToUpdate = currentPremisObjectFromUpdatedFileTypeOpt.get();
          final String metsFileId = this.updateFitsInfo(currentPremisFileToUpdate, fileInfoUpdate);

          fg.getFile().stream()
                  .filter(f -> f.getID().equals(metsFileId))
                  .forEach(f -> {
                    String newHrefStr = f.getFLocat().getHref().substring(0, f.getFLocat().getHref().lastIndexOf("/"))
                            + "/" + fileInfoUpdate.getFileNameForPreviousVersion();
                    f.getFLocat().setHref(newHrefStr);
                  });
        }

        // Change the name file and fits filename from the updated file with the reference name according its type
        premisFileToAdd.getOriginalName().setValue(fileInfoUpdate.getFileType());
        // Find fits info to change the name of the file
        this.addPremisFileForNewFile(premisFileToAdd, fileInfoUpdate);

        // Need to add events related with new file. First we need to find the resId in the Premis objectIdentifierValue
        Optional<ObjectIdentifierComplexType> objectIdentifierOpt = this.getPremisObjectIdentifier(premisFileToAdd, ResourceName.DATAFILE);
        if (objectIdentifierOpt.isPresent()) {
          String resIdFileToAdd = objectIdentifierOpt.get().getObjectIdentifierValue();
          // Find events from this file (eventIdentifierValue should be equal to the resIdFileToAdd)
          List<EventComplexType> eventListToAdd = metsFileToAdd.getAmdSec().getDigiprovMD().getMdWrap().getXmlData().getPremis().getEvent()
                  .stream()
                  .filter(e -> e.getEventIdentifier().getEventIdentifierValue().startsWith(resIdFileToAdd)).toList();
          premis.getEvent().addAll(eventListToAdd);
        }
        // Create a FileGroup if needed and then add the files in that group
        Div div = this.getParentDiv(mets.getStructMap().getDiv(), DLCMConstants.INTERNAL_ROOT);
        // Create a new file to add in the file group
        final File f = this.loadFileWithPredefinedIndex(DLCMConstants.INTERNAL_ROOT + "/" + fileInfoUpdate.getFileType());
        fg.getFile().add(f);
        // Add into Structure Map
        div.getFptr().add(this.getFprt(f));
        // Change the objectIdentifierValue of dlcm.mets of the new premis file with the corresponding ID that have been calculated to add into
        // filegroup tag
        premisFileToAdd.getObjectIdentifier().stream()
                .filter(co -> co.getObjectIdentifierType().getValue().endsWith(DLCMConstants.METS.toLowerCase()))
                .forEach(co -> co.setObjectIdentifierValue(f.getID()));

        premis.getObject().add(premisFileToAdd);
      }
    }
    // Update mets digiprovMD id
    mets.getAmdSec().getDigiprovMD().setID(this.getMetsId());
    return premis;
  }

  private void addPremisFileForNewFile(PremisFile premisFileToAdd, FileInfoUpdate fileInfoUpdate) {
    final Optional<ExtensionComplexType> extensionInfo = this.getPremisObjectExtension(premisFileToAdd, Fits.class);
    if (extensionInfo.isPresent()) {
      log.debug("Add fits info from updated file {} ", premisFileToAdd.getOriginalName());
      final Fits fitsInfo = (Fits) extensionInfo.get().getAny().get(0);
      // file path
      fitsInfo.getFileinfo().getFileInfoElements()
              .stream()
              .filter(fie -> fie.getName().getLocalPart().equals("filepath"))
              .forEach(fie -> {
                String contentFilePath = (String) fie.getValue().getContent().get(0);
                contentFilePath = contentFilePath.substring(0, contentFilePath.lastIndexOf("/"));
                fie.getValue().getContent().remove(0);
                fie.getValue().getContent().add(contentFilePath + "/" + fileInfoUpdate.getFileType());
              });
      // file name
      fitsInfo.getFileinfo().getFileInfoElements()
              .stream()
              .filter(fie -> fie.getName().getLocalPart().equals("filename"))
              .forEach(fie -> {
                fie.getValue().getContent().remove(0);
                fie.getValue().getContent().add(fileInfoUpdate.getFileType());
              });
    }
  }

  private Optional<ExtensionComplexType> getPremisObjectExtension(PremisFile premisFile, Class<?> clazz) {
    return premisFile.getObjectCharacteristics().get(0).getObjectCharacteristicsExtension()
            .stream()
            .filter(c -> clazz.isInstance(c.getAny().get(0)))
            .findFirst();
  }

  private String updateFitsInfo(PremisFile premisFile, FileInfoUpdate fileInfoUpdate) {
    log.debug("Change current premis object info {} ", premisFile.getOriginalName());

    // Change file name and the file name from fits
    premisFile.getOriginalName().setValue(fileInfoUpdate.getFileNameForPreviousVersion());
    // Find fits info to change the name of the file

    final Optional<ExtensionComplexType> extensionInfo = this.getPremisObjectExtension(premisFile, Fits.class);
    if (extensionInfo.isPresent()) {
      final Fits fitsInfo = (Fits) extensionInfo.get().getAny().get(0);

      fitsInfo.getFileinfo()
              .getFileInfoElements()
              .stream()
              .filter(fie -> fie.getName().getLocalPart().equals("filepath"))
              .forEach(fie -> {
                String contentFilePath = (String) fie.getValue().getContent().get(0);
                contentFilePath = contentFilePath.substring(0, contentFilePath.lastIndexOf("/"));
                fie.getValue().getContent().remove(0);
                fie.getValue().getContent().add(contentFilePath + "/" + fileInfoUpdate.getFileNameForPreviousVersion());
              });
      fitsInfo.getFileinfo()
              .getFileInfoElements()
              .stream()
              .filter(fie -> fie.getName().getLocalPart().equals("filename"))
              .forEach(fie -> {
                fie.getValue().getContent().remove(0);
                fie.getValue().getContent().add(fileInfoUpdate.getFileNameForPreviousVersion());
              });
    }

    Optional<ObjectIdentifierComplexType> objId = this.getPremisObjectIdentifier(premisFile, DLCMConstants.METS.toLowerCase());
    if (objId.isPresent()) {
      return objId.get().getObjectIdentifierValue();
    }
    throw new SolidifyCheckingException("Cannot find Id for PREMIS file " + premisFile.getXmlID());
  }

  private Optional<ObjectIdentifierComplexType> getPremisObjectIdentifier(PremisFile premisFile, String identifierType) {
    return premisFile.getObjectIdentifier()
            .stream()
            .filter(co -> co.getObjectIdentifierType().getValue().endsWith(identifierType))
            .findFirst();
  }

  private void initMetsIdFromFileIdAlreadyUsed(Mets mets) {
    Div div = mets.getStructMap().getDiv();
    int maxIndex = 0;
    for (Div divInside : div.getDiv()) {
      for (Fptr fptr : divInside.getFptr()) {
        if (Integer.parseInt(fptr.getFILEID().substring(this.idPrefix.length())) > maxIndex) {
          maxIndex = Integer.parseInt(fptr.getFILEID().substring(this.idPrefix.length()));
        }
      }
    }
    this.setMetsId(maxIndex);
  }

  @Override
  public Map<String, String> extractInfoFromMetadata(Path metadataFile) throws IOException, JAXBException {
    final Map<String, String> infoList = new HashMap<>();
    // Load Mets metadata from file
    final Mets mets = this.loadMetadataFile(metadataFile);
    final Resource datacite = mets.getDmdSec().getMdWrap().getXmlData().getResource();
    // Creation
    infoList.put(DLCMConstants.CREATION_FIELD, mets.getDmdSec().getMdWrap().getCREATED().toString());

    this.extractRightsFromMetadata(datacite, infoList);
    // No data tag
    infoList.computeIfAbsent(DLCMConstants.AIP_DATA_TAG, k -> DataTag.UNDEFINED.toString());
    // No dua type tag
    infoList.computeIfAbsent(DLCMConstants.AIP_DATA_USE_POLICY, k -> DataUsePolicy.NONE.toString());
    this.extractCollectionFromMetadata(datacite, infoList);
    this.extractComplianceLevel(mets, infoList);

    return infoList;
  }

  @Override
  public void extractDataciteMetadataFromFile(Path filePath, OutputStream outputStream) throws JAXBException, IOException {
    final Mets mets = this.loadMetadataFile(filePath);
    final Resource datacite = mets.getDmdSec().getMdWrap().getXmlData().getResource();
    MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion).marshal(datacite, outputStream);
  }

  @Override
  public void writeDataciteXmlFromIndexCompletedWithDeposit(Deposit deposit, OutputStream outputStream) throws JAXBException {
    log.trace("entering writeDataciteXmlFromIndexCompletedWithDeposit(): deposit={}, title={}", deposit, deposit.getTitle());
    final ArchiveMetadata archiveMetadata = this.getArchiveMetadataFromDeposit(deposit);
    log.trace("ArchiveMetadata: {}", archiveMetadata.getResId());
    final Resource datacite = this.loadDataciteMetadata(archiveMetadata.getOAIMetadata());
    log.trace("datacite loaded");
    this.completeDataciteMetadataWithDeposit(datacite, deposit);
    log.trace("datacite completed with deposit");
    MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion).marshal(datacite, outputStream);
    log.trace("datacite marshalled");
  }

  @Override
  public void writePremisXmlOfUpdatedFile(Deposit deposit, List<DepositDataFile> dataFiles, String outputLocation)
          throws JAXBException, IOException {
    // Create mets files
    final Mets mets = this.createMets(deposit.getResId(), deposit.getTitle(), ResourceName.DEPOSIT);
    mets.setAmdSec(this.createAdministrativeSection());
    // Create Premis entry
    final PremisComplexType premis = this.createPremis(deposit.getResId(), deposit.getTitle(), ResourceName.DEPOSIT);
    for (DepositDataFile dataFile : dataFiles) {
      // Add a premis file for each data file
      this.addPremisFile(premis, dataFile, DLCMConstants.TEMP_PREMIS_ID);
    }
    // Create Digital Provenance
    mets.getAmdSec().setDigiprovMD(this.createDigitalProvenance(premis));
    this.saveXmlInFile(mets, outputLocation);
  }

  private void updatePreservationPolicy(ArchivalInfoPackage aip, IntellectualEntity premisAip) {
    // Remove duplicate
    if (premisAip.getPreservationLevel().size() > 1) {
      final PreservationLevelComplexType policy = premisAip.getPreservationLevel().get(0);
      premisAip.getPreservationLevel()
              .removeIf(p -> p.getPreservationLevelValue().getValue().equals(policy.getPreservationLevelValue().getValue()));
      premisAip.getPreservationLevel().add(policy);
    }
    // Check if new policy
    long policyCount = premisAip.getPreservationLevel().stream()
            .filter(p -> p.getPreservationLevelValue().getValue().equals(aip.getRetentionDetail())).count();
    if (policyCount == 0) {
      // Add new preservation policy
      PreservationLevelComplexType preservationLevel = this.createPremisPreservationLevel(aip);
      preservationLevel.setPreservationLevelDateAssigned(this.useUTC(aip.getUpdateTime()).format(DateTimeFormatter.ISO_DATE_TIME));
      premisAip.getPreservationLevel().add(preservationLevel);
    }
  }

  private void extractComplianceLevel(Mets mets, Map<String, String> infoList) {
    // Compliance Level
    final List<ObjectComplexType> premisFiles = mets.getAmdSec().getDigiprovMD().getMdWrap().getXmlData().getPremis().getObject().stream()
            .filter(PremisFile.class::isInstance)
            .toList();
    ComplianceLevel complianceLevel = ComplianceLevel.FULL_COMPLIANCE;
    for (ObjectComplexType obj : premisFiles) {
      PremisFile premisFile = (PremisFile) obj;
      for (ObjectCharacteristicsComplexType o : premisFile.getObjectCharacteristics()) {
        for (ExtensionComplexType oe : o.getObjectCharacteristicsExtension()) {
          if (oe.getAny().get(0) instanceof DlcmInfo dlcmInfo) {
            ComplianceLevel fileComplianceLevel = ComplianceLevel.fromValue((dlcmInfo).getComplianceLevel());
            if (fileComplianceLevel.value() < complianceLevel.value()) {
              complianceLevel = fileComplianceLevel;
            }
          }

        }
      }
    }
    infoList.put(DLCMConstants.AIP_COMPLIANCE_LEVEL, Integer.toString(complianceLevel.value()));
  }

  private void extractCollectionFromMetadata(Resource datacite, Map<String, String> infoList) {
    // AIP RestCollection
    final StringBuilder aipListStringBuilder = new StringBuilder();
    for (RelatedIdentifier relatedIdentifier : datacite.getRelatedIdentifiers().getRelatedIdentifier()) {
      if (relatedIdentifier.getRelationType().equals(RelationType.HAS_PART)) {
        // Check if DOI
        String aipUnitId;
        if (relatedIdentifier.getRelatedIdentifierType() == RelatedIdentifierType.DOI) {
          aipUnitId = this.getArchiveMetadataByDoi(relatedIdentifier.getValue()).getResId();
        } else if (relatedIdentifier.getRelatedIdentifierType() == RelatedIdentifierType.HANDLE) {
          aipUnitId = relatedIdentifier.getValue();
        } else {
          throw new SolidifyCheckingException("Unknown relation : " + relatedIdentifier.getValue());
        }
        // Add the list
        if (aipListStringBuilder.length() == 0) {
          aipListStringBuilder.append(aipUnitId);
        } else {
          aipListStringBuilder
                  .append(SolidifyConstants.FIELD_SEP)
                  .append(aipUnitId);
        }
      }
    }
    if (aipListStringBuilder.length() != 0) {
      infoList.put(DLCMConstants.AIP_COLLECTION, aipListStringBuilder.toString());
    }
  }

  private void extractRightsFromMetadata(Resource datacite, Map<String, String> infoList) {
    // Rights
    for (RightsList.Rights rights : datacite.getRightsList().getRights()) {
      if (rights.getRightsURI().equals(DLCMConstants.DLCM_URI_FINAL_ACCESS_LEVEL)) {
        // Access Level
        infoList.put(DLCMConstants.AIP_ACCESS_LEVEL, rights.getValue());
      } else if (rights.getRightsURI().equals(DLCMConstants.DLCM_URI_EMBARGO_ACCESS_LEVEL)) {
        // Embargo level
        infoList.put(DLCMConstants.AIP_EMBARGO_LEVEL, rights.getValue());
      } else if (rights.getRightsURI().equals(DLCMConstants.DLCM_URI_DATA_TAG)) {
        // Data tag
        infoList.put(DLCMConstants.AIP_DATA_TAG, rights.getValue());
      } else if (rights.getRightsURI().equals(DLCMConstants.DLCM_URI_DATA_USE_POLICY)) {
        // Data tag
        infoList.put(DLCMConstants.AIP_DATA_USE_POLICY, rights.getValue());
      } else if (rights.getRightsIdentifierScheme().equals(DLCMConstants.SPDX)) {
        // Fair License
        infoList.put(DLCMConstants.AIP_LICENSE, rights.getRightsIdentifier());
      } else {
        // License
        infoList.put(DLCMConstants.AIP_LICENSE, rights.getValue());
      }
    }
  }

  private IntellectualEntity findObject(List<ObjectComplexType> objects, String packageType) {
    final List<ObjectComplexType> intellectualEntities = objects.stream().filter(
            IntellectualEntity.class::isInstance).toList();
    for (ObjectComplexType obj : intellectualEntities) {
      IntellectualEntity intellectualEntity = (IntellectualEntity) obj;
      if (intellectualEntity.getObjectIdentifier().get(0).getObjectIdentifierType().getValue().equals(this.getPackageType(packageType))) {
        return intellectualEntity;
      }
    }
    throw new SolidifyProcessingException("Cannot find object with type " + this.getPackageType(packageType));
  }

  private void setDataciteDate(Dates dates, DateType dateType, OffsetDateTime dateValue) {
    final Dates.Date date = new Dates.Date();
    date.setDateType(dateType);
    date.setValue(dateValue.format(DateTimeFormatter.ISO_DATE_TIME));
    // replace if it already exits
    Optional<Dates.Date> dateTypeOpt = dates.getDate().stream().filter(d -> d.getDateType().equals(dateType)).findFirst();
    if (dateTypeOpt.isPresent()) {
      dates.getDate().stream().filter(d -> d.getDateType().equals(dateType)).forEach(d -> d.setValue(date.getValue()));
    } else {
      dates.getDate().add(date);
    }
  }

  private void setDataciteDate(Dates dates, DateType dateType, OffsetDateTime dateValueBegin, OffsetDateTime dateValueEnd) {
    String dateRange;
    final Resource.Dates.Date date = new Resource.Dates.Date();
    date.setDateType(dateType);
    if (dateValueBegin != null) {
      dateRange = this.useUTC(dateValueBegin).format(DateTimeFormatter.ISO_DATE_TIME) + "/";
    } else {
      dateRange = "/";
    }
    if (dateValueEnd != null) {
      dateRange += this.useUTC(dateValueEnd).format(DateTimeFormatter.ISO_DATE_TIME);
    }
    date.setValue(dateRange);
    // replace if it already exits
    Optional<Resource.Dates.Date> dateTypeOpt = dates.getDate().stream().filter(d -> d.getDateType().equals(dateType)).findFirst();
    if (dateTypeOpt.isPresent()) {
      dates.getDate().stream().filter(d -> d.getDateType().equals(dateType)).forEach(d -> d.setValue(date.getValue()));
    } else {
      dates.getDate().add(date);
    }
  }

  private void removeDataciteDate(Dates dates, DateType dateType) {
    // remove if exits
    dates.getDate().removeIf(d -> d.getDateType().equals(dateType));
  }

  protected void addPremisFile(PremisComplexType premis, ArchivalInfoPackage aip) {
    // Create file object
    final ObjectComplexType f = this.createPremisFile(aip);
    premis.getObject().add(f);
  }

  private void addPremisFile(PremisComplexType premis, AbstractDataFile<?, ?> df, String metsId) throws JAXBException {
    // Create file object
    final ObjectComplexType f = this.createPremisFile(premis, df, metsId);
    premis.getObject().add(f);
    // Tool info
    if (df.getFileFormat() != null) {
      this.addPremisToolAgent(premis, df.getFileFormat().getTool());
    }
    if (df.getVirusCheck() != null) {
      this.addPremisToolAgent(premis, df.getVirusCheck().getTool());
    }
  }

  private void addPremisToolAgent(PremisComplexType premis, Tool tool) {
    for (final AgentComplexType agent : premis.getAgent()) {
      if (this.checkPremisAgent(agent, tool)) {
        return;
      }
    }
    premis.getAgent().add(this.createPremisAgent(tool.getName(), DLCMConstants.AGENT_SW, tool.getVersion()));
  }

  private void addSystemInfo(PremisComplexType premis) {
    final String osName = System.getProperty("os.name");
    String osVersion = System.getProperty("os.version");
    if (osName.contains(MetadataGenerator.WINDOWS)) {
      osVersion = "v" + osVersion;
    }
    premis.getAgent().add(this.createPremisAgent(osName, System.getProperty("os.arch"), osVersion));
  }

  private boolean checkIfRelationAlreadyInList(RelatedIdentifiers relations, String valueToSearch, RelationType relationType,
          RelatedIdentifierType relatedIdentifierType) {
    for (final RelatedIdentifiers.RelatedIdentifier relation : relations.getRelatedIdentifier()) {
      if (relation.getRelationType() == relationType &&
              relation.getRelatedIdentifierType() == relatedIdentifierType &&
              relation.getValue().equals(valueToSearch)) {
        return true;
      }
    }
    return false;
  }

  private boolean checkPremisAgent(AgentComplexType agent, Tool tool) {
    final AgentIdentifierComplexType id = agent.getAgentIdentifier().get(0);
    // Name
    if (!id.getAgentIdentifierValue().equals(tool.getName())) {
      return false;
    }
    // Type
    if (!agent.getAgentType().getValue().equals(DLCMConstants.AGENT_SW)) {
      return false;
    }

    // Version
    return agent.getAgentVersion().equals(tool.getVersion());
  }

  /**
   * Set XML Datacite <creators><creator>...</creator></creators> tags
   */
  private void completeDataciteCreators(Resource datacite, Deposit deposit) {
    if (!deposit.getContributorIds().isEmpty()) {

      /*
       * Ensure there is a <creators> tag
       */
      Resource.Creators creators = datacite.getCreators();
      if (creators == null) {
        creators = new Resource.Creators();
        datacite.setCreators(creators);
      }

      final List<Resource.Creators.Creator> existingCreators = creators.getCreator();

      /*
       * Clean list of creator before adding new ones
       */
      creators.getCreator().clear();

      /*
       * Add each contributors as creator if they are not already in creators list
       */
      for (final String contributorId : deposit.getContributorIds()) {

        final Person person = this.getPerson(contributorId);

        if (person != null && !this.personExistsInCreatorsList(person, existingCreators)) {
          creators.getCreator().add(this.createCreator(person));
        } else {
          throw new SolidifyCheckingException("Missing person");
        }
      }
    }
  }

  /**
   * Set XML Datacite <dates><date dateType="...">...</date></dates> tags
   */
  private void completeDataciteDates(Resource datacite, Deposit deposit) {
    /*
     * Ensure there is a <dates> tag
     */
    Resource.Dates dates = datacite.getDates();
    if (dates == null) {
      dates = new Resource.Dates();
      datacite.setDates(dates);
    }

    /*
     * Check what dates already exist
     */
    boolean creationDateExists = false;
    boolean acceptedDateExists = false;
    final List<Resource.Dates.Date> existingDates = dates.getDate();
    for (final Resource.Dates.Date existingDate : existingDates) {
      if (existingDate.getDateType() == DateType.CREATED) {
        creationDateExists = true;
      } else if (existingDate.getDateType() == DateType.ACCEPTED) {
        acceptedDateExists = true;
      }
    }

    /*
     * Set issued date
     */
    if (deposit.getPublicationDate() != null) {
      // TODO: add addDataCiteDate for LocalDate
      this.setDataciteDate(dates, DateType.ISSUED, deposit.getPublicationDate().atTime(OffsetTime.of(0, 0, 0, 0, ZoneOffset.UTC)));
    }

    /*
     * Set collected date
     */
    if (deposit.getCollectionBegin() != null || deposit.getCollectionEnd() != null) {
      this.setDataciteDate(dates, DateType.COLLECTED, deposit.getCollectionBegin(), deposit.getCollectionEnd());
    } else {
      this.removeDataciteDate(dates, DateType.COLLECTED);
    }

    /*
     * Set creation date
     */
    if (!creationDateExists) {
      this.setDataciteDate(dates, DateType.CREATED, this.useUTC(deposit.getCreationTime()));
    }

    /*
     * Update the updated date
     */
    this.setDataciteDate(dates, DateType.UPDATED, this.useUTC(deposit.getUpdateTime()));

    /*
     * Set validation date (= now)
     */
    if (!acceptedDateExists) {
      this.setDataciteDate(dates, DateType.ACCEPTED, this.useUTC(OffsetDateTime.now()));
    }

    /*
     * To indicate the end of an embargo period, use Available
     */
    if (deposit.hasEmbargo()) {
      this.setDataciteDate(dates, DateType.AVAILABLE, this.useUTC(deposit.getEmbargo().getEndDate()));
    } else {
      this.removeDataciteDate(dates, DateType.AVAILABLE);
    }
  }

  /**
   * Set XML Datacite <descriptions><description>...</description></descriptions> tags
   */
  private void completeDataciteDescription(Resource datacite, Deposit deposit) {
    // Do nothing if the deposit description is null or empty
    if (StringTool.isNullOrEmpty(deposit.getDescription())) {
      return;
    }

    // Create descriptions element if needed
    Resource.Descriptions descriptions = datacite.getDescriptions();
    if (descriptions == null) {
      descriptions = new Resource.Descriptions();
      datacite.setDescriptions(descriptions);
    }

    // Get the description of type ABSTRACT
    Resource.Descriptions.Description abstractDescription = null;
    final List<Resource.Descriptions.Description> descriptionList = descriptions.getDescription();
    for (final Resource.Descriptions.Description description : descriptionList) {
      if (DescriptionType.ABSTRACT.equals(description.getDescriptionType())) {
        abstractDescription = description;
        break;
      }
    }

    // Create the description of type ABSTRACT if it doesn't exist
    if (abstractDescription == null) {
      abstractDescription = new Resource.Descriptions.Description();
      abstractDescription.setDescriptionType(DescriptionType.ABSTRACT);
      descriptionList.add(abstractDescription);
    }

    // Set the content of the description of type ABSTRACT
    abstractDescription.getContent().clear();
    abstractDescription.getContent().add(deposit.getDescription());

    // Set description language
    final Language language = this.getLanguage(deposit);
    if (language != null) {
      abstractDescription.setLang(language.getCode());
    }
  }

  /**
   * Set XML Datacite <subjects>...</subjects> tags
   */
  private void completeDataciteSubject(Resource datacite, Deposit deposit) {
    Subjects subjects = datacite.getSubjects();
    // Keywords
    if (deposit.getKeywords() != null && !deposit.getKeywords().isEmpty()) {

      /*
       * First check that description with same value doesn't exist yet
       */
      boolean valueExists = false;

      if (subjects != null) {
        valueExists = (subjects.getSubject().stream().filter(s -> s.getSubjectScheme().equals(DLCMConstants.KEYWORDS)).count() == 1);
      } else {
        subjects = new Subjects();
        datacite.setSubjects(subjects);
      }

      Subject subject;
      if (!valueExists) {
        subject = new Subject();
        subject.setSubjectScheme(DLCMConstants.KEYWORDS);
        /*
         * Set description language
         */
        final Language language = this.getLanguage(deposit);
        if (language != null) {
          subject.setLang(language.getCode());
        }
        // Add subject
        subjects.getSubject().add(subject);
      } else {
        subject = subjects.getSubject().stream().filter(s -> s.getSubjectScheme().equals(DLCMConstants.KEYWORDS)).findFirst()
                .orElseThrow(() -> new SolidifyRuntimeException("No subject is present"));
      }
      // Update value
      subject.setValue(StringUtils.collectionToDelimitedString(deposit.getKeywords(), SolidifyConstants.FIELD_SEP));
    } else if (subjects != null) {
      subjects.getSubject().removeIf(s -> s.getSubjectScheme().equals(DLCMConstants.KEYWORDS));
    }
  }

  private void completeDataciteIdentifier(Resource datacite, Deposit deposit) {

    switch (this.defaultIdentifierType) {
      case DOI -> {
        this.setDataciteIdentifier(datacite, IdentifierType.DOI, deposit.getDoi());
        this.setDataciteAlternateIdentifier(datacite, IdentifierType.ARK, deposit.getArk());
      }
      case ARK -> {
        this.setDataciteIdentifier(datacite, IdentifierType.ARK, deposit.getArk());
        this.setDataciteAlternateIdentifier(datacite, IdentifierType.DOI, deposit.getDoi());
      }
      default -> throw new SolidifyProcessingException("Unexpected identifier type: " + this.defaultIdentifierType);
    }

  }

  private void setDataciteAlternateIdentifier(Resource datacite, IdentifierType idType, String id) {
    if (!StringTool.isNullOrEmpty(id)) {
      if (datacite.getAlternateIdentifiers() == null) {
        datacite.setAlternateIdentifiers(new Resource.AlternateIdentifiers());
      }
      for (AlternateIdentifier alternateId : datacite.getAlternateIdentifiers().getAlternateIdentifier()) {
        if (alternateId.getAlternateIdentifierType().equals(idType.getDescription())) {
          // Update alternate ID and return
          alternateId.setValue(id);
          return;
        }
      }
      // Add alternate ID
      final AlternateIdentifier identifier = new AlternateIdentifier();
      identifier.setAlternateIdentifierType(idType.getDescription());
      identifier.setValue(id);
      datacite.getAlternateIdentifiers().getAlternateIdentifier().add(identifier);
    }
  }

  private void setDataciteIdentifier(Resource datacite, IdentifierType idType, String id) {
    if (StringTool.isNullOrEmpty(id)) {
      throw new SolidifyProcessingException("Missing identifier for" + idType);
    }
    final Resource.Identifier identifier = new Resource.Identifier();
    identifier.setIdentifierType(idType.getDescription());
    identifier.setValue(id);
    datacite.setIdentifier(identifier);
  }

  /**
   * Set formats (= unique values of mimetypes of all datafiles)
   * <p>
   * Set XML Datacite <formats><format>...</format></formats> tags
   *
   * @param datacite
   * @param deposit
   */
  private void completeDataciteFormats(Resource datacite, Deposit deposit) {
    /*
     * Ensure there is a <formats> tag
     */
    Resource.Formats formats = datacite.getFormats();
    if (formats == null) {
      formats = new Resource.Formats();
      if (deposit.isCollection()) {
        formats.getFormat().add(SolidifyConstants.ZIP_MIME_TYPE);
      }
      datacite.setFormats(formats);
    }

    final ArrayList<String> mimetypes = new ArrayList<>();
    for (final DepositDataFile file : deposit.getDataFiles()) {
      final FileFormat format = file.getFileFormat();
      if (format != null) {
        final String mimetype = format.getContentType();
        if (!StringTool.isNullOrEmpty(mimetype) && !mimetypes.contains(mimetype)) {
          mimetypes.add(mimetype);
        }
      }
    }

    final List<String> existingFormats = formats.getFormat();
    for (final String mimetype : mimetypes) {
      if (!existingFormats.contains(mimetype)) {
        formats.getFormat().add(mimetype);
      }
    }
  }

  private void completeDataCiteHasPart(Resource datacite, List<String> aipIdList) {
    final RelatedIdentifiers relations = this.getDataCiteRelations(datacite);
    // Add relation for each AIPs
    for (final String aipId : aipIdList) {
      this.addAipInDataCite(relations, aipId);
    }
  }

  private void addAipInDataCite(RelatedIdentifiers relations, String aipId) {
    ArchiveMetadata aip = this.getArchiveMetadataById(aipId);
    if (aip.getIdentifierType().equals(RelatedIdentifierType.DOI.toString())) {
      if (!this.checkIfRelationAlreadyInList(relations, aip.getDoi(), RelationType.HAS_PART, RelatedIdentifierType.DOI)) {
        // Add DOI identifier
        relations.getRelatedIdentifier().add(this.createRelatedIdentifier(
                RelationType.HAS_PART, RelatedIdentifierType.DOI, aip.getDoi()));
      }
    } else if (aip.getIdentifierType().equals(RelatedIdentifierType.ARK.toString())) {
      if (!this.checkIfRelationAlreadyInList(relations, aip.getArk(), RelationType.HAS_PART, RelatedIdentifierType.ARK)) {
        // Add ARK identifier
        relations.getRelatedIdentifier().add(this.createRelatedIdentifier(
                RelationType.HAS_PART, RelatedIdentifierType.ARK, aip.getArk()));
      }
    } else {
      if (!this.checkIfRelationAlreadyInList(relations, aipId, RelationType.HAS_PART, RelatedIdentifierType.HANDLE)) {
        // Add DLCM identifier
        relations.getRelatedIdentifier().add(this.createRelatedIdentifier(
                RelationType.HAS_PART, RelatedIdentifierType.HANDLE, aipId));
      }
    }
  }

  private void completeDataCiteWithOrgUnit(Resource datacite, String orgUnitId) {
    final Resource.Contributors contributors = this.getDataCiteContributors(datacite);
    // OrgUnit ~ DataCite Research Group
    if (!this.checkIfContributorAlreadyInList(contributors, orgUnitId, ContributorType.RESEARCH_GROUP)) {
      OrganizationalUnit organizationalUnit = this.getOrganizationUnit(orgUnitId);
      String orgUnitDescription = this.getDetailedLabel(orgUnitId, organizationalUnit.getName());
      // OrgUnit affiliations
      final List<Affiliation> affiliations = new ArrayList<>();
      for (final Institution institution : this.getOrganizationUnitInstitutions(orgUnitId)) {
        affiliations.add(this.createDataciteAffiliation(institution));
      }
      contributors.getContributor().add(
              this.createContributor(
                      ContributorType.RESEARCH_GROUP,
                      this.createContributorName(NameType.ORGANIZATIONAL, orgUnitDescription),
                      affiliations));
    }

    // OrgUnit Managers ~ DataCite Project Managers
    for (OrgUnitPersonRoleListDTO member : this.getOrganizationUnitMembers(orgUnitId)) {
      if (member.getGrandChildren().get(0).getResId().equals(Role.MANAGER.getResId())
              && !this.checkIfContributorAlreadyInList(contributors, member.getFullName(), ContributorType.PROJECT_MANAGER)) {
        // Manager affiliations
        final List<Affiliation> affiliations = new ArrayList<>();
        for (final Institution institution : this.getPersonInstitutions(member.getResId())) {
          affiliations.add(this.createDataciteAffiliation(institution));
        }
        contributors.getContributor().add(
                this.createContributor(
                        ContributorType.PROJECT_MANAGER,
                        this.createContributorName(NameType.PERSONAL, member.getFullName()),
                        affiliations));
      }

    }

    // OrgUnit Funding Agencies ~ DataCite Funding Agencies
    final Resource.FundingReferences fundingReferences = this.getDataCiteFundingReferences(datacite);
    for (FundingAgency fundingAgency : this.getOrganizationUnitFundingAgencies(orgUnitId)) {
      if (!this.checkIfFundingReferenceAlreadyInList(fundingReferences, fundingAgency.getName())) {
        fundingReferences.getFundingReference().add(this.createFundingReference(fundingAgency));
      }
    }
  }

  private Resource.FundingReferences getDataCiteFundingReferences(Resource datacite) {
    Resource.FundingReferences fundingReferences = datacite.getFundingReferences();
    // Create object if needed
    if (fundingReferences == null) {
      fundingReferences = new Resource.FundingReferences();
      datacite.setFundingReferences(fundingReferences);
    }
    return fundingReferences;
  }

  private boolean checkIfFundingReferenceAlreadyInList(Resource.FundingReferences fundingReferences, String valueToSearch) {
    for (final Resource.FundingReferences.FundingReference fundingReference : fundingReferences.getFundingReference()) {
      if (fundingReference.getFunderName().contains(valueToSearch)) {
        return true;
      }
    }
    return false;
  }

  private Resource.FundingReferences.FundingReference createFundingReference(FundingAgency fundingAgency) {
    final FundingReference fundingReference = new Resource.FundingReferences.FundingReference();
    String fundingAgencyDescription = this.getDetailedLabel(fundingAgency.getAcronym(), fundingAgency.getName());
    fundingReference.setFunderName(fundingAgencyDescription);
    if (!StringTool.isNullOrEmpty(fundingAgency.getRorId())) {
      fundingReference.setFunderIdentifier(this.createFunderIdentifier(FunderIdentifierType.ROR, this.webUrls.getRor()
              + fundingAgency.getRorId()));
    } else if (fundingAgency.getUrl() != null) {
      fundingReference.setFunderIdentifier(this.createFunderIdentifier(FunderIdentifierType.OTHER, fundingAgency.getUrl().toString()));
    }
    return fundingReference;
  }

  private FunderIdentifier createFunderIdentifier(FunderIdentifierType funderIdentifierType, String funderIdentifierValue) {
    final FunderIdentifier funderIdentifier = new FunderIdentifier();
    funderIdentifier.setFunderIdentifierType(funderIdentifierType);
    funderIdentifier.setValue(funderIdentifierValue);
    return funderIdentifier;
  }

  private Resource.Contributors getDataCiteContributors(Resource datacite) {
    Resource.Contributors contributors = datacite.getContributors();
    // Create object if needed
    if (contributors == null) {
      contributors = new Resource.Contributors();
      datacite.setContributors(contributors);
    }
    return contributors;
  }

  private boolean checkIfContributorAlreadyInList(Resource.Contributors contributors, String valueToSearch, ContributorType contributorType) {
    for (final Contributor contributor : contributors.getContributor()) {
      if (contributor.getContributorType() == contributorType && contributor.getContributorName().getValue().contains(valueToSearch)) {
        return true;
      }
    }
    return false;
  }

  private Contributor createContributor(ContributorType contributorType, Resource.Contributors.Contributor.ContributorName contributorName,
          List<Affiliation> affiliations) {
    final Contributor contributor = new Resource.Contributors.Contributor();
    contributor.setContributorType(contributorType);
    contributor.setContributorName(contributorName);
    if (affiliations != null) {
      for (Affiliation affiliation : affiliations) {
        contributor.getAffiliation().add(affiliation);
      }
    }
    return contributor;
  }

  private ContributorName createContributorName(NameType nameType, String name) {
    final ContributorName contributorName = new ContributorName();
    contributorName.setNameType(nameType);
    contributorName.setValue(name);
    return contributorName;
  }

  private Resource completeDataciteMetadataWithDeposit(Resource datacite, Deposit deposit) {
    /*
     * Set title
     */
    this.completeDataciteTitle(datacite, deposit);

    /*
     * Set description
     */
    this.completeDataciteDescription(datacite, deposit);

    /*
     * Set subject
     */
    this.completeDataciteSubject(datacite, deposit);

    /*
     * Set Identifier (DOI-ARK) in DataCite XML
     */
    this.completeDataciteIdentifier(datacite, deposit);

    /*
     * Set publication year
     */
    this.completeDatacitePublicationYear(datacite, deposit);

    /*
     * Set creators
     */
    this.completeDataciteCreators(datacite, deposit);

    /*
     * Set publisher
     */
    this.setPublisher(datacite);

    /*
     * Set resourceType
     */
    this.completeDataciteResourceType(datacite, deposit);

    /*
     * Set rights
     */
    this.completeDataciteRights(datacite, deposit);

    /*
     * Set dates
     */
    this.completeDataciteDates(datacite, deposit);

    /*
     * Set formats (= unique values of mimetypes of all datafiles)
     */
    this.completeDataciteFormats(datacite, deposit);

    /*
     * If collection, generate 'IsPartOf' relation to OrgUnit
     */
    this.completeDataCiteWithOrgUnit(datacite, deposit.getOrganizationalUnitId());

    /*
     * If collection, generate 'HasPart' relation
     */
    if (deposit.isCollection()) {
      this.removeNotCustomHasPart(datacite);
      this.completeDataCiteHasPart(datacite, deposit.getCollection());
    }

    /*
     * Set identical, referenced by and obsolete by DOIs in DataCite XML
     */
    this.completeDataciteDoiRelations(datacite, deposit);

    return datacite;
  }

  private void removeNotCustomHasPart(Resource datacite) {
    // remove hasPart from Yareta (DOI (start with prefix (DLCMConfig))+ resId (verifed http status on
    // index))
    this.getDataCiteRelations(datacite).getRelatedIdentifier()
            .removeIf(this::relatedIdentifierToRemove);
  }

  private boolean relatedIdentifierToRemove(Resource.RelatedIdentifiers.RelatedIdentifier relatedIdentifier) {
    final boolean hasPartRelation = relatedIdentifier.getRelationType().equals(RelationType.HAS_PART);
    final boolean doiWithKnownPrefixRelation = relatedIdentifier.getRelatedIdentifierType().equals(RelatedIdentifierType.DOI) &&
            relatedIdentifier.getValue().startsWith(this.repositoryDescription.getDoiPrefix());
    final boolean handleRelation = relatedIdentifier.getRelatedIdentifierType().equals(RelatedIdentifierType.HANDLE);
    return hasPartRelation && (doiWithKnownPrefixRelation || handleRelation);
  }

  private void completeDataciteDoiRelations(Resource datacite, Deposit deposit) {
    final Resource.RelatedIdentifiers relatedIdentifiers = this.getDataCiteRelations(datacite);
    final List<Resource.RelatedIdentifiers.RelatedIdentifier> relatedIdentifierList = relatedIdentifiers.getRelatedIdentifier();

    // Remove relation types managed by deposit
    relatedIdentifiers.getRelatedIdentifier()
            .removeIf(relation -> relation.getRelationType().equals(RelationType.IS_IDENTICAL_TO)
                    || relation.getRelationType().equals(RelationType.IS_REFERENCED_BY)
                    || relation.getRelationType().equals(RelationType.IS_OBSOLETED_BY));

    this.addDoiRelation(relatedIdentifierList, deposit.getIsIdenticalTo(), RelationType.IS_IDENTICAL_TO);
    this.addDoiRelation(relatedIdentifierList, deposit.getIsObsoletedBy(), RelationType.IS_OBSOLETED_BY);
    for (String referecenByDoi : deposit.getIsReferencedBy()) {
      this.addDoiRelation(relatedIdentifierList, referecenByDoi, RelationType.IS_REFERENCED_BY);
    }
  }

  private void addDoiRelation(List<Resource.RelatedIdentifiers.RelatedIdentifier> relatedIdentifiers, String value, RelationType relationType) {
    if (!StringTool.isNullOrEmpty(value)) {
      if (!ValidationTool.isValidDOI(value)) {
        throw new SolidifyProcessingException(this.messageService.get("validation.deposit.doi.invalid",
                new Object[] { value }));
      }
      relatedIdentifiers.add(this.createRelatedIdentifier(relationType, RelatedIdentifierType.DOI, value));
    }
  }

  private void completeDataciteResourceType(Resource datacite, Deposit deposit) {
    final Resource.ResourceType resourceType = new Resource.ResourceType();
    resourceType.setValue(deposit.getArchiveType().getTypeName());
    resourceType.setResourceTypeGeneral(ResourceType.fromValue(deposit.getArchiveType().getMasterType().getTypeName()));
    datacite.setResourceType(resourceType);
  }

  /**
   * If deposit doesn't have any embargo, use the current year as publicationYear.
   * <p>
   * If deposit has an embargo, use the year of the embargo end date which will be the date of the final access level.
   * <p>
   * Set XML Datacite <publicationYear></publicationYear> tag
   */
  private void completeDatacitePublicationYear(Resource datacite, Deposit deposit) {
    if (deposit.hasEmbargo()) {
      datacite.setPublicationYear(String.valueOf(deposit.getEmbargo().getEndDate().getYear()));
    } else {
      datacite.setPublicationYear(String.valueOf(deposit.getPublicationDate().getYear()));
    }
  }

  /**
   * Set XML Datacite <rightsList><rights>...</rights></rightsList> tags
   */
  private void completeDataciteRights(Resource datacite, Deposit deposit) {

    /*
     * Ensure there is a <rightsList> tag
     */
    Resource.RightsList rightsList = datacite.getRightsList();
    if (rightsList == null) {
      rightsList = new Resource.RightsList();
      datacite.setRightsList(rightsList);
    }
    this.completeAccessLevel(datacite, deposit);
    this.completeEmbargo(datacite, deposit);
    this.completeLicense(datacite, deposit);
    this.completeDataTag(datacite, deposit);
    this.completeDUA(datacite, deposit);
  }

  private void completeDUA(Resource datacite, Deposit deposit) {
    // DUA Type
    if (deposit.getMetadataVersion().getVersionNumber() >= DLCMMetadataVersion.V4_0.getVersionNumber()
            && !deposit.getDataUsePolicy().equals(DataUsePolicy.NONE)) {
      for (final Resource.RightsList.Rights right : datacite.getRightsList().getRights()) {
        if (right.getRightsURI().equals(DLCMConstants.DLCM_URI_DATA_USE_POLICY)) {
          if (!right.getValue().equals(deposit.getDataUsePolicy().name())) {
            // Update it
            right.setValue(deposit.getDataUsePolicy().name());
          }
          return;
        }
      }
      final Resource.RightsList.Rights duaRights = new Resource.RightsList.Rights();
      duaRights.setValue(deposit.getDataUsePolicy().name());
      duaRights.setRightsURI(DLCMConstants.DLCM_URI_DATA_USE_POLICY);
      datacite.getRightsList().getRights().add(duaRights);
    }
  }

  private void completeDataTag(Resource datacite, Deposit deposit) {
    /*
     * Set Data Tag
     */
    if (!deposit.getDataSensitivity().equals(DataTag.UNDEFINED)) {
      for (final Resource.RightsList.Rights right : datacite.getRightsList().getRights()) {
        if (right.getRightsURI().equals(DLCMConstants.DLCM_URI_DATA_TAG)) {
          if (!right.getValue().equals(deposit.getDataSensitivity().name())) {
            // Update it
            right.setValue(deposit.getDataSensitivity().name());
          }
          return;
        }
      }
      final Resource.RightsList.Rights dataTagRights = new Resource.RightsList.Rights();
      dataTagRights.setValue(deposit.getDataSensitivity().name());
      dataTagRights.setRightsURI(DLCMConstants.DLCM_URI_DATA_TAG);
      datacite.getRightsList().getRights().add(dataTagRights);
    }
  }

  private void completeLicense(Resource datacite, Deposit deposit) {
    /*
     * Set License
     */
    if (!StringTool.isNullOrEmpty(deposit.getLicenseId())) {

      final License license = this.getLicense(deposit.getLicenseId());

      Optional<Resource.RightsList.Rights> licenceOpt = datacite.getRightsList().getRights().stream()
              .filter(r -> !DLCMConstants.getUriListWithoutLicense().contains(r.getRightsURI()))
              .findFirst();

      if (licenceOpt.isPresent()) {
        datacite.getRightsList().getRights().remove(licenceOpt.get());
      }

      final Resource.RightsList.Rights licenseRights = new Resource.RightsList.Rights();
      licenseRights.setRightsIdentifierScheme(DLCMConstants.SPDX);
      licenseRights.setRightsIdentifier(license.getOpenLicenseId());
      licenseRights.setRightsURI(license.getUrl().toString());
      licenseRights.setValue(license.getTitle());
      datacite.getRightsList().getRights().add(licenseRights);
    }

  }

  private void completeEmbargo(Resource datacite, Deposit deposit) {
    /*
     * Set eventual embargo Access Level
     */
    if (deposit.hasEmbargo()) {
      for (final Resource.RightsList.Rights right : datacite.getRightsList().getRights()) {
        if (right.getRightsURI().equals(DLCMConstants.DLCM_URI_EMBARGO_ACCESS_LEVEL)) {
          if (!right.getValue().equals(deposit.getEmbargo().getAccess().name())) {
            // Update it
            right.setValue(deposit.getEmbargo().getAccess().name());
          }
          return;
        }
      }
      final Resource.RightsList.Rights accessRights = new Resource.RightsList.Rights();
      accessRights.setValue(deposit.getEmbargo().getAccess().name());
      accessRights.setRightsURI(DLCMConstants.DLCM_URI_EMBARGO_ACCESS_LEVEL);
      datacite.getRightsList().getRights().add(accessRights);
    } else {
      // Remove embargo info
      datacite.getRightsList().getRights().removeIf(r -> r.getRightsURI().equals(DLCMConstants.DLCM_URI_EMBARGO_ACCESS_LEVEL));
    }
  }

  private void completeAccessLevel(Resource datacite, Deposit deposit) {
    /*
     * Set final Access level
     */
    if (deposit.getAccess() != null) {
      for (final Resource.RightsList.Rights right : datacite.getRightsList().getRights()) {
        if (right.getRightsURI().equals(DLCMConstants.DLCM_URI_FINAL_ACCESS_LEVEL)) {
          if (!right.getValue().equals(deposit.getAccess().name())) {
            // Update it and return
            right.setValue(deposit.getAccess().name());
          }
          return;
        }
      }

      final Resource.RightsList.Rights accessRights = new Resource.RightsList.Rights();
      accessRights.setValue(deposit.getAccess().name());
      accessRights.setRightsURI(DLCMConstants.DLCM_URI_FINAL_ACCESS_LEVEL);
      datacite.getRightsList().getRights().add(accessRights);
    }
  }

  private void completeDataciteTitle(Resource datacite, Deposit deposit) {
    // Do nothing if the deposit title is null or empty
    if (StringTool.isNullOrEmpty(deposit.getTitle())) {
      return;
    }

    // Create a titles element if needed
    Resource.Titles titles = datacite.getTitles();
    if (titles == null) {
      titles = new Resource.Titles();
      datacite.setTitles(titles);
    }

    // Get the main title which has no title type
    Resource.Titles.Title mainTitle = null;
    final List<Resource.Titles.Title> titleList = titles.getTitle();
    for (final Resource.Titles.Title title : titleList) {
      if (title.getTitleType() == null) {
        mainTitle = title;
        break;
      }
    }

    // Create the main title if it doesn't exist
    if (mainTitle == null) {
      mainTitle = new Resource.Titles.Title();
      titleList.add(mainTitle);
    }

    // Set the main title with the title coming from the deposit
    mainTitle.setValue(deposit.getTitle());
  }

  private AmdSec createAdministrativeSection() {
    final AmdSec amdSec = new AmdSec();
    amdSec.setID(this.getMetsId());
    return amdSec;
  }

  private Agent createAgent(String module) {
    // Mets Header Agent
    final Agent agent = new Agent();
    agent.setName(DLCMConstants.DLCM);
    agent.setROLE("CREATOR");
    agent.setTYPE("OTHER");
    agent.setOTHERTYPE("SOFTWARE");
    agent.setNote(module);
    return agent;
  }

  private Resource.Creators.Creator createCreator(Person person) {
    final Resource.Creators.Creator creator = new Resource.Creators.Creator();
    // Name
    creator.setCreatorName(this.createPersonalName(person.getFullName()));
    creator.setGivenName(person.getFirstName());
    creator.setFamilyName(person.getLastName());
    // ORCID
    if (!StringTool.isNullOrEmpty(person.getOrcid())) {
      final NameIdentifier orcid = new NameIdentifier();
      orcid.setSchemeURI(this.webUrls.getOrcid());
      orcid.setNameIdentifierScheme(DLCMConstants.ORCID);
      orcid.setValue(this.webUrls.getOrcid() + person.getOrcid());
      creator.getNameIdentifier().add(orcid);
    }
    // Institutions
    final List<Institution> institutions = this.getPersonInstitutions(person.getResId());
    if (!institutions.isEmpty()) {
      for (final Institution institution : institutions) {
        // Institution description
        creator.getAffiliation().add(this.createDataciteAffiliation(institution));
      }
    }
    return creator;
  }

  private Affiliation createDataciteAffiliation(Institution institution) {
    String institutionDescription = !StringTool.isNullOrEmpty(institution.getDescription())
            ? this.getDetailedLabel(institution.getName(), institution.getDescription())
            : institution.getName();
    final Affiliation affiliation = new Affiliation();
    affiliation.setValue(institutionDescription);
    if (!StringTool.isNullOrEmpty(institution.getRorId())) {
      affiliation.setAffiliationIdentifierScheme(DLCMConstants.ROR);
      affiliation.setAffiliationIdentifier(this.webUrls.getRor() + institution.getRorId());
    }
    return affiliation;
  }

  private MdRef createCustomMetadata(AbstractDataFile<?, ?> df) {

    final MdRef mdRef = new MdRef();
    mdRef.setID(this.getMetsId());
    mdRef.setCREATED(df.getCreationTime());
    mdRef.setMDTYPEVERSION(df.getFileName());
    mdRef.setMIMETYPE(df.getFileFormat().getContentType());
    if (df.getMetadataType() != null) {
      mdRef.setMDTYPE(df.getMetadataType().getFullName());
    }
    mdRef.setOTHERMDTYPE("Custom");
    mdRef.setSIZE(BigInteger.valueOf(df.getFileSize()));
    final DataFileChecksum checksum = this.getChecksum(df.getChecksums());
    mdRef.setCHECKSUMTYPE(checksum.getChecksumAlgo().toString());
    mdRef.setCHECKSUM(checksum.getChecksum());
    mdRef.setType(SIMPLE);
    mdRef.setHref(Package.METADATA.getName() + "/" + df.getFileName());
    return mdRef;
  }

  private DataClassification createDataClassification(AbstractDataFile<?, ?> df) {
    final DataClassification dataClassification = new DataClassification();
    // Category
    switch (df.getDataCategory()) {
      case Package -> dataClassification.setCategory(DataCategory.PACKAGE);
      case Primary -> dataClassification.setCategory(DataCategory.PRIMARY_DATA);
      case Secondary -> dataClassification.setCategory(DataCategory.SECONDARY_DATA);
      case Software -> dataClassification.setCategory(DataCategory.SOFTWARE);
      case Administrative -> dataClassification.setCategory(DataCategory.ADMINISTRATIVE_DATA);
      case Internal -> dataClassification.setCategory(DataCategory.INTERNAL);
      default -> dataClassification.setCategory(DataCategory.MISCELLANEOUS);
    }
    // Type
    dataClassification.setValue(df.getDataType().name());
    return dataClassification;
  }

  // *******************
  // ** METS Metadata **
  // *******************

  private DmdSec createDescriptiveSection() {
    final DmdSec dmdSec = new DmdSec();
    dmdSec.setID(this.getMetsId());
    return dmdSec;
  }

  private DigiprovMD createDigitalProvenance(PremisComplexType premis) {
    final DigiprovMD digiProv = new DigiprovMD();
    digiProv.setID(this.getMetsId());
    digiProv.setSTATUS("current");
    digiProv.setMdWrap(this.createMetadataWrap(premis));
    return digiProv;
  }

  private DlcmInfo createDlcmInfo(AbstractDataFile<?, ?> df) {
    final DlcmInfo dlcmInfo = new DlcmInfo();
    // Compliance Level
    dlcmInfo.setComplianceLevel(df.getComplianceLevel().value());
    // Data Category & type
    final DataClassification dataClassification = this.createDataClassification(df);
    dlcmInfo.setDataClassification(dataClassification);
    return dlcmInfo;
  }

  private FLocat createFileLocation(ArchivalInfoPackage aip) {
    final FLocat flocat = new FLocat();
    flocat.setID(this.getMetsId());
    flocat.setType(SIMPLE);
    flocat.setLOCTYPE("URL");
    if (!StringTool.isNullOrEmpty(aip.getArchiveId())) {
      flocat.setHref(aip.getArchiveId());
    } else {
      flocat.setHref(aip.getResId());
    }
    return flocat;
  }

  private FLocat createFileLocation(AbstractDataFile<?, ?> df, Path packagePath) {
    final FLocat flocat = new FLocat();
    flocat.setID(this.getMetsId());
    flocat.setType(SIMPLE);
    flocat.setLOCTYPE("URL");
    flocat.setHref(packagePath.relativize(Paths.get(df.getFinalData())).toString());
    return flocat;
  }

  private FLocat createFileLocationWithNextAvailableIndexcreateFileLocationWithNextAvailableIndex(String relativeFilePath) {
    final FLocat flocat = new FLocat();
    flocat.setID(this.getMetsId());
    flocat.setType(SIMPLE);
    flocat.setLOCTYPE("URL");
    flocat.setHref(relativeFilePath);
    return flocat;
  }

  private FileSec createFileSection() {
    final FileSec fileSec = new FileSec();
    fileSec.setID(this.getMetsId());
    return fileSec;
  }

  private MdWrap createMetadataWrap(AbstractDataFile<?, ?> df, Resource datacite) {
    return this.createMetadataWrap(datacite, df.getCreationTime(), df.getFileSize(), df.getChecksums());
  }

  private MdWrap createMetadataWrap(Resource datacite, OffsetDateTime creation, Long size, List<DataFileChecksum> checksums) {
    final MdWrap mdWrap = new MdWrap();
    mdWrap.setID(this.getMetsId());
    if (creation == null) {
      mdWrap.setCREATED(this.useUTC(OffsetDateTime.now()));
    } else {
      mdWrap.setCREATED(creation);
    }
    mdWrap.setMDTYPE("OTHER");
    mdWrap.setOTHERMDTYPE("DataCite");
    mdWrap.setMDTYPEVERSION("DataCite Metadata Schema 4.5 (" + this.metadataVersion.getRepresentationInfoSchema() + ")");
    mdWrap.setMIMETYPE(DLCMConstants.FORMAT_XML);
    if (size != null) {
      mdWrap.setSIZE(BigInteger.valueOf(size));
    } else {
      mdWrap.setSIZE(BigInteger.ZERO);
    }
    if (!checksums.isEmpty()) {
      final DataFileChecksum checksum = this.getChecksum(checksums);
      mdWrap.setCHECKSUMTYPE(checksum.getChecksumAlgo().toString());
      mdWrap.setCHECKSUM(checksum.getChecksum());
    } else {
      mdWrap.setCHECKSUM("ToCompute");
    }
    mdWrap.setXmlData(this.createXmlData(datacite));
    return mdWrap;
  }

  private MdWrap createMetadataWrap(PremisComplexType premis) {
    final MdWrap mdWrap = new MdWrap();
    mdWrap.setID(this.getMetsId());
    mdWrap.setCREATED(this.useUTC(OffsetDateTime.now()));
    mdWrap.setMDTYPE("PREMIS");
    mdWrap.setOTHERMDTYPE(this.metadataVersion.getAdministrativeInfoSchema());
    mdWrap.setMDTYPEVERSION("PREMIS version 3.0");
    mdWrap.setMIMETYPE(DLCMConstants.FORMAT_XML);
    mdWrap.setSIZE(BigInteger.ZERO);
    mdWrap.setCHECKSUMTYPE(this.defaultChecksumAlgo.toString());
    mdWrap.setCHECKSUM("ToCompute");
    mdWrap.setXmlData(this.createXmlData(premis));
    return mdWrap;
  }

  private Mets createMets(String resId, String name, String packageType) {
    final Mets mets = new Mets();
    this.resetMetsIds();
    // Set mets attributes
    mets.setOBJID(resId);
    mets.setLABEL(name);
    mets.setTYPE(packageType);
    mets.setPROFILE(this.metadataVersion.getMetsProfile());
    return mets;
  }

  private MetsHdr createMetsHeader(OffsetDateTime creationTime, OffsetDateTime updateTime, String module) {
    final MetsHdr metsHdr = new MetsHdr();
    metsHdr.setCREATEDATE(this.useUTC(creationTime));
    metsHdr.setLASTMODDATE(this.useUTC(updateTime));
    metsHdr.setAgent(this.createAgent(module));
    return metsHdr;
  }

  private ExtensionComplexType createObjectExtension(String name, Object obj) {
    final ExtensionComplexType compLvl = new ExtensionComplexType();
    if (obj instanceof String string) {
      final JAXBElement<String> jaxbElement = new JAXBElement<>(new QName(name), String.class, string);
      compLvl.getAny().add(jaxbElement);
    } else {
      compLvl.getAny().add(obj);
    }
    return compLvl;
  }

  private ExtensionComplexType createObjectExtension(String name, List<Object> objects) {
    final ExtensionComplexType compLvl = new ExtensionComplexType();
    for (Object obj : objects) {
      if (obj instanceof String string) {
        final JAXBElement<String> jaxbElement = new JAXBElement<>(new QName(name), String.class, string);
        compLvl.getAny().add(jaxbElement);
      } else {
        compLvl.getAny().add(obj);
      }
    }
    return compLvl;
  }

  private Resource.Creators.Creator.CreatorName createPersonalName(String name) {
    final Resource.Creators.Creator.CreatorName creatorName = new Resource.Creators.Creator.CreatorName();
    creatorName.setNameType(NameType.PERSONAL);
    creatorName.setValue(name);
    return creatorName;
  }

  // *********************
  // ** PREMIS Metadata **
  // *********************

  private PremisComplexType createPremis(String resId, String name, String packageType) {
    final PremisComplexType premis = new PremisComplexType();
    premis.setVersion("3.0");
    // OS info
    this.addSystemInfo(premis);
    // JRE info
    final AgentComplexType jre = this.createPremisAgent(System.getProperty("java.vendor"), DLCMConstants.AGENT_SW,
            System.getProperty("java.version"));
    jre.getAgentExtension().add(this.createObjectExtension("java.home", System.getProperty("java.home")));
    jre.getAgentExtension().add(this.createObjectExtension("java.class.path", System.getProperty("java.class.path")));
    premis.getAgent().add(jre);
    // DLCM info
    premis.getAgent().add(this.createPremisAgent(DLCMConstants.DLCM, DLCMConstants.AGENT_SW, this.gitInfoProperties.getBuild().getVersion()));
    // Deposit
    final IntellectualEntity premisDeposit = this.createPremisObject(resId, name, packageType);
    premis.getObject().add(premisDeposit);
    // Deposit History
    this.addHistoryEvents(packageType, resId, premis.getEvent(), premisDeposit.getObjectIdentifier());
    return premis;
  }

  private void addHistoryEvents(String packageType, String resId, List<EventComplexType> events, List<ObjectIdentifierComplexType> objectIds) {
    for (final StatusHistory h : this.getStatusHistoryList(resId, packageType)) {
      final EventComplexType e = this.createPremisEvent(packageType, h, objectIds);
      if (e != null) {
        events.add(e);
      }
    }
  }

  private AgentComplexType createPremisAgent(String name, String type, String version) {
    final AgentComplexType agent = new AgentComplexType();
    agent.getAgentIdentifier().add(this.createPremisAgentIdentifier(name));
    agent.setAgentType(this.creatPremisAuthority(type));
    if (version != null) {
      agent.setAgentVersion(version);
    }
    return agent;
  }

  private AgentIdentifierComplexType createPremisAgentIdentifier(String name) {
    final AgentIdentifierComplexType id = new AgentIdentifierComplexType();
    id.setAgentIdentifierType(this.creatPremisAuthority(DLCMConstants.DLCM));
    id.setAgentIdentifierValue(name);
    return id;
  }

  private EventComplexType createPremisEvent(String packageType, StatusHistory h, List<ObjectIdentifierComplexType> ids) {
    final String eventType = this.getPremisEventType(packageType, h.getStatus());
    if (eventType != null) {
      // Event Info
      final EventComplexType event = new EventComplexType();
      event.setEventIdentifier(this.createPremisEventIdentifier(h));
      event.setEventDateTime(this.useUTC(h.getChangeTime()).toString());
      event.setEventType(this.creatPremisAuthority(eventType));
      // Linked object
      final LinkingObjectIdentifierComplexType e = new LinkingObjectIdentifierComplexType();
      e.setLinkingObjectIdentifierType(ids.get(0).getObjectIdentifierType());
      e.setLinkingObjectIdentifierValue(ids.get(0).getObjectIdentifierValue());
      event.getLinkingObjectIdentifier().add(e);
      // Event detail
      event.getEventDetailInformation().add(this.createEventDetail(h.getDescription()));
      return event;
    }
    return null;
  }

  private EventDetailInformationComplexType createEventDetail(String description) {
    final EventDetailInformationComplexType eventDetail = new EventDetailInformationComplexType();
    eventDetail.setEventDetail(description);
    return eventDetail;
  }

  private EventIdentifierComplexType createPremisEventIdentifier(StatusHistory h) {
    final EventIdentifierComplexType id = new EventIdentifierComplexType();
    id.setEventIdentifierType(this.creatPremisAuthority("dlcm"));
    id.setEventIdentifierValue(h.getResId() + SolidifyConstants.ID_SEP + h.getSeq());
    return id;
  }

  private ObjectComplexType createPremisFile(ArchivalInfoPackage aip) {
    // File info
    final PremisFile f = new PremisFile();
    f.getObjectIdentifier().add(this.createPremisObjectIdentifier(aip.getResId(), ResourceName.AIP));
    final OriginalNameComplexType name = new OriginalNameComplexType();
    name.setValue(aip.getArchiveId());
    f.setOriginalName(name);
    f.getObjectCharacteristics().add(this.createPremisObjectCharacteristics(aip));
    return f;
  }

  private ObjectComplexType createPremisFile(PremisComplexType premis, AbstractDataFile<?, ?> df, String metsId) throws JAXBException {
    // File info
    final PremisFile f = new PremisFile();
    // Add dlcm uuid of data file
    f.getObjectIdentifier().add(this.createPremisObjectIdentifier(df.getResId(), ResourceName.DATAFILE));
    // Add mets id of data file
    final OriginalNameComplexType name = new OriginalNameComplexType();
    f.getObjectIdentifier().add(this.createPremisObjectIdentifier(metsId, DLCMConstants.METS));
    if (df.isInRoot()) {
      name.setValue(df.getFileName());
    } else {
      name.setValue(df.getRelativeLocation() + "/" + df.getFileName());
    }
    f.setOriginalName(name);
    f.getObjectCharacteristics().add(this.createPremisObjectCharacteristics(df));
    // File History
    this.addHistoryEvents(ResourceName.DATAFILE, df.getResId(), premis.getEvent(), f.getObjectIdentifier());
    return f;
  }

  private IntellectualEntity createPremisObject(String resId, String title, String packageType) {
    final IntellectualEntity obj = new IntellectualEntity();
    final OriginalNameComplexType name = new OriginalNameComplexType();
    name.setValue(title);
    obj.getObjectIdentifier().add(this.createPremisObjectIdentifier(resId, packageType));
    obj.setOriginalName(name);
    return obj;
  }

  private ObjectCharacteristicsComplexType createPremisObjectCharacteristics(ArchivalInfoPackage aip) {
    final ObjectCharacteristicsComplexType obj = new ObjectCharacteristicsComplexType();
    final CompositionLevelComplexType compLvl = new CompositionLevelComplexType();
    compLvl.setValue(BigInteger.ZERO);
    obj.setCompositionLevel(compLvl);
    // Size
    obj.setSize(aip.getArchiveSize());
    // Checksums
    for (final DataFileChecksum chk : aip.getChecksums()) {
      final FixityComplexType fixity = new FixityComplexType();
      fixity.setMessageDigest(chk.getChecksum());
      fixity.setMessageDigestAlgorithm(this.creatPremisAuthority(chk.getChecksumAlgo().toString()));
      fixity.setMessageDigestOriginator(this.creatPremisAuthority(chk.getChecksumOrigin().toString()));
      obj.getFixity().add(fixity);
    }
    // Format
    final FileFormat fileFormat = aip.getSmartFileFormat();
    final FormatComplexType fmt = new FormatComplexType();
    final FormatDesignationComplexType fmtDesignation = new FormatDesignationComplexType();
    fmtDesignation.setFormatName(this.creatPremisAuthority(fileFormat.getFormat()));
    fmtDesignation.setFormatVersion(fileFormat.getVersion());
    fmt.getContent().add(new JAXBElement<>(new QName(DLCMConstants.PREMIS_NAMESPACE_3, FORMAT_DESIGNATION), FormatDesignationComplexType.class,
            fmtDesignation));
    final FormatRegistryComplexType fmtRegistry = new FormatRegistryComplexType();
    fmtRegistry.setFormatRegistryName(this.creatPremisAuthority("PRONOM"));
    fmtRegistry.setFormatRegistryKey(this.creatPremisAuthority(fileFormat.getPuid()));
    fmt.getContent()
            .add(new JAXBElement<>(new QName(DLCMConstants.PREMIS_NAMESPACE_3, FORMAT_REGISTRY), FormatRegistryComplexType.class, fmtRegistry));

    obj.getFormat().add(fmt);
    return obj;
  }

  private ObjectCharacteristicsComplexType createPremisObjectCharacteristics(AbstractDataFile<?, ?> df) throws JAXBException {
    final ObjectCharacteristicsComplexType obj = new ObjectCharacteristicsComplexType();
    final CompositionLevelComplexType compLvl = new CompositionLevelComplexType();
    compLvl.setValue(BigInteger.ZERO);
    obj.setCompositionLevel(compLvl);
    // Size
    obj.setSize(df.getFileSize());
    // Checksums
    for (final DataFileChecksum checksumObject : df.getChecksums()) {
      DataFileChecksum checksum = checksumObject;
      final FixityComplexType fixity = new FixityComplexType();
      fixity.setMessageDigest(checksum.getChecksum());
      fixity.setMessageDigestAlgorithm(this.creatPremisAuthority(checksum.getChecksumAlgo().toString()));
      fixity.setMessageDigestOriginator(this.creatPremisAuthority(checksum.getChecksumOrigin().toString()));
      obj.getFixity().add(fixity);
    }
    // Format
    final FormatComplexType fmt = new FormatComplexType();
    final FormatDesignationComplexType fmtDesignation = new FormatDesignationComplexType();
    fmtDesignation.setFormatName(this.creatPremisAuthority(df.getFileFormat().getFormat()));
    fmtDesignation.setFormatVersion(df.getFileFormat().getVersion());
    fmt.getContent().add(new JAXBElement<>(new QName(DLCMConstants.PREMIS_NAMESPACE_3, FORMAT_DESIGNATION), FormatDesignationComplexType.class,
            fmtDesignation));
    final FormatRegistryComplexType fmtRegistry = new FormatRegistryComplexType();
    fmtRegistry.setFormatRegistryName(this.creatPremisAuthority("PRONOM"));
    fmtRegistry.setFormatRegistryKey(this.creatPremisAuthority(df.getFileFormat().getPuid()));
    fmt.getContent()
            .add(new JAXBElement<>(new QName(DLCMConstants.PREMIS_NAMESPACE_3, FORMAT_REGISTRY), FormatRegistryComplexType.class, fmtRegistry));

    obj.getFormat().add(fmt);
    // Compliance level
    obj.getObjectCharacteristicsExtension().add(this.createObjectExtension("dlcm", this.createDlcmInfo(df)));
    // Add File format detection output
    if (df.getFileFormat().getDetails() != null) {
      obj.getObjectCharacteristicsExtension().add(this.createObjectExtension("fits", df.getFileFormat().getSmartDetails(this.jaxbContext)));

    }

    // IANA Format
    if (df.getInfoPackage().getMetadataVersion().getVersionNumber() >= DLCMMetadataVersion.V4_0.getVersionNumber()) {
      final FormatComplexType ianaFmt = new FormatComplexType();
      final FormatRegistryComplexType ianaFmtRegistry = new FormatRegistryComplexType();
      ianaFmtRegistry.setFormatRegistryName(this.creatPremisAuthority("IANA"));
      ianaFmtRegistry.setFormatRegistryKey(this.creatPremisAuthority(df.getFileFormat().getContentType()));
      ianaFmt.getContent()
              .add(new JAXBElement<>(new QName(DLCMConstants.PREMIS_NAMESPACE_3, FORMAT_REGISTRY), FormatRegistryComplexType.class,
                      ianaFmtRegistry));
      obj.getFormat().add(ianaFmt);
    }
    return obj;
  }

  private ObjectIdentifierComplexType createPremisObjectIdentifier(String resId, String packageType) {
    final ObjectIdentifierComplexType id = new ObjectIdentifierComplexType();
    id.setObjectIdentifierType(this.creatPremisAuthority(this.getPackageType(packageType)));
    id.setObjectIdentifierValue(resId);
    return id;
  }

  private PreservationLevelComplexType createPremisPreservationLevel(PreservationPolicyInterface policy) {
    final PreservationLevelComplexType presLevel = new PreservationLevelComplexType();
    presLevel.setPreservationLevelType(this.creatPremisAuthority(DLCMConstants.POLICY));
    presLevel.setPreservationLevelValue(this.creatPremisAuthority(policy.getRetentionDetail()));
    return presLevel;
  }

  private StructMap createStructureMap() {
    final StructMap structMap = new StructMap();
    structMap.setID(this.getMetsId());
    structMap.setLABEL(DLCMConstants.STRUCTURE_MAP);
    structMap.setTYPE("physical");
    final Div div = new Div();
    div.setID(this.getMetsId());
    div.setLABEL("root");
    // TODO To check scanFolder(outputLocation for Files::isDirectory))
    structMap.setDiv(div);
    return structMap;
  }

  private XmlData createXmlData(PremisComplexType premis) {
    final XmlData xmlData = new XmlData();
    xmlData.setPremis(premis);
    return xmlData;
  }

  private XmlData createXmlData(Resource datacite) {
    final XmlData xmlData = new XmlData();
    xmlData.setResource(datacite);
    return xmlData;
  }

  private StringPlusAuthority creatPremisAuthority(String name) {
    final StringPlusAuthority agentType = new StringPlusAuthority();
    agentType.setValue(name);
    return agentType;
  }

  private FileGrp creatRootFileGroup() {
    final FileGrp root = new FileGrp();
    root.setID(this.getMetsId());
    root.setUSE(DLCMConstants.FILE_ROOT);
    return root;
  }

  private Div findSubLocation(Div parent, String relativeLocation) {
    for (final Div d : parent.getDiv()) {
      if (d.getLABEL().equals(relativeLocation)) {
        return d;
      }
    }
    final Div d = new Div();
    d.setID(this.getMetsId());
    d.setLABEL(relativeLocation);
    parent.getDiv().add(d);
    return d;
  }

  private FileGrp findSubLocation(FileGrp parent, String relativeLocation) {
    for (final FileGrp f : parent.getFileGrp()) {
      if (f instanceof FileGrp) {
        final FileGrp fg = f;
        if (fg.getUSE().equals(relativeLocation)) {
          return fg;
        }
      }
    }
    final FileGrp fg = new FileGrp();
    fg.setID(this.getMetsId());
    fg.setUSE(relativeLocation);
    parent.getFileGrp().add(fg);
    return fg;
  }

  private DataFileChecksum getChecksum(List<DataFileChecksum> checksums) {
    if (checksums.isEmpty()) {
      throw new SolidifyProcessingException("No checksum available");
    }
    for (final DataFileChecksum hash : checksums) {
      if (hash.getChecksumAlgo().equals(this.defaultChecksumAlgo)) {
        return hash;
      }

    }
    throw new SolidifyProcessingException("No default checksum alogrithm (" + this.defaultChecksumAlgo + ")");
  }

  private String getDataCiteMetadata(Resource resource) throws JAXBException {
    final StringWriter sw = new StringWriter();
    MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion).marshal(resource, sw);
    return sw.toString();
  }

  private String getFitsInfo(Fits fits) throws JAXBException {
    final StringWriter sw = new StringWriter();
    MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion).marshal(fits, sw);
    return sw.toString();
  }

  private Resource.RelatedIdentifiers getDataCiteRelations(Resource datacite) {
    Resource.RelatedIdentifiers relations = datacite.getRelatedIdentifiers();
    // Create object if needed
    if (relations == null) {
      relations = new Resource.RelatedIdentifiers();
      datacite.setRelatedIdentifiers(relations);
    }
    return relations;
  }

  private Fptr getFprt(File f) {
    final Fptr fptr = new Fptr();
    fptr.setFILEID(f.getFLocat().getID());
    return fptr;
  }

  private String getMetsId() {
    return this.idPrefix + ++this.metsId;
  }

  private void setMetsId(int nextFileId) {
    this.metsId = nextFileId;
  }

  private Optional<String> getORCID(Resource.Creators.Creator creator) {
    final List<Object> identifiers = creator.getNameIdentifier();
    for (final Object identifierObject : identifiers) {
      if (identifierObject instanceof NameIdentifier identifier
              && DLCMConstants.ORCID.equals(identifier.getNameIdentifierScheme())
              && this.webUrls.getOrcid().equals(identifier.getSchemeURI())) {
        return Optional.of(identifier.getValue().substring(identifier.getValue().lastIndexOf("/")));
      }
    }
    return Optional.empty();
  }

  private Div getParentDiv(Div divParent, String label) {
    // Check if already exists
    for (final Div d : divParent.getDiv()) {
      if (d.getLABEL().equals(label)) {
        return d;
      }
    }
    // Create it
    final Div d = new Div();
    d.setID(this.getMetsId());
    d.setLABEL(label);
    divParent.getDiv().add(d);
    return d;
  }

  private FileGrp getParentGroup(FileGrp fgParent, String label) {
    // Check if already exists
    for (final FileGrp fg : fgParent.getFileGrp()) {
      if (fg.getUSE().equals(label)) {
        return fg;
      }
    }
    // Create it
    final FileGrp fg = new FileGrp();
    fg.setID(this.getMetsId());
    fg.setUSE(label);
    fgParent.getFileGrp().add(fg);
    return fg;
  }

  private String getPremisMetadata(PremisComplexType premis) throws JAXBException {
    final StringWriter sw = new StringWriter();
    MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion).marshal(checkXMLObject(premis), sw);
    return sw.toString();
  }

  private String getCustomMetadata(List<MdRef> mdRefList) throws JAXBException {
    final StringWriter sw = new StringWriter();
    // Generate an empty Mets for custom metadata
    Mets mets = this.createMets("", "", "");
    mets.setDmdSec(this.createDescriptiveSection());
    mets.getDmdSec().getMdRef().addAll(mdRefList);
    // Convert in string
    MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion).marshal(mets, sw);
    return sw.toString();
  }

  private File loadAip(ArchivalInfoPackage aip) {
    final File f = new File();
    f.setID(this.getMetsId());
    f.setFLocat(this.createFileLocation(aip));
    return f;
  }

  private void loadAip(FileGrp fgRoot, Div divRoot, PremisComplexType premis, String label, ArchivalInfoPackage aip) {
    // Get parents
    final FileGrp fg = this.getParentGroup(fgRoot, label);
    final Div div = this.getParentDiv(divRoot, label);
    // load mets file from Aip
    final File f = this.loadAip(aip);
    // File Section
    fg.getFile().add(f);
    // Structure Map
    div.getFptr().add(this.getFprt(f));
    // Add premis metadata
    this.addPremisFile(premis, aip);
  }

  private MdWrap loadDescriptiveMetadata(AbstractDataFile<?, ?> df) throws JAXBException, IOException {
    Resource datacite = this.loadDataciteMetadataFile(Paths.get(df.getFinalData()));
    return this.createMetadataWrap(df, datacite);
  }

  private File loadFile(AbstractDataFile<?, ?> df, Path packagePath) {
    final File f = new File();
    f.setID(this.getMetsId());
    f.setFLocat(this.createFileLocation(df, packagePath));
    return f;
  }

  private File loadFileWithPredefinedIndex(String relativeFilePath) {
    final File f = new File();
    f.setID(this.getMetsId());
    f.setFLocat(this.createFileLocationWithNextAvailableIndexcreateFileLocationWithNextAvailableIndex(relativeFilePath));
    return f;
  }

  private void loadFile(FileGrp fgRoot, Div divRoot, PremisComplexType premis, String label, AbstractDataFile<?, ?> df, Path packagePath)
          throws JAXBException {
    // Get parents
    final FileGrp fg = this.getParentGroup(fgRoot, label);
    final Div div = this.getParentDiv(divRoot, label);
    // load mets file from data file
    final File f = this.loadFile(df, packagePath);
    // check if sub directory
    if (!df.isInRoot()) {
      // File Section
      final FileGrp fldFg = this.findSubLocation(fg, df.getRelativeLocation());
      fldFg.getFile().add(f);
      // Structure Map
      final Div fldDiv = this.findSubLocation(div, df.getRelativeLocation());
      fldDiv.getFptr().add(this.getFprt(f));
    } else {
      // File Section
      fg.getFile().add(f);
      // Structure Map
      div.getFptr().add(this.getFprt(f));
    }
    // Add premis metadata
    this.addPremisFile(premis, df, f.getID());
  }

  private Mets loadMetadataFile(Path metsFile) throws IOException, JAXBException {
    final Unmarshaller um = this.jaxbContext.createUnmarshaller();
    try (FileReader fileReader = new FileReader(metsFile.toFile())) {
      return (Mets) um.unmarshal(fileReader);
    }
  }

  private Resource loadDataciteMetadataFile(Path dataciteFile) throws IOException, JAXBException {
    final Unmarshaller um = this.jaxbContext.createUnmarshaller();
    try (FileReader fileReader = new FileReader(dataciteFile.toFile())) {
      return (Resource) um.unmarshal(fileReader);
    }
  }

  private Resource loadDataciteMetadata(String datacite) throws JAXBException {
    final Unmarshaller um = this.jaxbContext.createUnmarshaller();
    return (Resource) um.unmarshal(new StringReader(datacite));
  }

  private boolean nameExistsInCreatorsList(String fullname, List<Resource.Creators.Creator> creatorsList) {
    for (final Resource.Creators.Creator existingCreator : creatorsList) {
      if (existingCreator.getCreatorName().getValue().equals(fullname)) {
        return true;
      }
    }
    return false;
  }

  private boolean orcidExistsInCreatorsList(String orcid, List<Resource.Creators.Creator> creatorsList) {
    if (!StringTool.isNullOrEmpty(orcid)) {
      for (final Resource.Creators.Creator existingCreator : creatorsList) {
        final Optional<String> creatorOrcid = this.getORCID(existingCreator);
        if (creatorOrcid.isPresent() && orcid.equals(creatorOrcid.get())) {
          return true;
        }
      }
    }
    return false;
  }

  private boolean personExistsInCreatorsList(Person person, List<Resource.Creators.Creator> creatorsList) {

    /*
     * First check if ORCID already exists in list. This may prevent having the same person twice in
     * case the person is entered twice, with the same ORCID, but with both names written differently
     */
    if (!StringTool.isNullOrEmpty(person.getOrcid()) && this.orcidExistsInCreatorsList(person.getOrcid(), creatorsList)) {
      return true;
    }

    /*
     * Check on names. If no ORCID is given, dot not allow homonyms. Homonyms can thus be managed by
     * giving people's ORCID
     */
    return (StringTool.isNullOrEmpty(person.getOrcid()) && this.nameExistsInCreatorsList(person.getFullName(), creatorsList));
  }

  private <T extends AbstractDataFile<?, ?>> void referenceDataFile(Mets mets, PremisComplexType premis, List<T> dataFiles,
          List<ArchivalInfoPackage> aips, Path outputLocation) throws JAXBException, IOException {

    final StructMap structMap = mets.getStructMap();

    // Create Root group
    final FileGrp root = this.creatRootFileGroup();

    // Parse all data files
    final int totalDataFilesToProcess = dataFiles.size();
    int nbProcessedDataFiles = 0;
    for (final AbstractDataFile<?, ?> df : dataFiles) {
      switch (df.getDataCategory()) {
        case Package -> {
          switch (df.getDataType()) {
            case Metadata -> {
              // DLCM metadata
              mets.getDmdSec().setMdWrap(this.loadDescriptiveMetadata(df));
            }
            case CustomMetadata -> {
              // Custom Metadata
              this.loadFile(root, structMap.getDiv(), premis, DLCMConstants.METADATA_ROOT, df, outputLocation);
              // Load metadataType
              df.setMetadataType(this.getMetadataType(df.getMetadataTypeId()));
              mets.getDmdSec().getMdRef().add(this.createCustomMetadata(df));
            }
            default -> throw new SolidifyProcessingException("Wrong Data Type");
          }
        }
        case Primary -> {
          // Primary data (research data)
          this.loadFile(root, structMap.getDiv(), premis, DLCMConstants.RESEARCH_DATA_ROOT, df, outputLocation);
        }
        case Secondary -> {
          // Secondary data (documentation)
          this.loadFile(root, structMap.getDiv(), premis, DLCMConstants.DOC_ROOT, df, outputLocation);
        }
        case Software -> {
          // Software
          this.loadFile(root, structMap.getDiv(), premis, DLCMConstants.SOFTWARE_ROOT, df, outputLocation);
        }
        case Administrative -> {
          // Administrative data
          this.loadFile(root, structMap.getDiv(), premis, DLCMConstants.ADMIN_DATA_ROOT, df, outputLocation);
        }
        case Internal -> {
          // Internal (metadata files)
          this.loadFile(root, structMap.getDiv(), premis, DLCMConstants.INTERNAL_ROOT, df, outputLocation);
        }
        default -> throw new SolidifyProcessingException("Wrong Data Category");
      }
      nbProcessedDataFiles++;
      if (nbProcessedDataFiles % DATAFILE_BATCH_SIZE_FOR_LOG == 0) {
        log.info("{} / {} data files processed", nbProcessedDataFiles, totalDataFilesToProcess);
      }
    }
    log.info("All data files processed");
    // if AIP RestCollection
    if (!aips.isEmpty()) {
      for (final ArchivalInfoPackage aip : aips) {
        this.loadAip(root, structMap.getDiv(), premis, DLCMConstants.RESEARCH_DATA_ROOT, aip);
      }
    }

    // Assign root file group to file section
    mets.getFileSec().getFileGrp().add(root);
  }

  private void setPublisher(Resource datacite) {
    final Resource.Publisher publisher = new Resource.Publisher();
    publisher.setValue(this.repositoryDescription.getInstitution() + ", " + this.repositoryDescription.getLongName());
    if (!StringTool.isNullOrEmpty(this.repositoryDescription.getInstitutionRorId())) {
      publisher.setPublisherIdentifierScheme(DLCMConstants.ROR);
      publisher.setSchemeURI(this.webUrls.getRor());
      publisher.setPublisherIdentifier(this.webUrls.getRor() + this.repositoryDescription.getInstitutionRorId());
    }
    datacite.setPublisher(publisher);
  }

  private void updateChecksum(MdWrap mdWrap) throws IOException, JAXBException {
    try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
      Object obj = null;
      if (mdWrap.getXmlData().getResource() != null) {
        obj = mdWrap.getXmlData().getResource();
      } else if (mdWrap.getXmlData().getPremis() != null) {
        obj = mdWrap.getXmlData().getPremis();
      }
      if (obj != null) {
        final Marshaller marshaller = MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion);
        XMLTool.xml2Stream(marshaller, out, checkXMLObject(obj));
        final byte[] outputArray = out.toByteArray();
        final String hashAlgo = this.defaultChecksumAlgo.toString();
        mdWrap.setSIZE(BigInteger.valueOf(outputArray.length));
        mdWrap.setCHECKSUMTYPE(hashAlgo);
        mdWrap.setCHECKSUM(HashTool.hash(hashAlgo, outputArray));
      }

    } catch (TransformerFactoryConfigurationError | NoSuchAlgorithmException | TransformerException e) {
      throw new SolidifyProcessingException(e.getMessage(), e);
    }
  }

  private Resource.RelatedIdentifiers.RelatedIdentifier createRelatedIdentifier(
          RelationType relationType, RelatedIdentifierType relatedIdentifierType, String id) {
    Resource.RelatedIdentifiers.RelatedIdentifier relation = new Resource.RelatedIdentifiers.RelatedIdentifier();
    relation.setRelationType(relationType);
    relation.setRelatedIdentifierType(relatedIdentifierType);
    relation.setValue(id);
    return relation;
  }
}

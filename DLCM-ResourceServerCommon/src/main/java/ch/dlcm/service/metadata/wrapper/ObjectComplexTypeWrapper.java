/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ObjectComplexTypeWrapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.metadata.wrapper;

public class ObjectComplexTypeWrapper {

  private final ch.dlcm.model.xml.dlcm.v1.mets.ObjectComplexType objectComplexTypeV1;
  private final ch.dlcm.model.xml.dlcm.v2.mets.ObjectComplexType objectComplexTypeV2;
  private final ch.dlcm.model.xml.dlcm.v3.mets.ObjectComplexType objectComplexTypeV3;
  private final ch.dlcm.model.xml.dlcm.v4.mets.ObjectComplexType objectComplexTypeV4;


  public ObjectComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v1.mets.ObjectComplexType objectComplexType) {
    this.objectComplexTypeV1 = objectComplexType;
    this.objectComplexTypeV2 = null;
    this.objectComplexTypeV3 = null;
    this.objectComplexTypeV4 = null;
  }

  public ObjectComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v2.mets.ObjectComplexType objectComplexType) {
    this.objectComplexTypeV1 = null;
    this.objectComplexTypeV2 = objectComplexType;
    this.objectComplexTypeV3 = null;
    this.objectComplexTypeV4 = null;
  }

  public ObjectComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v3.mets.ObjectComplexType objectComplexType) {
    this.objectComplexTypeV1 = null;
    this.objectComplexTypeV2 = null;
    this.objectComplexTypeV3 = objectComplexType;
    this.objectComplexTypeV4 = null;
  }

  public ObjectComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v4.mets.ObjectComplexType objectComplexType) {
    this.objectComplexTypeV1 = null;
    this.objectComplexTypeV2 = null;
    this.objectComplexTypeV3 = null;
    this.objectComplexTypeV4 = objectComplexType;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - PremisComplexTypeWrapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.metadata.wrapper;

import java.util.ArrayList;
import java.util.List;

import ch.unige.solidify.exception.SolidifyRuntimeException;

public class PremisComplexTypeWrapper {
  private final ch.dlcm.model.xml.dlcm.v1.mets.PremisComplexType premisComplexTypeV1;
  private final ch.dlcm.model.xml.dlcm.v2.mets.PremisComplexType premisComplexTypeV2;
  private final ch.dlcm.model.xml.dlcm.v3.mets.PremisComplexType premisComplexTypeV3;
  private final ch.dlcm.model.xml.dlcm.v4.mets.PremisComplexType premisComplexTypeV4;


  public PremisComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v1.mets.PremisComplexType premisComplexType) {
    this.premisComplexTypeV1 = premisComplexType;
    this.premisComplexTypeV2 = null;
    this.premisComplexTypeV3 = null;
    this.premisComplexTypeV4 = null;
  }

  public PremisComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v2.mets.PremisComplexType premisComplexType) {
    this.premisComplexTypeV1 = null;
    this.premisComplexTypeV2 = premisComplexType;
    this.premisComplexTypeV3 = null;
    this.premisComplexTypeV4 = null;
  }

  public PremisComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v3.mets.PremisComplexType premisComplexType) {
    this.premisComplexTypeV1 = null;
    this.premisComplexTypeV2 = null;
    this.premisComplexTypeV3 = premisComplexType;
    this.premisComplexTypeV4 = null;
  }

  public PremisComplexTypeWrapper(ch.dlcm.model.xml.dlcm.v4.mets.PremisComplexType premisComplexType) {
    this.premisComplexTypeV1 = null;
    this.premisComplexTypeV2 = null;
    this.premisComplexTypeV3 = null;
    this.premisComplexTypeV4 = premisComplexType;
  }

  public List<ObjectComplexTypeWrapper> getObject() {
    final List<ObjectComplexTypeWrapper> objectWrapperList = new ArrayList<>();
    if (this.premisComplexTypeV1 != null) {
      for (ch.dlcm.model.xml.dlcm.v1.mets.ObjectComplexType object : this.premisComplexTypeV1.getObject()) {
        objectWrapperList.add(this.createSpecificObjectWrapperV1(object));
      }
    }
    if (this.premisComplexTypeV2 != null) {
      for (ch.dlcm.model.xml.dlcm.v2.mets.ObjectComplexType object : this.premisComplexTypeV2.getObject()) {
        objectWrapperList.add(this.createSpecificObjectWrapperV2(object));
      }
    }
    if (this.premisComplexTypeV3 != null) {
      for (ch.dlcm.model.xml.dlcm.v3.mets.ObjectComplexType object : this.premisComplexTypeV3.getObject()) {
        objectWrapperList.add(this.createSpecificObjectWrapperV3(object));
      }
    }
    if (this.premisComplexTypeV4 != null) {
      for (ch.dlcm.model.xml.dlcm.v4.mets.ObjectComplexType object : this.premisComplexTypeV4.getObject()) {
        objectWrapperList.add(this.createSpecificObjectWrapperV4(object));
      }
    }
    return objectWrapperList;
  }

  public Object getWrappedObject() {
    if (this.premisComplexTypeV1 != null) {
      return premisComplexTypeV1;
    }
    if (this.premisComplexTypeV2 != null) {
      return premisComplexTypeV2;
    }
    if (this.premisComplexTypeV3 != null) {
      return premisComplexTypeV3;
    }
    if (this.premisComplexTypeV4 != null) {
      return premisComplexTypeV4;
    }
    throw new SolidifyRuntimeException("PremisComplexTypeWrapper object is empty.");
  }

  private ObjectComplexTypeWrapper createSpecificObjectWrapperV1(ch.dlcm.model.xml.dlcm.v1.mets.ObjectComplexType object) {
    if (object instanceof ch.dlcm.model.xml.dlcm.v1.mets.PremisFile) {
      return new PremisFileWrapper((ch.dlcm.model.xml.dlcm.v1.mets.PremisFile) object);
    }
    // No more specific for now
    return new ObjectComplexTypeWrapper(object);
  }

  private ObjectComplexTypeWrapper createSpecificObjectWrapperV2(ch.dlcm.model.xml.dlcm.v2.mets.ObjectComplexType object) {
    if (object instanceof ch.dlcm.model.xml.dlcm.v2.mets.PremisFile) {
      return new PremisFileWrapper((ch.dlcm.model.xml.dlcm.v2.mets.PremisFile) object);
    }
    // No more specific for now
    return new ObjectComplexTypeWrapper(object);
  }

  private ObjectComplexTypeWrapper createSpecificObjectWrapperV3(ch.dlcm.model.xml.dlcm.v3.mets.ObjectComplexType object) {
    if (object instanceof ch.dlcm.model.xml.dlcm.v3.mets.PremisFile) {
      return new PremisFileWrapper((ch.dlcm.model.xml.dlcm.v3.mets.PremisFile) object);
    }
    // No more specific for now
    return new ObjectComplexTypeWrapper(object);
  }

  private ObjectComplexTypeWrapper createSpecificObjectWrapperV4(ch.dlcm.model.xml.dlcm.v4.mets.ObjectComplexType object) {
    if (object instanceof ch.dlcm.model.xml.dlcm.v4.mets.PremisFile) {
      return new PremisFileWrapper((ch.dlcm.model.xml.dlcm.v4.mets.PremisFile) object);
    }
    // No more specific for now
    return new ObjectComplexTypeWrapper(object);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - MetadataGenerator.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.metadata;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.data.domain.PageRequest;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.Language;
import ch.unige.solidify.model.xml.oai.v2.oai_dc.ElementType;
import ch.unige.solidify.model.xml.oai.v2.oai_dc.OaiDcType;
import ch.unige.solidify.model.xml.oai.v2.oai_dc.ObjectFactory;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.dlcm.DLCM1XmlNamespacePrefixMapper;
import ch.dlcm.DLCM2XmlNamespacePrefixMapper;
import ch.dlcm.DLCM3XmlNamespacePrefixMapper;
import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMProperties.WebUrls;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.Access;
import ch.dlcm.model.EmbargoInfo;
import ch.dlcm.model.FileInfoUpdate;
import ch.dlcm.model.IdentifierType;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.display.OrgUnitPersonRoleListDTO;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositContributor;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.settings.ArchiveType;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.License;
import ch.dlcm.model.settings.MetadataType;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.metadata.wrapper.MdWrapWrapper;
import ch.dlcm.service.metadata.wrapper.ObjectComplexTypeWrapper;
import ch.dlcm.service.metadata.wrapper.ObjectIdentifierComplexTypeWrapper;
import ch.dlcm.service.metadata.wrapper.PremisComplexTypeWrapper;
import ch.dlcm.service.metadata.wrapper.PremisFileWrapper;
import ch.dlcm.service.metadata.wrapper.StringPlusAuthorityWrapper;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchiveTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLanguageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLicenseRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackMetadataTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPersonRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPreservationPolicyRemoteResourceService;

public abstract class MetadataGenerator {

  private static final Logger log = LoggerFactory.getLogger(MetadataGenerator.class);
  protected static final int DATAFILE_BATCH_SIZE_FOR_LOG = 100;

  public static Marshaller getMarshaller(JAXBContext jaxbContext, DLCMMetadataVersion version) throws JAXBException {
    final Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    if (version.getVersionNumber() <= DLCMMetadataVersion.V1_1.getVersionNumber()) {
      marshaller.setProperty(DLCMConstants.XML_NAMESPACE_PREFIX_MAPPER, new DLCM1XmlNamespacePrefixMapper());
    } else if (version.getVersionNumber() == DLCMMetadataVersion.V4_0.getVersionNumber()) {
      marshaller.setProperty(DLCMConstants.XML_NAMESPACE_PREFIX_MAPPER, new DLCM3XmlNamespacePrefixMapper());
    } else {
      marshaller.setProperty(DLCMConstants.XML_NAMESPACE_PREFIX_MAPPER, new DLCM2XmlNamespacePrefixMapper());
    }
    return marshaller;
  }

  protected static Object checkXMLObject(Object obj) {
    // PREMIS exception because no XmlRootElement
    if (obj instanceof ch.dlcm.model.xml.dlcm.v1.mets.PremisComplexType premis1) {
      return new ch.dlcm.model.xml.dlcm.v1.mets.ObjectFactory().createPremis(premis1);
    } else if (obj instanceof ch.dlcm.model.xml.dlcm.v2.mets.PremisComplexType premis2) {
      return new ch.dlcm.model.xml.dlcm.v2.mets.ObjectFactory().createPremis(premis2);
    } else if (obj instanceof ch.dlcm.model.xml.dlcm.v3.mets.PremisComplexType premis3) {
      return new ch.dlcm.model.xml.dlcm.v3.mets.ObjectFactory().createPremis(premis3);
    } else if (obj instanceof ch.dlcm.model.xml.dlcm.v4.mets.PremisComplexType premis4) {
      return new ch.dlcm.model.xml.dlcm.v4.mets.ObjectFactory().createPremis(premis4);
    }
    return obj;
  }

  protected static final String WINDOWS = "Windows";
  protected static final String SIMPLE = "simple";
  protected static final String FORMAT_REGISTRY = "formatRegistry";
  protected static final String FORMAT_DESIGNATION = "formatDesignation";

  protected final ObjectFactory oaiDcFactory = new ObjectFactory();
  protected final DLCMRepositoryDescription repositoryDescription;
  protected final GitInfoProperties gitInfoProperties;
  protected final HistoryService historyService;

  protected final ChecksumAlgorithm defaultChecksumAlgo;
  protected final String idPrefix;
  protected final DLCMMetadataVersion metadataVersion;
  protected final String xsl4Indexing;
  protected final WebUrls webUrls;
  protected final IdentifierType defaultIdentifierType;

  protected JAXBContext jaxbContext;
  protected long metsId = 0;

  protected final MessageService messageService;
  private final FallbackLicenseRemoteResourceService licenseResourceService;
  private final FallbackPersonRemoteResourceService personResourceService;
  private final FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService;
  private final FallbackMetadataTypeRemoteResourceService metadataTypeService;
  private final FallbackLanguageRemoteResourceService languageResourceService;
  protected final FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataResourceService;
  protected final FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataResourceService;
  protected final FallbackPreservationPolicyRemoteResourceService preservationPolicyResourceService;
  protected final FallbackArchiveTypeRemoteResourceService archiveTypeResourceService;
  protected final FileFormatService fileFormatService;

  // *****************
  // ** Constructor **
  // *****************
  protected MetadataGenerator(DLCMMetadataVersion metadataVersion,
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repositoryDescription,
          MessageService messageService,
          FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataResourceService,
          FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataResourceService,
          FallbackMetadataTypeRemoteResourceService metadataTypeService,
          FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService,
          FallbackLicenseRemoteResourceService licenseResourceService,
          FallbackLanguageRemoteResourceService languageResourceService,
          FallbackPersonRemoteResourceService personResourceService,
          FallbackPreservationPolicyRemoteResourceService preservationPolicyResourceService,
          FallbackArchiveTypeRemoteResourceService archiveTypeResourceService,
          FileFormatService fileFormatService,
          HistoryService historyService,
          GitInfoProperties gitInfoProperties) {
    this.metadataVersion = metadataVersion;
    this.xsl4Indexing = this.getXsl4Indexing();
    this.repositoryDescription = repositoryDescription;
    this.messageService = messageService;
    this.archivePublicMetadataResourceService = archivePublicMetadataResourceService;
    this.archivePrivateMetadataResourceService = archivePrivateMetadataResourceService;
    this.metadataTypeService = metadataTypeService;
    this.orgUnitResourceService = orgUnitResourceService;
    this.languageResourceService = languageResourceService;
    this.licenseResourceService = licenseResourceService;
    this.personResourceService = personResourceService;
    this.fileFormatService = fileFormatService;
    this.historyService = historyService;
    this.preservationPolicyResourceService = preservationPolicyResourceService;
    this.archiveTypeResourceService = archiveTypeResourceService;
    this.gitInfoProperties = gitInfoProperties;
    this.defaultChecksumAlgo = ChecksumAlgorithm.valueOf(dlcmProperties.getParameters().getDefaultChecksum());
    this.idPrefix = dlcmProperties.getParameters().getMetsIdPrefix();
    this.webUrls = dlcmProperties.getWebUrls();
    this.defaultIdentifierType = dlcmProperties.getParameters().getDefaultIdentifierType();
  }

  // **********************
  // ** Abstract methods **
  // **********************

  public abstract void checkMetadataFile(DLCMMetadataVersion version, Path metadata, String packageType) throws IOException, JAXBException;

  public abstract void checkDescriptionMetadataFile(Path metadata) throws IOException, JAXBException;

  public abstract void completeDataciteMetadataFileWithDeposit(Deposit deposit, DepositDataFile df)
          throws JAXBException, IOException;

  public abstract void completeDataciteMetadataWithDeposit(Deposit deposit, Path tmpFilePath) throws JAXBException, IOException;

  public abstract URI generateDefaultDataCite(Order order, String packageId, String orgUnitId, Access accessLevel, Path metadataFile)
          throws JAXBException, IOException;

  public abstract <T extends AbstractDataFile<?, ?>> void generateMetadata(String resId, String name, OffsetDateTime creationTime,
          OffsetDateTime updateTime, String packageType, String module, String request, List<T> dataFiles, List<ArchivalInfoPackage> aips,
          Path outputLocation) throws JAXBException, IOException;

  public abstract void updateMetadata(SubmissionInfoPackage sip, Path metadataFile) throws JAXBException, IOException;

  public abstract void completeArchiveIndex(ArchiveMetadata archiveMetadata, Path xml) throws IOException, JAXBException;

  public abstract Object getDataCiteMetadata(String dataCiteString) throws JAXBException;

  public abstract JAXBElement<OaiDcType> getOaiDcMetadata(ArchiveMetadata archiveMetadata) throws JAXBException;

  public abstract void updateMetadataForDisposal(ArchivalInfoPackage aip, Path metadataFile) throws IOException, JAXBException;

  public abstract void updateMetadata(ArchivalInfoPackage aip, Path metadataFile, Path newMetadataFile, Path newPremisFile,
          List<FileInfoUpdate> fileInfoUpdates) throws IOException, JAXBException;

  public abstract void upgradeMetadata(ArchivalInfoPackage aip, Path metadataFile) throws IOException, JAXBException;

  public abstract int updateCompliance(ArchivalInfoPackage aip, Path metadataFile) throws IOException, JAXBException;

  public abstract Map<String, String> extractInfoFromMetadata(Path metadataFile) throws IOException, JAXBException;

  public abstract void extractDataciteMetadataFromFile(Path filePath, OutputStream outputStream) throws JAXBException, IOException;

  public abstract void writeDataciteXmlFromIndexCompletedWithDeposit(Deposit deposit, OutputStream outputStream) throws JAXBException;

  public abstract void writePremisXmlOfUpdatedFile(Deposit deposit, List<DepositDataFile> dataFiles, String outputLocation)
          throws JAXBException, IOException;

  // *********************
  // ** Default methods **
  // *********************

  public Deposit writeDepositFromIndex(Deposit deposit) {
    final ArchiveMetadata archiveMetadata = this.getArchiveMetadataFromDeposit(deposit);
    // Set metadata version
    deposit.setMetadataVersion(DLCMMetadataVersion.fromVersion(archiveMetadata.getMetadataVersion()));
    // Set title
    deposit.setTitle(archiveMetadata.getTitle());
    log.trace("completeDepositTitle done");
    // Set description
    this.completeDepositDescription(deposit, archiveMetadata);
    log.trace("completeDepositDescription done");
    // Set Keywords
    deposit.setKeywords(archiveMetadata.getKeywords());
    log.trace("completeDepositKeywords done");
    // Set Identifiers
    this.completeDepositIdentifiers(deposit, archiveMetadata);
    // Set DOI relations
    this.completeDepositDoiRelations(deposit, archiveMetadata);
    log.trace("completeDepositDoiRelations done");
    // Set Publication year, collection begin and collection end
    this.completeDepositDate(deposit, archiveMetadata);
    log.trace("completeDepositDate done");
    // Set Contributors
    this.completeDepositContributors(deposit, archiveMetadata);
    log.trace("completeDepositContributors done");
    // Set Access Level, Embargo and data tags
    this.completeDepositAccessLevelAndDataAndDUA(deposit, archiveMetadata);
    log.trace("completeDepositAccessLevelAndData done");
    // Set License
    this.completeDepositLicense(deposit, archiveMetadata);
    log.trace("completeDepositLicense done");
    // Set Preservation policy
    this.completeDepositPreservationPolicy(deposit, archiveMetadata);
    log.trace("completeDepositPreservationPolicy done");
    this.completeDepositCollectionContent(deposit, archiveMetadata);
    log.trace("completeDepositCollectionContent done");
    this.completeDepositArchiveType(deposit, archiveMetadata);
    log.trace("completeDepositArchiveType done");
    return deposit;
  }

  private void completeDepositArchiveType(Deposit deposit, ArchiveMetadata archiveMetadata) {
    String masterTypeId = archiveMetadata.getResourceTypeGeneral().toUpperCase();
    if (deposit.isCollection() && this.metadataVersion.getVersionNumber() < DLCMMetadataVersion.V3_0.getVersionNumber()) {
      // Force Collection for old version (<3.0)
      masterTypeId = ArchiveType.COLLECTION_ID;
    }
    List<ArchiveType> archiveTypes = this.archiveTypeResourceService.get(masterTypeId, archiveMetadata.getResourceType()).getData();
    if (archiveTypes.isEmpty()) {
      throw new SolidifyCheckingException(this.messageService.get(this.messageService.get("validation.deposit.archiveType.wrongConfig")));
    } else {
      deposit.setArchiveTypeId(archiveTypes.get(0).getResId());
      deposit.setArchiveType(archiveTypes.get(0));
    }
  }

  private void completeDepositDescription(Deposit deposit, ArchiveMetadata archiveMetadata) {
    deposit.setDescription(archiveMetadata.getAbstract());
    if (!StringTool.isNullOrEmpty(archiveMetadata.getAbstractLanguage())) {
      Language language = this.getLanguageByCode(archiveMetadata.getAbstractLanguage());
      deposit.setLanguageId(language.getResId());
      deposit.setLanguage(language);
    } else {
      deposit.setLanguageId(null);
      deposit.setLanguage(null);
    }
  }

  private void completeDepositIdentifiers(Deposit deposit, ArchiveMetadata archiveMetadata) {
    deposit.setDoi(archiveMetadata.getDoi());
    deposit.setArk(archiveMetadata.getArk());
  }

  private void completeDepositDoiRelations(Deposit deposit, ArchiveMetadata archiveMetadata) {
    // Reset others DOIs
    deposit.setIsIdenticalTo(null);
    deposit.setIsReferencedBy(null);
    deposit.setIsObsoletedBy(null);

    for (Map<String, Object> relation : archiveMetadata.getRelations()) {
      if (relation.get(DLCMConstants.DATACITE_RELATION_ID_TYPE_NAME).equals(DLCMConstants.DOI)) {
        switch ((String) relation.get(DLCMConstants.DATACITE_RELATION_TYPE_NAME)) {
          case "IsIdenticalTo" -> deposit.setIsIdenticalTo((String) relation.get(DLCMConstants.CONTENT_FIELD));
          case "IsObsoletedBy" -> deposit.setIsObsoletedBy((String) relation.get(DLCMConstants.CONTENT_FIELD));
          case "IsReferencedBy" -> deposit.getIsReferencedBy().add((String) relation.get(DLCMConstants.CONTENT_FIELD));
          default -> {
            // nothing to do
          }
        }
      }
    }
  }

  private void completeDepositCollectionContent(Deposit deposit, ArchiveMetadata archiveMetadata) {
    // Check if deposit is a collection
    if (deposit.getCollectionSize() > 0) {
      // Remove existing content
      deposit.getCollection().clear();
      // Re-create content
      for (Map<String, Object> relation : archiveMetadata.getRelations()) {
        if (relation.get(DLCMConstants.DATACITE_RELATION_TYPE_NAME).equals("HasPart")) {
          switch ((String) relation.get(DLCMConstants.DATACITE_RELATION_ID_TYPE_NAME)) {
            case DLCMConstants.DOI -> {
              String doi = (String) relation.get(DLCMConstants.CONTENT_FIELD);
              ArchiveMetadata archive = this.archivePublicMetadataResourceService.getMetadataByDoi(doi);
              deposit.getCollection().add(archive.getResId());
            }
            case DLCMConstants.HANDLE -> deposit.getCollection().add((String) relation.get(DLCMConstants.CONTENT_FIELD));
            default -> {
              // nothing to do
            }
          }
        }
      }
    }
  }

  private void completeDepositDate(Deposit deposit, ArchiveMetadata archiveMetadata) {
    // Set publication date
    deposit.setPublicationDate(archiveMetadata.getCreatedDate().toLocalDate());

    // Set data collection dates
    deposit.setCollectionBegin(archiveMetadata.getCollectedStartDate());
    deposit.setCollectionEnd(archiveMetadata.getCollectedEndDate());
  }

  private void completeDepositContributors(Deposit deposit, ArchiveMetadata archiveMetadata) {
    deposit.getContributors().clear();
    archiveMetadata.getCreators()
            .forEach(creator -> {
              Optional<Person> person = this.getPerson(creator);
              if (person.isPresent()) {
                DepositContributor contributor = new DepositContributor();
                contributor.setDeposit(deposit);
                contributor.setPersonId(person.get().getResId());
                contributor.setPosition(deposit.getContributors().size() * 10);
                deposit.getContributors().add(contributor);
              }
            });
  }

  private void completeDepositAccessLevelAndDataAndDUA(Deposit deposit, ArchiveMetadata archiveMetadata) {
    // Access Level
    deposit.setAccess(archiveMetadata.getFinalAccessLevel());
    // Embargo
    if (archiveMetadata.getEmbargoAccessLevel().isPresent()) {
      OffsetDateTime endDateEmbargo = archiveMetadata.getEmbargoEndDate()
              .orElseThrow(() -> new SolidifyCheckingException("Missing embargo end date for deposit [" + deposit.getResId() + "]"));
      OffsetDateTime startDateEmbargo = archiveMetadata.getCreatedDate();

      EmbargoInfo embargoInfo = new EmbargoInfo();
      embargoInfo.setMonths((int) ChronoUnit.MONTHS.between(startDateEmbargo, endDateEmbargo));
      embargoInfo.setAccess(archiveMetadata.getEmbargoAccessLevel()
              .orElseThrow(() -> new SolidifyCheckingException("Missing embargo end access level for deposit [" + deposit.getResId() + "]")));
      embargoInfo.setStartDate(startDateEmbargo);
      deposit.setEmbargo(embargoInfo);
    } else {
      deposit.setEmbargo(null);
    }
    // Data tag
    deposit.setDataSensitivity(archiveMetadata.getDataTag());
    // DUA Type
    deposit.setDataUsePolicy(archiveMetadata.getDataUsePolicy());
  }

  private void completeDepositLicense(Deposit deposit, ArchiveMetadata archiveMetadata) {
    // License
    License license = null;
    for (Map<String, Object> right : archiveMetadata.getRights()) {
      if (!DLCMConstants.getUriListWithoutLicense().contains(right.get("rightsURI"))) {
        if (right.containsKey("rightsIdentifierScheme") && right.get("rightsIdentifierScheme").equals("SPDX")) {
          license = this.licenseResourceService.getByOpenLicenseId((String) right.get("rightsIdentifier"));
        } else {
          license = this.getLicenseByTitle((String) right.get(DLCMConstants.CONTENT_FIELD));
        }
      }
    }

    if (license != null) {
      deposit.setLicenseId(license.getResId());
    } else {
      deposit.setLicenseId(null);
    }
  }

  private void completeDepositPreservationPolicy(Deposit deposit, ArchiveMetadata archiveMetadata) {
    String aipRetentionEnd = archiveMetadata.getRetention();
    PreservationPolicy preservationPolicy = this.getPreservationPolicy(aipRetentionEnd);
    if (preservationPolicy != null) {
      deposit.setPreservationPolicy(preservationPolicy);
    }
  }

  private Optional<Person> getPerson(Map<String, String> creator) {
    String orcid = creator.get(DLCMConstants.PERSON_ORCID);
    if (!StringTool.isNullOrEmpty(orcid)) {
      Optional<Person> personOpt = this.getPersonByOrcid(orcid);
      if (personOpt.isPresent()) {
        // return the Person found by its ORCID
        return personOpt;
      }
    }
    // If no Person has been found above by ORCID, search the Person by its fullname
    return this.getFirstPersonByFullname(creator.get(DLCMConstants.FAMILY_NAME) + ", " + creator.get(DLCMConstants.GIVEN_NAME));
  }

  protected Person getPerson(String personId) {
    return this.personResourceService.findOneWithCache(personId);
  }

  protected List<Person> getPersonByName(String fullName) {
    if (fullName.contains(",")) {
      String[] names = fullName.split(", ");
      String queryString = "firstName=" + URLEncoder.encode(names[1], StandardCharsets.UTF_8)
              + "&lastName=" + URLEncoder.encode(names[0], StandardCharsets.UTF_8);
      return this.personResourceService.getPersonList(queryString, PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE)).getData();
    } else {
      throw new SolidifyRuntimeException("fullName value '" + fullName + "' doesn't contain any coma");
    }
  }

  protected Optional<Person> getFirstPersonByFullname(String fullname) {
    List<Person> peopleList = this.getPersonByName(fullname);
    if (!peopleList.isEmpty()) {
      return Optional.of(peopleList.get(0));
    } else {
      return Optional.empty();
    }
  }

  protected Optional<Person> getPersonByOrcid(String orcid) {
    try {
      Person person = this.personResourceService.getByOrcid(orcid);
      return Optional.of(person);
    } catch (SolidifyResourceNotFoundException e) {
      log.warn("No person found with ORCID {}", orcid);
      return Optional.empty();
    }
  }

  protected List<Institution> getPersonInstitutions(String personId) {
    return this.personResourceService.findInstitutions(personId);
  }

  protected Language getLanguage(String languageId) {
    return this.languageResourceService.findOneWithCache(languageId);
  }

  protected Language getLanguageByCode(String code) {
    List<Language> languageList = this.languageResourceService.getLanguagesByCode(code);
    if (!languageList.isEmpty()) {
      return languageList.get(0);
    }
    return null;
  }

  protected Language getLanguage(Deposit deposit) {
    if (deposit.getLanguageId() != null) {
      return this.getLanguage(deposit.getLanguageId());
    }
    return null;
  }

  protected PreservationPolicy getPreservationPolicy(String retention) {
    List<PreservationPolicy> preservationPolicies = this.preservationPolicyResourceService.getPreservationPoliciesByRetention(retention);
    if (!preservationPolicies.isEmpty()) {
      return preservationPolicies.get(0);
    }
    return null;
  }

  protected License getLicense(String licenseId) {
    return this.licenseResourceService.findOneWithCache(licenseId);
  }

  protected License getLicenseByTitle(String title) {
    List<License> licenseList = this.licenseResourceService.getLicensesByTitle(title);
    if (!licenseList.isEmpty()) {
      return licenseList.get(0);
    }
    return null;
  }

  protected MetadataType getMetadataType(String metadataTypeId) {
    return this.metadataTypeService.findOneWithCache(metadataTypeId);
  }

  protected OrganizationalUnit getOrganizationUnit(String organizationalUnitId) {
    return this.orgUnitResourceService.findOneWithCache(organizationalUnitId);
  }

  protected List<Institution> getOrganizationUnitInstitutions(String organizationalUnitId) {
    return this.orgUnitResourceService.getInstitutions(organizationalUnitId);
  }

  protected List<FundingAgency> getOrganizationUnitFundingAgencies(String organizationalUnitId) {
    return this.orgUnitResourceService.getFundingAgencies(organizationalUnitId);
  }

  protected List<OrgUnitPersonRoleListDTO> getOrganizationUnitMembers(String organizationalUnitId) {
    return this.orgUnitResourceService.getMembers(organizationalUnitId);
  }

  protected ArchiveMetadata getArchiveMetadataById(String aipId) {
    return this.archivePublicMetadataResourceService.getIndexMetadata(aipId);
  }

  protected ArchiveMetadata getArchiveMetadataByDoi(String aipDoi) {
    return this.archivePublicMetadataResourceService.getMetadataByDoi(aipDoi);
  }

  /**
   * All date time in metadata files have to be stored with the UTC zone offset such as
   * 2019-11-06T08:45:20.449072Z
   */
  protected OffsetDateTime useUTC(OffsetDateTime dateTime) {
    return dateTime.withOffsetSameInstant(ZoneOffset.UTC);
  }

  protected void resetMetsIds() {
    this.metsId = 0;
  }

  protected void renameMetadataFile(Path mdPath) throws IOException {
    FileTool.renameFile(mdPath.resolve(DLCMConstants.METS_METADATA_FILE), mdPath.resolve(DLCMConstants.METADATA_FILE), true);
  }

  protected ElementType getOaiDcEntry(String value, String language) {
    final ElementType fieldValue = new ElementType();
    fieldValue.setValue(value);
    if (!StringTool.isNullOrEmpty(language)) {
      fieldValue.setLang(language);
    }
    return fieldValue;
  }

  protected String tranform4Indexing(String inputXml) {
    // Normalize DLCM metadata
    return XMLTool.transform(inputXml, this.xsl4Indexing);
  }

  protected String getPackageType(String packageType) {
    if (StringTool.isNullOrEmpty(packageType)) {
      return DLCMConstants.DLCM.toLowerCase();
    } else {
      return DLCMConstants.DLCM.toLowerCase() + "." + packageType.toLowerCase();
    }
  }

  protected String getPremisEventType(String packageType, String status) {
    // Creation
    if (status.equals(StatusHistory.CREATED_STATUS)) {
      if (packageType.equals(ResourceName.SIP)
              || packageType.equals(ResourceName.AIP)
              || packageType.equals(ResourceName.DIP)) {
        return "information package creation";
      } else {
        return "creation";
      }
    }
    // Approval
    if (status.equals(Deposit.DepositStatus.APPROVED.toString())) {
      return "appraisal";
    }
    // Submission agreement approval
    if (status.equals(Deposit.DepositStatus.SUBMISSION_AGREEMENT_APPROVED.toString())) {
      return "submission agreement approval";
    }
    // Format identification
    if (status.equals(DataFileStatus.FILE_FORMAT_IDENTIFIED.toString())) {
      return "format identification";
    }
    // Virus check
    if (status.equals(DataFileStatus.VIRUS_CHECKED.toString())) {
      return "virus check";
    }
    // Disposal
    if (status.equals(PackageStatus.DISPOSAL_APPROVED_BY_ORGUNIT.toString())
            || status.equals(PackageStatus.DISPOSAL_APPROVED.toString())) {
      return "deletion"; // or could be 'deaccession'
    }
    // Retention extension
    if (status.equals(PackageStatus.UPDATING_RETENTION.toString())) {
      return "policy assignment";
    }
    // Editing metadata
    if (status.equals(PackageStatus.EDITING_METADATA.toString())) {
      return "editing metadata";
    }
    // Upgrading metadata
    if (status.equals(PackageStatus.UPGRADING_METADATA.toString())) {
      return "updating metadata";
    }
    // Updating compliance level
    if (status.equals(PackageStatus.UPDATING_COMPLIANCE_LEVEL.toString())) {
      return "updating compliance level";
    }

    return null;
  }

  protected <T> void saveXmlInFile(T xml, String outputFile) throws IOException, JAXBException {
    try (OutputStream out = new BufferedOutputStream(new FileOutputStream(outputFile), SolidifyConstants.BUFFER_SIZE)) {
      XMLTool.xml2Stream(MetadataGenerator.getMarshaller(this.jaxbContext, this.metadataVersion), out, xml);
    } catch (TransformerFactoryConfigurationError | TransformerException e) {
      throw new SolidifyProcessingException(e.getMessage(), e);
    }
  }

  protected String getMetadataVersionFileName(ArchivalInfoPackage aip) {
    OffsetDateTime lastCompleted = this.historyService.getLastCompletedUpdate(aip.getResId());
    if (lastCompleted == null) {
      throw new SolidifyProcessingException("Cannot find last update of AIP (" + aip.getResId() + ")");
    }
    return DLCMConstants.REPRESENTATION_INFO_METADATA_FILE_PREFIX
            + lastCompleted.format(DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT_FOR_FILE)) + SolidifyConstants.XML_EXT;
  }

  private String getXsl4Indexing() {
    final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
    try {
      final org.springframework.core.io.Resource[] xsl = resolver.getResources("classpath*:/" + DLCMConstants.XSL_HOME + "/" +
              this.metadataVersion.getIndexingTransformation());
      if (xsl.length > 0) {
        return FileTool.toString(xsl[0].getInputStream());
      }
    } catch (final IOException e) {
      throw new SolidifyRuntimeException(XMLTool.XSL_PREFIX, e);
    }
    throw new SolidifyCheckingException(
            this.messageService.get("checking.metadata.missingxsl", new Object[] { this.metadataVersion.getIndexingTransformation() }));
  }

  protected String getDetailedLabel(String identifier, String description) {
    if (StringTool.isNullOrEmpty(description)) {
      throw new NullPointerException("Missing label description");
    }
    if (StringTool.isNullOrEmpty(identifier)) {
      return description;
    }
    return "[" + identifier + "] " + description;
  }

  protected ArchiveMetadata getArchiveMetadataFromDeposit(Deposit deposit) {
    final String aipId = this.archivePrivateMetadataResourceService.getAipIdFromDepositId(deposit.getResId());
    return this.archivePublicMetadataResourceService.getIndexMetadata(aipId);
  }

  protected void updatePremisObjectsForCollection(MdWrapWrapper mdWrap, ArchivalInfoPackage aip) {
    final PremisComplexTypeWrapper premis = mdWrap.getXmlData().getPremis();
    final List<String> collectionItemIdList = aip.getCollectionIds();
    final List<String> premisAipObjectIdList = new ArrayList<>();
    final List<String> premisObjectToRemove = new ArrayList<>();
    final String aipPackageType = this.getPackageType(ResourceName.AIP);

    // Determine the list of PREMIS objects no more in the collection
    for (ObjectComplexTypeWrapper object : premis.getObject()) {
      if (object instanceof PremisFileWrapper premisFileWrapper) {
        final ObjectIdentifierComplexTypeWrapper objectIdentifier = this.getFirstObjectIdentifier(premisFileWrapper, aip.getResId());
        final StringPlusAuthorityWrapper objectIdentifierType = objectIdentifier.getObjectIdentifierType();
        if (objectIdentifierType != null && aipPackageType.equals(objectIdentifierType.getValue())) {
          final String objectId = objectIdentifier.getObjectIdentifierValue();
          premisAipObjectIdList.add(objectId);
          if (!collectionItemIdList.contains(objectId)) {
            premisObjectToRemove.add(objectId);
          }
        }
      }
    }

    // Remove PREMIS objects no more in the collection
    for (String objectId : premisObjectToRemove) {
      premis.getObject().removeIf(object -> {
        if (object instanceof PremisFileWrapper premisFileWrapper) {
          final ObjectIdentifierComplexTypeWrapper objectIdentifier = this.getFirstObjectIdentifier(premisFileWrapper, aip.getResId());
          return objectId.equals(objectIdentifier.getObjectIdentifierValue());
        }
        return false;
      });
    }

    // Add new PREMIS object
    for (ArchivalInfoPackage collectionItem : aip.getCollection()) {
      if (!premisAipObjectIdList.contains(collectionItem.getResId())) {
        this.addWrappedPremisFile(premis, collectionItem);
      }
    }

  }

  protected List<StatusHistory> getStatusHistoryList(String resId, String packageType) {
    return switch (packageType) {
      case ResourceName.DEPOSIT -> this.historyService.getDepositLastSubmission(resId);
      case ResourceName.DATAFILE, ResourceName.ORDER -> this.historyService.getHistory(resId);
      default -> this.historyService.getLastProcess(resId);
    };
  }

  private void addWrappedPremisFile(PremisComplexTypeWrapper premis, ArchivalInfoPackage aip) {
    Object unwrappedPremis = premis.getWrappedObject();
    if (unwrappedPremis instanceof ch.dlcm.model.xml.dlcm.v1.mets.PremisComplexType premis1) {
      ((MetadataVersion1) this).addPremisFile(premis1, aip);
    }
    if (unwrappedPremis instanceof ch.dlcm.model.xml.dlcm.v2.mets.PremisComplexType premis2) {
      ((MetadataVersion2) this).addPremisFile(premis2, aip);
    }
    if (unwrappedPremis instanceof ch.dlcm.model.xml.dlcm.v3.mets.PremisComplexType premis3) {
      ((MetadataVersion3) this).addPremisFile(premis3, aip);
    }
    if (unwrappedPremis instanceof ch.dlcm.model.xml.dlcm.v4.mets.PremisComplexType premis4) {
      ((MetadataVersion4) this).addPremisFile(premis4, aip);
    }
  }

  private ObjectIdentifierComplexTypeWrapper getFirstObjectIdentifier(PremisFileWrapper premisFile, String aipId) {
    final List<ObjectIdentifierComplexTypeWrapper> objectIdentifierList = premisFile.getObjectIdentifier();
    if (objectIdentifierList.isEmpty()) {
      throw new SolidifyRuntimeException("PremisFile has no identifier in AIP " + aipId);
    }
    return objectIdentifierList.get(0);
  }
}

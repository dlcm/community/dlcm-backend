/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - SearchService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import org.springframework.stereotype.Service;

import ch.unige.solidify.service.IndexSearchRemoteService;

import ch.dlcm.model.IdentifierType;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;

@Service("searchRemoteService")
public class SearchService extends IndexSearchRemoteService<ArchiveMetadata> {

  public SearchService(FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService) {
    super(archivePublicMetadataRemoteResourceService);
  }

  public ArchiveMetadata searchByDOI(String doi) {
    return ((FallbackArchivePublicMetadataRemoteResourceService) this.indexMetadataRemoteResourceService).getMetadataByDoi(doi);
  }

  public ArchiveMetadata getByIdentifier(IdentifierType idType, String id) {
    return ((FallbackArchivePublicMetadataRemoteResourceService) this.indexMetadataRemoteResourceService).getByIdentifier(idType, id);
  }

}

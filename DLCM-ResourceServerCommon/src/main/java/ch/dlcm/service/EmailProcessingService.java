/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - EmailProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;

import ch.unige.solidify.service.EmailService;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.message.EmailMessage;

public abstract class EmailProcessingService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(EmailProcessingService.class);

  protected final EmailService emailService;
  protected final String submissionPortalHomepage;
  protected final String project;

  protected EmailProcessingService(MessageService messageService, EmailService emailService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.emailService = emailService;
    this.submissionPortalHomepage = dlcmProperties.getParameters().getSubmissionPortalHomepage();
    this.project = dlcmProperties.getParameters().getProjectNameForEmailTemplates();
  }

  @JmsListener(destination = "${dlcm.topic.emails}", containerFactory = "topicListenerFactory")
  public void receiveEmailMessage(EmailMessage emailMessage) {
    log.info("Reading message {}", emailMessage);
    this.processEmailMessage(emailMessage);
    log.info("Message {} processed", emailMessage);
  }

  protected abstract void processEmailMessage(EmailMessage emailMessage);

  protected Map<String, Object> getEmailDefaultParameters() {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("submissionPortalHomepage", this.submissionPortalHomepage);
    parameters.put("project", this.project);
    return parameters;
  }
}

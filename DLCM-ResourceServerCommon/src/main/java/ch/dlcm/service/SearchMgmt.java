/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - SearchMgmt.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.oai.OAIRecord;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.OAISearchService;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.MetadataVisibility;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.OrderArchive;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;

@Service
public class SearchMgmt extends DLCMService implements OAISearchService {
  private static final Logger log = LoggerFactory.getLogger(SearchMgmt.class);

  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;
  private final FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService;
  private final FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteResourceService;

  public SearchMgmt(
          MessageService messageService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService,
          FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService,
          FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteResourceService,
          DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.aipRemoteResourceService = aipRemoteResourceService;
    this.archivePublicMetadataRemoteResourceService = archivePublicMetadataRemoteResourceService;
    this.archivePrivateMetadataRemoteResourceService = archivePrivateMetadataRemoteResourceService;

  }

  public boolean checkMetadata(String aipId) {
    return (this.archivePublicMetadataRemoteResourceService.headIndexMetadata(aipId) == HttpStatus.OK
            && this.archivePrivateMetadataRemoteResourceService.headIndexMetadata(aipId) == HttpStatus.OK);
  }

  public ArchivalInfoPackage getAIP(String resId) {
    return this.aipRemoteResourceService.getAIPOnStorage(resId);
  }

  public RestCollection<OrderArchive> search(MetadataVisibility mdVisibilty, Order order, boolean allInfo, Pageable pageable) {
    RestCollection<OrderArchive> orderArchiveCollection = new RestCollection<>();
    try {
      switch (order.getQueryType()) {
        case ADVANCED -> orderArchiveCollection = this.search(mdVisibilty, order.getQuery(), null, allInfo, pageable);
        case DIRECT, SIMPLE -> orderArchiveCollection = new RestCollection<>(this.getOrderArchiveList(order.getQuery(), allInfo));
        default -> throw new SolidifyCheckingException("Unknown order type:" + order.getQueryType());
      }
    } catch (final RuntimeException e) {
      final String message = "Query (" + order.getQueryType().getLabel() + "=" + order.getQuery() + ") error: " + e.getMessage();
      throw new SolidifyRuntimeException(message, e);
    }
    final String queryTypeLabel = order.getQueryType().getLabel();
    final String query = order.getQuery();
    final long currentItems = orderArchiveCollection.getPage().getCurrentItems();
    final long totalItems = orderArchiveCollection.getPage().getTotalItems();
    log.info("Query ({}={}) submitted: result with {} item(s) of {}", queryTypeLabel, query, currentItems, totalItems);
    return orderArchiveCollection;
  }

  private List<OrderArchive> getOrderArchiveList(String query, boolean withMetadata) {
    final List<OrderArchive> orderArchiveList = new ArrayList<>();

    for (final String aip : query.split(SolidifyConstants.FIELD_SEP)) {
      // Find AIP
      ArchivalInfoPackage archive;
      try {
        archive = this.aipRemoteResourceService.getAIPOnStorage(aip);
      } catch (final SolidifyResourceNotFoundException e) {
        log.error("AIP not found ({})", aip);
        continue;
      }
      if (withMetadata) {
        // Find metadata
        ArchiveMetadata md;
        try {
          md = this.archivePublicMetadataRemoteResourceService.getIndexMetadata(aip);
        } catch (final SolidifyResourceNotFoundException e) {
          log.error("Archive Metadata not found ({})", aip);
          continue;
        }
        orderArchiveList.add(new OrderArchive(archive, md));
      } else {
        orderArchiveList.add(new OrderArchive(archive));
      }
    }
    return orderArchiveList;
  }

  private List<OrderArchive> getMetadata(RestCollection<ArchiveMetadata> mds, boolean withAip) {
    final List<OrderArchive> orderArchiveList = new ArrayList<>();

    for (final ArchiveMetadata md : mds.getData()) {
      if (withAip) {
        ArchivalInfoPackage archive;
        try {
          archive = this.aipRemoteResourceService.getAIPOnStorage(md.getResId());

        } catch (final SolidifyResourceNotFoundException e) {
          log.error("AIP not found ({})", md.getResId());
          continue;
        } catch (final SolidifyRestException e) {
          if (e.getCause() instanceof HttpClientErrorException
                  && ((HttpClientErrorException) e.getCause()).getStatusCode() == HttpStatus.UNAUTHORIZED) {
            log.error("AIP '{}' skipped while getting list of OrderArchive as the current user is not authorized to access it", md.getResId());
            continue;
          } else {
            throw new SolidifyRuntimeException("unable to get AIP '\" + md.getResId() + \"'", e);
          }
        }

        orderArchiveList.add(new OrderArchive(archive, md));
      } else {
        orderArchiveList.add(new OrderArchive(md));
      }
    }
    return orderArchiveList;

  }

  private RestCollection<OrderArchive> search(MetadataVisibility mdVisibility, String query, String setQuery, boolean withAip,
          Pageable pageable) {
    RestCollection<ArchiveMetadata> mdCollection;
    switch (mdVisibility) {
      case PUBLIC -> mdCollection = this.archivePublicMetadataRemoteResourceService.getIndexMetadataList(query, setQuery, pageable);
      case RESTRICTED -> mdCollection = this.archivePrivateMetadataRemoteResourceService.getIndexMetadataList(query, setQuery, pageable);
      default -> throw new SolidifyCheckingException("Unsupported metadata visibility :" + mdVisibility);
    }

    final List<OrderArchive> orderArchiveList = this.getMetadata(mdCollection, withAip);
    return new RestCollection<>(orderArchiveList, mdCollection.getPage());
  }

  private RestCollection<OrderArchive> search(MetadataVisibility mdVisibility, String field, String from, String to, String set, boolean withAip,
          Pageable pageable) {
    RestCollection<ArchiveMetadata> mdCollection;
    switch (mdVisibility) {
      case PUBLIC -> mdCollection = this.archivePublicMetadataRemoteResourceService.getMetadataList(null, field, from, to, set, null, pageable);
      case RESTRICTED -> mdCollection = this.archivePrivateMetadataRemoteResourceService.getMetadataList(null, field, from, to, set, null,
              pageable);
      default -> throw new SolidifyCheckingException("Unsupported metadata visibility :" + mdVisibility);
    }
    final List<OrderArchive> orderArchiveList = this.getMetadata(mdCollection, withAip);
    return new RestCollection<>(orderArchiveList, mdCollection.getPage());
  }

  // **OAI-PMH ** //

  @Override
  public RestCollection<OAIRecord> searchOAIRecords(String from, String until, String setQuery, Pageable pageable) {
    final RestCollection<ArchiveMetadata> mdCollection = this.archivePublicMetadataRemoteResourceService.getMetadataList(
            null,
            DLCMConstants.AIP_LAST_UPDATE,
            from,
            until,
            setQuery,
            null,
            pageable);
    final List<OAIRecord> oaiRecordList = new ArrayList<>();
    for (final ArchiveMetadata md : mdCollection.getData()) {
      oaiRecordList.add(md);
    }
    return new RestCollection<>(oaiRecordList, mdCollection.getPage());

  }

  @Override
  @Cacheable(DLCMCacheNames.SEARCH)
  public boolean isInSet(String resId, String set, Pageable pageable) {
    final String query = "_id:" + resId;
    final RestCollection<OrderArchive> collection = this.search(MetadataVisibility.PUBLIC, query, set, false, pageable);
    return (collection.getPage().getTotalItems() == 1);
  }

}

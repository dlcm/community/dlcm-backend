/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DataFileStatusService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.client.ResourceAccessException;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.service.FallbackRestClientService;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMProperties.FileList;
import ch.dlcm.exception.DLCMExclusionException;
import ch.dlcm.exception.DLCMIgnoreException;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.DataFileChecksum.ChecksumOrigin;
import ch.dlcm.model.DataFileChecksum.ChecksumType;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.oais.InfoPackageInterface;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.viruscheck.VirusCheckService;

@Service
public class DataFileStatusService extends DLCMService {
  private static final String DATA_FILE_PREFIX = "{Data File} ";
  private static final String DATA_GET_ERROR = "Error in getting data";
  private static final Logger log = LoggerFactory.getLogger(DataFileStatusService.class);

  private final String[] checksumList;
  private final FileList excludeList;
  private final FileList ignoreList;
  private final long fileSizeLimit;

  private final FileFormatService fileFormatService;
  private final MetadataService metadataService;
  private final VirusCheckService virusCheckService;
  private final FallbackRestClientService restClientService;

  public DataFileStatusService(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          FallbackRestClientService restClientService,
          MetadataService metadataService,
          FileFormatService fileFormatService,
          VirusCheckService virusCheckService) {
    super(messageService, dlcmProperties);
    this.metadataService = metadataService;
    this.fileFormatService = fileFormatService;
    this.virusCheckService = virusCheckService;
    this.restClientService = restClientService;
    this.checksumList = dlcmProperties.getParameters().getChecksumList();
    this.excludeList = dlcmProperties.getParameters().getExcludeList();
    this.ignoreList = dlcmProperties.getParameters().getIgnoreList();
    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit().toBytes();
  }

  public boolean getData(AbstractDataFile<?, ?> dataFile, String preparationPath) {
    final URI srcUri = dataFile.getSourceData();
    Path destData = dataFile.defineTargetFile(preparationPath);

    try {
      switch (srcUri.getScheme()) {
        case "http", "https" -> {
          this.logDataFileMessage(LogLevel.INFO, dataFile, "will be downloaded from " + dataFile.getSourceData() + " to " + destData);
          this.checkAvailableDiskSpace(srcUri, destData);
          if (dataFile instanceof DepositDataFile) {
            this.restClientService.downloadContent(srcUri.toString(), destData, TokenUsage.WITHOUT_TOKEN);
          } else {
            this.restClientService.downloadContent(srcUri.toString(), destData, TokenUsage.WITH_TOKEN);
          }
          this.logDataFileMessage(LogLevel.INFO, dataFile, "file successfully downloaded");
          dataFile.setFinalData(destData.toUri());
          return true;
        }
        case "file" -> {
          this.logDataFileMessage(LogLevel.INFO, dataFile, "will be moved from " + Paths.get(srcUri) + " to " + destData);
          this.checkAvailableDiskSpace(srcUri, destData);
          if (FileTool.moveFile(Paths.get(srcUri), destData)) {
            this.logDataFileMessage(LogLevel.INFO, dataFile, "file successfully moved");
            dataFile.setFinalData(destData.toUri());
            return true;
          }
        }
        default -> throw new SolidifyProcessingException(DATA_FILE_PREFIX + " Protocol '" + srcUri.getScheme() + "' not supported");
      }
    } catch (final FileAlreadyExistsException e) {
      this.logDataFileMessage(LogLevel.ERROR, dataFile, DATA_GET_ERROR, e);
      final String filePath = e.getFile();
      final String filename = Paths.get(filePath).getFileName().toString();
      throw new SolidifyProcessingException(this.messageService.get("datafile.error.already_exists", new Object[] { filename }));
    } catch (final IOException e) {
      this.logDataFileMessage(LogLevel.ERROR, dataFile, DATA_GET_ERROR, e);
      throw new SolidifyProcessingException(DATA_FILE_PREFIX + " Error in getting data: source=" + srcUri + " target=" + destData, e);
    } catch (final ResourceAccessException e) {
      this.logDataFileMessage(LogLevel.ERROR, dataFile, DATA_GET_ERROR, e);
      if (e.getCause() instanceof FileAlreadyExistsException fileAlreadyExistsException) {
        final String filePath = fileAlreadyExistsException.getFile();
        final String filename = Paths.get(filePath).getFileName().toString();
        throw new SolidifyProcessingException(this.messageService.get("datafile.error.already_exists", new Object[] { filename }));
      } else {
        throw new SolidifyProcessingException(DATA_FILE_PREFIX + " Error in getting data: source=" + srcUri + " target=" + destData);
      }
    }
    return false;
  }

  private void checkAvailableDiskSpace(URI srcUri, Path destPath) {
    try {
      long fileSize = this.getFileSize(srcUri);
      long usableSpace = destPath.toFile().getParentFile().getUsableSpace();
      if (usableSpace < fileSize) {
        if (log.isErrorEnabled()) {
          log.error("There isn't enough disk space to save file from {} to {} (file size: {}, space available: {})", srcUri, destPath,
                  StringTool.formatSmartSize(fileSize), StringTool.formatSmartSize(usableSpace));
        }
        throw new SolidifyProcessingException(this.messageService.get("datafile.error.insufficient_space"));
      } else if (log.isDebugEnabled()) {
        log.debug("There is enough disk space to save file from {} to {} (file size: {}, space available: {})", srcUri, destPath,
                StringTool.formatSmartSize(fileSize),
                StringTool.formatSmartSize(usableSpace));
      }
    } catch (SolidifyProcessingException e) {
      throw e;
    } catch (Exception e) {
      log.warn("unable to check available disk space to save file from {} to {}: {}", srcUri, destPath, e.getMessage());
    }
  }

  private long getFileSize(URI srcUri) {
    return switch (srcUri.getScheme()) {
      case "file" -> FileTool.getSize(Paths.get(srcUri));
      case "http", "https" -> this.restClientService.headContentLength(srcUri.toString(), TokenUsage.WITH_TOKEN);
      default -> throw new SolidifyProcessingException(DATA_FILE_PREFIX + " Protocol '" + srcUri.getScheme() + "' not supported");
    };
  }

  @Transactional
  public <T extends AbstractDataFile<?, ?>> T processDataFile(String location, ResourceService<T> resourceService, String id) {
    T df = resourceService.findOne(id);

    this.logDataFileMessage(LogLevel.INFO, df, "will be processed");

    try {
      switch (df.getStatus()) {
        case RECEIVED -> this.processWhenReceived(df);
        case TO_PROCESS -> this.processWhenToProcess(df, location);
        case PROCESSED -> this.processWhenProcessed(df);
        case CHANGE_DATA_CATEGORY -> this.processWhenChangingDataCategory(df, location);
        case CHANGE_RELATIVE_LOCATION -> this.processWhenChangingRelativeLocation(df);
        case CLEANING -> this.processWhenCleaning(df);
        case CHECK_COMPLIANCE, CHECK_COMPLIANCE_CLEANED -> this.processWhenCompliance(df);
        default -> {
          // Do nothing
        }
      }
    } catch (final DLCMIgnoreException e) {
      df.setStatusWithMessage(DataFileStatus.IGNORED_FILE, e.getMessage());
    } catch (final DLCMExclusionException e) {
      df.setStatusWithMessage(DataFileStatus.EXCLUDED_FILE, e.getMessage());
    } catch (final NoSuchAlgorithmException e) {
      df.setStatusWithMessage(DataFileStatus.IN_ERROR,
              this.messageService.get("datafile.error.checksum") + " [" + e.getMessage() + "]");
    } catch (final SolidifyCheckingException e) {
      String errorMessage = e.getMessage();
      if (e.getCause() != null) {
        errorMessage += " (" + e.getCause().getMessage() + ")";
      }
      df.setStatusWithMessage(DataFileStatus.IN_ERROR,
              this.messageService.get("datafile.error.technicalvalidation") + " [" + errorMessage + "]");
    } catch (final SolidifyProcessingException e) {
      String errorMessage = e.getMessage();
      if (e.getCause() != null) {
        errorMessage += " (" + e.getCause().getMessage() + ")";
      }
      df.setStatusWithMessage(DataFileStatus.IN_ERROR,
              this.messageService.get("datafile.error.processing") + " [" + errorMessage + "]");
    } catch (final SolidifyFileDeleteException e) {
      df.setStatusWithMessage(DataFileStatus.IN_ERROR,
              this.messageService.get("datafile.error.cannotclean", new Object[] { df.getFinalData() }));
    } catch (final Exception e) {
      this.logDataFileMessage(LogLevel.ERROR, df, "processing error", e);
      df.setStatusWithMessage(DataFileStatus.IN_ERROR,
              this.messageService.get("datafile.error", new Object[] { e.getMessage() }));
    }

    this.logProcessedDataFile(df);

    try {
      df = resourceService.save(df);
      this.logDataFileMessage(LogLevel.TRACE, df, "saved in database");
    } catch (final Exception e) {
      this.logDataFileMessage(LogLevel.ERROR, df, "processing error", e);
    }

    return df;
  }

  private <T extends AbstractDataFile<?, ?>> void processWhenCompliance(T df) throws IOException {
    this.checkCompliance(df);
    DataFileStatus statusToSet = df.getStatus().equals(DataFileStatus.CHECK_COMPLIANCE) ? DataFileStatus.CHECKED_COMPLIANCE
            : DataFileStatus.CHECKED_COMPLIANCE_CLEANED;
    df.setFileStatus(statusToSet);
  }

  private <T extends AbstractDataFile<?, ?>> void processWhenCleaning(T df) {
    this.logDataFileMessage(LogLevel.INFO, df, "will be deleted: " + df.getFinalData());
    this.deleteFile(df.getFinalData());
    df.setFileStatus(DataFileStatus.CLEANED);
  }

  private <T extends AbstractDataFile<?, ?>> void processWhenChangingRelativeLocation(T df) throws IOException {
    this.moveRelativeLocation(df);
    df.setFileStatus(DataFileStatus.PROCESSED);
  }

  private <T extends AbstractDataFile<?, ?>> void processWhenChangingDataCategory(T df, String location) throws IOException {
    this.changeDataCategory(location, df);
    this.verifyDataFile(df);
    df.setFileStatus(DataFileStatus.PROCESSED);
  }

  private <T extends AbstractDataFile<?, ?>> void processWhenProcessed(T df) throws IOException, NoSuchAlgorithmException {
    df.setComplianceLevel(ComplianceLevel.NO_COMPLIANCE);
    // Check virus
    this.verifyVirus(df);
    // Identify file for check metadata/package
    this.verifyDataFile(df);
    // Compute checksum
    this.verifyChecksum(df);
    // Verify exclusion list
    this.logDataFileMessage(LogLevel.INFO, df, "will be verified against exclusion list");
    this.verifyFileList(df, this.excludeList, new DLCMExclusionException("Exclusion"));
    this.logDataFileMessage(LogLevel.DEBUG, df, "successfully verified against exclusion list");
    // Verify ignore list
    this.logDataFileMessage(LogLevel.INFO, df, "will be verified against ignore list");
    this.verifyFileList(df, this.ignoreList, new DLCMIgnoreException("Ignore"));
    this.logDataFileMessage(LogLevel.DEBUG, df, "successfully verified against ignore list");
    df.setFileStatus(DataFileStatus.READY);
  }

  private <T extends AbstractDataFile<?, ?>> void processWhenReceived(T df) {
    if (df.getFinalData() != null) {
      df.setFileStatus(DataFileStatus.PROCESSED);
    } else if (df.checkSource()) {
      df.setFileStatus(DataFileStatus.TO_PROCESS);
    } else {
      df.setStatusWithMessage(DataFileStatus.IN_ERROR, this.messageService.get("datafile.error.check"));
    }
  }

  private <T extends AbstractDataFile<?, ?>> void processWhenToProcess(T df, String location) {
    this.checkPackageDirectory(location, df.getInfoPackage().getResId());
    if (this.getData(df, location + "/" + df.getInfoPackage().getResId())) {
      df.setFileStatus(DataFileStatus.PROCESSED);
    } else {
      df.setStatusWithMessage(DataFileStatus.IN_ERROR, this.messageService.get("datafile.error.download"));
    }
    if (df.getDataCategory() == DataCategory.Package
            && (df.getDataType() == DataCategory.UpdatePackage
            || df.getDataType() == DataCategory.UpdatedMetadata)) {
      if (df.getInfoPackage() instanceof Deposit deposit) {
        deposit.setContainsUpdatedMetadata(true);
      } else {
        ((InfoPackageInterface<?>) df.getInfoPackage()).getInfo().setContainsUpdatedMetadata(true);
      }
      df.setFileStatus(DataFileStatus.PROCESSED);
    }
  }

  private boolean checkPackageDirectory(String location, String packageId) {
    final Path p = Paths.get(location, packageId);
    if (!p.toFile().exists()) {
      try {
        Files.createDirectories(p);
      } catch (final IOException e) {
        throw new SolidifyProcessingException(this.messageService.get("package.error.check_directory", new Object[] { packageId, location }), e);
      }
    }
    return true;
  }

  private SolidifyRuntimeException getException(SolidifyRuntimeException exception, String message, Object[] args) {
    if (exception instanceof DLCMExclusionException) {
      return new DLCMExclusionException(this.messageService.get("datafile.error.excluded." + message, args));
    }
    if (exception instanceof DLCMIgnoreException) {
      return new DLCMIgnoreException(this.messageService.get("datafile.error.ignored." + message, args));
    }
    return new SolidifyRuntimeException(message);
  }

  private <T extends AbstractDataFile<?, ?>> void addEventHistory(T df, DataFileStatus status, String sizeLimit) {
    final StatusHistory stsHistory = new StatusHistory(df.getClass().getSimpleName(), df.getResId(), status.toString());
    stsHistory.setDescription(this.messageService.get("datafile.status." + stsHistory.getStatus().toLowerCase(), new Object[] { sizeLimit }));
    SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);
  }

  private <T extends AbstractDataFile<?, ?>> void moveRelativeLocation(T df) throws IOException {
    final URI currentFinalDataURI = df.getFinalData();
    final String currentFinalDataString = currentFinalDataURI.toString();
    final String categoryPrefix = df.getInitialPath();

    final String basePath = currentFinalDataString.substring(0, currentFinalDataString.indexOf(categoryPrefix) + categoryPrefix.length());

    String relativeLocation = df.getRelativeLocation() + (df.getRelativeLocation().equals("/") ? "" : "/") + df.getFileName();
    String[] listParams = relativeLocation.split("/");
    String relativeLocationEncoded = Arrays.stream(listParams)
            .map(p -> URLEncoder.encode(p, StandardCharsets.UTF_8).replace("+", DLCMConstants.PERCENT_ENCODING_SPACE_CHAR))
            .collect(Collectors.joining("/"));
    String destinationFinalDataString = basePath + relativeLocationEncoded;
    final URI destictionFinalDataURI = URI.create(destinationFinalDataString);

    this.logDataFileMessage(LogLevel.INFO, df, "will be moved from " + currentFinalDataString + " to " + destinationFinalDataString);
    FileTool.moveFile(Paths.get(currentFinalDataURI), Paths.get(destictionFinalDataURI));
    df.setFinalData(destictionFinalDataURI);
  }

  private <T extends AbstractDataFile<?, ?>> void changeDataCategory(String location, T df) throws IOException {
    final URI finalDataURI = df.getFinalData();
    final String prefixDataCategory = Paths.get(location, df.getInfoPackage().getResId()).toString();

    final Path destinationPath = df.defineTargetFile(prefixDataCategory);
    this.logDataFileMessage(LogLevel.INFO, df, "change category to " + df.getDataCategory() + " and data file to: " + df.getDataType());
    if (!Paths.get(finalDataURI).equals(destinationPath)) {
      FileTool.moveFile(Paths.get(finalDataURI), destinationPath);
      df.setFinalData(destinationPath.toUri());
    }
  }

  private <T extends AbstractDataFile<?, ?>> boolean verifyChecksum(T df) throws NoSuchAlgorithmException, IOException {
    // Checksum computation
    if (this.checksumList.length == 0) {
      return true;
    }

    // Compute all checksums
    String[] checksums;
    try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(Paths.get(df.getFinalData()).toFile()),
            SolidifyConstants.BUFFER_SIZE)) {
      this.logDataFileMessage(LogLevel.INFO, df, "checksums " + Arrays.toString(this.checksumList) + " will be calculated and verified");
      checksums = ChecksumTool.computeChecksum(inputStream, this.checksumList);
    }
    final List<DataFileChecksum> existingChecksums = df.getChecksums();

    // Check if exist => compare else add it
    for (int i = 0; i < this.checksumList.length; i++) {
      if (existingChecksums == null || existingChecksums.isEmpty()) {
        df.getChecksums().add(new DataFileChecksum(ChecksumAlgorithm.valueOf(this.checksumList[i]), ChecksumType.COMPLETE, ChecksumOrigin.DLCM,
                checksums[i]));
      } else {
        boolean found = false;
        for (final DataFileChecksum checksum : existingChecksums) {
          if (checksum.getChecksumAlgo() == ChecksumAlgorithm.valueOf(this.checksumList[i])
                  && checksum.getChecksumType() == ChecksumType.COMPLETE) {
            found = true;
            if (!checksum.getChecksum().equals(checksums[i])) {
              this.logDataFileMessage(LogLevel.WARN, df, checksum.getChecksumAlgo() + " checksum comparison failed");
              throw new SolidifyRuntimeException("Invalid checksum " + checksum.getChecksumAlgo() + " consistency");
            }
          }
        }
        if (!found) {
          df.getChecksums().add(new DataFileChecksum(ChecksumAlgorithm.valueOf(this.checksumList[i]), ChecksumType.COMPLETE, ChecksumOrigin.DLCM,
                  checksums[i]));
        }
      }
    }

    this.logDataFileMessage(LogLevel.DEBUG, df, "checksums successfully calculated and verified");
    return true;
  }

  private <T extends AbstractDataFile<?, ?>> void verifyDataFile(T df) throws IOException {
    // Check format with File Format tool
    this.logDataFileMessage(LogLevel.INFO, df, "format will be checked");
    this.fileFormatService.identifyFormat(df.getResId(), df);

    this.checkPackageMetadataFile(df);
  }

  private <T extends AbstractDataFile<?, ?>> void checkCompliance(T df) throws IOException {
    // Save original compliance level
    ComplianceLevel originalCompliance = df.getComplianceLevel();
    // Check format with File Format tool
    this.logDataFileMessage(LogLevel.INFO, df, "format will be checked");
    this.fileFormatService.assessComplianceLevel(df.getResId(), df);

    this.checkPackageMetadataFile(df);

    // Check compliance is correct
    if (!originalCompliance.equals(df.getComplianceLevel())) {
      throw new SolidifyCheckingException(
              this.messageService.get("datafile.error.wrong_compliance", new Object[] { originalCompliance, df.getComplianceLevel() }));
    }

  }

  private <T extends AbstractDataFile<?, ?>> void checkPackageMetadataFile(T df) throws IOException {
    // Additional specific check
    if (df.getDataCategory() == DataCategory.Package) {
      this.logDataFileMessage(LogLevel.INFO, df, "is a DataCategory.Package and will be specifically checked");
      switch (df.getDataType()) {
        case Metadata, UpdatedMetadata -> this.metadataService.checkDlcmMetadata(df.getInfoPackage().getMetadataVersion(), df);
        case CustomMetadata -> this.metadataService.checkCustomMetadata(df);
        default -> this.fileFormatService.checkPackage(df);
      }
    }
  }

  private <T extends AbstractDataFile<?, ?>> void verifyFileList(T df, FileList fileList, SolidifyRuntimeException exception) {
    // Skip check for OAIS information packages
    if (df.getDataCategory() == DataCategory.Package
            && (df.getDataType() == DataCategory.InformationPackage
            || df.getDataType() == DataCategory.UpdatePackage)) {
      return;
    }
    // Check PUID if data file has a PUID
    if (df.getFileFormat() != null && df.getFileFormat().getPuid() != null) {
      for (final String puid : fileList.getPuids()) {
        if (df.getFileFormat().getPuid().equals(puid)) {
          throw this.getException(exception, "puid", new Object[] { puid });
        }
      }
    }
    // Check file name
    final AntPathMatcher matcher = new AntPathMatcher();
    for (final String filePattern : fileList.getFiles()) {
      if (matcher.match(filePattern, df.getFullFileName())) {
        throw this.getException(exception, "file", new Object[] { filePattern });
      }
    }
  }

  private <T extends AbstractDataFile<?, ?>> void verifyVirus(T df) throws IOException {
    // Check if file size is under the limit defined in configuration
    long fileSize = FileTool.getSize(df.getFinalData());
    if (fileSize < this.fileSizeLimit) {
      this.logDataFileMessage(LogLevel.INFO, df, "will be checked for virus (size: " + StringTool.formatSmartSize(fileSize) + ")");
      try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(Paths.get(df.getFinalData()).toFile()),
              SolidifyConstants.BUFFER_SIZE)) {
        this.virusCheckService.checkVirus(df.getResId(), df, inputStream);
      }
    } else {
      this.logDataFileMessage(LogLevel.INFO, df, "is too big to be checked for virus (size: " + StringTool.formatSmartSize(fileSize) + ")");
      this.addEventHistory(df, DataFileStatus.VIRUS_SKIPPED,
              StringTool.formatSmartSize(this.fileSizeLimit));
    }
  }
}

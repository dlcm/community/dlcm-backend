/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - NoFileFormatTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.fileformat.notool;

import java.io.InputStream;

import org.json.JSONObject;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.FileFormat;

@Profile("ff-notool")
@Service
public class NoFileFormatTool extends FileFormatService {

  private final String applicationName;
  private final String applicationVersion;
  private final String[] goldenFormats;

  public NoFileFormatTool(DLCMProperties dlcmProperties, MessageService messageService) {
    super(dlcmProperties, messageService);
    this.applicationName = dlcmProperties.getAppConfig().getApplicationName();
    this.applicationVersion = dlcmProperties.getAppConfig().getApplicationVersion();
    this.goldenFormats = dlcmProperties.getParameters().getGoldenFormats();
  }

  @Override
  public JSONObject check() {
    final JSONObject check = new JSONObject();
    check.put("info", "Embedded tool based on Java native functions");
    return check;
  }

  @Override
  public String getToolDescription() {
    return "DLCM tool of " + this.applicationName;
  }

  @Override
  public String getToolName() {
    return "DLCM File Format";
  }

  @Override
  public String getToolPuid() {
    return null;
  }

  @Override
  public String getToolVersion() {
    return this.applicationVersion;
  }

  @Override
  public String[] listGoldenFormats() {
    return this.goldenFormats;
  }

  @Override
  public String runTool(AbstractDataFile<?, ?> file) {
    return null;
  }

  @Override
  public String runTool(AbstractDataFile<?, ?> file, InputStream inputStream) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setAverageCompliance(AbstractDataFile<?, ?> df, FileFormat fileformat) {
    // Do nothing
  }

  @Override
  public void setWeakCompliance(AbstractDataFile<?, ?> file, FileFormat fileFormat) {
    // Set mime type
    this.simpleSetFileContentType(file, fileFormat);

    // Set data file size
    this.simpleSetFileSize(file);
  }
}

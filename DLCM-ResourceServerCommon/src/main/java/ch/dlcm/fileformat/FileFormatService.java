/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - FileFormatService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.fileformat;

import java.io.IOException;
import java.net.URLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.FileFormat;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.Tool;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.service.ToolService;

public abstract class FileFormatService extends ToolService {
  private static final Logger log = LoggerFactory.getLogger(FileFormatService.class);

  private static final String DATA_FILE_STATUS = "datafile.status.";

  private final long fileSizeLimit;

  protected FileFormatService(DLCMProperties dlcmProperties, MessageService messageService) {
    super(messageService, dlcmProperties);
    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit().toBytes();
  }

  // *********************************
  // ** function to identify format **
  // *********************************

  public void identifyFormat(String resId, AbstractDataFile<?, ?> file) throws IOException {
    // Init data file
    final FileFormat fileFormat = this.initFileFormat(file);
    final Tool tool = fileFormat.getTool();

    StatusHistory stsHistory;

    // Run tool & Set details
    fileFormat.setDetails(this.runTool(file));

    try {
      final long fileSize = FileTool.getSize(file.getPath());

      if (fileSize < this.getMaxSupportedFileSize()) {
        if (log.isInfoEnabled()) {
          log.info("file {}: format will be checked by {} (size: {})", file.getPath(), this.getClass().getSimpleName(),
                  StringTool.formatSmartSize(fileSize));
        }
        this.setWeakCompliance(file, fileFormat);
        file.setComplianceLevel(ComplianceLevel.WEAK_COMPLIANCE);
        this.setAverageCompliance(file, fileFormat);
        file.setComplianceLevel(ComplianceLevel.AVERAGE_COMPLIANCE);
        // Log File Format identification
        stsHistory = new StatusHistory(file.getClass().getSimpleName(), resId, DataFileStatus.FILE_FORMAT_IDENTIFIED.toString());
        stsHistory.setDescription(this.messageService
                .get(DATA_FILE_STATUS + stsHistory.getStatus().toLowerCase(), new Object[] { tool.getName() + " " + tool.getVersion() }));
      } else {
        if (log.isWarnEnabled()) {
          log.warn("file {} is too big to be checked by {} (size: {})", file.getPath(), this.getClass().getSimpleName(),
                  StringTool.formatSmartSize(fileSize));
        }

        this.simpleSetFileSize(file);
        this.simpleSetFileContentType(file, fileFormat);
        file.setComplianceLevel(ComplianceLevel.NOT_ASSESSED);
        // Log no file format identification
        stsHistory = new StatusHistory(file.getClass().getSimpleName(), resId, DataFileStatus.FILE_FORMAT_SKIPPED.toString());
        stsHistory.setDescription(this.messageService.get(DATA_FILE_STATUS + stsHistory.getStatus().toLowerCase(),
                new Object[] { StringTool.formatSmartSize(this.getMaxSupportedFileSize()) }));
      }
      if (this.isGoldenFormat(fileFormat.getPuid())) {
        file.setComplianceLevel(ComplianceLevel.FULL_COMPLIANCE);
      }
    } catch (final SolidifyCheckingException e) {
      // force to set Content-type
      this.simpleSetFileContentType(file, fileFormat);
      // Log format conflict
      stsHistory = new StatusHistory(file.getClass().getSimpleName(), resId, DataFileStatus.FILE_FORMAT_UNKNOWN.toString());
      stsHistory.setDescription(this.messageService
              .get(DATA_FILE_STATUS + stsHistory.getStatus().toLowerCase(), new Object[] { tool.getName() + " " + tool.getVersion() }));
    }
    // Send message
    SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);
  }

  // *****************************************
  // ** function to verify compliance level **
  // *****************************************

  public ComplianceLevel assessComplianceLevel(String resId, AbstractDataFile<?, ?> file) {
    final FileFormat fileFormat = file.getFileFormat();
    final long fileSize = file.getFileSize();

    if (fileSize < this.getMaxSupportedFileSize()) {
      if (log.isInfoEnabled()) {
        log.info("file {}: format will be checked by {} (size: {})", resId, this.getClass().getSimpleName(),
                StringTool.formatSmartSize(fileSize));
      }
      this.setWeakCompliance(file, fileFormat);
      file.setComplianceLevel(ComplianceLevel.WEAK_COMPLIANCE);
      this.setAverageCompliance(file, fileFormat);
      file.setComplianceLevel(ComplianceLevel.AVERAGE_COMPLIANCE);
    } else {
      if (log.isWarnEnabled()) {
        log.warn("file {} is too big to be checked by {} (size: {})", resId, this.getClass().getSimpleName(),
                StringTool.formatSmartSize(fileSize));
      }
      file.setComplianceLevel(ComplianceLevel.NOT_ASSESSED);
    }
    if (this.isGoldenFormat(fileFormat.getPuid())) {
      file.setComplianceLevel(ComplianceLevel.FULL_COMPLIANCE);
    }
    return file.getComplianceLevel();
  }

  public void checkPackage(AbstractDataFile<?, ?> df) {
    df.setComplianceLevel(ComplianceLevel.WEAK_COMPLIANCE);
    // Set data file size
    final long size = FileTool.getSize(df.getFinalData());
    this.setSize(df, size);
    df.setComplianceLevel(ComplianceLevel.AVERAGE_COMPLIANCE);
    final ZipTool zip = new ZipTool(df.getFinalData());
    if (zip.checkZip()) {
      df.setComplianceLevel(ComplianceLevel.FULL_COMPLIANCE);
    }
  }

  public abstract String[] listGoldenFormats();

  public long getMaxSupportedFileSize() {
    return this.fileSizeLimit;
  }

  // *********************
  // ** Default Methods **
  // *********************

  public abstract void setAverageCompliance(AbstractDataFile<?, ?> df, FileFormat fileformat);

  public void setContentType(FileFormat fileFormat, String contentType) {
    if (fileFormat.getContentType() == null) {
      fileFormat.setContentType(contentType);
    } else if (!fileFormat.getContentType().equals(contentType)) {
      throw new SolidifyCheckingException(
              this.messageService.get("fileformat.error.contenttype.wrong", new Object[] { fileFormat.getContentType(), contentType }));
    }
  }

  public void setFormat(FileFormat fileFormat, String format) {
    if (fileFormat.getFormat() == null) {
      fileFormat.setFormat(format);
    } else if (!fileFormat.getFormat().equals(format)) {
      throw new SolidifyCheckingException(
              this.messageService.get("fileformat.error.format.wrong", new Object[] { fileFormat.getFormat(), format }));
    }
  }

  public void setPuid(FileFormat fileFormat, String puid) {
    if (puid != null) {
      if (fileFormat.getPuid() == null) {
        fileFormat.setPuid(puid);
      } else if (!fileFormat.getPuid().equals(puid)) {
        throw new SolidifyCheckingException(
                this.messageService.get("fileformat.error.version.wrong", new Object[] { fileFormat.getPuid(), puid }));
      }
    }
  }

  public void setSize(AbstractDataFile<?, ?> df, long size) {
    if (df.getFileSize() == 0) {
      df.setFileSize(size);
    } else if (df.getFileSize() != size) {
      throw new SolidifyCheckingException(this.messageService.get("fileformat.error.size.wrong", new Object[] { df.getFileSize(), size }));
    }
  }

  public void setVersion(FileFormat fileFormat, String version) {
    if (version != null) {
      if (fileFormat.getVersion() == null) {
        fileFormat.setVersion(version);
      } else if (!fileFormat.getVersion().equals(version)) {
        throw new SolidifyCheckingException(
                this.messageService.get("fileformat.error.version.wrong", new Object[] { fileFormat.getVersion(), version }));
      }
    }
  }

  public abstract void setWeakCompliance(AbstractDataFile<?, ?> file, FileFormat fileformat);

  protected void simpleSetFileContentType(AbstractDataFile<?, ?> file, FileFormat fileFormat) {
    String contentType;
    if (file instanceof DepositDataFile) {
      final String sourceData = file.getSourceData().toString();
      contentType = URLConnection.guessContentTypeFromName(sourceData.substring(sourceData.lastIndexOf("/") + 1));
      fileFormat.setContentType(contentType);
    } else {
      contentType = FileTool.getContentType(file.getPath()).toString();
    }
    this.setContentType(fileFormat, contentType);
  }

  protected void simpleSetFileSize(AbstractDataFile<?, ?> file) {
    try {
      final long size = FileTool.getSize(file.getPath());
      this.setSize(file, size);
    } catch (final SolidifyRuntimeException e) {
      this.setSize(file, 0);
    }
  }

  private FileFormat initFileFormat(AbstractDataFile<?, ?> file) {
    if (file.getFileFormat() == null) {
      file.setFileFormat(new FileFormat());
    }
    final FileFormat fileFormat = file.getFileFormat();
    this.setTool(fileFormat);
    return fileFormat;
  }

  private boolean isGoldenFormat(String puid) {
    for (final String f : this.listGoldenFormats()) {
      if (f.equals(puid)) {
        return true;
      }
    }
    return false;
  }

}

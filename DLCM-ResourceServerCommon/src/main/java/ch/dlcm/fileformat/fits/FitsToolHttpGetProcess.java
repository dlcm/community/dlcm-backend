/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - FitsToolHttpGetProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.fileformat.fits;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.NoTokenRestClientTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.AbstractDataFile;

@Profile({ "ff-fits-get", "ff-fits" })
@Service
public class FitsToolHttpGetProcess extends FitsTool {

  private final String fileFormatTool;
  private final RestTemplate restClt;

  public FitsToolHttpGetProcess(DLCMProperties dlcmProperties, MessageService messageService, NoTokenRestClientTool restClientTool) {
    super(dlcmProperties, messageService, restClientTool);
    this.fileFormatTool = dlcmProperties.getFileFormat().getTool();
    this.restClt = restClientTool.getClientWithPlusEncoderInterceptor();
  }

  @Override
  public String runTool(AbstractDataFile<?, ?> file) {
    final Map<String, Object> params = new HashMap<>();
    params.put("file", file.getPath().toString());
    return this.restClt.getForObject(this.fileFormatTool + "/" + FitsTool.EXAMINE + "?file={file}", String.class, params);
  }

  @Override
  public String runTool(AbstractDataFile<?, ?> file, InputStream inputStream) {
    throw new UnsupportedOperationException();
  }

}

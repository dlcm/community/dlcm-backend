/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - FitsTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.fileformat.fits;

import java.io.StringReader;
import java.util.List;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyToolException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.NoTokenRestClientTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.FileFormat;
import ch.dlcm.model.xml.fits.Fits;
import ch.dlcm.model.xml.fits.FitsMetadataType;
import ch.dlcm.model.xml.fits.IdentificationType.Identity;
import ch.dlcm.model.xml.fits.Tool;

public abstract class FitsTool extends FileFormatService {
  private static final Logger log = LoggerFactory.getLogger(FitsTool.class);

  private static final String VERSION_PREFIX = "build.version=";
  public static final String EXAMINE = "examine";

  private final String fileFormatTool;
  private final String[] goldenFormats;
  private final String[] defaultTools;
  private JAXBContext jaxbContext;
  private final RestTemplate restClt;
  private final String version;

  protected FitsTool(DLCMProperties dlcmProperties, MessageService messageService, NoTokenRestClientTool restClientTool) {
    super(dlcmProperties, messageService);
    this.fileFormatTool = dlcmProperties.getFileFormat().getTool();
    this.goldenFormats = dlcmProperties.getParameters().getGoldenFormats();
    this.defaultTools = dlcmProperties.getFileFormat().getDefaultTools();
    this.restClt = restClientTool.getClientWithPlusEncoderInterceptor();
    try {
      this.jaxbContext = JAXBContext.newInstance(Fits.class);
    } catch (final JAXBException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
    this.version = this.setToolVersion();
  }

  @Override
  public JSONObject check() {
    final JSONObject check = new JSONObject();
    HttpStatusCode sts = HttpStatus.INTERNAL_SERVER_ERROR;
    try {
      sts = this.restClt.getForEntity(this.fileFormatTool + "/", String.class).getStatusCode();
    } catch (final Exception e) {
      log.error("Error in checking FITS", e);
    }
    check.put("url", this.fileFormatTool);
    check.put("running", HttpStatus.valueOf(sts.value()).getReasonPhrase());
    return check;
  }

  @Override
  public String getToolDescription() {
    return "File Information Tool Set";
  }

  @Override
  public String getToolName() {
    return "FITS";
  }

  @Override
  public String getToolPuid() {
    return null;
  }

  @Override
  public String getToolVersion() {
    return this.version;
  }

  @Override
  public String[] listGoldenFormats() {
    return this.goldenFormats;
  }

  @Override
  public void setAverageCompliance(AbstractDataFile<?, ?> file, FileFormat fileFormat) {
    final Fits fitsInfo = this.getFits(fileFormat.getDetails());
    // Set MD5 checksum
    fileFormat.setMd5(this.getFitsInfo(fitsInfo.getFileinfo().getFileInfoElements(), "md5checksum"));

    // Get file identity
    final Identity fileIdentity = this.getIdentiy(fitsInfo);
    // Set format
    this.setFormat(fileFormat, fileIdentity.getFormat());

    // Set Version
    this.setVersion(fileFormat, this.getIdentityVersion(fileIdentity));

    // Set PUID
    final String filePuid = this.getIdentityPuid(fileIdentity);
    if (filePuid != null) {
      final String puid = fileIdentity.getExternalIdentifier().get(0).getValue();
      this.setPuid(fileFormat, puid);
    }
  }

  @Override
  public void setWeakCompliance(AbstractDataFile<?, ?> file, FileFormat fileFormat) {
    final Fits fitsInfo = this.getFits(fileFormat.getDetails());
    // Set data file size
    final String sizeStr = this.getFitsInfo(fitsInfo.getFileinfo().getFileInfoElements(), "size");
    this.setSize(file, Long.parseLong(sizeStr));

    // Get file identity
    final Identity fileIdentity = this.getIdentiy(fitsInfo);
    // Set mime type
    this.setContentType(fileFormat, fileIdentity.getMimetype());
  }

  private Fits getFits(String fitsXml) {
    Unmarshaller um;
    try {
      um = this.jaxbContext.createUnmarshaller();
      return (Fits) um.unmarshal(new StringReader(fitsXml));
    } catch (final JAXBException e) {
      throw new SolidifyToolException("Error in getting FITS results", e);
    }
  }

  private String getFitsInfo(List<JAXBElement<FitsMetadataType>> list, String name) {
    for (final JAXBElement<FitsMetadataType> item : list) {

      if (name.equals(item.getName().getLocalPart())) {
        return item.getValue().getContent().get(0).toString();
      }
    }
    return null;
  }

  private String getIdentityPuid(Identity fileIdentity) {
    if (fileIdentity.getExternalIdentifier().isEmpty()) {
      return null;
    }
    return fileIdentity.getExternalIdentifier().get(0).getValue();
  }

  private String getIdentityVersion(Identity fileIdentity) {
    if (fileIdentity.getVersion().isEmpty()) {
      return null;
    }
    return fileIdentity.getVersion().get(0).getValue();
  }

  private Identity getIdentiy(Fits fitsInfo) {
    if (fitsInfo.getIdentification() != null) {
      final List<Identity> list = fitsInfo.getIdentification().getIdentity();
      if (list.size() == 1) {
        return list.get(0);
      }
      for (final Identity id : list) {
        for (final Tool t : id.getTool()) {
          for (final String defaultTool : this.defaultTools) {
            if (t.getToolname().equals(defaultTool)) {
              return id;
            }
          }
        }
      }
    }
    throw new SolidifyCheckingException(this.messageService.get("fileformat.error.fits.identity", new Object[] { this.defaultTools }));
  }

  protected String setToolVersion() {
    String v = this.restClt.getForObject(this.fileFormatTool + "/version", String.class);
    if (v == null) {
      throw new SolidifyRuntimeException("Request to getting tool version result in a null object");
    }
    v = v.trim();
    if (v.startsWith(VERSION_PREFIX)) {
      return v.substring(VERSION_PREFIX.length());
    }
    return v;
  }

}

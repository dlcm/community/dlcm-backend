/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - FitsToolHttpPostProcess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.fileformat.fits;

import java.io.InputStream;

import org.springframework.context.annotation.Profile;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.NoTokenRestClientTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.AbstractDataFile;

@Profile("ff-fits-post")
@Service
public class FitsToolHttpPostProcess extends FitsTool {

  private final String fileFormatTool;
  private final RestTemplate restClt;

  public FitsToolHttpPostProcess(DLCMProperties dlcmProperties, MessageService messageService, NoTokenRestClientTool restClientTool) {
    super(dlcmProperties, messageService, restClientTool);
    this.fileFormatTool = dlcmProperties.getFileFormat().getTool();
    this.restClt = restClientTool.getClientWithPlusEncoderInterceptor();
  }

  @Override
  public String runTool(AbstractDataFile<?, ?> file) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);

    FileSystemResource fileSystemResource = new FileSystemResource(file.getPath());
    MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
    body.add("datafile", fileSystemResource);

    HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
    return this.restClt.postForEntity(this.fileFormatTool + "/" + FitsTool.EXAMINE, requestEntity, String.class).getBody();
  }

  @Override
  public String runTool(AbstractDataFile<?, ?> file, InputStream inputStream) {
    throw new UnsupportedOperationException();
  }
}

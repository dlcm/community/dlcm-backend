/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DLCMAsyncConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableAsync
public class DLCMAsyncConfig extends AsyncConfigurerSupport {

  private final long defaultAsyncExecutionTimeout;

  public DLCMAsyncConfig(DLCMProperties dlcmProperties) {
    this.defaultAsyncExecutionTimeout = dlcmProperties.getParameters().getDefaultAsyncExecutionTimeout();
  }

  /**
   * Configure async support for Spring MVC.
   */
  @Bean
  public WebMvcConfigurer webMvcConfigurerAdapter(AsyncTaskExecutor taskExecutor) {

    return new WebMvcConfigurer() {
      @Override
      public void configureAsyncSupport(AsyncSupportConfigurer configurer) {

        // set no time limit for async executions to allow downloading very large files with StreamingResponseBody
        configurer.setDefaultTimeout(DLCMAsyncConfig.this.defaultAsyncExecutionTimeout).setTaskExecutor(taskExecutor);
      }
    };
  }
}

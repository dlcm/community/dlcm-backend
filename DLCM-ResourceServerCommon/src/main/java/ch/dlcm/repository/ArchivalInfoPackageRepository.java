/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchivalInfoPackageRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;

@Repository
public interface ArchivalInfoPackageRepository extends SolidifyRepository<ArchivalInfoPackage> {

  @Query("SELECT aip FROM ArchivalInfoPackage aip JOIN aip.info info WHERE info.status = :status")
  List<ArchivalInfoPackage> findByStatus(PackageStatus status);

  @Query("SELECT aip FROM ArchivalInfoPackage aip JOIN aip.collection collectionElement WHERE collectionElement.resId = :aipId")
  List<ArchivalInfoPackage> findCollectionsContainingAip(String aipId);

  @Query("SELECT COALESCE(SUM(df.fileSize), 0) FROM AipDataFile df WHERE df.infoPackage.resId = :aipId")
  long getSize(String aipId);

  @Query("SELECT COUNT(DISTINCT aip.resId) FROM ArchivalInfoPackage aip JOIN aip.orders o WHERE o.queryType = 'SIMPLE' AND o.status = 'READY'")
  int countAIPDownloadOfCompletedOrders();
}

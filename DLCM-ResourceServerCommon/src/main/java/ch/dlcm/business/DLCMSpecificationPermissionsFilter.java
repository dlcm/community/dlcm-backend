/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DLCMSpecificationPermissionsFilter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.Resource;

import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.specification.FilterableByOrganizationalUnitsSpecification;

@Service
public class DLCMSpecificationPermissionsFilter<T extends Resource> implements SpecificationPermissionsFilter<T> {

  protected TrustedOrganizationalUnitRemoteResourceService trustedOrganizationalUnitRemoteResourceService;

  public DLCMSpecificationPermissionsFilter(
          TrustedOrganizationalUnitRemoteResourceService trustedOrganizationalUnitRemoteResourceService) {
    this.trustedOrganizationalUnitRemoteResourceService = trustedOrganizationalUnitRemoteResourceService;
  }

  @Override
  public Specification<T> addPermissionsFilter(Specification<T> spec, Principal principal) {

    if (spec instanceof FilterableByOrganizationalUnitsSpecification) {
      spec = this.addOrganizationalUnitsFilter((FilterableByOrganizationalUnitsSpecification<T>) spec, principal);
    }

    return spec;
  }

  protected Specification<T> addOrganizationalUnitsFilter(FilterableByOrganizationalUnitsSpecification<T> spec, Principal principal) {

    List<OrganizationalUnit> organizationalUnits = this.trustedOrganizationalUnitRemoteResourceService.getAuthorizedOrganizationalUnit(
            principal);

    if (organizationalUnits == null) {
      organizationalUnits = new ArrayList<>();
    }

    spec.setOrganizationalUnits(organizationalUnits);

    return (Specification<T>) spec;
  }
}

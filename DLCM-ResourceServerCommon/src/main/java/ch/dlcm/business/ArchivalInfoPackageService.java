/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ArchivalInfoPackageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.message.AipCheckMessage;
import ch.dlcm.message.AipMessage;
import ch.dlcm.model.AipCheckLevel;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.repository.ArchivalInfoPackageRepository;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackUserRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.specification.ArchivalInfoPackageSpecification;

@Service
public class ArchivalInfoPackageService extends InfoPackageService<ArchivalInfoPackage> {

  private static final String NOT_EXIST = "validation.resource.notexist";
  private static final String UNIT_CLOSED = "validation.organizationalUnit.closed";
  private static final String NOTIFICATION_APPROVE_DISPOSAL_ORGUNIT = "archival.notification.approve.disposal.orgunit.request";
  private static final String NOTIFICATION_APPROVE_DISPOSAL = "archival.notification.approve.disposal.request";
  private static final String NOTIFICATION_ARCHIVE_COMPLETED = "archival.notification.completed";
  private static final String ARCHIVE_CHECK = "message.check.archive";

  private final FallbackOrganizationalUnitRemoteResourceService organizationalUnitService;
  private final TrustedNotificationRemoteResourceService notificationService;
  private final FallbackUserRemoteResourceService userService;

  public ArchivalInfoPackageService(DLCMProperties dlcmProperties,
          HttpRequestInfoProvider httpRequestInfoProvider,
          SpecificationPermissionsFilter<ArchivalInfoPackage> specificationPermissionFilter, HistoryService historyService,
          FallbackOrganizationalUnitRemoteResourceService organizationalUnitService,
          TrustedNotificationRemoteResourceService notificationService,
          FallbackUserRemoteResourceService userService) {
    super(dlcmProperties, httpRequestInfoProvider, specificationPermissionFilter, historyService);
    this.organizationalUnitService = organizationalUnitService;
    this.notificationService = notificationService;
    this.userService = userService;
  }

  @Override
  public void validateLinkedResources(ArchivalInfoPackage item, BindingResult errors) {

    /*
     * OrganizationalUnit
     */
    if (!StringTool.isNullOrEmpty(item.getInfo().getOrganizationalUnitId())) {
      try {
        final OrganizationalUnit subitem = this.organizationalUnitService.findOne(item.getInfo().getOrganizationalUnitId());
        if (!subitem.isOpen() &&
                (item.getInfo().getStatus().equals(PackageStatus.EDITING_METADATA)
                        || item.getInfo().getStatus().equals(PackageStatus.IN_PREPARATION))) {
          errors.addError(new FieldError(item.getClass().getSimpleName(), DLCMConstants.ORG_UNIT_ID_FIELD,
                  this.messageService.get(UNIT_CLOSED, new Object[] { item.getInfo().getOrganizationalUnitId() })));
        }
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(item.getClass().getSimpleName(), DLCMConstants.ORG_UNIT_ID_FIELD,
                this.messageService.get(NOT_EXIST, new Object[] { item.getInfo().getOrganizationalUnitId() })));
      }
    }
  }

  @Override
  protected Specification<ArchivalInfoPackage> addSpecificationFilter(Specification<ArchivalInfoPackage> spec) {
    return this.specificationPermissionFilter.addPermissionsFilter(spec, this.httpRequestInfoProvider.getPrincipal());
  }

  /*****************************************/

  @Override
  protected <S extends Resource> RemoteResourceService<S> getResourceService(Class<S> type) {

    if (type == OrganizationalUnit.class) {
      return (RemoteResourceService<S>) this.organizationalUnitService;
    }
    throw new UnsupportedOperationException("Missing resource service for type " + type.getName());
  }

  /**
   * Method use to save AIP when creating an order or AIPDownload to avoid to create notifications related to the AIP.
   */
  public ArchivalInfoPackage saveForOrders(ArchivalInfoPackage aip) {
    return super.save(aip);
  }

  @Override
  public ArchivalInfoPackage save(ArchivalInfoPackage aip) {

    final ArchivalInfoPackage result = super.save(aip);

    OrganizationalUnit organizationalUnit = this.organizationalUnitService.findOne(aip.getInfo().getOrganizationalUnitId());
    User updaterUser = this.userService.findByExternalUid(aip.getLastUpdate().getWho());
    String message;
    NotificationType notificationType;
    if (aip.getInfo().getStatus() == PackageStatus.DISPOSABLE) {
      if (Boolean.TRUE.equals(aip.getDispositionApproval())) {
        message = this.messageService.get(NOTIFICATION_APPROVE_DISPOSAL_ORGUNIT);
        notificationType = NotificationType.APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST;
      } else {
        message = this.messageService.get(NOTIFICATION_APPROVE_DISPOSAL);
        notificationType = NotificationType.APPROVE_DISPOSAL_REQUEST;
      }
      this.notificationService.createNotification(updaterUser, message, notificationType, organizationalUnit, aip.getResId());
    } else if (aip.getInfo().getStatus() == PackageStatus.COMPLETED
            && (!this.historyService.hasBeenCompleted(aip.getResId())
                    || this.historyService.hasBeenUpdatedSinceLastCompletedStatus(aip.getResId()))) {
      message = this.messageService.get(NOTIFICATION_ARCHIVE_COMPLETED, new Object[] { aip.getResId() });
      this.notificationService.createArchiveCompletedNotifications(aip, updaterUser, message, organizationalUnit);
    }
    return result;
  }

  public boolean putPackageInQueue(ArchivalInfoPackage aip, AipCheckLevel checkLevel, String createdBy) {
    if (!this.putPackageInProcessingQueue(aip)) {
      return this.putPackageInCheckingQueue(aip, checkLevel, createdBy);
    }
    return true;
  }

  public boolean putPackageInProcessingQueue(ArchivalInfoPackage aip) {
    if (aip.getInfo().getStatus() == PackageStatus.IN_PREPARATION
            || aip.getInfo().getStatus() == PackageStatus.REINDEXING
            || aip.getInfo().getStatus() == PackageStatus.DISPOSAL_APPROVED
            || aip.getInfo().getStatus() == PackageStatus.DISPOSAL_APPROVED_BY_ORGUNIT
            || aip.getInfo().getStatus() == PackageStatus.UPDATING_RETENTION
            || aip.getInfo().getStatus() == PackageStatus.METADATA_EDITION_PENDING
            || aip.getInfo().getStatus() == PackageStatus.METADATA_UPGRADE_PENDING
            || aip.getInfo().getStatus() == PackageStatus.COMPLIANCE_LEVEL_UPDATE_PENDING
            || aip.getInfo().getStatus() == PackageStatus.PACKAGE_REPLICATION_PENDING
            || aip.getInfo().getStatus() == PackageStatus.TOMBSTONE_REPLICATION_PENDING) {
      boolean isBigPackage = this.getSize(aip) > this.fileSizeLimit.toBytes();
      SolidifyEventPublisher.getPublisher().publishEvent(new AipMessage(aip.getResId(), isBigPackage));
      return true;
    }
    return false;
  }

  public boolean putPackageInCheckingQueue(ArchivalInfoPackage aip, AipCheckLevel checkLevel, String createdBy) {
    if (aip.getInfo().getStatus() == PackageStatus.CHECK_PENDING
            || aip.getInfo().getStatus() == PackageStatus.FIX_PENDING) {
      boolean isBigPackage = this.getSize(aip) > this.fileSizeLimit.toBytes();
      SolidifyEventPublisher.getPublisher().publishEvent(new AipCheckMessage(aip.getResId(), isBigPackage, checkLevel, createdBy));
      return true;
    }
    return false;
  }

  public long getSize(String aipId) {
    return ((ArchivalInfoPackageRepository) this.itemRepository).getSize(aipId);
  }

  public long getSize(ArchivalInfoPackage aip) {
    if (aip.getArchiveSize() > 0) {
      return aip.getArchiveSize();
    }
    return ((ArchivalInfoPackageRepository) this.itemRepository).getSize(aip.getResId());
  }

  public Result resumePackage(String aipId, AipCheckLevel defaultAipCheckLevel, String createdBy) {
    ArchivalInfoPackage aip = this.findOne(aipId);
    List<StatusHistory> lastProcess = this.historyService.getLastProcess(aipId);
    Result result = super.savePackageResumeStatus(aip);
    if (result.getStatus() == Result.ActionStatus.EXECUTED) {
      if (lastProcess.get(0).getDescription().equals(this.messageService.get(ARCHIVE_CHECK))) {
        this.putPackageInQueue(aip, AipCheckLevel.DEEP_CHECK, createdBy);
      } else {
        this.putPackageInQueue(aip, defaultAipCheckLevel, createdBy);
      }
    }
    return result;
  }

  public List<ArchivalInfoPackage> findByStatus(PackageStatus packageStatus) {
    final List<ArchivalInfoPackage> aipList = ((ArchivalInfoPackageRepository) this.itemRepository).findByStatus(packageStatus);
    this.completeEmbeddedResources(aipList);
    return aipList;
  }

  public List<ArchivalInfoPackage> findCollectionsContainingAip(String aipId) {
    return ((ArchivalInfoPackageRepository) this.itemRepository).findCollectionsContainingAip(aipId);
  }

  @Override
  public ArchivalInfoPackageSpecification getSpecification(ArchivalInfoPackage resource) {
    return new ArchivalInfoPackageSpecification(resource);
  }

  public int countAIPDownloadOfCompletedOrders() {
    return ((ArchivalInfoPackageRepository) this.itemRepository).countAIPDownloadOfCompletedOrders();
  }
}

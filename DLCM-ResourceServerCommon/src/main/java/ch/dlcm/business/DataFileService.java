/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - DataFileService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.repository.DataFileRepository;
import ch.dlcm.service.DataCategoryService;

public abstract class DataFileService<T extends AbstractDataFile<?, ?>> extends ResourceService<T> {

  private static final String FILENAME_DIFFERENT_FROM_URI = "validation.dataFile.filenameDifferentFromUri";

  protected static final String DATA_CATEGORY_FIELD = "dataCategory";
  protected static final String DATA_TYPE_FIELD = "dataType";

  private final DataCategoryService dataCategoryService;

  protected DataFileService(DataCategoryService dataCategoryService) {
    this.dataCategoryService = dataCategoryService;
  }

  /**
   * Find a dataFile by its packageId, relativeLocation and fileName. The comparison on
   * relativeLocation and file name is case sensitive.
   *
   * @param packageId
   * @param relativeLocation
   * @param fileName
   * @return
   */
  public Optional<AbstractDataFile<?, ?>> findByPackageIdAndRelativeLocationAndFileName(String packageId, String relativeLocation,
          String fileName) {
    List<AbstractDataFile<?, ?>> existingDataFiles = this.findByPackageIdAndRelativeLocationAndFileNameCaseInsensitive(packageId,
            relativeLocation,
            fileName);

    List<AbstractDataFile<?, ?>> matchingDataFiles = existingDataFiles.stream()
            .filter(df -> df.getRelativeLocation().equals(relativeLocation) && df.getFileName().equals(fileName)).toList();

    if (matchingDataFiles.size() == 1) {
      return Optional.of(matchingDataFiles.get(0));
    } else if (matchingDataFiles.isEmpty()) {
      return Optional.empty();
    } else {
      throw new SolidifyRuntimeException(
              "Duplicated files found for packageId=" + packageId + ", relativeLocation=" + relativeLocation + ", fileName=" + fileName);
    }
  }

  /**
   * Find a dataFile by its packageId and sourceData. The comparison on sourceData is case sensitive.
   *
   * @param packageId
   * @param sourceData
   * @return
   */
  public Optional<AbstractDataFile<?, ?>> findByPackageIdAndRelativeLocationAndSourceData(String packageId, URI sourceData) {
    List<AbstractDataFile<?, ?>> existingDataFiles = this.findByPackageIdAndSourceData(packageId, sourceData);

    List<AbstractDataFile<?, ?>> matchingDataFiles = existingDataFiles.stream()
            .filter(df -> df.getSourceData().equals(sourceData)).toList();

    if (matchingDataFiles.size() == 1) {
      return Optional.of(matchingDataFiles.get(0));
    } else if (matchingDataFiles.isEmpty()) {
      return Optional.empty();
    } else {
      throw new SolidifyRuntimeException(
              "Duplicated files found for packageId=" + packageId + ", sourceData=" + sourceData);
    }
  }

  /**
   * Find a dataFile by its packageId, relativeLocation and fileName. The comparison on
   * relativeLocation and file name is case insensitive.
   *
   * @param packageId
   * @param relativeLocation
   * @param fileName
   * @return
   */
  private List<AbstractDataFile<?, ?>> findByPackageIdAndRelativeLocationAndFileNameCaseInsensitive(String packageId, String relativeLocation,
          String fileName) {
    return ((DataFileRepository) this.itemRepository).findByInfoPackageResIdAndRelativeLocationIgnoreCaseAndFileNameIgnoreCase(packageId,
            relativeLocation, fileName);
  }

  /**
   * Find a dataFile by its packageId and sourceData. The comparison on sourceData is case sensitive.
   *
   * @param packageId
   * @param sourceData
   * @return
   */
  private List<AbstractDataFile<?, ?>> findByPackageIdAndSourceData(String packageId, URI sourceData) {
    return ((DataFileRepository) this.itemRepository).findByInfoPackageResIdAndSourceData(packageId, sourceData);
  }

  /**
   * Return true if no other dataFile exist with the same relativeLocation and same file name. The
   * comparison on relativeLocation and file name is case sensitive.
   *
   * @param dataFile
   * @return
   */
  public boolean isDataFileUniqueInPackageAndRelativeLocation(AbstractDataFile<?, ?> dataFile) {
    Optional<AbstractDataFile<?, ?>> existingDataFile = this.findByPackageIdAndRelativeLocationAndFileName(dataFile.getInfoPackage().getResId(),
            dataFile.getRelativeLocation(), dataFile.getFileName());

    // dataFile is unique if no dataFile with same relativeLocation and name already exists and the checked datafile isn't the existing one (=
    // same resId)
    return existingDataFile.isEmpty() || existingDataFile.get().getResId().equals(dataFile.getResId());
  }

  @Override
  protected void validateItemSpecificRules(T dataFile, BindingResult errors) {
    // Check data category
    this.checkDataCategories(dataFile, errors);
    // Check that the filename is identical to the sourceData path (or the finalData path if it isalready set)
    this.checkFileName(dataFile, errors);
  }

  @Override
  public void patchResource(T dataFile, Map<String, Object> updateMap) {
    if (updateMap.containsKey("relativeLocation") && !dataFile.getRelativeLocation().equals(updateMap.get("relativeLocation"))) {
      updateMap.put("status", DataFileStatus.CHANGE_RELATIVE_LOCATION);
    } else if ((updateMap.containsKey(DATA_CATEGORY_FIELD)
            && dataFile.getDataCategory() != DataCategory.valueOf((String) updateMap.get(DATA_CATEGORY_FIELD)))
            || (updateMap.containsKey(DATA_TYPE_FIELD)
                    && dataFile.getDataType() != DataCategory.valueOf((String) updateMap.get(DATA_TYPE_FIELD)))) {
      updateMap.put("status", DataFileStatus.CHANGE_DATA_CATEGORY);
    }
    super.patchResource(dataFile, updateMap);
  }

  private void checkFileName(T dataFile, BindingResult errors) {
    final URI finalData = dataFile.getFinalData();
    final URI sourceData = dataFile.getSourceData();

    URI fileUri = sourceData;
    if (finalData != null) {
      fileUri = finalData;
    }

    if (fileUri != null) {
      final int lastIndexOfSlash = fileUri.toString().lastIndexOf('/');
      String fileNameFromUri = StringTool.decodeUrl(fileUri.toString().substring(lastIndexOfSlash + 1));

      if (fileNameFromUri != null && !fileNameFromUri.equals(dataFile.getFileName())) {
        errors.addError(new FieldError(dataFile.getClass().getSimpleName(), "fileName", this.messageService.get(FILENAME_DIFFERENT_FROM_URI)));
      }
    }
  }

  private void checkDataCategories(T dataFile, BindingResult errors) {
    final DataCategory dataCategory = dataFile.getDataCategory();
    final DataCategory dataType = dataFile.getDataType();
    if ((dataFile instanceof DepositDataFile && !this.dataCategoryService.checkDataFileCategory(dataCategory))
            || (!(dataFile instanceof DepositDataFile) && !this.dataCategoryService.checkPackageCategory(dataCategory))) {
      errors.addError(new FieldError(dataFile.getClass().getSimpleName(), DATA_CATEGORY_FIELD,
              this.messageService.get("validation.dataFile.dataCategory.unsupported",
                      new Object[] { dataCategory })));
    }
    if ((dataFile instanceof DepositDataFile && !this.dataCategoryService.checkDataFileCategoryAndType(dataCategory, dataType))
            || (!(dataFile instanceof DepositDataFile) && !this.dataCategoryService.checkPackageCategoryAndType(dataCategory, dataType))) {
      errors.addError(
              new FieldError(dataFile.getClass().getSimpleName(), DATA_TYPE_FIELD,
                      this.messageService.get("validation.dataFile.dataType.invalid",
                              new Object[] { dataType, dataCategory })));
    }
  }
}

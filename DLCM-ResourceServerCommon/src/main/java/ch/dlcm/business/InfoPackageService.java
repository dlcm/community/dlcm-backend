/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - InfoPackageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import org.springframework.util.unit.DataSize;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.CompositeResourceService;
import ch.unige.solidify.service.HttpRequestInfoProvider;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.InfoPackageInterface;
import ch.dlcm.service.HistoryService;

public abstract class InfoPackageService<T extends Resource> extends CompositeResourceService<T> {

  protected HttpRequestInfoProvider httpRequestInfoProvider;

  protected SpecificationPermissionsFilter<T> specificationPermissionFilter;

  protected HistoryService historyService;

  protected final DataSize fileSizeLimit;

  protected InfoPackageService(DLCMProperties dlcmProperties, HttpRequestInfoProvider httpRequestInfoProvider,
          SpecificationPermissionsFilter<T> specificationPermissionFilter, HistoryService historyService) {
    this.httpRequestInfoProvider = httpRequestInfoProvider;
    this.specificationPermissionFilter = specificationPermissionFilter;
    this.historyService = historyService;
    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit();
  }

  protected Result savePackageResumeStatus(InfoPackageInterface item) {
    final Result result = new Result(item.getResId());
    if (item.getInfo().getStatus() == PackageStatus.COMPLETED) {
      result.setMesssage(this.messageService.get("message.package.completed", new Object[] { item.getResId() }));
    } else if (item.getInfo().getStatus() != PackageStatus.IN_PREPARATION
            && item.getInfo().getStatus() != PackageStatus.IN_ERROR
            && item.getInfo().getStatus() != PackageStatus.PRESERVATION_ERROR) {
      result.setMesssage(this.messageService.get("message.package.inprogress", new Object[] { item.getResId() }));
    } else {
      result.setStatus(Result.ActionStatus.NOT_EXECUTED);
      if (!item.isReady()) {
        result.setMesssage(this.messageService.get("message.package.datafile.notready", new Object[] { item.getResId() }));
      } else {
        item.setPackageStatus(this.getResumeStatusFromHistory(item.getResId(), item.getInfo().getStatus()));
        this.save((T) item);
        result.setStatus(Result.ActionStatus.EXECUTED);
        result.setMesssage(this.messageService.get("message.package.resume", new Object[] { item.getResId() }));
      }
    }
    return result;
  }

  private PackageStatus getResumeStatusFromHistory(String id, PackageStatus errorStatus) {
    PackageStatus statusBeforeError;
    try {
      // Look into package history to get last status before an error occurred to use it again
      statusBeforeError = PackageStatus.valueOf(this.historyService.getStatusBeforeError(id, errorStatus.toString()));
    } catch (SolidifyCheckingException e) {
      // When crashing, there is no IN_ERROR status => get last status
      statusBeforeError = PackageStatus.valueOf(this.historyService.getLastStatus(id));
    }

    if (PackageStatus.isDisposalProcess(statusBeforeError)) {
      return PackageStatus.DISPOSABLE;
    } else if (PackageStatus.isCleaningProcess(statusBeforeError)) {
      return PackageStatus.CLEANING;
    } else if (PackageStatus.isReindexingProcess(statusBeforeError)) {
      return PackageStatus.REINDEXING;
    } else if (PackageStatus.isMetadataEditionProcess(statusBeforeError)) {
      return PackageStatus.METADATA_EDITION_PENDING;
    } else if (PackageStatus.isCheckProcess(statusBeforeError)) {
      return PackageStatus.CHECK_PENDING;
    } else if (PackageStatus.isPackageReplicationProcess(statusBeforeError)) {
      return PackageStatus.PACKAGE_REPLICATION_PENDING;
    } else if (PackageStatus.isTombstoneReplicationProcess(statusBeforeError)) {
      return PackageStatus.TOMBSTONE_REPLICATION_PENDING;
    } else if (PackageStatus.isMetadataUpgradeProcess(statusBeforeError)) {
      return PackageStatus.METADATA_UPGRADE_PENDING;
    } else if (PackageStatus.isComplianceUpdateProcess(statusBeforeError)) {
      return PackageStatus.COMPLIANCE_LEVEL_UPDATE_PENDING;
    } else {
      return PackageStatus.IN_PREPARATION;
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - AbstractPermissionEvaluatorTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import ch.unige.solidify.service.HttpRequestInfoProvider;

import ch.dlcm.DLCMConstants;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.model.Access;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.security.Role;
import ch.dlcm.service.SearchService;
import ch.dlcm.service.rest.propagate.PropagateOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;
import ch.dlcm.service.security.AbstractPermissionWithOrgUnitService;
import ch.dlcm.service.security.MfaPermissionService;

abstract class AbstractPermissionEvaluatorTest {

  protected static final String APPROVER_ID = "approverId";
  protected static final String CREATOR_ID = "creatorId";
  protected static final String EXISTENT_ID = "existentId";
  protected static final String INEXISTENT_ACTION = "W2dfwdg";
  protected static final String INEXISTENT_ID = "inexistentId";
  protected static final String MANAGER_ID = "managerId";
  protected static final String ORGANIZATIONAL_UNIT_ID = DLCMConstants.DB_ORG_UNIT_ID;
  protected static final String STEWARD_ID = "stewardId";
  protected static final String VISITOR_ID = "visitorId";
  protected static final String CLOSED_ARCHIVE_METADATA_ID = "closedArchiveMetadataId";
  protected static final String RESTRICTED_ARCHIVE_METADATA_ID = "restrictedArchiveMetadataId";
  protected static final String INDEX_NAME = "indexName";

  // Create Authentication objects
  protected final Authentication authApprover = new TestingAuthenticationToken(Role.APPROVER_ID, null);
  protected final Authentication authCreator = new TestingAuthenticationToken(Role.CREATOR_ID, null);
  protected final Authentication authManager = new TestingAuthenticationToken(Role.MANAGER_ID, null);
  protected final Authentication authSteward = new TestingAuthenticationToken(Role.STEWARD_ID, null);
  protected final Authentication authVisitor = new TestingAuthenticationToken(Role.VISITOR_ID, null);

  @Mock
  protected HttpRequestInfoProvider httpRequestInfoProvider;

  @Mock
  protected TrustedPersonRemoteResourceService trustedPersonRemoteResourceService;

  @Mock
  protected PropagateOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService;
  @Mock
  protected MfaPermissionService mfaPermissionService;
  @Mock
  protected SearchService searchService;
  @Mock
  protected SecurityContext securityContext;

  void setUp() {

    // By default, populate the SecurityContextHolder with a manager user
    this.setSecurityContextAuthentication(this.authManager);

    // Mock the trustedPersonRemoteResourceService
    when(this.trustedPersonRemoteResourceService.getLinkedPersonId(this.authManager)).thenReturn(MANAGER_ID);
    when(this.trustedPersonRemoteResourceService.getLinkedPersonId(this.authApprover)).thenReturn(APPROVER_ID);
    when(this.trustedPersonRemoteResourceService.getLinkedPersonId(this.authSteward)).thenReturn(STEWARD_ID);
    when(this.trustedPersonRemoteResourceService.getLinkedPersonId(this.authCreator)).thenReturn(CREATOR_ID);
    when(this.trustedPersonRemoteResourceService.getLinkedPersonId(this.authVisitor)).thenReturn(VISITOR_ID);
    when(this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(MANAGER_ID, ORGANIZATIONAL_UNIT_ID))
            .thenReturn(Optional.of(Role.MANAGER));
    when(this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(APPROVER_ID, ORGANIZATIONAL_UNIT_ID))
            .thenReturn(Optional.of(Role.APPROVER));
    when(this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(STEWARD_ID, ORGANIZATIONAL_UNIT_ID))
            .thenReturn(Optional.of(Role.STEWARD));
    when(this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(CREATOR_ID, ORGANIZATIONAL_UNIT_ID))
            .thenReturn(Optional.of(Role.CREATOR));
    when(this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(VISITOR_ID, ORGANIZATIONAL_UNIT_ID))
            .thenReturn(Optional.of(Role.VISITOR));

    // Mock the findOne on searchService
    final ArchiveMetadata publicArchiveMetadata = new ArchiveMetadata();
    publicArchiveMetadata.setResId(EXISTENT_ID);
    publicArchiveMetadata.setIndex(INDEX_NAME);
    publicArchiveMetadata.setMetadata(Map.of(DLCMConstants.AIP_ACCESS_LEVEL, Access.PUBLIC.toString(),
            DLCMConstants.AIP_ORGA_UNIT, ORGANIZATIONAL_UNIT_ID));

    final ArchiveMetadata closedArchiveMetadata = new ArchiveMetadata();
    closedArchiveMetadata.setIndex(INDEX_NAME);
    closedArchiveMetadata.setMetadata(Map.of(DLCMConstants.AIP_ACCESS_LEVEL, Access.CLOSED.toString(),
            DLCMConstants.AIP_ORGA_UNIT, ORGANIZATIONAL_UNIT_ID));

    final ArchiveMetadata restrictedArchiveMetdata = new ArchiveMetadata();
    restrictedArchiveMetdata.setIndex(INDEX_NAME);
    restrictedArchiveMetdata.setMetadata(Map.of(DLCMConstants.AIP_ACCESS_LEVEL, Access.RESTRICTED.toString(),
            DLCMConstants.AIP_ORGA_UNIT, ORGANIZATIONAL_UNIT_ID));

    when(this.searchService.findOne(EXISTENT_ID)).thenReturn(publicArchiveMetadata);
    when(this.searchService.findOne(CLOSED_ARCHIVE_METADATA_ID)).thenReturn(closedArchiveMetadata);
    when(this.searchService.findOne(RESTRICTED_ARCHIVE_METADATA_ID)).thenReturn(restrictedArchiveMetdata);
  }

  protected void allowedActionsTest(Map<Authentication, List<DLCMControllerAction>> mapAllowedActions,
          AbstractPermissionWithOrgUnitService permissionService) {
    for (final Map.Entry<Authentication, List<DLCMControllerAction>> allowedActions : mapAllowedActions.entrySet()) {
      this.setSecurityContextAuthentication(allowedActions.getKey());
      for (final DLCMControllerAction action : allowedActions.getValue()) {
        assertTrue(permissionService.isAllowed(EXISTENT_ID, action.toString()));
      }
    }
  }

  protected void forbiddenActionsTest(Map<Authentication, List<DLCMControllerAction>> mapAllowedActions,
          AbstractPermissionWithOrgUnitService permissionService) {
    for (final Map.Entry<Authentication, List<DLCMControllerAction>> allowedActions : mapAllowedActions.entrySet()) {
      this.setSecurityContextAuthentication(allowedActions.getKey());
      for (final DLCMControllerAction action : this.getActionsNotInList(allowedActions.getValue())) {
        assertFalse(permissionService.isAllowed(EXISTENT_ID, action.toString()));
      }
    }
  }

  /**
   * Return all actions not present in the given list of actions.
   *
   * @param actions the list of actions
   * @return the list of all actions not present
   */
  protected List<DLCMControllerAction> getActionsNotInList(List<DLCMControllerAction> actions) {
    final List<DLCMControllerAction> notInList = new ArrayList<>();
    for (final DLCMControllerAction action : DLCMControllerAction.values()) {
      if (!actions.contains(action)) {
        notInList.add(action);
      }
    }
    return notInList;
  }

  protected void setSecurityContextAuthentication(Authentication authentication) {
    Mockito.when(this.securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(this.securityContext);
  }
}

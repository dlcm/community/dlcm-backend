/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - FitsTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tools;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.util.StandaloneRestClientTool;

@ExtendWith(SpringExtension.class)
@Disabled("Run it manually if Fits is installed on port 8080")
class FitsTest {
  private static String FITS_URL = "http://localhost:8080/fits-1.1.3/";

  private static final Logger log = LoggerFactory.getLogger(FitsTest.class);

  @Test
  void versionTest() {
    final RestTemplate restClt = new StandaloneRestClientTool().getClient();
    final String fitsVersion = restClt.getForObject(FITS_URL + "/version", String.class).trim();
    log.info("FITS " + fitsVersion);
  }

}

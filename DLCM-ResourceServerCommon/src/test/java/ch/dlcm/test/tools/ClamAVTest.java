/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - ClamAVTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tools;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.util.FileTool;

import ch.dlcm.viruscheck.clamav.ClamAVClient;
import ch.dlcm.viruscheck.clamav.ClamAVResult;

@ExtendWith(SpringExtension.class)
@Disabled("Run it manually if ClamAV is running on port 3310")
class ClamAVTest {
  private static String FILE_TO_SCAN = "/home/cazix/Downloads/NetBSD-8.1-amd64.iso";

  private static final Logger log = LoggerFactory.getLogger(ClamAVTest.class);

  private final ClamAVClient clamAVClt = new ClamAVClient("localhost", 3310);

  @Test
  void pingTest() {
    log.info("Ping result: " + this.clamAVClt.ping());
  }

  @Test
  void statsTest() {
    log.info("Status result: " + this.clamAVClt.stats());
  }

  @Test
  void testFile() throws IOException {
    final InputStream is = FileTool.getInputStream(Paths.get(FILE_TO_SCAN));
    assertNotNull(is);
    final ClamAVResult res = this.clamAVClt.scan(is);
    switch (res.getStatus()) {
      case FAILED -> assertEquals(ClamAVResult.Status.PASSED, res.getStatus(), "Virus found: " + res.getSignature());
      case PASSED -> assertEquals(ClamAVResult.Status.PASSED, res.getStatus(), "No virus");
      default -> {
      }
    }
  }

  @Test
  void testSuccess() {

    final String testString = "abcde12345".repeat(10000);
    final InputStream is = new ByteArrayInputStream(testString.getBytes());
    assertNotNull(is);
    final ClamAVResult res = this.clamAVClt.scan(is);
    assertEquals(ClamAVResult.Status.PASSED, res.getStatus());
    assertEquals(ClamAVResult.RESPONSE_OK, res.getResult());
  }

  @Test
  void testVirus() {
    final InputStream is = new ByteArrayInputStream("X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*".getBytes());
    assertNotNull(is);
    final ClamAVResult res = this.clamAVClt.scan(is);
    assertEquals(ClamAVResult.Status.FAILED, res.getStatus());
    assertEquals("stream: Win.Test.EICAR_HDB-1 FOUND", res.getResult());
    assertEquals("Win.Test.EICAR_HDB-1", res.getSignature());
  }

  @Test
  void versionNumberTest() {
    log.info("Version result: " + this.clamAVClt.versionNumber());
  }

  @Test
  void versionTest() {
    log.info("Version result: " + this.clamAVClt.version());
  }

}

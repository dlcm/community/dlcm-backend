/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - IdentifierServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.tools;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.util.ValidationTool;

import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.service.IdentifierService;

@ExtendWith(SpringExtension.class)
class IdentifierServiceTest {

  private static final Logger log = LoggerFactory.getLogger(IdentifierServiceTest.class);

  private final static int NB_TEST_ENCODE_DECODE = 1000;

  private IdentifierService identifierService;

  @Test
  void multipleDOIEncodingDecodingTest() {
    for (int i = 0; i < NB_TEST_ENCODE_DECODE; i++) {
      final String uuid = UUID.randomUUID().toString();
      final String doi = this.identifierService.createDOI(uuid);
      assertTrue(ValidationTool.isValidDOI(doi));
      log.info("DOI={}", doi);
      final String newUuid = this.identifierService.getResIdFromDOI(doi);
      assertEquals(uuid, newUuid);
      assertTrue(ValidationTool.isValidUUID(newUuid));
    }
  }

  @Test
  void multipleARKEncodingDecodingTest() {
    for (int i = 0; i < NB_TEST_ENCODE_DECODE; i++) {
      final String uuid = UUID.randomUUID().toString();
      final String ark = this.identifierService.createARK(uuid);
      assertTrue(ValidationTool.isValidARKv18(ark));
      log.info("ARK={}", ark);
      final String newUuid = this.identifierService.getResIdFromARK(ark);
      assertEquals(uuid, newUuid);
      assertTrue(ValidationTool.isValidUUID(newUuid));
    }
  }

  @BeforeEach
  public void setUp() {
    this.identifierService = new IdentifierService(new DLCMRepositoryDescription(new GitInfoProperties()));
  }

}

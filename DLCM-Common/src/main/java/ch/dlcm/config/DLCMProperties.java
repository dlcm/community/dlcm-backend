/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Common - DLCMProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import jakarta.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.unit.DataSize;

import ch.unige.solidify.IndexApplicationProperties;
import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.model.index.IndexDefinition;
import ch.unige.solidify.model.index.IndexDefinitionList;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.IdentifierType;
import ch.dlcm.model.MetadataVisibility;
import ch.dlcm.rest.ModuleName;

@Component
@ConfigurationProperties(prefix = "dlcm")
public class DLCMProperties implements IndexApplicationProperties {
  private static final Logger log = LoggerFactory.getLogger(DLCMProperties.class);

  private static final String TMP = "tmp";

  private SolidifyProperties appConfig;

  private MultipartProperties multipartProperties;

  public DLCMProperties(SolidifyProperties appConfig, MultipartProperties multipartProperties) {
    this.appConfig = appConfig;
    this.multipartProperties = multipartProperties;
  }

  // Working folders for the applications
  private enum WorkingDirectory {
    DEPOSIT("deposits"), SIP("sip"), AIP("aip"), DIP("dip"), ORDER("order"), LOG("log");

    private final String name;

    WorkingDirectory(String name) {
      this.name = name;
    }

    private String getName() {
      return this.name;
    }
  }

  public SolidifyProperties getAppConfig() {
    return this.appConfig;
  }

  public MultipartProperties getMultipartProperties() {
    return this.multipartProperties;
  }

  @PostConstruct
  private void checkConfiguration() {
    // uploadFileSizeLimit should be defined from spring.servlet.multipart.max-file-size minus 1%
    // (to manage difference between logic and physical size).
    this.parameters.uploadFileSizeLimit = DataSize.ofBytes(this.getMultipartProperties().getMaxFileSize().toBytes() * 99 / 100);
    // Use a temporary directory if dlcm.home is not defined
    if (this.home.isEmpty()) {
      try {
        this.home = Files.createTempDirectory("dlcm").toString();

        log.warn("dlcm.home not defined, using the temporary directory {}", this.home);
      } catch (IOException e) {
        log.error("Impossible to create temporary directory");
      }
    }

    // Create all needed directories
    try {
      for (WorkingDirectory d : WorkingDirectory.values()) {
        Path p = Paths.get(this.home, d.getName());
        // Create directory
        Files.createDirectories(p);
        // Create temporary directory
        Files.createDirectories(p.resolve(TMP));
      }
      Files.createDirectories(Paths.get(this.home, WorkingDirectory.DEPOSIT.getName(), DLCMConstants.ANONYMIZED_DEPOSITS_FOLDER));
    } catch (IOException e) {
      throw new IllegalStateException("DLCM needed directory cannot be created", e);
    }

  }

  // ****************
  // ** Properties **
  // ****************
  private String home = "";
  private Data data = new Data();
  private Wait wait = new Wait();
  private Queue queue = new Queue();
  private Topic topic = new Topic();
  private Http http = new Http();
  private Module module = new Module();
  private Parameters parameters = new Parameters();
  private Preparation preparation = new Preparation();
  private PreservationPlanning preservationPlanning = new PreservationPlanning();
  private Storage storage = new Storage();
  private Storage securedStorage = new Storage();
  private Indexing indexing = new Indexing();
  private FileFormat fileFormat = new FileFormat();
  private VirusCheck virusCheck = new VirusCheck();
  private Documentation documentation = new Documentation();
  private List<ScheduledTaskConfig> scheduledTaskConfigs = new ArrayList<>();
  private WebUrls webUrls = new WebUrls();

  // *************************
  // ** Embedded Properties **
  // *************************

  // Global Parameters
  public static class Parameters {
    private String[] goldenFormats = {
            "fmt/2", "fmt/4", "fmt/11", "fmt/12", "fmt/13", "fmt/44", "fmt/92", "fmt/95",
            "fmt/101", "fmt/141", "fmt/152", "fmt/155",
            "fmt/289",
            "fmt/353", "fmt/354",
            "fmt/476", "fmt/477", "fmt/478", "fmt/479", "fmt/480", "fmt/481", "fmt/483",
            "fmt/531",
            "fmt/729",
            "fmt/817", "fmt/896",
            "fmt/965",
            "x-fmt/18",
            "x-fmt/111",
            "x-fmt/235", "x-fmt/280",
            "x-fmt/392"
    };
    private String[] forbiddenCharacters = { "~", "!", "@", "#", "$", "%", "^", "&", "*", "`", ";", "?", "\\" };
    private String[] checksumList = { "MD5", "SHA1", "SHA256" };
    private String defaultChecksum = "SHA256";
    private String defaultLicense = "CC-BY-4.0";
    private String defaultMasterArchiveType = "DATASET";
    private String metsIdPrefix = "_";
    private int authorizedUnitsCacheTime = 60;
    private int archiveRatingCacheTime = 60;
    private int archiveUserFingerprintDurationMinutes = 5;

    private DataSize fileSizeLimit = DataSize.parse("4GB");

    private DataSize uploadFileSizeLimit = DataSize.parse("6GB");

    private FileList excludeList = new FileList(
            new String[] { "fmt/394", "fmt/503", "fmt/682" },
            new String[] { "/**/*%0A*.*", "/**/*%0D*.*", "/**/__MACOSX/**", "/**/Thumbs.db*", "/**/.DS_Store" });
    // For more information see: https://www.loc.gov/preservation/resources/rfs/
    private FileList ignoreList = new FileList(
            new String[] { "x-fmt/428", "x-fmt/263", "fmt/613", "fmt/484", "fmt/610", "x-fmt/265", "x-fmt/267", "x-fmt/268", "x-fmt/266" },
            new String[] { "/**/old/**", "/**/*.lnk", "/**/*.zzz", "/**/*.zip", "/**/*.rar", "/**/*.7z", "/**/*.arj", "/**/*.tar", "/**/*.bz",
                    "/**/*.bz2", "/**/*.gz", "/**/*.z" });
    private long defaultAsyncExecutionTimeout = 0;
    private int maxPreservationPolicyRetentionPeriodInYears = 100;

    private String submissionPortalHomepage = "http://localhost:4200"; // used in emails templates to build links to the portal
    private String projectNameForEmailTemplates = "DLCM";

    private boolean emailOnNotificationTypesSubscriptionChangeEnabled = true;

    private int maxFileNumber = 50000;

    private int dataFileThreadPoolSize = Runtime.getRuntime().availableProcessors();

    private boolean researchDataEnabled = true;
    private boolean administrativeDataEnabled = false;

    private IdentifierType defaultIdentifierType = IdentifierType.DOI;

    public String[] getGoldenFormats() {
      return this.goldenFormats;
    }

    public String[] getForbiddenCharacters() {
      return this.forbiddenCharacters;
    }

    public String[] getChecksumList() {
      return this.checksumList;
    }

    public String getDefaultChecksum() {
      return this.defaultChecksum;
    }

    public String getDefaultLicense() {
      return this.defaultLicense;
    }

    public String getDefaultMasterArchiveType() {
      return this.defaultMasterArchiveType;
    }

    public void setDefaultMasterArchiveType(String defaultMasterArchiveType) {
      this.defaultMasterArchiveType = defaultMasterArchiveType;
    }

    public String getMetsIdPrefix() {
      return this.metsIdPrefix;
    }

    public int getArchiveRatingCacheTime() {
      return this.archiveRatingCacheTime;
    }

    public int getAuthorizedUnitsCacheTime() {
      return this.authorizedUnitsCacheTime;
    }

    public void setGoldenFormats(String[] goldenFormats) {
      this.goldenFormats = goldenFormats;
    }

    public void setForbiddenCharacters(String[] forbiddenCharacters) {
      this.forbiddenCharacters = forbiddenCharacters;
    }

    public void setChecksumList(String[] checksumList) {
      this.checksumList = checksumList;
    }

    public void setDefaultChecksum(String defaultChecksum) {
      this.defaultChecksum = defaultChecksum;
    }

    public void setDefaultLicense(String defaultLicense) {
      this.defaultLicense = defaultLicense;
    }

    public void setMetsIdPrefix(String metsIdPrefix) {
      this.metsIdPrefix = metsIdPrefix;
    }

    public void setAuthorizedUnitsCacheTime(int authorizedUnitsCacheTime) {
      this.authorizedUnitsCacheTime = authorizedUnitsCacheTime;
    }

    public DataSize getFileSizeLimit() {
      return this.fileSizeLimit;
    }

    public DataSize getUploadFileSizeLimit() {
      return this.uploadFileSizeLimit;
    }

    public void setFileSizeLimit(DataSize fileSizeLimit) {
      this.fileSizeLimit = fileSizeLimit;
    }

    public void setUploadFileSizeLimit(DataSize uploadFileSizeLimit) {
      this.uploadFileSizeLimit = uploadFileSizeLimit;
    }

    public FileList getExcludeList() {
      return this.excludeList;
    }

    public void setExcludeList(FileList excludeList) {
      this.excludeList = excludeList;
    }

    public FileList getIgnoreList() {
      return this.ignoreList;
    }

    public void setIgnoreList(FileList ignoreList) {
      this.ignoreList = ignoreList;
    }

    public long getDefaultAsyncExecutionTimeout() {
      return this.defaultAsyncExecutionTimeout;
    }

    public void setDefaultAsyncExecutionTimeout(long defaultAsyncExecutionTimeout) {
      this.defaultAsyncExecutionTimeout = defaultAsyncExecutionTimeout;
    }

    public int getMaxPreservationPolicyRetentionPeriodInYears() {
      return this.maxPreservationPolicyRetentionPeriodInYears;
    }

    public void setMaxPreservationPolicyRetentionPeriodInYears(int maxPreservationPolicyRetentionPeriodInYears) {
      this.maxPreservationPolicyRetentionPeriodInYears = maxPreservationPolicyRetentionPeriodInYears;
    }

    public int getArchiveUserFingerprintDurationMinutes() {
      return this.archiveUserFingerprintDurationMinutes;
    }

    public void setArchiveUserFingerprintDurationMinutes(int archiveUserFingerprintDurationMinutes) {
      this.archiveUserFingerprintDurationMinutes = archiveUserFingerprintDurationMinutes;
    }

    public boolean isEmailOnNotificationTypesSubscriptionChangeEnabled() {
      return this.emailOnNotificationTypesSubscriptionChangeEnabled;
    }

    public void setEmailOnNotificationTypesSubscriptionChangeEnabled(boolean emailOnNotificationTypesSubscriptionChangeEnabled) {
      this.emailOnNotificationTypesSubscriptionChangeEnabled = emailOnNotificationTypesSubscriptionChangeEnabled;
    }

    public String getProjectNameForEmailTemplates() {
      return this.projectNameForEmailTemplates;
    }

    public void setProjectNameForEmailTemplates(String projectNameForEmailTemplates) {
      this.projectNameForEmailTemplates = projectNameForEmailTemplates;
    }

    public String getSubmissionPortalHomepage() {
      return this.submissionPortalHomepage;
    }

    public void setSubmissionPortalHomepage(String submissionPortalHomepage) {
      this.submissionPortalHomepage = submissionPortalHomepage;
    }

    public int getMaxFileNumber() {
      return this.maxFileNumber;
    }

    public void setMaxFileNumber(int maxFileNumber) {
      this.maxFileNumber = maxFileNumber;
    }

    public int getDataFileThreadPoolSize() {
      return this.dataFileThreadPoolSize;
    }

    public void setDataFileThreadPoolSize(int dataFileThreadPoolSize) {
      this.dataFileThreadPoolSize = dataFileThreadPoolSize;
    }

    public boolean isResearchDataEnabled() {
      return this.researchDataEnabled;
    }

    public void setResearchDataEnabled(boolean researchDataEnabled) {
      this.researchDataEnabled = researchDataEnabled;
    }

    public boolean isAdministrativeDataEnabled() {
      return this.administrativeDataEnabled;
    }

    public void setAdministrativeDataEnabled(boolean administrativeDataEnabled) {
      this.administrativeDataEnabled = administrativeDataEnabled;
    }

    public IdentifierType getDefaultIdentifierType() {
      return this.defaultIdentifierType;
    }

    public void setDefaultIdentifierType(IdentifierType defaultIdentifierType) {
      this.defaultIdentifierType = defaultIdentifierType;
    }

  }

  public static class FileList {
    private String[] files = {};
    private String[] puids = {};

    public FileList(String[] puids, String[] files) {
      if (puids != null && puids.length > 0) {
        this.puids = puids;
      }
      if (files != null && files.length > 0) {
        this.files = files;
      }
    }

    public String[] getFiles() {
      return this.files;
    }

    public void setFiles(String[] files) {
      this.files = files;
    }

    public String[] getPuids() {
      return this.puids;
    }

    public void setPuids(String[] puids) {
      this.puids = puids;
    }
  }

  public static class Data {
    private boolean init = false;
    private boolean initIndexes = true;

    public boolean isInit() {
      return this.init;
    }

    public boolean isInitIndexes() {
      return this.initIndexes;
    }

    public void setInit(boolean init) {
      this.init = init;
    }

    public void setInitIndexes(boolean init) {
      this.initIndexes = init;
    }

  }

  // Wait Parameter
  public static class Wait {
    private int maxTries = 10;
    private int milliseconds = 500;

    public int getMaxTries() {
      return this.maxTries;
    }

    public int getMilliseconds() {
      return this.milliseconds;
    }

    public void setMaxTries(int maxTries) {
      this.maxTries = maxTries;
    }

    public void setMilliseconds(int milliseconds) {
      this.milliseconds = milliseconds;
    }
  }

  // Queues
  public static class Queue {
    private String job = "job";
    private String deposit = "deposit";
    private String sip = "sip";
    private String aip = "aip";
    private String dip = "dip";
    private String aipDownload = "aip-download";
    private String aipRefresh = "aip-refresh";
    private String aipFixity = "aip.fixity";
    private String order = "order";
    private String orgunit = "orgunit";
    private String archive = "archive";
    private String depositDataFile = "deposit.datafile";
    private String sipDataFile = "sip.datafile";
    private String aipDataFile = "aip.datafile";
    private String dipDataFile = "dip.datafile";
    private String notification = "notification";
    private int nbParallelThreads = 2;

    public String getJob() {
      return this.job;
    }

    public String getDeposit() {
      return this.deposit;
    }

    public String getSip() {
      return this.sip;
    }

    public String getAip() {
      return this.aip;
    }

    public String getDip() {
      return this.dip;
    }

    public String getAipDownload() {
      return this.aipDownload;
    }

    public String getAipRefresh() {
      return this.aipRefresh;
    }

    public String getAipFixity() {
      return this.aipFixity;
    }

    public String getOrder() {
      return this.order;
    }

    public void setJob(String job) {
      this.job = job;
    }

    public void setDeposit(String deposit) {
      this.deposit = deposit;
    }

    public void setSip(String sip) {
      this.sip = sip;
    }

    public void setAip(String aip) {
      this.aip = aip;
    }

    public void setDip(String dip) {
      this.dip = dip;
    }

    public void setAipDownload(String aipDownload) {
      this.aipDownload = aipDownload;
    }

    public void setAipRefresh(String aipRefresh) {
      this.aipRefresh = aipRefresh;
    }

    public void setAipFixity(String aipFixity) {
      this.aipFixity = aipFixity;
    }

    public void setOrder(String order) {
      this.order = order;
    }

    public String getOrgunit() {
      return this.orgunit;
    }

    public void setOrgunit(String orgunit) {
      this.orgunit = orgunit;
    }

    public String getArchive() {
      return this.archive;
    }

    public void setArchive(String archive) {
      this.archive = archive;
    }

    public String getDepositDataFile() {
      return this.depositDataFile;
    }

    public void setDepositDataFile(String depositDataFile) {
      this.depositDataFile = depositDataFile;
    }

    public String getSipDataFile() {
      return this.sipDataFile;
    }

    public void setSipDataFile(String sipDataFile) {
      this.sipDataFile = sipDataFile;
    }

    public String getAipDataFile() {
      return this.aipDataFile;
    }

    public void setAipDataFile(String aipDataFile) {
      this.aipDataFile = aipDataFile;
    }

    public String getDipDataFile() {
      return this.dipDataFile;
    }

    public void setDipDataFile(String dipDataFile) {
      this.dipDataFile = dipDataFile;
    }

    public String getNotification() {
      return this.notification;
    }

    public void setNotification(String notification) {
      this.notification = notification;
    }

    public int getNbParallelThreads() {
      return this.nbParallelThreads;
    }

    public void setNbParallelThreads(int nbParallelThreads) {
      this.nbParallelThreads = nbParallelThreads;
    }
  }

  // Topic queues
  public static class Topic {
    private String cache = "cache";
    private String emails = "emails";
    private String archiveReindexed = "archive-reindexed";

    public String getCache() {
      return this.cache;
    }

    public String getEmails() {
      return this.emails;
    }

    public String getArchiveReindexed() {
      return this.archiveReindexed;
    }

    public void setCache(String cache) {
      this.cache = cache;
    }

    public void setEmails(String emails) {
      this.emails = emails;
    }

    public void setArchiveReindexed(String archiveReindexed) {
      this.archiveReindexed = archiveReindexed;
    }
  }

  // Web URLs
  public static class WebUrls {
    private String orcid = "https://orcid.org/";
    private String ror = "https://ror.org/";
    private String rorApi = "https://api.ror.org/organizations/";
    private String grid = "https://grid.ac/institutes/";
    private String isni = "https://isni.org/isni/";
    private String crossrefFunder = "https://api.crossref.org/funders/";
    private String wikidata = "https://www.wikidata.org/wiki/";

    public String getOrcid() {
      return this.orcid;
    }

    public void setOrcid(String orcid) {
      this.orcid = orcid;
    }

    public String getRor() {
      return this.ror;
    }

    public void setRor(String ror) {
      this.ror = ror;
    }

    public String getRorApi() {
      return this.rorApi;
    }

    public void setRorApi(String rorApi) {
      this.rorApi = rorApi;
    }

    public String getGrid() {
      return this.grid;
    }

    public void setGrid(String grid) {
      this.grid = grid;
    }

    public String getIsni() {
      return this.isni;
    }

    public void setIsni(String isni) {
      this.isni = isni;
    }

    public String getCrossrefFunder() {
      return this.crossrefFunder;
    }

    public void setCrossrefFunder(String crossrefFunder) {
      this.crossrefFunder = crossrefFunder;
    }

    public String getWikidata() {
      return this.wikidata;
    }

    public void setWikidata(String wikidata) {
      this.wikidata = wikidata;
    }

  }

  // HTTP parameters
  public static class Http {
    private String expectedHeadersCharset = "ISO-8859-1";
    private String shibbolethHeadersCharset = "UTF-8";

    public String getExpectedHeadersCharset() {
      return this.expectedHeadersCharset;
    }

    public String getShibbolethHeadersCharset() {
      return this.shibbolethHeadersCharset;
    }

    public void setExpectedHeadersCharset(String expectedHeadersCharset) {
      this.expectedHeadersCharset = expectedHeadersCharset;
    }

    public void setShibbolethHeadersCharset(String shibbolethHeadersCharset) {
      this.shibbolethHeadersCharset = shibbolethHeadersCharset;
    }
  }

  // Modules
  public static class Module {
    private ModuleDetail admin = new ModuleDetail();
    private ModuleDetail preingest = new ModuleDetail();
    private ModuleDetail ingest = new ModuleDetail();
    private ModuleDetail dataMgmt = new ModuleDetail();
    private ModuleClusterDetail archivalStorage = new ModuleClusterDetail();
    private ModuleDetail preservationPlanning = new ModuleDetail();
    private ModuleDetail access = new ModuleDetail();

    public ModuleDetail getAdmin() {
      return this.admin;
    }

    public ModuleDetail getPreingest() {
      return this.preingest;
    }

    public ModuleDetail getIngest() {
      return this.ingest;
    }

    public ModuleDetail getDataMgmt() {
      return this.dataMgmt;
    }

    public ModuleClusterDetail getArchivalStorage() {
      return this.archivalStorage;
    }

    public ModuleDetail getPreservationPlanning() {
      return this.preservationPlanning;
    }

    public ModuleDetail getAccess() {
      return this.access;
    }

    public void setAdmin(ModuleDetail admin) {
      this.admin = admin;
    }

    public void setPreingest(ModuleDetail preingest) {
      this.preingest = preingest;
    }

    public void setIngest(ModuleDetail ingest) {
      this.ingest = ingest;
    }

    public void setDataMgmt(ModuleDetail dataMgmt) {
      this.dataMgmt = dataMgmt;
    }

    public void setArchivalStorage(ModuleClusterDetail archivalStorage) {
      this.archivalStorage = archivalStorage;
    }

    public void setPreservationPlanning(ModuleDetail preservationPlanning) {
      this.preservationPlanning = preservationPlanning;
    }

    public void setAccess(ModuleDetail access) {
      this.access = access;
    }

  }

  public static class ModuleDetail {
    private String url = "";
    private String publicUrl = "";
    private boolean enable = false;

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getPublicUrl() {
      if (this.publicUrl.isEmpty()) {
        return this.getUrl();
      }
      return this.publicUrl;
    }

    public void setPublicUrl(String publicUrl) {
      this.publicUrl = publicUrl;
    }

    public boolean isEnable() {
      return this.enable;
    }

    public void setEnable(boolean enable) {
      this.enable = enable;
    }
  }

  public static class ModuleClusterDetail {
    private String[] urls = {};
    private String[] publicUrls = {};
    private boolean enable = false;

    public String[] getUrls() {
      return this.urls;
    }

    public void setUrls(String[] urls) {
      this.urls = urls;
    }

    public String[] getPublicUrls() {
      if (this.publicUrls.length == 0) {
        return this.getUrls();
      }
      return this.publicUrls;
    }

    public void setPublicUrls(String[] publicUrls) {
      this.publicUrls = publicUrls;
    }

    public boolean isEnable() {
      return this.enable;
    }

    public void setEnable(boolean enable) {
      this.enable = enable;
    }
  }

  // Preparation service in pre-ingest
  public static class Preparation {
    private String id = "";
    private String url = "";
    private String user = "";
    private String apiKey = "";

    public String getId() {
      return this.id;
    }

    public String getUrl() {
      return this.url;
    }

    public String getUser() {
      return this.user;
    }

    public String getApiKey() {
      return this.apiKey;
    }

    public void setId(String id) {
      this.id = id;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public void setUser(String user) {
      this.user = user;
    }

    public void setApiKey(String apiKey) {
      this.apiKey = apiKey;
    }
  }

  // Preservation Planning parameters
  public static class PreservationPlanning {
    private int jobPageSize = 100;
    private int hour = 21;
    private int weekDay = 7; // 1 for Monday, 2 for Tuesday, etc.
    private int monthDay = 1;
    private int month = 4; // 1 for January, 2 for February, 3 March, etc.
    private int timeToKeepOrders = 100; // measured in days

    public int getTimeToKeepOrders() {
      return this.timeToKeepOrders;
    }

    public int getMonth() {
      return this.month;
    }

    public void setMonth(int month) {
      this.month = month;
    }

    public int getHour() {
      return this.hour;
    }

    public void setHour(int hour) {
      this.hour = hour;
    }

    public int getWeekDay() {
      return this.weekDay;
    }

    public void setWeekDay(int weekDay) {
      this.weekDay = weekDay;
    }

    public int getMonthDay() {
      return this.monthDay;
    }

    public void setMonthDay(int monthDay) {
      this.monthDay = monthDay;
    }

    public int getJobPageSize() {
      return this.jobPageSize;
    }

    public void setJobPageSize(int jobPageSize) {
      this.jobPageSize = jobPageSize;
    }

    public void setTimeToKeepOrders(int timeToKeepOrders) {
      this.timeToKeepOrders = timeToKeepOrders;
    }

  }

  // Storage service in archival storage
  public static class Storage {
    private String url = "s3-changeit.dlcm.ch";
    private S3Properties s3Properties = new S3Properties();
    private DNAProperties dnaProperties = new DNAProperties();

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public S3Properties getS3Properties() {
      return this.s3Properties;
    }

    public void setS3Properties(S3Properties s3Properties) {
      this.s3Properties = s3Properties;
    }

    public DNAProperties getDnaProperties() {
      return this.dnaProperties;
    }

    public void setDnaProperties(DNAProperties dnaProperties) {
      this.dnaProperties = dnaProperties;
    }

    public static class S3Properties {

      private String space = "dlcm-changeit";
      private String accessKey = "dlcm";
      private String secretKey = "dlcm-changeit";
      private String s3Signer = "S3SignerType";
      private boolean isPathStyleAccess = false;

      public String getSpace() {
        return this.space;
      }

      public String getAccessKey() {
        return this.accessKey;
      }

      public String getSecretKey() {
        return this.secretKey;
      }

      public String getS3Signer() {
        return this.s3Signer;
      }

      public boolean getIsPathStyleAccess() {
        return this.isPathStyleAccess;
      }

      public void setSpace(String space) {
        this.space = space;
      }

      public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
      }

      public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
      }

      public void setS3Signer(String s3Signer) {
        this.s3Signer = s3Signer;
      }

      public void setIsPathStyleAccess(boolean isPathStyleAccess) {
        this.isPathStyleAccess = isPathStyleAccess;
      }
    }

    public static class DNAProperties {

      private String microFactory = "dnamic";
      private String codecType = "no-dna-codec";
      private String mainPrimer = "dlcm";
      private String workingFolder = "";
      private String command = "";

      public String getMicroFactory() {
        return this.microFactory;
      }

      public void setMicroFactory(String microFactory) {
        this.microFactory = microFactory;
      }

      public String getCodecType() {
        return this.codecType;
      }

      public void setCodecType(String codecType) {
        this.codecType = codecType;
      }

      public String getWorkingFolder() {
        return this.workingFolder;
      }

      public void setWorkingFolder(String workingFolder) {
        this.workingFolder = workingFolder;
      }

      public String getCommand() {
        return this.command;
      }

      public void setCommand(String command) {
        this.command = command;
      }

      public String getMainPrimer() {
        return this.mainPrimer;
      }

      public void setMainPrimer(String mainPrimer) {
        this.mainPrimer = mainPrimer;
      }

    }
  }

  // Indexing service in data mgmt
  public static class Indexing {
    private static final String MAPPING_PRIVATE = "indexprivate.json";
    private static final String MAPPING_PUBLIC = "index.json";

    private static final String SETTINGS_PUBLIC = "settings.json";

    private String indexName = "datamgmt";
    private String[] exceptions = {};

    public String getIndexName() {
      return this.indexName;
    }

    public void setIndexName(String indexName) {
      this.indexName = indexName;
    }

    public String getPrivateIndexName() {
      return this.getIndexName() + DLCMConstants.PRIVATE;
    }

    public IndexDefinitionList getIndexDefinitionList() {
      IndexDefinitionList list = new IndexDefinitionList();
      list.getIndexList().add(new IndexDefinition(this.getIndexName(), MAPPING_PUBLIC, SETTINGS_PUBLIC));
      list.getIndexList().add(new IndexDefinition(this.getPrivateIndexName(), MAPPING_PRIVATE));
      return list;
    }

    public String[] getExceptions() {
      return this.exceptions;
    }

    public void setExceptions(String[] exceptions) {
      this.exceptions = exceptions;
    }

    public String getIndexName(MetadataVisibility metadataVisibility) {
      return switch (metadataVisibility) {
        case ALL -> this.getIndexName() + IndexConstants.INDEX_SEPARATOR + this.getPrivateIndexName();
        case RESTRICTED -> this.getPrivateIndexName();
        default -> this.getIndexName();
      };
    }
  }

  public static class Documentation {

    public static class User {
      private String name;
      private String role;

      public String getName() {
        return this.name;
      }

      public void setName(final String name) {
        this.name = name;
      }

      public String getRole() {
        return this.role;
      }

      public void setRole(final String role) {
        this.role = role;
      }
    }

    private boolean generateData = false;
    private List<Documentation.User> users = new ArrayList<>();

    public List<Documentation.User> getUsers() {
      return this.users;
    }

    public void setUsers(final List<Documentation.User> users) {
      this.users = users;
    }

    public boolean shouldGenerateData() {
      return this.generateData;
    }

    public void setGenerateData(boolean generateData) {
      this.generateData = generateData;
    }

  }

  // File Format service
  public static class FileFormat {
    private String tool = "";
    private String[] defaultTools = { "Droid", "Jhove" };

    public String getTool() {
      return this.tool;
    }

    public void setTool(String tool) {
      this.tool = tool;
    }

    public String[] getDefaultTools() {
      return this.defaultTools;
    }

    public void setDefaultTools(String[] defaultTools) {
      this.defaultTools = defaultTools;
    }

  }

  // Virus check service
  public static class VirusCheck {
    private String tool = "";
    private int timeout = 60000;

    public String getTool() {
      return this.tool;
    }

    public int getTimeout() {
      return this.timeout;
    }

    public void setTool(String tool) {
      this.tool = tool;
    }

    public void setTimeout(int timeout) {
      this.timeout = timeout;
    }

  }

  public Data getData() {
    return this.data;
  }

  public Wait getWait() {
    return this.wait;
  }

  public Queue getQueue() {
    return this.queue;
  }

  public Topic getTopic() {
    return this.topic;
  }

  public Http getHttp() {
    return this.http;
  }

  public Module getModule() {
    return this.module;
  }

  public static class ScheduledTaskConfig {
    private String id;
    private String type;
    private String name;
    private String cronExpression;
    private boolean enabled = true;

    public String getId() {
      return this.id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getType() {
      return this.type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getCronExpression() {
      return this.cronExpression;
    }

    public void setCronExpression(String cronExpression) {
      this.cronExpression = cronExpression;
    }

    public boolean isEnabled() {
      return this.enabled;
    }

    public void setEnabled(boolean enabled) {
      this.enabled = enabled;
    }
  }

  public Parameters getParameters() {
    return this.parameters;
  }

  public WebUrls getWebUrls() {
    return this.webUrls;
  }

  public Preparation getPreparation() {
    return this.preparation;
  }

  public PreservationPlanning getPreservationPlanning() {
    return this.preservationPlanning;
  }

  public Storage getStorage() {
    return this.storage;
  }

  public Storage getSecuredStorage() {
    return this.securedStorage;
  }

  public Indexing getIndexing() {
    return this.indexing;
  }

  public FileFormat getFileFormat() {
    return this.fileFormat;
  }

  public VirusCheck getVirusCheck() {
    return this.virusCheck;
  }

  public void setHome(String home) {
    this.home = home;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public void setWait(Wait wait) {
    this.wait = wait;
  }

  public void setQueue(Queue queue) {
    this.queue = queue;
  }

  public void setTopic(Topic topic) {
    this.topic = topic;
  }

  public void setHttp(Http http) {
    this.http = http;
  }

  public void setModule(Module module) {
    this.module = module;
  }

  public void setParameters(Parameters parameters) {
    this.parameters = parameters;
  }

  public void setWebUrls(WebUrls webUrls) {
    this.webUrls = webUrls;
  }

  public void setPreparation(Preparation preparation) {
    this.preparation = preparation;
  }

  public void setPreservationPlanning(PreservationPlanning preservationPlanning) {
    this.preservationPlanning = preservationPlanning;
  }

  public void setStorage(Storage storage) {
    this.storage = storage;
  }

  public void setSecuredStorage(Storage securedStorage) {
    this.securedStorage = securedStorage;
  }

  public void setIndexing(Indexing indexing) {
    this.indexing = indexing;
  }

  public void setFileFormat(FileFormat fileFormat) {
    this.fileFormat = fileFormat;
  }

  public void setVirusCheck(VirusCheck virusCheck) {
    this.virusCheck = virusCheck;
  }

  public String getHome() {
    return this.home;
  }

  public String getPreingestLocation() {
    return this.getHome() + "/" + WorkingDirectory.DEPOSIT.getName();
  }

  public String getIngestLocation() {
    return this.getHome() + "/" + WorkingDirectory.SIP.getName();
  }

  public String getArchivalLocation() {
    return this.getHome() + "/" + WorkingDirectory.AIP.getName();
  }

  public String getAccessLocation() {
    return this.getHome() + "/" + WorkingDirectory.DIP.getName();
  }

  public String getOrderLocation() {
    return this.getHome() + "/" + WorkingDirectory.ORDER.getName();
  }

  public String getTempLocation(String location) {
    return location + "/" + TMP;
  }

  // Get all archival storage modules (all nodes)
  public String[] getArchivalStorageUrls() {
    return this.getModule().getArchivalStorage().getUrls();
  }

  public String[] getArchivalStoragePublicUrls() {
    return this.getModule().getArchivalStorage().getPublicUrls();
  }

  // Get default/master archival storage module
  public String getDefaultArchivalStorageUrl() {
    return this.getModule().getArchivalStorage().getUrls()[0];
  }

  public String getDefaultArchivalStoragePublicUrl() {
    return this.getModule().getArchivalStorage().getPublicUrls()[0];
  }

  public Documentation getDocumentation() {
    return this.documentation;
  }

  public void setDocumentation(Documentation documentation) {
    this.documentation = documentation;
  }

  public List<ScheduledTaskConfig> getScheduledTasks() {
    return this.scheduledTaskConfigs;
  }

  @Override
  public String getIndexUrl() {
    final String dataMgmtUrl = this.getModule().getDataMgmt().getUrl();
    return dataMgmtUrl.substring(0, dataMgmtUrl.lastIndexOf(ModuleName.DATAMGMT));
  }
}

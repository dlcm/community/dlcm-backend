/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Common - DLCMRepositoryDescription.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.config;

import javax.xml.namespace.QName;

import jakarta.xml.bind.JAXBElement;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.oai.OAIRepositoryInfo;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.xml.dlcm.v2.dlcm.RepositoryInfo;

@Component
@ConfigurationProperties(prefix = "dlcm.repository")
public class DLCMRepositoryDescription implements OAIRepositoryInfo {

  private String name = "DLCM";
  private String longName = "DLCM Solution";
  private String description = "Repository for Research Datasets";
  private String institution = "Université de Genève";
  private String institutionRorId = "01swzsf04";
  private String location = "Genève, CH";
  private String email = "admin@dlcm.ch";
  private String productionDate = "2016-04-01T09:00:00Z";
  private String doiPrefix = "10.99999";
  private String arkNAAN = "99999";
  private String arkShoulder = "fk9";
  private String archiveHomePage = "";
  private String prefix = "dlcm";
  private String domain = "dlcm.ch";

  private final GitInfoProperties gitInfoProperties;

  public DLCMRepositoryDescription(GitInfoProperties gitInfoProperties) {
    this.gitInfoProperties = gitInfoProperties;
  }

  @Override
  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getLongName() {
    return this.longName;
  }

  public void setLongName(String longName) {
    this.longName = longName;
  }

  @Override
  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getInstitution() {
    return this.institution;
  }

  public void setInstitution(String institution) {
    this.institution = institution;
  }

  public String getInstitutionRorId() {
    return this.institutionRorId;
  }

  public void setInstitutionRorId(String institutionRorId) {
    this.institutionRorId = institutionRorId;
  }

  public String getLocation() {
    return this.location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  @Override
  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public String getProductionDate() {
    return this.productionDate;
  }

  public void setProductionDate(String productionDate) {
    this.productionDate = productionDate;
  }

  public String getDoiPrefix() {
    return this.doiPrefix;
  }

  public void setDoiPrefix(String doiPrefix) {
    this.doiPrefix = doiPrefix;
  }

  public String getArkNAAN() {
    return this.arkNAAN;
  }

  public void setArkNAAN(String arkNAAN) {
    this.arkNAAN = arkNAAN;
  }

  public String getArkShoulder() {
    return this.arkShoulder;
  }

  public void setArkShoulder(String arkShoulder) {
    this.arkShoulder = arkShoulder;
  }

  @Override
  public String getPrefix() {
    return this.prefix;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  @Override
  public String getDomain() {
    return this.domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  @Override
  public String getArchiveHomePage() {
    if (StringTool.isNullOrEmpty(this.archiveHomePage)) {
      throw new SolidifyRuntimeException("Missing 'archive-home-page' parameter");
    }
    return this.archiveHomePage;
  }

  public void setArchiveHomePage(String archiveHomePage) {
    if (!archiveHomePage.endsWith("/")) {
      archiveHomePage = archiveHomePage + "/";
    }
    this.archiveHomePage = archiveHomePage;
  }

  public String getVersion() {
    return this.gitInfoProperties.getBuild().getVersion();
  }

  @Override
  public JAXBElement<?> getRepositoryDefinition() {
    final RepositoryInfo repoInfo = new RepositoryInfo();
    repoInfo.setName(this.getName());
    repoInfo.setDescription(this.getDescription());
    repoInfo.setInstitution(this.getInstitution());
    repoInfo.setPoweredBy(DLCMConstants.DLCM_SOLUTION + " v" + this.gitInfoProperties.getBuild().getVersion());
    return new JAXBElement<>(new QName(DLCMConstants.DLCM_NAMESPACE_3, "repositoryInfo"), RepositoryInfo.class, repoInfo);
  }

  @Override
  public String getRepositoryNamespace() {
    return DLCMConstants.DLCM_NAMESPACE_3;
  }

  @Override
  public String getRepositorySchema() {
    return DLCMConstants.DLCM_SCHEMA_3;
  }

}

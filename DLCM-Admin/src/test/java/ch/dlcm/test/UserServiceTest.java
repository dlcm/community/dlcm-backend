/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - UserServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.service.AbstractUserSynchronizationService;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.service.TrustedRestClientService;
import ch.unige.solidify.util.PropagateRestClientTool;

import ch.dlcm.business.UserService;
import ch.dlcm.repository.InstitutionRepository;
import ch.dlcm.repository.PersonRepository;
import ch.dlcm.repository.UserRepository;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
class UserServiceTest {

  private UserService userService;

  @Mock
  private AbstractUserSynchronizationService userSynchronizationService;
  @Mock
  private UserRepository userRepository;
  @Mock
  private PersonRepository personRepository;
  @Mock
  private InstitutionRepository institutionRepository;
  @Mock
  private HttpRequestInfoProvider httpRequestInfoProvider;
  @Mock
  private AuthorizationClientProperties authProperties;
  @Mock
  private TrustedRestClientService trustedRestClientService;
  @Mock
  private PropagateRestClientTool propagateRestClientTool;

  private final static String USER_ID = "USER";
  private final Map<String, Object> correctMap = Map.of("applicationRole", Map.of("resId", USER_ID));

  @BeforeAll
  void setUp() {
    userService = new UserService(userSynchronizationService, userRepository, personRepository, institutionRepository, httpRequestInfoProvider,
            authProperties, trustedRestClientService, propagateRestClientTool);
  }

  @ParameterizedTest
  @MethodSource("wrongMaps")
  void containsWrongApplicationRoleIdTest(Map<String, Object> wrongMap) {
    assertFalse(this.userService.containsApplicationRoleId(wrongMap));
  }

  @Test
  void containsApplicationRoleIdTest() {
    assertTrue(this.userService.containsApplicationRoleId(correctMap));
  }

  @ParameterizedTest
  @MethodSource("wrongMaps")
  void getWrongApplicationRoleIdTest(Map<String, Object> wrongMap) {
    assertNull(this.userService.getApplicationRoleId(wrongMap));
  }

  @Test
  void getCorrectApplicationRoleIdTest() {
    assertEquals(USER_ID, this.userService.getApplicationRoleId(correctMap));
  }

  static Stream<Arguments> wrongMaps() {
    return Stream.of(
            Arguments.of(Map.of()),
            Arguments.of(Map.of("applicationRole", Map.of())),
            Arguments.of(Map.of("applicationRole", Map.of("a", Map.of()))),
            Arguments.of(Map.of("a", Map.of())),
            Arguments.of(Map.of("a", Map.of("b", USER_ID))));
  }
}

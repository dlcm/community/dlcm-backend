/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - AdminPermissionEvaluatorTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test;

import static ch.dlcm.controller.DLCMControllerAction.DELETE;
import static ch.dlcm.controller.DLCMControllerAction.DOWNLOAD_FILE;
import static ch.dlcm.controller.DLCMControllerAction.GET;
import static ch.dlcm.controller.DLCMControllerAction.GET_CURRENT_FORM;
import static ch.dlcm.controller.DLCMControllerAction.GET_FILE;
import static ch.dlcm.controller.DLCMControllerAction.GET_FORM;
import static ch.dlcm.controller.DLCMControllerAction.GET_MEMBER;
import static ch.dlcm.controller.DLCMControllerAction.HISTORY;
import static ch.dlcm.controller.DLCMControllerAction.LIST_FILES;
import static ch.dlcm.controller.DLCMControllerAction.LIST_FORMS;
import static ch.dlcm.controller.DLCMControllerAction.LIST_MEMBERS;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.dlcm.business.OrganizationalUnitService;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.security.OrganizationalUnitPermissionService;

@ExtendWith(SpringExtension.class)
class OrgUnitPermissionEvaluatorTest extends AbstractPermissionEvaluatorTest {

  private final Map<Authentication, List<DLCMControllerAction>> mapOrgUnitActions = new LinkedHashMap<>();

  @Mock
  private OrganizationalUnitService organizationalUnitService;

  private OrganizationalUnitPermissionService organizationalUnitPermissionService;

  @Mock
  protected TrustedOrganizationalUnitRemoteResourceService trustedOrganizationalUnitRemoteResourceService;

  @Override
  @BeforeEach
  void setUp() {
    super.setUp();

    // Set the map of allowed actions, valid with a OrgUnit
    this.mapOrgUnitActions.put(this.authManager, Arrays.asList(DLCMControllerAction.values()));
    this.mapOrgUnitActions.put(this.authApprover,
            Arrays.asList(GET, DOWNLOAD_FILE, HISTORY, LIST_FILES, GET_FILE, LIST_MEMBERS, GET_MEMBER, LIST_FORMS, GET_FORM, GET_CURRENT_FORM));
    this.mapOrgUnitActions.put(this.authSteward,
            Arrays.asList(GET, DOWNLOAD_FILE, HISTORY, LIST_FILES, GET_FILE, LIST_MEMBERS, GET_MEMBER, LIST_FORMS, GET_FORM, GET_CURRENT_FORM));
    this.mapOrgUnitActions.put(this.authCreator,
            Arrays.asList(GET, DOWNLOAD_FILE, HISTORY, LIST_FILES, GET_FILE, LIST_MEMBERS, GET_MEMBER, LIST_FORMS, GET_FORM, GET_CURRENT_FORM));
    this.mapOrgUnitActions.put(this.authVisitor,
            Arrays.asList(GET, DOWNLOAD_FILE, HISTORY, LIST_FILES, GET_FILE, LIST_MEMBERS, GET_MEMBER, LIST_FORMS, GET_FORM, GET_CURRENT_FORM));

    // Mock the findOne on organizationalUnitService
    final OrganizationalUnit orgUnit = new OrganizationalUnit();
    orgUnit.setResId(AbstractPermissionEvaluatorTest.ORGANIZATIONAL_UNIT_ID);
    when(this.organizationalUnitService.findOne(AbstractPermissionEvaluatorTest.EXISTENT_ID)).thenReturn(orgUnit);

    // Construct the permissions service
    this.organizationalUnitPermissionService = new OrganizationalUnitPermissionService(this.organizationalUnitService,
            this.trustedPersonRemoteResourceService, trustedOrganizationalUnitRemoteResourceService);
  }

  @Test
  void orgUnitAllowedActions() {
    this.allowedActionsTest(this.mapOrgUnitActions, this.organizationalUnitPermissionService);
  }

  @Test
  void orgUnitForbiddenActions() {
    this.forbiddenActionsTest(this.mapOrgUnitActions, this.organizationalUnitPermissionService);
  }

  @Test
  void wrongAction() {
    this.setSecurityContextAuthentication(this.authManager);
    assertThrows(SolidifyRuntimeException.class, () -> {
      this.organizationalUnitPermissionService.isAllowed(AbstractPermissionEvaluatorTest.EXISTENT_ID,
              AbstractPermissionEvaluatorTest.INEXISTENT_ACTION);
    });
  }

  @Test
  void wrongResId() {
    this.setSecurityContextAuthentication(this.authManager);
    assertThrows(NoSuchElementException.class, () -> {
      this.organizationalUnitPermissionService.isAllowed(AbstractPermissionEvaluatorTest.INEXISTENT_ID, DELETE.toString());
    });
  }

}

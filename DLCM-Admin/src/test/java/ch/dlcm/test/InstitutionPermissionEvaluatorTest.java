/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - AdminPermissionEvaluatorTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.dlcm.business.InstitutionPersonRoleService;
import ch.dlcm.business.OrganizationalUnitService;
import ch.dlcm.business.PersonInstitutionRoleService;
import ch.dlcm.business.PersonService;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.security.InstitutionPermissionService;

@ExtendWith(SpringExtension.class)
class InstitutionPermissionEvaluatorTest extends AbstractPermissionEvaluatorTest {

  @Mock
  PersonService personService;
  @Mock
  OrganizationalUnitService orgUnitService;
  @Mock
  PersonInstitutionRoleService personInstitutionRoleService;
  @Mock
  InstitutionPersonRoleService institutionPersonRoleService;

  private InstitutionPermissionService institutionPermissionService;

  @Override
  @BeforeEach
  void setUp() {
    super.setUp();
    this.institutionPermissionService = new InstitutionPermissionService(personService, orgUnitService, personInstitutionRoleService,
            institutionPersonRoleService);
  }

  @Test
  void inexistentOrgUnit() {
    final OrganizationalUnit inexistentOrgUnit = new OrganizationalUnit();
    inexistentOrgUnit.setResId(INEXISTENT_ID);
    this.setSecurityContextAuthentication(this.authManager);
    assertFalse(this.institutionPermissionService.isInstitutionalManager(inexistentOrgUnit));
  }

}

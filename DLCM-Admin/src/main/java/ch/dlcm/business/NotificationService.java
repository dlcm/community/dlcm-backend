/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - NotificationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.controller.AdminController;
import ch.dlcm.message.EmailMessage;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.dto.NotificationContributorDto;
import ch.dlcm.model.dto.NotificationsContributorDto;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationCategory;
import ch.dlcm.model.notification.NotificationMark;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.repository.NotificationRepository;
import ch.dlcm.repository.NotificationTypeRepository;
import ch.dlcm.repository.UserRepository;
import ch.dlcm.service.HistoryService;
import ch.dlcm.specification.NotificationSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class NotificationService extends ResourceService<Notification> {

  private static final String NOTIFICATION_NO_ORGANIZATIONAL_UNIT = "validation.notification.must.have.orgunit";
  private static final String NOTIFICATION_NO_ORGANIZATIONAL_UNIT_NAME = "validation.notification.newOrgUnit.missingName";
  private static final String NOTIFICATION_PENDING_ORGANIZATIONAL_UNIT_JOIN_REQUEST = "validation.notification.joinOrgUnit.pendingRequest";
  private static final String NOTIFICATION_PENDING_ARCHIVE_ACCESS_REQUEST = "validation.notification.archiveAccess.pendingRequest";
  private static final String NOTIFICATION_INFO_WRONG_STATUS = "validation.notification.info.wrongStatus";
  private static final String ACCESS_DATASET_REQUEST_REFUSAL_WITHOUT_RESPONSE = "validation.notification.datasetAccessRefusal.missingMessage";
  private static final String JOIN_ORGANIZATIONAL_UNIT_REQUEST_REFUSAL_WITHOUT_RESPONSE = "validation.notification.joinOrgUnitRefusal.missingMessage";
  private static final String CREATE_ORGANIZATIONAL_UNIT_REQUEST_REFUSAL_WITHOUT_RESPONSE = "validation.notification.createOrgUnitRefusal.missingMessage";
  private static final String APPROVE_DISPOSAL_BY_ORG_UNIT_REQUEST_REFUSAL_WITHOUT_RESPONSE = "validation.notification.approveDisposalByOrgUnitRefusal.missingMessage";
  private static final String APPROVE_DISPOSAL_REQUEST_REFUSAL_WITHOUT_RESPONSE = "validation.notification.approveDisposalRefusal.missingMessage";

  private static final List<NotificationType> NOTIFICATION_TYPES_WITH_MANDATORY_RESPONSE_WHEN_REFUSED = List.of(
          NotificationType.ACCESS_DATASET_REQUEST, NotificationType.JOIN_ORGUNIT_REQUEST, NotificationType.CREATE_ORGUNIT_REQUEST,
          NotificationType.APPROVE_DISPOSAL_REQUEST, NotificationType.APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST);
  private static final String NO_NOTIFICATION = "No such sent notification";

  private final NotificationRepository notificationRepository;
  private final NotificationTypeRepository notificationTypeRepository;
  private final UserRepository userRepository;
  private final UserService userService;
  private final HistoryService historyService;

  public NotificationService(NotificationRepository notificationRepository, NotificationTypeRepository notificationTypeRepository,
          UserRepository userRepository, UserService userService, HistoryService historyService) {
    this.notificationRepository = notificationRepository;
    this.notificationTypeRepository = notificationTypeRepository;
    this.userRepository = userRepository;
    this.userService = userService;
    this.historyService = historyService;
  }

  public Notification getNotificationForCurrentUser(String notificationId, User currentUser) {
    final Notification notification = this.notificationRepository.findInboxAndSentNotificationById(currentUser.getPerson().getResId(),
            currentUser.getResId(),
            currentUser.getApplicationRole().getLevel(), notificationId, false);
    if (notification == null) {
      throw new NoSuchElementException(NO_NOTIFICATION);
    }
    return notification;
  }

  public Notification getInboxNotification(String notificationId, User currentUser) {
    final String personId = currentUser.getPerson().getResId();
    final int userRoleLevel = currentUser.getApplicationRole().getLevel();
    final Notification notification = this.notificationRepository.findInboxAndSentNotificationById(personId, currentUser.getResId(),
            userRoleLevel, notificationId, true);
    if (notification == null) {
      throw new NoSuchElementException();
    }
    return notification;
  }

  public Notification getSentNotification(String notificationId, User currentUser) {
    final Notification sentNotification = this.notificationRepository.findByResId(notificationId);
    if (sentNotification == null || sentNotification.getEmitter() != currentUser) {
      throw new NoSuchElementException(NO_NOTIFICATION);
    }
    return sentNotification;
  }

  public Page<Notification> listInboxNotification(User currentUser, Notification search, Pageable pageable) {
    if (currentUser == null) {
      throw new SolidifyRuntimeException("The current user is null");
    }
    NotificationStatus notificationStatus = null;
    NotificationType notificationType = null;
    NotificationCategory notificationCategory = null;
    String notifiedOrgUnitId = null;
    NotificationMark notificationMark = null;
    if (search != null) {
      notificationStatus = search.getNotificationStatus();
      notificationType = search.getNotificationType();
      notificationMark = search.getNotificationMark();
      if (search.getNotifiedOrgUnit() != null) {
        notifiedOrgUnitId = search.getNotifiedOrgUnit().getResId();
      }
      if (notificationType != null) {
        notificationCategory = notificationType.getNotificationCategory();
        if (notificationCategory != null) {
          // if searching on category, don't specify type
          final NotificationType notificationTypeTemp = notificationType;
          if (NotificationType.getAllNotificationTypes().stream().noneMatch(nt -> notificationTypeTemp.getResId().equals(nt.getResId()))) {
            notificationType = null;
          }
        }
      }
    }
    Person currentPerson = currentUser.getPerson();
    if (currentPerson == null) {
      throw new SolidifyRuntimeException("The current user " + currentUser.getFirstName() + " " + currentUser.getLastName() +
              " is not associated with a person");
    }
    String personId = currentPerson.getResId();
    int userRoleLevel = currentUser.getApplicationRole().getLevel();
    return this.notificationRepository.findInboxNotificationByPersonAndStatus(personId, notificationStatus, notificationType,
            notificationCategory, notificationMark, notifiedOrgUnitId, userRoleLevel, pageable);
  }

  public Page<Notification> listSentNotification(Notification search, User currentUser, Pageable pageable) {
    search.setEmitter(currentUser);
    final Specification<Notification> specification = this.getSpecification(search);
    return this.findAll(specification, pageable);
  }

  @Transactional
  public void deleteAllNotificationsOnOrgUnit(String notifiedOrgUnitId) {
    this.notificationRepository.deleteNotificationsByNotifiedOrgUnitId(notifiedOrgUnitId);
  }

  @Transactional
  public void deleteAllNotificationsOnDeposit(String depositId) {
    this.notificationRepository.deleteNotificationsByObjectId(depositId);
  }

  @Override
  protected void validateItemSpecificRules(Notification item, BindingResult errors) {

    if (!NotificationType.CREATE_ORGUNIT_REQUEST.equals(item.getNotificationType()) && Objects.isNull(item.getNotifiedOrgUnit())) {
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "notifiedOrgUnit", this.messageService.get(NOTIFICATION_NO_ORGANIZATIONAL_UNIT)));
    } else if (NotificationType.CREATE_ORGUNIT_REQUEST.equals(item.getNotificationType()) && StringTool.isNullOrEmpty(item.getObjectId())) {
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "objectId", this.messageService.get(NOTIFICATION_NO_ORGANIZATIONAL_UNIT_NAME)));
    } else if (this.notificationRequiresResponseMessageWhenRefused(item)
            && NotificationStatus.REFUSED.equals(item.getNotificationStatus())
            && StringTool.isNullOrEmpty(item.getResponseMessage())) {
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "responseMessage",
                      this.messageService.get(this.getMissingResponseValidationErrorKey(item))));
    } else if (NotificationType.ACCESS_DATASET_REQUEST.equals(item.getNotificationType())
            && NotificationStatus.PENDING.equals(item.getNotificationStatus())
            && this.isPendingNotification(item)) {
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "objectId", this.messageService.get(NOTIFICATION_PENDING_ARCHIVE_ACCESS_REQUEST)));

    } else if (NotificationType.JOIN_ORGUNIT_REQUEST.equals(item.getNotificationType())
            && NotificationStatus.PENDING.equals(item.getNotificationStatus())
            && this.isPendingNotification(item)) {
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "notifiedOrgUnit",
                      this.messageService.get(NOTIFICATION_PENDING_ORGANIZATIONAL_UNIT_JOIN_REQUEST)));
    } else if (NotificationCategory.INFO.equals(item.getNotificationType().getNotificationCategory())
            && item.getNotificationStatus() != NotificationStatus.NON_APPLICABLE) {
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "notificationStatus", this.messageService.get(NOTIFICATION_INFO_WRONG_STATUS)));
    }
  }

  private boolean isPendingNotification(Notification item) {
    NotificationType notificationType = item.getNotificationType();

    Notification notification = new Notification();
    if (notificationType.equals(NotificationType.ACCESS_DATASET_REQUEST)) {
      notification.setObjectId(item.getObjectId());
    } else if (notificationType.equals(NotificationType.JOIN_ORGUNIT_REQUEST)) {
      OrganizationalUnit notifiedOrgUnit = new OrganizationalUnit();
      notifiedOrgUnit.setResId(item.getNotifiedOrgUnit().getResId());
      notification.setNotifiedOrgUnit(notifiedOrgUnit);
    }
    notification.setNotificationType(notificationType);
    notification.setNotificationStatus(NotificationStatus.PENDING);
    notification.setEmitter(item.getEmitter());
    NotificationSpecification specification = new NotificationSpecification(notification);
    specification.setExceptResId(item.getResId());
    return this.notificationRepository.exists(specification);
  }

  private boolean notificationRequiresResponseMessageWhenRefused(Notification item) {
    return NOTIFICATION_TYPES_WITH_MANDATORY_RESPONSE_WHEN_REFUSED.contains(item.getNotificationType());
  }

  private String getMissingResponseValidationErrorKey(Notification item) {
    if (item.getNotificationType().equals(NotificationType.ACCESS_DATASET_REQUEST)) {
      return ACCESS_DATASET_REQUEST_REFUSAL_WITHOUT_RESPONSE;
    } else if (item.getNotificationType().equals(NotificationType.JOIN_ORGUNIT_REQUEST)) {
      return JOIN_ORGANIZATIONAL_UNIT_REQUEST_REFUSAL_WITHOUT_RESPONSE;
    } else if (item.getNotificationType().equals(NotificationType.CREATE_ORGUNIT_REQUEST)) {
      return CREATE_ORGANIZATIONAL_UNIT_REQUEST_REFUSAL_WITHOUT_RESPONSE;
    } else if (item.getNotificationType().equals(NotificationType.APPROVE_DISPOSAL_REQUEST)) {
      return APPROVE_DISPOSAL_REQUEST_REFUSAL_WITHOUT_RESPONSE;
    } else if (item.getNotificationType().equals(NotificationType.APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST)) {
      return APPROVE_DISPOSAL_BY_ORG_UNIT_REQUEST_REFUSAL_WITHOUT_RESPONSE;
    }
    return null;
  }

  @Override
  public NotificationSpecification getSpecification(Notification resource) {
    return new NotificationSpecification(resource);
  }

  /**
   * Update the sentTime property of a Notification Note: Transactional is required to allow the method to be called from a scheduled task
   * context that runs outside the main thread JPA session
   *
   * @param notificationId the id of the notification
   * @param sentTime       the time at which the notification was sent
   * @return the updated notification
   */
  @Transactional
  public Notification updateSentTime(String notificationId, OffsetDateTime sentTime) {
    Notification notification = this.findOne(notificationId);
    notification.setSentTime(sentTime);
    return this.save(notification);
  }

  @Override
  public Notification save(Notification item) {

    // Email emitter for certain types of notification
    this.sendEmailToEmitter(item);

    // Status is non-applicable if notification category is info
    Optional<NotificationType> notificationType = this.notificationTypeRepository.findById(item.getNotificationType().getResId());
    if (notificationType.isPresent() && NotificationCategory.INFO.equals(notificationType.get().getNotificationCategory())) {
      item.setNotificationStatus(NotificationStatus.NON_APPLICABLE);
    }

    // Save the notification
    return super.save(item);
  }

  private void sendEmailToEmitter(Notification notification) {
    if ((notification.getNotificationType().equals(NotificationType.ACCESS_DATASET_REQUEST)
            || notification.getNotificationType().equals(NotificationType.JOIN_ORGUNIT_REQUEST)
            || notification.getNotificationType().equals(NotificationType.CREATE_ORGUNIT_REQUEST)
            || notification.getNotificationType().equals(NotificationType.APPROVE_DISPOSAL_REQUEST)
            || notification.getNotificationType().equals(NotificationType.APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST))
            && (notification.getNotificationStatus().equals(NotificationStatus.APPROVED)
              || notification.getNotificationStatus().equals(NotificationStatus.REFUSED))) {
      EmailMessage.EmailTemplate emailTemplate = this.getEmailTemplate(notification.getNotificationType(), notification.getNotificationStatus());
      if (emailTemplate != null) {
        Map<String, Object> parameters = new HashMap<>();
        if (notification.getNotificationType().equals(NotificationType.JOIN_ORGUNIT_REQUEST)) {
          parameters.put("objectId", notification.getNotifiedOrgUnit().getResId());
        } else {
          parameters.put("objectId", notification.getObjectId());
        }
        if (notification.getNotificationStatus().equals(NotificationStatus.REFUSED)) {
          parameters.put(DLCMConstants.REASON, notification.getResponseMessage());
        }
        EmailMessage emailMessage = new EmailMessage(notification.getEmitter().getEmail(), emailTemplate, parameters);
        SolidifyEventPublisher.getPublisher().publishEvent(Objects.requireNonNull(emailMessage));
      }
    }
  }

  private EmailMessage.EmailTemplate getEmailTemplate(NotificationType notificationType, NotificationStatus notificationStatus) {
    Map<String, Map<NotificationStatus, EmailMessage.EmailTemplate>> templateMap = Map.of(
            NotificationType.ACCESS_DATASET_REQUEST.getResId(), Map.of(
                    NotificationStatus.APPROVED, EmailMessage.EmailTemplate.REQUEST_TO_ACCESS_DATASET_ACCEPTED,
                    NotificationStatus.REFUSED, EmailMessage.EmailTemplate.REQUEST_TO_ACCESS_DATASET_REFUSED
            ),
            NotificationType.JOIN_ORGUNIT_REQUEST.getResId(), Map.of(
                    NotificationStatus.APPROVED, EmailMessage.EmailTemplate.REQUEST_TO_JOIN_ORG_UNIT_ACCEPTED,
                    NotificationStatus.REFUSED, EmailMessage.EmailTemplate.REQUEST_TO_JOIN_ORG_UNIT_REFUSED
            ),
            NotificationType.CREATE_ORGUNIT_REQUEST.getResId(), Map.of(
                    NotificationStatus.APPROVED, EmailMessage.EmailTemplate.REQUEST_TO_CREATE_ORG_UNIT_ACCEPTED,
                    NotificationStatus.REFUSED, EmailMessage.EmailTemplate.REQUEST_TO_CREATE_ORG_UNIT_REFUSED
            ),
            NotificationType.APPROVE_DISPOSAL_REQUEST.getResId(), Map.of(
                    NotificationStatus.APPROVED, EmailMessage.EmailTemplate.REQUEST_TO_DISPOSE_ARCHIVE_ACCEPTED
            ),
            NotificationType.APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST.getResId(), Map.of(
                    NotificationStatus.APPROVED, EmailMessage.EmailTemplate.REQUEST_TO_DISPOSE_ARCHIVE_ACCEPTED
            )
    );

    return Optional.ofNullable(templateMap.get(notificationType.getResId()))
            .map(statusMap -> statusMap.get(notificationStatus))
            .orElse(null);
  }

  /*
   * Verify that each contributorsId correspond to an authenticated user and the type of notification is the type of
   * MY_INDIRECT_DEPOSIT_COMPLETED_INFO
   */
  public List<Notification> createNotificationForContributors(NotificationsContributorDto notificationsContributorsDTO) {
    List<Notification> list = new ArrayList<>();
    for (NotificationContributorDto notificationContributorDto : notificationsContributorsDTO.getListNotifications()) {
      if (!notificationContributorDto.getNotification().getNotificationType().equals(NotificationType.MY_INDIRECT_COMPLETED_DEPOSIT_INFO)) {
        throw new SolidifyRuntimeException("only valid for MY_INDIRECT_COMPLETED_DEPOSIT_INFO notification type");
      }
      User user = this.userRepository.findByPersonResId(notificationContributorDto.getContributorsId());
      if (user != null) {
        Notification notification = notificationContributorDto.getNotification();
        notification.setRecipient(user.getPerson());
        notification.setNotificationStatus(NotificationStatus.PENDING);
        Notification item = this.save(notification);
        list.add(item);
      }
    }
    return list;
  }

  public HttpEntity<Notification> changeStatus(String id, NotificationStatus newStatus) {
    return this.changeStatus(id, newStatus, null);
  }

  public HttpEntity<Notification> changeStatus(String id, NotificationStatus newStatus, String responseMessage) {
    final Notification notification = this.getNotificationForCurrentUser(id);
    if (notification != null) {
      if (NotificationCategory.INFO.equals(notification.getNotificationType().getNotificationCategory()) &&
              NotificationStatus.REFUSED.equals(newStatus)) {
        throw new SolidifyUnmodifiableException("Notification of category INFO cannot be refused");
      }
      if (notification.getNotificationStatus().equals(NotificationStatus.APPROVED) || notification.getNotificationStatus().equals(NotificationStatus.REFUSED)) {
        throw new SolidifyUnmodifiableException("Notification already approved or refused, cannot be changed");
      }
      notification.setNotificationStatus(newStatus);
      notification.setResponseMessage(responseMessage);
      this.save(notification);
      if (NotificationStatus.NON_APPLICABLE != notification.getNotificationStatus()) {
        StatusHistory statusHistory = new StatusHistory(Notification.class.getSimpleName(), notification.getResId(), OffsetDateTime.now(),
                newStatus.toString(), newStatus.getDescription(), this.getAuthenticatedUserExternalUid());
        this.historyService.saveEvent(statusHistory);
      }
      return new ResponseEntity<>(notification, HttpStatus.OK);
    } else {
      throw new SolidifyResourceNotFoundException("Notification " + id + " not found");
    }
  }

  public HttpEntity<Notification> changeMark(String id, NotificationMark notificationMark) {
    final Notification notification = this.getNotificationForCurrentUser(id);
    if (notification != null) {
      notification.setNotificationMark(notificationMark);
      this.save(notification);
      StatusHistory statusHistory = new StatusHistory(Notification.class.getSimpleName(), notification.getResId(), OffsetDateTime.now(),
              notificationMark.toString(), notificationMark.getDescription(), this.getAuthenticatedUserExternalUid());
      this.historyService.saveEvent(statusHistory);
      return new ResponseEntity<>(notification, HttpStatus.OK);
    } else {
      throw new SolidifyResourceNotFoundException("Notification " + id + " not found");
    }
  }

  public Notification getNotificationForCurrentUser(String notificationId) {
    return this.getNotificationForCurrentUser(notificationId, this.userService.getCurrentUser());
  }

}

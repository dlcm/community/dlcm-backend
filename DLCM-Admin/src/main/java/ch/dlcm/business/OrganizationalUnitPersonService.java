/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitPersonService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.service.JoinResource3TiersService;
import ch.unige.solidify.specification.Join3TiersSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.display.OrgUnitPersonRoleDTO;
import ch.dlcm.model.display.OrgUnitPersonRoleListDTO;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.OrganizationalUnitPersonRole;
import ch.dlcm.model.settings.Person;
import ch.dlcm.specification.OrganizationalUnitPersonRoleSpecification;
import ch.dlcm.specification.PersonSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class OrganizationalUnitPersonService extends JoinResource3TiersService<OrganizationalUnit, Person, Role, OrganizationalUnitPersonRole> {

  @Override
  public Join3TiersSpecification<OrganizationalUnitPersonRole> getJoinSpecification(OrganizationalUnitPersonRole joinResource) {
    return new OrganizationalUnitPersonRoleSpecification(joinResource);
  }

  @Override
  protected SolidifySpecification<Person> getChildSpecification(Person person) {
    return new PersonSpecification(person);
  }

  @Override
  protected String getChildToRelationPropertyName() {
    return OrganizationalUnitPersonRole.PERSON_RELATION_PROPERTY_NAME;
  }

  @Override
  public Role getEmptyGrandChildResourceObject() {
    return new Role();
  }

  @Override
  public void setGrandChildResource(OrganizationalUnitPersonRole joinResource, Role role) {
    joinResource.setRole(role);
  }

  @Override
  public Role getGrandChildResource(OrganizationalUnitPersonRole joinResource) {
    return joinResource.getRole();
  }

  @Override
  public OrganizationalUnitPersonRole getEmptyJoinResourceObject() {
    return new OrganizationalUnitPersonRole();
  }

  @Override
  public OrganizationalUnit getEmptyParentResourceObject() {
    return new OrganizationalUnit();
  }

  @Override
  public Person getEmptyChildResourceObject() {
    return new Person();
  }

  @Override
  public void setParentResource(OrganizationalUnitPersonRole joinResource, OrganizationalUnit orgUnit) {
    joinResource.setOrganizationalUnit(orgUnit);
  }

  @Override
  public void setChildResource(OrganizationalUnitPersonRole joinResource, Person person) {
    joinResource.setPerson(person);
  }

  @Override
  public Relation3TiersChildDTO getChildDTO(Person person, List<OrganizationalUnitPersonRole> joinResources) {
    List<OrgUnitPersonRoleDTO> list = new ArrayList<>();
    for (OrganizationalUnitPersonRole orgUnitPersonRole : joinResources) {
      list.add(this.getGrandChildDTO(orgUnitPersonRole));
    }
    return new OrgUnitPersonRoleListDTO(person, list);
  }

  @Override
  public OrgUnitPersonRoleDTO getGrandChildDTO(OrganizationalUnitPersonRole joinResource) {
    return new OrgUnitPersonRoleDTO(joinResource);
  }

  @Override
  public List<String> getDefaultGrandChildIds() {
    return Arrays.asList(Role.VISITOR.getResId());
  }

}

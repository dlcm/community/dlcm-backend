/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - SubmissionAgreementUserService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.service.JoinResource2TiersService;
import ch.unige.solidify.specification.Join2TiersSpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.display.SubmissionAgreementUserDTO;
import ch.dlcm.model.policies.SubmissionAgreementUser;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.specification.SubmissionAgreementUserSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class SubmissionAgreementUserService
        extends JoinResource2TiersService<SubmissionAgreement, User, SubmissionAgreementUser, String> {

  @Override
  public Join2TiersSpecification<SubmissionAgreementUser> getJoinSpecification(SubmissionAgreementUser submissionAgreementUser) {
    return new SubmissionAgreementUserSpecification(submissionAgreementUser);
  }

  @Override
  public User getChildResource(SubmissionAgreementUser submissionAgreementUser) {
    return submissionAgreementUser.getUser();
  }

  @Override
  public String getChildPathFromJoinResource() {
    return SubmissionAgreementUser.PATH_TO_USER;
  }

  @Override
  public JoinResourceContainer<SubmissionAgreementUser> getChildDTO(SubmissionAgreementUser submissionAgreementUser) {
    return new SubmissionAgreementUserDTO(submissionAgreementUser);
  }

  @Override
  public SubmissionAgreementUser getEmptyJoinResourceObject() {
    return new SubmissionAgreementUser();
  }

  @Override
  public SubmissionAgreement getEmptyParentResourceObject() {
    return new SubmissionAgreement();
  }

  @Override
  public User getEmptyChildResourceObject() {
    return new User();
  }

  @Override
  public void setParentResource(SubmissionAgreementUser submissionAgreementUser, SubmissionAgreement submissionAgreement) {
    submissionAgreementUser.setSubmissionAgreement(submissionAgreement);
  }

  @Override
  public void setChildResource(SubmissionAgreementUser submissionAgreementUser, User user) {
    submissionAgreementUser.setUser(user);
  }

}

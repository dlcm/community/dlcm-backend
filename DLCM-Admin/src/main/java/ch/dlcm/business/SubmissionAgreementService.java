/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - SubmissionAgreementService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.service.ResourceService;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.repository.SubmissionAgreementRepository;
import ch.dlcm.specification.SubmissionAgreementSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class SubmissionAgreementService extends ResourceService<SubmissionAgreement> {

  private static final String TITLE_VERSION_ALREADY_EXIST = "validation.submissionAgreement.error.titleAndVersionAlreadyExist";

  @Override
  public SubmissionAgreementSpecification getSpecification(SubmissionAgreement resource) {
    return new SubmissionAgreementSpecification(resource);
  }

  @Override
  protected void validateItemSpecificRules(SubmissionAgreement item, BindingResult errors) {

    /**
     * Check title + version uniqueness
     */
    SubmissionAgreement existingSubmissionAgreement = ((SubmissionAgreementRepository) this.itemRepository).findByTitleAndVersion(
            item.getTitle(), item.getVersion());
    if (existingSubmissionAgreement != null && !existingSubmissionAgreement.getResId().equals(item.getResId())) {
      errors.addError(new FieldError(item.getClass().getSimpleName(), "title", this.messageService.get(TITLE_VERSION_ALREADY_EXIST)));
      errors.addError(new FieldError(item.getClass().getSimpleName(), "version", this.messageService.get(TITLE_VERSION_ALREADY_EXIST)));
    }
  }
}

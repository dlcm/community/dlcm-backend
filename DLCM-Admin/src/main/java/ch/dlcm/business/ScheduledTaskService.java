/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - ScheduledTaskService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import static ch.unige.solidify.service.SchedulerService.SchedulingCancelType.DONT_INTERRUPT_RUNNING_TASK;
import static ch.unige.solidify.service.SchedulerService.SchedulingCancelType.INTERRUPT_RUNNING_TASK;

import java.util.List;

import jakarta.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.scheduler.AbstractTaskRunnable;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.service.SchedulerService;
import ch.unige.solidify.service.SchedulerService.SchedulingCancelType;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.schedule.ScheduledTask;
import ch.dlcm.model.schedule.TaskType;
import ch.dlcm.service.NotificationsEmailService;
import ch.dlcm.specification.ScheduledTaskSpecification;
import ch.dlcm.task.NotifyArchiveAccessRequestTask;
import ch.dlcm.task.NotifyArchivesCompletedToApproversTask;
import ch.dlcm.task.NotifyArchivesCompletedToCreatorTask;
import ch.dlcm.task.NotifyDepositsApprovedToCreatorTask;
import ch.dlcm.task.NotifyDepositsCompletedToContributorsTask;
import ch.dlcm.task.NotifyDepositsCompletedToCreator;
import ch.dlcm.task.NotifyDepositsInErrorTask;
import ch.dlcm.task.NotifyDepositsToValidateTask;
import ch.dlcm.task.NotifyJoinMemberOrgUnitToValidateTask;
import ch.dlcm.task.NotifyOrgUnitCreationToValidateTask;

@Service
@ConditionalOnBean(AdminController.class)
public class ScheduledTaskService extends ResourceService<ScheduledTask> {
  private static final Logger log = LoggerFactory.getLogger(ScheduledTaskService.class);

  private final SchedulerService<ScheduledTask> schedulerService;
  private final NotificationsEmailService notificationsEmailService;
  private final NotificationService notificationService;

  public ScheduledTaskService(SchedulerService<ScheduledTask> schedulerService, NotificationService notificationService,
          NotificationsEmailService notificationsEmailService) {
    this.schedulerService = schedulerService;
    this.notificationService = notificationService;
    this.notificationsEmailService = notificationsEmailService;
  }

  @Override
  public SolidifySpecification<ScheduledTask> getSpecification(ScheduledTask scheduledTask) {
    return new ScheduledTaskSpecification(scheduledTask);
  }

  /**
   * Cancel all tasks scheduling. If a task is running it completes until the end.
   */
  public void disableAllTasksScheduling() {
    this.schedulerService.cancelAllTasksScheduling();
    log.info("All scheduled tasks cancelled");
  }

  /**
   * unschedule and/or (re)start a task depending on its 'enabled' property
   *
   * @param scheduledTask
   */
  public void applyTaskScheduling(ScheduledTask scheduledTask) {
    this.cancelTaskSchedulingIfScheduled(scheduledTask, DONT_INTERRUPT_RUNNING_TASK);
    this.startTaskSchedulingIfEnabled(scheduledTask);
  }

  /**
   * Get all tasks from database and (re)start scheduling for the ones that are enabled.
   * The tasks that are disabled get unscheduled.
   */
  @PostConstruct()
  public void scheduleEnabledTasks() {
    this.disableAllTasksScheduling();
    List<ScheduledTask> enabledScheduledTaskList = this.findAll(Pageable.unpaged()).toList().stream().filter(ScheduledTask::isEnabled).toList();
    if (!enabledScheduledTaskList.isEmpty()) {
      for (ScheduledTask scheduledTask : enabledScheduledTaskList) {
        this.applyTaskScheduling(scheduledTask);
      }
    } else {
      log.info("There is no task to schedule");
    }
  }

  /**
   * Unschedule a task and stop its execution if it is running
   *
   * @param scheduledTask
   */
  public void killTask(ScheduledTask scheduledTask) {
    this.schedulerService.cancelTaskScheduling(scheduledTask, INTERRUPT_RUNNING_TASK);
    scheduledTask.setEnabled(false);
    this.save(scheduledTask);
    log.info("Scheduling cancelled for task '{}' ({}) with kill running task signal", scheduledTask.getResId(), scheduledTask.getName());
  }

  /****************************************************************/

  private void startTaskSchedulingIfEnabled(ScheduledTask scheduledTask) {
    if (Boolean.TRUE.equals(scheduledTask.isEnabled())) {
      this.schedulerService.startTaskScheduling(this.getRunnableTask(scheduledTask));
      log.info("Scheduling started for task '{}' ({}) [next execution: {}]", scheduledTask.getResId(), scheduledTask.getName(),
              scheduledTask.getNextExecution());
    } else {
      log.debug("Task '{}' ({}) won't be scheduled as it is disabled", scheduledTask.getResId(), scheduledTask.getName());
    }
  }

  private void cancelTaskSchedulingIfScheduled(ScheduledTask scheduledTask, SchedulingCancelType schedulingCancelType) {
    if (this.schedulerService.isTaskScheduled(scheduledTask)) {
      this.schedulerService.cancelTaskScheduling(scheduledTask, schedulingCancelType);
      log.info("Scheduling cancelled for task '{}' ({})", scheduledTask.getResId(), scheduledTask.getName());
    } else {
      log.debug("No need to cancel task '{}' ({}) as it is not scheduled", scheduledTask.getResId(), scheduledTask.getName());
    }
  }

  /**
   * Get a runnable object corresponding to the task type
   *
   * @param scheduledTask
   * @return
   */
  private AbstractTaskRunnable<ScheduledTask> getRunnableTask(ScheduledTask scheduledTask) {
    return switch (scheduledTask.getTaskType()) {
      case NOTIFY_DEPOSITS_TO_VALIDATE -> new NotifyDepositsToValidateTask(scheduledTask, this, this.notificationsEmailService,
              this.notificationService);
      case NOTIFY_APPROVED_DEPOSITS_CREATOR -> new NotifyDepositsApprovedToCreatorTask(scheduledTask, this, this.notificationsEmailService,
              this.notificationService);
      case NOTIFY_COMPLETED_DEPOSITS_CREATOR -> new NotifyDepositsCompletedToCreator(scheduledTask, this, this.notificationsEmailService,
              this.notificationService);
      case NOTIFY_DEPOSITS_IN_ERROR -> new NotifyDepositsInErrorTask(scheduledTask, this, this.notificationsEmailService,
              this.notificationService);
      case NOTIFY_COMPLETED_DEPOSITS_CONTRIBUTORS -> new NotifyDepositsCompletedToContributorsTask(scheduledTask, this,
              this.notificationsEmailService, this.notificationService);
      case NOTIFY_COMPLETED_ARCHIVES_CREATOR -> new NotifyArchivesCompletedToCreatorTask(scheduledTask, this, this.notificationsEmailService,
              this.notificationService);
      case NOTIFY_COMPLETED_ARCHIVES_APPROVERS -> new NotifyArchivesCompletedToApproversTask(scheduledTask, this, this.notificationsEmailService,
              this.notificationService);
      case NOTIFY_ORG_UNIT_CREATION_TO_VALIDATE -> new NotifyOrgUnitCreationToValidateTask(scheduledTask, this, this.notificationsEmailService,
              this.notificationService);
      case NOTIFY_JOIN_MEMBER_ORG_UNIT_TO_VALIDATE -> new NotifyJoinMemberOrgUnitToValidateTask(scheduledTask, this,
              this.notificationsEmailService, this.notificationService);
      case NOTIFY_ARCHIVE_ACCESS_REQUEST -> new NotifyArchiveAccessRequestTask(scheduledTask, this, this.notificationsEmailService,
              this.notificationService);
      default -> throw new UnsupportedOperationException("Unsupported scheduled task type " + scheduledTask.getTaskType());
    };
  }

  /****************************************************************/

  public void createScheduledTaskFromConfig(DLCMProperties.ScheduledTaskConfig scheduledTaskConfig) {
    if (!StringTool.isNullOrEmpty(scheduledTaskConfig.getId())) {
      if (!this.existsById(scheduledTaskConfig.getId())) {
        ScheduledTask scheduledTask = new ScheduledTask();
        scheduledTask.setResId(scheduledTaskConfig.getId());
        scheduledTask.setTaskType(TaskType.valueOf(scheduledTaskConfig.getType()));
        scheduledTask.setName(scheduledTaskConfig.getName());
        scheduledTask.setCronExpression(scheduledTaskConfig.getCronExpression());
        scheduledTask.setEnabled(scheduledTaskConfig.isEnabled());
        ScheduledTask savedScheduledTask = this.save(scheduledTask);
        log.info("ScheduledTask '{}' of type '{}' created", savedScheduledTask.getResId(), savedScheduledTask.getTaskType().name());
        if (Boolean.TRUE.equals(savedScheduledTask.isEnabled())) {
          this.startTaskSchedulingIfEnabled(savedScheduledTask);
        }
      }
    } else {
      log.warn("invalid config for scheduled task '{}'", scheduledTaskConfig.getName());
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - LicenseService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.ResourceService;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.License;
import ch.dlcm.repository.LicenseRepository;
import ch.dlcm.specification.LicenseSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class LicenseService extends ResourceService<License> {

  private static final Logger log = LoggerFactory.getLogger(LicenseService.class);

  private final LicenseRepository licenseRepository;

  public LicenseService(LicenseRepository licenseRepository) {
    this.licenseRepository = licenseRepository;
  }

  public License getByOpenLicenseId(String openLicenseid) {
    License item = this.licenseRepository.findByOpenLicenseId(openLicenseid)
            .orElseThrow(() -> new NoSuchElementException("No resource with Open License Id " + openLicenseid));
    this.afterFind(item);
    return item;
  }

  @Override
  public LicenseSpecification getSpecification(License resource) {
    return new LicenseSpecification(resource);
  }

  public void initOptionalData() {
    this.createLicense("CC0-1.0", "Creative Commons Zero v1.0 Universal", "https://creativecommons.org/publicdomain/zero/1.0/",
            "Creative Commons");
    this.createLicense("CC-BY-4.0", "Creative Commons Attribution 4.0 International", "https://creativecommons.org/licenses/by/4.0/",
            "Creative Commons");
    this.createLicense("CC-BY-SA-4.0", "Creative Commons Attribution Share Alike 4.0 International",
            "https://creativecommons.org/licenses/by-sa/4.0/",
            "Creative Commons");
    this.createLicense("CC-BY-ND-4.0", "Creative Commons Attribution No Derivatives 4.0 International",
            "https://creativecommons.org/licenses/by-nd/4.0/", "Creative Commons");
    this.createLicense("CC-BY-NC-4.0", "Creative Commons Attribution Non Commercial 4.0 International",
            "https://creativecommons.org/licenses/by-nc/4.0/", "Creative Commons");
    this.createLicense("CC-BY-NC-SA-4.0", "Creative Commons Attribution Non Commercial Share Alike 4.0 International",
            "https://creativecommons.org/licenses/by-nc-sa/4.0/", "Creative Commons");
    this.createLicense("CC-BY-NC-ND-4.0", "Creative Commons Attribution Non Commercial No Derivatives 4.0 International",
            "https://creativecommons.org/licenses/by-nc-nd/4.0/", "Creative Commons");
  }

  private void createLicense(String openLicenseId, String title, String webSite, String maintainer) {
    if (this.licenseRepository.findByOpenLicenseId(openLicenseId).isEmpty()) {
      License l = new License();
      l.setResId(openLicenseId);
      l.setDomainContent(true);
      l.setDomainData(true);
      l.setDomainSoftware(false);
      l.setOpenLicenseId(openLicenseId);
      l.setMaintainer(maintainer);
      l.setTitle(title);
      try {
        l.setUrl(new URL(webSite));
      } catch (MalformedURLException e) {
        this.log.warn("Error in creating license: Url {} is malformed", webSite);
      }
      l.setOdConformance(License.ConformanceStatus.NOT_REVIEWED);
      l.setOsdConformance(License.ConformanceStatus.NOT_REVIEWED);
      l.setStatus(License.LicenseStatus.ACTIVE);
      l.setIsGeneric(true);
      this.save(l);
      this.log.info("License '{}' created", openLicenseId);
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - RatingTypeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.RatingType;

@Service
@ConditionalOnBean(AdminController.class)
public class RatingTypeService extends ResourceService<RatingType> {

  private static final Logger log = LoggerFactory.getLogger(RatingTypeService.class);

  @Override
  public SolidifySpecification<RatingType> getSpecification(RatingType resource) {
    return null;
  }

  public void initRequiredData() {
    for (RatingType ratingType : RatingType.getRatingTypeList()) {
      if (this.itemRepository.findById(ratingType.getResId()).isEmpty()) {
        this.save(ratingType);
        this.log.info("RatingType {} created", ratingType.getName());
      }
    }
  }
}

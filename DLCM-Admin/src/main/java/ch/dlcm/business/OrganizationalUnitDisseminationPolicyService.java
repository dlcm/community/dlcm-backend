/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitDisseminationPolicyService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.display.OrganizationalUnitDisseminationPolicyDTO;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.model.policies.OrganizationalUnitDisseminationPolicy;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.repository.OrganizationalUnitDisseminationPolicyRepository;
import ch.dlcm.specification.OrganizationalUnitDisseminationPolicySpecification;
import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.service.JoinResource2TiersService;
import ch.unige.solidify.specification.Join2TiersSpecification;
import ch.unige.solidify.util.StringTool;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@Service
@ConditionalOnBean(AdminController.class)
public class OrganizationalUnitDisseminationPolicyService
        extends JoinResource2TiersService<OrganizationalUnit, DisseminationPolicy, OrganizationalUnitDisseminationPolicy, String> {

  private static final String NAME_KEY = "name";
  private static final String PARAMETERS_FIELD = "parameters";
  private final OrganizationalUnitDisseminationPolicyRepository orgUnitDisseminationPolicyRepository;

  public OrganizationalUnitDisseminationPolicyService(OrganizationalUnitDisseminationPolicyRepository orgUnitDisseminationPolicyRepository) {
    this.orgUnitDisseminationPolicyRepository = orgUnitDisseminationPolicyRepository;
  }

  @Override
  public Join2TiersSpecification<OrganizationalUnitDisseminationPolicy> getJoinSpecification(
          OrganizationalUnitDisseminationPolicy orgUnitDisseminationPolicy) {
    return new OrganizationalUnitDisseminationPolicySpecification(orgUnitDisseminationPolicy);
  }

  @Override
  public DisseminationPolicy getChildResource(OrganizationalUnitDisseminationPolicy orgUnitDisseminationPolicy) {
    return orgUnitDisseminationPolicy.getDisseminationPolicy();
  }

  @Override
  public String getChildPathFromJoinResource() {
    return OrganizationalUnitDisseminationPolicy.PATH_TO_DISSEMINATION_POLICY;
  }

  @Override
  public JoinResourceContainer<OrganizationalUnitDisseminationPolicy> getChildDTO(
          OrganizationalUnitDisseminationPolicy orgUnitDisseminationPolicy) {
    return new OrganizationalUnitDisseminationPolicyDTO(orgUnitDisseminationPolicy);
  }

  @Override
  public OrganizationalUnitDisseminationPolicy getEmptyJoinResourceObject() {
    return new OrganizationalUnitDisseminationPolicy();
  }

  @Override
  public OrganizationalUnit getEmptyParentResourceObject() {
    return new OrganizationalUnit();
  }

  @Override
  public DisseminationPolicy getEmptyChildResourceObject() {
    return new DisseminationPolicy();
  }

  @Override
  public void setParentResource(OrganizationalUnitDisseminationPolicy orgUnitDisseminationPolicy, OrganizationalUnit orgUnit) {
    orgUnitDisseminationPolicy.setOrganizationalUnit(orgUnit);
  }

  @Override
  public void setChildResource(OrganizationalUnitDisseminationPolicy orgUnitDisseminationPolicy, DisseminationPolicy policy) {
    orgUnitDisseminationPolicy.setDisseminationPolicy(policy);
  }

  @Override
  protected void validateItemSpecificRules(OrganizationalUnitDisseminationPolicy orgUnitDisseminationPolicy, BindingResult errors) {
    List<OrganizationalUnitDisseminationPolicy> list = this.orgUnitDisseminationPolicyRepository.findByOrgUnitId(
            orgUnitDisseminationPolicy.getOrganizationalUnit().getResId());

    // Ignore current
    boolean isUpdate = !StringTool.isNullOrEmpty(orgUnitDisseminationPolicy.getCompositeKey());
    if (isUpdate) {
      list = list.stream()
              .filter(o -> o.getCompositeKey() != null
                      && !o.getCompositeKey().equals(orgUnitDisseminationPolicy.getCompositeKey()))
              .toList();
    }

    // Check is another association exist with same name
    Map<String, String> parametersMap = orgUnitDisseminationPolicy.getParametersMap();
    String disseminationPolicyName = parametersMap.get(NAME_KEY);
    if (list.stream().anyMatch(
            o -> o.getParametersMap() != null && o.getParametersMap().containsKey(NAME_KEY)
                    && o.getParametersMap().get(NAME_KEY).equalsIgnoreCase(disseminationPolicyName))) {
      errors.addError(new FieldError(orgUnitDisseminationPolicy.getClass().getSimpleName(), PARAMETERS_FIELD,
              this.messageService.get("validation.disseminationPolicy.organizationUnit.nameAlreadyUsed",
                      new Object[] { disseminationPolicyName })));
    }

    // Check is another association of same type have same params
    list.stream()
            .filter(o -> o.getDisseminationPolicy().getResId().equals(orgUnitDisseminationPolicy.getDisseminationPolicy().getResId()))
            .filter(o -> o.getParametersMap() != null && (o.getParametersMap().size() != 1 || !o.getParametersMap().containsKey(NAME_KEY)))
            .forEach(o -> {
              boolean hasSameParams = true;

              String currentName = o.getParametersMap().get(NAME_KEY);

              for (Map.Entry<String, String> entry : parametersMap.entrySet()) {
                if (!entry.getKey().equals(NAME_KEY)) { // Exclude 'name' from comparison
                  String otherValue = o.getParametersMap().get(entry.getKey());
                  if (otherValue == null || !otherValue.equals(entry.getValue())) {
                    hasSameParams = false;
                    break;
                  }
                }
              }
              if (hasSameParams) {
                errors.addError(new FieldError(orgUnitDisseminationPolicy.getClass().getSimpleName(), PARAMETERS_FIELD,
                        this.messageService.get("validation.disseminationPolicy.organizationUnit.sameParamsExist",
                                new Object[] { currentName })));
              }
            });

    if (!parametersMap.isEmpty() && parametersMap.containsKey("mediaPattern") && parametersMap.containsKey("manifestPattern")) {
      String mediaPattern = parametersMap.get("mediaPattern");
      String manifestPattern = parametersMap.get("manifestPattern");
      if (mediaPattern.equals(manifestPattern)) {
        errors.addError(new FieldError(orgUnitDisseminationPolicy.getClass().getSimpleName(), PARAMETERS_FIELD,
                this.messageService.get("validation.disseminationPolicy.organizationUnit.patternAreTheSame",
                        new Object[] { mediaPattern, manifestPattern })));
      }
      // Check if patterns are valid
      this.validatePattern(orgUnitDisseminationPolicy, errors, mediaPattern);
      this.validatePattern(orgUnitDisseminationPolicy, errors, manifestPattern);

    }
  }

  private void validatePattern(OrganizationalUnitDisseminationPolicy orgUnitDisseminationPolicy, BindingResult errors, String patterns) {
    Stream.of(patterns.split(","))
            .map(String::trim)
            .forEach(pattern -> {
              if (!new AntPathMatcher().isPattern(pattern)) {
                errors.addError(new FieldError(orgUnitDisseminationPolicy.getClass().getSimpleName(), PARAMETERS_FIELD,
                        this.messageService.get("validation.disseminationPolicy.organizationUnit.invalidPattern",
                                new Object[]{pattern})));
              }
            });
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - ArchiveStatisticsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.service.ResourceService;

import ch.dlcm.DLCMConstants;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.ArchiveStatistics;
import ch.dlcm.service.SearchMgmt;
import ch.dlcm.specification.ArchiveStatisticsSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class ArchiveStatisticsService extends ResourceService<ArchiveStatistics> {
  private static final Logger log = LoggerFactory.getLogger(ArchiveStatisticsService.class);

  private final SearchMgmt searchMgmt;

  public ArchiveStatisticsService(SearchMgmt searchMgmt) {
    this.searchMgmt = searchMgmt;
  }

  public ArchiveStatistics addView(String resId) {
    ArchiveStatistics archive = this.findOne(resId);
    archive.addView();
    return this.save(archive);
  }

  public ArchiveStatistics addDownload(String resId) {
    ArchiveStatistics archive = this.findOne(resId);
    archive.addDownload();
    return this.save(archive);
  }

  @Override
  protected void validateItemSpecificRules(ArchiveStatistics archiveStats, BindingResult errors) {
    // Check if AIP exists
    if (!this.searchMgmt.checkMetadata(archiveStats.getResId())) {
      errors.addError(new FieldError(archiveStats.getClass().getSimpleName(), "resId",
              this.messageService.get("validation.archive.notexist", new Object[] { archiveStats.getResId() })));
    }
  }

  @Override
  public ArchiveStatisticsSpecification getSpecification(ArchiveStatistics resource) {
    return new ArchiveStatisticsSpecification(resource);
  }

  public void initDocumentationData() {
    this.createArchiveStatistics(DLCMConstants.TEST_RES_ID);
  }

  private void createArchiveStatistics(String resId) {
    if (this.itemRepository.findById(resId).isEmpty()) {
      ArchiveStatistics a = new ArchiveStatistics();
      a.setResId(resId);
      a.init();
      this.save(a);
      this.log.info("Archive Statistics '{}' created", resId);
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - ArchiveUserRatingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.dto.AverageRating;
import ch.dlcm.model.settings.ArchiveUserRating;
import ch.dlcm.repository.ArchiveUserRatingRepository;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.specification.ArchiveUserRatingSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class ArchiveUserRatingService extends ResourceService<ArchiveUserRating> {

  private static final String NOT_EXIST = "validation.resource.notexist";

  private final UserService userService;
  private final RatingTypeService ratingTypeService;
  private final FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService;

  public ArchiveUserRatingService(UserService userService, RatingTypeService ratingTypeService,
          FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService) {
    this.userService = userService;
    this.ratingTypeService = ratingTypeService;
    this.archivePublicMetadataRemoteResourceService = archivePublicMetadataRemoteResourceService;
  }

  @Override
  public void validateLinkedResources(ArchiveUserRating archiveUserRating, BindingResult errors) {
    /*
     * User
     */
    if (archiveUserRating.getUser() != null && !StringTool.isNullOrEmpty(archiveUserRating.getUser().getResId())
            && !this.userService.existsById(archiveUserRating.getUser().getResId())) {
      errors.addError(new FieldError(archiveUserRating.getClass().getSimpleName(), "user",
              this.messageService.get(NOT_EXIST, new Object[] { archiveUserRating.getUser().getResId() })));
    }

    /*
     * Rating Type
     */
    if (archiveUserRating.getRatingType() != null && !StringTool.isNullOrEmpty(archiveUserRating.getRatingType().getResId())
            && !this.ratingTypeService.existsById(archiveUserRating.getRatingType().getResId())) {
      errors.addError(new FieldError(archiveUserRating.getClass().getSimpleName(), "ratingType",
              this.messageService.get(NOT_EXIST, new Object[] { archiveUserRating.getRatingType().getResId() })));
    }

  }

  @Override
  protected void validateItemSpecificRules(ArchiveUserRating archiveUserRating, BindingResult errors) {
    /*
     * Check that the archive id exits.
     */
    if (archiveUserRating.getArchiveId() != null) {
      try {
        this.archivePublicMetadataRemoteResourceService.getIndexMetadata(archiveUserRating.getArchiveId());
      } catch (SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(archiveUserRating.getClass().getSimpleName(), "archiveId",
                this.messageService.get("validation.archive.notexist", new Object[] { archiveUserRating.getResId() })));
      } catch (Exception e) {
        throw e;
      }
    }

    /*
     * Grade
     */
    if (archiveUserRating.getGrade() == null) {
      errors.addError(new FieldError(archiveUserRating.getClass().getSimpleName(), "grade",
              this.messageService.get("validation.archiveUserRating.grade")));
    }

    /*
     * Grade value between 1 and 5
     */
    if ((archiveUserRating.getGrade() != null) && (archiveUserRating.getGrade() < 1 || archiveUserRating.getGrade() > 5)) {
      errors.addError(new FieldError(archiveUserRating.getClass().getSimpleName(), "grade",
              this.messageService.get("validation.archiveUserRating.gradeNotValid")));
    }
  }

  @Override
  public ArchiveUserRating save(ArchiveUserRating archiveUserRating) {
    try {
      return super.save(archiveUserRating);
    } catch (DataIntegrityViolationException e) {
      Exception rootCause = (Exception) e.getRootCause();
      if (rootCause != null && rootCause.getMessage().toLowerCase().contains("duplicate")) {
        ValidationError validationError = new ValidationError();
        validationError.addError("user", this.messageService.get("validation.archiveUserRating.duplicate"));
        validationError.addError("archiveId", this.messageService.get("validation.archiveUserRating.duplicate"));
        validationError.addError("ratingType", this.messageService.get("validation.archiveUserRating.duplicate"));
        throw new SolidifyValidationException(validationError);
      } else {
        throw e;
      }
    }
  }

  public List<ArchiveUserRating> findByArchiveId(String archiveId) {
    return ((ArchiveUserRatingRepository) this.itemRepository).findByArchiveId(archiveId);

  }

  /**
   * This method is intended to be used only in admin module and it's a directly called to the
   * repository
   *
   * @param archiveId
   * @return
   */
  public List<AverageRating> findAverageRating(String archiveId) {
    return ((ArchiveUserRatingRepository) this.itemRepository).findRatingAverageByArchiveId(archiveId);
  }

  @Override
  public ArchiveUserRatingSpecification getSpecification(ArchiveUserRating resource) {
    return new ArchiveUserRatingSpecification(resource);
  }
}

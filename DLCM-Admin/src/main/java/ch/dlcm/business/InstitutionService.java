/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - InstitutionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ValidationTool;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.repository.InstitutionRepository;
import ch.dlcm.service.RorService;
import ch.dlcm.specification.InstitutionSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class InstitutionService extends ResourceService<Institution> {
  private static final Logger log = LoggerFactory.getLogger(InstitutionService.class);

  private final InstitutionRepository institutionRepository;
  private final RorService rorService;

  public InstitutionService(InstitutionRepository institutionRepository, RorService rorService) {
    super();
    this.institutionRepository = institutionRepository;
    this.rorService = rorService;
  }

  @Override
  public InstitutionSpecification getSpecification(Institution resource) {
    return new InstitutionSpecification(resource);
  }

  public void initOptionalData() {
    this.createInstitution("UNIGE", "Université de Genève", "https://www.unige.ch/", "01swzsf04", "unige.ch");
    this.createInstitution("EPFL", "Ecole Polytechnique Fédérale de Lausanne", "https://www.epfl.ch/", "02s376052", "epfl.ch");
    this.createInstitution("ETH", "ETH Zürich", "https://www.ethz.ch/", "05a28rw58", "ethz.ch");
    this.createInstitution("UZH", "University of Zurich", "https://www.uzh.ch/", "02crff812", "uzh.ch");
    this.createInstitution("HES-SO", "Haute Ecole Spécialisée de Suisse Occidentale", "https://www.hes-so.ch/", "01xkakk17", "hesge.ch",
            "hes-so.ch");
    this.createInstitution("ZHAW", "Zurich University of Applied Sciences", "https://www.zhaw.ch/", "05pmsvm27", "zhaw.ch");
    this.createInstitution("HUG", "Hôpitaux Universitaires de Genève", "https://www.hug-ge.ch", "01m1pv723", "hcuge.ch");
    this.createInstitution("IHEID", "Graduate Institute of International & Development Studies", "https://www.graduateinstitute.ch/",
            "007ygn379", "graduateinstitute.ch");
    this.createInstitution("SWITCHedu-ID", "SWITCH edu-ID", "https://www.switch.ch/edu-id/", null, "eduid.ch");
    this.createInstitution("UNIL", "Université de Lausanne", "https://www.unil.ch/", "019whta54", "unil.ch");
    this.createInstitution("CERN", "European Organization for Nuclear Research", "https://home.cern/", "01ggx4157", "cern.ch");
    this.createInstitution("UNIFR", "Université de Fribourg", "https://www.unifr.ch", "022fs9h90", "unifr.ch");
    this.createInstitution("BFH", "Bern University of Applied Sciences", "https://www.bfh.ch", "02bnkt322", "bfh.ch");
  }

  private void createInstitution(String name, String description, String webSite, String rorId, String... emailSuffixes) {
    if (((InstitutionRepository) this.itemRepository).findByName(name).isEmpty()) {
      Institution i = new Institution();
      i.setName(name);
      i.setDescription(description);
      // URL
      if (!StringTool.isNullOrEmpty(webSite)) {
        try {
          i.setUrl(new URL(webSite));
        } catch (MalformedURLException e) {
          InstitutionService.log.warn("Error in creating institution: URL {} is malformed", webSite);
        }
      }
      // ROR ID
      if (!StringTool.isNullOrEmpty(rorId)) {
        i.setRorId(rorId);
      }
      // Email suffuxes
      if (!StringTool.isNullOrEmpty(emailSuffixes[0])) {
        for (String emailSuffix : emailSuffixes) {
          i.getEmailSuffixes().add(emailSuffix);
        }
      }
      this.save(i);
      InstitutionService.log.info("Institution '{}' created", name);
    }
  }

  @Override
  public void validateItemSpecificRules(Institution institution, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(institution.getRorId()) && !ValidationTool.isValidRorId(institution.getRorId())) {
      errors.addError(new FieldError(institution.getClass().getSimpleName(), "rorId",
              this.messageService.get("validation.rorId.invalid", new Object[] { institution.getRorId() })));
    }
  }

  @Override
  protected Institution afterFind(Institution institution) {
    super.afterFind(institution);
    if (!StringTool.isNullOrEmpty(institution.getRorId())) {
      this.rorService.setInfo(institution, this.rorService.loadInfo(institution.getRorId()));
    }
    return institution;
  }

  public Institution getByRorId(String rorId) {
    Institution item = this.institutionRepository.findByRorId(rorId)
            .orElseThrow(() -> new NoSuchElementException("No resource with ROR Id " + rorId));
    this.afterFind(item);
    return item;
  }

  public Institution getByName(String name) {
    Institution item = this.institutionRepository.findByName(name)
            .orElseThrow(() -> new NoSuchElementException("No resource with name " + name));
    this.afterFind(item);
    return item;
  }

}

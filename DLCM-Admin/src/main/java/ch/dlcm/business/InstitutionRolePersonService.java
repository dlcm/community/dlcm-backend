/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - InstitutionRolePersonService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.service.JoinResource3TiersService;
import ch.unige.solidify.specification.Join3TiersSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.display.InstitutionRolePersonDTO;
import ch.dlcm.model.display.InstitutionRolePersonListDTO;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.InstitutionPersonRole;
import ch.dlcm.model.settings.Person;
import ch.dlcm.specification.InstitutionRolePersonSpecification;
import ch.dlcm.specification.RoleSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class InstitutionRolePersonService extends JoinResource3TiersService<Institution, Role, Person, InstitutionPersonRole> {

  @Override
  public Join3TiersSpecification<InstitutionPersonRole> getJoinSpecification(InstitutionPersonRole joinResource) {
    return new InstitutionRolePersonSpecification(joinResource);
  }

  @Override
  protected SolidifySpecification<Role> getChildSpecification(Role role) {
    return new RoleSpecification(role);
  }

  @Override
  protected String getChildToRelationPropertyName() {
    return InstitutionPersonRole.ROLE_RELATION_PROPERTY_NAME;
  }

  @Override
  public Person getEmptyGrandChildResourceObject() {
    return new Person();
  }

  @Override
  public void setGrandChildResource(InstitutionPersonRole joinResource, Person grandChildResource) {
    joinResource.setPerson(grandChildResource);
  }

  @Override
  public Person getGrandChildResource(InstitutionPersonRole joinResource) {
    return joinResource.getPerson();
  }

  @Override
  public Relation3TiersChildDTO getChildDTO(Role childItem, List<InstitutionPersonRole> joinResources) {
    List<InstitutionRolePersonDTO> list = new ArrayList<>();
    for (InstitutionPersonRole institutionPersonRole : joinResources) {
      list.add(this.getGrandChildDTO(institutionPersonRole));
    }
    return new InstitutionRolePersonListDTO(childItem, list);
  }

  @Override
  public InstitutionRolePersonDTO getGrandChildDTO(InstitutionPersonRole joinResource) {
    return new InstitutionRolePersonDTO(joinResource);
  }

  @Override
  public InstitutionPersonRole getEmptyJoinResourceObject() {
    return new InstitutionPersonRole();
  }

  @Override
  public Institution getEmptyParentResourceObject() {
    return new Institution();
  }

  @Override
  public Role getEmptyChildResourceObject() {
    return new Role();
  }

  @Override
  public void setParentResource(InstitutionPersonRole joinResource, Institution institution) {
    joinResource.setInstitution(institution);

  }

  @Override
  public void setChildResource(InstitutionPersonRole joinResource, Role role) {
    joinResource.setRole(role);

  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - FundingAgencyService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ValidationTool;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.repository.FundingAgencyRepository;
import ch.dlcm.service.RorService;
import ch.dlcm.specification.FundingAgencySpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class FundingAgencyService extends ResourceService<FundingAgency> {

  private static final Logger log = LoggerFactory.getLogger(FundingAgencyService.class);

  private final FundingAgencyRepository fundingAgencyRepository;
  private final RorService rorService;

  public FundingAgencyService(FundingAgencyRepository fundingAgencyRepository, RorService rorService) {
    super();
    this.fundingAgencyRepository = fundingAgencyRepository;
    this.rorService = rorService;
  }

  @Override
  public FundingAgencySpecification getSpecification(FundingAgency resource) {
    return new FundingAgencySpecification(resource);
  }

  public void initOptionalData() {
    this.createFundingAgency("swissuniversities", "swissuniversities", "https://www.swissuniversities.ch", "02fsagg23");
    this.createFundingAgency("SNF", "Swiss National Science Foundation", "https://www.snf.ch", "00yjd3n13");
    this.createFundingAgency("CTI", "Commission for Technology and Innovation", "https://www.kti.admin.ch/", "012y9zp98");
    this.createFundingAgency("H2020", "Horizon 2020", "https://ec.europa.eu/programmes/horizon2020/", "");
  }

  private void createFundingAgency(String acronym, String name, String url, String rorId) {
    if (((FundingAgencyRepository) this.itemRepository).findByAcronym(acronym).isEmpty()) {
      FundingAgency fa = new FundingAgency();
      fa.setAcronym(acronym);
      fa.setName(name);
      if (!StringTool.isNullOrEmpty(url)) {
        try {
          fa.setUrl(new URL(url));
        } catch (MalformedURLException e) {
          this.log.warn("Error in creating funding agency: URL {} is malformed", url);
        }
      }
      if (!StringTool.isNullOrEmpty(rorId)) {
        fa.setRorId(rorId);
      }
      this.save(fa);
      this.log.info("Funding agency '{}' created", acronym);
    }
  }

  @Override
  public void validateItemSpecificRules(FundingAgency fundingAgency, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(fundingAgency.getRorId()) && !ValidationTool.isValidRorId(fundingAgency.getRorId())) {
      errors.addError(new FieldError(fundingAgency.getClass().getSimpleName(), "rorId",
              this.messageService.get("validation.rorId.invalid", new Object[] { fundingAgency.getRorId() })));
    }
  }

  @Override
  protected FundingAgency afterFind(FundingAgency fundingAgency) {
    super.afterFind(fundingAgency);
    if (!StringTool.isNullOrEmpty(fundingAgency.getRorId())) {
      this.rorService.setInfo(fundingAgency, this.rorService.loadInfo(fundingAgency.getRorId()));
    }
    return fundingAgency;
  }

  public FundingAgency getByRorId(String rorId) {
    FundingAgency item = this.fundingAgencyRepository.findByRorId(rorId)
            .orElseThrow(() -> new NoSuchElementException("No resource with ROR Id " + rorId));
    this.afterFind(item);
    return item;
  }

  public FundingAgency getByAcronym(String acronym) {
    FundingAgency item = this.fundingAgencyRepository.findByAcronym(acronym)
            .orElseThrow(() -> new NoSuchElementException("No resource with acronym " + acronym));
    this.afterFind(item);
    return item;
  }

  public FundingAgency getByName(String name) {
    FundingAgency item = this.fundingAgencyRepository.findByName(name)
            .orElseThrow(() -> new NoSuchElementException("No resource with name " + name));
    this.afterFind(item);
    return item;
  }
}

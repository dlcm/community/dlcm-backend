/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - InstitutionPersonRoleService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.service.JoinResource3TiersService;
import ch.unige.solidify.specification.Join3TiersSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.display.InstitutionPersonRoleDTO;
import ch.dlcm.model.display.InstitutionPersonRoleListDTO;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.InstitutionPersonRole;
import ch.dlcm.model.settings.Person;
import ch.dlcm.repository.InstitutionPersonRoleRepository;
import ch.dlcm.specification.InstitutionPersonRoleSpecification;
import ch.dlcm.specification.PersonSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class InstitutionPersonRoleService extends JoinResource3TiersService<Institution, Person, Role, InstitutionPersonRole> {
  public Optional<Role> findRoleByPersonAndInstitution(String personId, String institutionId) {
    return ((InstitutionPersonRoleRepository) this.joinRepository).findRoleByPersonAndInstitution(personId, institutionId);
  }

  @Override
  public Join3TiersSpecification<InstitutionPersonRole> getJoinSpecification(InstitutionPersonRole joinResource) {
    return new InstitutionPersonRoleSpecification(joinResource);
  }

  @Override
  protected SolidifySpecification<Person> getChildSpecification(Person person) {
    return new PersonSpecification(person);
  }

  @Override
  protected String getChildToRelationPropertyName() {
    return InstitutionPersonRole.PERSON_RELATION_PROPERTY_NAME;
  }

  @Override
  public Role getEmptyGrandChildResourceObject() {
    return new Role();
  }

  @Override
  public void setGrandChildResource(InstitutionPersonRole joinResource, Role role) {
    joinResource.setRole(role);
  }

  @Override
  public Role getGrandChildResource(InstitutionPersonRole joinResource) {
    return joinResource.getRole();
  }

  @Override
  public InstitutionPersonRole getEmptyJoinResourceObject() {
    return new InstitutionPersonRole();
  }

  @Override
  public Institution getEmptyParentResourceObject() {
    return new Institution();
  }

  @Override
  public Person getEmptyChildResourceObject() {
    return new Person();
  }

  @Override
  public void setParentResource(InstitutionPersonRole joinResource, Institution institution) {
    joinResource.setInstitution(institution);
  }

  @Override
  public void setChildResource(InstitutionPersonRole joinResource, Person person) {
    joinResource.setPerson(person);
  }

  @Override
  public Relation3TiersChildDTO getChildDTO(Person person, List<InstitutionPersonRole> joinResources) {
    List<InstitutionPersonRoleDTO> list = new ArrayList<>();
    for (InstitutionPersonRole institutionPersonRole : joinResources) {
      list.add(this.getGrandChildDTO(institutionPersonRole));
    }
    return new InstitutionPersonRoleListDTO(person, list);
  }

  @Override
  public InstitutionPersonRoleDTO getGrandChildDTO(InstitutionPersonRole joinResource) {
    return new InstitutionPersonRoleDTO(joinResource);
  }

  @Override
  public List<String> getDefaultGrandChildIds() {
    return Arrays.asList(Role.VISITOR.getResId());
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - DisseminationPolicyService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import static ch.dlcm.DLCMConstants.DISSEMINATION_POLICY_BASIC_ID;
import static ch.dlcm.DLCMConstants.DISSEMINATION_POLICY_HEDERA_ID;
import static ch.dlcm.DLCMConstants.DISSEMINATION_POLICY_IIIF_ID;
import static ch.dlcm.DLCMConstants.DISSEMINATION_POLICY_OAIS_ID;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.repository.DisseminationPolicyRepository;
import ch.dlcm.specification.DisseminationPolicySpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class DisseminationPolicyService extends ResourceService<DisseminationPolicy> {

  private final DisseminationPolicyRepository disseminationPolicyRepository;

  public DisseminationPolicyService(DisseminationPolicyRepository disseminationPolicyRepository) {
    this.disseminationPolicyRepository = disseminationPolicyRepository;
  }

  @Override
  public DisseminationPolicySpecification getSpecification(DisseminationPolicy resource) {
    return new DisseminationPolicySpecification(resource);
  }

  @Override
  protected void beforeSave(DisseminationPolicy item) {
    if (item.getType() == DisseminationPolicy.DisseminationPolicyType.IIIF ||
            item.getType() == DisseminationPolicy.DisseminationPolicyType.HEDERA) {
      Map<String, String> defaultParams;
      String parametersJson = item.getParameters();
      JsonMapper jsonMapper = JsonMapper.builder()
              .configure(MapperFeature.SORT_CREATOR_PROPERTIES_FIRST, true)
              .build();
      try {
        if (StringTool.isNullOrEmpty(parametersJson)) {
          defaultParams = this.getDefaultParameters(item.getType());
        } else {
          defaultParams = jsonMapper.readerFor(LinkedHashMap.class).readValue(parametersJson);
        }
        defaultParams.replace(DLCMConstants.DP_NAME_PARAM, item.getName());
        parametersJson = jsonMapper.writeValueAsString(defaultParams);
        item.setParameters(parametersJson);
      } catch (JsonProcessingException e) {
        throw new SolidifyRuntimeException("Error while parsing parameters json", e);
      }
    }
  }

  public void initRequiredData() {
    this.createDisseminationPolicy(DISSEMINATION_POLICY_BASIC_ID, DisseminationPolicy.DisseminationPolicyType.BASIC,
            "Dissemination policy for basic", "", "", DisseminationPolicy.DownloadFileName.ARCHIVE_NAME);
    this.createDisseminationPolicy(DISSEMINATION_POLICY_OAIS_ID, DisseminationPolicy.DisseminationPolicyType.OAIS,
            "Dissemination policy for oais", "", "oais", DisseminationPolicy.DownloadFileName.ARCHIVE_ID);
    this.createDisseminationPolicy(DISSEMINATION_POLICY_HEDERA_ID, DisseminationPolicy.DisseminationPolicyType.HEDERA,
            "Dissemination policy for hedera", "", "hedera", DisseminationPolicy.DownloadFileName.ARCHIVE_NAME);
    this.createDisseminationPolicy(DISSEMINATION_POLICY_IIIF_ID, DisseminationPolicy.DisseminationPolicyType.IIIF,
            "Dissemination policy for IIIF", "", "iiif", DisseminationPolicy.DownloadFileName.ARCHIVE_NAME);
  }

  public void createDisseminationPolicy(String resId, DisseminationPolicy.DisseminationPolicyType disseminationPolicyType, String name,
          String prefix, String suffix, DisseminationPolicy.DownloadFileName downloadFileName) {
    if (this.disseminationPolicyRepository.findById(resId).isEmpty()) {
      DisseminationPolicy disseminationPolicy = new DisseminationPolicy();
      disseminationPolicy.setResId(resId);
      disseminationPolicy.setName(name);
      disseminationPolicy.setType(disseminationPolicyType);
      disseminationPolicy.setPrefix(prefix);
      disseminationPolicy.setDownloadFileName(downloadFileName);
      disseminationPolicy.setSuffix(suffix);
      this.save(disseminationPolicy);
    }
  }

  private Map<String, String> getDefaultParameters(DisseminationPolicy.DisseminationPolicyType type) throws JsonProcessingException {
    Map<String, String> defaultParams = new LinkedHashMap<>();
    defaultParams.put(DLCMConstants.DP_NAME_PARAM, "");

    if (type == DisseminationPolicy.DisseminationPolicyType.IIIF) {
      defaultParams.put(DLCMConstants.DP_IIIF_SERVER_PARAM, "");
      defaultParams.put(DLCMConstants.DP_IIIF_CONTEXT_PARAM, "");
      defaultParams.put(DLCMConstants.DP_IIIF_MEDIA_FOLDER_PARAM, "media");
      defaultParams.put(DLCMConstants.DP_IIIF_MANIFESTS_FOLDER_PARAM, "manifests");
      defaultParams.put(DLCMConstants.DP_IIIF_MEDIA_PATTERNS_PARAM, "**/*.jpg,**/*.jpeg,**/*.png");
      defaultParams.put(DLCMConstants.DP_IIIF_MANIFESTS_PATTERNS_PARAM, "**/*.json");
    } else if (type == DisseminationPolicy.DisseminationPolicyType.HEDERA) {
      defaultParams.put(DLCMConstants.DP_NAME_PARAM, "");

      defaultParams.put(DLCMConstants.DP_HEDERA_PROJECT_PARAM, "");
      defaultParams.put(DLCMConstants.DP_HEDERA_SERVER_PARAM, "");

      defaultParams.put(DLCMConstants.DP_HEDERA_SOURCE_DATASET_FILE_SOURCE_FOLDER_PARAM, "source-datasets");
      defaultParams.put(DLCMConstants.DP_HEDERA_SOURCE_DATASET_FILE_TARGET_FOLDER_PARAM, "source-datasets");
      defaultParams.put(DLCMConstants.DP_HEDERA_SOURCE_DATASET_FILE_PATTERN_PARAM, "**/*.xml");

      defaultParams.put(DLCMConstants.DP_HEDERA_RDF_DATA_FILE_SOURCE_FOLDER_PARAM, "rml");
      defaultParams.put(DLCMConstants.DP_HEDERA_RDF_DATA_FILE_TARGET_FOLDER_PARAM, "rml");
      defaultParams.put(DLCMConstants.DP_HEDERA_RDF_DATA_FILE_PATTERN_PARAM, "**/*.ttl");

      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_IIIF_SOURCE_FOLDER_PARAM, "research-data-files/IIIF");
      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_IIIF_TARGET_FOLDER_PARAM, "research-data-files/IIIF");
      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_IIIF_PATTERN_PARAM, "**/*.jpg,**/*.jpeg,**/*.png");

      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_SOURCE_FOLDER_PARAM, "research-data-files/Manifests_IIIF");
      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_TARGET_FOLDER_PARAM, "research-data-files/Manifests_IIIF");
      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_PATTERN_PARAM, "**/*.json");

      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_TEI_SOURCE_FOLDER_PARAM, "research-data-files/TEI");
      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_TEI_TARGET_FOLDER_PARAM, "research-data-files/TEI");
      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_TEI_PATTERN_PARAM, "**/*.xml");

      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_WEB_SOURCE_FOLDER_PARAM, "research-data-files/WEB");
      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_WEB_TARGET_FOLDER_PARAM, "research-data-files/WEB");
      defaultParams.put(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_WEB_PATTERN_PARAM, "**/*.txt");
    }
    return defaultParams;
  }

  public void patchResource(DisseminationPolicy item, Map<String, Object> updateMap) {
    if (updateMap.containsKey(DLCMConstants.DISSEMINATION_POLICY_TYPE_FIELD)) {
      DisseminationPolicy.DisseminationPolicyType newType = updateMap.get(DLCMConstants.DISSEMINATION_POLICY_TYPE_FIELD) == null ?
              null :
              DisseminationPolicy.DisseminationPolicyType.valueOf(updateMap.get(DLCMConstants.DISSEMINATION_POLICY_TYPE_FIELD).toString());

      if (newType == null || !newType.equals(item.getType())) {
        updateMap.put(DLCMConstants.DISSEMINATION_POLICY_PARAMETERS_FIELD, null);
      }
    }
    super.patchResource(item, updateMap);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - NotificationTypeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.specification.NotificationTypeSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class NotificationTypeService extends ResourceService<NotificationType> {
  private static final Logger log = LoggerFactory.getLogger(NotificationTypeService.class);

  @Override
  public SolidifySpecification<NotificationType> getSpecification(NotificationType resource) {
    return new NotificationTypeSpecification(resource);
  }

  public void initRequiredData() {
    for (NotificationType notificationType : NotificationType.getAllNotificationTypes()) {
      if (this.itemRepository.findById(notificationType.getResId()).isEmpty()) {
        this.save(notificationType);
        this.log.info("NotificationType {} created with notified org unit role {} , notified application role {} and notified institution manager {}",
                notificationType.getResId(), notificationType.getNotifiedOrgUnitRole(), notificationType.getNotifiedApplicationRole(), notificationType.getNotifiedInstitutionManager());
      }
    }
  }
}

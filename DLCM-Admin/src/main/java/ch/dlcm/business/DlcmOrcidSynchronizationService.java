/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrcidSynchronizationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.business.OrcidSynchronizationService;
import ch.unige.solidify.model.OrcidSynchronization;
import ch.unige.solidify.model.xml.orcid.v3_0.common.CreditName;
import ch.unige.solidify.model.xml.orcid.v3_0.common.ExternalId;
import ch.unige.solidify.model.xml.orcid.v3_0.common.ExternalIds;
import ch.unige.solidify.model.xml.orcid.v3_0.common.FuzzyDate;
import ch.unige.solidify.model.xml.orcid.v3_0.common.OrcidId;
import ch.unige.solidify.model.xml.orcid.v3_0.work.Contributor;
import ch.unige.solidify.model.xml.orcid.v3_0.work.ContributorEmail;
import ch.unige.solidify.model.xml.orcid.v3_0.work.Work;
import ch.unige.solidify.model.xml.orcid.v3_0.work.WorkContributors;
import ch.unige.solidify.model.xml.orcid.v3_0.work.WorkTitle;
import ch.unige.solidify.service.OrcidClientService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.orcid.DlcmOrcidSynchronization;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePublicMetadataRemoteResourceService;

@Service
@Profile("orcid")
public class DlcmOrcidSynchronizationService extends OrcidSynchronizationService {
  private static final Logger log = LoggerFactory.getLogger(DlcmOrcidSynchronizationService.class);

  private final TrustedArchivalInfoPackageRemoteResourceService trustedAipRemoteResourceService;
  private final TrustedArchivePublicMetadataRemoteResourceService trustedArchiveMetadataRemoteResourceService;
  private final PersonService personService;
  private final UserService userService;

  public DlcmOrcidSynchronizationService(OrcidClientService orcidClientService,
          TrustedArchivalInfoPackageRemoteResourceService trustedAipRemoteResourceService,
          TrustedArchivePublicMetadataRemoteResourceService trustedArchiveMetadataRemoteResourceService, PersonService personService,
          UserService userService) {
    super(orcidClientService);
    this.trustedAipRemoteResourceService = trustedAipRemoteResourceService;
    this.trustedArchiveMetadataRemoteResourceService = trustedArchiveMetadataRemoteResourceService;
    this.personService = personService;
    this.userService = userService;
  }

  public List<OrcidSynchronization> findByAipId(String aipId) {
    return this.findByObjectId(aipId);
  }

  @Override
  public OrcidSynchronization createOrcidSynchronization(String personId, BigInteger putCode, String aipId, OffsetDateTime uploadDate) {
    Person person = this.personService.findOne(personId);

    DlcmOrcidSynchronization orcidSync = new DlcmOrcidSynchronization();
    orcidSync.setPerson(person);
    orcidSync.setAipId(aipId);
    orcidSync.setPutCode(putCode);
    orcidSync.setUploadDate(uploadDate);
    return this.save(orcidSync);
  }

  @Override
  protected Work buildWork(String aipId) {
    final ArchivalInfoPackage aip = this.trustedAipRemoteResourceService.findOne(aipId);
    final ArchiveMetadata archiveMetadata = this.trustedArchiveMetadataRemoteResourceService.getIndexMetadata(aipId);

    final Work work = new Work();

    // Title
    final WorkTitle workTitle = new WorkTitle();
    workTitle.setTitle(aip.getInfo().getName());
    work.setTitle(workTitle);

    // Publication date
    final LocalDate localDate = aip.getPublicationDate();
    final FuzzyDate fuzzyDate = new FuzzyDate();
    final FuzzyDate.Year fuzzyYear = new FuzzyDate.Year();
    final FuzzyDate.Month fuzzyMonth = new FuzzyDate.Month();
    final FuzzyDate.Day fuzzyDay = new FuzzyDate.Day();
    fuzzyYear.setValue(localDate.getYear());
    fuzzyMonth.setValue(localDate.getMonthValue());
    fuzzyDay.setValue(localDate.getDayOfMonth());
    fuzzyDate.setYear(fuzzyYear);
    fuzzyDate.setMonth(fuzzyMonth);
    fuzzyDate.setDay(fuzzyDay);
    work.setPublicationDate(fuzzyDate);

    // External id
    final ExternalIds externalIds = new ExternalIds();
    final ExternalId externalId = new ExternalId();
    externalId.setExternalIdValue(archiveMetadata.getDoi());
    externalId.setExternalIdType(ORCID_DOI_TYPE);
    externalId.setExternalIdRelationship(ORCID_SELF_RELATIONSHIP);
    externalIds.getExternalId().add(externalId);
    work.setExternalIds(externalIds);

    // Type
    work.setType(ORCID_DATASET_TYPE);

    // Contributors
    final WorkContributors workContributors = new WorkContributors();
    work.setContributors(workContributors);
    for (Map<String, String> creator : archiveMetadata.getCreators()) {
      final Contributor contributor = new Contributor();
      final String givenName = creator.get("givenName");
      final String familyName = creator.get("familyName");
      final String orcidValue = creator.get(DLCMConstants.PERSON_ORCID);
      if (!StringTool.isNullOrEmpty(orcidValue)) {
        final OrcidId orcidId = this.buildOrcidId(orcidValue);
        contributor.setContributorOrcid(orcidId);
      }
      final CreditName creditName = new CreditName();
      creditName.setValue(givenName + " " + familyName);
      contributor.setCreditName(creditName);

      String email = this.getEmail(givenName, familyName, orcidValue);
      if (email != null) {
        final ContributorEmail contributorEmail = new ContributorEmail();
        contributorEmail.setValue(email);
        contributor.setContributorEmail(contributorEmail);
      }

      workContributors.getContributor().add(contributor);
    }
    return work;
  }

  private String getEmail(String givenName, String familyName, String orcidValue) {
    if (!StringTool.isNullOrEmpty(orcidValue)) {
      Person person = this.personService.getByOrcid(orcidValue);
      return this.getEmail(person);
    } else {
      List<Person> personList = this.personService.findByFirstNameAndLastName(givenName, familyName);
      if (personList.size() == 1) {
        return this.getEmail(personList.get(0));
      } else if (personList.size() > 1) {
        log.warn("Homonym found on {} {} between personId {} {}", givenName, familyName,
                personList.get(0).getResId(), personList.get(1).getResId());
      }
      return null;
    }
  }

  private String getEmail(Person person) {
    User user = this.userService.findByPersonResId(person.getResId());
    if (user != null) {
      return user.getEmail();
    }
    return null;
  }
}

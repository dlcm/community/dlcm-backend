/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - UserService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import static ch.dlcm.DLCMRestFields.APPLICATION_ROLE_FIELD;
import static ch.dlcm.DLCMRestFields.RES_ID_FIELD;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.auth.client.service.UserCreatorService;
import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.AuthUserDto;
import ch.unige.solidify.auth.model.PersonInfo;
import ch.unige.solidify.auth.model.UserInfo;
import ch.unige.solidify.auth.service.UserInfoLocalService;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.SolidifyApplicationRole;
import ch.unige.solidify.service.AbstractUserSynchronizationService;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.service.TrustedRestClientService;
import ch.unige.solidify.service.UserInfoService;
import ch.unige.solidify.util.PropagateRestClientTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.Person;
import ch.dlcm.repository.InstitutionRepository;
import ch.dlcm.repository.PersonRepository;
import ch.dlcm.repository.UserRepository;
import ch.dlcm.specification.UserSpecification;

@Service
@ConditionalOnBean(AdminController.class)
@Primary
public class UserService extends ResourceService<User> implements UserInfoService, UserInfoLocalService, UserCreatorService {
  private static final Logger log = LoggerFactory.getLogger(UserService.class);

  private static final String USERS_ENDPOINT = "/users/";
  private static final String DELETE_TOKEN_ENDPOINT = "/delete-token";
  private static final String CHANGE_ROLE_ENDPOINT = "/change-role";
  private static final String CREATOR_EDITOR_NAME = "UserService";

  private final String authModuleUrl;
  private final AbstractUserSynchronizationService userSynchronizationService;
  private final UserRepository userRepository;
  private final PersonRepository personRepository;
  private final InstitutionRepository institutionRepository;
  private final HttpRequestInfoProvider httpRequestInfoProvider;

  private final TrustedRestClientService trustedRestClientService;

  private final PropagateRestClientTool propagateRestClientTool;

  public UserService(
          @Lazy AbstractUserSynchronizationService userSynchronizationService,
          UserRepository userRepository,
          PersonRepository personRepository,
          InstitutionRepository institutionRepository,
          HttpRequestInfoProvider httpRequestInfoProvider,
          AuthorizationClientProperties authProperties,
          TrustedRestClientService trustedRestClientService,
          PropagateRestClientTool propagateRestClientTool) {
    this.userSynchronizationService = userSynchronizationService;
    this.userRepository = userRepository;
    this.personRepository = personRepository;
    this.institutionRepository = institutionRepository;
    this.authModuleUrl = authProperties.getAuthorizationServerUrl();
    this.httpRequestInfoProvider = httpRequestInfoProvider;
    this.trustedRestClientService = trustedRestClientService;
    this.propagateRestClientTool = propagateRestClientTool;
  }

  @Override
  public User findByExternalUid(String uniqueId) {
    return ((UserRepository) this.itemRepository).findByExternalUid(uniqueId);
  }

  public User findByPersonResId(String personId) {
    return this.userRepository.findByPersonResId(personId);
  }

  /**
   * Method to get a list of user that have a role application level lower than the one specified by
   * the parameter.
   *
   * @param applicationRole minimum application role from which you want to get all the users
   * @return list of users
   */
  public List<User> findByApplicationRoleLevelHierarchy(ApplicationRole applicationRole) {
    final UserSpecification spec = this.getSpecification(new User());
    spec.setMinimumApplicationRole(applicationRole);

    return this.findAll(spec, Pageable.unpaged()).toList();
  }

  public void revokeAccessAndRefreshTokens(String externalUid) {
    this.trustedRestClientService.deleteResource(this.authModuleUrl + USERS_ENDPOINT + externalUid + DELETE_TOKEN_ENDPOINT);
  }

  @Override
  public User save(User user) {
    if (user.getApplicationRole() == null) {
      user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.USER));
    }
    return super.save(user);
  }

  @Override
  public UserSpecification getSpecification(User resource) {
    return new UserSpecification(resource);
  }

  @Override
  @Transactional
  public synchronized String createNewUser(String externalUid) {
    if (!this.userRepository.existsByExternalUid(externalUid)) {
      final AuthUserDto authUserDto = this.userSynchronizationService.getAuthenticatedUser(externalUid);
      // Create a new user
      User newUser = new User(authUserDto);
      // Create Person
      Person newPerson = new Person();
      newPerson.setFirstName(newUser.getFirstName());
      newPerson.setLastName(newUser.getLastName());
      newPerson.setOrcid(authUserDto.getOrcid());
      newPerson.setVerifiedOrcid(authUserDto.isVerifiedOrcid());
      newPerson.setCreatedBy(CREATOR_EDITOR_NAME);
      newPerson.setUpdatedBy(CREATOR_EDITOR_NAME);
      // Set institution
      Optional<Institution> institution = this.institutionRepository.findByEmailSuffix(authUserDto.getHomeOrganization());
      if (institution.isPresent()) {
        newPerson.addInstitution(institution.get());
      }
      newPerson = this.personRepository.save(newPerson);
      // Update user
      newUser.setPerson(newPerson);
      // Save user
      this.userRepository.save(newUser);
    }
    return externalUid;
  }

  public User getCurrentUser() {
    final Principal principal = this.httpRequestInfoProvider.getPrincipal();
    if (principal != null) {
      return this.findByExternalUid(principal.getName());
    }
    return null;
  }

  public void updateUserRole(String externalUid, String role) {
    RestTemplate restTemplate = this.propagateRestClientTool.getClient();
    try {
      restTemplate.postForObject(this.authModuleUrl + USERS_ENDPOINT + externalUid + CHANGE_ROLE_ENDPOINT, role, AuthUserDto.class);
    } catch (HttpClientErrorException.Forbidden e) {
      throw new AccessDeniedException("Forbidden to set role " + role + " for user " + externalUid);
    } catch (RestClientException e) {
      throw new SolidifyRuntimeException("Unable to change role " + role + " for user " + externalUid);
    }
  }

  @Override
  // Override method to avoid auto-REST call when accessing to users locally
  protected User afterFind(User user) {
    // Update creator name
    if (!StringTool.isNullOrEmpty(user.getCreatedBy())) {
      User creator = this.findByExternalUid(user.getCreatedBy());
      if (creator != null) {
        user.setCreatorName(creator.getFullName());
      }
    }
    // Update editor name
    if (!StringTool.isNullOrEmpty(user.getUpdatedBy())) {
      User editor = this.findByExternalUid(user.getUpdatedBy());
      if (editor != null) {
        user.setEditorName(editor.getFullName());
      }
    }
    return user;
  }

  public void createDocumentationUser(DLCMProperties.Documentation.User user, Person person) {
    String name = user.getName().toUpperCase();
    String mail = name + "@unige.ch";
    User userDb = ((UserRepository) this.itemRepository).findByEmail(mail);
    if (userDb == null) {
      User newUser = new User();
      newUser.setResId(user.getName().toLowerCase());
      newUser.setFirstName("[Permanent Test Data] " + name + "_firstName");
      newUser.setLastName("[Permanent Test Data] " + name + "_lastName");
      newUser.setEmail(mail);
      newUser.setHomeOrganization(name + "_homeOrganization");
      // When generating documentation a user is created and security context is set with Principal(anonymousUser)
      // for this user no need to add _externalUid to the name of the authentication otherwise we get an error that user doesn't exist in the DB.
      if (user.getName().equals("anonymousUser")) {
        newUser.setExternalUid(user.getName());
      } else {
        newUser.setExternalUid(name + "_externalUid");
      }
      newUser.setPerson(person);
      if (user.getRole().equalsIgnoreCase(AuthApplicationRole.ADMIN.getName())) {
        newUser.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ADMIN));
      } else if (user.getRole().equalsIgnoreCase(AuthApplicationRole.ROOT.getName())) {
        newUser.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ROOT));
      } else if (user.getRole().equalsIgnoreCase(AuthApplicationRole.TRUSTED_CLIENT.getName())) {
        newUser.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.TRUSTED_CLIENT));
      } else {
        newUser.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.USER));
      }
      User createdUser = this.save(newUser);
      log.info("Test user '{}' created", createdUser.getResId());
    }
  }

  public boolean containsApplicationRoleId(Map<String, Object> updateMap) {
    if (!updateMap.containsKey(APPLICATION_ROLE_FIELD) ||
            !(updateMap.get(APPLICATION_ROLE_FIELD) instanceof Map)) {
      return false;
    }
    final Map<String, Object> applicationRoleMap = (Map<String, Object>) updateMap.get(APPLICATION_ROLE_FIELD);
    return applicationRoleMap.containsKey(RES_ID_FIELD)
            && ((applicationRoleMap.get(RES_ID_FIELD)) instanceof String || applicationRoleMap.get(RES_ID_FIELD) == null);
  }

  public String getApplicationRoleId(Map<String, Object> updateMap) {
    if (!this.containsApplicationRoleId(updateMap)) {
      return null;
    }
    return (String) ((Map<String, Object>) updateMap.get(APPLICATION_ROLE_FIELD)).get(RES_ID_FIELD);
  }

  @Override
  public void saveAuthUserDto(UserInfo userInfo, AuthUserDto authUserDto) {
    User user = (User) userInfo;
    user.setAuthUserDto(authUserDto);
    this.save(user);
  }

  @Override
  public void saveUserPersonInfo(UserInfo userInfo, PersonInfo personInfo) {
    User user = (User) userInfo;
    Person person = user.getPerson();
    person.setOrcid(personInfo.getOrcid());
    person.setVerifiedOrcid(personInfo.isVerifiedOrcid());
    this.personRepository.save(person);
  }

}

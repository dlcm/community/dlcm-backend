/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - PersonService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.PersonWithOrcid;
import ch.unige.solidify.service.PersonWithOrcidService;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ValidationTool;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.repository.PersonRepository;
import ch.dlcm.specification.PersonSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PersonService extends ResourceService<Person> implements PersonWithOrcidService {

  private final PersonRepository personRepository;
  private final UserService userService;

  public PersonService(UserService userService, PersonRepository personRepository) {
    this.userService = userService;
    this.personRepository = personRepository;
  }

  public Page<Institution> findInstitutionsByPersonIdAndInstitutionName(String personId, String institutionName, boolean showMembers,
          Pageable pageable) {
    return this.personRepository.findInstitutionsByPersonIdAndInstitutionName(personId, institutionName, institutionName, showMembers, pageable);
  }

  public boolean isPersonMemberOfInstitutionWithoutRole(String personId, String institutionId) {
    return this.personRepository.isPersonMemberOfInstitutionWithoutRole(personId, institutionId);
  }

  public Page<OrganizationalUnit> findOrganizationalUnitsByPersonIdAndOrgUnitName(String personId, String orgUnitName,
          boolean roleSuperiorToVisitor, Pageable pageable) {
    return this.personRepository.findOrganizationalUnitsByPersonIdAndOrgUnitName(personId, orgUnitName, orgUnitName, roleSuperiorToVisitor,
            Role.VISITOR_ID, pageable);
  }

  public Page<OrganizationalUnit> findOpenOrganizationalUnitsByPersonIdAndOrgUnitName(String personId, String orgUnitName, String orgUnitName2,
          boolean roleSuperiorToVisitor, Pageable pageable) {
    return this.personRepository.findOpenOrganizationalUnitsByPersonIdAndOrgUnitName(personId, orgUnitName, orgUnitName2, roleSuperiorToVisitor,
            Role.VISITOR_ID, pageable);
  }

  public Page<Person> searchAllWithUser(String search, Pageable pageable) {
    return this.personRepository.searchAllWithUser(search, pageable);
  }

  public Person getByOrcid(String orcid) {
    Person item = this.personRepository.findByOrcid(orcid)
            .orElseThrow(() -> new NoSuchElementException("No resource with orcid " + orcid));
    this.afterFind(item);
    return item;
  }

  @Override
  public PersonSpecification getSpecification(Person resource) {
    return new PersonSpecification(resource);
  }

  @Override
  public PersonWithOrcid getPerson(Authentication authentication) {
    final String authUserId = authentication.getName();
    final User user = this.userService.findByExternalUid(authUserId);
    if (user != null) {
      return user.getPerson();
    } else {
      throw new SolidifyRuntimeException("User referenced by " + authUserId + " is not in database");
    }
  }

  @Override
  public void validateItemSpecificRules(Person person, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(person.getOrcid()) && !ValidationTool.isValidOrcid(person.getOrcid())) {
      errors.addError(new FieldError(person.getClass().getSimpleName(), "orcid",
              this.messageService.get("validation.orcId.invalid", new Object[] { person.getOrcid() })));
    }
  }

  @Override
  public PersonWithOrcid save(PersonWithOrcid person) {
    if (person instanceof Person personOrcId) {
      return super.save(personOrcId);
    } else {
      throw new SolidifyRuntimeException("PersonService cannot save instance of " + person.getClass());
    }
  }

  public List<Person> findByOrgUnitAndRoleLevelHierarchy(String organizationalUnitId, Role role) {
    final PersonSpecification spec = this.getSpecification(new Person());
    spec.setOrgUnitResId(organizationalUnitId);
    spec.setRole(role);
    return this.findAll(spec, Pageable.unpaged()).toList();
  }

  public List<Person> findByFirstNameAndLastName(String firstName, String lastName) {
    final Person person = new Person();
    person.setFirstName(firstName);
    person.setLastName(lastName);
    final PersonSpecification spec = this.getSpecification(person);
    return this.findAll(spec, Pageable.unpaged()).toList();
  }

  public Person createTestPerson(String name, int index) {
    final String permanentLabel = "[Permanent Test Data] "; // Is it the right place for test data
    final String firstName = permanentLabel + name + " First Name";
    final String lastName = permanentLabel + name + " Last Name";
    Person testPerson = ((PersonRepository) this.itemRepository).findByFirstName(firstName);
    if (testPerson == null) {
      testPerson = new Person();
      testPerson.setResId(name);
      testPerson.setFirstName(firstName);
      testPerson.setLastName(lastName);
      testPerson.setOrcid("0000-0000-0000-" + String.format("%04d", index));
      this.save(testPerson);
    }
    return testPerson;
  }

  @Transactional
  public boolean hasSubscribedToNotificationType(String resId, NotificationType notificationType) {
    Person person = this.findOne(resId);
    return person.getSubscribedNotifications().stream()
            .anyMatch(subscribedNotificationType -> subscribedNotificationType.getResId().equals(notificationType.getResId()));
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - PersonOrganizationalUnitService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.service.JoinResource3TiersService;
import ch.unige.solidify.specification.Join3TiersSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.display.OrganizationalUnitRoleDTO;
import ch.dlcm.model.display.OrganizationalUnitRoleListDTO;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.OrganizationalUnitPersonRole;
import ch.dlcm.model.settings.Person;
import ch.dlcm.specification.OrganizationalUnitSpecification;
import ch.dlcm.specification.PersonOrganizationalUnitRoleSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PersonOrganizationalUnitService extends JoinResource3TiersService<Person, OrganizationalUnit, Role, OrganizationalUnitPersonRole> {

  @Override
  public Join3TiersSpecification<OrganizationalUnitPersonRole> getJoinSpecification(OrganizationalUnitPersonRole joinResource) {
    return new PersonOrganizationalUnitRoleSpecification(joinResource);
  }

  @Override
  protected SolidifySpecification<OrganizationalUnit> getChildSpecification(OrganizationalUnit orgUnit) {
    return new OrganizationalUnitSpecification(orgUnit);
  }

  @Override
  protected String getChildToRelationPropertyName() {
    return OrganizationalUnitPersonRole.ORG_UNIT_RELATION_PROPERTY_NAME;
  }

  @Override
  public Role getEmptyGrandChildResourceObject() {
    return new Role();
  }

  @Override
  public void setGrandChildResource(OrganizationalUnitPersonRole joinResource, Role role) {
    joinResource.setRole(role);
  }

  @Override
  public Role getGrandChildResource(OrganizationalUnitPersonRole joinResource) {
    return joinResource.getRole();
  }

  @Override
  public OrganizationalUnitPersonRole getEmptyJoinResourceObject() {
    return new OrganizationalUnitPersonRole();
  }

  @Override
  public Person getEmptyParentResourceObject() {
    return new Person();
  }

  @Override
  public OrganizationalUnit getEmptyChildResourceObject() {
    return new OrganizationalUnit();
  }

  @Override
  public void setParentResource(OrganizationalUnitPersonRole joinResource, Person person) {
    joinResource.setPerson(person);
  }

  @Override
  public void setChildResource(OrganizationalUnitPersonRole joinResource, OrganizationalUnit orgUnit) {
    joinResource.setOrganizationalUnit(orgUnit);
  }

  @Override
  public Relation3TiersChildDTO getChildDTO(OrganizationalUnit orgUnit, List<OrganizationalUnitPersonRole> joinResources) {
    List<OrganizationalUnitRoleDTO> list = new ArrayList<>();
    for (OrganizationalUnitPersonRole orgUnitPersonRole : joinResources) {
      list.add(this.getGrandChildDTO(orgUnitPersonRole));
    }
    return new OrganizationalUnitRoleListDTO(orgUnit, list);
  }

  @Override
  public OrganizationalUnitRoleDTO getGrandChildDTO(OrganizationalUnitPersonRole joinResource) {
    return new OrganizationalUnitRoleDTO(joinResource);
  }

  @Override
  public List<String> getDefaultGrandChildIds() {
    return Arrays.asList(Role.VISITOR.getResId());
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - PreservationPolicyService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.service.ResourceService;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.repository.PreservationPolicyRepository;
import ch.dlcm.specification.PreservationPolicySpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PreservationPolicyService extends ResourceService<PreservationPolicy> {

  private static final Logger log = LoggerFactory.getLogger(PreservationPolicyService.class);

  private static final String INVALID_RETENTION = "validation.preservationPolicy.invalidRetentionPeriod";
  private static final String TOO_LONG_RETENTION = "validation.preservationPolicy.tooLongRetentionPeriod";
  private static final String INVALID_MAIN_STORAGE = "validation.preservationPolicy.invalidMainStorage";

  private final int maxPreservationPolicyRetentionPeriodInYears;
  private final String[] archivalStorageUrls;

  public PreservationPolicyService(DLCMProperties dlcmProperties) {
    this.maxPreservationPolicyRetentionPeriodInYears = dlcmProperties.getParameters().getMaxPreservationPolicyRetentionPeriodInYears();
    this.archivalStorageUrls = dlcmProperties.getModule().getArchivalStorage().getUrls();
  }

  @Override
  protected void validateItemSpecificRules(PreservationPolicy preservationPolicy, BindingResult errors) {

    // Check preservation duration period
    if (preservationPolicy.getRetention() > this.maxPreservationPolicyRetentionPeriodInYears * DLCMConstants.DAYS_BY_YEAR) {
      errors.addError(new FieldError(preservationPolicy.getClass().getSimpleName(), "retention",
              this.messageService.get(TOO_LONG_RETENTION, new Object[] { this.maxPreservationPolicyRetentionPeriodInYears })));
    }

    if (preservationPolicy.getRetention() < 0) {
      errors.addError(new FieldError(preservationPolicy.getClass().getSimpleName(), "retention", this.messageService.get(INVALID_RETENTION)));
    }

    // Check main storage
    if (preservationPolicy.getMainStorage() < 0 || preservationPolicy.getMainStorage() >= this.archivalStorageUrls.length) {
      errors.addError(new FieldError(preservationPolicy.getClass().getSimpleName(), "mainStorage",
              this.messageService.get(INVALID_MAIN_STORAGE, new Object[] { 0, this.archivalStorageUrls.length - 1 })));
    }
  }

  @Override
  public PreservationPolicySpecification getSpecification(PreservationPolicy resource) {
    return new PreservationPolicySpecification(resource);
  }

  public void initOptionalData() {
    this.createPreservationPolicy("Keep It For 5 Years", 5 * DLCMConstants.DAYS_BY_YEAR, true);
    this.createPreservationPolicy("Keep It For 10 Years", 10 * DLCMConstants.DAYS_BY_YEAR, true);
    this.createPreservationPolicy("Keep It For 15 Years", 15 * DLCMConstants.DAYS_BY_YEAR, true);
    this.createPreservationPolicy("Keep It Forever", 0, true);
  }

  public void createPreservationPolicy(String name, int retention, boolean approval) {
    if (((PreservationPolicyRepository) this.itemRepository).findByName(name) == null) {
      PreservationPolicy presPolicy = new PreservationPolicy();
      presPolicy.setName(name);
      presPolicy.setRetention(retention);
      presPolicy.setDispositionApproval(approval);
      this.save(presPolicy);
      log.info("Preservation Policy '{}' created", name);
    }
  }
}

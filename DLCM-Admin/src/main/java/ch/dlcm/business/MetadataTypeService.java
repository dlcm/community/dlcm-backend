/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - MetadataTypeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.MetadataFormat;
import ch.dlcm.model.settings.MetadataType;
import ch.dlcm.repository.MetadataTypeRepository;
import ch.dlcm.specification.MetadataTypeSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class MetadataTypeService extends ResourceService<MetadataType> {
  private static final Logger log = LoggerFactory.getLogger(MetadataTypeService.class);

  private final MetadataTypeRepository metadataTypeRepository;

  public MetadataTypeService(MetadataTypeRepository metadataTypeRepository) {
    this.metadataTypeRepository = metadataTypeRepository;
  }

  public Optional<MetadataType> findByNameAndVersion(String name, String version) {
    return this.metadataTypeRepository.findByNameAndVersion(name, version);
  }

  @Override
  public MetadataTypeSpecification getSpecification(MetadataType resource) {
    return new MetadataTypeSpecification(resource);
  }

  public void initOptionalData() {
    // Basic metadata format
    this.createMetadataType(MetadataFormat.SCHEMA_LESS, "Free", "1.0", "No check on metadata", null);
    this.createMetadataType(MetadataFormat.XML, "XML", "1.0", "Simple XML metadata", null);
    this.createMetadataType(MetadataFormat.JSON, "JSON", "1.0", "Simple JSON metadata", null);
  }

  private void createMetadataType(MetadataFormat metadataFormat, String name, String version, String description, String schema) {
    if (this.findByNameAndVersion(name, version).isEmpty()) {
      MetadataType metadataType = new MetadataType();
      metadataType.setMetadataFormat(metadataFormat);
      metadataType.setName(name);
      metadataType.setVersion(version);
      metadataType.setDescription(description);
      metadataType.setResId(metadataType.getFullName().replace(" ", "-").toLowerCase());
      if (!StringTool.isNullOrEmpty(schema)) {
        try {
          ClassPathResource schemaResource = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + schema);
          metadataType.setMetadataSchema(FileTool.toString(schemaResource.getInputStream()));
        } catch (IOException e) {
          this.log.info("Metadata Type '{}' cannot be created", metadataType.getFullName());
          return;
        }
      }
      this.save(metadataType);
      this.log.info("Metadata Type '{}' created", metadataType.getFullName());
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitRoleService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.service.JoinResource3TiersService;
import ch.unige.solidify.specification.Join3TiersSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.display.RolePersonDTO;
import ch.dlcm.model.display.RolePersonListDTO;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.OrganizationalUnitPersonRole;
import ch.dlcm.model.settings.Person;
import ch.dlcm.specification.OrganizationalUnitRolePersonSpecification;
import ch.dlcm.specification.RoleSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class OrganizationalUnitRoleService extends JoinResource3TiersService<OrganizationalUnit, Role, Person, OrganizationalUnitPersonRole> {

  @Override
  public Join3TiersSpecification<OrganizationalUnitPersonRole> getJoinSpecification(OrganizationalUnitPersonRole joinResource) {
    return new OrganizationalUnitRolePersonSpecification(joinResource);
  }

  @Override
  protected SolidifySpecification<Role> getChildSpecification(Role role) {
    return new RoleSpecification(role);
  }

  @Override
  protected String getChildToRelationPropertyName() {
    return OrganizationalUnitPersonRole.ROLE_RELATION_PROPERTY_NAME;
  }

  @Override
  public Person getEmptyGrandChildResourceObject() {
    return new Person();
  }

  @Override
  public void setGrandChildResource(OrganizationalUnitPersonRole joinResource, Person grandChildResource) {
    joinResource.setPerson(grandChildResource);
  }

  @Override
  public Person getGrandChildResource(OrganizationalUnitPersonRole joinResource) {
    return joinResource.getPerson();
  }

  @Override
  public Relation3TiersChildDTO getChildDTO(Role childItem, List<OrganizationalUnitPersonRole> joinResources) {
    List<RolePersonDTO> list = new ArrayList<>();
    for (OrganizationalUnitPersonRole orgUnitPersonRole : joinResources) {
      list.add(this.getGrandChildDTO(orgUnitPersonRole));
    }
    return new RolePersonListDTO(childItem, list);
  }

  @Override
  public RolePersonDTO getGrandChildDTO(OrganizationalUnitPersonRole joinResource) {
    return new RolePersonDTO(joinResource);
  }

  @Override
  public OrganizationalUnitPersonRole getEmptyJoinResourceObject() {
    return new OrganizationalUnitPersonRole();
  }

  @Override
  public OrganizationalUnit getEmptyParentResourceObject() {
    return new OrganizationalUnit();
  }

  @Override
  public Role getEmptyChildResourceObject() {
    return new Role();
  }

  @Override
  public void setParentResource(OrganizationalUnitPersonRole joinResource, OrganizationalUnit orgUnit) {
    joinResource.setOrganizationalUnit(orgUnit);

  }

  @Override
  public void setChildResource(OrganizationalUnitPersonRole joinResource, Role role) {
    joinResource.setRole(role);

  }
}

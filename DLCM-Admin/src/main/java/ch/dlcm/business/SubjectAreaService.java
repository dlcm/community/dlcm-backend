/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - SubjectAreaService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.ResourceService;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.SubjectArea;
import ch.dlcm.repository.SubjectAreaRepository;
import ch.dlcm.specification.SubjectAreaSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class SubjectAreaService extends ResourceService<SubjectArea> {
  private static final Logger log = LoggerFactory.getLogger(SubjectAreaService.class);

  private final SubjectAreaRepository subjectAreaRepository;

  public SubjectAreaService(SubjectAreaRepository subjectAreaRepository) {
    this.subjectAreaRepository = subjectAreaRepository;
  }

  public List<String> listSource() {
    return this.subjectAreaRepository.listSource();
  }

  public Optional<SubjectArea> findBySourceAndCode(String source, String code) {
    return this.subjectAreaRepository.findBySourceAndCode(source, code);
  }

  @Override
  public SubjectAreaSpecification getSpecification(SubjectArea resource) {
    return new SubjectAreaSpecification(resource);
  }

  public void initOptionalData() {
    // 1st level
    this.createSubjectArea("SNF", "00000", "Undefined domain");
    this.createSubjectArea("SNF", "10000", "Human and Social Sciences");
    this.createSubjectArea("SNF", "20000", "Mathematics, Natural- and Engineering Sciences");
    this.createSubjectArea("SNF", "30000", "Biology and Medicine");
  }

  private void createSubjectArea(String source, String code, String name) {
    if (this.findBySourceAndCode(source, code).isEmpty()) {
      SubjectArea rd = new SubjectArea();
      rd.setSource(source);
      rd.setCode(code);
      rd.setName(name);
      this.save(rd);
      SubjectAreaService.log.info("Subject Area '{}-{}' created", source, code);
    }
  }
}

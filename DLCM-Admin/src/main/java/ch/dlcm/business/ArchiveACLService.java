/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - ArchiveACLService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.dlcm.DLCMConstants;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.repository.ArchiveACLRepository;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.specification.ArchiveACLSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class ArchiveACLService extends ResourceService<ArchiveACL> {

  private static final String NOT_EXIST = "validation.resource.notexist";
  private static final String ALREADY_ARCHIVE_ACL_FOR_SAME_USER_AND_AIP = "validation.archiveAcl.alreadyArchiveAclForSameUserAndAip";
  private static final String USER_IN_ACL_DOES_NOT_EXIST = "validation.archiveAcl.userDoesNotExist";
  private static final String REQUIRED_SIGNED_DUA = "validation.archiveAcl.requiredSignedDua";
  private static final String NOT_REQUIRED_SIGNED_DUA = "validation.archiveAcl.notRequiredSignedDua";

  private final UserService userService;
  private final OrganizationalUnitService orgUnitService;
  private final FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService;

  public ArchiveACLService(UserService userService, OrganizationalUnitService orgUnitService,
          FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService) {
    this.userService = userService;
    this.orgUnitService = orgUnitService;
    this.archivePublicMetadataRemoteResourceService = archivePublicMetadataRemoteResourceService;
  }

  public Page<ArchiveACL> findNotDeletedByUser(User user, Pageable pageable) {
    ArchiveACL acl = new ArchiveACL();
    acl.setUser(user);
    acl.setDeleted(false);
    return this.findAll(this.getSpecification(acl), pageable);
  }

  public ArchiveACL getAclFromNotification(Notification notification) {
    final ArchiveACL archiveACL = new ArchiveACL();
    archiveACL.setAipId(notification.getObjectId());
    archiveACL.setUser(notification.getEmitter());
    archiveACL.setOrganizationalUnit(notification.getNotifiedOrgUnit());
    archiveACL.setSignedDuaFile(notification.getSignedDuaFile());
    return archiveACL;
  }

  @Override
  public void validateLinkedResources(ArchiveACL archiveACL, BindingResult errors) {
    /*
     * User
     */
    if (archiveACL.getUser() != null && !StringTool.isNullOrEmpty(archiveACL.getUser().getResId())
            && !this.userService.existsById(archiveACL.getUser().getResId())) {
      errors.addError(new FieldError(archiveACL.getClass().getSimpleName(), "user",
              this.messageService.get(NOT_EXIST, new Object[] { archiveACL.getUser().getResId() })));
    }

    /*
     * Organizational Unit
     */
    if (archiveACL.getOrganizationalUnit() != null && !StringTool.isNullOrEmpty(archiveACL.getOrganizationalUnit().getResId())
            && !this.orgUnitService.existsById(archiveACL.getOrganizationalUnit().getResId())) {
      errors.addError(new FieldError(archiveACL.getClass().getSimpleName(), "organizationalUnit",
              this.messageService.get(NOT_EXIST, new Object[] { archiveACL.getOrganizationalUnit().getResId() })));
    }
  }

  @Override
  protected void validateItemSpecificRules(ArchiveACL archiveACL, BindingResult errors) {
    if (archiveACL.getAipId() == null) {
      // aipId nullity has already been detected by validateConstraints method, do nothing more
      return;
    }
    ArchiveMetadata metadata = this.archivePublicMetadataRemoteResourceService.getIndexMetadata(archiveACL.getAipId());
    String dataUsePolicy = (String) (metadata.getMetadata(DLCMConstants.AIP_DATA_USE_POLICY));

    /*
     * Check there is a signedDua when the archive to request access is signed
     */
    if (!StringTool.isNullOrEmpty(dataUsePolicy) && DataUsePolicy.SIGNED_DUA.toString().equals(dataUsePolicy)
            && archiveACL.getSignedDuaFile() == null) {
      errors.addError(new FieldError(archiveACL.getClass().getSimpleName(), "signedDua",
              this.messageService.get(REQUIRED_SIGNED_DUA)));
    }

    /*
     * Check there is no signedDua when the archive to request access has no dua type signed
     */
    if (!StringTool.isNullOrEmpty(dataUsePolicy) && !DataUsePolicy.SIGNED_DUA.toString().equals(dataUsePolicy)
            && archiveACL.getSignedDuaFile() != null) {
      errors.addError(new FieldError(archiveACL.getClass().getSimpleName(), "signedDua",
              this.messageService.get(NOT_REQUIRED_SIGNED_DUA)));
    }

    /*
     * Check that the user really exists
     */
    if (!this.userService.existsById(archiveACL.getUser().getResId())) {
      errors.addError(new FieldError(archiveACL.getClass().getSimpleName(), "user",
              this.messageService.get(USER_IN_ACL_DOES_NOT_EXIST)));
    }

    /*
     * Check if the user has already an active ACL for this archive
     */
    Optional<ArchiveACL> archiveACLOptional = ((ArchiveACLRepository) this.itemRepository)
            .findActiveACLByUserAndAipId(archiveACL.getUser().getResId(), archiveACL.getAipId());
    if (archiveACLOptional.isPresent() && !archiveACL.getResId().equals(archiveACLOptional.get().getResId())) {
      errors.addError(new FieldError(archiveACL.getClass().getSimpleName(), "user",
              this.messageService.get(ALREADY_ARCHIVE_ACL_FOR_SAME_USER_AND_AIP)));
    }

    /*
     * Check that the AIP's org unit corresponds to the archivalACL's org unit
     */
    try {
      // Get indexed AIP's org unit
      String orgUnitId = (String) (metadata.getMetadata(DLCMConstants.AIP_ORGA_UNIT));
      if (StringTool.isNullOrEmpty(orgUnitId)) {
        throw new SolidifyProcessingException("Missing orgUnit info");
      } else if (archiveACL.getOrganizationalUnit() != null && !archiveACL.getOrganizationalUnit().getResId().equals(orgUnitId)) {
        errors.addError(new FieldError(archiveACL.getClass().getSimpleName(), "organizationalUnit",
                this.messageService.get("validation.organizationalUnit.wrong",
                        new Object[] { archiveACL.getOrganizationalUnit().getResId(), archiveACL.getResId() })));
      }
    } catch (SolidifyResourceNotFoundException e) {
      errors.addError(new FieldError(archiveACL.getClass().getSimpleName(), "aipId",
              this.messageService.get("validation.archive.notexist", new Object[] { archiveACL.getResId() })));
    } catch (Exception e) {
      throw e;
    }
  }

  @Override
  public ArchiveACL save(ArchiveACL archiveACL) {
    try {
      return super.save(archiveACL);
    } catch (DataIntegrityViolationException e) {
      Exception rootCause = (Exception) e.getRootCause();
      if (rootCause != null && rootCause.getMessage().toLowerCase().contains("duplicate")) {
        ValidationError validationError = new ValidationError();
        validationError.addError("user", this.messageService.get("validation.archiveAcl.duplicate"));
        validationError.addError("aipId", this.messageService.get("validation.archiveAcl.duplicate"));
        throw new SolidifyValidationException(validationError);
      } else {
        throw e;
      }
    }
  }

  @Override
  public void delete(String id) {
    ArchiveACL archiveACL = this.findOne(id);
    archiveACL.setDeleted(true);
    this.save(archiveACL);
  }

  @Override
  public ArchiveACLSpecification getSpecification(ArchiveACL resource) {
    return new ArchiveACLSpecification(resource);
  }
}

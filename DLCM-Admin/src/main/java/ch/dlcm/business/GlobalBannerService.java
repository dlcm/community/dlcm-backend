/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - GlobalBannerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.time.OffsetDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.model.GlobalBanner;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.GlobalBannerSpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.repository.GlobalBannerRepository;

@Service
@ConditionalOnBean(AdminController.class)
public class GlobalBannerService extends ResourceService<GlobalBanner> {
  static final Logger log = LoggerFactory.getLogger(GlobalBannerService.class);
  private static final String CONFLICTING_BANNER = "validation.globalBanner.error.conflict";
  private static final String NAME_ALREADY_EXIST = "validation.globalBanner.error.nameAlreadyExist";
  private static final String INVALID_DATE = "validation.globalBanner.error.dateStartAfterDateEnd";

  private GlobalBannerRepository globalBannerRepository;

  public GlobalBannerService(GlobalBannerRepository globalBannerRepository) {
    this.globalBannerRepository = globalBannerRepository;
  }

  public GlobalBanner findActive() {
    return this.globalBannerRepository.findActive(OffsetDateTime.now());
  }

  public GlobalBanner findActiveOverlaping(GlobalBanner globalBanner) {
    List<GlobalBanner> globalBannerMatching = this.globalBannerRepository.findActiveOverlaping(globalBanner.getStartDate(),
            globalBanner.getEndDate());
    globalBannerMatching = globalBannerMatching.stream().filter(g -> !g.getResId().equals(globalBanner.getResId())).toList();
    if (globalBannerMatching.isEmpty()) {
      return null;
    }
    return globalBannerMatching.get(0);
  }

  public GlobalBanner findByName(String name) {
    return ((GlobalBannerRepository) this.itemRepository).findByName(name);
  }

  @Override
  public GlobalBannerSpecification getSpecification(GlobalBanner resource) {
    return new GlobalBannerSpecification(resource);
  }

  @Override
  protected void validateItemSpecificRules(GlobalBanner globalBanner, BindingResult errors) {
    // check title uniqueness
    GlobalBanner existingGlobalBanner = this.findByName(globalBanner.getName());
    if (existingGlobalBanner != null && !existingGlobalBanner.getResId().equals(globalBanner.getResId())) {
      errors.addError(new FieldError(globalBanner.getClass().getSimpleName(), "name", this.messageService.get(NAME_ALREADY_EXIST)));
    }

    // check date coherence
    if (globalBanner.getStartDate() != null
            && globalBanner.getEndDate() != null
            && (globalBanner.getStartDate().isAfter(globalBanner.getEndDate()) || globalBanner.getStartDate() == globalBanner.getEndDate())) {
      errors.addError(new FieldError(globalBanner.getClass().getSimpleName(), "startDate", this.messageService.get(INVALID_DATE)));
      errors.addError(new FieldError(globalBanner.getClass().getSimpleName(), "endDate", this.messageService.get(INVALID_DATE)));
    }

    if (globalBanner.getEnabled().booleanValue()) {
      GlobalBanner matchingGlobalBanner = this.findActiveOverlaping(globalBanner);
      if (matchingGlobalBanner != null) {
        throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
                this.messageService.get(CONFLICTING_BANNER, new Object[] { matchingGlobalBanner.getName() }));
      }
    }
  }
}

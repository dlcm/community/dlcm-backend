/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - AdditionalFieldsFormService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.Objects;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.JSONTool;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.AdditionalFieldsForm;
import ch.dlcm.model.settings.FormDescriptionType;
import ch.dlcm.repository.AdditionalFieldsFormRepository;
import ch.dlcm.specification.AdditionalFieldsFormSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class AdditionalFieldsFormService extends ResourceService<AdditionalFieldsForm> {

  private static final String INVALID_FORMLY_DESCRIPTION = "validation.additionalFieldsForm.description.invalidFormly";
  private static final String DESCRIPTION_CANNOT_BE_UPDATED = "validation.additionalFieldsForm.description.cannotBeUpdated";

  public AdditionalFieldsForm findLastVersionForOrgUnit(String orgUnitId) {
    return ((AdditionalFieldsFormRepository) this.itemRepository).findFirstByOrganizationalUnitResIdOrderByCreationWhenDesc(orgUnitId);
  }

  @Override
  protected void validateItemSpecificRules(AdditionalFieldsForm item, BindingResult errors) {
    if (Objects.requireNonNull(item.getType()) == FormDescriptionType.FORMLY) {
      this.validateFormlyForm(item, errors);
    }
  }

  private void validateFormlyForm(AdditionalFieldsForm item, BindingResult errors) {

    // check description is valid JSON
    // TODO: check against a Formly schema ?
    try {
      JSONTool.wellformedArray(item.getDescription());
    } catch (SolidifyCheckingException e) {
      errors.addError(new FieldError(item.getClass().getSimpleName(), "description", this.messageService.get(INVALID_FORMLY_DESCRIPTION)));
    }

    // description can not be updated on existing forms
    if (item.isDescriptionDirty() && this.existsById(item.getResId())) {
      errors.addError(new FieldError(item.getClass().getSimpleName(), "description", this.messageService.get(DESCRIPTION_CANNOT_BE_UPDATED)));
    }
  }

  @Override
  public AdditionalFieldsFormSpecification getSpecification(AdditionalFieldsForm resource) {
    return new AdditionalFieldsFormSpecification(resource);
  }
}

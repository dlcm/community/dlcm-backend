/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitSubmissionPolicyService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.service.JoinResource2TiersService;
import ch.unige.solidify.specification.Join2TiersSpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.display.OrganizationalUnitSubmissionPolicyDTO;
import ch.dlcm.model.policies.OrganizationalUnitSubmissionPolicy;
import ch.dlcm.model.policies.OrganizationalUnitSubmissionPolicyId;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.specification.OrganizationalUnitSubmissionPolicySpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class OrganizationalUnitSubmissionPolicyService
        extends JoinResource2TiersService<OrganizationalUnit, SubmissionPolicy, OrganizationalUnitSubmissionPolicy, OrganizationalUnitSubmissionPolicyId> {

  private static final String DEFAULT_POLICY_REQUIRED = "validation.organizationalUnit.submissionPolicy.defaultRequired";

  @Override
  public Join2TiersSpecification<OrganizationalUnitSubmissionPolicy> getJoinSpecification(
          OrganizationalUnitSubmissionPolicy orgUnitSubmissionPolicy) {
    return new OrganizationalUnitSubmissionPolicySpecification(orgUnitSubmissionPolicy);
  }

  @Override
  public SubmissionPolicy getChildResource(OrganizationalUnitSubmissionPolicy orgUnitSubmissionPolicy) {
    return orgUnitSubmissionPolicy.getSubmissionPolicy();
  }

  @Override
  public String getChildPathFromJoinResource() {
    return OrganizationalUnitSubmissionPolicy.PATH_TO_SUBMISSION_POLICY;
  }

  @Override
  public JoinResourceContainer<OrganizationalUnitSubmissionPolicy> getChildDTO(
          OrganizationalUnitSubmissionPolicy orgUnitSubmissionPolicy) {
    return new OrganizationalUnitSubmissionPolicyDTO(orgUnitSubmissionPolicy);
  }

  @Override
  public OrganizationalUnitSubmissionPolicy getEmptyJoinResourceObject() {
    return new OrganizationalUnitSubmissionPolicy();
  }

  @Override
  public OrganizationalUnit getEmptyParentResourceObject() {
    return new OrganizationalUnit();
  }

  @Override
  public SubmissionPolicy getEmptyChildResourceObject() {
    return new SubmissionPolicy();
  }

  @Override
  public void setParentResource(OrganizationalUnitSubmissionPolicy orgUnitSubmissionPolicy, OrganizationalUnit orgUnit) {
    orgUnitSubmissionPolicy.setOrganizationalUnit(orgUnit);
  }

  @Override
  public void setChildResource(OrganizationalUnitSubmissionPolicy orgUnitSubmissionPolicy, SubmissionPolicy policy) {
    orgUnitSubmissionPolicy.setSubmissionPolicy(policy);
  }

  @Override
  protected void beforeValidate(OrganizationalUnitSubmissionPolicy joinResource) {
    // Set the submission policy as default it is the first policy
    this.forceDefaultIfFirstRelation(joinResource);
  }

  @Override
  protected void validateItemSpecificRules(OrganizationalUnitSubmissionPolicy joinResource, BindingResult errors) {
    if (Boolean.FALSE.equals(joinResource.getDefaultPolicy())) {
      Join2TiersSpecification<OrganizationalUnitSubmissionPolicy> spec = this.getSpecificationForDefault(joinResource);
      List<OrganizationalUnitSubmissionPolicy> existingDefaultRelations = this.findAllRelations(spec);
      if (existingDefaultRelations.isEmpty() || (existingDefaultRelations.size() == 1 && existingDefaultRelations.get(0).getSubmissionPolicy()
              .getResId().equals(joinResource.getSubmissionPolicy().getResId()))) {
        // if we save this joinResource, the org unit wouldn't have any default policy
        errors.addError(
                new FieldError(joinResource.getClass().getSimpleName(), "defaultPolicy", this.messageService.get(DEFAULT_POLICY_REQUIRED)));
      }
    }
  }

  @Override
  protected void beforeSave(OrganizationalUnitSubmissionPolicy joinResource) {
    if (Boolean.TRUE.equals(joinResource.getDefaultPolicy())) {
      // If there is already another default policy, set it to false
      String orgUnitId = joinResource.getOrganizationalUnit().getResId();
      String policyId = joinResource.getSubmissionPolicy().getResId();
      Join2TiersSpecification<OrganizationalUnitSubmissionPolicy> spec = this.getJoinSpecification(orgUnitId, new SubmissionPolicy());
      for (OrganizationalUnitSubmissionPolicy orgUnitPolicy : this.findAllRelations(spec, Pageable.unpaged())) {
        if (Boolean.TRUE.equals(orgUnitPolicy.getDefaultPolicy()) && !orgUnitPolicy.getSubmissionPolicy().getResId().equals(policyId)) {
          orgUnitPolicy.setDefaultPolicy(false);
          this.joinRepository.save(joinResource);
        }
      }
    }
  }

  private void forceDefaultIfFirstRelation(OrganizationalUnitSubmissionPolicy joinResource) {
    Join2TiersSpecification<OrganizationalUnitSubmissionPolicy> spec = this.getSpecificationForDefault(joinResource);
    long existingDefaultRelationsTotal = this.joinRepository.count(spec);
    if (existingDefaultRelationsTotal == 0) {
      // no relation by default yet -> set this one as default
      joinResource.setDefaultPolicy(true);
    }
  }

  private Join2TiersSpecification<OrganizationalUnitSubmissionPolicy> getSpecificationForDefault(
          OrganizationalUnitSubmissionPolicy joinResource) {
    String orgUnitId = joinResource.getOrganizationalUnit().getResId();
    Join2TiersSpecification<OrganizationalUnitSubmissionPolicy> spec = this.getJoinSpecification(orgUnitId, this.getEmptyChildResourceObject());
    spec.getJoinCriteria().setDefaultPolicy(true);
    return spec;
  }
}

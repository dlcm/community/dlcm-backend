/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitPreservationPolicyService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.service.JoinResource2TiersService;
import ch.unige.solidify.specification.Join2TiersSpecification;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.display.OrganizationalUnitPreservationPolicyDTO;
import ch.dlcm.model.policies.OrganizationalUnitPreservationPolicy;
import ch.dlcm.model.policies.OrganizationalUnitPreservationPolicyId;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.specification.OrganizationalUnitPreservationPolicySpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class OrganizationalUnitPreservationPolicyService
        extends JoinResource2TiersService<OrganizationalUnit, PreservationPolicy, OrganizationalUnitPreservationPolicy, OrganizationalUnitPreservationPolicyId> {

  private static final String DEFAULT_POLICY_REQUIRED = "validation.organizationalUnit.preservationPolicy.defaultRequired";

  @Override
  public Join2TiersSpecification<OrganizationalUnitPreservationPolicy> getJoinSpecification(
          OrganizationalUnitPreservationPolicy orgUnitPreservationPolicy) {
    return new OrganizationalUnitPreservationPolicySpecification(orgUnitPreservationPolicy);
  }

  @Override
  public PreservationPolicy getChildResource(OrganizationalUnitPreservationPolicy orgUnitPreservationPolicy) {
    return orgUnitPreservationPolicy.getPreservationPolicy();
  }

  @Override
  public String getChildPathFromJoinResource() {
    return OrganizationalUnitPreservationPolicy.PATH_TO_PRESERVATION_POLICY;
  }

  @Override
  public JoinResourceContainer<OrganizationalUnitPreservationPolicy> getChildDTO(
          OrganizationalUnitPreservationPolicy orgUnitPreservationPolicy) {
    return new OrganizationalUnitPreservationPolicyDTO(orgUnitPreservationPolicy);
  }

  @Override
  public OrganizationalUnitPreservationPolicy getEmptyJoinResourceObject() {
    return new OrganizationalUnitPreservationPolicy();
  }

  @Override
  public OrganizationalUnit getEmptyParentResourceObject() {
    return new OrganizationalUnit();
  }

  @Override
  public PreservationPolicy getEmptyChildResourceObject() {
    return new PreservationPolicy();
  }

  @Override
  public void setParentResource(OrganizationalUnitPreservationPolicy orgUnitPreservationPolicy, OrganizationalUnit orgUnit) {
    orgUnitPreservationPolicy.setOrganizationalUnit(orgUnit);
  }

  @Override
  public void setChildResource(OrganizationalUnitPreservationPolicy orgUnitPreservationPolicy, PreservationPolicy policy) {
    orgUnitPreservationPolicy.setPreservationPolicy(policy);
  }

  @Override
  protected void beforeValidate(OrganizationalUnitPreservationPolicy joinResource) {
    // Set the preservation policy as default it is the first policy
    this.forceDefaultIfFirstRelation(joinResource);
  }

  @Override
  protected void validateItemSpecificRules(OrganizationalUnitPreservationPolicy joinResource, BindingResult errors) {
    if (Boolean.FALSE.equals(joinResource.getDefaultPolicy())) {
      Join2TiersSpecification<OrganizationalUnitPreservationPolicy> spec = this.getSpecificationForDefault(joinResource);
      List<OrganizationalUnitPreservationPolicy> existingDefaultRelations = this.findAllRelations(spec);
      if (existingDefaultRelations.isEmpty() || (existingDefaultRelations.size() == 1 && existingDefaultRelations.get(0).getPreservationPolicy()
              .getResId().equals(joinResource.getPreservationPolicy().getResId()))) {
        // if we save this joinResource, the org unit wouldn't have any default policy
        errors.addError(
                new FieldError(joinResource.getClass().getSimpleName(), "defaultPolicy", this.messageService.get(DEFAULT_POLICY_REQUIRED)));
      }
    }
  }

  @Override
  protected void beforeSave(OrganizationalUnitPreservationPolicy joinResource) {
    if (Boolean.TRUE.equals(joinResource.getDefaultPolicy())) {
      // If there is already another default policy, set it to false
      String orgUnitId = joinResource.getOrganizationalUnit().getResId();
      String policyId = this.getChildResource(joinResource).getResId();
      Join2TiersSpecification<OrganizationalUnitPreservationPolicy> spec = this.getJoinSpecification(orgUnitId,
              this.getEmptyChildResourceObject());
      for (OrganizationalUnitPreservationPolicy orgUnitPolicy : this.findAllRelations(spec, Pageable.unpaged())) {
        if (Boolean.TRUE.equals(orgUnitPolicy.getDefaultPolicy()) && !this.getChildResource(orgUnitPolicy).getResId().equals(policyId)) {
          orgUnitPolicy.setDefaultPolicy(false);
          this.joinRepository.save(joinResource);
        }
      }
    }
  }

  private void forceDefaultIfFirstRelation(OrganizationalUnitPreservationPolicy joinResource) {
    Join2TiersSpecification<OrganizationalUnitPreservationPolicy> spec = this.getSpecificationForDefault(joinResource);
    long existingDefaultRelationsTotal = this.joinRepository.count(spec);
    if (existingDefaultRelationsTotal == 0) {
      // no relation by default yet -> set this one as default
      joinResource.setDefaultPolicy(true);
    }
  }

  private Join2TiersSpecification<OrganizationalUnitPreservationPolicy> getSpecificationForDefault(
          OrganizationalUnitPreservationPolicy joinResource) {
    String orgUnitId = joinResource.getOrganizationalUnit().getResId();
    Join2TiersSpecification<OrganizationalUnitPreservationPolicy> spec = this.getJoinSpecification(orgUnitId,
            this.getEmptyChildResourceObject());
    spec.getJoinCriteria().setDefaultPolicy(true);
    return spec;
  }
}

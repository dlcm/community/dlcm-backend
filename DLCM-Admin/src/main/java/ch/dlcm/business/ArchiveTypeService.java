/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - ArchiveTypeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.service.ResourceService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.ArchiveType;
import ch.dlcm.model.xml.dlcm.v4.mets.ResourceType;
import ch.dlcm.repository.ArchiveTypeRepository;
import ch.dlcm.specification.ArchiveTypeSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class ArchiveTypeService extends ResourceService<ArchiveType> {
  private static final Logger log = LoggerFactory.getLogger(ArchiveTypeService.class);

  private final String defaultMasterType;
  private final ArchiveTypeRepository archiveTypeRepository;

  public ArchiveTypeService(DLCMProperties dlcmProperties, ArchiveTypeRepository archiveTypeRepository) {
    this.defaultMasterType = dlcmProperties.getParameters().getDefaultMasterArchiveType();
    this.archiveTypeRepository = archiveTypeRepository;
  }

  @Override
  public ArchiveTypeSpecification getSpecification(ArchiveType resource) {
    return new ArchiveTypeSpecification(resource);
  }

  public List<String> listReferenceTypes() {
    final List<String> list = new ArrayList<>();
    for (ResourceType resourceType : ch.dlcm.model.xml.dlcm.v4.mets.ResourceType.values()) {
      list.add(resourceType.value());
    }
    return list;
  }

  public void initRequiredData() {
    // master types
    String dataset = ch.dlcm.model.xml.dlcm.v4.mets.ResourceType.valueOf(this.defaultMasterType).value();
    String collection = ch.dlcm.model.xml.dlcm.v4.mets.ResourceType.COLLECTION.value();
    this.createArchiveType(null, dataset);
    this.createArchiveType(null, collection);
    this.createArchiveType(dataset, dataset);
    this.createArchiveType(collection, dataset);
  }

  private void createArchiveType(String masterArchiveType, String archiveTypeName) {
    ArchiveType masterType = null;
    // Load Master Type
    if (masterArchiveType != null) {
      Optional<ArchiveType> mType = this.archiveTypeRepository.findMasterTypeByName(masterArchiveType);
      if (mType.isEmpty()) {
        throw new SolidifyRuntimeException("Master archive type not exists (" + masterArchiveType + ")");
      }
      masterType = mType.get();
    }
    if (this.archiveTypeRepository.findByMasterTypeAndTypeName(masterType, archiveTypeName).isEmpty()) {
      ArchiveType archiveType = new ArchiveType();
      archiveType.setMasterType(masterType);
      archiveType.setTypeName(archiveTypeName);
      this.save(archiveType);
      if (masterArchiveType == null)
        this.log.info("Archive Type '{}' created", archiveTypeName);
      else {
        this.log.info("Archive Type '{}/{}' created", masterArchiveType, archiveTypeName);

      }
    }
  }

  @Override
  public void delete(String resId) {
    if (resId.equals(ArchiveType.COLLECTION_ID) || resId.equals(this.defaultMasterType)) {
      throw new SolidifyUndeletableException(
              this.messageService.get("validation.resource.undeletable",
                      new Object[] { ArchiveType.class.getClass().getSimpleName() + ": " + resId }));
    }
    super.delete(resId);
  }

  @Override
  protected void validateItemSpecificRules(ArchiveType archiveType, BindingResult errors) {
    // Check there is only one level
    if (!archiveType.isMasterArchiveType() && !archiveType.getMasterType().isMasterArchiveType()) {
      errors.addError(new FieldError(archiveType.getClass().getSimpleName(), "masterType",
              this.messageService.get("validation.archiveType.notMasterType", new Object[] { this.listReferenceTypes() })));
    }
    // Check if master type is part of reference types
    if (archiveType.isMasterArchiveType() && !this.listReferenceTypes().contains(archiveType.getTypeName())) {
      errors.addError(new FieldError(archiveType.getClass().getSimpleName(), "typeName",
              this.messageService.get("validation.archiveType.notReferenceType", new Object[] { this.listReferenceTypes() })));

    }
  }

  @Override
  protected void beforeSave(ArchiveType archiveType) {
    super.beforeSave(archiveType);
    if (archiveType.isMasterArchiveType()) {
      archiveType.setResId(archiveType.getTypeName().toUpperCase());
    }
  }
}

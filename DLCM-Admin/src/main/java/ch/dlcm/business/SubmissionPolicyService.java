/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - SubmissionPolicyService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.service.ResourceService;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.policies.SubmissionAgreementType;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.repository.SubmissionPolicyRepository;
import ch.dlcm.specification.SubmissionPolicySpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class SubmissionPolicyService extends ResourceService<SubmissionPolicy> {
  private static final Logger log = LoggerFactory.getLogger(SubmissionPolicyService.class);

  @Override
  protected void validateItemSpecificRules(SubmissionPolicy submissionPolicy, BindingResult errors) {
    if (SubmissionAgreementType.WITHOUT.equals(submissionPolicy.getSubmissionAgreementType()) &&
        submissionPolicy.getSubmissionAgreement() != null) {
      errors.addError(new FieldError(submissionPolicy.getClass().getSimpleName(), "submissionAgreement",
              this.messageService.get("validation.submissionPolicy.withoutSubmissionAgreement")));
    }
    if (!SubmissionAgreementType.WITHOUT.equals(submissionPolicy.getSubmissionAgreementType()) &&
            submissionPolicy.getSubmissionAgreement() == null) {
      errors.addError(new FieldError(submissionPolicy.getClass().getSimpleName(), "submissionAgreement",
              this.messageService.get("validation.submissionPolicy.withSubmissionAgreement")));
    }
  }

  @Override
  public SubmissionPolicySpecification getSpecification(SubmissionPolicy resource) {
    return new SubmissionPolicySpecification(resource);
  }

  public void initOptionalData() {
    this.createSubmissionPolicy("Deposit with approval", true);
    this.createSubmissionPolicy("Deposit without approval", false);
  }

  public void createSubmissionPolicy(String name, boolean approval) {
    if (((SubmissionPolicyRepository) this.itemRepository).findByName(name) == null) {
      SubmissionPolicy subPolicy = new SubmissionPolicy();
      subPolicy.setName(name);
      subPolicy.setSubmissionApproval(approval);
      this.save(subPolicy);
      this.log.info("Submission Policy '{}' created", name);
    }
  }
}

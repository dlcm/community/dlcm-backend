/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.service.CompositeResourceService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.License;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.repository.OrganizationalUnitRepository;
import ch.dlcm.specification.OrganizationalUnitSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class OrganizationalUnitService extends CompositeResourceService<OrganizationalUnit> {
  private static final Logger log = LoggerFactory.getLogger(OrganizationalUnitService.class);

  private static final String CLOSING_DATE_SET_WHEN_NO_OPENING_DATE = "validation.dates.closingDateSetWhenNoOpeningDate";
  private static final String OPENING_DATE_NOT_BEFORE_CLOSING_DATE = "validation.dates.openingDateMustBeBeforeClosingDate";

  private final LicenseService licenseService;
  private final NotificationService notificationService;

  public OrganizationalUnitService(LicenseService licenseService, NotificationService notificationService) {
    this.licenseService = licenseService;
    this.notificationService = notificationService;
  }

  @Override
  public OrganizationalUnit save(OrganizationalUnit orgUnit) {
    License defaultLicense = orgUnit.getDefaultLicense();
    if (defaultLicense != null) {
      if (defaultLicense.getResId() != null) {
        defaultLicense = this.licenseService.findOne(defaultLicense.getResId());
        orgUnit.setDefaultLicense(defaultLicense);
      } else {
        throw new SolidifyValidationException(new ValidationError("Default license resId must not be null"));
      }
    }
    return super.save(orgUnit);
  }

  @Override
  public void delete(String id) {
    this.notificationService.deleteAllNotificationsOnOrgUnit(id);
    super.delete(id);
  }

  public OrganizationalUnit findByName(String name) {
    return ((OrganizationalUnitRepository)this.itemRepository).findByName(name);
  }

  @Override
  public void validateItemSpecificRules(OrganizationalUnit item, BindingResult errors) {

    /**
     * A closing date can not be set if the opening date is not set
     */
    if (item.getOpeningDate() == null && item.getClosingDate() != null) {

      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "closingDate", this.messageService.get(CLOSING_DATE_SET_WHEN_NO_OPENING_DATE)));
    }

    /**
     * If opening and closing dates are set, the opening date must be before the closing date
     */
    if (item.getOpeningDate() != null && item.getClosingDate() != null && item.getOpeningDate().isAfter(item.getClosingDate())) {

      errors.addError(new FieldError(item.getClass().getSimpleName(), "closingDate",
              this.messageService.get(OPENING_DATE_NOT_BEFORE_CLOSING_DATE, new Object[] { item.getOpeningDate(), item.getClosingDate() })));
    }
  }

  @Override
  public OrganizationalUnitSpecification getSpecification(OrganizationalUnit resource) {
    return new OrganizationalUnitSpecification(resource);
  }

  public void initDocumentationData() {
    this.createOrganizationalUnit("TEST", "Research Data Management Hub", "https://www.dlcm.ch/");
  }

  private void createOrganizationalUnit(String name, String description, String website) {
    if (((OrganizationalUnitRepository) this.itemRepository).findByName(name) == null) {
      OrganizationalUnit p = new OrganizationalUnit();
      p.setResId(name.toLowerCase());
      p.setName(name);
      p.setDescription(description);
      if (!StringTool.isNullOrEmpty(website)) {
        try {
          p.setUrl(new URL(website));
        } catch (MalformedURLException e) {
          this.log.warn("Error in creating organizational unit: URL {} is malformed", website);
        }
      }
      this.save(p);
      this.log.info("Organizational Unit '{}' created", name);
    }
  }
}

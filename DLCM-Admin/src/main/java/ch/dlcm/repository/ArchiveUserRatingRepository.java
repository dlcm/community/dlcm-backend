/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - ArchiveUserRatingRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.dto.AverageRating;
import ch.dlcm.model.settings.ArchiveUserRating;

@Repository
@ConditionalOnBean(AdminController.class)
public interface ArchiveUserRatingRepository extends SolidifyRepository<ArchiveUserRating> {

  @Query("SELECT NEW ch.dlcm.model.dto.AverageRating(r.resId AS ratingType , coalesce(avg(a.grade), 0) AS averageValue, count(a.grade) AS numberVotes)"
          + "FROM RatingType r LEFT JOIN ArchiveUserRating a "
          + "ON r.resId = a.ratingType.resId AND (a.archiveId = :archiveId OR a.archiveId is NULL) "
          + "GROUP BY r.resId")
  List<AverageRating> findRatingAverageByArchiveId(String archiveId);

  List<ArchiveUserRating> findByArchiveId(String archiveId);
}

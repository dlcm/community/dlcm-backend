/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - PersonRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;

@Repository
@ConditionalOnBean(AdminController.class)
public interface PersonRepository extends SolidifyRepository<Person> {

  /**
   * Needed to pass two times the same parameter since we cannot use the same query parameter multiple times with the LIKE operation in the same
   * query.
   * Check: https://github.com/spring-projects/spring-data-jpa/pull/2852 and https://github.com/spring-projects/spring-data-jpa/issues/1929
   */
  @Query("""
          SELECT ou FROM OrganizationalUnit ou
             WHERE ou IN (
                SELECT DISTINCT oupr.compositeKey.organizationalUnit FROM Person p
                JOIN p.organizationalUnitRoles oupr
                JOIN Role visitorRole ON visitorRole.resId = :visitorResId
                WHERE oupr.compositeKey.person.resId = :personId
                  AND ((:roleSuperiorToVisitor = TRUE AND oupr.compositeKey.role.level < visitorRole.level)
                        OR (:roleSuperiorToVisitor = FALSE AND oupr.compositeKey.role.level <= visitorRole.level))
                  AND (:orgUnitName IS NULL OR oupr.compositeKey.organizationalUnit.name LIKE %:orgUnitName%))
             OR ou IN (
                SELECT DISTINCT o FROM OrganizationalUnit o, Institution i
                JOIN i.organizationalUnits io
                JOIN i.personRoles ipr
                JOIN Role visitorRole ON visitorRole.resId = :visitorResId
                WHERE ipr.compositeKey.person.resId = :personId
                  AND ((:roleSuperiorToVisitor = TRUE AND ipr.compositeKey.role.level < visitorRole.level))
                        OR ((:roleSuperiorToVisitor = FALSE AND ipr.compositeKey.role.level <= visitorRole.level))
                  AND (:orgUnitNameReplica IS NULL OR io.name LIKE %:orgUnitNameReplica%))
             ORDER BY ou.name
          """)
  Page<OrganizationalUnit> findOrganizationalUnitsByPersonIdAndOrgUnitName(String personId, String orgUnitName, String orgUnitNameReplica,
          boolean roleSuperiorToVisitor, String visitorResId, Pageable pageable);

  @Query("""
          SELECT i FROM Institution i
          WHERE i IN (
             SELECT DISTINCT ins
             FROM Institution ins
             JOIN ins.personRoles ipr
             WHERE ((:institutionName IS NULL OR ipr.compositeKey.institution.name LIKE %:institutionName%) AND ipr.compositeKey.person.resId = :personId))
           OR i IN(
             SELECT DISTINCT pi
             FROM Person p
             JOIN p.institutions pi
             WHERE ((:showMembers = TRUE AND p.resId = :personId) AND (:institutionNameReplica IS NULL OR pi.name LIKE %:institutionNameReplica%)))
          """)

  Page<Institution> findInstitutionsByPersonIdAndInstitutionName(String personId, String institutionName, String institutionNameReplica,
          boolean showMembers, Pageable pageable);

  @Query("""
          SELECT CASE WHEN (COUNT(i) > 0) THEN TRUE ELSE FALSE END FROM Institution i JOIN i.people ip
           WHERE i.resId = :institutionId AND ip.resId = :personId
          """)
  boolean isPersonMemberOfInstitutionWithoutRole(String personId, String institutionId);

  @Query("""
          SELECT ou FROM OrganizationalUnit ou
          WHERE ou IN (
             SELECT DISTINCT oupr.compositeKey.organizationalUnit
             FROM Person p
             JOIN p.organizationalUnitRoles oupr
             JOIN Role visitorRole ON visitorRole.resId = :visitorResId
             WHERE oupr.compositeKey.person.resId = :personId
               AND ((:roleSuperiorToVisitor = TRUE AND oupr.compositeKey.role.level < visitorRole.level)
                     OR (:roleSuperiorToVisitor = FALSE))
               AND (:orgUnitName IS NULL OR oupr.compositeKey.organizationalUnit.name LIKE %:orgUnitName%))
           OR ou IN (
             SELECT DISTINCT o
             FROM OrganizationalUnit o, Institution i
             JOIN i.organizationalUnits io
             JOIN i.personRoles ipr
             JOIN Role visitorRole ON visitorRole.resId = :visitorResId
             WHERE ipr.compositeKey.person.resId = :personId
               AND ((:roleSuperiorToVisitor = TRUE AND ipr.compositeKey.role.level < visitorRole.level)
                     OR (:roleSuperiorToVisitor = FALSE))
               AND (:orgUnitNameReplica IS NULL OR io.name LIKE %:orgUnitNameReplica%))
           AND ou.openingDate <= CURRENT_DATE
           AND (ou.closingDate IS NULL OR ou.closingDate > CURRENT_DATE)
           ORDER BY ou.name
          """)
  Page<OrganizationalUnit> findOpenOrganizationalUnitsByPersonIdAndOrgUnitName(String personId, String orgUnitName, String orgUnitNameReplica,
          boolean roleSuperiorToVisitor,
          String visitorResId, Pageable pageable);

  @Query("""
          SELECT DISTINCT p FROM Person p
            INNER JOIN User u ON u.person.resId = p.resId
            WHERE p.firstName LIKE %:search% OR p.lastName LIKE %:search% OR p.orcid LIKE %:search%
           """)
  Page<Person> searchAllWithUser(String search, Pageable pageable);

  Person findByFirstName(String firstName);

  Optional<Person> findByOrcid(String orcid);
}

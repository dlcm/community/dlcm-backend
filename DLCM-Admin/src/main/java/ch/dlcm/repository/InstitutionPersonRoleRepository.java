/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - InstitutionPersonRoleRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.JoinRepository;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.InstitutionPersonRole;

@Repository
@ConditionalOnBean(AdminController.class)
public interface InstitutionPersonRoleRepository extends JoinRepository<InstitutionPersonRole> {

  @Query("SELECT r FROM Role r JOIN r.institutionPersons ipr WHERE ipr.compositeKey.institution.resId = :institutionId" +
          " AND ipr.compositeKey.person.resId = :personId")
  Optional<Role> findRoleByPersonAndInstitution(String personId, String institutionId);
}

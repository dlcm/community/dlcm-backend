/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - NotificationRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationCategory;
import ch.dlcm.model.notification.NotificationMark;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;

@Repository
@ConditionalOnBean(AdminController.class)
public interface NotificationRepository extends SolidifyRepository<Notification> {

  @Query("SELECT DISTINCT n"
          + " FROM Notification n"
          + " JOIN n.notificationType nt"
          + " LEFT JOIN n.emitter e"
          + " LEFT JOIN e.person ep"
          + " LEFT JOIN ep.institutions epi"
          + " LEFT JOIN epi.personRoles epir"
          + " LEFT JOIN n.notifiedOrgUnit ou"
          + " LEFT JOIN ou.personRoles oupr"
          + " LEFT JOIN oupr.compositeKey.role r"
          + " LEFT JOIN nt.notifiedOrgUnitRole nour"
          + " LEFT JOIN nt.notifiedApplicationRole nar"
          + " LEFT JOIN ou.institutions i"
          + " LEFT JOIN i.personRoles ipr"
          + " LEFT JOIN ipr.compositeKey.role ri"
          + " WHERE ((nt.notifiedApplicationRole IS NOT NULL AND :userApplicationRoleLevel <= nar.level)"
          // for roles in org unit
          + "     OR (nt.notifiedOrgUnitRole IS NOT NULL AND r.level <= nour.level AND oupr.compositeKey.person.resId = :personId)"
          // for inherited roles in institution
          + "     OR (nt.notifiedOrgUnitRole IS NOT NULL AND ri.level <= nour.level AND ipr.compositeKey.person.resId = :personId)"
          // for specific role manager in institution
          + "     OR (nt.notifiedInstitutionManager = TRUE AND epir.compositeKey.person.resId = :personId AND epir.compositeKey.role.resId = ch.dlcm.model.security.Role.MANAGER_ID)"
          // being the recipient of the notification
          + "     OR (n.recipient.resId = :personId)"
          // being admin, root or trusted client
          + "     OR :userApplicationRoleLevel <= (SELECT r.level FROM SolidifyApplicationRole r where r.resId = ch.unige.solidify.auth.model.AuthApplicationRole.ADMIN_ID))"
          + " AND (n.notificationStatus = :notificationStatus OR :notificationStatus IS NULL)"
          + " AND (n.notificationType = :notificationType OR :notificationType IS NULL)"
          + " AND (nt.notificationCategory = :notificationCategory OR :notificationCategory IS NULL)"
          + " AND (n.notificationMark = :notificationMark OR :notificationMark IS NULL)"
          + " AND (n.notifiedOrgUnit.resId = :notifiedOrgUnitId OR :notifiedOrgUnitId IS NULL)")
  Page<Notification> findInboxNotificationByPersonAndStatus(String personId, NotificationStatus notificationStatus,
          NotificationType notificationType, NotificationCategory notificationCategory, NotificationMark notificationMark,
          String notifiedOrgUnitId, int userApplicationRoleLevel,
          Pageable pageable);

  /**
   * Retrieves a notification based on the user's roles and their relationship to it.
   * A notification is considered accessible if one of the following conditions is met:
   *
   * <ul>
   *   <li><b>Roles & Permissions:</b>
   *     <ul>
   *       <li><b>ADMIN/ROOT users:</b> Have full access to all notifications.</li>
   *       <li><b>Organizational Unit Roles:</b> May grant access to notifications related to the corresponding unit.</li>
   *       <li><b>Institution-Inherited Roles:</b> Extend access beyond a specific organizational unit.</li>
   *       <li><b>Specific role Manager in the Institution:</b> Allow access to relevant notifications.</li>
   *     </ul>
   *   </li>
   *   <li><b>Direct Involvement:</b>
   *     <ul>
   *       <li>The user is the <b>emitter</b> (sender) of the notification.</li>
   *       <li>The user is the <b>recipient</b> of the notification.</li>
   *     </ul>
   *   </li>
   * </ul>
   */
  @Query("""
          SELECT DISTINCT n
          FROM Notification n
            JOIN n.notificationType nt
            LEFT JOIN n.emitter e
            LEFT JOIN e.person ep
            LEFT JOIN ep.institutions epi
            LEFT JOIN epi.personRoles epir
            LEFT JOIN n.notifiedOrgUnit ou
            LEFT JOIN ou.personRoles oupr
            LEFT JOIN oupr.compositeKey.role r
            LEFT JOIN nt.notifiedOrgUnitRole nour
            LEFT JOIN nt.notifiedApplicationRole nar
            LEFT JOIN ou.institutions i
            LEFT JOIN i.personRoles ipr
            LEFT JOIN ipr.compositeKey.role ri
          WHERE
            (
              (nt.notifiedApplicationRole IS NOT NULL AND :userApplicationRoleLevel <= nar.level)
              OR (nt.notifiedOrgUnitRole IS NOT NULL AND r.level <= nour.level AND oupr.compositeKey.person.resId = :personId)
              OR (nt.notifiedOrgUnitRole IS NOT NULL AND ri.level <= nour.level AND ipr.compositeKey.person.resId = :personId)
              OR (nt.notifiedInstitutionManager = TRUE AND epir.compositeKey.person.resId = :personId 
                    AND epir.compositeKey.role.resId = ch.dlcm.model.security.Role.MANAGER_ID)
              OR (NOT :inboxOnly = TRUE AND n.emitter.resId = :userId)
              OR (n.recipient.resId = :personId)
              OR (:userApplicationRoleLevel <= (SELECT r.level 
                                                FROM SolidifyApplicationRole r 
                                                WHERE r.resId = ch.unige.solidify.auth.model.AuthApplicationRole.ADMIN_ID))
            )
            AND n.resId = :notificationId
          """)
  Notification findInboxAndSentNotificationById(String personId, String userId, int userApplicationRoleLevel, String notificationId,
          boolean inboxOnly);

  Notification findByResId(String resID);

  @Modifying
  @Query("DELETE FROM Notification n WHERE n.notifiedOrgUnit.resId = :notifiedOrgUnitId")
  void deleteNotificationsByNotifiedOrgUnitId(String notifiedOrgUnitId);

  @Modifying
  @Query("DELETE FROM Notification n WHERE n.objectId = :objectId")
  void deleteNotificationsByObjectId(String objectId);
}

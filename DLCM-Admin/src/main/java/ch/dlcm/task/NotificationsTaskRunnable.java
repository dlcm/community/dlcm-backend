/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - NotificationsTaskRunnable.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.scheduler.AbstractTaskRunnable;

import ch.dlcm.business.NotificationService;
import ch.dlcm.business.ScheduledTaskService;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.schedule.ScheduledTask;
import ch.dlcm.service.NotificationsEmailService;
import ch.dlcm.specification.NotificationSpecification;

public abstract class NotificationsTaskRunnable extends AbstractTaskRunnable<ScheduledTask> {
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  protected ScheduledTaskService scheduledTaskService;
  protected NotificationsEmailService notificationsEmailService;
  protected final NotificationService notificationService;

  protected NotificationsTaskRunnable(ScheduledTask scheduledTask, ScheduledTaskService scheduledTaskService,
          NotificationsEmailService notificationsEmailService, NotificationService notificationService) {
    super(scheduledTask);
    this.scheduledTaskService = scheduledTaskService;
    this.notificationsEmailService = notificationsEmailService;
    this.notificationService = notificationService;
  }

  @Override
  protected void saveScheduledTask(ScheduledTask scheduledTask) {
    if (this.scheduledTaskService.existsById(scheduledTask.getResId())) {
      this.scheduledTaskService.save(scheduledTask);
    } else {
      log.warn("The scheduled task '{}' does not exist anymore", scheduledTask.getResId());
    }
  }

  /**
   * Get notifications of the given type that have been created since the last execution of the task and have not been sent yet
   *
   * @param notificationType
   * @return
   */
  protected List<Notification> getNotificationsToSend(NotificationType notificationType) {
    log.debug("Notifications to be send by mail of type: {}", notificationType);

    //Get all notifications with PENDING status, sentTime is equal to NULL and Type is equal to notificationType
    Notification search = new Notification();
    search.setNotificationType(notificationType);
    NotificationSpecification notificationSpecification = new NotificationSpecification(search);
    notificationSpecification.setOnlyWithNullSentTime(true);
    notificationSpecification.setNotificationStatusList(List.of(NotificationStatus.PENDING, NotificationStatus.NON_APPLICABLE));

    Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(ActionName.CREATION));

    return this.notificationService.findAll(notificationSpecification, pageable).toList();
  }


}

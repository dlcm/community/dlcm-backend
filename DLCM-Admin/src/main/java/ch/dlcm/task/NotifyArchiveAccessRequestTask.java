/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - NotifyOrgUnitCreationToValidateTask.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.dlcm.business.NotificationService;
import ch.dlcm.business.ScheduledTaskService;
import ch.dlcm.message.EmailMessage;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.schedule.ScheduledTask;
import ch.dlcm.service.NotificationsEmailService;

public class NotifyArchiveAccessRequestTask extends NotificationsTaskRunnable {
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public NotifyArchiveAccessRequestTask(ScheduledTask scheduledTask,
          ScheduledTaskService scheduledTaskService, NotificationsEmailService notificationsEmailService,
          NotificationService notificationService) {
    super(scheduledTask, scheduledTaskService, notificationsEmailService, notificationService);
  }

  @Override
  protected void executeTask() {
    List<Notification> notificationToSend = this.getNotificationsToSend(NotificationType.ACCESS_DATASET_REQUEST);
    log.debug("Task: Notify Archive Access Request with notification: {}", notificationToSend.size());
    if (!notificationToSend.isEmpty()) {
      this.notificationsEmailService.notifyResourceWithMultipleEmitterByRole(notificationToSend, NotificationType.ACCESS_DATASET_REQUEST,
              EmailMessage.EmailTemplate.NEW_REQUEST_TO_ACCESS_DATASET);
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - SubmissionAgreementController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.specification.Join2TiersSpecification;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.SubmissionAgreementUserService;
import ch.dlcm.business.UserService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.policies.SubmissionAgreementUser;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_SUBMISSION_AGREEMENT)
public class SubmissionAgreementController extends ResourceWithFileController<SubmissionAgreement> {

  private final UserService userService;
  private final SubmissionAgreementUserService submissionAgreementUserService;

  public SubmissionAgreementController(UserService userService, SubmissionAgreementUserService submissionAgreementUserService) {
    this.userService = userService;
    this.submissionAgreementUserService = submissionAgreementUserService;
  }

  @PostMapping("/" + DLCMActionName.UPLOAD_SUBMISSION_AGREEMENT)
  public HttpEntity<SubmissionAgreement> createItemAndUploadFile(@RequestParam("title") String title,
          @RequestParam(value = "description", required = false) String description,
          @RequestParam("version") String version,
          @RequestParam(DLCMConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    SubmissionAgreement submissionAgreement = new SubmissionAgreement();
    submissionAgreement.setTitle(title);
    submissionAgreement.setDescription(description);
    submissionAgreement.setVersion(version);
    return super.createItemAndUploadFile(submissionAgreement, file, mimeType);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.UPLOAD_FILE)
  @Override
  public HttpEntity<SubmissionAgreement> uploadFile(@PathVariable("id") String submissionAgreementId,
          @RequestParam(DLCMConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(submissionAgreementId, file, mimeType);
  }

  @UserPermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DOWNLOAD_FILE)
  @ResponseBody
  @Override
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable("id") String submissionAgreementId) {
    return super.downloadFile(submissionAgreementId);
  }

  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DELETE_FILE)
  @ResponseBody
  @Override
  public HttpEntity<SubmissionAgreement> deleteFile(@PathVariable("id") String submissionAgreementId) {
    return super.deleteFile(submissionAgreementId);
  }

  @UserPermissions
  @Override
  public HttpEntity<SubmissionAgreement> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<SubmissionAgreement>> list(@ModelAttribute SubmissionAgreement search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<SubmissionAgreement>> advancedSearch(@ModelAttribute SubmissionAgreement resource,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(resource, search, matchtype, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<SubmissionAgreement>> advancedSearch(@RequestBody SubmissionAgreement search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(search, matchtype, pageable);
  }

  @Override
  public ResponseEntity<Void>  delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void>  deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @UserPermissions
  @GetMapping("/" + DLCMActionName.GET_ALL_APPROBATIONS)
  public ResponseEntity<RestCollection<SubmissionAgreementUser>> getAllApprobation(@RequestParam(required = false) String submissionAgreementId,
          @RequestParam(required = false) String userId, Pageable pageable) {
    final SubmissionAgreementUser submissionAgreementUser = new SubmissionAgreementUser();
    if (submissionAgreementId != null) {
      final SubmissionAgreement submissionAgreement = new SubmissionAgreement();
      submissionAgreement.setResId(submissionAgreementId);
      submissionAgreementUser.setSubmissionAgreement(submissionAgreement);
    }
    if (userId != null) {
      final User user = new User();
      user.setResId(userId);
      submissionAgreementUser.setUser(user);
    }
    final Join2TiersSpecification<SubmissionAgreementUser> specification =
            this.submissionAgreementUserService.getJoinSpecification(submissionAgreementUser);
    final Page<SubmissionAgreementUser> result = this.submissionAgreementUserService.findAllRelations(specification, pageable);
    return new ResponseEntity<>(new RestCollection<>(result, pageable), HttpStatus.OK);
  }

  @UserPermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.GET_MY_APPROBATIONS)
  public ResponseEntity<RestCollection<SubmissionAgreementUser>> getMyApprobation(@PathVariable String id, Pageable pageable) {
    final String externalUid = this.getAuthenticatedUserExternalUid();
    final User user = this.userService.findByExternalUid(externalUid);
    final Join2TiersSpecification<SubmissionAgreementUser> joinSpecification = this.submissionAgreementUserService.getJoinSpecification(id, user);
    joinSpecification.setFilterOnChildResId(true);
    final Page<SubmissionAgreementUser> submissionAgreementUserPage = this.submissionAgreementUserService.findAllRelations(joinSpecification, pageable);
    return new ResponseEntity<>(new RestCollection<>(submissionAgreementUserPage, pageable), HttpStatus.OK);
  }

  @UserPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.APPROVE)
  public ResponseEntity<Object> approve(@PathVariable String id) {
    final String externalUid = this.getAuthenticatedUserExternalUid();
    final User user = this.userService.findByExternalUid(externalUid);
    final SubmissionAgreement submissionAgreement = this.itemService.findOne(id);
    final SubmissionAgreementUser submissionAgreementUser = new SubmissionAgreementUser();
    submissionAgreementUser.setUser(user);
    submissionAgreementUser.setSubmissionAgreement(submissionAgreement);
    this.submissionAgreementUserService.saveRelation(submissionAgreement, user, submissionAgreementUser);
    return ResponseEntity.ok().build();
  }

}

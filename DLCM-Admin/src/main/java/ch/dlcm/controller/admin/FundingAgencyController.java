/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - FundingAgencyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.FundingAgencyService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_FUNDING_AGENCY)
public class FundingAgencyController extends ResourceWithFileController<FundingAgency> {

  @Override
  public HttpEntity<FundingAgency> create(@RequestBody FundingAgency fundingAgency) {
    return super.create(fundingAgency);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<FundingAgency> get(@PathVariable String id) {
    return super.get(id);
  }

  @EveryonePermissions
  @GetMapping(path = SolidifyConstants.URL_ID, params = DLCMConstants.IDENTIFIER_TYPE_PARAM)
  public HttpEntity<FundingAgency> getById(@PathVariable String id,
          @RequestParam(defaultValue = ResourceIdentifierType.DEFAULT) ResourceIdentifierType identifierType) {

    FundingAgency fundingAgency = null;
    switch (identifierType) {
      case ROR_ID:
        fundingAgency = ((FundingAgencyService) this.itemService).getByRorId(id);
        this.addLinks(fundingAgency);
        return new ResponseEntity<>(fundingAgency, HttpStatus.OK);
      case ACRONYM:
        fundingAgency = ((FundingAgencyService) this.itemService).getByAcronym(id);
        this.addLinks(fundingAgency);
        return new ResponseEntity<>(fundingAgency, HttpStatus.OK);
      case NAME:
        fundingAgency = ((FundingAgencyService) this.itemService).getByName(id);
        this.addLinks(fundingAgency);
        return new ResponseEntity<>(fundingAgency, HttpStatus.OK);
      case RES_ID:
        return super.get(id);
      default:
        break;
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<RestCollection<FundingAgency>> list(@ModelAttribute FundingAgency search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<RestCollection<FundingAgency>> advancedSearch(@ModelAttribute FundingAgency resource, @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(resource, search, matchtype, pageable);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<RestCollection<FundingAgency>> advancedSearch(@RequestBody FundingAgency search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(search, matchtype, pageable);
  }

  @Override
  public HttpEntity<FundingAgency> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.UPLOAD_LOGO)
  @Override
  public HttpEntity<FundingAgency> uploadFile(@PathVariable String id, @RequestParam(DLCMConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @EveryonePermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DOWNLOAD_LOGO)
  @ResponseBody
  @Override
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DELETE_LOGO)
  @ResponseBody
  @Override
  public HttpEntity<FundingAgency> deleteFile(@PathVariable String id) {
    return super.deleteFile(id);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - RoleController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.security.Role;
import ch.dlcm.rest.UrlPath;

@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_ROLE)
public class RoleController extends ResourceController<Role> {

  @NoOnePermissions
  @Override
  public HttpEntity<Role> create(@RequestBody Role role) {
    return super.create(role);
  }

  @UserPermissions
  @Override
  public HttpEntity<Role> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<Role>> list(@ModelAttribute Role search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @NoOnePermissions
  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @NoOnePermissions
  @Override
  public ResponseEntity<Void>  deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - ArchiveTypeController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.ArchiveTypeService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.ArchiveType;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_ARCHIVE_TYPE)
public class ArchiveTypeController extends ResourceController<ArchiveType> {

  @Override
  public HttpEntity<ArchiveType> create(@RequestBody ArchiveType archiveType) {
    return super.create(archiveType);
  }

  @UserPermissions
  @Override
  public HttpEntity<ArchiveType> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<ArchiveType>> list(@ModelAttribute ArchiveType search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<ArchiveType> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_REFERENCE_TYPES)
  public HttpEntity<List<String>> listMasterTypes() {
    return new ResponseEntity<>(((ArchiveTypeService) this.itemService).listReferenceTypes(), HttpStatus.OK);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listMasterTypes()).withRel(ActionName.VALUES));
    w.add(Tool.filter(linkTo(this.getClass()).toUriComponentsBuilder(), DLCMConstants.ARCHIVE_MASTER_TYPE_ID_PARAM, "true")
            .withRel(DLCMActionName.LIST_ARCHIVE_MASTER_TYPES));
    w.add(Tool.filter(linkTo(this.getClass()).toUriComponentsBuilder(), DLCMConstants.ARCHIVE_MASTER_TYPE_ID_PARAM, ArchiveType.DATASET_ID)
            .withRel(DLCMActionName.LIST_ARCHIVE_TYPES_OF_MASTER_TYPE));
    w.add(Tool.filter(linkTo(this.getClass()).toUriComponentsBuilder(), DLCMConstants.ARCHIVE_MASTER_TYPE_ID_PARAM, ArchiveType.COLLECTION_ID)
            .withRel(DLCMActionName.LIST_ARCHIVE_TYPES_OF_MASTER_TYPE));
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - NotificationController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.NotificationService;
import ch.dlcm.business.UserService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.dto.NotificationsContributorDto;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationMark;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.HistoryService;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_NOTIFICATION)
public class NotificationController extends ResourceWithFileController<Notification> {

  private final NotificationService notificationService;
  private final HistoryService historyService;
  private final UserService userService;

  public NotificationController(NotificationService notificationService, HistoryService historyService, UserService userService) {
    this.notificationService = notificationService;
    this.historyService = historyService;
    this.userService = userService;
  }

  @PreAuthorize("@notificationPermissionService.isAllowedToCreate(#notification)")
  @Override
  public HttpEntity<Notification> create(@RequestBody Notification notification) {
    if (notification.getEmitter() == null) {
      notification.setEmitter(this.userService.getCurrentUser());
    }
    notification.setNotificationStatus(NotificationStatus.PENDING);
    return super.create(notification);
  }

  @PostMapping("/" + DLCMActionName.BY_CONTRIBUTORS)
  @TrustedUserPermissions
  public HttpEntity<List<Notification>> createForContributors(@RequestBody NotificationsContributorDto notificationsContributorsDTO) {
    List<Notification> list = ((NotificationService) this.itemService).createNotificationForContributors(notificationsContributorsDTO);
    for (Notification item : list) {
      this.addLinks(item);
    }
    return new ResponseEntity<>(list, HttpStatus.CREATED);
  }

  @Override
  public HttpEntity<Notification> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<Notification>> list(@ModelAttribute Notification search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<Notification> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @DeleteMapping(DLCMActionName.DELETE_NOTIFICATIONS_ON_DEPOSIT + "/" + SolidifyConstants.URL_ID)
  public ResponseEntity<Void> deleteNotificationsOnDeposit(@PathVariable String id) {
    ((NotificationService) this.itemService).deleteAllNotificationsOnDeposit(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @UserPermissions
  @GetMapping("/" + DLCMActionName.INBOX + SolidifyConstants.URL_ID)
  public HttpEntity<Notification> getInboxNotification(@PathVariable String id) {
    final Notification inboxNotification = ((NotificationService) this.itemService).getInboxNotification(id, this.userService.getCurrentUser());
    return new ResponseEntity<>(inboxNotification, HttpStatus.OK);
  }

  @UserPermissions
  @GetMapping("/" + DLCMActionName.SENT + SolidifyConstants.URL_ID)
  public HttpEntity<Notification> getSentNotification(@PathVariable String id) {
    final Notification sentNotification = ((NotificationService) this.itemService).getSentNotification(id, this.userService.getCurrentUser());
    return new ResponseEntity<>(sentNotification, HttpStatus.OK);
  }

  @UserPermissions
  @GetMapping("/" + DLCMActionName.INBOX)
  @SuppressWarnings("squid:S4684")
  public HttpEntity<RestCollection<Notification>> listInboxNotification(@ModelAttribute Notification search, Pageable pageable) {
    final Page<Notification> listItem = ((NotificationService) (this.itemService)).listInboxNotification(this.userService.getCurrentUser(),
            search, pageable);
    final RestCollection<Notification> collection = this.addRemainingLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @UserPermissions
  @GetMapping("/" + DLCMActionName.SENT)
  @SuppressWarnings("squid:S4684")
  public HttpEntity<RestCollection<Notification>> listSentNotification(@ModelAttribute Notification search, Pageable pageable) {
    final Page<Notification> listItem = ((NotificationService) (this.itemService)).listSentNotification(search,
            this.userService.getCurrentUser(), pageable);
    final RestCollection<Notification> collection = this.addRemainingLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @PreAuthorize("@notificationPermissionService.isMyNotification(#id)")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.SET_APPROVED)
  public HttpEntity<Notification> setProcessedStatus(@PathVariable String id) {
    return this.notificationService.changeStatus(id, NotificationStatus.APPROVED);
  }

  @PreAuthorize("@notificationPermissionService.isMyNotification(#id)")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.SET_REFUSED)
  public HttpEntity<Notification> setRefusedStatus(@PathVariable String id,
          @RequestParam(value = DLCMConstants.REASON) String reason) {
    return this.notificationService.changeStatus(id, NotificationStatus.REFUSED, reason);
  }

  @PreAuthorize("@notificationPermissionService.isMyNotification(#id)")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.SET_PENDING)
  public HttpEntity<Notification> setPendingStatus(@PathVariable String id) {
    return this.notificationService.changeStatus(id, NotificationStatus.PENDING);
  }

  @PreAuthorize("@notificationPermissionService.isMyNotification(#id)")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.SET_READ)
  public HttpEntity<Notification> setReadMark(@PathVariable String id) {
    return this.notificationService.changeMark(id, NotificationMark.READ);
  }

  @UserPermissions
  @PostMapping("/" + DLCMActionName.SET_READ_ALL)
  public HttpEntity<Void> setReadMarkList(@RequestBody String[] ids) {
    for (final String id : ids) {
      this.notificationService.changeMark(id, NotificationMark.READ);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PreAuthorize("@notificationPermissionService.isMyNotification(#id)")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.SET_UNREAD)
  public HttpEntity<Notification> setUnreadMark(@PathVariable String id) {
    return this.notificationService.changeMark(id, NotificationMark.UNREAD);
  }

  @UserPermissions
  @PostMapping("/" + DLCMActionName.SET_UNREAD_ALL)
  public HttpEntity<Void> setUnreadMarkList(@RequestBody String[] ids) {
    for (final String id : ids) {
      this.notificationService.changeMark(id, NotificationMark.UNREAD);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PreAuthorize("@notificationPermissionService.isAllowedToUploadDua(#notificationId)")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.UPLOAD_DUA)
  @Override
  public HttpEntity<Notification> uploadFile(@PathVariable("id") String notificationId, @RequestParam(DLCMConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(notificationId, file, mimeType);
  }

  @PreAuthorize("@notificationPermissionService.isAllowedToDownloadDua(#notificationId)")
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DOWNLOAD_DUA)
  @Override
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable("id") String notificationId) {
    return super.downloadFile(notificationId);
  }

  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DELETE_DUA)
  @Override
  public HttpEntity<Notification> deleteFile(@PathVariable String id) {
    return super.deleteFile(id);
  }

  private RestCollection<Notification> addRemainingLinks(Page<Notification> listItem, Pageable pageable) {
    final RestCollection<Notification> collection = new RestCollection<>(listItem, pageable);
    for (final Notification t : listItem) {
      this.addLinks(t);
    }
    collection.add(linkTo(this.getClass()).withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder())
            .withRel(ActionName.MODULE));
    this.addSortLinks(linkTo(this.getClass()), collection);
    this.addPageLinks(linkTo(this.getClass()), collection, pageable);
    this.addOthersLinks(collection);
    return collection;
  }

  @PreAuthorize("@notificationPermissionService.isMyNotification(#id)")
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.HISTORY)
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable String id, Pageable pageable) {
    if (!this.itemService.existsById(id)) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    final Page<StatusHistory> listItem = this.historyService.findByResId(id, pageable);
    final RestCollection<StatusHistory> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(methodOn(this.getClass()).history(id, pageable)).withSelfRel());
    collection
            .add(Tool.parentLink((linkTo(methodOn(this.getClass()).history(id, pageable))).toUriComponentsBuilder()).withRel(ActionName.PARENT));
    this.addPageLinks(linkTo(methodOn(this.getClass()).history(id, pageable)), collection, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

}

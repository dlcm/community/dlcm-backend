/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - ScheduledTaskController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.security.RootPermissions;

import ch.dlcm.business.ScheduledTaskService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.schedule.ScheduledTask;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@RootPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_SCHEDULED_TASKS)
public class ScheduledTaskController extends ResourceController<ScheduledTask> {

  @PostMapping("/" + DLCMActionName.SCHEDULE_ENABLED_TASKS)
  public ResponseEntity<Void> scheduleEnabledTasks() {
    ((ScheduledTaskService) this.itemService).scheduleEnabledTasks();
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PostMapping("/" + DLCMActionName.DISABLE_TASKS_SCHEDULING)
  public ResponseEntity<Void> disableTasksScheduling() {
    ((ScheduledTaskService) this.itemService).disableAllTasksScheduling();
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.KILL_TASK)
  public ResponseEntity<Void> killTask(@PathVariable String id) {
    ScheduledTask scheduledTask = this.itemService.findOne(id);
    ((ScheduledTaskService) this.itemService).killTask(scheduledTask);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - ArchiveStatisticsController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceReadOnlyController;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.validation.ValidationError;

import ch.dlcm.business.ArchiveStatisticsService;
import ch.dlcm.business.ArchiveUserRatingService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.dto.ArchiveStatisticsDto;
import ch.dlcm.model.dto.AverageRating;
import ch.dlcm.model.settings.ArchiveStatistics;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_ARCHIVE_STATISTICS)
public class ArchiveStatisticsController extends ResourceReadOnlyController<ArchiveStatistics> {

  private final ArchiveUserRatingService archiveUserRatingService;

  public ArchiveStatisticsController(ArchiveUserRatingService archiveUserRatingService) {
    this.archiveUserRatingService = archiveUserRatingService;
  }

  @Override
  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<ArchiveStatistics> get(@PathVariable String id) {
    return super.get(id);
  }

  @TrustedUserPermissions
  @PostMapping
  @SuppressWarnings("squid:S4684")
  public HttpEntity<ArchiveStatistics> create(@RequestBody ArchiveStatistics archive) {
    // Check if already exists
    if (archive.getResId() != null && this.itemService.existsById(archive.getResId())) {
      throw new SolidifyValidationException(new ValidationError(HttpStatus.BAD_REQUEST.getReasonPhrase()),
              this.messageService.get("validation.resource.alreadyexist", new Object[] { archive.getResId() }));
    }

    final ArchiveStatistics arcStats = this.itemService.save(archive);
    this.addLinks(arcStats);
    return new ResponseEntity<>(arcStats, HttpStatus.CREATED);
  }

  @TrustedUserPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.ADD_VIEW)
  public HttpEntity<Result> addView(@PathVariable String id) {
    final Result res = new Result(id);
    final ArchiveStatistics archive = ((ArchiveStatisticsService) this.itemService).addView(id);
    res.setStatus(Result.ActionStatus.EXECUTED);
    res.setMesssage(
            this.messageService.get("message.archive.view", new Object[] { archive.getResId(), archive.getStatistics().getViewNumber() }));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @TrustedUserPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.ADD_DOWNLOAD)
  public HttpEntity<Result> addDownload(@PathVariable String id) {
    final Result res = new Result(id);
    final ArchiveStatistics archive = ((ArchiveStatisticsService) this.itemService).addDownload(id);
    res.setStatus(Result.ActionStatus.EXECUTED);
    res.setMesssage(
            this.messageService.get("message.archive.download",
                    new Object[] { archive.getResId(), archive.getStatistics().getDownloadNumber() }));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.STATISTICS)
  public HttpEntity<ArchiveStatisticsDto> getArchive(@PathVariable String id) {
    final ArchiveStatistics arArchiveStatistics = this.itemService.findOne(id);
    // add the rating values from the cache
    List<AverageRating> averageRatings = this.archiveUserRatingService.findAverageRating(id);

    ArchiveStatisticsDto archiveStatisticsDto = new ArchiveStatisticsDto();
    archiveStatisticsDto.setStatistics(arArchiveStatistics.getStatistics());
    archiveStatisticsDto.setAverageRating(averageRatings);

    return new ResponseEntity<>(archiveStatisticsDto, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.AVERAGE_RATING)
  public HttpEntity<List<AverageRating>> getAverageRating(@PathVariable String id) {
    List<AverageRating> averageRating = this.archiveUserRatingService.findAverageRating(id);
    return new ResponseEntity<>(averageRating, HttpStatus.OK);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - SubmissionAgreementUserController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.MultiRelation2TiersController;
import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.TrustedUserPermissions;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.policies.SubmissionAgreementUser;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@RestController
@ConditionalOnBean(AdminController.class)
@TrustedUserPermissions
@RequestMapping(UrlPath.ADMIN_SUBMISSION_AGREEMENT + SolidifyConstants.URL_PARENT_ID + ResourceName.USER)
public class SubmissionAgreementUserController
        extends MultiRelation2TiersController<SubmissionAgreement, User, SubmissionAgreementUser> {

  @Override
  @TrustedUserPermissions
  public HttpEntity<List<JoinResourceContainer<SubmissionAgreementUser>>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @Override
  @AdminPermissions
  public HttpEntity<JoinResourceContainer<SubmissionAgreementUser>> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @AdminPermissions
  public HttpEntity<RestCollection<JoinResourceContainer<SubmissionAgreementUser>>> list(@PathVariable String parentid,
          @ModelAttribute User filterItem, Pageable pageable) {
    filterItem.setResId(null);
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<JoinResourceContainer<SubmissionAgreementUser>> update(@PathVariable String parentid, @PathVariable String id,
          @RequestBody SubmissionAgreementUser joinResource) {
    return super.update(parentid, id, joinResource);
  }

  @Override
  @TrustedUserPermissions
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  @TrustedUserPermissions
  public ResponseEntity<Void>  deleteList(@PathVariable String parentid, @RequestBody(required = false) String[] ids) {
    return super.deleteList(parentid, ids);
  }

}

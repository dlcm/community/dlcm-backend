/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - InstitutionPersonController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_INSTITUTION + SolidifyConstants.URL_PARENT_ID + ResourceName.MEMBER)
public class InstitutionPersonController extends AssociationController<Institution, Person> {

  @Override
  public HttpEntity<List<Person>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @UserPermissions
  @Override
  public HttpEntity<Person> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<Person>> list(@PathVariable String parentid, @ModelAttribute Person filterItem, Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  public HttpEntity<Person> update(@PathVariable String parentid, @PathVariable String id, Person v2) {
    return super.update(parentid, id, v2);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @Override
  protected String getParentFieldName() {
    return "institutions";
  }

  @Override
  public Person getEmptyChildResourceObject() {
    return new Person();
  }

  @Override
  protected boolean addChildOnParent(Institution institution, Person person) {
    return institution.addItem(person);
  }

  @Override
  protected boolean removeChildFromParent(Institution institution, Person person) {
    return institution.removeItem(person);
  }

}

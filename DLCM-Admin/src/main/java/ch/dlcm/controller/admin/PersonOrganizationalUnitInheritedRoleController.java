/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - PersonOrganizationalUnitController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.security.TrustedUserPermissions;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.security.Role;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.security.InstitutionPermissionService;

@RestController
@TrustedUserPermissions
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PERSON + SolidifyConstants.URL_PARENT_ID + DLCMActionName.LIST_INHERITED_ROLE)
public class PersonOrganizationalUnitInheritedRoleController {

  private final InstitutionPermissionService institutionPermissionService;
  public PersonOrganizationalUnitInheritedRoleController(InstitutionPermissionService institutionPermissionService) {
    this.institutionPermissionService = institutionPermissionService;
  }

  @PostMapping("/" + ResourceName.ORG_UNIT + SolidifyConstants.URL_ID)
  public HttpEntity<Role> getInheritedRole(@PathVariable("parentid") String personId, @PathVariable("id") String orgUnitId){
    Optional<Role> optionalRole = this.institutionPermissionService.getInheritedRole(orgUnitId, personId);
    if(optionalRole.isPresent()) {
      return new HttpEntity<>(optionalRole.get());
    } else {
      throw new SolidifyResourceNotFoundException("No inherited role");
    }
  }
}

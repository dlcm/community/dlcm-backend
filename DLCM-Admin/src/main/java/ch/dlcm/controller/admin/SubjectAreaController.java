/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - SubjectAreaController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.Label;
import ch.unige.solidify.model.Language;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.rest.Searchable;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringParserTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.business.LanguageService;
import ch.dlcm.business.SubjectAreaService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.SubjectArea;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.specification.SubjectAreaSearchSpecification;
import ch.dlcm.specification.SubjectAreaSpecification;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_SUBJECT_DOMAIN)
public class SubjectAreaController extends ResourceController<SubjectArea> {

  private static final String CODE = "Code";
  private static final String ENGLISH = "EN";
  private static final String SUBJECT_AREA = "subjectAreas-";

  private final LanguageService languageService;

  public SubjectAreaController(LanguageService languageService) {
    this.languageService = languageService;
  }

  @Override
  public HttpEntity<SubjectArea> create(@RequestBody SubjectArea subjectArea) {
    return super.create(subjectArea);
  }

  @UserPermissions
  @Override
  public HttpEntity<SubjectArea> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<SubjectArea>> list(@ModelAttribute SubjectArea search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @UserPermissions
  @SuppressWarnings("squid:S4684")
  @GetMapping(params = "languageId")
  public HttpEntity<RestCollection<SubjectArea>> list(@RequestParam String languageId, @ModelAttribute SubjectArea search,
          Pageable pageable) {
    final SubjectAreaSpecification spec = (SubjectAreaSpecification) this.itemService.getSpecification(search);
    spec.setLanguageId(languageId);
    Page<SubjectArea> listItem = this.itemService.findAll(spec, pageable);
    this.setResourceLinks(listItem);
    RestCollection<SubjectArea> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @SuppressWarnings("squid:S4684")
  @GetMapping(value = "/" + ActionName.SEARCH, params = "languageId")
  public HttpEntity<RestCollection<SubjectArea>> advancedSearch(@RequestParam String languageId, @ModelAttribute SubjectArea resource,
          @RequestParam("search") String search, @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    if (resource instanceof Searchable) {
      final List<SearchCriteria> criterias = StringParserTool.parseSearchString(search);
      final SubjectAreaSearchSpecification spec = (SubjectAreaSearchSpecification) resource.getSearchSpecification(criterias.get(0));
      spec.setLanguageId(languageId);
      this.itemService.applyCriteriaInSpecification(spec, resource, matchtype, criterias);
      Page<SubjectArea> listItem = this.itemService.findBySearchCriteria(spec, pageable);
      final WebMvcLinkBuilder linkBuilder = linkTo(methodOn(this.getClass()).advancedSearch(null, search, matchtype, pageable));
      this.setResourceLinks(listItem);
      final RestCollection<SubjectArea> collection = this.setCollectionLinksForMethod(listItem, pageable, linkBuilder);
      return new ResponseEntity<>(collection, HttpStatus.OK);
    } else {
      throw new SolidifyRuntimeException("resource is not searchable");
    }
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<SubjectArea>> advancedSearch(@ModelAttribute SubjectArea resource,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(resource, search, matchtype, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<SubjectArea>> advancedSearch(@RequestBody SubjectArea search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(search, matchtype, pageable);
  }

  @Override
  public HttpEntity<SubjectArea> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void>  delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void>  deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @PostMapping("/" + DLCMActionName.INIT)
  public HttpEntity<Result> init(@RequestParam(defaultValue = "SNF") String source) {
    final Result res = new Result();
    try {
      final long created = this.load(source);
      res.setStatus(ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.subject.area.init.success", new Object[] { created }));
    } catch (final Exception e) {
      res.setStatus(ActionStatus.NOT_EXECUTED);
      res.setMesssage(this.messageService.get("message.subject.area.init.error", new Object[] { e.getMessage() }));
    }
    res.add(linkTo(this.getClass()).slash(DLCMActionName.INIT).withSelfRel());
    res.add(linkTo(this.getClass()).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_SUBJECT_AREA_SOURCES)
  public HttpEntity<List<String>> listSources() {
    return new ResponseEntity<>(((SubjectAreaService) this.itemService).listSource(), HttpStatus.OK);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listSources()).withRel(ActionName.VALUES));
  }

  private Label createLabel(Language language, String description) {
    final Label label = new Label();
    label.setLanguage(language);
    label.setText(description);
    return label;
  }

  private SubjectArea generateResearchObject(String source, Map<String, Language> languages, JSONObject item) {
    // Find existing domain or create it
    final SubjectArea rd = ((SubjectAreaService) this.itemService).findBySourceAndCode(source, item.getString(CODE))
            .orElse(new SubjectArea());
    // Initialize when new
    if (StringTool.isNullOrEmpty(rd.getCode())) {
      rd.setSource(source);
      rd.setCode(item.getString(CODE));
      rd.setName(item.getString(ENGLISH));
    }
    if (rd.getLabels().isEmpty()) {
      // Add labels
      for (final Entry<String, Language> language : languages.entrySet()) {
        final String labelValue = this.getLabelValue(item, language.getKey());
        final Label label = this.createLabel(language.getValue(), labelValue);
        rd.getLabels().add(label);
      }
    } else {
      // Update labels
      for (final Label label : rd.getLabels()) {
        final String labelValue = this.getLabelValue(item, label.getLanguage().getResId());
        label.setText(labelValue);
      }
    }
    return rd;
  }

  private String getLabelValue(JSONObject item, String languageId) {
    try {
      return item.getString(languageId);
    } catch (final JSONException e) {
      return item.getString(ENGLISH);
    }
  }

  private long load(String source) throws IOException {
    long count = 0;
    // load data
    final JSONArray rdList = new JSONArray(FileTool.toString(new ClassPathResource(SUBJECT_AREA + source + ".json").getInputStream()));
    final Map<String, Language> languages = this.loadLanguages();
    for (final Object item : rdList) {
      if (item instanceof JSONObject) {
        final SubjectArea rd = this.generateResearchObject(source, languages, (JSONObject) item);
        this.itemService.save(rd);
        count++;
      }
    }
    return count;
  }

  private Map<String, Language> loadLanguages() {
    final Map<String, Language> list = new HashMap<>();
    for (final Language l : this.languageService.findAll(Pageable.unpaged())) {
      list.put(l.getResId(), l);
    }
    return list;
  }

}

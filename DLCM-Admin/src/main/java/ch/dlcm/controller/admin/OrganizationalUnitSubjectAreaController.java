/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitInstitutionController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.SubjectArea;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_ORG_UNIT + SolidifyConstants.URL_PARENT_ID + ResourceName.SUBJECT_AREA)
public class OrganizationalUnitSubjectAreaController extends AssociationController<OrganizationalUnit, SubjectArea> {

  @Override
  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#parentid, 'UPDATE')")
  public HttpEntity<List<SubjectArea>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @UserPermissions
  @Override
  public HttpEntity<SubjectArea> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<SubjectArea>> list(@PathVariable String parentid, @ModelAttribute SubjectArea filterItem, Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#parentid, 'UPDATE')")
  public HttpEntity<SubjectArea> update(@PathVariable String parentid, @PathVariable String id, @RequestBody SubjectArea v2) {
    return super.update(parentid, id, v2);
  }

  @Override
  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#parentid, 'UPDATE')")
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#parentid, 'UPDATE')")
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @Override
  protected String getParentFieldName() {
    return "organizationalUnits";
  }

  @Override
  public SubjectArea getEmptyChildResourceObject() {
    return new SubjectArea();
  }

  @Override
  protected boolean addChildOnParent(OrganizationalUnit orgUnit, SubjectArea subjectArea) {
    return orgUnit.addSubjectArea(subjectArea);
  }

  @Override
  protected boolean removeChildFromParent(OrganizationalUnit orgUnit, SubjectArea subjectArea) {
    return orgUnit.removeSubjectArea(subjectArea);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitsUserController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Tool;

import ch.dlcm.business.OrganizationalUnitService;
import ch.dlcm.business.PersonService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.dto.AuthorizedOrganizationalUnitDto;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;
import ch.dlcm.service.security.InstitutionPermissionService;
import ch.dlcm.specification.OrganizationalUnitSpecification;

@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_AUTHORIZED_ORG_UNIT)
public class OrganizationalUnitsUserController extends SolidifyController implements ControllerWithHateoasHome, ApplicationRoleListService {
  private static final Logger logger = LoggerFactory.getLogger(OrganizationalUnitsUserController.class);

  private final OrganizationalUnitService organizationalUnitsService;
  private final PersonService personService;
  private final TrustedPersonRemoteResourceService trustedPersonRemoteResourceService;
  private final InstitutionPermissionService institutionPermissionService;

  public OrganizationalUnitsUserController(OrganizationalUnitService organizationalUnitsService,
          PersonService personService,
          InstitutionPermissionService institutionPermissionService,
          TrustedPersonRemoteResourceService trustedPersonRemoteResourceService) {
    this.organizationalUnitsService = organizationalUnitsService;
    this.personService = personService;
    this.institutionPermissionService = institutionPermissionService;
    this.trustedPersonRemoteResourceService = trustedPersonRemoteResourceService;
  }

  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<AuthorizedOrganizationalUnitDto> getAuthorizedOrganizationalUnit(@PathVariable("id") String orgUnitId) {
    try {
      final List<AuthorizedOrganizationalUnitDto> organizationalUnitsList = this
              .getListAuthorizedOrganizationalUnits(PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE), new OrganizationalUnit(), false)
              .getContent();
      final AuthorizedOrganizationalUnitDto orgUnit = organizationalUnitsList.stream()
              .filter(orgUnitItem -> orgUnitId.equals(orgUnitItem.getResId()))
              .findFirst()
              .orElse(null);

      if (orgUnit == null) {
        throw new SolidifyResourceNotFoundException("Could not find authorized org unit with id " + orgUnitId + " for current user");
      }
      this.addLinks(orgUnit);
      return new ResponseEntity<>(orgUnit, HttpStatus.OK);
    } catch (final SolidifyRuntimeException ex) {
      logger.warn(ex.getMessage());
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  /**
   * @param openOnly if true returns only open organizational units
   * @return a list of OrganizationalUnits on which the authenticated user has permissions
   */
  @GetMapping
  @SuppressWarnings("squid:S4684")
  public HttpEntity<RestCollection<AuthorizedOrganizationalUnitDto>> listAll(
          @RequestParam(value = "openOnly", required = false, defaultValue = "true") boolean openOnly,
          @RequestParam(value = "roleSuperiorToVisitor", required = false, defaultValue = "false") boolean roleSuperiorToVisitor,
          @ModelAttribute OrganizationalUnit search,
          Pageable pageable) {
    if (openOnly) {
      final LocalDate now = LocalDate.now();
      search.setOpeningDate(now);
      search.setClosingDate(now);
    }
    final Page<AuthorizedOrganizationalUnitDto> organizationalUnitsList = this.getListAuthorizedOrganizationalUnits(pageable, search, roleSuperiorToVisitor);

    final RestCollection<AuthorizedOrganizationalUnitDto> collection = new RestCollection<>(organizationalUnitsList, pageable);
    collection.add(linkTo(this.getClass()).withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder()).withRel(ActionName.MODULE));

    this.addPageLinks(linkTo(this.getClass()), collection, pageable);

    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  protected void addLinks(OrganizationalUnit orgUnit) {
    orgUnit.removeLinks();
    orgUnit.addLinks(linkTo(this.getClass()), true, true);
  }

  private Page<AuthorizedOrganizationalUnitDto> getListAuthorizedOrganizationalUnits(Pageable pageable, OrganizationalUnit search, boolean roleSuperiorToVisitor) {
    Page<AuthorizedOrganizationalUnitDto> listItem;
    if (this.isRootOrTrustedOrAdminRole()) {
      final OrganizationalUnitSpecification orgUnitSpecification = this.organizationalUnitsService.getSpecification(search);
      final Page<OrganizationalUnit> orgUnitList = this.organizationalUnitsService.findAll(orgUnitSpecification, pageable);
      listItem = this.getAuthorizedOrgUnitWithRole(orgUnitList);
    } else {
      listItem = this.getOrganizationalUnitsWithPermissionsForAuthenticatedUser(search, roleSuperiorToVisitor, pageable);
    }
    return listItem;
  }

  /**
   * @return all organizational units on which the authenticated user has permissions
   * @throws SolidifyRuntimeException if the authenticated user is not associated to any Person
   */
  private Page<AuthorizedOrganizationalUnitDto> getOrganizationalUnitsWithPermissionsForAuthenticatedUser(OrganizationalUnit search,
          boolean roleSuperiorToVisitor, Pageable pageable) {

    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();
    Page<OrganizationalUnit> organizationalUnitPage;

    if (personId != null) {
      // Sort not supported => remove it
      if (!pageable.getSort().isUnsorted()) {
        logger.warn("Sort ({}) not supported: remove it", pageable.getSort());
        pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize());
      }
      // Filter on OrgUnit
      if (search.getOpeningDate() == null) {
        // All orgUnits
        organizationalUnitPage = this.personService.findOrganizationalUnitsByPersonIdAndOrgUnitName(personId, search.getName(),
                roleSuperiorToVisitor, pageable);

      } else {
        // All open orgUnits
        organizationalUnitPage = this.personService.findOpenOrganizationalUnitsByPersonIdAndOrgUnitName(personId, search.getName(), search.getName(),
                roleSuperiorToVisitor, pageable);
      }
    } else {
      final String externalUid = this.getAuthenticatedUserExternalUid();
      throw new SolidifyRuntimeException("User '" + externalUid + "' does not correspond to any Person");
    }
    // convert to org unit dto and add the role in each org unit
    return this.getAuthorizedOrgUnitWithRole(organizationalUnitPage);
  }

  private Page<AuthorizedOrganizationalUnitDto> getAuthorizedOrgUnitWithRole(Page<OrganizationalUnit> organizationalUnitPage) {
    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();

    // convert to org unit dto and add the role in each org unit
    Page<AuthorizedOrganizationalUnitDto> orgUnitDtoList = organizationalUnitPage.map(AuthorizedOrganizationalUnitDto::fromEntity);

    if (personId != null) {
      for (AuthorizedOrganizationalUnitDto orgUnitItem : orgUnitDtoList) {
        Optional<Role> orgUnitRole = this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(personId, orgUnitItem.getResId());
        orgUnitRole.ifPresent(orgUnitItem::setRoleFromOrganizationalUnit);
        Optional<Role> inheritedRole = this.institutionPermissionService.getInheritedRole(orgUnitItem.getResId(), personId);
        inheritedRole.ifPresent(orgUnitItem::setRoleFromInstitution);
        Optional<Role> role = Role.maxRole(orgUnitRole, inheritedRole);
        role.ifPresent(orgUnitItem::setRole);
      }
    }
    return orgUnitDtoList;
  }
}

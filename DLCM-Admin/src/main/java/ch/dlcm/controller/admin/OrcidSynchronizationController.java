/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrcidSynchronizationController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.exception.SolidifyOrcidWorkMissingInProfileException;
import ch.unige.solidify.model.OrcidSynchronization;
import ch.unige.solidify.model.OrcidWorkUpdateMode;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.business.DlcmOrcidSynchronizationService;
import ch.dlcm.business.PersonService;
import ch.dlcm.model.orcid.DlcmOrcidSynchronization;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@NoOnePermissions
@RestController
@Profile("orcid")
@RequestMapping(UrlPath.ADMIN_ORCID_SYNCHRONIZATION)
public class OrcidSynchronizationController extends ResourceController<DlcmOrcidSynchronization> {
  private final DlcmOrcidSynchronizationService dlcmOrcidSynchronizationService;
  private final PersonService personService;

  public OrcidSynchronizationController(DlcmOrcidSynchronizationService dlcmOrcidSynchronizationService, PersonService personService) {
    this.dlcmOrcidSynchronizationService = dlcmOrcidSynchronizationService;
    this.personService = personService;
  }

  @UserPermissions
  @PostMapping("/" + DLCMActionName.SYNCHRONIZE + "/{aipId}")
  public HttpEntity<Void> synchronize(@PathVariable String aipId) {
    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();
    final Person person = this.personService.findOne(personId);
    this.dlcmOrcidSynchronizationService.sendToOrcidProfile(person, aipId, OrcidWorkUpdateMode.CREATE_IF_MISSING);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @TrustedUserPermissions
  @PostMapping("/" + DLCMActionName.SYNCHRONIZE_ALL_EXISTING + "/{aipId}")
  public HttpEntity<Void> synchronizeAllExisting(@PathVariable String aipId) {
    final List<OrcidSynchronization> synchroList = this.dlcmOrcidSynchronizationService.findByAipId(aipId);
    for (OrcidSynchronization synchro : synchroList) {
      final Person person = this.personService.findOne(synchro.getPersonId());
      try {
        this.dlcmOrcidSynchronizationService.sendToOrcidProfile(person, aipId, OrcidWorkUpdateMode.UPDATE_ONLY);
      } catch (SolidifyOrcidWorkMissingInProfileException e) {
        // ORCID Work doesn't exist in profile. We do not create it automatically again.
      }
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - MetadataTypeController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.MetadataTypeService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.MetadataFormat;
import ch.dlcm.model.settings.MetadataType;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_METADATA_TYPE)
public class MetadataTypeController extends ResourceController<MetadataType> {

  private static final Logger log = LoggerFactory.getLogger(MetadataTypeController.class);

  @Override
  public HttpEntity<MetadataType> create(@RequestBody MetadataType metadataType) {
    return super.create(metadataType);
  }

  @UserPermissions
  @Override
  public HttpEntity<MetadataType> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<MetadataType>> list(@ModelAttribute MetadataType metadataType, Pageable pageable) {
    return super.list(metadataType, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<MetadataType>> advancedSearch(@ModelAttribute MetadataType metadataType,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(metadataType, search, matchtype, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<MetadataType>> advancedSearch(@RequestBody MetadataType metadataType,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(metadataType, matchtype, pageable);
  }

  @Override
  public HttpEntity<MetadataType> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @PostMapping("/" + DLCMActionName.INIT)
  public HttpEntity<Result> init() {
    final Result res = new Result();
    try {
      final int created = this.createMetadataTypes();
      res.setStatus(ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.metadata-type.init.success", new Object[] { created }));
    } catch (final IOException e) {
      res.setStatus(ActionStatus.NOT_EXECUTED);
      res.setMesssage(this.messageService.get("message.metadata-type.init.error", new Object[] { e.getMessage() }));
    }
    res.add(linkTo(this.getClass()).slash(DLCMActionName.INIT).withSelfRel());
    res.add(linkTo(this.getClass()).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_METADATA_FORMAT)
  public HttpEntity<MetadataFormat[]> listMetadataFormat() {
    return new ResponseEntity<>(MetadataFormat.values(), HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + SolidifyConstants.SCHEMA)
  public HttpEntity<StreamingResponseBody> schema(@PathVariable String id) {
    final MetadataType metadataType = this.itemService.findOne(id);
    String mimeType;
    String name;
    switch (metadataType.getMetadataFormat()) {
      case JSON:
        name = metadataType.getFullName() + SolidifyConstants.JSON_EXT;
        mimeType = "application/json";
        break;
      case XML:
        name = metadataType.getFullName() + SolidifyConstants.XML_EXT;
        mimeType = SolidifyConstants.XML_MIME_TYPE;
        break;
      case CUSTOM:
        name = metadataType.getFullName();
        mimeType = "text/plain";
        break;
      default:
        name = metadataType.getFullName();
        mimeType = "application/octet-stream";
        break;

    }
    if (metadataType.getMetadataSchema() != null) {
      final InputStream is = new ByteArrayInputStream(metadataType.getMetadataSchema().getBytes());
      return this.buildDownloadResponseEntity(is, name, mimeType, metadataType.getMetadataSchema().length());
    } else {
      throw new SolidifyHttpErrorException(HttpStatus.NO_CONTENT, "No schema associated with this metadata type");
    }
  }

  @EveryonePermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.VALIDATE)
  public HttpEntity<Boolean> validate(@PathVariable String id,
          @RequestParam(DLCMConstants.FILE) MultipartFile file) throws IOException {
    final MetadataType metadataType = this.itemService.findOne(id);
    String destinationFileName = file.getOriginalFilename() + "_" + UUID.randomUUID();
    final Path tempFile = Paths.get(System.getProperty("java.io.tmpdir")).resolve(destinationFileName);
    Files.copy(file.getInputStream(), tempFile);
    try {
      switch (metadataType.getMetadataFormat()) {
        case SCHEMA_LESS:
          if (FileTool.getSize(tempFile) == 0) {
            throw new SolidifyCheckingException("No data");
          }
          break;
        case JSON:
          if (StringTool.isNullOrEmpty(metadataType.getMetadataSchema())) {
            JSONTool.wellformed(tempFile);
          } else {
            JSONTool.validate(metadataType.getMetadataSchema(),
                    FileTool.toString(new BufferedInputStream(new FileInputStream(tempFile.toFile()), SolidifyConstants.BUFFER_SIZE)));
          }
          break;
        case XML:
          if (StringTool.isNullOrEmpty(metadataType.getMetadataSchema())) {
            XMLTool.wellformed(tempFile);
          } else {
            XMLTool.validate(metadataType.getMetadataSchema(),
                    FileTool.toString(new BufferedInputStream(new FileInputStream(tempFile.toFile()), SolidifyConstants.BUFFER_SIZE)));
          }
          break;
        case CUSTOM:
        default:
          return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
      }
    } catch (final SolidifyCheckingException e) {
      FileTool.deleteFile(tempFile);
      return new ResponseEntity<>(false, HttpStatus.OK);
    }
    FileTool.deleteFile(tempFile);
    return new ResponseEntity<>(true, HttpStatus.OK);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listMetadataFormat()).withRel(ActionName.VALUES));
  }

  private boolean createMetadataType(MetadataFormat format, String name, String version, String description, String url, String schema)
          throws IOException {
    if (((MetadataTypeService) this.itemService).findByNameAndVersion(name, version).isEmpty()) {
      final MetadataType metadataType = new MetadataType();
      metadataType.setMetadataFormat(format);
      metadataType.setName(name);
      metadataType.setVersion(version);
      metadataType.setDescription(description);
      if (!StringTool.isNullOrEmpty(url)) {
        metadataType.setUrl(new URL(url));
      }
      if (!StringTool.isNullOrEmpty(schema)) {
        metadataType.setMetadataSchema(FileTool.toString(new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/metadata/" + schema)
                .getInputStream()));
      }
      this.itemService.save(metadataType);
      log.info("Metadata Type '{}' created", metadataType.getFullName());
      return true;
    }
    return false;
  }

  private int createMetadataTypes() throws IOException {
    int count = 0;
    if (this.createMetadataType(MetadataFormat.XML, "DC", "20021212", "Dublin Core", "https://www.dublincore.org", "simpledc-20021212.xsd")) {
      count++;
    }
    if (this.createMetadataType(MetadataFormat.XML, "MARC", "21", "MARC 21", "https://www.loc.gov/marc/bibliographic/", "MARC21slim.xsd")) {
      count++;
    }
    if (this.createMetadataType(MetadataFormat.XML, "EAD", "3.0", "Encoded Archival Description", "https://www.loc.gov/ead/ead3schema.html",
            "ead-3-1.1.1.xsd")) {
      count++;
    }
    if (this.createMetadataType(MetadataFormat.XML, "EAD", "2.0", "Encoded Archival Description", "https://www.loc.gov/ead/eadschema.html",
            "ead-2002.xsd")) {
      count++;
    }
    if (this.createMetadataType(MetadataFormat.XML, "LIDO", "1.0", "Lightweight Information Describing Objects", "http://www.lido-schema.org/",
            "lido-1.0.xsd")) {
      count++;
    }
    if (this.createMetadataType(MetadataFormat.XML, "MIX", "1.0", "Metadata for Images", "http://www.loc.gov/standards/mix/", "mix-1.0.xsd")) {
      count++;
    }
    if (this.createMetadataType(MetadataFormat.XML, "MIX", "2.0", "Metadata for Images", "http://www.loc.gov/standards/mix/", "mix-2.0.xsd")) {
      count++;
    }
    if (this.createMetadataType(MetadataFormat.XML, "MODS", "3.6", "Metadata Object Description Schema", "http://www.loc.gov/standards/mods/",
            "mods-3.6.xsd")) {
      count++;
    }
    if (this.createMetadataType(MetadataFormat.XML, "MODS", "3.7", "Metadata Object Description Schema", "http://www.loc.gov/standards/mods/",
            "mods-3.7.xsd")) {
      count++;
    }
    if (this.createMetadataType(MetadataFormat.JSON, "Person", "1.0", "Person JSON Schema",
            "https://json-schema.org/learn/miscellaneous-examples.html", "person.json")) {
      count++;
    }
    if (this.createMetadataType(MetadataFormat.JSON, "Geographical Location", "1.0", "Geographical Location JSON Schema",
            "https://json-schema.org/learn/miscellaneous-examples.html", "geo-point.json")) {
      count++;
    }
    return count;
  }
}

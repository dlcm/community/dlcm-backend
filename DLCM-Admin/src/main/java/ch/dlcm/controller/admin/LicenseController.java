/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - LicenseController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.LicenseService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.settings.License;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_LICENSE)
public class LicenseController extends ResourceWithFileController<License> {
  private static final Logger log = LoggerFactory.getLogger(LicenseController.class);

  @Override
  public HttpEntity<License> create(@RequestBody License license) {
    return super.create(license);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<License> get(@PathVariable String id) {
    return super.get(id);
  }

  @EveryonePermissions
  @GetMapping(path = SolidifyConstants.URL_ID, params = DLCMConstants.IDENTIFIER_TYPE_PARAM)
  public HttpEntity<License> getById(@PathVariable String id,
          @RequestParam(defaultValue = ResourceIdentifierType.DEFAULT) ResourceIdentifierType identifierType) {

    switch (identifierType) {
      case SPDX_ID -> {
        License license = ((LicenseService) this.itemService).getByOpenLicenseId(id);
        this.addLinks(license);
        return new ResponseEntity<>(license, HttpStatus.OK);
      }
      case RES_ID -> {
        return super.get(id);
      }
      default -> {
      }
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<RestCollection<License>> list(@ModelAttribute License license, Pageable pageable) {
    return super.list(license, pageable);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<RestCollection<License>> advancedSearch(@ModelAttribute License license, @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(license, search, matchtype, pageable);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<RestCollection<License>> advancedSearch(@RequestBody License license,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(license, matchtype, pageable);
  }

  @Override
  public HttpEntity<License> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @PostMapping(UrlPath.ADMIN_LICENSE_IMPORT_LIST)
  public HttpEntity<String> createList(@RequestBody List<Map<String, Object>> payloadsList) {
    for (final Map<String, Object> payload : payloadsList) {
      final String openLicenseId = (String) payload.get(DLCMConstants.OPEN_LICENSE_ID_FIELD);
      License license;
      try {
        license = ((LicenseService) this.itemService).getByOpenLicenseId(openLicenseId);
      } catch (NoSuchElementException e) {
        license = new License();
      }
      this.itemService.patchResource(license, payload);
      this.itemService.save(license);
    }
    return new ResponseEntity<>(new JSONObject().put(ActionName.IMPORTED, payloadsList.size()).toString(), HttpStatus.CREATED);
  }

  @PostMapping(UrlPath.ADMIN_LICENSE_IMPORT_FILE)
  public HttpEntity<String> importJson(@RequestPart("file") MultipartFile file) {
    try {
      // Try import json array License format
      final List<Map<String, Object>> licensesMap = Arrays.asList(new ObjectMapper().readValue(file.getBytes(), HashMap[].class));
      return this.createList(licensesMap);
    } catch (final IOException e) {
      log.error("Error importing JSON {}", file, e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W licenseResource) {
    licenseResource.add(linkTo(methodOn(this.getClass()).importJson(null)).withRel(ActionName.IMPORT));
    licenseResource.add(linkTo(methodOn(this.getClass()).createList(null)).withRel(ActionName.IMPORT));
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.UPLOAD_LOGO)
  @Override
  public HttpEntity<License> uploadFile(@PathVariable String id, @RequestParam(DLCMConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @EveryonePermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DOWNLOAD_LOGO)
  @ResponseBody
  @Override
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DELETE_LOGO)
  @ResponseBody
  @Override
  public HttpEntity<License> deleteFile(@PathVariable String id) {
    return super.deleteFile(id);
  }
}

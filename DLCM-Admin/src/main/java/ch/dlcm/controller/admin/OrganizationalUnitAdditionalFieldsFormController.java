/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitAdditionalFieldsFormController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;

import ch.dlcm.business.AdditionalFieldsFormService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.controller.DLCMCompositionController;
import ch.dlcm.model.settings.AdditionalFieldsForm;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.HistoryService;

@RestController
@ConditionalOnBean(AdminController.class)
@PreAuthorize("@institutionPermissionService.isInstitutionalManagerOfOrgUnit(#parentid)")
@RequestMapping(UrlPath.ADMIN_ORG_UNIT + SolidifyConstants.URL_PARENT_ID + ResourceName.ADDITIONAL_FIELDS_FORMS)
public class OrganizationalUnitAdditionalFieldsFormController extends DLCMCompositionController<OrganizationalUnit, AdditionalFieldsForm> {

  public OrganizationalUnitAdditionalFieldsFormController(HistoryService historyService) {
    super(historyService);
  }

  @Override
  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#parentid, 'GET_FORM')")
  public HttpEntity<AdditionalFieldsForm> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#parentid, 'LIST_FORMS')")
  public HttpEntity<RestCollection<AdditionalFieldsForm>> list(@PathVariable String parentid, @ModelAttribute AdditionalFieldsForm filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  @PostMapping
  @SuppressWarnings("squid:S4684")
  public HttpEntity<AdditionalFieldsForm> create(@PathVariable final String parentid, final @Valid @RequestBody AdditionalFieldsForm childItem) {

    if (childItem.getResId() != null && this.subResourceService.existsById(childItem.getResId())) {
      throw new SolidifyRuntimeException(this.messageService.get("validation.resource.alreadyexist", new Object[] { childItem.getResId() }));
    }

    final OrganizationalUnit organizationalUnit = this.resourceService.findOne(parentid);

    childItem.setOrganizationalUnit(organizationalUnit);
    this.subResourceService.save(childItem);
    return new ResponseEntity<>(childItem, HttpStatus.CREATED);
  }

  @Override
  @PatchMapping(SolidifyConstants.URL_ID)
  public HttpEntity<AdditionalFieldsForm> update(@PathVariable final String parentid, @PathVariable final String id,
          @RequestBody final Map<String, Object> newForm) {

    if (this.resourceService.findOne(parentid) == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    AdditionalFieldsForm existingForm = this.subResourceService.findOne(id);
    this.subResourceService.patchResource(existingForm, newForm);
    this.subResourceService.save(existingForm);
    return new ResponseEntity<>(existingForm, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable final String parentid, @PathVariable final String id) {
    return super.delete(parentid, id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable final String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @Override
  @PreAuthorize("@downloadTokenPermissionService.isAllowed(#id, T(ch.dlcm.model.security.DownloadTokenType).ORGANIZATIONAL_UNIT_ADDITIONAL_FIELDS_FORM)")
  public HttpEntity<FileSystemResource> download(@PathVariable String parentid, @PathVariable String id) {
    return super.download(parentid, id);
  }

  @GetMapping(ResourceName.CURRENT_VERSION)
  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#parentid, 'GET_CURRENT_FORM')")
  public HttpEntity<AdditionalFieldsForm> getCurrentMetadataForm(@PathVariable final String parentid) {
    AdditionalFieldsForm currentForm = ((AdditionalFieldsFormService) this.subResourceService).findLastVersionForOrgUnit(parentid);
    if (currentForm != null) {
      return new ResponseEntity<>(currentForm, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(String parentid, W w) {
    w.add(linkTo(methodOn(this.getClass()).getCurrentMetadataForm(parentid)).withRel(ActionName.VALUES));
  }

  @Override
  protected OrganizationalUnit getParentResourceProperty(AdditionalFieldsForm subResource) {
    return subResource.getOrganizationalUnit();
  }

  @Override
  protected void setParentResourceProperty(AdditionalFieldsForm subResource, OrganizationalUnit parentResource) {
    subResource.setOrganizationalUnit(parentResource);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - DisseminationPolicyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_DISSEMINATION_POLICY)
public class DisseminationPolicyController extends ResourceController<DisseminationPolicy> {

  @Override
  @RootPermissions
  public HttpEntity<DisseminationPolicy> create(@RequestBody DisseminationPolicy disseminationPolicy) {
    return super.create(disseminationPolicy);
  }

  @UserPermissions
  @Override
  public HttpEntity<DisseminationPolicy> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<DisseminationPolicy>> list(@ModelAttribute DisseminationPolicy disseminationPolicy, Pageable pageable) {
    return super.list(disseminationPolicy, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<DisseminationPolicy>> advancedSearch(@ModelAttribute DisseminationPolicy disseminationPolicy,
          @RequestParam("search") String search, @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(disseminationPolicy, search, matchtype, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<DisseminationPolicy>> advancedSearch(@RequestBody DisseminationPolicy disseminationPolicy,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(disseminationPolicy, matchtype, pageable);
  }

  @Override
  public HttpEntity<DisseminationPolicy> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - PersonController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Map;
import java.util.Objects;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.PersonService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PERSON)
public class PersonController extends ResourceWithFileController<Person> {

  @Override
  public HttpEntity<Person> create(@RequestBody Person person) {
    return super.create(person);
  }

  @UserPermissions
  @Override
  public HttpEntity<Person> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @GetMapping(path = SolidifyConstants.URL_ID, params = DLCMConstants.IDENTIFIER_TYPE_PARAM)
  public HttpEntity<Person> getById(@PathVariable String id,
          @RequestParam(defaultValue = ResourceIdentifierType.DEFAULT) ResourceIdentifierType identifierType) {

    if (Objects.requireNonNull(identifierType) == ResourceIdentifierType.ORCID) {
      Person person = ((PersonService) this.itemService).getByOrcid(id);
      this.addLinks(person);
      return new ResponseEntity<>(person, HttpStatus.OK);
    } else if (identifierType == ResourceIdentifierType.RES_ID) {
      return super.get(id);
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

  @Override
  public HttpEntity<RestCollection<Person>> list(@ModelAttribute Person search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<RestCollection<Person>> advancedSearch(@ModelAttribute Person person,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(person, search, matchtype, pageable);
  }

  @Override
  public HttpEntity<RestCollection<Person>> advancedSearch(@RequestBody Person search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(search, matchtype, pageable);
  }

  @PreAuthorize("hasAnyAuthority('"
          + AuthApplicationRole.ADMIN_ID + "', '"
          + AuthApplicationRole.TRUSTED_CLIENT_ID + "', '"
          + AuthApplicationRole.ROOT_ID
          + "') or @personPermissionService.isUserConnectedLinkedToPerson(#id)")
  @Override
  public HttpEntity<Person> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    this.setNotVerifiedOrcidIfEdited(id, updateMap);
    return super.update(id, updateMap);
  }

  private void setNotVerifiedOrcidIfEdited(String id, Map<String, Object> updateMap) {
    if (!updateMap.containsKey(DLCMConstants.PERSON_ORCID)) {
      return;
    }

    String newOrcid = updateMap.get(DLCMConstants.PERSON_ORCID) == null ? null : updateMap.get(DLCMConstants.PERSON_ORCID).toString();
    String oldOrcid = this.itemService.findOne(id).getOrcid();

    if ((StringTool.isNullOrEmpty(newOrcid) && !StringTool.isNullOrEmpty(oldOrcid))
            || (!StringTool.isNullOrEmpty(oldOrcid) && !oldOrcid.equals(newOrcid))) {
      updateMap.put("verifiedOrcid", false);
    }
  }

  @AdminPermissions
  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @AdminPermissions
  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @GetMapping("/" + DLCMActionName.SEARCH_WITH_USER)
  public HttpEntity<RestCollection<Person>> searchPeopleWithUser(@RequestParam(name = "search", defaultValue = "%") String search,
          Pageable pageable) {
    final Page<Person> listItem = ((PersonService) this.itemService).searchAllWithUser(search, pageable);
    final WebMvcLinkBuilder linkBuilder = linkTo(methodOn(this.getClass()).searchPeopleWithUser(search, pageable));
    this.setResourceLinks(listItem);
    final RestCollection<Person> collection = this.setCollectionLinksForMethod(listItem, pageable, linkBuilder);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @Override
  protected RestCollection<Person> setCollectionLinksForMethod(Page<Person> listItem, Pageable pageable, WebMvcLinkBuilder linkBuilder) {
    final RestCollection<Person> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkBuilder.withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder()).withRel(ActionName.MODULE));
    this.addSortLinks(linkBuilder, collection);
    this.addPageLinks(linkBuilder, collection, pageable);
    this.addOthersLinks(collection);
    return collection;
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.UPLOAD_AVATAR)
  @PreAuthorize("hasAnyAuthority('"
          + AuthApplicationRole.ADMIN_ID + "', '"
          + AuthApplicationRole.TRUSTED_CLIENT_ID + "', '"
          + AuthApplicationRole.ROOT_ID
          + "') "
          + "or @personPermissionService.isUserConnectedLinkedToPerson(#id)")
  public HttpEntity<Person> uploadAvatar(@PathVariable String id, @RequestParam(DLCMConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DOWNLOAD_AVATAR)
  @ResponseBody
  public HttpEntity<StreamingResponseBody> downloadAvatar(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @PreAuthorize("hasAnyAuthority('"
          + AuthApplicationRole.ADMIN_ID + "', '"
          + AuthApplicationRole.TRUSTED_CLIENT_ID + "', '"
          + AuthApplicationRole.ROOT_ID
          + "') "
          + "or @personPermissionService.isUserConnectedLinkedToPerson(#id)")
  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DELETE_AVATAR)
  @ResponseBody
  public HttpEntity<Person> deleteAvatar(@PathVariable String id) {
    return super.deleteFile(id);
  }

}

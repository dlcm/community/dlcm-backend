/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - InstitutionsUserController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.business.InstitutionPersonRoleService;
import ch.dlcm.business.InstitutionService;
import ch.dlcm.business.PersonService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.dto.AuthorizedInstitutionDto;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.specification.InstitutionSpecification;

@RestController
@UserPermissions
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_AUTHORIZED_INSTITUTION)
public class InstitutionsUserController extends SolidifyController implements ControllerWithHateoasHome, ApplicationRoleListService {
  private static final Logger logger = LoggerFactory.getLogger(InstitutionsUserController.class);

  private final PersonService personService;
  private final InstitutionPersonRoleService institutionPersonRoleService;
  private final InstitutionService institutionService;

  public InstitutionsUserController(PersonService personService,
          InstitutionService institutionService,
          InstitutionPersonRoleService institutionPersonRoleService) {
    this.personService = personService;
    this.institutionService = institutionService;
    this.institutionPersonRoleService = institutionPersonRoleService;
  }

  /**
   * @return a list of Institutions for which the authenticated user is manager
   */
  @GetMapping
  @SuppressWarnings("squid:S4684")
  public HttpEntity<RestCollection<AuthorizedInstitutionDto>> listAll(@ModelAttribute Institution search,
          @RequestParam(required = false, defaultValue = "false") boolean showMembers, Pageable pageable) {
    final Page<AuthorizedInstitutionDto> institutionsList = this.getListAuthorizedInstitutions(pageable, search, showMembers);

    final RestCollection<AuthorizedInstitutionDto> collection = new RestCollection<>(institutionsList, pageable);
    collection.add(linkTo(this.getClass()).withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder()).withRel(ActionName.MODULE));

    this.addPageLinks(linkTo(this.getClass()), collection, pageable);

    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  private Page<AuthorizedInstitutionDto> getListAuthorizedInstitutions(Pageable pageable, Institution search, boolean showMembers) {
    Page<AuthorizedInstitutionDto> listItem;
    if (this.isRootOrTrustedOrAdminRole()) {
      final InstitutionSpecification institutionSpecification = this.institutionService.getSpecification(search);
      final Page<Institution> institutionList = this.institutionService.findAll(institutionSpecification, pageable);
      listItem = this.getAuthorizedInstitutionWithRole(institutionList);
    } else {
      listItem = this.getInstitutionsWithPermissionsForAuthenticatedUser(search, showMembers, pageable);
    }
    return listItem;
  }

  /**
   * @return all institutions on which the authenticated user has permissions
   * @throws SolidifyRuntimeException if the authenticated user is not associated to any Person
   */
  private Page<AuthorizedInstitutionDto> getInstitutionsWithPermissionsForAuthenticatedUser(Institution search,
          boolean showMembers, Pageable pageable) {

    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();
    Page<Institution> institutionPage;

    if (personId != null) {
      // Sort not supported => remove it
      if (!pageable.getSort().isUnsorted()) {
        logger.warn("Sort ({}) not supported: remove it", pageable.getSort());
        pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize());
      }
      institutionPage = this.personService.findInstitutionsByPersonIdAndInstitutionName(personId, search.getName(), showMembers, pageable);
    } else {
      final String externalUid = this.getAuthenticatedUserExternalUid();
      throw new SolidifyRuntimeException("User '" + externalUid + "' does not correspond to any Person");
    }
    // convert to institution dto and add the role in each institution
    return this.getAuthorizedInstitutionWithRole(institutionPage);
  }

  private Page<AuthorizedInstitutionDto> getAuthorizedInstitutionWithRole(Page<Institution> institutionPage) {
    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();

    // convert to institution dto and add the role in each institution
    Page<AuthorizedInstitutionDto> institutionDtoList = institutionPage.map(AuthorizedInstitutionDto::fromEntity);

    if (personId != null) {
      for (AuthorizedInstitutionDto institutionItem : institutionDtoList) {
        Optional<Role> role = this.institutionPersonRoleService.findRoleByPersonAndInstitution(personId, institutionItem.getResId());
        role.ifPresent(institutionItem::setRole);
      }
    }
    return institutionDtoList;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_ORG_UNIT)
public class OrganizationalUnitController extends ResourceWithFileController<OrganizationalUnit> {

  private static final Logger log = LoggerFactory.getLogger(OrganizationalUnitController.class);

  @Override
  @PreAuthorize("@institutionPermissionService.isInstitutionalManager(#organizationalUnit)")
  public HttpEntity<OrganizationalUnit> create(@RequestBody OrganizationalUnit organizationalUnit) {
    return super.create(organizationalUnit);
  }

  @UserPermissions
  @Override
  public HttpEntity<OrganizationalUnit> get(@PathVariable String id) {
    return super.get(id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<OrganizationalUnit>> list(@ModelAttribute OrganizationalUnit search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<OrganizationalUnit>> advancedSearch(@ModelAttribute OrganizationalUnit resource,
          @RequestParam("search") String search, @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(resource, search, matchtype, pageable);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<OrganizationalUnit>> advancedSearch(@RequestBody OrganizationalUnit search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(search, matchtype, pageable);
  }

  @PreAuthorize("@organizationalUnitPermissionService.isAllowedToUpdate(#id)")
  @Override
  public HttpEntity<OrganizationalUnit> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  /**
   * Delete an @OrganizationalUnit.
   * @param id
   * @return HttpStatus.OK (200) in case of delete or HttpStatus.ACCEPTED (202) in case of closing the org unit to distinguish between both operations
   */
  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#id, 'DELETE')")
  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    if (this.itemService.findOne(id).getIsEmpty()) {
      return super.delete(id);
    }
    return this.closeOrgUnit(id, null);
  }

  @NoOnePermissions
  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
  }

  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#id, 'DELETE')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.CLOSE)
  public ResponseEntity<Void> close(@PathVariable String id, @RequestParam(value = "closingDate", required = false) String closingDate) {
    return this.closeOrgUnit(id, closingDate);
  }

  private ResponseEntity<Void> closeOrgUnit(String orgUnitId, String closingDate) {
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringTool.DATE_FORMAT);
    final OrganizationalUnit orgUnit = this.itemService.findOne(orgUnitId);
    if (orgUnit.isOpen()) {
      if (closingDate != null) {
        orgUnit.setClosingDate(LocalDate.parse(closingDate, formatter));
      } else {
        orgUnit.setClosingDate(LocalDate.now());
      }
      this.itemService.save(orgUnit);
    }
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }

  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#id, 'UPDATE')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.UPLOAD_LOGO)
  @Override
  public HttpEntity<OrganizationalUnit> uploadFile(@PathVariable String id, @RequestParam(DLCMConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @UserPermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DOWNLOAD_LOGO)
  @ResponseBody
  @Override
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @PreAuthorize("@organizationalUnitPermissionService.isAllowed(#id, 'UPDATE')")
  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DELETE_LOGO)
  @ResponseBody
  @Override
  public HttpEntity<OrganizationalUnit> deleteFile(@PathVariable String id) {
    return super.deleteFile(id);
  }
}

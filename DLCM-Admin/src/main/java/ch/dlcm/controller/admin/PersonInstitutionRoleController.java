/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - PersonInstitutionRoleController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.Relation3TiersController;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.InstitutionPersonRole;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@RestController
@UserPermissions
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PERSON + SolidifyConstants.URL_PARENT_ID + ResourceName.INSTITUTION)
public class PersonInstitutionRoleController
        extends Relation3TiersController<Person, Institution, Role, InstitutionPersonRole> {

  @Override
  @PreAuthorize("@institutionPermissionService.isInstitutionalManager(#id)")
  public HttpEntity<List<InstitutionPersonRole>> create(@PathVariable String parentid, @PathVariable String id,
          @RequestBody String[] roleIds) {
    if (roleIds.length > 1) {
      throw new SolidifyUnmodifiableException("Only one role by person by institution is allowed");
    }
    return super.create(parentid, id, roleIds);
  }

  @Override
  @AdminPermissions // only ADMIN users can add multiple institution at the same time to a person
  public HttpEntity<List<InstitutionPersonRole>> create(@PathVariable String parentid, @RequestBody String[] institutionIds) {
    return super.create(parentid, institutionIds);
  }

  @Override
  @PreAuthorize("@institutionPermissionService.isInstitutionalManager(#id)")
  public HttpEntity<Relation3TiersChildDTO> get(@PathVariable String parentid, @PathVariable String id) {
    HttpEntity<Relation3TiersChildDTO> relation3TiersChildDTO;
    try {
      relation3TiersChildDTO = super.get(parentid, id);
    } catch (SolidifyResourceNotFoundException ex) {
      // Person has no role in Institution, this not an error don't let the SolidfyControllerAdvice catch it.
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return relation3TiersChildDTO;
  }

  @Override
  @AdminPermissions // only ADMIN users can list a person's institution
  public HttpEntity<RestCollection<Relation3TiersChildDTO>> list(@PathVariable String parentid, @ModelAttribute Institution filterInstitution,
          Pageable pageable) {
    return super.list(parentid, filterInstitution, pageable);
  }

  @Override
  @PreAuthorize("@institutionPermissionService.isInstitutionalManager(#id)")
  public HttpEntity<List<InstitutionPersonRole>> update(@PathVariable String parentid, @PathVariable String id,
          @RequestBody InstitutionPersonRole joinResource) {
    return super.update(parentid, id, joinResource);
  }

  @Override
  @PreAuthorize("@institutionPermissionService.isInstitutionalManager(#id)")
  public HttpEntity<InstitutionPersonRole> update(@PathVariable String parentid, @PathVariable String id,
          @PathVariable String grandChildId, @RequestBody Map<String, Object> joinResource) {
    return super.update(parentid, id, grandChildId, joinResource);
  }

  @Override
  @PreAuthorize("@institutionPermissionService.isInstitutionalManager(#id)")
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id, @RequestBody(required = false) String[] roleIds) {
    return super.delete(parentid, id, roleIds);
  }

  @Override
  @AdminPermissions // only ADMIN users can remove multiple institution at the same time from a person
  public ResponseEntity<Void> delete(@PathVariable String parentid, @RequestBody(required = false) String[] institutionIds) {
    return super.delete(parentid, institutionIds);
  }

  @PostMapping(SolidifyConstants.URL_ID + "/" + DLCMActionName.SET_ROLE)
  @PreAuthorize("@institutionPermissionService.isInstitutionalManager(#id)")
  public HttpEntity<HttpStatus> setGrandChildList(@PathVariable String parentid, @PathVariable String id, @RequestBody String[] gdChildIds) {
    return super.setGrandChildList(parentid, id, gdChildIds);
  }
}

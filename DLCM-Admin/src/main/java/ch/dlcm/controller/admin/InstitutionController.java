/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - InstitutionController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.InstitutionService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_INSTITUTION)
public class InstitutionController extends ResourceWithFileController<Institution> {

  @Override
  public HttpEntity<Institution> create(@RequestBody Institution institution) {
    return super.create(institution);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<Institution> get(@PathVariable String id) {
    return super.get(id);
  }

  @EveryonePermissions
  @GetMapping(path = SolidifyConstants.URL_ID, params = DLCMConstants.IDENTIFIER_TYPE_PARAM)
  public HttpEntity<Institution> getById(@PathVariable String id,
          @RequestParam(defaultValue = ResourceIdentifierType.DEFAULT) ResourceIdentifierType identifierType) {
    Institution institution = null;
    switch (identifierType) {
      case ROR_ID:
        institution = ((InstitutionService) this.itemService).getByRorId(id);
        this.addLinks(institution);
        return new ResponseEntity<>(institution, HttpStatus.OK);
      case NAME:
        institution = ((InstitutionService) this.itemService).getByName(id);
        this.addLinks(institution);
        return new ResponseEntity<>(institution, HttpStatus.OK);
      case RES_ID:
        return super.get(id);
      default:
        break;
    }
    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<RestCollection<Institution>> list(@ModelAttribute Institution institution, Pageable pageable) {
    return super.list(institution, pageable);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<RestCollection<Institution>> advancedSearch(@ModelAttribute Institution institution, @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(institution, search, matchtype, pageable);
  }

  @EveryonePermissions
  @Override
  public HttpEntity<RestCollection<Institution>> advancedSearch(@RequestBody Institution institution,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(institution, matchtype, pageable);
  }

  @Override
  @PreAuthorize("@institutionPermissionService.isInstitutionalManager(#id)")
  public HttpEntity<Institution> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.UPLOAD_LOGO)
  @PreAuthorize("@institutionPermissionService.isInstitutionalManager(#id)")
  @Override
  public HttpEntity<Institution> uploadFile(@PathVariable String id, @RequestParam(DLCMConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @EveryonePermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DOWNLOAD_LOGO)
  @ResponseBody
  @Override
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DELETE_LOGO)
  @PreAuthorize("@institutionPermissionService.isInstitutionalManager(#id)")
  @ResponseBody
  @Override
  public HttpEntity<Institution> deleteFile(@PathVariable String id) {
    return super.deleteFile(id);
  }

}

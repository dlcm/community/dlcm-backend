/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitSubmissionPolicyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.Relation2TiersController;
import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.business.OrganizationalUnitService;
import ch.dlcm.business.SubmissionPolicyService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.policies.OrganizationalUnitSubmissionPolicy;
import ch.dlcm.model.policies.OrganizationalUnitSubmissionPolicyId;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@RestController
@ConditionalOnBean(AdminController.class)
@PreAuthorize("@organizationalUnitPermissionService.isOrgUnitManager(#parentid)")
@RequestMapping(UrlPath.ADMIN_ORG_UNIT + SolidifyConstants.URL_PARENT_ID + ResourceName.SUB_POLICY)
public class OrganizationalUnitSubmissionPolicyController
        extends Relation2TiersController<OrganizationalUnit, SubmissionPolicy, OrganizationalUnitSubmissionPolicy, OrganizationalUnitSubmissionPolicyId> {

  private final OrganizationalUnitService orgUnitService;
  private final SubmissionPolicyService submissionPolicyService;

  public OrganizationalUnitSubmissionPolicyController(OrganizationalUnitService orgUnitService, SubmissionPolicyService submissionPolicyService) {
    this.orgUnitService = orgUnitService;
    this.submissionPolicyService = submissionPolicyService;
  }

  @Override
  public HttpEntity<List<JoinResourceContainer<OrganizationalUnitSubmissionPolicy>>> create(@PathVariable String parentid,
          @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @UserPermissions
  @Override
  public HttpEntity<JoinResourceContainer<OrganizationalUnitSubmissionPolicy>> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @UserPermissions
  @Override
  public HttpEntity<RestCollection<JoinResourceContainer<OrganizationalUnitSubmissionPolicy>>> list(@PathVariable String parentid,
          @ModelAttribute SubmissionPolicy filterItem, Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  public HttpEntity<JoinResourceContainer<OrganizationalUnitSubmissionPolicy>> update(@PathVariable String parentid, @PathVariable String id,
          @RequestBody OrganizationalUnitSubmissionPolicy joinResource) {
    if(joinResource.getDefaultPolicy() != null && joinResource.getDefaultPolicy()) {
      OrganizationalUnit orgUnit = orgUnitService.findOne(parentid);
      SubmissionPolicy submissionPolicy = submissionPolicyService.findOne(id);
      orgUnit.setDefaultSubmissionPolicy(submissionPolicy);
      orgUnitService.save(orgUnit);
    }
    return super.update(parentid, id, joinResource);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody(required = false) String[] ids) {
    return super.deleteList(parentid, ids);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - ArchiveACLController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.security.Principal;
import java.time.OffsetDateTime;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.validation.ValidationError;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.ArchiveACLService;
import ch.dlcm.business.NotificationService;
import ch.dlcm.business.OrganizationalUnitService;
import ch.dlcm.business.UserService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.specification.ArchiveACLSpecification;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_ARCHIVE_ACL)
public class ArchiveACLController extends ResourceWithFileController<ArchiveACL> {

  private final HttpRequestInfoProvider httpRequestInfoProvider;
  private final UserService userService;
  private final ArchiveACLService archiveACLService;
  private final OrganizationalUnitService orgUnitService;

  private final NotificationService notificationService;
  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteService;

  public ArchiveACLController(HttpRequestInfoProvider httpRequestInfoProvider,
          UserService userService,
          ArchiveACLService archiveACLService,
          OrganizationalUnitService orgUnitService,
          NotificationService notificationService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteService) {
    this.httpRequestInfoProvider = httpRequestInfoProvider;
    this.userService = userService;
    this.archiveACLService = archiveACLService;
    this.orgUnitService = orgUnitService;
    this.notificationService = notificationService;
    this.aipRemoteService = aipRemoteService;
  }

  @Override
  @PreAuthorize("@aclPermissionService.isAllowedToCreate(#archiveACL)")
  public HttpEntity<ArchiveACL> create(@RequestBody ArchiveACL archiveACL) {
    this.validateCreationAndUpdate(archiveACL);
    return super.create(archiveACL);
  }

  @Override
  @PreAuthorize("@aclPermissionService.isAllowed(#id, 'GET')")
  public HttpEntity<ArchiveACL> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @PreAuthorize("@aclPermissionService.isAllowedToList(#search)")
  public HttpEntity<RestCollection<ArchiveACL>> list(@ModelAttribute ArchiveACL search, Pageable pageable) {
    if (search.isDeleted()) {
      throw new SolidifyValidationException(new ValidationError(HttpStatus.BAD_REQUEST.getReasonPhrase()),
              this.messageService.get("validation.archiveAcl.cannotUpdateDeletedArchiveAcl"));
    }
    final ArchiveACLSpecification spec = (ArchiveACLSpecification) this.itemService.getSpecification(search);
    spec.setDeleted(false);
    Page<ArchiveACL> listItem = this.itemService.findAll(spec, pageable);
    this.setResourceLinks(listItem);
    RestCollection<ArchiveACL> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @SuppressWarnings("squid:S4684")
  @AdminPermissions
  @GetMapping(value = "/" + DLCMActionName.LIST_ALL_ARCHIVE_ACL)
  public HttpEntity<RestCollection<ArchiveACL>> listDeletedAndNotDeleted(@RequestParam(value = "deleted", required = false) Boolean deleted,
          @ModelAttribute ArchiveACL search, Pageable pageable) {
    final ArchiveACLSpecification spec = (ArchiveACLSpecification) this.itemService.getSpecification(search);
    spec.setDeleted(deleted);
    Page<ArchiveACL> listItem = this.itemService.findAll(spec, pageable);
    this.setResourceLinks(listItem);
    RestCollection<ArchiveACL> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @UserPermissions
  @GetMapping("/" + DLCMActionName.GET_MY_ACLS)
  public HttpEntity<RestCollection<ArchiveACL>> listMyACL(Pageable pageable) {
    User currentUser = this.getCurrentUser();
    Page<ArchiveACL> acls = this.archiveACLService.findNotDeletedByUser(currentUser, pageable);
    return new ResponseEntity<>(new RestCollection<>(acls, pageable), HttpStatus.OK);
  }

  @Override
  @PreAuthorize("@aclPermissionService.isAllowed(#id, 'DELETE')")
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  @NoOnePermissions
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
  }

  @Override
  @AdminPermissions
  public HttpEntity<ArchiveACL> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    ArchiveACL archiveACL = this.itemService.findOne(id);
    this.itemService.patchResource(archiveACL, updateMap);
    this.validateCreationAndUpdate(archiveACL);
    return super.update(id, updateMap);
  }

  @PreAuthorize("@aclPermissionService.isAllowed(#id, 'GET')")
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DOWNLOAD_DUA)
  @ResponseBody
  @Override
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @NoOnePermissions
  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DELETE_DUA)
  @ResponseBody
  @Override
  public HttpEntity<ArchiveACL> deleteFile(@PathVariable String id) {
    return super.deleteFile(id);
  }

  @PreAuthorize("@aclPermissionService.isAllowedToCreate(#archiveId, #userId, #organizationalUnitId)")
  @PostMapping("/" + DLCMActionName.UPLOAD_DUA)
  public HttpEntity<ArchiveACL> createItemAndUploadFile(@RequestParam("archiveId") String archiveId,
          @RequestParam("userId") String userId,
          @RequestParam("organizationalUnitId") String organizationalUnitId,
          @RequestParam(DLCMConstants.FILE) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    // Make a pre-validation because validation on save is done after uploading the file
    User user;
    try {
      user = this.userService.findOne(userId);
    } catch (SolidifyResourceNotFoundException e) {
      throw new SolidifyValidationException(new ValidationError("The provided user does not exist"));
    }
    OrganizationalUnit organizationalUnit;
    try {
      organizationalUnit = this.orgUnitService.findOne(organizationalUnitId);
    } catch (SolidifyResourceNotFoundException e) {
      throw new SolidifyValidationException(new ValidationError("The provided organizational unit does not exist"));
    }
    try {
      this.aipRemoteService.findOne(archiveId);
    } catch (SolidifyResourceNotFoundException e) {
      throw new SolidifyValidationException(new ValidationError("The provided archive does not exist"));
    }
    ArchiveACL archiveACL = new ArchiveACL();
    archiveACL.setAipId(archiveId);
    archiveACL.setUser(user);
    archiveACL.setOrganizationalUnit(organizationalUnit);
    return super.createItemAndUploadFile(archiveACL, file, mimeType);
  }

  @PreAuthorize("@aclPermissionService.isAllowedToCreate(#notificationId)")
  @PostMapping("/" + DLCMActionName.CREATE_FROM_NOTIFICATION + SolidifyConstants.URL_ID)
  public HttpEntity<ArchiveACL> createFromNotification(@PathVariable("id") String notificationId) {
    Notification notification = this.notificationService.findOne(notificationId);
    final ArchiveACL archiveACL = this.archiveACLService.getAclFromNotification(notification);
    return super.create(archiveACL);
  }

  @NoOnePermissions
  @Override
  public HttpEntity<ArchiveACL> uploadFile(@PathVariable String id, @RequestParam(SolidifyConstants.FILE_PARAM) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  private User getCurrentUser() {
    final Principal principal = this.httpRequestInfoProvider.getPrincipal();
    return this.userService.findByExternalUid(principal.getName());
  }

  private void validateCreationAndUpdate(ArchiveACL archiveACL) {
    // Validate these rules only at the creation and update
    // Expiration date in the past is allowed for soft deleted ACL
    if (archiveACL.isDeleted()) {
      throw new SolidifyValidationException(new ValidationError(HttpStatus.BAD_REQUEST.getReasonPhrase()),
              this.messageService.get("validation.archiveAcl.cannotCreateOrUpdateDeletedArchiveAcl"));
    }
    final OffsetDateTime archiveAclExpiration = archiveACL.getExpiration();
    if (archiveAclExpiration != null && archiveAclExpiration.isBefore(OffsetDateTime.now())) {
      throw new SolidifyValidationException(new ValidationError(HttpStatus.BAD_REQUEST.getReasonPhrase()),
              this.messageService.get("validation.archiveAcl.expirationTimeInPast"));
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - RatingTypeController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceReadOnlyController;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.validation.ValidationError;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.settings.RatingType;
import ch.dlcm.rest.UrlPath;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_RATING_TYPE)
public class RatingTypeController extends ResourceReadOnlyController<RatingType> {

  @RootPermissions
  @PostMapping
  @SuppressWarnings("squid:S4684")
  public HttpEntity<RatingType> create(@RequestBody RatingType ratingType) {
    // Check if already exists
    if (ratingType.getResId() != null && this.itemService.existsById(ratingType.getResId())) {
      throw new SolidifyValidationException(new ValidationError(HttpStatus.BAD_REQUEST.getReasonPhrase()),
              this.messageService.get("validation.resource.alreadyexist", new Object[] { ratingType.getResId() }));
    }

    final RatingType createdRatingType = this.itemService.save(ratingType);
    this.addLinks(createdRatingType);
    return new ResponseEntity<>(createdRatingType, HttpStatus.CREATED);
  }

  @Override
  public HttpEntity<RatingType> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<RatingType>> list(@ModelAttribute RatingType search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<RestCollection<RatingType>> advancedSearch(@ModelAttribute RatingType resource,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(resource, search, matchtype, pageable);
  }

  @Override
  public HttpEntity<RestCollection<RatingType>> advancedSearch(@RequestBody RatingType search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(search, matchtype, pageable);
  }
}

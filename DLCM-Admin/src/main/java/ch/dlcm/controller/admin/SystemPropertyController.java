/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - SystemPropertyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.model.OrcidConfigDTO;
import ch.unige.solidify.model.index.FacetProperties;
import ch.unige.solidify.service.rest.trusted.TrustedIndexFieldAliasRemoteResourceService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AdminController;
import ch.dlcm.rest.UrlPath;

@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_SYSTEM_PROPERTY)
public class SystemPropertyController implements ControllerWithHateoasHome {

  private final SystemProperties systemProperties;

  private final TrustedIndexFieldAliasRemoteResourceService indexFieldAliasRemoteService;

  private final String indexName;

  public SystemPropertyController(DLCMProperties dlcmProperties, SolidifyProperties solidifyProperties,
          TrustedIndexFieldAliasRemoteResourceService indexFieldAliasRemoteService) {
    this.systemProperties = new SystemProperties(dlcmProperties, solidifyProperties);
    this.indexFieldAliasRemoteService = indexFieldAliasRemoteService;
    this.indexName = dlcmProperties.getIndexing().getIndexName();
  }

  @Schema(description = "A system property is defined by the back-end administrators, for the whole system.")
  private static class SystemProperties extends RepresentationModel<SystemProperties> {

    @Schema(description = "The default checksum type used by the system")
    private final String defaultChecksum;

    @Schema(description = "The list of checksum algo used by the system")
    private final String[] checksums;

    @Schema(description = "The default license used by the system.")
    private final String defaultLicense;

    @Schema(description = "The maximum upload file size, in bytes.")
    private final String uploadFileSizeLimit;

    @Schema(description = "The list of golden formats of the system.")
    private final String[] goldenFormats;

    @Schema(description = "The list of forbidden characters the system.")
    private final String[] forbiddenCharacters;

    @Schema(description = "If archiving research data is supported")
    private final boolean researchDataSupported;

    @Schema(description = "If archiving administrative data is supported.")
    private final boolean administrativeDataSupported;

    @Schema(description = "The default identifier type used by the system: DOI or ARK.")
    private final String defaultIdentifierType;

    @Schema(description = "Describes facet properties used on archives search page.")
    private List<FacetProperties> searchFacets;

    @Schema(description = "ORCID configuration (authorizeUrl, clientId, scope)")
    private final OrcidConfigDTO orcid;

    public SystemProperties(DLCMProperties dlcmProperties, SolidifyProperties solidifyProperties) {
      this.defaultChecksum = dlcmProperties.getParameters().getDefaultChecksum();
      this.defaultLicense = dlcmProperties.getParameters().getDefaultLicense();
      this.uploadFileSizeLimit = String.valueOf(dlcmProperties.getParameters().getUploadFileSizeLimit().toBytes());
      this.goldenFormats = dlcmProperties.getParameters().getGoldenFormats();
      this.forbiddenCharacters = dlcmProperties.getParameters().getForbiddenCharacters();
      this.checksums = dlcmProperties.getParameters().getChecksumList();
      this.researchDataSupported = dlcmProperties.getParameters().isResearchDataEnabled();
      this.administrativeDataSupported = dlcmProperties.getParameters().isAdministrativeDataEnabled();
      this.defaultIdentifierType = dlcmProperties.getParameters().getDefaultIdentifierType().getDescription();
      this.orcid = new OrcidConfigDTO(solidifyProperties.getOrcid());
    }

    public String getDefaultChecksum() {
      return this.defaultChecksum;
    }

    public String getDefaultLicense() {
      return this.defaultLicense;
    }

    public String getUploadFileSizeLimit() {
      return this.uploadFileSizeLimit;
    }

    public List<FacetProperties> getSearchFacets() {
      return this.searchFacets;
    }

    public String[] getGoldenFormats() {
      return this.goldenFormats;
    }

    public String[] getForbiddenCharacters() {
      return this.forbiddenCharacters;
    }

    public String[] getChecksums() {
      return this.checksums;
    }

    public boolean isResearchDataSupported() {
      return this.researchDataSupported;
    }

    public boolean isAdministrativeDataSupported() {
      return this.administrativeDataSupported;
    }

    public String getDefaultIdentifierType() {
      return this.defaultIdentifierType;
    }

    public OrcidConfigDTO getOrcid() {
      return this.orcid;
    }

    public void setSearchFacets(List<FacetProperties> searchFacets) {
      this.searchFacets = searchFacets;
    }
  }

  @GetMapping
  public HttpEntity<SystemProperties> systemProperties() {
    List<FacetProperties> facetProperties = this.indexFieldAliasRemoteService.getFacetProperties(this.indexName);
    this.systemProperties.setSearchFacets(facetProperties);
    this.systemProperties.removeLinks();
    this.systemProperties.add(linkTo(this.getClass()).withSelfRel());
    return new ResponseEntity<>(this.systemProperties, HttpStatus.OK);
  }
}

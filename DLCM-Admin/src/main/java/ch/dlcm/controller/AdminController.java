/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - AdminController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ModuleController;

import ch.dlcm.business.ApplicationRoleService;
import ch.dlcm.business.ArchiveTypeService;
import ch.dlcm.business.DisseminationPolicyService;
import ch.dlcm.business.FundingAgencyService;
import ch.dlcm.business.InstitutionService;
import ch.dlcm.business.LanguageService;
import ch.dlcm.business.LicenseService;
import ch.dlcm.business.MetadataTypeService;
import ch.dlcm.business.NotificationTypeService;
import ch.dlcm.business.PreservationPolicyService;
import ch.dlcm.business.RatingTypeService;
import ch.dlcm.business.RoleService;
import ch.dlcm.business.ScheduledTaskService;
import ch.dlcm.business.SubjectAreaService;
import ch.dlcm.business.SubmissionPolicyService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.UrlPath;

@RestController
@ConditionalOnProperty(prefix = "dlcm.module.admin", name = "enable")
@RequestMapping(UrlPath.ADMIN)
public class AdminController extends ModuleController {

  AdminController(DLCMProperties dlcmProperties,
          LanguageService languageService,
          RoleService roleService,
          LicenseService licenseService,
          FundingAgencyService fundingAgencyService,
          InstitutionService institutionService,
          PreservationPolicyService preservationPolicyService,
          SubmissionPolicyService submissionPolicyService,
          ApplicationRoleService applicationRoleService,
          SubjectAreaService subjectAreaService,
          MetadataTypeService metadataTypeService,
          NotificationTypeService notificationTypeService,
          RatingTypeService ratingTypeService,
          ScheduledTaskService scheduledTaskService,
          DisseminationPolicyService disseminationPolicyService,
          ArchiveTypeService archiveTypeService) {
    super(ModuleName.ADMIN);

    // Initialize required data
    applicationRoleService.initRequiredData();
    roleService.initRequiredData();
    ratingTypeService.initRequiredData();
    notificationTypeService.initRequiredData();
    archiveTypeService.initRequiredData();
    disseminationPolicyService.initRequiredData();

    // Initialize optional data
    if (dlcmProperties.getData().isInit()) {
      languageService.initOptionalData();
      licenseService.initOptionalData();
      fundingAgencyService.initOptionalData();
      institutionService.initOptionalData();
      preservationPolicyService.initOptionalData();
      submissionPolicyService.initOptionalData();
      subjectAreaService.initOptionalData();
      metadataTypeService.initOptionalData();
      for (DLCMProperties.ScheduledTaskConfig scheduledTaskConfig : dlcmProperties.getScheduledTasks()) {
        scheduledTaskService.createScheduledTaskFromConfig(scheduledTaskConfig);
      }
    }
  }
}

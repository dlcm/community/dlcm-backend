/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - RorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.NoTokenRestClientTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMProperties.WebUrls;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.RorInfo;
import ch.dlcm.model.RorInfo.IdentifierDetail;
import ch.dlcm.model.RorInterface;

@Service
@ConditionalOnBean(AdminController.class)
public class RorService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(RorService.class);

  private NoTokenRestClientTool restClientTool;
  private WebUrls webUrls;

  public RorService(MessageService messageService, DLCMProperties dlcmProperties, NoTokenRestClientTool restClientTool) {
    super(messageService, dlcmProperties);
    this.restClientTool = restClientTool;
    this.webUrls = dlcmProperties.getWebUrls();
  }

  @Cacheable(cacheNames = DLCMCacheNames.ROR_INFO)
  public RorInfo loadInfo(String rorId) {
    if (StringTool.isNullOrEmpty(rorId)) {
      // Do nothing
      return null;
    }
    try {
      return this.restClientTool.getClient().getForObject(this.webUrls.getRorApi() + rorId, RorInfo.class);
    } catch (RuntimeException e) {
      log.warn("Cannot extract info from ror.org", e);
    }
    return null;
  }

  public void setInfo(RorInterface researchOrg, RorInfo researchOrgDetails) {
    try {
      // ROR
      researchOrg.getIdentifiers().put(DLCMConstants.ROR, researchOrgDetails.getId());
      // WEB
      if (researchOrgDetails.getWebSites().length > 0) {
        researchOrg.getIdentifiers().put(DLCMConstants.WEB, researchOrgDetails.getWebSites()[0]);
      }
      // GRID
      if (researchOrgDetails.getExternalIds().getGrid() != null) {
        researchOrg.getIdentifiers().put(DLCMConstants.GRID, this.webUrls.getGrid() + researchOrgDetails.getExternalIds().getGrid().getPreferred());
      }
      // ISNI
      if (researchOrgDetails.getExternalIds().getIsni() != null) {
        researchOrg.getIdentifiers().put(DLCMConstants.ISNI,
                this.webUrls.getIsni() + this.getIdentifier(researchOrgDetails.getExternalIds().getIsni()));
      }
      // Crossref Funder
      if (researchOrgDetails.getExternalIds().getCrossrefFunder() != null) {
        researchOrg.getIdentifiers().put(DLCMConstants.CROSSREF_FUNDER,
                this.webUrls.getCrossrefFunder() + this.getIdentifier(researchOrgDetails.getExternalIds().getCrossrefFunder()));
      }
      // Wikidata
      if (researchOrgDetails.getExternalIds().getWikidata() != null) {
        researchOrg.getIdentifiers().put(DLCMConstants.WIKIDATA,
                this.webUrls.getWikidata() + this.getIdentifier(researchOrgDetails.getExternalIds().getWikidata()));
      }
    } catch (NullPointerException e) {
      researchOrg.getIdentifiers().put(DLCMConstants.ROR, this.webUrls.getRor() + researchOrg.getRorId());
    }
  }

  private String getIdentifier(IdentifierDetail identifier) {
    String id = identifier.getPreferred();
    if (StringTool.isNullOrEmpty(id)) {
      id = identifier.getAll()[0];
    }
    if (id.contains(" ")) {
      return id.replaceAll("\\s+", "");
    }
    return id;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.dlcm.business.OrganizationalUnitService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.security.Role;
import ch.dlcm.service.rest.abstractservice.OrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;

@Service
@ConditionalOnBean(AdminController.class)
public class OrganizationalUnitPermissionService extends AbstractPermissionWithOrgUnitService {

  private final OrganizationalUnitService organizationalUnitService;
  private final OrganizationalUnitRemoteResourceService trustedOrganizationalUnitRemoteService;

  public OrganizationalUnitPermissionService(OrganizationalUnitService organizationalUnitService,
          TrustedPersonRemoteResourceService personRemoteResourceService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService) {
    super(personRemoteResourceService);
    this.organizationalUnitService = organizationalUnitService;
    this.trustedOrganizationalUnitRemoteService = organizationalUnitRemoteResourceService;
  }

  @Override
  protected boolean isManagerAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return true;
  }

  @Override
  protected boolean isStewardAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return this.isActionAllowedForEveryMember(action);
  }

  @Override
  protected boolean isApproverAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return this.isActionAllowedForEveryMember(action);
  }

  @Override
  protected boolean isCreatorAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return this.isActionAllowedForEveryMember(action);
  }

  @Override
  protected boolean isVisitorAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return this.isActionAllowedForEveryMember(action);
  }

  private boolean isActionAllowedForEveryMember(DLCMControllerAction action) {
    return action == DLCMControllerAction.LIST_MEMBERS ||
            action == DLCMControllerAction.GET_MEMBER ||
            action == DLCMControllerAction.LIST_FORMS ||
            action == DLCMControllerAction.GET_FORM ||
            action == DLCMControllerAction.GET_CURRENT_FORM;
  }

  @Override
  OrganizationalUnitAwareResource getExistingResource(String resId) {
    return this.organizationalUnitService.findOne(resId);
  }

  public boolean isOrgUnitManager(String orgUnitId) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    final String personId = this.getPersonId();
    final Optional<Role> orgUnitRole = this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(personId, orgUnitId);
    final Optional<Role> inheritedRole = this.trustedPersonRemoteResourceService.findInheritedOrganizationalRole(personId, orgUnitId);
    final Optional<Role> roles = Role.maxRole(orgUnitRole, inheritedRole);
    return roles.map(role -> role.equals(Role.MANAGER)).orElse(false);
  }

  public boolean isCurrentUserOrgUnitMember(String orgUnitId) {
    return this.isCurrentUserOrgUnitMember(orgUnitId, this.getPersonId());
  }

  public boolean isCurrentUserOrgUnitMember(String orgUnitId, String personId) {
    final String callerPersonId = this.getPersonId();
    if (!callerPersonId.equals(personId)) {
      return false;
    }
    return (this.trustedOrganizationalUnitRemoteService.getAuthorizedOrganizationalUnit(orgUnitId)).isPresent();
  }

}

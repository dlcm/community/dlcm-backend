/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - OrganizationalUnitPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.dlcm.business.InstitutionPersonRoleService;
import ch.dlcm.business.OrganizationalUnitService;
import ch.dlcm.business.PersonInstitutionRoleService;
import ch.dlcm.business.PersonService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.model.display.OrgUnitPersonRoleDTO;
import ch.dlcm.model.display.OrgUnitPersonRoleListDTO;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.InstitutionPersonRole;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.OrganizationalUnitPersonRole;
import ch.dlcm.model.settings.Person;

@Service
@ConditionalOnBean(AdminController.class)
public class InstitutionPermissionService implements ApplicationRoleListService {

  private final PersonService personService;
  private final OrganizationalUnitService orgUnitService;
  private final PersonInstitutionRoleService personInstitutionRoleService;
  private final InstitutionPersonRoleService institutionPersonRoleService;

  public InstitutionPermissionService(PersonService personService, OrganizationalUnitService orgUnitService,
          PersonInstitutionRoleService personInstitutionRoleService, InstitutionPersonRoleService institutionPersonRoleService) {
    this.personService = personService;
    this.orgUnitService = orgUnitService;
    this.personInstitutionRoleService = personInstitutionRoleService;
    this.institutionPersonRoleService = institutionPersonRoleService;
  }

  public boolean isInstitutionalManager(String institutionId) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();
    final List<InstitutionPersonRole> listRelation = this.personInstitutionRoleService.findAllRelations(personId, institutionId);
    if (listRelation.isEmpty()) {
      return false;
    } else if (listRelation.size() == 1) {
      return Role.MANAGER.equals(listRelation.get(0).getRole());
    } else {
      throw new SolidifyRuntimeException("Multiple roles detected for person " + personId + " in institution " + institutionId);
    }
  }

  public boolean isInstitutionalManagerOfOrgUnit(String orgUnitId) {
    final OrganizationalUnit orgUnit = this.orgUnitService.findOne(orgUnitId);
    return this.isInstitutionalManager(orgUnit);
  }

  public boolean isInstitutionalManager(OrganizationalUnit orgUnit) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    if (orgUnit.getInstitutions() == null) {
      return false;
    }
    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();
    for (Institution institution : orgUnit.getInstitutions()) {
      if (this.personInstitutionRoleService.relationExists(personId, institution.getResId(), Role.MANAGER_ID)) {
        return true;
      }
    }
    return false;
  }

  public Optional<Role> getInheritedRole(String orgUnitId, String personId) {
    Role maximumRole = null;
    final OrganizationalUnit orgUnit = this.orgUnitService.findOne(orgUnitId);
    for (Institution institution : orgUnit.getInstitutions()) {
      Role role;
      final List<InstitutionPersonRole> listRelation = this.personInstitutionRoleService.findAllRelations(personId, institution.getResId());
      if (listRelation.isEmpty()) {
        continue;
      } else if (listRelation.size() == 1) {
        role = listRelation.get(0).getRole();
      } else {
        throw new SolidifyRuntimeException("Multiple roles detected for person " + personId + " in institution " + institution.getResId());
      }
      if (maximumRole == null || maximumRole.getLevel() > role.getLevel()) { // Level close to 0 = Greater right
        maximumRole = role;
      }
    }
    return Optional.ofNullable(maximumRole);
  }

  public List<OrgUnitPersonRoleListDTO> getInheritedPersonRoles(String orgUnitId) {
    final Map<Person, Role> mapPersonRole = new HashMap<>();
    final OrganizationalUnit orgUnit = this.orgUnitService.findOne(orgUnitId);
    for (Institution institution : orgUnit.getInstitutions()) {
      for (InstitutionPersonRole institutionPersonRole : this.institutionPersonRoleService.findAllRelations(institution.getResId())) {
        final Person currentPerson = institutionPersonRole.getPerson();
        if (mapPersonRole.containsKey(institutionPersonRole.getPerson())) {
          if (mapPersonRole.get(currentPerson).getLevel() < institutionPersonRole.getRole().getLevel()) {
            mapPersonRole.put(currentPerson, institutionPersonRole.getRole());
          }
        } else {
          mapPersonRole.put(currentPerson, institutionPersonRole.getRole());
        }
      }
    }
    final List<OrgUnitPersonRoleListDTO> result = new ArrayList<>();
    for (Person person : mapPersonRole.keySet()) {
      final OrganizationalUnitPersonRole oupr = new OrganizationalUnitPersonRole();
      oupr.setRole(mapPersonRole.get(person));
      oupr.setPerson(person);
      oupr.setOrganizationalUnit(orgUnit);
      final OrgUnitPersonRoleDTO ouprDTO = new OrgUnitPersonRoleDTO(oupr);
      final OrgUnitPersonRoleListDTO orgUnitPersonRoleListDTO = new OrgUnitPersonRoleListDTO(person, Collections.singletonList(ouprDTO));
      result.add(orgUnitPersonRoleListDTO);
    }
    return result;
  }

  public boolean isInstitutionalManagerOfAtLeastOneOrganizationalUnit() {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();
    final List<InstitutionPersonRole> listRelation = this.personInstitutionRoleService.findAllRelations(personId);
    if (listRelation.isEmpty()) {
      return false;
    }
    return listRelation.stream().anyMatch(r -> r.getRole().equals(Role.MANAGER));
  }

  public boolean isInstitutionMember(String institutionId) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    final String personId = this.personService.getPerson(SecurityContextHolder.getContext().getAuthentication()).getResId();
    if (this.isInstitutionalMemberWithRole(personId)) {
      return true;
    }
    return this.isInstitutionalMemberWithoutRole(personId, institutionId);
  }

  private boolean isInstitutionalMemberWithRole(String personId) {
    final List<InstitutionPersonRole> listRelation = this.personInstitutionRoleService.findAllRelations(personId);
    return !listRelation.isEmpty();
  }

  private boolean isInstitutionalMemberWithoutRole(String personId, String institutionId) {
    return this.personService.isPersonMemberOfInstitutionWithoutRole(personId, institutionId);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - AclPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.validation.ValidationError;

import ch.dlcm.business.ArchiveACLService;
import ch.dlcm.business.NotificationService;
import ch.dlcm.business.OrganizationalUnitService;
import ch.dlcm.business.UserService;
import ch.dlcm.controller.AdminController;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.rest.abstractservice.ArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;

@Service
@ConditionalOnBean(AdminController.class)
public final class AclPermissionService extends AbstractPermissionWithOrgUnitService {
  private static final Logger log = LoggerFactory.getLogger(AclPermissionService.class);

  private final ArchivalInfoPackageRemoteResourceService aipRemoteResourceService;
  private final ArchiveACLService archiveACLService;

  private final UserService userService;

  private final OrganizationalUnitService organizationalUnitService;
  private final InstitutionPermissionService institutionPermissionService;

  private final NotificationService notificationService;

  public AclPermissionService(TrustedPersonRemoteResourceService personRemoteResourceService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService,
          ArchiveACLService archiveACLService,
          UserService userService,
          OrganizationalUnitService organizationalUnitService,
          InstitutionPermissionService institutionPermissionService,
          NotificationService notificationService) {
    super(personRemoteResourceService);
    this.aipRemoteResourceService = aipRemoteResourceService;
    this.archiveACLService = archiveACLService;
    this.userService = userService;
    this.organizationalUnitService = organizationalUnitService;
    this.institutionPermissionService = institutionPermissionService;
    this.notificationService = notificationService;
  }

  public boolean isAllowedToCreate(String archiveId, String userId, String organizationalUnitId) {
    final ArchiveACL archiveACL = new ArchiveACL();
    final User user = this.userService.findOne(userId);
    final OrganizationalUnit orgUnit = this.organizationalUnitService.findOne(organizationalUnitId);
    archiveACL.setAipId(archiveId);
    archiveACL.setUser(user);
    archiveACL.setOrganizationalUnit(orgUnit);
    return this.isAllowedToCreate(archiveACL);
  }

  public boolean isAllowedToCreate(String notificationId) {
    final Notification notification = this.notificationService.findOne(notificationId);
    final ArchiveACL archiveACL = this.archiveACLService.getAclFromNotification(notification);
    return this.isAllowedToCreate(archiveACL);
  }

  public boolean isAllowedToCreate(String archiveId, String userId, String orgUnitId, String notificationId) {
    final Notification notification = this.notificationService.findOne(notificationId);
    final ArchiveACL archiveACL = new ArchiveACL();
    final User user = this.userService.findOne(userId);
    final OrganizationalUnit orgUnit = this.organizationalUnitService.findOne(orgUnitId);
    if (!notification.getNotifiedOrgUnit().equals(orgUnit)) {
      return false;
    }
    if (!notification.getNotificationType().equals(NotificationType.ACCESS_DATASET_REQUEST)) {
      return false;
    }
    if (!notification.getEmitter().equals(user)) {
      return false;
    }
    if (!notification.getObjectId().equals(archiveId)) {
      return false;
    }
    archiveACL.setAipId(archiveId);
    archiveACL.setUser(user);
    archiveACL.setOrganizationalUnit(orgUnit);
    return this.isAllowedToCreate(archiveACL);
  }

  public boolean isAllowedToList(ArchiveACL search) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    String personId = this.getPersonId();
    return this.isAllowedToPerformActionOnResource(personId, search, DLCMControllerAction.LIST);
  }

  @Override
  public boolean isAllowed(String targetResId, String actionString) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    OrganizationalUnitAwareResource archiveAcl = this.getExistingResource(targetResId);
    if (archiveAcl == null) {
      throw new NoSuchElementException(targetResId);
    }

    // Only ADMIN or superior role can get a deleted archive acl
    final DLCMControllerAction action = this.getControllerAction(actionString);
    if (action == DLCMControllerAction.GET && Boolean.TRUE.equals(((ArchiveACL) archiveAcl).isDeleted())) {
      return false;
    }

    // Only Manager and Steward can get an archive acl and download its DUA
    final String personId = this.getPersonId();
    if (action == DLCMControllerAction.GET) {
      final String orgUnitId = archiveAcl.getOrganizationalUnitId();
      Optional<Role> institutionInheritedRole = this.institutionPermissionService.getInheritedRole(orgUnitId, personId);
      Optional<Role> orgUnitRole = this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(personId, orgUnitId);
      Optional<Role> role = Role.maxRole(orgUnitRole, institutionInheritedRole);
      return role.filter(value -> value.getLevel() <= Role.STEWARD.getLevel()).isPresent();
    }
    return this.isAllowedToPerformActionOnResource(personId, archiveAcl, action);
  }

  @Override
  protected boolean isManagerAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return this.isManagerOrStewardAllowedToCreateDeleteOrListAcl(existingResource, action);
  }

  @Override
  protected boolean isStewardAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return this.isManagerOrStewardAllowedToCreateDeleteOrListAcl(existingResource, action);
  }

  private boolean isManagerOrStewardAllowedToCreateDeleteOrListAcl(OrganizationalUnitAwareResource existingResource,
          DLCMControllerAction action) {
    if (existingResource instanceof ArchiveACL archiveACL && action.equals(DLCMControllerAction.CREATE)) {
      return this.checkArchiveAclCreationRequestConsistency(archiveACL);
    } else
      return action.equals(DLCMControllerAction.LIST) || action.equals(DLCMControllerAction.DELETE);
  }

  private boolean checkArchiveAclCreationRequestConsistency(ArchiveACL archiveACL) {
    final String aclOrgUnitId = archiveACL.getOrganizationalUnitId();
    final String aipId = archiveACL.getAipId();
    if (aipId == null) {
      throw new SolidifyValidationException(new ValidationError("AIP id from ACL is null."));
    }
    final ArchivalInfoPackage aip = this.aipRemoteResourceService.findOne(aipId);
    final String aipOrgUnitId = aip.getOrganizationalUnitId();
    if (!aclOrgUnitId.equals(aipOrgUnitId)) {
      log.warn("OrgUnit Id from ACL {} differs from orgUnit Id from AIP {}.", aclOrgUnitId, aipOrgUnitId);
      return false;
    }
    return true;
  }

  @Override
  protected OrganizationalUnitAwareResource getExistingResource(String resId) {
    return this.archiveACLService.findOne(resId);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - NotificationPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import java.security.Principal;
import java.text.ParseException;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.nimbusds.jwt.SignedJWT;

import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.service.HttpRequestInfoProvider;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.security.User;
import ch.dlcm.repository.NotificationRepository;
import ch.dlcm.repository.UserRepository;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;

@Service
@ConditionalOnBean(AdminController.class)
public class NotificationPermissionService implements ApplicationRoleListService {

  private final HttpRequestInfoProvider httpRequestInfoProvider;
  private final UserRepository userRepository;
  private final NotificationRepository notificationRepository;
  private final InstitutionPermissionService institutionPermissionService;
  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;
  private final TrustedPersonRemoteResourceService trustedPersonRemoteResourceService;

  public NotificationPermissionService(UserRepository userRepository, NotificationRepository notificationRepository,
          HttpRequestInfoProvider httpRequestInfoProvider,
          InstitutionPermissionService institutionPermissionService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService,
          TrustedPersonRemoteResourceService trustedPersonRemoteResourceService) {
    this.userRepository = userRepository;
    this.notificationRepository = notificationRepository;
    this.httpRequestInfoProvider = httpRequestInfoProvider;
    this.institutionPermissionService = institutionPermissionService;
    this.aipRemoteResourceService = aipRemoteResourceService;
    this.trustedPersonRemoteResourceService = trustedPersonRemoteResourceService;
  }

  public boolean isAllowedToCreate(Notification notification) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    NotificationType notificationType = notification.getNotificationType();
    return notificationType.equals(NotificationType.ACCESS_DATASET_REQUEST) ||
            notificationType.equals(NotificationType.JOIN_ORGUNIT_REQUEST) ||
            notificationType.equals(NotificationType.CREATE_ORGUNIT_REQUEST) ||
            notificationType.equals(NotificationType.APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST);
  }

  public boolean isMyNotification(String notificationId) {
    final Notification myNotification;
    if (this.isRootOrTrustedOrAdminRole()) {
      myNotification = this.notificationRepository.findByResId(notificationId);
    } else {
      final Principal principal = this.httpRequestInfoProvider.getPrincipal();
      final User currentUser = this.userRepository.findByExternalUid(principal.getName());
      final String personId = currentUser.getPerson().getResId();
      final int userApplicationRoleLevel = currentUser.getApplicationRole().getLevel();
      myNotification = this.notificationRepository.findInboxAndSentNotificationById(personId, currentUser.getResId(),
              userApplicationRoleLevel, notificationId, false);
    }
    return myNotification != null;
  }

  public boolean isAllowedToUploadDua(String notificationId) {
    final Notification notification = this.notificationRepository.findByResId(notificationId);
    if (!notification.getNotificationType().equals(NotificationType.ACCESS_DATASET_REQUEST)) {
      return false;
    }
    return this.isNotificationOfCurrentUser(notification);
  }

  public boolean isAllowedToDownloadDua(String notificationId) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    final Notification notification = this.notificationRepository.findByResId(notificationId);
    if (!notification.getNotificationType().equals(NotificationType.ACCESS_DATASET_REQUEST)) {
      return false;
    }
    if (this.isNotificationOfCurrentUser(notification)) {
      return true;
    }
    final String archiveId = notification.getObjectId();
    final ArchivalInfoPackage aip = this.aipRemoteResourceService.findOne(archiveId);
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    final User user = this.userRepository.findByExternalUid(authentication.getName());
    Optional<Role> institutionInheritedRole = this.institutionPermissionService.getInheritedRole(aip.getOrganizationalUnitId(),
            user.getPerson().getResId());
    Optional<Role> orgUnitRole = this.trustedPersonRemoteResourceService.findOrganizationalUnitRole(user.getPerson().getResId(),
            aip.getOrganizationalUnitId());
    Optional<Role> role = Role.maxRole(orgUnitRole, institutionInheritedRole);

    return role.filter(value -> value.getLevel() <= Role.STEWARD.getLevel()).isPresent();
  }

  private boolean isNotificationOfCurrentUser(Notification notification) {
    try {
      final String token = this.httpRequestInfoProvider.getIncomingToken();
      final String externalUid = SignedJWT.parse(token).getJWTClaimsSet().getClaim("user_name").toString();
      return notification.getEmitter().getExternalUid().equals(externalUid);
    } catch (ParseException ex) {
      return false;
    }
  }
}

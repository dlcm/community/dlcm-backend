/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - RatingPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import java.security.Principal;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.HttpRequestInfoProvider;

import ch.dlcm.controller.AdminController;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveUserRating;
import ch.dlcm.repository.ArchiveUserRatingRepository;
import ch.dlcm.repository.UserRepository;

@Service
@ConditionalOnBean(AdminController.class)
public class RatingPermissionService {
  private final HttpRequestInfoProvider httpRequestInfoProvider;
  private final UserRepository userRepository;
  private final ArchiveUserRatingRepository archiveUserRatingRepository;

  public RatingPermissionService(UserRepository userRepository, ArchiveUserRatingRepository archiveUserRatingRepository,
          HttpRequestInfoProvider httpRequestInfoProvider) {
    this.userRepository = userRepository;
    this.archiveUserRatingRepository = archiveUserRatingRepository;
    this.httpRequestInfoProvider = httpRequestInfoProvider;
  }

  public boolean isMyRating(String ratingId) {
    final Principal principal = this.httpRequestInfoProvider.getPrincipal();
    final User currentUser = this.userRepository.findByExternalUid(principal.getName());
    Optional<ArchiveUserRating> archiveUserRating = this.archiveUserRatingRepository.findById(ratingId);
    return archiveUserRating.isPresent() && archiveUserRating.get().getUser().getResId().equals(currentUser.getResId());
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - NotificationsEmailService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.NotificationService;
import ch.dlcm.business.PersonService;
import ch.dlcm.business.UserService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AdminController;
import ch.dlcm.message.EmailMessage;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.Person;

@Service
@ConditionalOnBean(AdminController.class)
public class NotificationsEmailService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(NotificationsEmailService.class);

  private final PersonService personService;
  private final UserService userService;
  private final NotificationService notificationService;

  protected NotificationsEmailService(MessageService messageService, PersonService personService, UserService userService,
          NotificationService notificationService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.personService = personService;
    this.userService = userService;
    this.notificationService = notificationService;
  }

  /**
   * Group notifications by person according to role specified in the type of notification, in order to keep them only if the related deposit
   * has still the given status. (we don't send a notification if the deposit status has changed since the notification was
   * created as it wouldn't make sense anymore).
   * For each deposit, it is needed to filter accordingly by the role specified in NotificationType
   *
   * @param notificationsToSend list of notifications to check
   * @return
   */
  protected Map<String, List<Notification>> getFilteredNotificationsByPersonIdAndByRoleHierarchy(List<Notification> notificationsToSend) {

    Map<String, List<Notification>> notificationsByPersonId = new HashMap<>();
    for (Notification notificationToSend : notificationsToSend) {
      // keep the notification only if it has the specified filter status
      if (notificationToSend.getNotificationStatus().equals(NotificationStatus.PENDING)) {
        // we have to find the person with the role specified in NotificationType
        Role role = notificationToSend.getNotificationType().getNotifiedOrgUnitRole();
        List<Person> personToNotify;
        if (role != null) {
          //find all person with this role
          personToNotify = this.personService.findByOrgUnitAndRoleLevelHierarchy(notificationToSend.getNotifiedOrgUnit().getResId(), role);
        } else {
          ApplicationRole applicationRole = notificationToSend.getNotificationType().getNotifiedApplicationRole();
          List<User> usersToSend = this.userService.findByApplicationRoleLevelHierarchy(applicationRole);
          personToNotify = usersToSend.stream().map(User::getPerson).toList();
        }
        for (Person person : personToNotify) {
          notificationsByPersonId.computeIfAbsent(person.getResId(), s -> new ArrayList<>());
          notificationsByPersonId.get(person.getResId()).add(notificationToSend);
        }
      }
    }
    return notificationsByPersonId;
  }

  /**
   * Group notifications by person and keep them only if the related deposit has still the given status. (we don't send a notification if
   * the deposit status has changed since the notification was created as it wouldn't make sense anymore).
   *
   * @param notificationsToSend
   * @return
   */
  private Map<String, List<Notification>> getFilteredNotificationsByRecipient(List<Notification> notificationsToSend) {
    Map<String, List<Notification>> notificationsByPersonId = new HashMap<>();
    for (Notification notificationToSend : notificationsToSend) {

      // keep the notification only if it has the specified filter status
      if (notificationToSend.getNotificationStatus().equals(NotificationStatus.PENDING)
              || notificationToSend.getNotificationStatus().equals(NotificationStatus.NON_APPLICABLE)) {
        String recipientId = notificationToSend.getRecipient().getResId();
        notificationsByPersonId.computeIfAbsent(recipientId, s -> new ArrayList<>());
        notificationsByPersonId.get(recipientId).add(notificationToSend);
      }
    }
    return notificationsByPersonId;
  }

  /**
   * Create and send an email to the person if he has subscribed to the notification type, with resource ids linked to the notifications.
   * The sending process uses a JMS queue.
   *
   * @param personId
   * @param notifications
   * @param notificationType
   * @param emailTemplate
   */
  private void sendEmailWithResourceIdsIfUserHasSubscribedToNotificationType(String personId, List<Notification> notifications,
          NotificationType notificationType, EmailMessage.EmailTemplate emailTemplate) {

    if (this.personService.hasSubscribedToNotificationType(personId, notificationType)) {

      // validator's email
      User recipientUser = this.userService.findByPersonResId(personId);

      // compute a list of unique deposits ids to notify (to not include the same deposit more than once in an email)
      HashMap<String, Object> parameters = new HashMap<>();
      notifications.stream().map(Notification::getObjectId).distinct().forEach(n -> parameters.put(n, null));

      this.sendEmailAndMarkNotificationAsSent(notifications, emailTemplate, recipientUser, parameters);
    }
  }

  /**
   * Create and send an email to the person if he has subscribed to the notification type, with org unit id and the emitter linked to the notifications.
   * The sending process uses a JMS queue.
   *
   * @param personId
   * @param notifications
   * @param notificationType
   * @param emailTemplate
   */
  private void sendEmailWithResourceIdAndEmitterIfUserHasSubscribedToNotificationType(String personId, List<Notification> notifications,
          NotificationType notificationType, EmailMessage.EmailTemplate emailTemplate) {

    if (this.personService.hasSubscribedToNotificationType(personId, notificationType)) {

      // validator's email
      User recipientUser = this.userService.findByPersonResId(personId);

      HashMap<String, Object> parameters = new HashMap<>();
      for (Notification n : notifications) {
        parameters.put(n.getObjectId(), n.getEmitter().getPerson().getResId());
      }

      this.sendEmailAndMarkNotificationAsSent(notifications, emailTemplate, recipientUser, parameters);
    }
  }

  private void sendEmailWithEmitterIdAndResourceIdIfUserHasSubscribedToNotificationType(String personId, List<Notification> notifications,
          NotificationType notificationType, EmailMessage.EmailTemplate emailTemplate) {

    if (this.personService.hasSubscribedToNotificationType(personId, notificationType)) {
      User recipientUser = this.userService.findByPersonResId(personId);
      HashMap<String, Object> parameters = new HashMap<>();
      for (Notification n : notifications) {
        parameters.put(n.getEmitter().getPerson().getResId(), n.getObjectId());
      }
      this.sendEmailAndMarkNotificationAsSent(notifications, emailTemplate, recipientUser, parameters);
    } else {
      // Need to mark notification as sent, otherwise, it is going to be checked every time the job runs.
      OffsetDateTime sentTime = OffsetDateTime.now();
      for (Notification notification : notifications) {
        this.notificationService.updateSentTime(notification.getResId(), sentTime);
        log.info("notification {} sentTime updated with value {}", notification.getResId(), sentTime);
      }
    }

  }

  /**
   * Create and send an email to the person if he has subscribed to the notification type, with org unit id and the emitter linked to the notifications.
   * The sending process uses a JMS queue.
   *
   * @param personId
   * @param notifications
   * @param notificationType
   * @param emailTemplate
   */
  private void sendEmailWithResourceIdAndEmitterAndOrgUnitIfUserHasSubscribedToNotificationType(String personId,
          List<Notification> notifications,
          NotificationType notificationType, EmailMessage.EmailTemplate emailTemplate) {

    if (this.personService.hasSubscribedToNotificationType(personId, notificationType)) {

      // validator's email
      User recipientUser = this.userService.findByPersonResId(personId);

      String[] uniqueOrgUnits = notifications.stream().map(notification -> notification.getNotifiedOrgUnit().getResId()).distinct()
              .toArray(String[]::new);
      HashMap<String, Object> parameters = new HashMap<>();
      for (String orgUnit : uniqueOrgUnits) {
        HashMap<String, String> personRoleParams = new HashMap<>();
        //send for each org unit, the personId and the objectId implicated
        notifications.stream().filter(n -> n.getNotifiedOrgUnit().getResId().equals(orgUnit))
                .forEach(no -> personRoleParams.put(no.getEmitter().getPerson().getResId(), no.getObjectId()));
        parameters.put(orgUnit, personRoleParams);
      }

      this.sendEmailAndMarkNotificationAsSent(notifications, emailTemplate, recipientUser, parameters);
    }
  }

  public void notifyResourceByRole(List<Notification> notificationsToSend, NotificationType notificationType,
          EmailMessage.EmailTemplate template) {
    Map<String, List<Notification>> notificationsByPersonId = this
            .getFilteredNotificationsByPersonIdAndByRoleHierarchy(notificationsToSend);

    for (Map.Entry<String, List<Notification>> entry : notificationsByPersonId.entrySet()) {
      this.sendEmailWithResourceIdsIfUserHasSubscribedToNotificationType(entry.getKey(), entry.getValue(),
              notificationType, template);
    }
  }

  public void notifyResourceByRecipient(List<Notification> notificationsToSend, NotificationType notificationType,
          EmailMessage.EmailTemplate template) {
    Map<String, List<Notification>> notificationsByPersonId = this
            .getFilteredNotificationsByRecipient(notificationsToSend);

    for (Map.Entry<String, List<Notification>> entry : notificationsByPersonId.entrySet()) {
      this.sendEmailWithResourceIdsIfUserHasSubscribedToNotificationType(entry.getKey(), entry.getValue(),
              notificationType, template);
    }
  }

  public void notifyResourceWithEmitterByRole(List<Notification> notificationsToSend, NotificationType notificationType,
          EmailMessage.EmailTemplate template) {
    Map<String, List<Notification>> notificationsByPersonId = this
            .getFilteredNotificationsByPersonIdAndByRoleHierarchy(notificationsToSend);

    for (Map.Entry<String, List<Notification>> entry : notificationsByPersonId.entrySet()) {
      this.sendEmailWithResourceIdAndEmitterIfUserHasSubscribedToNotificationType(entry.getKey(), entry.getValue(),
              notificationType, template);
    }
  }

  public void notifyResourceWithMultipleEmitterByRole(List<Notification> notificationsToSend, NotificationType notificationType,
          EmailMessage.EmailTemplate template) {
    Map<String, List<Notification>> notificationsByPersonId = this
            .getFilteredNotificationsByPersonIdAndByRoleHierarchy(notificationsToSend);

    for (Map.Entry<String, List<Notification>> entry : notificationsByPersonId.entrySet()) {
      this.sendEmailWithEmitterIdAndResourceIdIfUserHasSubscribedToNotificationType(entry.getKey(), entry.getValue(),
              notificationType, template);
    }
  }

  public void notifyResourceWithEmitterAndOrgUnitByRole(List<Notification> notificationsToSend, NotificationType notificationType,
          EmailMessage.EmailTemplate template) {
    Map<String, List<Notification>> notificationsByPersonId = this
            .getFilteredNotificationsByPersonIdAndByRoleHierarchy(notificationsToSend);

    for (Map.Entry<String, List<Notification>> entry : notificationsByPersonId.entrySet()) {
      this.sendEmailWithResourceIdAndEmitterAndOrgUnitIfUserHasSubscribedToNotificationType(entry.getKey(), entry.getValue(),
              notificationType, template);
    }
  }

  protected void sendEmailAndMarkNotificationAsSent(List<Notification> notifications, EmailMessage.EmailTemplate emailTemplate,
          User recipientUser, Map<String, Object> parameters) {
    // send email
    SolidifyEventPublisher.getPublisher().publishEvent(new EmailMessage(recipientUser.getEmail(), emailTemplate, parameters));
    // mark all notifications as sent
    OffsetDateTime sentTime = OffsetDateTime.now();
    for (Notification notification : notifications) {
      this.notificationService.updateSentTime(notification.getResId(), sentTime);
      log.info("notification {} sentTime updated with value {}", notification.getResId(), sentTime);
    }
  }
}

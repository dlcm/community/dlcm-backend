/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - AdminEmailProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import static ch.dlcm.message.EmailMessage.EmailTemplate.REQUEST_TO_ACCESS_DATASET_REFUSED;
import static ch.dlcm.message.EmailMessage.EmailTemplate.REQUEST_TO_CREATE_ORG_UNIT_REFUSED;
import static ch.dlcm.message.EmailMessage.EmailTemplate.REQUEST_TO_DISPOSE_ARCHIVE_ACCEPTED;
import static ch.dlcm.message.EmailMessage.EmailTemplate.REQUEST_TO_JOIN_ORG_UNIT_REFUSED;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import jakarta.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.model.EmailParameters;
import ch.unige.solidify.service.EmailService;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.OrganizationalUnitService;
import ch.dlcm.business.PersonService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AdminController;
import ch.dlcm.message.EmailMessage;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;

@Service
@ConditionalOnBean(AdminController.class)
@Profile("email-service")
public class AdminEmailProcessingService extends EmailProcessingService {
  private static final Logger log = LoggerFactory.getLogger(AdminEmailProcessingService.class);

  private final OrganizationalUnitService organizationalUnitService;
  private final PersonService personService;
  private final TrustedArchivalInfoPackageRemoteResourceService archivalInfoPackageRemoteResourceService;
  private static final String ORG_UNITS_CONSTANT = "organizationalUnits";

  protected AdminEmailProcessingService(MessageService messageService, EmailService emailService,
          DLCMProperties dlcmProperties, OrganizationalUnitService organizationalUnitService, PersonService personService,
          TrustedArchivalInfoPackageRemoteResourceService archivalInfoPackageRemoteResourceService) {
    super(messageService, emailService, dlcmProperties);
    this.organizationalUnitService = organizationalUnitService;
    this.personService = personService;
    this.archivalInfoPackageRemoteResourceService = archivalInfoPackageRemoteResourceService;
  }

  @Override
  public void processEmailMessage(EmailMessage emailMessage) {
    log.info("Reading message by Admin module {}", emailMessage);
    Map<String, Object> parameterList;
    try {
      switch (emailMessage.getTemplate()) {
        case NEW_SUBSCRIPTION, REMOVE_SUBSCRIPTION -> {
          parameterList = this.getEmailDefaultParameters();
          parameterList.put("notificationTypes", emailMessage.getParameters().get(DLCMConstants.NOTIFICATION_IDS));
          EmailParameters emailParameters = new EmailParameters().setToList(Collections.singletonList(emailMessage.getTo()))
                  .setSubject(emailMessage.getTemplate().getSubject()).setTemplate(emailMessage.getTemplate().toString().toLowerCase())
                  .setTemplateParameters(parameterList).addCc(null).addBcc(null);
          this.emailService.sendEmailWithTemplate(emailParameters);
        }
        case NEW_ORG_UNIT_TO_APPROVE -> {
          parameterList = this.getEmailDefaultParameters();
          parameterList.put(ORG_UNITS_CONSTANT, emailMessage.getParameters());
          EmailParameters emailParameters = new EmailParameters().setToList(Collections.singletonList(emailMessage.getTo()))
                  .setSubject(emailMessage.getTemplate().getSubject()).setTemplate(emailMessage.getTemplate().toString().toLowerCase())
                  .setTemplateParameters(parameterList).addCc(null).addBcc(null);
          this.emailService.sendEmailWithTemplate(emailParameters);
        }
        case NEW_MEMBER_ORG_UNIT_TO_APPROVE -> {
          parameterList = this.getEmailDefaultParameters();
          parameterList.put(ORG_UNITS_CONSTANT, emailMessage.getParameters());
          this.sendEmailWithOrgUnitsName(emailMessage);
        }
        case REQUEST_TO_CREATE_ORG_UNIT_ACCEPTED, REQUEST_TO_CREATE_ORG_UNIT_REFUSED -> {
          parameterList = this.getEmailDefaultParameters();
          OrganizationalUnit one = this.organizationalUnitService.findByName(emailMessage.getParameters().get("objectId").toString());
          parameterList.put("orgunit", one);
          if (emailMessage.getTemplate().equals(REQUEST_TO_CREATE_ORG_UNIT_REFUSED)) {
            parameterList.put(DLCMConstants.REASON, emailMessage.getParameters().get(DLCMConstants.REASON).toString());
          }

          EmailParameters emailParameters = new EmailParameters().setToList(Collections.singletonList(emailMessage.getTo()))
                  .setSubject(emailMessage.getTemplate().getSubject()).setTemplate(emailMessage.getTemplate().toString().toLowerCase())
                  .setTemplateParameters(parameterList).addCc(null).addBcc(null);
          this.emailService.sendEmailWithTemplate(emailParameters);
        }
        case REQUEST_TO_JOIN_ORG_UNIT_ACCEPTED, REQUEST_TO_JOIN_ORG_UNIT_REFUSED -> {
          parameterList = this.getEmailDefaultParameters();
          OrganizationalUnit one = this.organizationalUnitService.findOne(emailMessage.getParameters().get("objectId").toString());
          parameterList.put("orgunit", one);
          if (emailMessage.getTemplate().equals(REQUEST_TO_JOIN_ORG_UNIT_REFUSED)) {
            parameterList.put(DLCMConstants.REASON, emailMessage.getParameters().get(DLCMConstants.REASON).toString());
          }
          EmailParameters emailParameters = new EmailParameters().setToList(Collections.singletonList(emailMessage.getTo()))
                  .setSubject(emailMessage.getTemplate().getSubject()).setTemplate(emailMessage.getTemplate().toString().toLowerCase())
                  .setTemplateParameters(parameterList).addCc(null).addBcc(null);
          this.emailService.sendEmailWithTemplate(emailParameters);
        }
        case REQUEST_TO_ACCESS_DATASET_ACCEPTED,
             REQUEST_TO_ACCESS_DATASET_REFUSED,
             REQUEST_TO_DISPOSE_ARCHIVE_ACCEPTED-> {
          parameterList = this.getEmailDefaultParameters();
          ArchivalInfoPackage one = this.archivalInfoPackageRemoteResourceService.findOne(emailMessage.getParameters().get("objectId").toString());
          parameterList.put("aip", one);
          if (emailMessage.getTemplate().equals(REQUEST_TO_ACCESS_DATASET_REFUSED)) {
            parameterList.put(DLCMConstants.REASON, emailMessage.getParameters().get(DLCMConstants.REASON).toString());
          }
          EmailParameters emailParameters = new EmailParameters().setToList(Collections.singletonList(emailMessage.getTo()))
                  .setSubject(emailMessage.getTemplate().getSubject()).setTemplate(emailMessage.getTemplate().toString().toLowerCase())
                  .setTemplateParameters(parameterList).addCc(null).addBcc(null);
          this.emailService.sendEmailWithTemplate(emailParameters);
        }
      }
    } catch (MessagingException e) {
      log.error("Cannot send email ({}): {}", emailMessage.getTemplate().getSubject(), e.getMessage(), e);
    }
  }

  private void sendEmailWithOrgUnitsName(EmailMessage emailMessage) throws MessagingException {
    Map<String, Object> parameterList;

    parameterList = this.getEmailDefaultParameters();
    parameterList.put(ORG_UNITS_CONSTANT, emailMessage.getParameters());
    List<String> orgUnitNames = new ArrayList<>();
    List<String> personNames = new ArrayList<>();
    for (String orgUnitId : emailMessage.getParameters().keySet()) {
      orgUnitNames.add(this.organizationalUnitService.findOne(orgUnitId).getName());
      Map<String, String> personRoleParams = (Map<String, String>) emailMessage.getParameters().get(orgUnitId);
      personRoleParams.keySet().forEach(pr -> personNames.add(this.personService.findOne(pr).getFullName()));
    }
    parameterList.put("orgUnitNames", orgUnitNames);
    parameterList.put("personNames", personNames);

    EmailParameters emailParameters = new EmailParameters().setToList(Collections.singletonList(emailMessage.getTo()))
            .setSubject(emailMessage.getTemplate().getSubject()).setTemplate(emailMessage.getTemplate().toString().toLowerCase())
            .setTemplateParameters(parameterList).addCc(null).addBcc(null);

    this.emailService.sendEmailWithTemplate(emailParameters);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Common - NotificationProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.message.NotificationMessage;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationMark;
import ch.dlcm.service.rest.fallback.FallbackNotificationRemoteResourceService;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import java.util.Comparator;

@Service
public class NotificationProcessingService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(NotificationProcessingService.class);

  private final FallbackNotificationRemoteResourceService notificationRemoteResourceService;

  protected NotificationProcessingService(MessageService messageService, DLCMProperties dlcmProperties,
          FallbackNotificationRemoteResourceService notificationService) {
    super(messageService, dlcmProperties);
    this.notificationRemoteResourceService = notificationService;
  }

  @JmsListener(destination = "${dlcm.queue.notification}")
  public void processNotificationService(NotificationMessage notificationMessage) {
    log.trace("Reading message {}", notificationMessage);
    RestCollection<Notification> notificationList = this.notificationRemoteResourceService.findByObjectIdAndNotificationType(
            notificationMessage.getResId(), notificationMessage.getNotificationTypeId());
    if (!notificationList.getData().isEmpty()) {
      //Get latest notification
      Notification notification = notificationList.getData().stream().max(Comparator.comparing(n -> n.getLastUpdate().getWhen())).orElseThrow();
      notification.setNotificationStatus(notificationMessage.getNotificationStatus());
      notification.setNotificationMark(NotificationMark.READ);
      this.notificationRemoteResourceService.updateNotification(notification);
      log.trace("Notification {} status changed to {}", notificationMessage.getResId(), notificationMessage.getNotificationStatus());
    }
  }
}

= DLCM Backend

image::DLCM-logo.png[DLCM Technology, 100]

DLCM-Backend is the backend part of the _DLCM_ technology, an archiving solution based on the http://www.oais.info/[OAIS] model (ISO 14721).

== Prerequisites

The DLCM technology was developed with the _Solidify_ framework, developed by the _UNIGE_, extended from https://spring.io/projects/spring-boot[Spring Boot].

In order to build the DLCM-Backend project, you need to build the following dependencies first with their corresponding versions:

* solidify : https://gitlab.unige.ch/solidify/solidify-backend
* solidify-authorization : https://gitlab.unige.ch/solidify/solidify-authorization
* solidify-tools : https://gitlab.unige.ch/solidify/solidify-tools
* dlcm-portal : https://gitlab.unige.ch/dlcm/ui/dlcm-portal

If you are inside the _UNIGE_ network, you do not need to build these two projects. If you are outside the _UNIGE_ network, then you will have to pull and build the two dependencies.

== Building project


To build the project, run this _Maven_ command

*  If you are inside the _UNIGE_ network:
+
[source,sh]
----
mvn clean package
----

* If you are outside the _UNIGE_ Network:
+
[source,sh]
----
mvn clean package -P public
----

=== For Windows users

Add the `dlcm.storage.url` property in the link:DLCM-Solution\src\main\resources\bootstrap.yml[DLCM-Backend-Solution.yml] file by setting a valid path. Example for Windows: `file:/C:/Users/<user name>/storage`

=== Generating documentation and the open api specification file

After building the projects, you should see the generated documentation in the `DLCM-Solution/target` directory and the open api documentation in `DLCM-Solution/target/classes/static`.

If you want to customize information in the generated doc (url, baseapi, servlet name…), you can edit properties in link:pom.xml[pom.xml] parent:

[source,xml]
....
<documentation.host>localhost</documentation.host>
<documentation.basePath>/</documentation.basePath>
<documentation.specification>OPENAPI_V3</documentation.specification>
<documentation.scheme>http</documentation.scheme>
<documentation.server.servlet.context-path>/dlcm</documentation.server.servlet.context-path>
<documentation.format>JSON</documentation.format>
....

=== Skipping copyright check

To run a build without the copyright verification, run this _Maven_ command

[source,sh]
----
mvn <goals> -Dlicense.skipCheckLicense
----

=== Adding copyright

If missing copyright in new source files, to add the copyright in these file headers, run this _Maven_ command

[source,sh]
----
mvn license:update-file-header
----

== Contributing to project

Please read our link:CONTRIBUTING.adoc[contributing guidelines] before contributing to the project.


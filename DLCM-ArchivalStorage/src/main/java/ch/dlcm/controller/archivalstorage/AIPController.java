/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - AIPController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.archivalstorage;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.IOException;
import java.io.InputStream;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.business.DataFileService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AbstractPackageController;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.message.NotificationMessage;
import ch.dlcm.model.AipCheckLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.AipStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.DataCategoryService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.propagate.PropagateMetadataTypeRemoteResourceService;
import ch.dlcm.storage.StorageService;

@RestController
@ConditionalOnBean(ArchivalStorageController.class)
@RequestMapping(UrlPath.ARCHIVAL_STORAGE_AIP)
public class AIPController extends AbstractPackageController<AipDataFile, ArchivalInfoPackage> {

  private static final Logger log = LoggerFactory.getLogger(AIPController.class);

  private static final String AIP_IN_PROGRESS = "message.package.inprogress";

  protected final StorageService storageService;
  protected final String archivalTempLocation;

  public AIPController(DLCMProperties dlcmProperties, HistoryService historyService, MetadataService metadataService,
          PropagateMetadataTypeRemoteResourceService metadataTypeService, DataFileService<AipDataFile> dataFileService,
          DataCategoryService dataCategoryService, StorageService storageService) {
    super(historyService, metadataService, metadataTypeService, dataFileService, dataCategoryService);
    this.storageService = storageService;
    this.archivalTempLocation = dlcmProperties.getTempLocation(dlcmProperties.getArchivalLocation());
  }

  @Override
  @PreAuthorize("@archivalInfoPackagePermissionService.isAllowedToCreate(#aip)")
  public HttpEntity<ArchivalInfoPackage> create(@RequestBody ArchivalInfoPackage aip) {
    return super.create(aip);
  }

  @Override
  @PreAuthorize("@archivalInfoPackagePermissionService.isAllowed(#aipId, 'GET')"
          + " || @embargoPermissionService.isPublic(#aipId, 'ArchivalInfoPackage')"
          + " || @downloadWithAclPermissionService.isAllowed(#aipId)")
  public HttpEntity<ArchivalInfoPackage> get(@PathVariable("id") String aipId) {
    return super.get(aipId);
  }

  @Override
  // Same permission level as for DLCM 2.0
  // Todo: restrict access: see issue DLCM-1851
  @UserPermissions
  public HttpEntity<RestCollection<ArchivalInfoPackage>> list(@ModelAttribute ArchivalInfoPackage search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @PreAuthorize("@archivalInfoPackagePermissionService.isAllowedToUpdate(#aipId)")
  public HttpEntity<ArchivalInfoPackage> update(@PathVariable("id") String aipId, @RequestBody Map<String, Object> updateMap) {
    return super.update(aipId, updateMap);
  }

  @Override
  @AdminPermissions
  public ResponseEntity<Void> delete(@PathVariable("id") String aipId) {
    // Check if AIP exists
    final ArchivalInfoPackage aip = this.itemService.findOne(aipId);
    if (aip.isInProgress() || !aip.isDeletable()) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
    aip.setPackageStatus(PackageStatus.DISPOSABLE);
    this.itemService.save(aip);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @Override
  @AdminPermissions
  public ResponseEntity<Void> deleteList(@RequestBody String[] aipIds) {
    return super.deleteList(aipIds);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.REPLICATE_TOMBSTONE)
  @TrustedUserPermissions
  public HttpEntity<Result> replicateTombstone(
          @PathVariable("id") String aipId,
          @RequestParam(value = "lastArchiving") @DateTimeFormat(iso = ISO.DATE_TIME) OffsetDateTime lastArchiving) {
    final Result result = this.replicatePackage(aipId, lastArchiving, PackageStatus.TOMBSTONE_REPLICATION_PENDING);
    result.add(linkTo(this.getClass()).slash(aipId).slash(DLCMActionName.REPLICATE_TOMBSTONE).withSelfRel());
    result.add(linkTo(this.getClass()).slash(aipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.REPLICATE_PACKAGE)
  @TrustedUserPermissions
  public HttpEntity<Result> replicatePackage(
          @PathVariable("id") String aipId,
          @RequestParam(value = "lastArchiving") @DateTimeFormat(iso = ISO.DATE_TIME) OffsetDateTime lastArchiving) {
    final Result result = this.replicatePackage(aipId, lastArchiving, PackageStatus.PACKAGE_REPLICATION_PENDING);
    result.add(linkTo(this.getClass()).slash(aipId).slash(DLCMActionName.REPLICATE_PACKAGE).withSelfRel());
    result.add(linkTo(this.getClass()).slash(aipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.APPROVE_DISPOSAL)
  @AdminPermissions
  public HttpEntity<Result> approveDisposal(@PathVariable("id") String aipId,
          @RequestParam(value = DLCMConstants.REASON) String reason) {
    final Result result = this.approveDisposalPackage(aipId, PackageStatus.DISPOSAL_APPROVED, reason);
    result.add(linkTo(this.getClass()).slash(aipId).slash(DLCMActionName.APPROVE_DISPOSAL).withSelfRel());
    result.add(linkTo(this.getClass()).slash(aipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.APPROVE_DISPOSAL_BY_ORGUNIT)
  @PreAuthorize("hasAnyAuthority('"
          + AuthApplicationRole.ADMIN_ID + "', '"
          + AuthApplicationRole.TRUSTED_CLIENT_ID + "', '"
          + AuthApplicationRole.ROOT_ID
          + "') or @archivalInfoPackagePermissionService.isAllowed(#aipId,'APPROVE_DISPOSAL_BY_ORGUNIT')")
  public HttpEntity<Result> approveDisposalByOrgunit(@PathVariable("id") String aipId,
          @RequestParam(value = DLCMConstants.REASON) String reason) {
    final Result result = this.approveDisposalPackage(aipId, PackageStatus.DISPOSAL_APPROVED_BY_ORGUNIT, reason);
    result.add(linkTo(this.getClass()).slash(aipId).slash(DLCMActionName.APPROVE_DISPOSAL_BY_ORGUNIT).withSelfRel());
    result.add(linkTo(this.getClass()).slash(aipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.EXTEND_RETENTION)
  @PreAuthorize("hasAnyAuthority('"
          + AuthApplicationRole.ADMIN_ID + "', '"
          + AuthApplicationRole.TRUSTED_CLIENT_ID + "', '"
          + AuthApplicationRole.ROOT_ID
          + "') or @archivalInfoPackagePermissionService.isAllowed(#aipId, 'EXTEND_RETENTION')")
  public HttpEntity<Result> extendRetention(@PathVariable("id") String aipId, @RequestParam(value = "duration") Integer durationInDays) {
    // Check if AIP exists
    final ArchivalInfoPackage aip = this.itemService.findOne(aipId);
    final Result result = new Result(aip.getResId());
    if (aip.isInProgress()) {
      result.setMesssage(this.messageService.get(AIP_IN_PROGRESS, new Object[] { aipId }));
    } else {
      result.setStatus(ActionStatus.NOT_EXECUTED);
      try {
        if (aip.getInfo().getStatus() == PackageStatus.DISPOSABLE) {
          // update status
          aip.setPackageStatus(PackageStatus.UPDATING_RETENTION);
          // update retention
          aip.setRetention(aip.getRetention() + durationInDays);
          this.itemService.save(aip);
          result.setStatus(ActionStatus.EXECUTED);
          result.setMesssage(this.messageService.get("message.package.retention", new Object[] { aipId }));
        } else {
          result.setMesssage(this.messageService.get("message.package.not_disposable", new Object[] { aipId }));
        }
      } catch (SolidifyUndeletableException e) {
        result.setMesssage(e.getMessage());
      }
    }
    if (result.getStatus() == ActionStatus.EXECUTED) {
      ((ArchivalInfoPackageService) this.itemService).putPackageInProcessingQueue(aip);
    }
    result.add(linkTo(this.getClass()).slash(aipId).slash(DLCMActionName.EXTEND_RETENTION).withSelfRel());
    result.add(linkTo(this.getClass()).slash(aipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.CHECK)
  @AdminPermissions
  // Check if AIP exists
  public HttpEntity<Result> check(@PathVariable("id") String aipId) {
    return this.startCheckAction(DLCMActionName.CHECK, aipId, AipCheckLevel.DEEP_CHECK, "message.package.checking");
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.CHECK_FIXITY)
  @AdminPermissions
  public HttpEntity<Result> checkFixity(@PathVariable("id") String aipId,
          @RequestParam(name = "level", defaultValue = "") String checkFixityLevel) {
    AipCheckLevel aipCheckLevel = this.storageService.getDefaultFixityCheckLevel();
    if (!StringTool.isNullOrEmpty(checkFixityLevel)) {
      try {
        aipCheckLevel = AipCheckLevel.valueOf(checkFixityLevel.toUpperCase());
      } catch (final IllegalArgumentException exception) {
        log.warn("invalid FixityCheckLevel '{}'", checkFixityLevel);
      }
    }
    return this.startCheckAction(DLCMActionName.CHECK_FIXITY, aipId, aipCheckLevel, "message.fixity.check.started");
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.FIX_AIP_INFO)
  @RootPermissions
  public HttpEntity<Result> fixAipInfo(@PathVariable("id") String aipId) {
    final ArchivalInfoPackage aip = this.itemService.findOne(aipId);
    final Result result = new Result(aip.getResId());
    if (!aip.isInError()) {
      result.setMesssage(this.messageService.get("message.package.not_in_error", new Object[] { aipId }));
    } else {
      aip.setPackageStatus(PackageStatus.FIX_PENDING);
      this.itemService.save(aip);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get(this.messageService.get("message.fix-aip-info")));
    }
    if (result.getStatus() == ActionStatus.EXECUTED) {
      ((ArchivalInfoPackageService) this.itemService).putPackageInCheckingQueue(aip, AipCheckLevel.FIX_AIP_INFO,
              this.getAuthenticatedUserExternalUid());
    }
    result.add(linkTo(this.getClass()).slash(aipId).slash(DLCMActionName.FIX_AIP_INFO).withSelfRel());
    result.add(linkTo(this.getClass()).slash(aipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  private HttpEntity<Result> startCheckAction(String action, String aipId, AipCheckLevel aipCheckLevel, String actionMessage) {
    final ArchivalInfoPackage aip = this.itemService.findOne(aipId);
    final Result result = new Result(aip.getResId());
    if (aip.isInProgress()) {
      result.setMesssage(this.messageService.get(AIP_IN_PROGRESS, new Object[] { aipId }));
    } else {
      // Set to status to trigger check process
      if (action.equals(DLCMActionName.CHECK)) {
        aip.setPackageStatusWithMessage(PackageStatus.CHECK_PENDING, this.messageService.get("message.check.archive"));
      } else if (action.equals(DLCMActionName.CHECK_FIXITY)) {
        aip.setPackageStatusWithMessage(PackageStatus.CHECK_PENDING, this.messageService.get("message.check.fixity"));
      } else {
        aip.setPackageStatus(PackageStatus.CHECK_PENDING);
      }
      this.itemService.save(aip);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get(actionMessage, new Object[] { aipId }));
    }
    // Send JMS message
    if (result.getStatus() == ActionStatus.EXECUTED) {
      ((ArchivalInfoPackageService) this.itemService).putPackageInCheckingQueue(aip, aipCheckLevel, this.getAuthenticatedUserExternalUid());
    }
    result.add(linkTo(this.getClass()).slash(aipId).slash(action).withSelfRel());
    result.add(linkTo(this.getClass()).slash(aipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.PUT_IN_ERROR)
  @RootPermissions
  public HttpEntity<Result> putInError(@PathVariable("id") String aipId) {
    final ArchivalInfoPackage aip = this.itemService.findOne(aipId);
    final Result result = new Result(aip.getResId());
    if (aip.isCompleted()) {
      result.setMesssage(this.messageService.get("message.package.completed", new Object[] { aipId }));
    } else {
      // Set to status to trigger check process
      aip.setPackageStatus(PackageStatus.IN_ERROR);
      this.itemService.save(aip);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.package.in_error", new Object[] { aipId }));
    }
    result.add(linkTo(this.getClass()).slash(aipId).slash(DLCMActionName.PUT_IN_ERROR).withSelfRel());
    result.add(linkTo(this.getClass()).slash(aipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PreAuthorize("hasAnyAuthority('TRUSTED_CLIENT', 'ROOT') " +
          "|| @downloadTokenPermissionService.isAllowed(#aipId, T(ch.dlcm.model.security.DownloadTokenType).AIP)")
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD)
  public HttpEntity<StreamingResponseBody> getFiles(@PathVariable("id") String aipId) {
    final ArchivalInfoPackage item = this.itemService.findOne(aipId);
    try {
      log.info("start AIP download '{}'", aipId);
      final InputStream is = this.storageService.getAIPStream(item);
      return this.buildDownloadResponseEntity(is, aipId + SolidifyConstants.ZIP_EXT, DLCMConstants.AIP_MIME_TYPE,
              this.storageService.getAIPSize(item));
    } catch (final IOException e) {
      throw new SolidifyRuntimeException("Error downloading AIP " + aipId, e);
    }
  }

  @Override
  @PreAuthorize("@archivalInfoPackagePermissionService.isAllowed(#aipId, 'HISTORY')")
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable("id") String aipId, Pageable pageable) {
    return super.history(aipId, pageable);
  }

  @GetMapping("/" + DLCMActionName.LIST_AIP_CONTAINER)
  @EveryonePermissions
  public HttpEntity<ArchiveContainer[]> listAipContainer() {
    return new ResponseEntity<>(ArchiveContainer.values(), HttpStatus.OK);
  }

  @GetMapping("/" + DLCMActionName.LIST_STATUS)
  @EveryonePermissions
  public HttpEntity<PackageStatus[]> listStatus() {
    return new ResponseEntity<>(PackageStatus.values(), HttpStatus.OK);
  }

  @SuppressWarnings("squid:S4684")
  @GetMapping("/" + DLCMActionName.LIST_AIP_WITH_STATUS)
  @AdminPermissions
  public HttpEntity<RestCollection<AipStatus>> listStatuses(@ModelAttribute ArchivalInfoPackage search, Pageable pageable) {

    final Specification<ArchivalInfoPackage> spec = this.itemService.getSpecification(search);
    final Page<ArchivalInfoPackage> listItems = this.itemService.findAll(spec, pageable);
    final List<AipStatus> aipStatuses = new ArrayList<>();
    for (final ArchivalInfoPackage aip : listItems) {
      aipStatuses.add(new AipStatus(aip));
      this.addLinks(aip);
    }

    final Page<AipStatus> aipStatusesPage = new PageImpl<>(aipStatuses, pageable, listItems.getTotalElements());

    final RestCollection<AipStatus> collection = new RestCollection<>(aipStatusesPage, pageable);
    collection.add(linkTo(this.getClass()).slash(DLCMActionName.LIST_AIP_WITH_STATUS).withSelfRel());

    collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder()).withRel(ActionName.MODULE));

    this.addSortLinks(linkTo(this.getClass()).slash(DLCMActionName.LIST_AIP_WITH_STATUS), collection);
    this.addPageLinks(linkTo(this.getClass()).slash(DLCMActionName.LIST_AIP_WITH_STATUS), collection, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.REINDEX)
  @AdminPermissions
  public HttpEntity<Result> reindex(@PathVariable("id") String aipId) {
    final Result result = this.reindexPackage(aipId);
    result.add(linkTo(this.getClass()).slash(aipId).slash(DLCMActionName.REINDEX).withSelfRel());
    result.add(linkTo(this.getClass()).slash(aipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.RESUME)
  @AdminPermissions
  public HttpEntity<Result> resume(@PathVariable("id") String aipId) {
    final Result result = ((ArchivalInfoPackageService) this.itemService).resumePackage(aipId, this.storageService.getDefaultFixityCheckLevel(),
            this.getAuthenticatedUserExternalUid());
    result.add(linkTo(this.getClass()).slash(aipId).slash(ActionName.RESUME).withSelfRel());
    result.add(linkTo(this.getClass()).slash(aipId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @GetMapping("/" + SolidifyConstants.SCHEMA)
  @EveryonePermissions
  public HttpEntity<StreamingResponseBody> schema(@RequestParam(value = "version", required = false) String version) {
    return this.getSchemaStream(version, new AipDataFile());
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.UPLOAD)
  @TrustedUserPermissions
  public HttpEntity<AipDataFile> sendFile(@PathVariable("id") String aipId, @RequestParam(DLCMConstants.FILE) MultipartFile file)
          throws IOException {
    final ArchivalInfoPackage item = this.itemService.findOne(aipId);
    if (item.isCompleted() || item.getInfo().getStatus() == PackageStatus.STORED) {
      throw new SolidifyRuntimeException(this.messageService.get("archival.aip.completed", new Object[] { aipId }));
    }
    final FileUploadDto<AipDataFile> fileUploadDto = new FileUploadDto<>(new AipDataFile(item));
    fileUploadDto.setOriginalFileName(file.getOriginalFilename());
    fileUploadDto.setInputStream(file.getInputStream());
    fileUploadDto.setDataCategory(DataCategory.Package);
    fileUploadDto.setDataType(DataCategory.InformationPackage);

    return this.upload(aipId, fileUploadDto, this.archivalTempLocation, UploadMode.STANDARD);
  }

  @TrustedUserPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.START_METADATA_EDITING)
  public HttpEntity<Result> putInMetadataEditing(@PathVariable("id") String aipId) {
    return this.changeAipStatus(PackageStatus.METADATA_EDITION_PENDING, aipId);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.START_METADATA_UPGRADE)
  @TrustedUserPermissions
  public HttpEntity<Result> upgradeMetadata(@PathVariable("id") String aipId) {
    return this.changeAipStatus(PackageStatus.METADATA_UPGRADE_PENDING, aipId);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.START_COMPLIANCE_LEVEL_UPDATE)
  @TrustedUserPermissions
  public HttpEntity<Result> updateComplianceLevel(@PathVariable("id") String aipId) {
    return this.changeAipStatus(PackageStatus.COMPLIANCE_LEVEL_UPDATE_PENDING, aipId);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(Tool.filter(linkTo(this.getClass()).toUriComponentsBuilder(), DLCMConstants.ARCHIVAL_UNIT_PARAM, "true")
            .withRel(DLCMActionName.ARCHIVAL_UNITS));
    w.add(Tool.filter(linkTo(this.getClass()).toUriComponentsBuilder(), DLCMConstants.ARCHIVAL_UNIT_PARAM, "false")
            .withRel(DLCMActionName.ARCHIVAL_COLLECTIONS));
    w.add(linkTo(methodOn(this.getClass()).listStatus()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listAipContainer()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listVersions()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).schema(DLCMMetadataVersion.getDefaultVersion().getVersion())).withRel(SolidifyConstants.SCHEMA));
    w.add(linkTo(methodOn(this.getClass()).profile(DLCMMetadataVersion.getDefaultVersion().getVersion())).withRel(ResourceName.PROFILE));
  }

  private Result reindexPackage(String aipId) {
    // Check if AIP exists
    final ArchivalInfoPackage aip = this.itemService.findOne(aipId);
    final Result result = new Result(aip.getResId());
    if (aip.isInProgress()) {
      result.setMesssage(this.messageService.get(AIP_IN_PROGRESS, new Object[] { aipId }));
    } else {
      result.setStatus(ActionStatus.NOT_EXECUTED);
      if (!aip.isIndexable()) {
        result.setMesssage(this.messageService.get("message.package.datafile.notready", new Object[] { aipId }));
      } else {
        aip.setPackageStatus(PackageStatus.REINDEXING);
        this.itemService.save(aip);
        result.setStatus(ActionStatus.EXECUTED);
        result.setMesssage(this.messageService.get("message.package.reindex", new Object[] { aipId }));
      }
    }
    if (result.getStatus() == ActionStatus.EXECUTED) {
      ((ArchivalInfoPackageService) this.itemService).putPackageInProcessingQueue(aip);
    }
    return result;
  }

  private Result approveDisposalPackage(String aipId, PackageStatus status, String reason) {
    // Check if AIP exists
    final ArchivalInfoPackage aip = this.itemService.findOne(aipId);
    final Result result = new Result(aip.getResId());
    if (aip.isInProgress()) {
      result.setMesssage(this.messageService.get(AIP_IN_PROGRESS, new Object[] { aipId }));
    } else {
      result.setStatus(ActionStatus.NOT_EXECUTED);
      try {
        if (aip.getInfo().getStatus() == PackageStatus.DISPOSABLE) {
          aip.setPackageStatusWithMessage(status, reason);
          SolidifyEventPublisher.getPublisher()
                  .publishEvent(new NotificationMessage(aipId, NotificationStatus.APPROVED,
                          status.equals(PackageStatus.DISPOSAL_APPROVED) ? NotificationType.APPROVE_DISPOSAL_REQUEST.toString()
                                  : NotificationType.APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST.toString()));
          this.itemService.save(aip);
          result.setStatus(ActionStatus.EXECUTED);
          result.setMesssage(this.messageService.get("message.package.disposal", new Object[] { aipId }));
        } else {
          result.setMesssage(this.messageService.get("message.package.not_disposable", new Object[] { aipId }));
        }
      } catch (SolidifyUndeletableException e) {
        result.setMesssage(e.getMessage());
      }
    }
    if (result.getStatus() == ActionStatus.EXECUTED) {
      ((ArchivalInfoPackageService) this.itemService).putPackageInProcessingQueue(aip);
    }
    return result;
  }

  private Result replicatePackage(String aipId, OffsetDateTime lastArchiving, PackageStatus status) {
    // Check if AIP exists
    final ArchivalInfoPackage aip = this.itemService.findOne(aipId);
    final Result result = new Result(aip.getResId());
    if (aip.isInProgress()) {
      result.setMesssage(this.messageService.get(AIP_IN_PROGRESS, new Object[] { aipId }));
    } else {
      result.setStatus(ActionStatus.NOT_EXECUTED);
      if (aip.getInfo().getStatus().equals(PackageStatus.COMPLETED)) {
        aip.setPackageStatus(status);
        aip.setLastArchiving(lastArchiving);
        this.itemService.save(aip);
        result.setStatus(ActionStatus.EXECUTED);
        result.setMesssage(this.messageService.get("message.package.replicate", new Object[] { aipId }));
      } else {
        result.setMesssage(this.messageService.get("message.package.not_completed", new Object[] { aipId }));
      }
    }
    return result;
  }

  private HttpEntity<Result> changeAipStatus(PackageStatus newStatus, String aipId) {
    ArchivalInfoPackage aip = this.itemService.findOne(aipId);
    final PackageStatus currentStatus = aip.getInfo().getStatus();
    final Result result = new Result(aipId);
    if (currentStatus == PackageStatus.COMPLETED
            && (newStatus == PackageStatus.METADATA_EDITION_PENDING
                    || newStatus == PackageStatus.METADATA_UPGRADE_PENDING
                    || newStatus == PackageStatus.COMPLIANCE_LEVEL_UPDATE_PENDING)) {
      aip.getInfo().setStatus(newStatus);
      aip = this.itemService.save(aip);
      result.setStatus(Result.ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.aip.status"));
      if (newStatus == PackageStatus.METADATA_UPGRADE_PENDING
              || newStatus == PackageStatus.COMPLIANCE_LEVEL_UPDATE_PENDING) {
        ((ArchivalInfoPackageService) this.itemService).putPackageInProcessingQueue(aip);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } else {
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage(this.messageService.get("message.aip.status.error"));
      return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }

  }
}

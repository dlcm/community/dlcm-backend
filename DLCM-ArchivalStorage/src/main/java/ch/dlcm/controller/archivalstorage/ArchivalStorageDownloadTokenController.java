/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - ArchivalStorageDownloadTokenController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.archivalstorage;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.model.security.DownloadToken;
import ch.unige.solidify.repository.DownloadTokenRepository;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.security.RootPermissions;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.controller.DLCMDownloadTokenController;
import ch.dlcm.model.security.DownloadTokenType;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@RestController
@ConditionalOnBean(ArchivalStorageController.class)
public class ArchivalStorageDownloadTokenController extends DLCMDownloadTokenController {

  private final String archivalStorageModulePath;

  public ArchivalStorageDownloadTokenController(
          DownloadTokenRepository downloadTokenRepository,
          SolidifyProperties solidifyConfig,
          DLCMProperties dlcmProperties) throws URISyntaxException {
    super(solidifyConfig, downloadTokenRepository);
    this.archivalStorageModulePath = new URI(dlcmProperties.getModule().getArchivalStorage().getPublicUrls()[0]).getPath();
  }

  @GetMapping(UrlPath.ARCHIVAL_STORAGE_AIP + SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD_TOKEN)
  @RootPermissions
  public ResponseEntity<DownloadToken> getTokenForAip(@PathVariable("id") String aipId) {
    final String cookiePath = this.archivalStorageModulePath
            + "/" + ResourceName.AIP
            + "/" + aipId
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(aipId, DownloadTokenType.AIP, cookiePath);
  }

  @GetMapping(UrlPath.ARCHIVAL_STORAGE_AIP + SolidifyConstants.URL_PARENT_ID + ResourceName.DATAFILE
          + SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD_TOKEN)
  @RootPermissions
  public ResponseEntity<DownloadToken> getTokenForAipDataFile(@PathVariable String parentid, @PathVariable String id) {
    final String cookiePath = this.archivalStorageModulePath
            + "/" + ResourceName.AIP
            + "/" + parentid
            + "/" + ResourceName.DATAFILE
            + "/" + id
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(id, DownloadTokenType.AIP_DATAFILE, cookiePath);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - StoredAIPController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.archivalstorage;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.NoSqlResourceController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.service.NoSqlResourceService;

import ch.dlcm.DLCMConstants;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.storage.StorageService;

@AdminPermissions
@RestController
@ConditionalOnBean(ArchivalStorageController.class)
@RequestMapping(UrlPath.ARCHIVAL_STORAGE_STORED_AIP)
public class StoredAIPController extends NoSqlResourceController<StoredAIP> {

  private final StorageService storageService;

  public StoredAIPController(NoSqlResourceService<StoredAIP> noSqlResourceService, StorageService storageService) {
    super(noSqlResourceService);
    this.storageService = storageService;
  }

  @Override
  public HttpEntity<StoredAIP> create(@RequestBody StoredAIP t) {
    return super.create(t);
  }

  @Override
  public HttpEntity<StoredAIP> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<StoredAIP>> list(@ModelAttribute StoredAIP search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<StoredAIP> update(@PathVariable String id, @RequestBody StoredAIP t2) {
    return super.update(id, t2);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD)
  @PreAuthorize("@downloadTokenPermissionService.isAllowed(#id, T(ch.dlcm.model.security.DownloadTokenType).STORED_AIP)")
  public HttpEntity<StreamingResponseBody> getFiles(@PathVariable String id) {
    final StoredAIP item = this.noSqlResourceService.findOne(id);
    if (item == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    try {
      final InputStream is = this.storageService.getAIPStream(item);
      return this.buildDownloadResponseEntity(is, id + SolidifyConstants.ZIP_EXT, DLCMConstants.AIP_MIME_TYPE,
              this.storageService.getAIPSize(item));
    } catch (final IOException e) {
      throw new SolidifyRuntimeException("Error during download of Stored AIP " + id, e);
    }
  }

}

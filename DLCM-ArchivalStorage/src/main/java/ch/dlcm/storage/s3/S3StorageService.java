/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - S3StorageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.s3;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3Configuration;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Exception;
import software.amazon.awssdk.services.s3.model.S3Object;
import software.amazon.awssdk.utils.IoUtils;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMProperties.Storage;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.exception.FixityCheckLevelNotImplementedException;
import ch.dlcm.model.AipCheckLevel;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StoredArchiveInterface;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicDataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.storage.StorageService;

@Profile("arc-s3")
@Service
@ConditionalOnBean(ArchivalStorageController.class)
public class S3StorageService extends StorageService {
  private static final String INVALID_URL = "Invalid S3 Virtual Hosted–Style URL: ";
  private static final Logger log = LoggerFactory.getLogger(S3StorageService.class);
  private final String standardStorage;
  private final String securedStorage;
  private final S3Client s3ClientForArchive;
  private final S3Client s3ClientForSecuredArchive;

  public S3StorageService(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repositoryDescription,
          MessageService messageService,
          MetadataService metadataService,
          FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService,
          FallbackArchivePublicDataRemoteResourceService archivePublicDataRemoteResourceService,
          FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService,
          FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteResourceService,
          HistoryService historyService) {
    super(dlcmProperties, repositoryDescription, messageService, metadataService, orgUnitResourceService, archivePublicDataRemoteResourceService,
            archivePublicMetadataRemoteResourceService, archivePrivateMetadataRemoteResourceService, historyService);
    this.standardStorage = dlcmProperties.getStorage().getS3Properties().getSpace();
    this.securedStorage = dlcmProperties.getSecuredStorage().getS3Properties().getSpace();
    this.s3ClientForArchive = this.getNewS3Client(dlcmProperties.getStorage());
    this.s3ClientForSecuredArchive = this.getNewS3Client(dlcmProperties.getSecuredStorage());
  }

  @Override
  public List<String> checkFixity(ArchivalInfoPackage aip, List<ChecksumAlgorithm> algorithmsToRecalculate, AipCheckLevel fixityCheckLevel)
          throws NoSuchAlgorithmException, IOException {

    if (fixityCheckLevel == AipCheckLevel.SIMPLE_FIXITY) {
      return this.checkSimpleFixity(aip, algorithmsToRecalculate);
    } else if (fixityCheckLevel == AipCheckLevel.DEEP_FIXITY) {
      return this.checkDeepFixity(aip, algorithmsToRecalculate);
    }

    throw new FixityCheckLevelNotImplementedException(
            "FixityCheckLevel '" + fixityCheckLevel.name() + "' not implemented for storage of type " + this.getClass().getSimpleName());
  }

  @Override
  public String checkPackage(ArchivalInfoPackage aip, boolean isWorkingAip, boolean deleteData) throws IOException {
    URI aipFile;
    String aipPackage;
    if (!isWorkingAip) {
      if (!this.doesObjectExists(aip.getArchiveUri())) {
        throw new SolidifyCheckingException(this.messageService.get("checking.storage.notfound", new Object[] { aip.getArchiveId() }));
      }
      InputStream aipStream = this.downloadFileWithS3Url(aip.getArchiveUri());
      // Create a temporary file
      Path aipTemp = Files.createTempFile(aip.getResId(), ".zip");
      try (OutputStream outputStream = new FileOutputStream(aipTemp.toFile())) {
        IoUtils.copy(aipStream, outputStream);
        aipFile = aipTemp.toUri();
      }
      // create temp directory to unzip
      aipPackage = Files.createTempDirectory("temp") + "/" + DLCMConstants.PACKAGE;
    } else {
      aipFile = this.getPackageFile(aip, true);
      final Path aipArchive = Paths.get(aipFile);
      if (!FileTool.checkFile(aipArchive) || !FileTool.isFile(aipArchive)) {
        throw new SolidifyCheckingException(this.messageService.get("checking.storage.notfound", new Object[] { aipFile }));
      }
      aipPackage = this.getAIPFolder(aip.getResId()) + "/" + DLCMConstants.PACKAGE;
    }

    this.check(aip, aipFile, aipPackage, deleteData);

    return aipPackage;
  }

  @Override
  public long getAIPFileNumber(StoredArchiveInterface aip) {
    HeadObjectRequest headObjectRequest = HeadObjectRequest.builder()
            .bucket(this.getBucket(aip))
            .key(this.getArchiveId(aip))
            .build();
    HeadObjectResponse headObjectResponse = this.getS3Client(aip).headObject(headObjectRequest);

    return Long.parseLong(headObjectResponse.metadata().get(DLCMConstants.AIP_FILE_NUMBER));
  }

  @Override
  public Map<String, String> getAIPInfo(StoredArchiveInterface aip) {
    HeadObjectRequest headObjectRequest = HeadObjectRequest.builder()
            .bucket(this.getBucket(aip))
            .key(this.getArchiveId(aip))
            .build();

    return this.getS3Client(aip).headObject(headObjectRequest).metadata();
  }

  @Override
  public long getAIPSize(StoredArchiveInterface aip) {
    HeadObjectRequest headObjectRequest = HeadObjectRequest.builder()
            .bucket(this.getBucket(aip))
            .key(this.getArchiveId(aip))
            .build();

    return this.getS3Client(aip).headObject(headObjectRequest).contentLength();
  }

  @Override
  public InputStream getAIPStream(StoredArchiveInterface aip) {
    GetObjectRequest getObjectRequest = GetObjectRequest.builder()
            .bucket(this.getBucket(aip))
            .key(this.getArchiveId(aip))
            .build();

    InputStream inputStream = this.getS3Client(aip).getObject(getObjectRequest, ResponseTransformer.toInputStream());
    log.debug("Successfully obtained AIP: {}, from an S3 bucket", aip.getArchiveContainer().name());

    return new BufferedInputStream(inputStream);
  }

  @Override
  public URI getArchiveLocation(StoredArchiveInterface aip) throws URISyntaxException {
    GetUrlRequest request = GetUrlRequest.builder().bucket(this.getBucket(aip)).key(this.getArchiveId(aip)).build();
    URL url = this.getS3Client(aip).utilities().getUrl(request);
    if (log.isDebugEnabled()) {
      log.debug("The URL for S3 Object {} is {}", this.getArchiveId(aip), url);
    }
    return url.toURI();
  }

  @Override
  public URI findArchiveLocation(StoredArchiveInterface aip) throws URISyntaxException {
    GetUrlRequest request = GetUrlRequest.builder().bucket(this.getBucket(aip)).key(this.getArchiveId(aip)).build();
    if (this.doesObjectExists(this.s3ClientForArchive, this.getBucket(aip), this.getArchiveId(aip))) {
      return this.s3ClientForArchive.utilities().getUrl(request).toURI();
    }
    if (this.doesObjectExists(this.s3ClientForSecuredArchive, this.getBucket(aip), this.getArchiveId(aip))) {
      return this.s3ClientForSecuredArchive.utilities().getUrl(request).toURI();
    }
    return null;
  }

  @Override
  public AipCheckLevel getDefaultFixityCheckLevel() {
    return AipCheckLevel.SIMPLE_FIXITY;
  }

  @Override
  public PackageStatus getStatus(ArchivalInfoPackage aip) throws IOException {
    // Check AIP
    final Path aipFile = Paths.get(aip.getDataFiles().get(0).getFinalData());
    if (!FileTool.checkFile(aipFile)) {
      return PackageStatus.IN_ERROR;
    }

    // Check S3 Storage if aip is present
    if (this.doesObjectExists(this.getS3Client(aip), this.getBucket(aip), this.getArchiveId(aip))) {
      return PackageStatus.STORED;
    }

    return PackageStatus.READY;
  }

  @Override
  public List<StoredAIP> listStoredAip() {
    final ArchivalInfoPackage aipForSearching = new ArchivalInfoPackage();
    // Normal Storage
    aipForSearching.getInfo().setDataSensitivity(DataTag.BLUE);

    ListObjectsV2Request listObjectsRequest = ListObjectsV2Request.builder()
            .bucket(this.getBucket(aipForSearching))
            .build();

    final List<StoredAIP> list = new ArrayList<>(
            this.listStorageLocation(this.getS3Client(aipForSearching).listObjectsV2(listObjectsRequest)));
    // Secured Storage
    aipForSearching.getInfo().setDataSensitivity(DataTag.CRIMSON);

    listObjectsRequest = ListObjectsV2Request.builder()
            .bucket(this.getBucket(aipForSearching))
            .build();

    list.addAll(this.listStorageLocation(this.getS3Client(aipForSearching).listObjectsV2(listObjectsRequest)));
    return list;
  }

  @Override
  public void storeAIP(ArchivalInfoPackage aip) {
    try (BufferedInputStream in = new BufferedInputStream(Files.newInputStream(Paths.get(aip.getDataFiles().get(0).getFinalData())))) {
      this.updateAipWithObjectMetadata(aip, in);
    } catch (IOException e) {
      throw new SolidifyProcessingException("Error in storing AIP in S3 container (" + aip.getResId() + ")", e);
    }

  }

  @Override
  public void replaceAIP(ArchivalInfoPackage aip, Path newPackage) {
    try (BufferedInputStream in = new BufferedInputStream(Files.newInputStream(newPackage))) {
      // Delete previous archive
      this.deleteObjectWithUrl(aip.getArchiveUri());
      // Update archive location
      aip.setArchiveId(this.getArchiveLocation(aip).toString());
      this.updateAipWithObjectMetadata(aip, in);
    } catch (IOException | URISyntaxException e) {
      throw new SolidifyProcessingException("Error in storing AIP in S3 container (" + aip.getResId() + ")", e);
    }
  }

  @Override
  public String submitAIP(ArchivalInfoPackage aip) throws IOException, URISyntaxException {
    final Path aipFile = Paths.get(aip.getDataFiles().get(0).getFinalData());
    HeadObjectResponse headObject = this.getHeadObject(aip);
    if (FileTool.checkFile(aipFile) && FileTool.getSize(aipFile) == headObject.contentLength()) {
      return this.getArchiveLocation(aip).toString();
    }
    return null;
  }

  /**
   * Fetch the file from S3 storage and really recalculates the file hashes
   *
   * @param aip archivalInfoPackage object
   * @param algorithmsToCalculate list of checksum algorithms
   * @return list of checksums
   * @throws NoSuchAlgorithmException no checksum algorithm
   * @throws IOException error when manipulating stream
   */
  private List<String> checkDeepFixity(ArchivalInfoPackage aip, List<ChecksumAlgorithm> algorithmsToCalculate)
          throws NoSuchAlgorithmException, IOException {

    try (InputStream stream = this.getAIPStream(aip)) {
      return ChecksumTool.getChecksums(stream, algorithmsToCalculate);
    }
  }

  /**
   * Get the requested available file hashes by using the one provided by S3
   *
   * @param aip archivalInfoPackage object
   * @param algorithmsToRecalculate list of checksum algorithms
   * @return the available hash values provided by S3 storage, if they are available (currently only MD5 is returned)
   */
  private List<String> checkSimpleFixity(ArchivalInfoPackage aip, List<ChecksumAlgorithm> algorithmsToRecalculate) {

    final List<String> hashes = new ArrayList<>();

    for (final ChecksumAlgorithm algo : algorithmsToRecalculate) {

      if (algo.name().equals("MD5")) {

        HeadObjectRequest headObjectRequest = HeadObjectRequest.builder()
                .bucket(this.getBucket(aip))
                .key(this.getArchiveId(aip))
                .build();

        final HeadObjectResponse metadata = this.getS3Client(aip).headObject(headObjectRequest);
        String md5 = metadata.eTag();

        if (!md5.contains("-")) {

          /*
           * Case of MD5 of the whole file
           */

          // Remove leading and trailing quotes if present
          if (md5.startsWith("\"") && md5.endsWith("\"") && md5.length() >= 2) {
            md5 = md5.substring(1, md5.length() - 1);
          }

          hashes.add(md5);

        } else {

          /*
           * The file has been uploaded using multi part --> the returned MD5 is not the checksum of the
           * original file
           */
          // TODO: support fixity check for multi part files
          hashes.add(null);
        }

      } else {
        hashes.add(null);
      }
    }

    return hashes;
  }

  private String getBucket(StoredArchiveInterface archive) {
    if (archive.getDataSensitivity().value() < DataTag.ORANGE.value()) {
      return this.standardStorage;
    } else {
      return this.securedStorage;
    }
  }

  private S3Client getNewS3Client(Storage storageConfig) {

    AwsBasicCredentials awsCredentials = AwsBasicCredentials.create(
            storageConfig.getS3Properties().getAccessKey(),
            storageConfig.getS3Properties().getSecretKey());

    S3Configuration s3Config = S3Configuration.builder()
            .pathStyleAccessEnabled(storageConfig.getS3Properties().getIsPathStyleAccess())
            .build();

    return S3Client.builder()
            .credentialsProvider(StaticCredentialsProvider.create(awsCredentials))
            .endpointOverride(URI.create(storageConfig.getUrl()))
            .region(Region.of("GLOBAL"))
            .serviceConfiguration(s3Config)
            .build();
  }

  private S3Client getS3Client(StoredArchiveInterface archive) {
    if (archive.getDataSensitivity().value() < DataTag.ORANGE.value()) {
      return this.s3ClientForArchive;
    } else {
      return this.s3ClientForSecuredArchive;
    }
  }

  private HeadObjectResponse getHeadObject(StoredArchiveInterface archive) {
    HeadObjectRequest getObjectRequest = HeadObjectRequest.builder()
            .bucket(this.getBucket(archive))
            .key(this.getArchiveId(archive))
            .build();

    return this.getS3Client(archive).headObject(getObjectRequest);
  }

  private List<StoredAIP> listStorageLocation(ListObjectsV2Response s3list) {
    final List<StoredAIP> list = new ArrayList<>();
    for (final S3Object s3obj : s3list.contents()) {
      if (!s3obj.key().endsWith("/")) {
        try {
          list.add(this.getStoredAip(s3obj.key()));
        } catch (final URISyntaxException e) {
          log.error("Cannot find stored AIP (" + s3obj.key() + ")", e);
        }
      }
    }
    return list;
  }

  private void updateAipWithObjectMetadata(ArchivalInfoPackage aip, BufferedInputStream in) {
    Map<String, String> s3md = new HashMap<>();
    PutObjectRequest.Builder builder = PutObjectRequest.builder()
            .bucket(this.getBucket(aip))
            .contentType(SolidifyConstants.ZIP_MIME_TYPE);

    for (final Entry<String, Object> metadata : aip.getAIPMetadata().entrySet()) {
      s3md.put(metadata.getKey(), metadata.getValue().toString());
    }
    Long contentLength = aip.isDisposalInProgress() ? aip.getTombstoneSize() : aip.getArchiveSize();
    builder.contentLength(contentLength);

    PutObjectRequest request = builder.metadata(s3md).key(this.getArchiveId(aip)).build();

    // Upload data
    try {
      this.getS3Client(aip).putObject(request, RequestBody.fromInputStream(in, contentLength));
    } catch (AwsServiceException | SdkClientException e) {
      throw new SolidifyProcessingException("Error in storing AIP file in S3 container (" + aip.getResId() + ")", e);
    }
  }

  private boolean doesObjectExists(S3Client s3Client, String bucketName, String objectKey) {
    try {
      HeadObjectRequest headObjectRequest = HeadObjectRequest.builder()
              .bucket(bucketName)
              .key(objectKey)
              .build();

      s3Client.headObject(headObjectRequest);
      log.debug("S3 Object: {} exists in Bucket: {}", objectKey, bucketName);
      return true;
    } catch (S3Exception e) {
      if (e.statusCode() == 404) {
        log.debug("S3 Object: {} does not exist in bucket: {}", objectKey, bucketName);
        return false;
      } else {
        log.error("S3 exception occurred: {}", e.awsErrorDetails().errorMessage());
        throw e;
      }
    }
  }

  /**
   * Downloads a file from S3 given its URL, and returns it as an InputStream.
   *
   * @param s3Url The URL of the S3 object.
   * @return InputStream of the downloaded S3 object.
   */
  private boolean doesObjectExists(URI s3Url) {
    String bucketName = "";
    String objectKey = "";
    try {
      // Parse the S3 URL to extract bucket name, object key
      String host = s3Url.getHost();
      objectKey = s3Url.getPath().startsWith("/") ? s3Url.getPath().substring(1) : s3Url.getPath();
      // Virtual Hosted–Style URL: https://bucket-name.tenant.unige.ch/object-key
      String[] hostParts = host.split("\\.");
      if (hostParts.length < 4) {
        throw new IllegalArgumentException(INVALID_URL + s3Url);
      }
      bucketName = hostParts[0];
      // Build the GetObjectRequest
      HeadObjectRequest headObjectRequest = HeadObjectRequest.builder()
              .bucket(bucketName)
              .key(objectKey)
              .build();

      if (bucketName.equals(this.standardStorage)) {
        this.s3ClientForArchive.headObject(headObjectRequest);
      } else {
        this.s3ClientForSecuredArchive.headObject(headObjectRequest);
      }
      log.debug("S3 Object with URL: {} exists in Bucket: {}", s3Url, bucketName);
      return true;
    } catch (S3Exception e) {
      if (e.statusCode() == 404) {
        log.debug("S3 Object with URL: {} does not exist in bucket: {}", s3Url, bucketName);
        return false;
      } else {
        log.error("S3 exception occurred: {}", e.awsErrorDetails().errorMessage());
        throw e;
      }
    }

  }

  /**
   * Downloads a file from S3 given its URL, and returns it as an InputStream.
   *
   * @param s3Url The URL of the S3 object.
   * @return InputStream of the downloaded S3 object.
   */
  private InputStream downloadFileWithS3Url(URI s3Url) {
    // Parse the S3 URL to extract bucket name, object key
    String host = s3Url.getHost();
    String objectKey = s3Url.getPath().startsWith("/") ? s3Url.getPath().substring(1) : s3Url.getPath();
    // Virtual Hosted–Style URL: https://bucket-name.tenant.unige.ch/object-key
    String[] hostParts = host.split("\\.");
    if (hostParts.length < 4) {
      throw new IllegalArgumentException(INVALID_URL + s3Url);
    }
    // Get the bucketName
    String bucketName = hostParts[0];
    // Build the GetObjectRequest
    GetObjectRequest getObjectRequest = GetObjectRequest.builder()
            .bucket(bucketName)
            .key(objectKey)
            .build();
    InputStream inputStream;
    try {
      if (bucketName.equals(this.standardStorage)) {
        inputStream = this.s3ClientForArchive.getObject(getObjectRequest, ResponseTransformer.toInputStream());
      } else {
        inputStream = this.s3ClientForSecuredArchive.getObject(getObjectRequest, ResponseTransformer.toInputStream());
      }
      return new BufferedInputStream(inputStream);

    } catch (AwsServiceException | SdkClientException e) {
      throw new SolidifyCheckingException("Failed to download S3 object from bucket: " + bucketName + ", key: " + objectKey, e);
    }
  }

  private void deleteObjectWithUrl(URI s3Url) {
    // Parse the S3 URL to extract bucket name, object key
    String host = s3Url.getHost();
    String objectKey = s3Url.getPath().startsWith("/") ? s3Url.getPath().substring(1) : s3Url.getPath();

    // Virtual Hosted–Style URL: https://bucket-name.tenant.unige.ch/object-key
    String[] hostParts = host.split("\\.");
    if (hostParts.length < 4) {
      throw new IllegalArgumentException(INVALID_URL + s3Url);
    }
    // Get the bucketName
    String bucketName = hostParts[0];

    // Build the GetObjectRequest
    DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder()
            .bucket(bucketName)
            .key(objectKey)
            .build();
    try {
      if (bucketName.equals(this.standardStorage)) {
        this.s3ClientForArchive.deleteObject(deleteObjectRequest);
      } else {
        this.s3ClientForSecuredArchive.deleteObject(deleteObjectRequest);
      }
    } catch (AwsServiceException | SdkClientException e) {
      throw new SolidifyCheckingException("Failed to download S3 object from bucket: " + bucketName + ", key: " + objectKey, e);
    }
  }

}

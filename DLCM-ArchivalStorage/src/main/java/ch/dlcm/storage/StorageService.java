/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - StorageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import jakarta.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.BagItTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.exception.DLCMPreservationException;
import ch.dlcm.model.AipCheckLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.FileInfoUpdate;
import ch.dlcm.model.IdentifierType;
import ch.dlcm.model.Package;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.StoredArchiveInterface;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.index.ArchivePublicData;
import ch.dlcm.model.index.ArchivePublicData.PublicDataFileType;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.SubjectArea;
import ch.dlcm.model.tool.MetadataMigrationTool;
import ch.dlcm.service.DLCMService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicDataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;

public abstract class StorageService extends DLCMService {

  private static final Logger log = LoggerFactory.getLogger(StorageService.class);

  private static final String ARCHIVE_MESSAGE_SUFFIX = "' archive";
  private static final String FILE_UPDATE_MSG = "Added file to update of type {}";
  protected static final String AIP_ERROR_PURGE_FILES_MSG = "archival.aip.error.purge.files";
  protected static final String AIP_METADATA_FILES_MSG = "archival.disposal.error.metadata";
  protected final String repositoryName;
  protected final String repositoryInstitution;
  protected final String repositoryLocation;
  private final String archivalLocation;
  private final String indexName;
  private final String privateIndexName;
  private final String[] indexingExceptions;
  private final int waitMilliseconds;
  private final int waitMaxTries;

  protected final MetadataService metadataService;
  protected final FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService;
  private final FallbackArchivePublicDataRemoteResourceService archivePublicDataRemoteResourceService;
  private final FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService;
  private final FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteResourceService;

  private final HistoryService historyService;

  protected StorageService(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repositoryDescription,
          MessageService messageService,
          MetadataService metadataService,
          FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService,
          FallbackArchivePublicDataRemoteResourceService archivePublicDataRemoteResourceService,
          FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService,
          FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteResourceService,
          HistoryService historyService) {
    super(messageService, dlcmProperties);
    this.archivalLocation = dlcmProperties.getArchivalLocation();
    this.repositoryName = repositoryDescription.getName();
    this.repositoryInstitution = repositoryDescription.getInstitution();
    this.repositoryLocation = repositoryDescription.getLocation();
    this.indexName = dlcmProperties.getIndexing().getIndexName();
    this.indexingExceptions = dlcmProperties.getIndexing().getExceptions();
    this.privateIndexName = dlcmProperties.getIndexing().getPrivateIndexName();
    this.waitMilliseconds = dlcmProperties.getWait().getMilliseconds();
    this.waitMaxTries = dlcmProperties.getWait().getMaxTries();
    this.metadataService = metadataService;
    this.orgUnitResourceService = orgUnitResourceService;
    this.archivePublicDataRemoteResourceService = archivePublicDataRemoteResourceService;
    this.archivePublicMetadataRemoteResourceService = archivePublicMetadataRemoteResourceService;
    this.archivePrivateMetadataRemoteResourceService = archivePrivateMetadataRemoteResourceService;
    this.historyService = historyService;
  }

  // **************************
  // ** Methods to implement **
  // **************************

  // Get Archive File Number
  public abstract long getAIPFileNumber(StoredArchiveInterface archive) throws IOException;

  // Get Archive info
  public abstract Map<String, String> getAIPInfo(StoredArchiveInterface archive) throws IOException, JAXBException;

  // Get Archive size
  public abstract long getAIPSize(StoredArchiveInterface archive) throws IOException;

  // Get Archive stream
  public abstract InputStream getAIPStream(StoredArchiveInterface archive) throws IOException;

  // Get Archive URI
  public abstract URI getArchiveLocation(StoredArchiveInterface archive) throws URISyntaxException;

  // find Archive URI
  public abstract URI findArchiveLocation(StoredArchiveInterface archive) throws URISyntaxException;

  // Get the default FixityCheckLevel for the storage implementation
  public abstract AipCheckLevel getDefaultFixityCheckLevel();

  // Get AIP status
  public abstract PackageStatus getStatus(ArchivalInfoPackage aip) throws IOException;

  // List already stored AIP
  public abstract List<StoredAIP> listStoredAip();

  // Store AIP
  public abstract void storeAIP(ArchivalInfoPackage aip);

  // Store AIP
  public abstract void replaceAIP(ArchivalInfoPackage aip, Path newPackage);

  // Submit AIP
  public abstract String submitAIP(ArchivalInfoPackage aip) throws IOException, URISyntaxException;

  public abstract String checkPackage(ArchivalInfoPackage aip, boolean isWorkingAip, boolean deleteData) throws IOException, URISyntaxException;

  // Perform a fixity check on AIP with requested FixityCheckLevel
  public abstract List<String> checkFixity(ArchivalInfoPackage aip, List<ChecksumAlgorithm> algorithmsToRecalculate,
          AipCheckLevel fixityCheckLevel) throws NoSuchAlgorithmException, IOException;

  // Update archive (Metadata & Metadata files)
  public void updateArchiveMetadata(ArchivalInfoPackage aip) throws IOException, JAXBException, URISyntaxException {
    // Check & extract archive
    final Path aipPackageFolder = Paths.get(this.checkPackage(aip, false, false));
    final Path aipDataFolder = this.getDataFolder(aip.getArchiveContainer(), aipPackageFolder);
    // Extract updated package
    Path newMetadataFile = null;
    Path newPremisFile = null;
    if (!aip.getDataFiles().isEmpty()) {
      newMetadataFile = Paths.get(this.checkUpdatePackage(aip));
      if (!FileTool.checkFile(newMetadataFile)) {
        throw new DLCMPreservationException(
                this.messageService.get("archival.aip.error.updated-metadata", new Object[] { newMetadataFile }));
      }
    }
    log.debug("Path of the newMetadataFile added in the aip: {}", newMetadataFile);

    // Get updated folder
    final Path aipUpdateFolder = this.getAIPFolder(aip.getResId()).resolve(DLCMConstants.UPDATE_FOLDER);
    log.debug("Path of the updated folder for the aip {}", aipUpdateFolder);
    // Check if there is any updated file: DUA, Readme or thumbnail and move them into the data
    List<FileInfoUpdate> fileInfoUpdates = this.checkAndReplaceUpdatedFiles(aipUpdateFolder, aipDataFolder, aip);
    if (!fileInfoUpdates.isEmpty()) {
      newPremisFile = Paths.get(this.checkPremisFile(aip));
    }
    log.debug("Path of the premisFile added in the aip: {}", newPremisFile);

    // Update metadata
    this.metadataService.updateMetadata(aip, aipPackageFolder.resolve(this.getMetadataRelativePath(aip.getArchiveContainer())), newMetadataFile,
            newPremisFile, fileInfoUpdates);
    // Generate new package
    final Path newPackageFile = this.generateNewPackage(aip, aipDataFolder);
    // Purge package folder (temporary folder for updating archive )
    if (FileTool.checkFile(aipPackageFolder) && !FileTool.deleteFolder(aipPackageFolder)) {
      throw new DLCMPreservationException(this.messageService.get(AIP_ERROR_PURGE_FILES_MSG));
    }
    // Purge update folder (temporary folder for archive files)
    if (FileTool.checkFile(aipUpdateFolder) && !FileTool.deleteFolder(aipUpdateFolder)) {
      throw new DLCMPreservationException(this.messageService.get(AIP_ERROR_PURGE_FILES_MSG));
    }
    // Set the archive size
    aip.setArchiveSize(FileTool.getSize(newPackageFile));
    aip.setArchiveFileNumber(aip.getArchiveFileNumber() + 1); // add one to ArchiveFileNumber as we create xml file every time we do an update
    // Save new archive
    this.replaceAIP(aip, newPackageFile);
    // Add update
    aip.incrementUpdateNumber();
  }

  // Upgrade archive (migrate metadata to latest version)
  public void upgradeArchiveMetadata(ArchivalInfoPackage aip) throws IOException, JAXBException, URISyntaxException {
    // Check & extract archive
    final Path aipPackageFolder = Paths.get(this.checkPackage(aip, false, false));
    final Path aipDataFolder = this.getDataFolder(aip.getArchiveContainer(), aipPackageFolder);
    // Migrate metadata
    MetadataMigrationTool.migrateMetadataToDefaultVersion(aip.getInfo().getMetadataVersion(),
            aipDataFolder.resolve(Package.METADATA_FILE.getName()));
    aip.getInfo().setMetadataVersion(DLCMMetadataVersion.getDefaultVersion());
    // Upgrade metadata by adding Premis event
    this.metadataService.upgradeMetadata(aip, aipPackageFolder.resolve(this.getMetadataRelativePath(aip.getArchiveContainer())));
    // Generate new package
    final Path newPackageFile = this.generateNewPackage(aip, aipDataFolder);
    // Purge package folder (temporary folder for updating archive )
    if (FileTool.checkFile(aipPackageFolder) && !FileTool.deleteFolder(aipPackageFolder)) {
      throw new DLCMPreservationException(this.messageService.get(AIP_ERROR_PURGE_FILES_MSG));
    }
    // Set the archive size
    aip.setArchiveSize(FileTool.getSize(newPackageFile));
    aip.setArchiveFileNumber(aip.getArchiveFileNumber() + 1); // add one to ArchiveFileNumber as we create xml file every time we do an update
    // Save new archive
    this.replaceAIP(aip, newPackageFile);
    // Add update
    aip.incrementUpdateNumber();
  }

  // Update compliance level
  public int updateComplianceLevel(ArchivalInfoPackage aip) throws IOException, JAXBException, URISyntaxException {
    // Check & extract archive
    final Path aipPackageFolder = Paths.get(this.checkPackage(aip, false, false));
    final Path aipDataFolder = this.getDataFolder(aip.getArchiveContainer(), aipPackageFolder);
    final Path metadataXml = aipDataFolder.resolve(Package.METADATA_FILE.getName());
    // Copy original metadata XML file
    final Path originalXml = metadataXml.getParent()
            .resolve(metadataXml.getFileName().toString()
                    .replaceFirst(SolidifyConstants.XML_EXT,
                            "-" + OffsetDateTime.now().format(DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT_FOR_FILE))
                                    + SolidifyConstants.XML_EXT));
    if (!FileTool.copyFile(metadataXml, originalXml)) {
      throw new SolidifyProcessingException(
              this.messageService.get("archival.aip.error.metadata-copy", new Object[] { metadataXml, originalXml }));
    }
    // Upgrade metadata by adding Premis event
    final int updateNumber = this.metadataService.updateComplianceLevel(aip,
            aipPackageFolder.resolve(this.getMetadataRelativePath(aip.getArchiveContainer())));
    if (updateNumber > 0) {
      this.updateHistory(aip, PackageStatus.COMPLIANCE_LEVEL_UPDATED.toString(),
              this.messageService.get("archival.aip.compliance-updated", new Object[] { updateNumber }));
      // Generate new package
      final Path newPackageFile = this.generateNewPackage(aip, aipDataFolder);
      // Set the archive size
      aip.setArchiveSize(FileTool.getSize(newPackageFile));
      aip.setArchiveFileNumber(aip.getArchiveFileNumber() + 1); // add one to ArchiveFileNumber as we create xml file every time we do an update
      // Save new archive
      this.replaceAIP(aip, newPackageFile);
      // Add update
      aip.incrementUpdateNumber();
    }
    // Purge package folder (temporary folder for updating archive )
    if (FileTool.checkFile(aipPackageFolder) && !FileTool.deleteFolder(aipPackageFolder)) {
      throw new DLCMPreservationException(this.messageService.get(AIP_ERROR_PURGE_FILES_MSG));
    }
    return updateNumber;
  }

  // *********************
  // ** Default Methods **
  // *********************
  public void disposeArchive(ArchivalInfoPackage aip) throws IOException, JAXBException, URISyntaxException {
    // Check & extract archive
    Path aipPackage = Paths.get(this.checkPackage(aip, false, false));
    // Create tombstone
    final Path tombstoneFolder = aipPackage.getParent().resolve(DLCMConstants.TOMBSTONE);
    // Clean tombstone folder if exists because of previous executions
    if (FileTool.checkFile(tombstoneFolder) && !FileTool.deleteFolder(tombstoneFolder)) {
      throw new DLCMPreservationException(this.messageService.get(AIP_ERROR_PURGE_FILES_MSG));
    }
    // Check that tombstone folder exists
    if (!FileTool.ensureFolderExists(tombstoneFolder)) {
      throw new DLCMPreservationException(this.messageService.get("archival.disposal.error.tombstone.folder.missing"));
    }
    // Move metadata files
    this.moveFilesForDisposal(aip, aipPackage, tombstoneFolder);

    // Delete data files if exist
    if (!FileTool.deleteFolder(aipPackage)) {
      throw new DLCMPreservationException(this.messageService.get(AIP_ERROR_PURGE_FILES_MSG));
    }
    // Update metadata
    this.metadataService.updateMetadataForDisposal(aip, tombstoneFolder.resolve(Package.METADATA_FILE.getName()));
    // Generate tombstone archive
    Path tombstonePackage = this.generateNewPackage(aip, tombstoneFolder);
    //
    aip.setTombstoneSize(FileTool.getSize(tombstonePackage));
    // Save tombstone archive
    this.replaceAIP(aip, tombstonePackage);
  }

  private void moveFilesForDisposal(ArchivalInfoPackage aip, Path aipPackage, Path tombstoneFolder) throws IOException {
    // Move metadata files
    for (Path metadataFile : FileTool.readFolder(aipPackage.resolve(this.getRelativeFolder(aip.getArchiveContainer())))) {
      if (!FileTool.moveFile(metadataFile, tombstoneFolder.resolve(metadataFile.getFileName()))) {
        throw new DLCMPreservationException(this.messageService.get(AIP_METADATA_FILES_MSG, new Object[] { tombstoneFolder }));
      }
    }
    // Move metadata folder
    final Path aipFolder = aipPackage.resolve(this.getRelativeFolder(aip.getArchiveContainer()));
    this.movFolderForDisposal(aipFolder, Package.INTERNAL.getName().substring(1), tombstoneFolder);
    // Move Custom metadata
    this.movFolderForDisposal(aipFolder, Package.METADATA.getName().substring(1), tombstoneFolder);
    // Mode schemas files
    this.movFolderForDisposal(aipFolder, Package.SCHEMAS.getName().substring(1), tombstoneFolder);
  }

  private void movFolderForDisposal(Path rootFolder, String subFolder, Path tombstoneFolder) throws IOException {
    final Path folder = rootFolder.resolve(subFolder);
    if (FileTool.isFolder(folder)) {
      for (Path file : FileTool.readFolder(folder)) {
        if (!FileTool.moveFile(file, tombstoneFolder.resolve(subFolder).resolve(file.getFileName()))) {
          throw new DLCMPreservationException(this.messageService.get(AIP_METADATA_FILES_MSG, new Object[] { tombstoneFolder }));
        }
      }
    }
  }

  // Get working directory for AIP
  public Path getAIPFolder(String resId) {
    return Paths.get(this.archivalLocation).resolve(resId);
  }

  // Check Working AIP
  public void checkAIP(ArchivalInfoPackage aip) throws IOException, URISyntaxException {
    this.checkPackage(aip, true, true);
  }

  // Check Archive
  public void checkArchive(ArchivalInfoPackage aip) throws IOException, URISyntaxException {
    this.checkPackage(aip, false, true);
  }

  // Perform a fixity check on AIP file with default FixityCheckLevel
  public List<String> checkFixity(ArchivalInfoPackage aip, List<ChecksumAlgorithm> algorithmsToRecalculate)
          throws NoSuchAlgorithmException, IOException {
    return this.checkFixity(aip, algorithmsToRecalculate, this.getDefaultFixityCheckLevel());
  }

  // Check if AIP metadata index already exists
  public boolean checkIndexing(String aipId) {
    return (this.archivePublicMetadataRemoteResourceService.headIndexMetadata(aipId) == HttpStatus.OK
            && this.archivePrivateMetadataRemoteResourceService.headIndexMetadata(aipId) == HttpStatus.OK);
  }

  // Delete AIP metadata index
  public boolean deleteIndex(String aipId) {
    int tries = 0;
    do {
      // Delete indexes
      this.archivePublicMetadataRemoteResourceService.deleteIndex(aipId);
      this.archivePrivateMetadataRemoteResourceService.deleteIndex(aipId);
      if (!this.checkIndexing(aipId)) {
        // Delete Thumbnail
        if (this.archivePublicDataRemoteResourceService.checkArchivePublicData(aipId, PublicDataFileType.ARCHIVE_THUMBNAIL)) {
          this.archivePublicDataRemoteResourceService.deleteArchivePublicData(aipId, PublicDataFileType.ARCHIVE_THUMBNAIL);
        }
        // Delete dua
        if (this.archivePublicDataRemoteResourceService.checkArchivePublicData(aipId, PublicDataFileType.ARCHIVE_DUA)) {
          this.archivePublicDataRemoteResourceService.deleteArchivePublicData(aipId, PublicDataFileType.ARCHIVE_DUA);
        }
        // Delete README
        if (this.archivePublicDataRemoteResourceService.checkArchivePublicData(aipId, PublicDataFileType.ARCHIVE_README)) {
          this.archivePublicDataRemoteResourceService.deleteArchivePublicData(aipId, PublicDataFileType.ARCHIVE_README);
        }
        return true;
      }
      SolidifyTime.waitInMilliSeconds(this.waitMilliseconds);
      tries++;
    } while (tries < this.waitMaxTries);

    return false;
  }

  // Get stored AIP
  public StoredAIP getStoredAip(String aipId) throws URISyntaxException {
    final StoredAIP aip = new StoredAIP();
    aip.setResId(aipId);
    aip.setArchiveUri(this.findArchiveLocation(aip));
    return aip;
  }

  // Index AIP metadata
  public boolean indexAIP(ArchivalInfoPackage aip) {
    // Get DLCM metadata
    ArchiveMetadata md = this.getAipMetadata(this.getAIPFolder(aip.getResId()), this.indexName, aip);
    // Complete DLCM metadata with AIP metadata
    for (final Entry<String, Object> metadata : aip.getAIPMetadata().entrySet()) {
      md.getMetadata().put(metadata.getKey(), metadata.getValue());
    }
    // Persistent Identifiers
    if (!StringTool.isNullOrEmpty(md.getDoi())) {
      md.getMetadata().put(IdentifierType.DOI.getDescription().toLowerCase(), md.getDoi());
    }
    if (!StringTool.isNullOrEmpty(md.getArk())) {
      md.getMetadata().put(IdentifierType.ARK.getDescription().toLowerCase(), md.getArk());
    }
    // Add orgUnit Details
    final OrganizationalUnit orgUnit = this.orgUnitResourceService.findOneWithCache(aip.getInfo().getOrganizationalUnitId());
    // OrgUnit Name
    md.getMetadata().put(DLCMConstants.AIP_ORGA_UNIT_NAME, orgUnit.getName());
    // Institutions
    List<Institution> institutionList = this.orgUnitResourceService.getInstitutions(orgUnit.getResId());
    if (institutionList != null && !institutionList.isEmpty()) {
      final List<String> nameList = new ArrayList<>();
      final List<String> descList = new ArrayList<>();
      for (final Institution i : institutionList) {
        nameList.add(i.getName());
        descList.add(i.getDescription());
      }
      md.getMetadata().put(DLCMConstants.AIP_INSTITUTIONS, nameList);
      md.getMetadata().put(DLCMConstants.AIP_INSTITUTION_DESCRIPTIONS, descList);
    }
    // Research domains
    if (orgUnit.getSubjectAreas() != null && !orgUnit.getSubjectAreas().isEmpty()) {
      final List<String> subjectAreas = new ArrayList<>();
      for (final SubjectArea rd : orgUnit.getSubjectAreas()) {
        subjectAreas.add(rd.getName());
      }
      md.getMetadata().put(DLCMConstants.AIP_SUBJECT_AREAS, subjectAreas);
    }
    // Issued date
    if (md.getIssuedDate() != null) {
      md.getMetadata().put(DLCMConstants.AIP_ISSUED_DATE, md.getIssuedDate().toString());
    }
    final List<ArchiveMetadata> metadataList = this.splitMetadata(md, aip);
    final List<ArchiveMetadata> publicMetadataList = metadataList.stream().filter(m -> m.getIndex().equals(this.indexName)).toList();
    final List<ArchiveMetadata> privateMetadataList = metadataList.stream().filter(m -> m.getIndex().equals(this.privateIndexName)).toList();
    return this.archivePublicMetadataRemoteResourceService.indexMetadata(publicMetadataList)
            && this.archivePrivateMetadataRemoteResourceService.indexMetadata(privateMetadataList);
  }

  public void checkArchiveFileNumber(ArchivalInfoPackage aip, long fileNumber, long updateNumber, boolean fixAip) {
    // Check file number
    if (aip.getArchiveFileNumber() == null || aip.getArchiveFileNumber() == 0
            || aip.getInfo().getStatus().equals(PackageStatus.EDITING_METADATA)) {
      // Update it if not set for previous AIP
      aip.setArchiveFileNumber(fileNumber);
      return;
    }
    // Check update number
    if (aip.getUpdateNumber() != updateNumber) {
      if (fixAip) {
        aip.setUpdateNumber(updateNumber);
      } else {
        throw new SolidifyCheckingException(
                this.messageService.get("archival.aip.error.update-number", new Object[] { aip.getUpdateNumber(), updateNumber }));
      }
    }
    // Check disposed AIP
    if (aip.isTombstone() && fileNumber != 1 + aip.getUpdateNumber()) {
      throw new SolidifyCheckingException(this.messageService.get("archival.aip.error.file-number", new Object[] { 1, fileNumber }));
    }
    // Check normal AIP
    if (aip.getArchiveFileNumber() != fileNumber) {
      if (fixAip) {
        aip.setArchiveFileNumber(fileNumber);
      } else {
        throw new SolidifyCheckingException(
                this.messageService.get("archival.aip.error.file-number", new Object[] { aip.getArchiveFileNumber(), fileNumber }));
      }
    }
    // Check collection AIP
    if (aip.isCollection()) {
      fileNumber -= aip.getUpdateNumber() + 1;
      for (final ArchivalInfoPackage aiu : aip.getCollection()) {
        fileNumber += aiu.getArchiveFileNumber() - aiu.getUpdateNumber() - 1;
      }
      if (aip.getCollectionFileNumber() != fileNumber) {
        if (fixAip) {
          aip.setCollectionFileNumber(fileNumber);
        } else {
          throw new SolidifyCheckingException(this.messageService.get("archival.aip.error.collection-file-number",
                  new Object[] { aip.getCollectionFileNumber(), fileNumber }));
        }
      }
    } else if (aip.getCollectionFileNumber() != 0) {
      if (fixAip) {
        aip.setCollectionFileNumber(0L);
      } else {
        throw new SolidifyCheckingException(
                this.messageService.get("archival.aip.error.collection-file-number", new Object[] { aip.getCollectionFileNumber(), 0 }));
      }
    }
  }

  public void checkArchiveSize(ArchivalInfoPackage aip, long size, boolean fixAip) {
    // Check size
    if (aip.getArchiveSize() == null || aip.getArchiveSize() == 0 || aip.getInfo().getStatus().equals(PackageStatus.EDITING_METADATA)) {
      // Update it if not set for previous AIP
      aip.setArchiveSize(size);
      return;
    }
    // Check size
    Long archiveSize = aip.getArchiveSize();
    if (aip.isTombstone()) {
      archiveSize = aip.getTombstoneSize();
    }
    if (archiveSize != size) {
      if (fixAip) {
        if (aip.isTombstone()) {
          aip.setTombstoneSize(size);
        } else {
          aip.setArchiveSize(size);
        }
      } else {
        throw new SolidifyCheckingException(
                this.messageService.get("archival.aip.error.size", new Object[] { archiveSize, size }));
      }
    }
    // For collection, add others AIPs
    if (aip.isCollection()) {
      long collectionSize = size;
      for (final ArchivalInfoPackage aiu : aip.getCollection()) {
        if (aiu.isTombstone()) {
          collectionSize += aiu.getTombstoneSize();
        } else {
          collectionSize += aiu.getArchiveSize();
        }
      }
      if (aip.getCollectionArchiveSize() != collectionSize) {
        if (fixAip) {
          aip.setCollectionArchiveSize(collectionSize);
        } else {
          throw new SolidifyCheckingException(
                  this.messageService.get("archival.aip.error.collection-size",
                          new Object[] { aip.getCollectionArchiveSize(), collectionSize }));
        }
      }
    } else if (aip.getCollectionArchiveSize() != 0) {
      if (fixAip) {
        aip.setCollectionArchiveSize(0L);
      } else {
        throw new SolidifyCheckingException(
                this.messageService.get("archival.aip.error.collection-size", new Object[] { aip.getCollectionArchiveSize(), 0 }));
      }
    }
  }

  public URI getPackageFile(ArchivalInfoPackage aip, boolean isWorkingAip) {
    if (isWorkingAip) {
      return aip.getDataFiles().get(0).getFinalData();
    }
    return aip.getArchiveUri();
  }

  public Path getDataFolder(ArchiveContainer archiveContainer, Path packageFolder) {
    return switch (archiveContainer) {
      case BAG_IT -> packageFolder.resolve(DLCMConstants.DATA_FOLDER);
      case ZIP -> packageFolder;
      default -> throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { archiveContainer }));
    };
  }
  // *********************
  // ** Protected Methods
  // *********************

  // Build archive key
  protected String getArchiveId(StoredArchiveInterface archive) {
    if (archive instanceof StoredAIP storedAip) {
      return storedAip.getOrganizationalUnitId() + "/" + storedAip.getAipId();
    }
    return archive.getOrganizationalUnitId() + "/" + archive.getResId();
  }

  protected Path generateNewPackage(ArchivalInfoPackage aip, Path packageFolder) throws IOException {
    // Update package info
    switch (aip.getArchiveContainer()) {
      case BAG_IT:
        // Create BagIt
        BagItTool.create(packageFolder, this.supportedChecksumAlgo, true, this.getBagMetadata(aip));
        break;
      case ZIP:
        break;
      default:
        throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { aip.getArchiveContainer() }));
    }
    // Generate new archive
    final Path newPackage = this.getAIPFolder(aip.getResId()).resolve(aip.getResId());
    final ZipTool zipPackage = new ZipTool(newPackage.toUri());
    if (!zipPackage.zipFiles(packageFolder)) {
      throw new SolidifyProcessingException(this.messageService.get("archival.disposal.error.zip"));
    }
    if (!FileSystemUtils.deleteRecursively(packageFolder)) {
      throw new SolidifyProcessingException(this.messageService.get("archival.disposal.error.purge-tombstone"));
    }
    return newPackage;
  }

  protected String getRelativeFolder(ArchiveContainer archiveContainer, Package subFolder) {
    if (!subFolder.isSubFolder()) {
      throw new SolidifyCheckingException(this.messageService.get("archival.aip.error.folder", new Object[] { subFolder.getName() }));
    }

    return switch (archiveContainer) {
      case BAG_IT -> DLCMConstants.DATA_FOLDER + subFolder.getName();
      case ZIP -> subFolder.getName().substring(1);
      default -> throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { archiveContainer }));
    };
  }

  protected String getRelativeFolder(ArchiveContainer archiveContainer) {

    return switch (archiveContainer) {
      case BAG_IT -> DLCMConstants.DATA_FOLDER;
      case ZIP -> Package.ROOT.getName();
      default -> throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { archiveContainer }));
    };
  }

  protected String getMetadataRelativePath(ArchiveContainer archiveContainer) {
    return switch (archiveContainer) {
      case BAG_IT -> DLCMConstants.DATA_FOLDER + "/" + Package.METADATA_FILE.getName();
      case ZIP -> Package.METADATA_FILE.getName();
      default -> throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { archiveContainer }));
    };
  }

  public String getUpdatedMetadataRelativePathPattern(ArchiveContainer archiveContainer) {
    return switch (archiveContainer) {
      case BAG_IT -> DLCMConstants.DATA_FOLDER + "/" + DLCMConstants.REPRESENTATION_INFO_METADATA_FILE_PATTERN;
      case ZIP -> DLCMConstants.REPRESENTATION_INFO_METADATA_FILE_PATTERN;
      default -> throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { archiveContainer }));
    };
  }

  protected String getThumbnailRelativePath(ArchiveContainer archiveContainer) {
    return switch (archiveContainer) {
      case BAG_IT -> DLCMConstants.DATA_FOLDER + "/" + DLCMConstants.INTERNAL_FOLDER + "/"
              + Package.DATASET_THUMBNAIL.getName();
      case ZIP -> DLCMConstants.INTERNAL_FOLDER + "/" + Package.DATASET_THUMBNAIL.getName();
      default -> throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { archiveContainer }));
    };
  }

  protected String getDuaRelativePath(ArchiveContainer archiveContainer) {
    return switch (archiveContainer) {
      case BAG_IT -> DLCMConstants.DATA_FOLDER + "/" + DLCMConstants.INTERNAL_FOLDER + "/"
              + Package.ARCHIVE_DUA.getName();
      case ZIP -> DLCMConstants.INTERNAL_FOLDER + "/" + Package.ARCHIVE_DUA.getName();
      default -> throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { archiveContainer }));
    };
  }

  protected void completeAipInfoWithBagitInfo(Map<String, String> infoList, Path bagitInfoFile) {
    Map<String, String> bagInfoList = BagItTool.readBagItFile(bagitInfoFile);
    // Metadata version
    if (bagInfoList.containsKey(DLCMConstants.BAGIT_METADATA_SCHEMA)) {
      String value = bagInfoList.get(DLCMConstants.BAGIT_METADATA_SCHEMA);
      infoList.put(DLCMConstants.METADATA_VERSION_FIELD, value.substring(10, value.indexOf(".xsd")));
    }
    // Creation
    if (bagInfoList.containsKey(DLCMConstants.BAGIT_DATE)) {
      infoList.put(DLCMConstants.CREATION_FIELD, bagInfoList.get(DLCMConstants.BAGIT_DATE));
    }
    // Retention
    if (bagInfoList.containsKey(DLCMConstants.BAGIT_ARC_RETENTION)) {
      infoList.put(DLCMConstants.AIP_RETENTION, bagInfoList.get(DLCMConstants.BAGIT_ARC_RETENTION));
    }
    // Disposal approval
    if (bagInfoList.containsKey(DLCMConstants.BAGIT_ARC_DISPOSITION_APPROVAL)) {
      infoList.put(DLCMConstants.AIP_DISPOSITION_APPROVAL, bagInfoList.get(DLCMConstants.BAGIT_ARC_DISPOSITION_APPROVAL));
    }
    // Title
    if (bagInfoList.containsKey(DLCMConstants.BAGIT_ARC_NAME)) {
      infoList.put(DLCMConstants.AIP_TITLE, bagInfoList.get(DLCMConstants.BAGIT_ARC_NAME));
    }
    // Size
    String bagitSize = bagInfoList.get(BagItTool.BAGIT_PAYLOAD).substring(0, bagInfoList.get(BagItTool.BAGIT_PAYLOAD).indexOf('.'));
    String bagitNumber = bagInfoList.get(BagItTool.BAGIT_PAYLOAD).substring(bagInfoList.get(BagItTool.BAGIT_PAYLOAD).indexOf('.') + 1);
    if (bagInfoList.containsKey(DLCMConstants.BAGIT_PACKAGE_FILE_NUMBER)
            && !bagitNumber.equals(bagInfoList.get(DLCMConstants.BAGIT_PACKAGE_FILE_NUMBER))) {
      // Tombstone
      infoList.put(DLCMConstants.AIP_SIZE, Long.toString(StringTool.getSizeFromSmartSize(bagInfoList.get(DLCMConstants.BAGIT_PACKAGE_SIZE))));
      infoList.put(DLCMConstants.AIP_TOMBSTONE_SIZE, bagitSize);
      // File number
      infoList.put(DLCMConstants.AIP_FILE_NUMBER, bagInfoList.get(DLCMConstants.BAGIT_PACKAGE_FILE_NUMBER));
    } else {
      // Archive
      infoList.put(DLCMConstants.AIP_SIZE, bagitSize);
      // File number
      infoList.put(DLCMConstants.AIP_FILE_NUMBER, bagitNumber);

    }
  }

  protected void completeAipInfoWithMetadata(Map<String, String> infoList, DLCMMetadataVersion metadataVersion, Path metadataFile)
          throws IOException, JAXBException {
    infoList.putAll(this.metadataService.extractInfoFromMetadata(metadataVersion, metadataFile));
  }

  // Check package validity with Zip integrity and BagIt conformanceAipContainer
  protected void check(ArchivalInfoPackage aip, URI archiveUri, String aipPackagePath, boolean deleteData) throws IOException {
    final ZipTool zip = new ZipTool(archiveUri);
    // Check if zip is correct
    if (!zip.checkZip()) {
      throw new SolidifyCheckingException(this.messageService.get("archival.aip.error.zip.check"));
    }

    Path aipFolder = Paths.get(aipPackagePath);
    // Unzip AIP
    if (!zip.unzipFiles(aipFolder, true)) {
      throw new SolidifyCheckingException(this.messageService.get("archival.aip.error.zip.unzip"));
    }

    // Check how many updates
    long fileNumber = this.getPackageFileNumber(aip.getArchiveContainer(), archiveUri);
    long updateNumber = this.getPackageUpdateNumber(aip.getArchiveContainer(), aipFolder);

    // Check package & metadata
    switch (aip.getArchiveContainer()) {
      case BAG_IT -> {
        // Check if BagIt is correct
        BagItTool.check(DLCMConstants.BAGIT_PROFILE, aipPackagePath);
        // Check DLCM metadata
        final Path bagItMdFile = Paths.get(aipPackagePath, DLCMConstants.DATA_FOLDER, Package.METADATA_FILE.getName());
        this.metadataService.checkMetadataFile(aip.getInfo().getMetadataVersion(), bagItMdFile, new AipDataFile());
        // Check archive file number
        this.checkArchiveFileNumber(aip, fileNumber, updateNumber, false);
      }
      case ZIP -> {
        // Check DLCM metadata
        final Path zipMdFile = Paths.get(aipPackagePath, Package.METADATA_FILE.getName());
        this.metadataService.checkMetadataFile(aip.getInfo().getMetadataVersion(), zipMdFile, new AipDataFile());
        // Check archive file number
        this.checkArchiveFileNumber(aip, fileNumber, updateNumber, false);
      }
      default -> throw new SolidifyCheckingException(this.messageService.get(ERROR_AIP_CONTAINER, new Object[] { aip.getArchiveContainer() }));
    }

    // Check archive size
    this.checkArchiveSize(aip, FileTool.getSize(archiveUri), false);

    // Remove extracted files
    if (deleteData && !FileTool.deleteFolder(aipFolder)) {
      throw new SolidifyCheckingException(this.messageService.get(AIP_ERROR_PURGE_FILES_MSG));
    }
  }

  protected void checkLocation(Path locationPath) {
    if (!FileTool.checkFile(locationPath)) {
      throw new SolidifyCheckingException("Location (" + locationPath + ") does not exist");
    }
  }

  protected void updateHistory(ArchivalInfoPackage aip, String status, String description) {
    final StatusHistory stsHistory = new StatusHistory(aip.getClass().getSimpleName(), aip.getResId(), status);
    stsHistory.setDescription(description);
    SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);
  }

  // *********************
  // ** Private Methods **
  // *********************

  private String checkUpdatePackage(ArchivalInfoPackage aip) {
    final URI updatedPackageFile = aip.getDataFiles().get(0).getFinalData();
    final ZipTool zip = new ZipTool(updatedPackageFile);

    // Check if zip is correct
    if (!zip.checkZip()) {
      throw new SolidifyCheckingException(this.messageService.get("archival.aip.error.zip.check"));
    }

    // Unzip AIP
    final String updatedPackage = this.getAIPFolder(aip.getResId()) + "/" + DLCMConstants.UPDATE_FOLDER;
    Path updatePackagePath = Paths.get(updatedPackage);
    if (!zip.unzipFiles(updatePackagePath, true)) {
      throw new SolidifyCheckingException(this.messageService.get("archival.aip.error.zip.unzip"));
    }

    // Find updated metadata file
    List<Path> updatedMetadataFiles = new ArrayList<>(FileTool.readFolder(updatePackagePath).stream()
            .filter(file -> file.getFileName().toString().startsWith(DLCMConstants.UPDATED_METADATA_FILE_PREFIX))
            .toList());
    if (updatedMetadataFiles.isEmpty()
            || !this.containsPermittedUpdatedFiles(updatedMetadataFiles)) {
      throw new DLCMPreservationException(
              this.messageService.get("archival.aip.error.updated-metadata", new Object[] { updatedPackage }));
    }
    Collections.sort(updatedMetadataFiles);

    // return the last element b/c the files are ascending sorted by date
    return updatedMetadataFiles.get(updatedMetadataFiles.size() - 1).toString();
  }

  private String checkPremisFile(ArchivalInfoPackage aip) {
    final String updatedPackage = this.getAIPFolder(aip.getResId()) + "/" + DLCMConstants.UPDATE_FOLDER;

    // Find updated premis file
    List<Path> premisFiles = new ArrayList<>(FileTool.readFolder(Paths.get(updatedPackage)).stream()
            .filter(file -> file.getFileName().toString().startsWith(DLCMConstants.UPDATED_ADMINISTRATIVE_INFO_FILE))
            .toList());
    Collections.sort(premisFiles);
    // return the last element b/c the files are ascending sorted by date
    return premisFiles.get(premisFiles.size() - 1).toString();
  }

  private List<ArchiveMetadata> splitMetadata(ArchiveMetadata md, ArchivalInfoPackage aip) {
    List<ArchiveMetadata> metadataList = new ArrayList<>();
    ArchiveMetadata publicMd = this.createArchiveMetadata(md.getResId(), this.indexName);
    ArchiveMetadata privateMd = this.createArchiveMetadata(md.getResId(), this.privateIndexName);

    // Reorganize metadata fields => Public/Private/Files
    for (Entry<String, Object> entry : md.getMetadata().entrySet()) {
      if (entry.getKey().startsWith(DLCMConstants.AIP_PREFIX)
              || entry.getKey().equals(DLCMConstants.CREATION_FIELD)
              || entry.getKey().equals(DLCMConstants.METADATA_VERSION_FIELD)) {
        // Common Info
        publicMd.getMetadata().put(entry.getKey(), entry.getValue());
        privateMd.getMetadata().put(entry.getKey(), entry.getValue());
      } else if (entry.getKey().equals(DLCMConstants.DATACITE_FIELD)
              || entry.getKey().equals(DLCMConstants.DATACITE_XML_FIELD)
              || entry.getKey().equals(IdentifierType.DOI.getDescription().toLowerCase())
              || entry.getKey().equals(IdentifierType.ARK.getDescription().toLowerCase())) {
        // Public Info
        publicMd.getMetadata().put(entry.getKey(), entry.getValue());
      } else {
        // Private Info
        if (entry.getKey().equals(DLCMConstants.PREMIS_FIELD)) {
          // PREMIS information
          this.reorganizePremisMetadata(metadataList, privateMd,
                  (Map<String, Object>) (((Map<String, Object>) entry.getValue()).get(DLCMConstants.ITEMS_FIELD)), aip);
        } else {
          privateMd.getMetadata().put(entry.getKey(), entry.getValue());
        }
      }
    }
    // Add private metadata
    metadataList.add(this.mergeCustomMetadata(privateMd));
    // Add public metadata
    metadataList.add(publicMd);
    return metadataList;
  }

  private void reorganizePremisMetadata(List<ArchiveMetadata> metadataList, ArchiveMetadata privateMetadata, Map<String, Object> itemsList,
          ArchivalInfoPackage aip) {
    List<Object> itemList = (List<Object>) (itemsList.get(DLCMConstants.ITEM_FIELD));
    // For each item
    for (Object obj : itemList) {
      Map<String, Object> item = ((Map<String, Object>) obj);
      String type = (String) item.get(DLCMConstants.TYPE_FIELD);
      ArchiveMetadata metadata;
      // Test type
      if (type.equals(DLCMConstants.PACKAGE)) {
        // Packages: deposit/sip/aip
        metadata = privateMetadata;
        // Add security info
        this.addSecurityInfo(metadata, aip);
      } else {
        if (type.equals(DLCMConstants.AIP)) {
          // AIP file
          Map<String, Object> aipInfo = (Map<String, Object>) item.get(DLCMConstants.AIP);
          String aipId = (String) aipInfo.get(DLCMConstants.ID_FIELD);
          metadata = this.createArchiveMetadata(aip.getResId() + "-" + item.get(DLCMConstants.ID_FIELD).toString(),
                  this.privateIndexName);
          aip.getCollection().forEach(archive -> {
            if (archive.getResId().equals(aipId)) {
              metadata.getMetadata().putAll(archive.getAIPMetadata());
            }
          });
        } else {
          // Data file
          metadata = this.createArchiveMetadata((String) item.get(DLCMConstants.ID_FIELD), this.privateIndexName);
        }
        // Add archive ID (AIP)
        metadata.getMetadata().put(DLCMConstants.ARCHIVE_ID_FIELD, aip.getResId());
        // Add security info
        this.addSecurityInfo(metadata, aip);
        // Add new entry
        metadataList.add(metadata);
      }
      // Add different fields
      for (Entry<String, Object> metadataField : item.entrySet()) {
        if (!metadataField.getKey().equals(DLCMConstants.ID_FIELD)) {
          metadata.getMetadata().put(metadataField.getKey(), metadataField.getValue());
        }
      }
    }
  }

  private void addSecurityInfo(ArchiveMetadata metadata, ArchivalInfoPackage aip) {
    // OrgUnit Id
    metadata.getMetadata().put(DLCMConstants.AIP_ORGA_UNIT, aip.getInfo().getOrganizationalUnitId());
    // Access Level
    metadata.getMetadata().put(DLCMConstants.AIP_ACCESS_LEVEL, aip.getInfo().getAccess().toString());
    // Content structure public
    metadata.getMetadata().put(DLCMConstants.AIP_CONTENT_STRUCTURE_PUBLIC, aip.getInfo().getContentStructurePublic());
    // Embargo
    if (aip.getInfo().hasEmbargo()) {
      metadata.getMetadata().put(DLCMConstants.AIP_EMBARGO_LEVEL, aip.getInfo().getEmbargo().getAccess().toString());
      metadata.getMetadata().put(DLCMConstants.AIP_EMBARGO_END_DATE, aip.getInfo().getEmbargo().getEndDate().toString());
    }
  }

  private ArchiveMetadata mergeCustomMetadata(ArchiveMetadata privateMd) {
    // Reorganize custom-metadata/custom-metadata => custom-metadata
    Map<String, Object> customMd = (Map<String, Object>) (privateMd.getMetadata().remove(DLCMConstants.CUSTOM_METADATA_FIELD));
    // Check if there are custom metadata
    if (customMd != null) {
      customMd = (Map<String, Object>) (customMd.remove(DLCMConstants.CUSTOM_METADATA_FIELD));
      privateMd.getMetadata().put(DLCMConstants.CUSTOM_METADATA_FIELD, customMd.get(DLCMConstants.CUSTOM_METADATA_FIELD));
      // Merge Custom metadata info & content
      List<Map<String, Object>> mdList = new ArrayList<>();
      try {
        // list of custom metadata
        mdList.addAll((List<Map<String, Object>>) (privateMd.getMetadata().get(DLCMConstants.CUSTOM_METADATA_FIELD)));
      } catch (ClassCastException e) {
        // Just one custom metadata
        mdList.add((Map<String, Object>) (privateMd.getMetadata().get(DLCMConstants.CUSTOM_METADATA_FIELD)));
      }
      // For each custom metadata
      for (Map<String, Object> md : mdList) {
        // Remove schema fields
        md.entrySet().removeIf(f -> f.getKey().startsWith("xmlns:"));
        // Move metadata content
        String name = (String) (md.get("name"));
        String content = (String) (privateMd.getMetadata().remove(name));
        md.put(DLCMConstants.CONTENT_FIELD, content);
      }
    }
    return privateMd;
  }

  private ArchiveMetadata createArchiveMetadata(String aipId, String indexName) {
    ArchiveMetadata archiveMetadata = new ArchiveMetadata();
    archiveMetadata.setIndex(indexName);
    archiveMetadata.setResId(aipId);
    return archiveMetadata;
  }

  // Filter metadata
  private Map<String, Object> filterMetadata(Map<String, Object> metadata) {
    StringTool.filterMap(metadata, this.indexingExceptions);
    return metadata;
  }

  // Get AIP metadata
  private ArchiveMetadata getAipMetadata(Path workingDir, String index, ArchivalInfoPackage aip) {
    try (InputStream is = this.getArchiveStream(aip)) {
      return this.getMetadataFromInputStream(workingDir, index, aip, is);
    } catch (IOException | JAXBException e) {
      throw new SolidifyProcessingException("Problem in extracting AIP", e);
    }
  }

  private InputStream getArchiveStream(ArchivalInfoPackage aip) throws IOException {
    // Check if there is a local copy
    if (aip.getDataFiles().size() == 1
            && aip.getDataFiles().get(0).getDataType() == DataCategory.InformationPackage
            && FileTool.checkFile(Paths.get(aip.getDataFiles().get(0).getFinalData()))) {
      return new BufferedInputStream(Files.newInputStream(Paths.get(aip.getDataFiles().get(0).getFinalData())));
    } else {
      // Get from storage
      return this.getAIPStream(aip);
    }
  }

  private ArchiveMetadata getMetadataFromInputStream(Path workingDir, String index, ArchivalInfoPackage aip, InputStream is)
          throws IOException, JAXBException {

    final Path destination = Paths.get(workingDir + "/" + DLCMConstants.PACKAGE);
    String metadataFile = this.getMetadataRelativePath(aip.getArchiveContainer());
    String metadataDir = this.getRelativeFolder(aip.getArchiveContainer(), Package.METADATA);
    String updatedMetadataFilesPattern = this.getUpdatedMetadataRelativePathPattern(aip.getArchiveContainer());
    String internalDir = this.getRelativeFolder(aip.getArchiveContainer(), Package.INTERNAL);

    final List<String> filesToExtract = List.of(metadataFile, updatedMetadataFilesPattern, metadataDir + "/**", internalDir + "/**");
    if (ZipTool.unzipFiles(is, destination, filesToExtract, null, true)
            && FileTool.checkFile(destination.resolve(metadataFile))) {
      // DLCM metadata
      ArchiveMetadata metadata = this.toArchiveMetadata(index, aip.getResId(), aip.getInfo().getMetadataVersion(),
              destination.resolve(metadataFile));
      if (metadata == null) {
        throw new SolidifyProcessingException("Problem in loading metadata for '" + aip.getResId() + ARCHIVE_MESSAGE_SUFFIX);
      }
      // Check if there is custom metadata
      if (FileTool.isFolder(destination.resolve(metadataDir))) {
        // Add Custom metadata
        for (final Path p : FileTool.readFolder(destination.resolve(metadataDir))) {
          metadata.getMetadata().put(p.getFileName().toString(), FileTool.toString(p));
        }
      }
      // filter metadata
      metadata.setMetadata(this.filterMetadata(metadata.getMetadata()));

      Path internalPath = destination.resolve(internalDir);
      // check thumbnail if exists
      this.addArchivePublicData(metadata, aip.getResId(), PublicDataFileType.ARCHIVE_THUMBNAIL, internalPath);

      // Delete dua if exists
      this.addArchivePublicData(metadata, aip.getResId(), PublicDataFileType.ARCHIVE_DUA, internalPath);

      // Delete README if exists
      this.addArchivePublicData(metadata, aip.getResId(), PublicDataFileType.ARCHIVE_README, internalPath);

      // Count metadata files : dlcm.xml + updated files
      Long updatedFiles = FileTool.findFiles(destination)
              .stream()
              .filter(f -> f.getFileName().toString().startsWith(DLCMConstants.REPRESENTATION_INFO_METADATA_FILE_PREFIX))
              .count();
      if (!updatedFiles.equals(aip.getUpdateNumber())) {
        aip.setUpdateNumber(updatedFiles);
      }
      // Remove extracted files
      if (!FileTool.deleteFolder(destination)) {
        throw new SolidifyProcessingException("Problem in deleting working folder for '" + aip.getResId() + ARCHIVE_MESSAGE_SUFFIX);
      }
      return metadata;
    }
    throw new SolidifyProcessingException("Problem in extracting metadata for '" + aip.getResId() + ARCHIVE_MESSAGE_SUFFIX);
  }

  private void addArchivePublicData(ArchiveMetadata metadata, String aipId, PublicDataFileType publicDataType, Path target) {
    if (this.archivePublicDataRemoteResourceService.checkArchivePublicData(aipId, publicDataType)
            && !this.archivePublicDataRemoteResourceService.deleteArchivePublicData(aipId, publicDataType)) {
      throw new SolidifyProcessingException(
              "Problem in deleting public data file ('" + publicDataType + "') for '" + aipId + ARCHIVE_MESSAGE_SUFFIX);
    }
    final DLCMMetadataVersion archiveVersion = DLCMMetadataVersion.fromVersion(metadata.getMetadataVersion());
    Path dataFilePath = target.resolve(this.getDataFileName(archiveVersion, publicDataType));
    if (FileTool.checkFile(dataFilePath)) {
      String mimeType = this.getMimeTypeFromFile(metadata, DataCategory.Internal, this.getDataType(archiveVersion, publicDataType));
      this.archivePublicDataRemoteResourceService.createArchivePublicData(aipId, publicDataType, dataFilePath, mimeType);
      metadata.getMetadata().put(ArchivePublicData.getMetadataFieldForPublicData(publicDataType), Boolean.TRUE);
      metadata.getMetadata().put(ArchivePublicData.getMetadataFieldForPublicDataContentType(publicDataType), mimeType);
    } else {
      metadata.getMetadata().put(ArchivePublicData.getMetadataFieldForPublicData(publicDataType), Boolean.FALSE);
    }
  }

  private String getDataFileName(DLCMMetadataVersion version, PublicDataFileType publicDataType) {
    switch (publicDataType) {
      case ARCHIVE_DUA -> {
        return Package.ARCHIVE_DUA.getName();
      }
      case ARCHIVE_README -> {
        return Package.ARCHIVE_README.getName();
      }
      case ARCHIVE_THUMBNAIL -> {
        if (version.getVersionNumber() < DLCMMetadataVersion.V3_1.getVersionNumber()) {
          return Package.DATASET_THUMBNAIL.getName();
        } else {
          return Package.ARCHIVE_THUMBNAIL.getName();
        }
      }
    }
    throw new SolidifyProcessingException("Wrong data file type: " + publicDataType);
  }

  private DataCategory getDataType(DLCMMetadataVersion version, PublicDataFileType publicDataType) {
    switch (publicDataType) {
      case ARCHIVE_DUA -> {
        return DataCategory.ArchiveDataUseAgreement;
      }
      case ARCHIVE_README -> {
        return DataCategory.ArchiveReadme;
      }
      case ARCHIVE_THUMBNAIL -> {
        if (version.getVersionNumber() < DLCMMetadataVersion.V3_1.getVersionNumber()) {
          return DataCategory.DatasetThumbnail;
        } else {
          return DataCategory.ArchiveThumbnail;
        }
      }
    }
    throw new SolidifyProcessingException("Wrong data file type: " + publicDataType);
  }

  private String getMimeTypeFromFile(ArchiveMetadata archiveMetadata, DataCategory dataCategory, DataCategory dataType) {
    // First check premis data category anda datatype to take the file object according to it.
    List<Object> itemList = (List<Object>) ((Map<String, Object>) ((Map<String, Object>) archiveMetadata.getMetadata()
            .get(DLCMConstants.PREMIS_FIELD))
                    .get(DLCMConstants.ITEMS_FIELD))
                            .get(DLCMConstants.ITEM_FIELD);
    for (Object obj : itemList) {
      Map<String, Object> item = ((Map<String, Object>) obj);
      if (item.get(DLCMConstants.TYPE_FIELD).equals("dataFile")) {
        HashMap<String, String> dlcmMetadata = (HashMap<String, String>) item.get("dlcm");
        if (dataCategory.name().equals(dlcmMetadata.get("category")) && dataType.name().equals(dlcmMetadata.get("type"))) {
          // DatasetThumbnail only available for archives with metadata version < 3.1
          if (dataType.equals(DataCategory.DatasetThumbnail)) {
            return ((HashMap<String, String>) item.get("file")).get("mimeType");
          } else {
            return ((HashMap<String, String>) item.get("format")).get("contentType");
          }
        }
      }
    }
    return null;
  }

  private MultiValueMap<String, String> getBagMetadata(ArchivalInfoPackage aip) {
    MultiValueMap<String, String> metadata = new LinkedMultiValueMap<>();
    metadata.add(DLCMConstants.BAGIT_SOURCE,
            this.repositoryName + ", " + this.repositoryInstitution);
    metadata.add(DLCMConstants.BAGIT_SOURCE_ADDR, this.repositoryLocation);
    metadata.add(DLCMConstants.BAGIT_METADATA_SCHEMA, aip.getInfo().getMetadataVersion().getMetsSchema());
    metadata.add(DLCMConstants.BAGIT_ARC_ID, aip.getResId());
    metadata.add(DLCMConstants.BAGIT_ARC_NAME, aip.getInfo().getName());
    metadata.add(DLCMConstants.BAGIT_ARC_RETENTION, aip.getRetention().toString());
    metadata.add(DLCMConstants.BAGIT_ARC_RETENTION_DURATION, aip.getSmartRetention());
    metadata.add(DLCMConstants.BAGIT_ARC_DISPOSITION_APPROVAL, aip.getDispositionApproval().toString());
    metadata.add(DLCMConstants.BAGIT_ORG_UNIT_ID, aip.getInfo().getOrganizationalUnitId());
    metadata.add(DLCMConstants.BAGIT_ORG_UNIT_NAME, this.orgUnitResourceService.findOne(aip.getInfo().getOrganizationalUnitId()).getName());
    metadata.add(DLCMConstants.BAGIT_PACKAGE_SIZE, aip.getSmartSize());
    metadata.add(DLCMConstants.BAGIT_PACKAGE_FILE_NUMBER, aip.getArchiveFileNumber().toString());
    return metadata;
  }

  private ArchiveMetadata toArchiveMetadata(String index, String id, DLCMMetadataVersion version, Path dlcmXml)
          throws IOException, JAXBException {
    final ArchiveMetadata am = new ArchiveMetadata();
    am.setResId(id);
    am.setIndex(index);
    return this.metadataService.completeArchiveIndex(version, am, dlcmXml);
  }

  private boolean containsPermittedUpdatedFiles(List<Path> updatedMetadataFiles) {
    return updatedMetadataFiles.stream().anyMatch(file -> !file.getFileName().toString().startsWith(DLCMConstants.UPDATED_METADATA_FILE_PREFIX)
            || !file.getFileName().toString().endsWith(DLCMConstants.THUMBNAIL_EXTENSION)
            || !file.getFileName().toString().endsWith(DLCMConstants.README_EXTENSION)
            || !file.getFileName().toString().endsWith(DLCMConstants.DUA_EXTENSION));
  }

  private List<FileInfoUpdate> checkAndReplaceUpdatedFiles(Path aipUpdateFolder, Path aipDataFolder, ArchivalInfoPackage aip) {
    List<FileInfoUpdate> filesUpdate = new ArrayList<>();
    List<Path> allUpdatedFiles = new ArrayList<>();
    // Get all files from the updated folder
    log.debug("Check into internal folder {} of aip for updated files.", Path.of(aipUpdateFolder + "/" + DLCMConstants.INTERNAL_FOLDER));
    if (FileTool.checkFile(Path.of(aipUpdateFolder + "/" + DLCMConstants.INTERNAL_FOLDER))) {
      allUpdatedFiles.addAll(FileTool.readFolder(Path.of(aipUpdateFolder + "/" + DLCMConstants.INTERNAL_FOLDER)));
    }
    log.debug("Aip {} has {} files in its internal folder to updated.", aip.getResId(), allUpdatedFiles.size());
    log.debug("Filename of the updated file {}", allUpdatedFiles.isEmpty() ? null : allUpdatedFiles.get(0).getFileName());
    OffsetDateTime aipLastCompletedTime = this.historyService.getLastCompletedUpdate(aip.getResId());
    String lastCompletedTimeStr = aipLastCompletedTime.format(DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT_FOR_FILE));
    log.debug("Date of the last completed time of the AIP {}", lastCompletedTimeStr);

    // Check all files from thumbnail, readme and dua extension and check if their timestamp from the filename is later from the last time
    // the aip was completed, meaning the file has being updated since.
    List<Path> filesToUpdated = allUpdatedFiles.stream().filter(file -> file.getFileName().toString().endsWith(DLCMConstants.THUMBNAIL_EXTENSION)
            || file.getFileName().toString().endsWith(DLCMConstants.README_EXTENSION)
            || file.getFileName().toString().endsWith(DLCMConstants.DUA_EXTENSION))
            .filter(file -> {
              String timestamp = file.getFileName().toString()
                      .substring(file.getFileName().toString().indexOf("-") + 1, file.getFileName().toString().lastIndexOf("."));
              // Check if the file has been created after the last time the aip was completed.
              OffsetDateTime dateTime = LocalDateTime.parse(timestamp, DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT_FOR_FILE))
                      .atZone(ZoneId.systemDefault()).toOffsetDateTime();
              return dateTime.isAfter(aipLastCompletedTime);
            })
            .toList();
    List<Path> allAipDataFiles = new ArrayList<>();
    log.debug("Check into internal folder {} of aip files.", Path.of(aipDataFolder + "/" + DLCMConstants.INTERNAL_FOLDER));
    if (FileTool.checkFile(Path.of(aipDataFolder + "/" + DLCMConstants.INTERNAL_FOLDER))) {
      allAipDataFiles.addAll(FileTool.readFolder(Path.of(aipDataFolder + "/" + DLCMConstants.INTERNAL_FOLDER)));
    }
    log.debug("Aip {} has {} files in its internal folder.", aip.getResId(), allAipDataFiles.size());

    for (Path fileToCopy : filesToUpdated) {
      log.debug("Check for file to update {}", fileToCopy);
      // Get extension of the file to copy as reference
      String extension = fileToCopy.getFileName().toString().substring(fileToCopy.getFileName().toString().lastIndexOf("."));
      // Get files from the aip data folder in order to check if there is a need to rename previous version of thumbnail, readme or dua files.
      List<Path> filesFromThatExtension = allAipDataFiles.stream()
              .filter(p -> p.getFileName().toString().substring(p.getFileName().toString().lastIndexOf(".")).equals(extension)).toList();
      switch (extension) {
        case DLCMConstants.THUMBNAIL_EXTENSION -> {
          FileInfoUpdate fileInfoUpdate = this.renamePreviousReferenceFileAndUpdateReferenceFile(filesFromThatExtension, aipDataFolder,
                  fileToCopy, DLCMConstants.ARCHIVE_THUMBNAIL, lastCompletedTimeStr, DLCMConstants.THUMBNAIL_EXTENSION,
                  DataCategory.ArchiveThumbnail);
          log.debug(FILE_UPDATE_MSG, fileInfoUpdate.getFileType());
          filesUpdate.add(fileInfoUpdate);
        }
        case DLCMConstants.README_EXTENSION -> {
          FileInfoUpdate fileInfoUpdate = this.renamePreviousReferenceFileAndUpdateReferenceFile(filesFromThatExtension, aipDataFolder,
                  fileToCopy,
                  DLCMConstants.ARCHIVE_README, lastCompletedTimeStr, DLCMConstants.README_EXTENSION, DataCategory.ArchiveReadme);
          log.debug(FILE_UPDATE_MSG, fileInfoUpdate.getFileType());
          filesUpdate.add(fileInfoUpdate);
        }
        case DLCMConstants.DUA_EXTENSION -> {
          FileInfoUpdate fileInfoUpdate = this.renamePreviousReferenceFileAndUpdateReferenceFile(filesFromThatExtension, aipDataFolder,
                  fileToCopy, DLCMConstants.ARCHIVE_DUA, lastCompletedTimeStr, DLCMConstants.DUA_EXTENSION,
                  DataCategory.ArchiveDataUseAgreement);
          log.debug(FILE_UPDATE_MSG, fileInfoUpdate.getFileType());
          filesUpdate.add(fileInfoUpdate);
        }
        default -> throw new SolidifyProcessingException("Unknown extension: " + extension);
      }
    }

    return filesUpdate;
  }

  private FileInfoUpdate renamePreviousReferenceFileAndUpdateReferenceFile(List<Path> aipDataFiles, Path folderToCopy, Path fileToCopy,
          String referenceFileName, String timeStamp, String fileExtension, DataCategory type) {
    FileInfoUpdate fileInfoUpdate;
    String newReferenceFileName = null;
    FileInfoUpdate.FileAction fileAction = FileInfoUpdate.FileAction.ADDED;
    try {
      Optional<Path> currentFileReferenceOpt = aipDataFiles.stream()
              .filter(file -> file.getFileName().toString().startsWith(referenceFileName)).findAny();
      if (currentFileReferenceOpt.isPresent()) {
        newReferenceFileName = DLCMConstants.DLCM_ARCHIVE + "-" + timeStamp + fileExtension;
        FileTool.moveFile(currentFileReferenceOpt.get(),
                Path.of(folderToCopy + "/" + DLCMConstants.INTERNAL_FOLDER + "/" + newReferenceFileName));
        fileAction = FileInfoUpdate.FileAction.UPDATED;
      }
      FileTool.moveFile(fileToCopy,
              Path.of(folderToCopy + "/" + DLCMConstants.INTERNAL_FOLDER + "/" + referenceFileName));
      fileInfoUpdate = new FileInfoUpdate(referenceFileName, fileAction, newReferenceFileName, type);

    } catch (IOException e) {
      throw new SolidifyProcessingException("Unable to copy file " + fileToCopy.getFileName() + " to " + folderToCopy);
    }
    return fileInfoUpdate;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - FileStorageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.file;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.BagItTool;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.exception.FixityCheckLevelNotImplementedException;
import ch.dlcm.model.AipCheckLevel;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.Package;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StoredArchiveInterface;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicDataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.storage.StorageService;

@Profile("arc-file")
@Service
@ConditionalOnBean(ArchivalStorageController.class)
public class FileStorageService extends StorageService {

  private static final Logger log = LoggerFactory.getLogger(FileStorageService.class);
  private final Path storagePath;
  private final Path securedStoragePath;

  public FileStorageService(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repositoryDescription,
          MessageService messageService,
          MetadataService metadataService,
          FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService,
          FallbackArchivePublicDataRemoteResourceService archivePublicDataRemoteResourceService,
          FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService,
          FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteResourceService,
          HistoryService historyService) {
    super(dlcmProperties, repositoryDescription, messageService, metadataService, orgUnitResourceService, archivePublicDataRemoteResourceService,
            archivePublicMetadataRemoteResourceService, archivePrivateMetadataRemoteResourceService, historyService);
    this.storagePath = Paths.get(URI.create(dlcmProperties.getStorage().getUrl()));
    this.checkLocation(this.storagePath);
    this.securedStoragePath = Paths.get(URI.create(dlcmProperties.getSecuredStorage().getUrl()));
    this.checkLocation(this.securedStoragePath);
  }

  @Override
  public List<String> checkFixity(ArchivalInfoPackage aip, List<ChecksumAlgorithm> algorithmsToRecalculate, AipCheckLevel fixityCheckLevel)
          throws NoSuchAlgorithmException, IOException {

    if (fixityCheckLevel == AipCheckLevel.DEEP_FIXITY) {
      return this.checkDeepFixity(aip, algorithmsToRecalculate);
    }

    throw new FixityCheckLevelNotImplementedException(
            "FixityCheckLevel '" + fixityCheckLevel.name() + "' not implemented for storage of type " + this.getClass().getSimpleName());
  }

  @Override
  public long getAIPFileNumber(StoredArchiveInterface archive) {
    return this.getPackageFileNumber(archive.getArchiveContainer(), archive.getArchiveUri());
  }

  @Override
  public Map<String, String> getAIPInfo(StoredArchiveInterface aip) throws IOException, JAXBException {
    final Map<String, String> infoList = new HashMap<>();
    final List<String> filesToExtract = new ArrayList<>();
    String metadataRelativePath;
    String researchDataRelativePath;
    // load zip archive
    final ZipTool archive = new ZipTool(aip.getArchiveUri());
    // Determine archive container
    if (archive.contains(BagItTool.BAGIT_FILE)) {
      infoList.put(DLCMConstants.AIP_CONTAINER, ArchiveContainer.BAG_IT.toString());
      // file to extract from archive
      filesToExtract.add(BagItTool.BAGIT_INFO_FILE);
      metadataRelativePath = this.getMetadataRelativePath(ArchiveContainer.BAG_IT);
      researchDataRelativePath = this.getRelativeFolder(ArchiveContainer.BAG_IT, Package.RESEARCH);
    } else {
      infoList.put(DLCMConstants.AIP_CONTAINER, ArchiveContainer.ZIP.toString());
      // file to extract from archive
      metadataRelativePath = this.getMetadataRelativePath(ArchiveContainer.ZIP);
      researchDataRelativePath = this.getRelativeFolder(ArchiveContainer.ZIP, Package.RESEARCH);
    }
    filesToExtract.add(metadataRelativePath);
    // Extract file from archive
    final Path aipPackage = this.getAIPFolder(aip.getResId()).resolve(DLCMConstants.PACKAGE);
    if (archive.unzipFiles(aipPackage, filesToExtract, true)) {
      if (FileTool.checkFile(aipPackage.resolve(BagItTool.BAGIT_INFO_FILE))) {
        this.completeAipInfoWithBagitInfo(infoList, aipPackage.resolve(BagItTool.BAGIT_INFO_FILE));
      }
      if (infoList.containsKey(DLCMConstants.METADATA_VERSION_FIELD)) {
        this.completeAipInfoWithMetadata(infoList, DLCMMetadataVersion.fromVersion(infoList.get(DLCMConstants.METADATA_VERSION_FIELD)),
                aipPackage.resolve(metadataRelativePath));
      }
    }
    // For units, it contains also a folder "researchdata" with the data files.
    if (infoList.containsKey(DLCMConstants.AIP_TOMBSTONE_SIZE) || archive.containsFolder(researchDataRelativePath)) {
      infoList.put(DLCMConstants.AIP_UNIT, String.valueOf(true));
    } else {
      infoList.put(DLCMConstants.AIP_UNIT, String.valueOf(false));
    }
    return infoList;
  }

  @Override
  public long getAIPSize(StoredArchiveInterface aip) {
    return FileTool.getSize(aip.getArchiveUri());
  }

  @Override
  public InputStream getAIPStream(StoredArchiveInterface aip) throws IOException {
    return new BufferedInputStream(Files.newInputStream(Paths.get(aip.getArchiveUri())));
  }

  @Override
  public URI getArchiveLocation(StoredArchiveInterface archive) {
    if (archive.getDataSensitivity().value() < DataTag.ORANGE.value()) {
      return this.storagePath.resolve(this.getArchiveId(archive)).toUri();
    } else {
      return this.securedStoragePath.resolve(this.getArchiveId(archive)).toUri();
    }
  }

  @Override
  public URI findArchiveLocation(StoredArchiveInterface archive) throws URISyntaxException {
    if (FileTool.checkFile(this.storagePath.resolve(this.getArchiveId(archive)))) {
      return this.storagePath.resolve(this.getArchiveId(archive)).toUri();
    }
    if (FileTool.checkFile(this.securedStoragePath.resolve(this.getArchiveId(archive)))) {
      return this.securedStoragePath.resolve(this.getArchiveId(archive)).toUri();
    }
    return null;
  }

  @Override
  public AipCheckLevel getDefaultFixityCheckLevel() {
    return AipCheckLevel.DEEP_FIXITY;
  }

  @Override
  public PackageStatus getStatus(ArchivalInfoPackage aip) {
    // Check AIP
    final Path aipFile = Paths.get(aip.getDataFiles().get(0).getFinalData());
    if (!FileTool.checkFile(aipFile)) {
      return PackageStatus.IN_ERROR;
    }
    // Check file system storage
    final Path aipArchive = Paths.get(this.getArchiveLocation(aip));
    if (FileTool.checkFile(aipArchive)) {
      if (FileTool.getSize(aipFile) == FileTool.getSize(aipArchive)) {
        return PackageStatus.STORED;
      }
      return PackageStatus.IN_PROGRESS;
    }
    return PackageStatus.READY;
  }

  @Override
  public List<StoredAIP> listStoredAip() {
    final List<StoredAIP> list = new ArrayList<>();
    // Normal Storage
    list.addAll(this.listStorageLocation(this.storagePath));
    // Secured Storage
    list.addAll(this.listStorageLocation(this.securedStoragePath));
    return list;
  }

  @Override
  public void storeAIP(ArchivalInfoPackage aip) {
    try {
      if (!FileTool.copyFile(aip.getDataFiles().get(0).getFinalData(), this.getArchiveLocation(aip))) {
        throw new SolidifyProcessingException("Cannot copy AIP data file from AIP '" + aip.getResId() + "'");
      }
    } catch (final IOException e) {
      throw new SolidifyProcessingException("Cannot store AIP file '" + aip.getResId() + "'", e);
    }
  }

  @Override
  public void replaceAIP(ArchivalInfoPackage aip, Path newPackage) {
    try {
      // Delete previous archive
      FileTool.deleteFile(aip.getArchiveUri());
      // Update archive location
      aip.setArchiveId(this.getArchiveLocation(aip).toString());
      // Replace by new package
      FileTool.moveFile(newPackage, Paths.get(aip.getArchiveUri()));
    } catch (final IOException e) {
      throw new SolidifyProcessingException("Cannot replace new AIP file '" + aip.getResId() + "'", e);
    }
  }

  @Override
  public String submitAIP(ArchivalInfoPackage aip) {
    final Path aipFile = Paths.get(aip.getDataFiles().get(0).getFinalData());
    final Path aipArchive = Paths.get(this.getArchiveLocation(aip));
    if (FileTool.checkFile(aipFile) && FileTool.checkFile(aipArchive) && FileTool.getSize(aipFile) == FileTool.getSize(aipArchive)) {
      return aipArchive.toUri().toString();
    }
    return null;
  }

  @Override
  public String checkPackage(ArchivalInfoPackage aip, boolean isWorkingAip, boolean deleteData) throws IOException {
    final URI aipFile = this.getPackageFile(aip, isWorkingAip);
    final Path aipArchive = Paths.get(aipFile);
    if (!FileTool.checkFile(aipArchive) || !FileTool.isFile(aipArchive)) {
      throw new SolidifyCheckingException(this.messageService.get("checking.storage.notfound", new Object[] { aipFile }));
    }
    // Get path to aip package
    String aipPackage = this.getAIPFolder(aip.getResId()) + "/" + DLCMConstants.PACKAGE;
    this.check(aip, aipFile, aipPackage, deleteData);

    return aipPackage;
  }

  /**
   * Recalculate files hashes by reading the file entirely
   *
   * @param aip
   * @param algorithmsToRecalculate
   * @return
   * @throws NoSuchAlgorithmException
   * @throws IOException
   */
  private List<String> checkDeepFixity(ArchivalInfoPackage aip, List<ChecksumAlgorithm> algorithmsToRecalculate)
          throws NoSuchAlgorithmException, IOException {

    return Arrays.asList(ChecksumTool.computeChecksum(this.getAIPStream(aip), algorithmsToRecalculate));
  }

  private List<StoredAIP> listStorageLocation(Path locationPath) {
    final List<StoredAIP> list = new ArrayList<>();
    for (final Path orgUnit : FileTool.scanFolder(locationPath, Files::isDirectory)) {
      for (final Path aip : FileTool.scanFolder(orgUnit, Files::isRegularFile)) {
        final String id = orgUnit.getFileName() + "/" + aip.getFileName();
        try {
          list.add(this.getStoredAip(id));
        } catch (final URISyntaxException e) {
          log.error("Cannot find stored AIP ({})", id, e);
        }
      }
    }
    return list;
  }
}

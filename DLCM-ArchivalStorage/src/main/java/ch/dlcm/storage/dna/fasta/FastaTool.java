/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - FileTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.dna.fasta;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.compound.DNACompoundSet;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.io.DNASequenceCreator;
import org.biojava.nbio.core.sequence.io.FastaStreamer;
import org.biojava.nbio.core.sequence.io.GenericFastaHeaderParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.FileTool;

public class FastaTool {
  private static final Logger log = LoggerFactory.getLogger(FastaTool.class);

  /////////
  // DNA //
  /////////

  public static boolean checkDNAFasta(URI uri) {
    return checkDNAFasta(Paths.get(uri));
  }

  public static boolean checkDNAFasta(Path path) {
    try {
      StrictFastaReaderHelper.readFastaDNASequence(path.toFile());
      return true;
    } catch (IOException | CompoundNotFoundException e) {
      log.error("Check DNA Fasta file failed: {}", e.getMessage(), e);
      return false;
    }
  }

  public static DNASequence getDynamicFirstSequence(URI uri) {
    return getDynamicFirstSequence(Paths.get(uri));
  }

  public static DNASequence getDynamicFirstSequence(Path path) {
    try (InputStream inputStream = FileTool.getInputStream(path)) {
      StrictFastaReader<DNASequence, NucleotideCompound> fastaReader = new StrictFastaReader<>(
              inputStream,
              new DnamicFastaHeaderParser<>(),
              new DNASequenceCreator(DNACompoundSet.getDNACompoundSet()));
      Map<String, DNASequence> sequences = fastaReader.process(1);
      if (sequences.size() == 0) {
        throw new SolidifyCheckingException("No DNA sequence found");
      }
      if (sequences.size() > 1) {
        throw new SolidifyCheckingException("Wrong DNA sequence number: " + sequences.size());
      }
      return sequences.entrySet().iterator().next().getValue();
    } catch (IOException | CompoundNotFoundException e) {
      throw new SolidifyRuntimeException("Cannot find first DNA sequence", e);
    }
  }

  public static String toString(InputStream inputStream) throws IOException {
    final StringBuilder resultStringBuilder = new StringBuilder();

    try {
      StrictFastaReader<DNASequence, NucleotideCompound> fastaReader = new StrictFastaReader<>(
              inputStream,
              new GenericFastaHeaderParser<>(),
              new DNASequenceCreator(DNACompoundSet.getDNACompoundSet()));

      int seqNumber = 0;
      Map<String, DNASequence> dnaSequences;

      while ((dnaSequences = fastaReader.process(10)) != null) {
        for (Map.Entry<String, DNASequence> dnaSequence : dnaSequences.entrySet()) {
          seqNumber++;
          resultStringBuilder
                  .append(seqNumber)
                  .append(" : ")
                  .append(dnaSequence.getKey())
                  .append("\n")
                  .append(dnaSequence.getValue())
                  .append("\n");
        }

      }
      resultStringBuilder
              .append("\nTotal: ")
              .append(seqNumber)
              .append(" sequence(s)");

    } catch (Exception e) {
      log.error("Cannot read Fasta: {}", e.getMessage(), e);
    }
    return resultStringBuilder.toString();
  }

  /////////
  // RNA //
  /////////

  public static boolean checkRNAFasta(URI uri) {
    return checkRNAFasta(Paths.get(uri));
  }

  public static boolean checkRNAFasta(Path path) {
    try {
      StrictFastaReaderHelper.readFastaRNASequence(path.toFile());
      return true;
    } catch (IOException | CompoundNotFoundException e) {
      log.error("Check RNA Fasta file failed: {}", e.getMessage(), e);
      return false;
    }
  }

  /////////////
  // Protein //
  /////////////

  public static boolean checkProteinFasta(URI uri) {
    return checkProteinFasta(Paths.get(uri));
  }

  public static boolean checkProteinFasta(Path path) {
    try {
      StrictFastaReaderHelper.readFastaProteinSequence(path.toFile());
      return true;
    } catch (IOException | CompoundNotFoundException e) {
      log.error("Check Protein Fasta file failed: {}", e.getMessage(), e);
      return false;
    }
  }

  /////////////
  // Generic //
  /////////////

  public static void logFasta(Path path) {
    FastaStreamer
            .from(path)
            .stream()
            .forEach(sequence -> log.info("{} -> {}", sequence.getOriginalHeader(), sequence.getSequenceAsString()));
  }

  /**
   * This constructor should not be called
   */
  private FastaTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}

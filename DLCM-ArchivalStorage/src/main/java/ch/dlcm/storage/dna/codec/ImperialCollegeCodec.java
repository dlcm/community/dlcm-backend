/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - ImperialCollegeCodec.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.dna.codec;

import java.net.URI;

import org.apache.commons.lang3.NotImplementedException;

import ch.unige.solidify.exception.SolidifyCheckingException;

import ch.dlcm.config.DLCMProperties.Storage.DNAProperties;
import ch.dlcm.storage.dna.DNACodec;
import ch.dlcm.storage.dna.DNACodecType;

public class ImperialCollegeCodec extends DNACodec {

  public ImperialCollegeCodec(DNAProperties dnaProperties) {
    super(dnaProperties);
    if (this.getDnaCodecType() != DNACodecType.IC_CODEC) {
      throw new SolidifyCheckingException(
              "Wrong DNA configuration: " + this.getClass().getSimpleName() + " <> " + this.getDnaCodecType().getName());
    }
  }

  @Override
  public URI encode(String id, String name, URI aipUri) {
    throw new NotImplementedException(this.getDnaCodecType().getName());
  }

  @Override
  public URI decode(URI dnaFastaFile) {
    throw new NotImplementedException(this.getDnaCodecType().getName());
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - DNAFastaReader.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.dna.fasta;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.io.BufferedReaderBytesRead;
import org.biojava.nbio.core.sequence.io.template.SequenceCreatorInterface;
import org.biojava.nbio.core.sequence.io.template.SequenceHeaderParserInterface;
import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StrictFastaReader<S extends Sequence<?>, C extends Compound> {

  private static final Logger logger = LoggerFactory.getLogger(StrictFastaReader.class);

  private SequenceCreatorInterface<C> sequenceCreator;
  private SequenceHeaderParserInterface<S, C> headerParser;
  private BufferedReaderBytesRead bufferedReader;
  private InputStreamReader inputStreanReader;
  private FileInputStream fileInputStream = null;
  private long fileIndex = 0;
  private long sequenceIndex = 0;
  private int maxLineSize = 80;
  private String savedLine = "";
  private String savedHeader = "";

  /**
   * If you are going to use FileProxyProteinSequenceCreator then do not use this constructor because we need details about
   * local file offsets for quick reads. InputStreams does not give you the name of the stream to access quickly via file seek. A seek in
   * an inputstream is forced to read all the data so you don't gain anything.
   *
   * @param is inputStream
   * @param headerParser
   * @param sequenceCreator
   */
  public StrictFastaReader(InputStream is, SequenceHeaderParserInterface<S, C> headerParser, SequenceCreatorInterface<C> sequenceCreator) {
    this.headerParser = headerParser;
    this.inputStreanReader = new InputStreamReader(is);
    this.bufferedReader = new BufferedReaderBytesRead(this.inputStreanReader);
    this.sequenceCreator = sequenceCreator;
  }

  /**
   * If you are going to use the FileProxyProteinSequenceCreator then you
   * need to use this constructor because we need details about
   * the location of the file.
   *
   * @param file
   * @param headerParser
   * @param sequenceCreator
   * @throws FileNotFoundException if the file does not exist, is a directory
   * rather than a regular file, or for some other reason cannot be opened
   * for reading.
   * @throws SecurityException if a security manager exists and its checkRead
   * method denies read access to the file.
   */
  public StrictFastaReader(File file, SequenceHeaderParserInterface<S, C> headerParser, SequenceCreatorInterface<C> sequenceCreator)
          throws FileNotFoundException {
    this.headerParser = headerParser;
    this.fileInputStream = new FileInputStream(file);
    this.inputStreanReader = new InputStreamReader(this.fileInputStream);
    this.bufferedReader = new BufferedReaderBytesRead(this.inputStreanReader);
    this.sequenceCreator = sequenceCreator;
  }

  /**
   * The parsing is done in this method.<br>
   * This method tries to process all the available fasta records
   * in the File or InputStream, closes the underlying resource,
   * and return the results in {@link LinkedHashMap}.<br>
   * You don't need to call {@link #close()} after calling this method.
   *
   * @see #process(int)
   * @return {@link HashMap} containing all the parsed fasta records
   * present, starting current fileIndex onwards.
   * @throws IOException if an error occurs reading the input file
   * @throws CompoundNotFoundException
   */
  public Map<String, S> process() throws IOException, CompoundNotFoundException {
    Map<String, S> sequences = this.process(-1);
    this.close();
    return sequences;
  }

  /**
   * This method tries to parse maximum <code>max</code> records from
   * the open File or InputStream, and leaves the underlying resource open.<br>
   * Subsequent calls to the same method continue parsing the rest of the file.<br>
   * This is particularly useful when dealing with very big data files,
   * (e.g. NCBI nr database), which can't fit into memory and will take long
   * time before the first result is available.<br>
   * <b>N.B.</b>
   * <ul>
   * <li>This method can't be called after calling its NO-ARGUMENT twin.</li>
   * <li>remember to close the underlying resource when you are done.</li>
   * </ul>
   *
   * @see #process()
   * @author Amr ALHOSSARY
   * @since 3.0.6
   * @param max maximum number of records to return, <code>-1</code> for infinity.
   * @return {@link HashMap} containing maximum <code>max</code> parsed fasta records
   * present, starting current fileIndex onwards.
   * @throws IOException if an error occurs reading the input file
   * @throws CompoundNotFoundException
   */
  public Map<String, S> process(int max) throws IOException, CompoundNotFoundException {
    StringBuilder sb = new StringBuilder();
    int processedSequences = 0;
    boolean keepGoing = true;

    Map<String, S> sequences = new LinkedHashMap<>();

    // Read iteration
    String currentLine = "";
    if (this.savedLine != null && !this.savedLine.isEmpty()) {
      currentLine = this.savedLine;
    }
    String currentHeader = "";
    if (this.savedHeader != null && !this.savedHeader.isEmpty()) {
      currentHeader = this.savedHeader;
    }

    do {
      currentLine = currentLine.trim(); // nice to have but probably not needed
      if (!currentLine.isEmpty()) {
        if (currentLine.startsWith(">")) {// start of new fasta record
          if (sb.length() > 0) {
            // i.e. if there is already a sequence before
            S sequence = this.extractSequence(currentHeader, sb);
            sequences.put(sequence.getAccession().getID(), sequence);
            processedSequences++;
            sb.setLength(0); // this is faster than allocating new buffers, better memory utilization (same buffer)
          }
          currentHeader = currentLine.substring(1);
        } else if (currentLine.startsWith(";")) {
          // Comment line
        } else {
          // mark the start of the sequence with the fileIndex before the line was read
          if (sb.length() == 0) {
            this.sequenceIndex = this.fileIndex;
          }
          // Check line size upto MAX_LINE_SIZE characters (default 80)
          if (currentLine.length() > this.maxLineSize) {
            throw new IOException("Line size too long: " + currentLine.length());
          }
          sb.append(currentLine);
        }
      }

      // Read next line
      this.fileIndex = this.bufferedReader.getBytesRead();
      currentLine = this.bufferedReader.readLine();

      if (currentLine == null) {
        // i.e. EOF
        if (sb.length() == 0 && !currentHeader.isEmpty()) {
          logger.warn("Can't parse sequence {}. Got sequence of length 0!", this.sequenceIndex);
          logger.warn("header: {}", currentHeader);
          currentHeader = null;
        } else if (sb.length() > 0) {
          S sequence = this.extractSequence(currentHeader, sb);
          sequences.put(sequence.getAccession().getID(), sequence);
          processedSequences++;
          currentHeader = null;
        }
        keepGoing = false;
      }
      if (max > -1 && processedSequences >= max) {
        keepGoing = false;
      }
    } while (keepGoing);

    this.savedLine = currentLine;
    this.savedHeader = currentHeader;

    return max > -1 && sequences.isEmpty() ? null : sequences;
  }

  private S extractSequence(String header, StringBuilder sb) throws CompoundNotFoundException, IOException {
    logger.debug("Sequence index={} {}", this.sequenceIndex, this.fileIndex);
    @SuppressWarnings("unchecked")
    S sequence = (S) this.sequenceCreator.getSequence(sb.toString(), this.sequenceIndex);
    this.headerParser.parseHeader(header, sequence);
    return sequence;
  }

  public void close() throws IOException {
    this.bufferedReader.close();
    this.inputStreanReader.close();
    // If stream was created from File object then we need to close it
    if (this.fileInputStream != null) {
      this.fileInputStream.close();
    }
    this.savedLine = this.savedHeader = null;
  }

}

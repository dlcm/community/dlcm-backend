/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - DNACodec.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.dna;

import java.net.URI;
import java.util.List;

import org.biojava.nbio.core.sequence.DNASequence;

import ch.dlcm.config.DLCMProperties.Storage.DNAProperties;
import ch.dlcm.storage.dna.fasta.FastaTool;

public abstract class DNACodec {
  protected static final String FASTA_EXTENTION = ".fasta";

  private String microFactory;
  private DNACodecType dnaCodecType;
  private String mainPrimer;

  protected DNACodec(DNAProperties dnaProperties) {
    this.microFactory = dnaProperties.getMicroFactory();
    this.dnaCodecType = DNACodecType.fromName(dnaProperties.getCodecType());
    this.mainPrimer = dnaProperties.getMainPrimer();
  }

  /**
   * The function encodes an archive file in DNA archive file.
   * It generates DNA sequences in Fasta format with the description (see below).
   *
   * @param id: archive identifier (String)
   * @param name: archive name (String)
   * @param aipUri: archive file (URI)
   * @return DNA archive file (URI) in Fasta format
   * ==========================================================
   * Fasta description:
   * -----------------
   * - First sequence
   * => dnamic|<archive id>|<sequence id>|parameter1=<value1>|...|parameterN=<valueN>|<archive name>
   * - Others sequences
   * => dnamic|<archive id>|<sequence id>|<archive name>
   */
  public abstract URI encode(String id, String name, URI aipUri);

  /**
   * The function decodes a DNA archive file in an archive file.
   * It reads the first Fasta sequence description to get parameters if needed.
   * Then, it generates the binary file by decoding DNA sequences with the parameters.
   *
   * @param dnaFastaFile
   * @return
   */
  public abstract URI decode(URI dnaFastaFile);

  protected String getMicroFactory() {
    return this.microFactory;
  }

  protected DNACodecType getDnaCodecType() {
    return this.dnaCodecType;
  }

  protected URI generateDnaArchiveFileName(URI aipUri) {
    return URI.create(aipUri.toString() + FASTA_EXTENTION);
  }

  protected URI generateArchiveFileName(URI fastaFile) {
    return URI.create(fastaFile.toString().replaceAll(FASTA_EXTENTION, ""));
  }

  protected List<String> extractDnaArchiveInfo(URI fastaFile) {
    final DNASequence dnaSequence = FastaTool.getDynamicFirstSequence(fastaFile);
    return dnaSequence.getComments();
  }

  protected String getMainPrimer() {
    return this.mainPrimer;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - DNAFastaReader.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.dna.fasta;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.RNASequence;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompoundSet;
import org.biojava.nbio.core.sequence.compound.DNACompoundSet;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.compound.RNACompoundSet;
import org.biojava.nbio.core.sequence.io.DNASequenceCreator;
import org.biojava.nbio.core.sequence.io.FastaSequenceParser;
import org.biojava.nbio.core.sequence.io.FileProxyDNASequenceCreator;
import org.biojava.nbio.core.sequence.io.FileProxyRNASequenceCreator;
import org.biojava.nbio.core.sequence.io.GenericFastaHeaderParser;
import org.biojava.nbio.core.sequence.io.ProteinSequenceCreator;
import org.biojava.nbio.core.sequence.io.RNASequenceCreator;

import ch.unige.solidify.SolidifyConstants;

/**
 * @author Scooter Willis <willishf at gmail dot com>
 */
public class StrictFastaReaderHelper {

  /**
   * Selecting lazySequenceLoad=true will parse the FASTA file and figure out the accessionid and offsets and return sequence objects
   * that can in the future read the sequence from the disk. This allows the loading of large fasta files where you are only interested
   * in one sequence based on accession id.
   *
   * @param file
   * @param lazySequenceLoad
   * @return
   * @throws IOException
   * @throws CompoundNotFoundException
   */
  public static Map<String, DNASequence> readFastaDNASequence(File file, boolean lazySequenceLoad)
          throws IOException, CompoundNotFoundException {
    if (!lazySequenceLoad) {
      return readFastaDNASequence(file);
    }

    StrictFastaReader<DNASequence, NucleotideCompound> fastaProxyReader = new StrictFastaReader<>(
            file,
            new GenericFastaHeaderParser<>(),
            new FileProxyDNASequenceCreator(
                    file,
                    DNACompoundSet.getDNACompoundSet(),
                    new FastaSequenceParser()));
    return fastaProxyReader.process();

  }

  /**
   * Selecting lazySequenceLoad=true will parse the FASTA file and figure out the accessionid and offsets and return sequence objects
   * that can in the future read the sequence from the disk. This allows the loading of large fasta files where you are only interested
   * in one sequence based on accession id.
   *
   * @param file
   * @param lazySequenceLoad
   * @return
   * @throws IOException
   * @throws CompoundNotFoundException
   */
  public static Map<String, RNASequence> readFastaRNASequence(File file, boolean lazySequenceLoad)
          throws IOException, CompoundNotFoundException {
    if (!lazySequenceLoad) {
      return readFastaRNASequence(file);
    }

    StrictFastaReader<RNASequence, NucleotideCompound> fastaProxyReader = new StrictFastaReader<>(
            file,
            new GenericFastaHeaderParser<>(),
            new FileProxyRNASequenceCreator(
                    file,
                    RNACompoundSet.getRNACompoundSet(),
                    new FastaSequenceParser()));
    return fastaProxyReader.process();

  }

  /**
   * Read a fasta file containing amino acids with setup that would handle most
   * cases.
   *
   * @param file
   * @return
   * @throws IOException
   * @throws CompoundNotFoundException
   */
  public static Map<String, ProteinSequence> readFastaProteinSequence(File file) throws IOException, CompoundNotFoundException {
    FileInputStream inStream = new FileInputStream(file);
    Map<String, ProteinSequence> proteinSequences = readFastaProteinSequence(inStream);
    inStream.close();
    return proteinSequences;
  }

  /**
   * Read a fasta file containing amino acids with setup that would handle most
   * cases. User is responsible for closing InputStream because you opened it
   *
   * @param inStream
   * @return
   * @throws IOException
   * @throws CompoundNotFoundException
   */
  public static Map<String, ProteinSequence> readFastaProteinSequence(
          InputStream inStream) throws IOException, CompoundNotFoundException {
    StrictFastaReader<ProteinSequence, AminoAcidCompound> fastaReader = new StrictFastaReader<>(
            inStream,
            new GenericFastaHeaderParser<>(),
            new ProteinSequenceCreator(AminoAcidCompoundSet.getAminoAcidCompoundSet()));
    return fastaReader.process();
  }

  /**
   * Read a fasta DNA sequence
   *
   * @param inStream
   * @return
   * @throws IOException
   * @throws CompoundNotFoundException
   */
  public static Map<String, DNASequence> readFastaDNASequence(          InputStream inStream) throws IOException, CompoundNotFoundException {
    StrictFastaReader<DNASequence, NucleotideCompound> fastaReader = new StrictFastaReader<>(
            inStream,
            new GenericFastaHeaderParser<>(),
            new DNASequenceCreator(DNACompoundSet.getDNACompoundSet()));
    return fastaReader.process();
  }

  /**
   * @param file
   * @return
   * @throws IOException
   * @throws CompoundNotFoundException
   */
  public static Map<String, DNASequence> readFastaDNASequence(File file) throws IOException, CompoundNotFoundException {
    FileInputStream inStream = new FileInputStream(file);
    Map<String, DNASequence> dnaSequences = readFastaDNASequence(inStream);
    inStream.close();
    return dnaSequences;
  }

  /**
   * Read a fasta RNA sequence
   *
   * @param inStream
   * @return
   * @throws IOException
   * @throws CompoundNotFoundException
   */
  public static Map<String, RNASequence> readFastaRNASequence(InputStream inStream) throws IOException, CompoundNotFoundException {
    StrictFastaReader<RNASequence, NucleotideCompound> fastaReader = new StrictFastaReader<>(
            inStream,
            new GenericFastaHeaderParser<>(),
            new RNASequenceCreator(RNACompoundSet.getRNACompoundSet()));
    return fastaReader.process();
  }

  /**
   * @param file
   * @return
   * @throws IOException
   * @throws CompoundNotFoundException
   */
  public static Map<String, RNASequence> readFastaRNASequence(File file) throws IOException, CompoundNotFoundException {
    FileInputStream inStream = new FileInputStream(file);
    Map<String, RNASequence> rnaSequences = readFastaRNASequence(inStream);
    inStream.close();
    return rnaSequences;
  }

  private StrictFastaReaderHelper() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - DNAStorageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.dna;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jakarta.xml.bind.JAXBException;

import org.apache.commons.lang3.NotImplementedException;
import org.biojava.nbio.core.sequence.DNASequence;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMProperties.Storage.DNAProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.exception.FixityCheckLevelNotImplementedException;
import ch.dlcm.model.AipCheckLevel;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StoredArchiveInterface;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicDataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.storage.StorageService;
import ch.dlcm.storage.dna.codec.ImperialCollegeCodec;
import ch.dlcm.storage.dna.codec.TestCodec;
import ch.dlcm.storage.dna.codec.UNIGECodec;
import ch.dlcm.storage.dna.codec.UNIGEPythonCodec;
import ch.dlcm.storage.dna.fasta.FastaTool;

@Profile("arc-dna")
@Service
@ConditionalOnBean(ArchivalStorageController.class)
public class DNAStorageService extends StorageService {
  private static final String PRIMER_EXTENSION = ".primer";
  private static final String METADATA_EXTENSION = ".metadata";
  private static final String FASTA_EXTENSION = ".fasta";

  private final DNACodec dnaCodec;
  private final Path storagePath;
  private final DNACodec securedDnaCodec;
  private final Path securedStoragePath;

  public DNAStorageService(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repositoryDescription,
          MessageService messageService,
          MetadataService metadataService,
          FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService,
          FallbackArchivePublicDataRemoteResourceService archivePublicDataRemoteResourceService,
          FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteResourceService,
          FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteResourceService,
          HistoryService historyService) {
    super(dlcmProperties, repositoryDescription, messageService, metadataService, orgUnitResourceService, archivePublicDataRemoteResourceService,
            archivePublicMetadataRemoteResourceService, archivePrivateMetadataRemoteResourceService, historyService);
    this.dnaCodec = this.getDNACodec(dlcmProperties.getStorage().getDnaProperties());
    this.storagePath = Paths.get(URI.create(dlcmProperties.getStorage().getUrl()));
    this.checkLocation(this.storagePath);
    this.securedDnaCodec = this.getDNACodec(dlcmProperties.getSecuredStorage().getDnaProperties());
    this.securedStoragePath = Paths.get(URI.create(dlcmProperties.getSecuredStorage().getUrl()));
    this.checkLocation(this.securedStoragePath);
  }

  @Override
  public List<String> checkFixity(ArchivalInfoPackage aip, List<ChecksumAlgorithm> algorithmsToRecalculate, AipCheckLevel fixityCheckLevel)
          throws NoSuchAlgorithmException, IOException {
    throw new FixityCheckLevelNotImplementedException(
            "FixityCheckLevel '" + fixityCheckLevel.name() + "' not implemented for storage of type " + this.getClass().getSimpleName());
  }

  @Override
  public long getAIPFileNumber(StoredArchiveInterface archive) {
    throw new NotImplementedException();
  }

  @Override
  public Map<String, String> getAIPInfo(StoredArchiveInterface aip) throws IOException, JAXBException {
    throw new NotImplementedException();
  }

  @Override
  public long getAIPSize(StoredArchiveInterface aip) {
    throw new NotImplementedException();
  }

  @Override
  public InputStream getAIPStream(StoredArchiveInterface aip) throws IOException {
    if (!FileTool.checkFile(this.getArchiveLocation(aip))) {
      // TODO : DNAMIC get DNA archive from Micro-factory
    }
    // Check DNA archive if it is a valid FASTA file
    FastaTool.checkDNAFasta(this.getArchiveLocation(aip));
    // Decode DNA archive to get AIP with the DNA Codec
    URI dnaFile = this.getDNACodec(aip).decode(this.getArchiveLocation(aip));
    return new BufferedInputStream(Files.newInputStream(Paths.get(dnaFile)));
  }

  @Override
  public URI getArchiveLocation(StoredArchiveInterface archive) {
    if (archive.getDataSensitivity().value() < DataTag.ORANGE.value()) {
      return this.storagePath.resolve(this.getArchiveId(archive)).toUri();
    } else {
      return this.securedStoragePath.resolve(this.getArchiveId(archive)).toUri();
    }
  }

  @Override
  public URI findArchiveLocation(StoredArchiveInterface archive) throws URISyntaxException {
    throw new NotImplementedException();
  }

  @Override
  public AipCheckLevel getDefaultFixityCheckLevel() {
    return AipCheckLevel.DEEP_FIXITY;
  }

  @Override
  public PackageStatus getStatus(ArchivalInfoPackage aip) {
    // Check AIP
    final Path aipFile = Paths.get(aip.getDataFiles().get(0).getFinalData());
    if (!FileTool.checkFile(aipFile)) {
      return PackageStatus.IN_ERROR;
    }
    // Check file system storage
    final Path aipArchive = Paths.get(this.getArchiveLocation(aip));
    if (FileTool.checkFile(aipArchive)) {
      if (FileTool.getSize(aipFile) == FileTool.getSize(aipArchive)) {
        return PackageStatus.STORED;
      }
      return PackageStatus.IN_PROGRESS;
    }
    return PackageStatus.READY;
  }

  @Override
  public List<StoredAIP> listStoredAip() {
    throw new NotImplementedException();
  }

  @Override
  public void storeAIP(ArchivalInfoPackage aip) {
    try {
      // Encode primer file
      URI primerFile = this.generateFile(this.getAIPFolder(aip.getResId()).resolve(PRIMER_EXTENSION), aip.getResId());
      this.generateFastaFile(aip, primerFile, this.getFastaFileName(aip, PRIMER_EXTENSION + FASTA_EXTENSION).toUri());

      // Encode metadata file
      StringBuilder sb = new StringBuilder();
      for (final Entry<String, Object> metadata : aip.getAIPMetadata().entrySet()) {
        sb.append(metadata.getKey())
                .append("=")
                .append(metadata.getValue())
                .append("\n");
      }
      URI metadataFile = this.generateFile(this.getAIPFolder(aip.getResId()).resolve(METADATA_EXTENSION), sb.toString());
      this.generateFastaFile(aip, metadataFile, this.getFastaFileName(aip, METADATA_EXTENSION + FASTA_EXTENSION).toUri());

      // Encode AIP on DNA archive with the DNA Codec
      this.generateFastaFile(aip, aip.getDataFiles().get(0).getFinalData(), this.getFastaFileName(aip, FASTA_EXTENSION).toUri());

      // Copy AIP file
      if (!FileTool.copyFile(aip.getDataFiles().get(0).getFinalData(), Paths.get(this.getArchiveLocation(aip)).toUri())) {
        throw new SolidifyProcessingException("Cannot copy AIP data file from AIP '" + aip.getResId() + "'");
      }
      // TODO : DNAMIC send to Micro-factory
    } catch (final IOException e) {
      throw new SolidifyProcessingException("Cannot store AIP file '" + aip.getResId() + "'", e);
    }
  }

  @Override
  public void replaceAIP(ArchivalInfoPackage aip, Path newPackage) {
    throw new NotImplementedException();
  }

  @Override
  public String submitAIP(ArchivalInfoPackage aip) {
    final DNACodec dnaCodec = this.getDNACodec(aip);
    final DNASequence dnaSequence = FastaTool.getDynamicFirstSequence(this.getFastaFileName(aip, FASTA_EXTENSION));
    final MultiValueMap<String, String> codecParams = this.getCodecParameters(dnaSequence.getComments());
    // TODO : DNAMIC get URI of DNA archive from Micro-factory
    UriComponents dnaURI = UriComponentsBuilder
            .newInstance()
            .scheme("dna")
            .host(dnaCodec.getMicroFactory())
            .path("/" + dnaCodec.getDnaCodecType().getName()
                    + "/" + dnaCodec.getMainPrimer()
                    + "/" + this.getArchiveId(aip))
            .queryParams(codecParams)
            .build();
    return dnaURI.toUriString();
  }

  private MultiValueMap<String, String> getCodecParameters(List<String> comments) {
    final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    comments
            .stream()
            .filter(s -> s.contains("="))
            .forEach(s -> {
              String[] p = s.split("=");
              params.add(p[0], p[1]);
            });
    return params;
  }

  @Override
  public String checkPackage(ArchivalInfoPackage aip, boolean isWorkingAip, boolean deleteData) throws IOException {
    final URI aipFile = this.getPackageFile(aip, isWorkingAip);
    final Path aipArchive = Paths.get(aipFile);
    if (!FileTool.checkFile(aipArchive) || !FileTool.isFile(aipArchive)) {
      throw new SolidifyCheckingException(this.messageService.get("checking.storage.notfound", new Object[] { aipFile }));
    }
    // Get path to aip package
    String aipPackage = this.getAIPFolder(aip.getResId()) + "/" + DLCMConstants.PACKAGE;
    this.check(aip, aipFile, aipPackage, deleteData);

    return aipPackage;
  }

  private DNACodec getDNACodec(DNAProperties dnaProperties) {
    return switch (DNACodecType.fromName(dnaProperties.getCodecType())) {
      case IC_CODEC -> new ImperialCollegeCodec(dnaProperties);
      case UNIGE_CODEC -> new UNIGECodec(dnaProperties);
      case UNIGE_PYTHON_CODEC -> new UNIGEPythonCodec(dnaProperties);
      case TEST_CODEC -> new TestCodec(dnaProperties);
      default -> throw new SolidifyCheckingException("No DNA codec: " + dnaProperties.getCodecType());
    };
  }

  private DNACodec getDNACodec(StoredArchiveInterface archive) {
    if (archive.getDataSensitivity().value() < DataTag.ORANGE.value()) {
      return this.dnaCodec;
    } else {
      return this.securedDnaCodec;
    }
  }

  private URI generateFile(Path file, String content) throws IOException {
    final InputStream inputStream = new ByteArrayInputStream(content.getBytes());
    Files.copy(inputStream, file);
    return file.toUri();
  }

  private void generateFastaFile(ArchivalInfoPackage aip, URI sourceFile, URI targetFile) throws IOException {
    // Encode file in FastA
    URI fastaFile = this.getDNACodec(aip).encode(aip.getResId(), aip.getInfo().getName(), sourceFile);
    // Check file if it is a valid FASTA file
    FastaTool.checkDNAFasta(fastaFile);
    // Copy Fasta file in DNA cache storage
    if (!FileTool.copyFile(fastaFile, targetFile)) {
      throw new SolidifyProcessingException("Cannot copy '" + sourceFile.toString() + "' to '" + sourceFile.toString() + "'");
    }
  }

  private Path getFastaFileName(ArchivalInfoPackage aip, String suffix) {
    final Path archivePath = Paths.get(this.getArchiveLocation(aip));
    return archivePath.getParent().resolve(archivePath.getFileName().toString() + suffix);
  }
}

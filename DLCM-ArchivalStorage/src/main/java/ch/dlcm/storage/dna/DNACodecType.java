/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - DNACodecType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.dna;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.validation.ValidationError;

@Schema(description = """
        DNA codec:
        - NO_CODEC => No DNA codex
        - UNIGE_PYTHON_CODEC => UNIGE DNA codec in Python
        - UNIGE_CODEC => UNIGE DNA codec in Java
        - IC_CODEC => Imperial College DNA codec
        - TEST_CODEC => Codec for testing
        """)
public enum DNACodecType {

  //@formatter:off
  NO_CODEC("no-dna-codec"),
  UNIGE_PYTHON_CODEC("unige-python-dna-codec"),
  UNIGE_CODEC("unige-dna-codec"),
  IC_CODEC("ic-dna-codec"),
  TEST_CODEC("test-codec");
  //@formatter:on

  @Schema(description = "The name of the DNA codec.")
  private final String name;

  DNACodecType(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public static DNACodecType fromName(String codecNameType) {
    return switch (codecNameType) {
      case "no-dna-codec" -> NO_CODEC;
      case "unige-python-dna-codec" -> UNIGE_PYTHON_CODEC;
      case "unige-dna-codec" -> UNIGE_CODEC;
      case "ic-dna-codec" -> IC_CODEC;
      case "test-codec" -> TEST_CODEC;
      default -> throw new SolidifyValidationException(new ValidationError("Unknown DNA codec"));
    };
  }

}

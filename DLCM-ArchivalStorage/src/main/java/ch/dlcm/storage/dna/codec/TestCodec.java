/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - TestCodec.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.dna.codec;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.config.DLCMProperties.Storage.DNAProperties;
import ch.dlcm.storage.dna.DNACodec;
import ch.dlcm.storage.dna.DNACodecType;

public class TestCodec extends DNACodec {
  private final Path dnaArchiveForTesting = Paths.get("src", "test", "resources", "dna", "sample6.dnamic.dna.fasta").toAbsolutePath();
  private final Map<String, URI> aipList = new HashMap<>();

  public TestCodec(DNAProperties dnaProperties) {
    super(dnaProperties);
    if (this.getDnaCodecType() != DNACodecType.TEST_CODEC) {
      throw new SolidifyCheckingException(
              "Wrong DNA configuration: " + this.getClass().getSimpleName() + " <> " + this.getDnaCodecType().getName());
    }
  }

  @Override
  public URI encode(String id, String name, URI aipUri) {
    final Path aipPath = Paths.get(aipUri);
    final Path dnaPath = aipPath.getParent().resolve(aipPath.getFileName().toString() + ".fasta");
    try {
      if (FileTool.copyFile(this.dnaArchiveForTesting, dnaPath)) {
        this.aipList.put(id, aipUri);
        return dnaPath.toUri();
      }
      throw new SolidifyProcessingException("Cannot encode AIP (" + aipUri + ") with " + this.getDnaCodecType().getName() + " DNA codec");
    } catch (IOException e) {
      throw new SolidifyProcessingException("Cannot encode AIP (" + aipUri + ") with " + this.getDnaCodecType().getName() + " DNA codec", e);
    }
  }

  @Override
  public URI decode(URI dnaFastaFile) {
    final String aipName = Paths.get(dnaFastaFile).getFileName().toString();
    return this.aipList.get(aipName);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - DnamicFastaHeaderParser.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.dna.fasta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.AccessionID;
import org.biojava.nbio.core.sequence.io.template.SequenceHeaderParserInterface;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.biojava.nbio.core.sequence.template.Compound;

import ch.unige.solidify.exception.SolidifyCheckingException;

public class DnamicFastaHeaderParser<S extends AbstractSequence<C>, C extends Compound> implements SequenceHeaderParserInterface<S, C> {

  private static final String DNAMIC = "dnamic";
  private static final String ARCHIVE_ID = "archiveId";
  private static final String ARCHIVE_NAME = "archiveName";
  private static final String SEQUENCE_ID = "sequenceId";

  @Override
  public void parseHeader(String header, S sequence) {
    // DNAMIC Fasta header description
    // dnamic|<archive id>|<sequence id>|parameter1=<value1>|...|parameterN=<valueN>|<archive name>
    sequence.setOriginalHeader(header);
    Map<String, String> headerValues = this.parseHeaderValues(header);
    if (headerValues.isEmpty()) {
      throw new SolidifyCheckingException("Empty DNAMIC header");
    }
    sequence.setAccession(new AccessionID(headerValues.get(SEQUENCE_ID)));
    if (headerValues.size() > 1) {
      sequence.setDescription("[" + headerValues.get(ARCHIVE_ID) + "] " + headerValues.get(ARCHIVE_NAME));
      final List<String> comments = new ArrayList<>();
      comments.add(DNAMIC);
      headerValues.entrySet().stream()
              .filter(e -> !e.getKey().equals(SEQUENCE_ID)
                      && !e.getKey().equals(ARCHIVE_ID)
                      && !e.getKey().equals(ARCHIVE_NAME))
              .forEach(e -> comments.add(e.getKey() + "=" + e.getValue()));
      sequence.setComments(comments);
    }
  }

  private Map<String, String> parseHeaderValues(String header) {
    Map<String, String> headerValues = new HashMap<>();
    if (header.startsWith(DNAMIC)) {
      String[] values = header.split("\\|");
      if (values.length < 4) {
        throw new SolidifyCheckingException("Wrong DNAMIC header format: " + header);
      }
      headerValues.put(ARCHIVE_ID, values[1]);
      headerValues.put(SEQUENCE_ID, values[2]);
      headerValues.put(ARCHIVE_NAME, values[values.length - 1]);
      for (int i = 3; i < values.length - 1; i++) {
        String[] param = values[i].split("=");
        if (param.length < 2) {
          throw new SolidifyCheckingException("Cannot parse parameter: " + values[i]);
        }
        headerValues.put(param[0], param[1]);
      }
    } else {
      headerValues.put(SEQUENCE_ID, header);
    }
    return headerValues;
  }

}

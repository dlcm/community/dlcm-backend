/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - UNIGEPythonCodec.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.storage.dna.codec;

import java.io.IOException;
import java.net.URI;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.util.CommandTool;

import ch.dlcm.config.DLCMProperties.Storage.DNAProperties;
import ch.dlcm.storage.dna.DNACodec;

public class UNIGEPythonCodec extends DNACodec {

  private final String installFolder;
  private final String pythonCommand;

  public UNIGEPythonCodec(DNAProperties dnaProperties) {
    super(dnaProperties);
    this.installFolder = dnaProperties.getWorkingFolder();
    this.pythonCommand = dnaProperties.getCommand();
    throw new SolidifyCheckingException(
            "Wrong DNA configuration: " + this.getClass().getSimpleName() + " <> " + this.getDnaCodecType().getName());
  }

  @Override
  public URI encode(String id, String name, URI aipUri) {
    try {
      // To encode a binary file
      final URI dnaUri = this.generateDnaArchiveFileName(aipUri);
      CommandTool.run(this.installFolder, this.pythonCommand, "cli.py", "encode", aipUri.toString(), dnaUri.toString(), id, name);
      return dnaUri;
    } catch (IOException e) {
      throw new SolidifyProcessingException("Cannot encode in DNA", e);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      throw new SolidifyProcessingException("Execution interrupted in DNA encoding", e);
    }
  }

  @Override
  public URI decode(URI dnaFastaFile) {
    try {
      // To decode a DNAfile
      final URI aipUri = this.generateArchiveFileName(dnaFastaFile);
      CommandTool.run(this.installFolder, this.pythonCommand, "cli.py", "encode", dnaFastaFile.toString(), aipUri.toString());
      return aipUri;
    } catch (IOException e) {
      throw new SolidifyProcessingException("Cannot encode in DNA", e);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      throw new SolidifyProcessingException("Execution interrupted in DNA conversion", e);
    }
  }

}

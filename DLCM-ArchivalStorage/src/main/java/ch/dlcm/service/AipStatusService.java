/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - AipStatusService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import jakarta.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.TrustedRestClientService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.message.cache.ArchiveReindexedMessage;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.Access;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.DataFileChecksum.ChecksumOrigin;
import ch.dlcm.model.DataFileChecksum.ChecksumType;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.settings.License;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.rest.trusted.TrustedLicenseRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;
import ch.dlcm.storage.StorageService;
import ch.dlcm.viruscheck.VirusCheckService;

@Service
@ConditionalOnBean(ArchivalStorageController.class)
public class AipStatusService extends AbstractAipService {
  private static final Logger log = LoggerFactory.getLogger(AipStatusService.class);

  private static final String AIP_ERROR_CHECK = "archival.aip.error.check";
  private static final String AIP_ERROR_PROCESSING = "archival.aip.error.process";
  private static final String DISPOSAL_ERROR_DATAFILE = "archival.disposal.error.datafile";
  private static final String AIP_MISSING_INFO = "archival.reload.error.missing-info";

  private final int waitMilliseconds;
  private final int waitMaxTries;
  private final String[] checksumAlgoList;
  private final long fileSizeLimit;

  private final ArchivalInfoPackageService aipService;
  private final FileFormatService fileFormatService;
  private final StorageService storageService;
  private final VirusCheckService virusCheckService;

  private final TrustedLicenseRemoteResourceService licenseRemoteResourceService;
  private final TrustedRestClientService trustedDLCMRestClientService;
  private final String accessRefreshUrl;
  private final String orcidSynchroUrl;

  public AipStatusService(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          ArchivalInfoPackageService aipService,
          StorageService storageService,
          FileFormatService fileFormatService,
          VirusCheckService virusCheckService,
          HistoryService historyService,
          TrustedNotificationRemoteResourceService notificationRemoteService,
          TrustedUserRemoteResourceService userRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService,
          TrustedLicenseRemoteResourceService licenseRemoteResourceService,
          TrustedRestClientService trustedDLCMRestClientService) {
    super(messageService, historyService, userRemoteService, organizationalUnitRemoteService, notificationRemoteService, dlcmProperties);
    this.waitMilliseconds = dlcmProperties.getWait().getMilliseconds();
    this.waitMaxTries = dlcmProperties.getWait().getMaxTries();
    this.checksumAlgoList = dlcmProperties.getParameters().getChecksumList();
    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit().toBytes();
    this.aipService = aipService;
    this.storageService = storageService;
    this.fileFormatService = fileFormatService;
    this.virusCheckService = virusCheckService;
    this.licenseRemoteResourceService = licenseRemoteResourceService;
    this.trustedDLCMRestClientService = trustedDLCMRestClientService;
    this.accessRefreshUrl = dlcmProperties.getModule().getAccess().getUrl() + "/" + DLCMActionName.REFRESH;
    this.orcidSynchroUrl = dlcmProperties.getModule().getAdmin().getUrl() + "/" + ResourceName.ORCID_SYNCHRONIZATION + "/" +
            DLCMActionName.SYNCHRONIZE_ALL_EXISTING;
  }

  @Transactional
  public ArchivalInfoPackage processAipStatus(String aipId) {
    // Load AIP
    final ArchivalInfoPackage aip = this.aipService.findOne(aipId);
    try {

      this.logWillBeProcessedPackage(aip);

      switch (aip.getInfo().getStatus()) {
        case IN_PREPARATION -> this.processInPreparationAip(aip);
        case READY -> this.processReadyAip(aip);
        case CHECKED -> this.processCheckedAip(aip);
        case DISPOSAL_APPROVED_BY_ORGUNIT, DISPOSAL_APPROVED -> this.processDisposableAip(aip);
        case IN_PROGRESS -> this.processInProgressAip(aip);
        case RELOADED -> this.processReloadedAip(aip);
        case REINDEXING -> this.processReindexingAip(aip);
        case STORED -> this.processStoredAip(aip);
        case INDEXING -> this.processIndexingAip(aip);
        case PACKAGE_REPLICATION_PENDING -> this.startReplicationPackageAip(aip);
        case REPLICATING_PACKAGE -> this.processReplicatingPackageAip(aip);
        case TOMBSTONE_REPLICATION_PENDING -> this.startReplicationTombstoneAip(aip);
        case REPLICATING_TOMBSTONE -> this.processReplicatingTombstoneAip(aip);
        case UPDATING_RETENTION -> this.processUpdatingRetentionAip(aip);
        case METADATA_EDITION_PENDING -> this.startMetadataUpdateAip(aip);
        case EDITING_METADATA -> this.updateMetadata(aip);
        case METADATA_UPGRADE_PENDING -> this.startMetadataUpgradeAip(aip);
        case UPGRADING_METADATA -> this.upgradeMetadata(aip);
        case COMPLIANCE_LEVEL_UPDATE_PENDING -> this.startComplianceLevelUpdateAip(aip);
        case UPDATING_COMPLIANCE_LEVEL -> this.updateComplianceLevel(aip);

        default -> throw new SolidifyRuntimeException(
                this.messageService.get("archival.aip.error.status", new Object[] { aip.getInfo().getStatus() }));
      }
    } catch (final SolidifyCheckingException e) {
      this.logPackageMessage(LogLevel.ERROR, aip, AIP_ERROR_CHECK, e);
      this.inError(aip, this.messageService.get(AIP_ERROR_CHECK), e.getMessage());
      this.sendErrorNotification(aip);
    } catch (final SolidifyProcessingException e) {
      this.logPackageMessage(LogLevel.ERROR, aip, AIP_ERROR_PROCESSING, e);
      this.inError(aip, this.messageService.get(AIP_ERROR_PROCESSING), e.getMessage());
      this.sendErrorNotification(aip);
    } catch (final Exception e) {
      this.logPackageMessage(LogLevel.ERROR, aip, "processing error", e);
      this.inError(aip, e.getMessage());
      this.sendErrorNotification(aip);
    }

    this.logProcessedPackage(aip);

    return this.aipService.save(aip);
  }

  @Transactional
  public void purgeAipDataFile(String aipId) {
    // Load AIP
    final ArchivalInfoPackage aip = this.aipService.findOne(aipId);
    this.logPackageMessage(LogLevel.INFO, aip, "purge package");
    for (int i = (aip.getDataFiles().size() - 1); i >= 0; i--) {
      aip.removeItem(aip.getDataFiles().get(i));
    }
  }

  // AIP with IN_PREPARATION status
  private void processInPreparationAip(ArchivalInfoPackage aip) {
    if (aip.isReloaded()) {
      aip.setPackageStatus(PackageStatus.RELOADED);
    } else if (aip.isAlreadyStored() || aip.isAlreadyDisposed()) {
      aip.setPackageStatus(PackageStatus.STORED);
    } else if (!aip.isValid()) {
      this.inError(aip, this.messageService.get("archival.aip.error.invalid"));
    } else if (aip.isReady()) {
      aip.setPackageStatus(PackageStatus.READY);
    } else if (aip.isCollection() && !aip.isReady()) {
      if (aip.getCollection().isEmpty()) {
        this.inError(aip, this.messageService.get("archival.aip.error.collection-empty"));
      } else {
        this.inError(aip, this.messageService.get("archival.aip.error.collection-not-ready"));
      }
    }
  }

  // AIP with READY status
  private void processReadyAip(ArchivalInfoPackage aip) throws IOException, URISyntaxException {
    // Check AIP before stored (i.e. working AIP)
    this.logPackageMessage(LogLevel.INFO, aip, "will be checked before being stored");
    this.storageService.checkAIP(aip);
    aip.setPackageStatus(PackageStatus.CHECKED);
    this.logPackageMessage(LogLevel.INFO, aip, "successfully checked");
  }

  // AIP with CHEKCKED status
  private void processCheckedAip(ArchivalInfoPackage aip) throws IOException {
    final PackageStatus sts = this.storageService.getStatus(aip);
    if (sts == PackageStatus.READY) {
      this.logPackageMessage(LogLevel.INFO, aip, "will be stored");
      this.storageService.storeAIP(aip);
      this.logPackageMessage(LogLevel.INFO, aip, "successfully stored");
      aip.setPackageStatus(PackageStatus.IN_PROGRESS);
    } else if (sts == PackageStatus.STORED) {
      this.logPackageMessage(LogLevel.DEBUG, aip, "is already stored");
      aip.setPackageStatus(PackageStatus.STORED);
    } else {
      this.inError(aip, this.messageService.get(AIP_ERROR_CHECK));
    }
  }

  // AIP with IN_PROGRESS status
  private void processInProgressAip(ArchivalInfoPackage aip) throws IOException, URISyntaxException {
    if (this.storageService.getStatus(aip) == PackageStatus.STORED) {
      String archiveId = this.storageService.submitAIP(aip);
      if (archiveId != null) {
        aip.setArchiveId(archiveId);
        this.setArchiveSizeAndFileNumber(aip);
        this.copyFileInfo(aip);
        aip.setPackageStatus(PackageStatus.STORED);
      } else {
        this.inError(aip, this.messageService.get("archival.aip.error.storage"));
      }
    }
  }

  // AIP with REINDEXING status
  private void processReindexingAip(ArchivalInfoPackage aip) {
    this.logPackageMessage(LogLevel.INFO, aip, "will be deleted from index");
    if (this.storageService.deleteIndex(aip.getResId())) {
      aip.setPackageStatus(PackageStatus.STORED);
    } else {
      this.inError(aip, this.messageService.get("archival.aip.error.reindexing"));
    }
  }

  // AIP with STORED status
  private void processStoredAip(ArchivalInfoPackage aip) {
    this.logPackageMessage(LogLevel.INFO, aip, "check if metadata exist in index");
    if (this.storageService.checkIndexing(aip.getResId())) {
      this.completeAip(aip);
    } else {
      this.logPackageMessage(LogLevel.INFO, aip, "metadata will be indexed");
      if (this.storageService.indexAIP(aip)) {
        aip.setPackageStatus(PackageStatus.INDEXING);
      } else {
        this.inError(aip, this.messageService.get("archival.aip.error.indexing"));
      }
    }
  }

  // AIP with INDEXING status
  private void processIndexingAip(ArchivalInfoPackage aip) {
    if (this.checkIndexing(aip.getResId())) {
      this.completeAip(aip);
      SolidifyEventPublisher.getPublisher().publishEvent(new ArchiveReindexedMessage(aip.getResId()));
    } else {
      this.inError(aip, this.messageService.get("archival.aip.error.noindex"));
    }
  }

  // AIP with DISPOSABLE status
  private void processDisposableAip(ArchivalInfoPackage aip) throws IOException, NoSuchAlgorithmException, JAXBException {
    this.logPackageMessage(LogLevel.INFO, aip, "will be start the disposal process");

    if (!aip.isAlreadyDisposed()) {
      if (Boolean.TRUE.equals(aip.getDispositionApproval())) {
        // Check history if there is the approval step has been done
        this.historyService.checkLastEvents(aip.getResId(), this.messageService.get("archival.disposal.error.no-approval-by-orgunit"),
                PackageStatus.DISPOSAL_APPROVED_BY_ORGUNIT.toString(),
                PackageStatus.DISPOSABLE.toString());
      } else {
        // Check history if there is the confirmation step has been done
        this.historyService.checkLastEvents(aip.getResId(), this.messageService.get("archival.disposal.error.no-approval"),
                PackageStatus.DISPOSAL_APPROVED.toString(),
                PackageStatus.DISPOSABLE.toString());
      }
      // Dispose an archive
      try {
        this.storageService.disposeArchive(aip);
      } catch (URISyntaxException e) {
        log.error("Error when processing AIP '{}' for disposal", aip.getArchiveId());
        this.logPackageMessage(LogLevel.ERROR, aip, "Disposal processing error", e);
      }
      // Update Aip
      this.updateAipWithTombstone(aip);
    }
    // Reindex metadata
    aip.setPackageStatus(PackageStatus.REINDEXING);
    aip.setLastArchiving(OffsetDateTime.now());
  }

  // AIP with RELOADED status
  private void processReloadedAip(ArchivalInfoPackage aip) throws NoSuchAlgorithmException, IOException, JAXBException {
    this.logPackageMessage(LogLevel.INFO, aip, "will be reloaded");
    this.updateAipFromArchive(aip);
    aip.setPackageStatus(PackageStatus.STORED);
  }

  // AIP with METADATA_EDITION_PENDING status
  private void startMetadataUpdateAip(ArchivalInfoPackage aip) {
    // Start AIP metadata edition
    aip.setPackageStatus(PackageStatus.EDITING_METADATA);
    this.logPackageMessage(LogLevel.INFO, aip, "metadata edition is starting");
  }

  // AIP with EDITING_METADATA status
  private void updateMetadata(ArchivalInfoPackage aip) {
    final List<AipDataFile> aipDataFileList = aip.getDataFiles();
    if (aipDataFileList.isEmpty()) {
      throw new SolidifyRuntimeException("No updated metadata AIP data file present");
    } else if (aipDataFileList.size() > 1) {
      throw new SolidifyRuntimeException("There should be only one updated metadata AIP data file present");
    } else {
      try {
        this.storageService.updateArchiveMetadata(aip);
        this.updateAipWithNewPackage(aip);
        aip.setPackageStatus(PackageStatus.REINDEXING);
        aip.setLastArchiving(OffsetDateTime.now());
      } catch (IOException e) {
        throw new SolidifyRuntimeException("IOException during AIP metadata update", e);
      } catch (JAXBException e) {
        throw new SolidifyRuntimeException("JAXBException during AIP metadata update", e);
      } catch (NoSuchAlgorithmException | URISyntaxException e) {
        throw new SolidifyRuntimeException("NoSuchAlgorithmException | URISyntaxException during AIP metadata update", e);
      }
    }
  }

  // AIP with METADATA_UPGRADE_PENDING status
  private void startMetadataUpgradeAip(ArchivalInfoPackage aip) {
    // Start AIP metadata edition
    aip.setPackageStatus(PackageStatus.UPGRADING_METADATA);
    this.logPackageMessage(LogLevel.INFO, aip, "metadata upgrading is starting");
  }

  // AIP with UPGRADING_METADATA status
  private void upgradeMetadata(ArchivalInfoPackage aip) {
    try {
      this.storageService.upgradeArchiveMetadata(aip);
      this.updateAipWithNewPackage(aip);
      aip.setPackageStatus(PackageStatus.REINDEXING);
      aip.setLastArchiving(OffsetDateTime.now());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("IOException during AIP metadata update", e);
    } catch (JAXBException e) {
      throw new SolidifyRuntimeException("JAXBException during AIP metadata update", e);
    } catch (NoSuchAlgorithmException | URISyntaxException e) {
      throw new SolidifyRuntimeException("NoSuchAlgorithmException | URISyntaxException during AIP metadata update", e);
    }
  }

  // AIP with COMPLIANCE_LEVEL_UPDATE_PENDING status
  private void startComplianceLevelUpdateAip(ArchivalInfoPackage aip) {
    // Start AIP metadata edition
    aip.setPackageStatus(PackageStatus.UPDATING_COMPLIANCE_LEVEL);
    this.logPackageMessage(LogLevel.INFO, aip, "compliance level updating is starting");
  }

  // AIP with UPDATING_COMPLIANCE_LEVEL status
  private void updateComplianceLevel(ArchivalInfoPackage aip) {
    try {
      if (this.storageService.updateComplianceLevel(aip) > 0) {
        this.updateAipWithNewPackage(aip);
        aip.setPackageStatus(PackageStatus.REINDEXING);
        aip.setLastArchiving(OffsetDateTime.now());
      } else {
        this.completeAip(aip);
      }
    } catch (IOException e) {
      throw new SolidifyRuntimeException("IOException during AIP compliance level update", e);
    } catch (JAXBException e) {
      throw new SolidifyRuntimeException("JAXBException during AIP compliance level update", e);
    } catch (NoSuchAlgorithmException | URISyntaxException e) {
      throw new SolidifyRuntimeException("NoSuchAlgorithmException | URISyntaxException during AIP metadata update", e);
    }
  }

  // AIP with PACKAGE_REPLICATION_PENDING status
  private void startReplicationPackageAip(ArchivalInfoPackage aip) {
    // Start AIP package replication
    aip.setPackageStatus(PackageStatus.REPLICATING_PACKAGE);
    this.logPackageMessage(LogLevel.INFO, aip, "package replication is starting");
  }

  // AIP with REPLICATING_PACKAGE status
  private void processReplicatingPackageAip(ArchivalInfoPackage aip) throws NoSuchAlgorithmException, IOException {
    this.checkDataFile(aip);
    this.storageService.storeAIP(aip);
    this.updateAipWithNewPackage(aip);
    this.completeAip(aip);
  }

  // AIP with TOMBSTONE_REPLICATION_PENDING status
  private void startReplicationTombstoneAip(ArchivalInfoPackage aip) {
    // Start AIP tombstone replication
    aip.setPackageStatus(PackageStatus.REPLICATING_TOMBSTONE);
    this.logPackageMessage(LogLevel.INFO, aip, "tombstone replication is starting");
  }

  // AIP with REPLICATING_TOMBSTONE status
  private void processReplicatingTombstoneAip(ArchivalInfoPackage aip) throws NoSuchAlgorithmException, IOException {
    this.checkDataFile(aip);
    this.storageService.storeAIP(aip);
    this.updateAipWithTombstone(aip);
    this.completeAip(aip);
  }

  // AIP with UPDATING_RETENTION status
  private void processUpdatingRetentionAip(ArchivalInfoPackage aip)
          throws IOException, NoSuchAlgorithmException, JAXBException, URISyntaxException {
    // Update archive
    this.storageService.updateArchiveMetadata(aip);
    // Update AIP
    this.updateAipWithNewPackage(aip);
    // Reindex metadata
    aip.setPackageStatus(PackageStatus.REINDEXING);
    aip.setLastArchiving(OffsetDateTime.now());
  }

  private void checkDataFile(ArchivalInfoPackage aip) {
    // Check if there is exactly one data file
    if (aip.getDataFiles().size() != 1) {
      throw new SolidifyCheckingException(this.messageService.get(DISPOSAL_ERROR_DATAFILE));
    }
    AipDataFile aipDataFile = aip.getDataFiles().get(0);
    // Check if read
    if (!aipDataFile.getStatus().equals(DataFileStatus.READY)) {
      throw new SolidifyCheckingException(this.messageService.get(DISPOSAL_ERROR_DATAFILE));
    }
    if (!DataCategory.Package.equals(aipDataFile.getDataCategory()) || !DataCategory.InformationPackage.equals(aipDataFile.getDataType())) {
      throw new SolidifyCheckingException(this.messageService.get(DISPOSAL_ERROR_DATAFILE));
    }
  }

  private void updateAipWithTombstone(ArchivalInfoPackage aip) throws IOException, NoSuchAlgorithmException {
    // Update AIP tombstone checksums
    this.updateChecksums(aip, ChecksumOrigin.DLCM_TOMBSTONE);
    // Update AIP tombstone size
    aip.setTombstoneSize(this.storageService.getAIPSize(aip));
  }

  private void updateAipWithNewPackage(ArchivalInfoPackage aip) throws IOException, NoSuchAlgorithmException {
    // Update AIP checksums
    this.updateChecksums(aip, ChecksumOrigin.DLCM);
    // Update AIP size & file number
    this.setArchiveSizeAndFileNumber(aip);
  }

  private boolean checkIndexing(String aipId) {
    int tries = 0;
    while (tries < this.waitMaxTries) {
      SolidifyTime.waitInMilliSeconds(this.waitMilliseconds);
      if (this.storageService.checkIndexing(aipId)) {
        log.info("AIP '{}' metadata exists in index", aipId);
        return true;
      }
      tries++;
    }
    log.info("AIP '{}' metadata does not exist in index", aipId);
    return false;
  }

  private void updateChecksums(ArchivalInfoPackage aip, ChecksumOrigin checksumOrigin) throws IOException, NoSuchAlgorithmException {
    // Remove checksum for new package
    if (checksumOrigin.equals(ChecksumOrigin.DLCM)) {
      aip.getChecksums().removeIf(c -> c.getChecksumOrigin().equals(ChecksumOrigin.DLCM));
    }
    // Compute checksum
    try (InputStream inputStream = this.storageService.getAIPStream(aip)) {
      this.computeChecksums(aip, checksumOrigin, inputStream);
    }
  }

  private void computeChecksums(ArchivalInfoPackage aip, ChecksumOrigin checksumOrigin, InputStream aipStream)
          throws NoSuchAlgorithmException, IOException {
    final String[] algoList = this.checksumAlgoList;
    this.logPackageMessage(LogLevel.INFO, aip, "checksums " + Arrays.toString(algoList) + " will be calculated");
    final String[] checksumList = ChecksumTool.computeChecksum(aipStream, algoList);
    for (int i = 0; i < algoList.length; i++) {
      aip.getChecksums()
              .add(new DataFileChecksum(ChecksumAlgorithm.valueOf(algoList[i]), ChecksumType.COMPLETE, checksumOrigin, checksumList[i]));
    }
  }

  private void copyFileInfo(ArchivalInfoPackage aip) {
    final List<AipDataFile> aipFiles = aip.getDataFiles();
    final AipDataFile aipFile = aipFiles.get(0);
    aip.setChecksums(aipFile.getChecksums());
    if (aipFile.getFileFormat() != null) {
      aip.setFileFormat(aipFile.getFileFormat());
    }
    if (aipFile.getVirusCheck() != null) {
      aip.setVirusCheck(aipFile.getVirusCheck());
    }
  }

  private Path downloadAIP(ArchivalInfoPackage aip, InputStream aipStream) throws IOException {
    final Path aipFile = this.storageService.getAIPFolder(aip.getResId()).resolve(ResourceName.AIP_FILE);
    // check if folder exists
    if (!FileTool.ensureFolderExists(aipFile.getParent())) {
      throw new IOException("Cannot create " + aipFile.getParent());
    }

    try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(aipFile.toFile()), SolidifyConstants.BUFFER_SIZE)) {

      int byteRead;
      final byte[] data = new byte[SolidifyConstants.BUFFER_SIZE];

      while ((byteRead = aipStream.read(data, 0, data.length)) != -1) {
        outputStream.write(data, 0, byteRead);
      }
      outputStream.flush();

    } catch (final IOException e) {
      throw new SolidifyProcessingException("Error during AIP downloading", e);
    }
    return aipFile;
  }

  private void setArchiveSizeAndFileNumber(ArchivalInfoPackage aip) throws IOException {
    long size = this.storageService.getAIPSize(aip);
    long fileNumber = this.storageService.getAIPFileNumber(aip);
    // Check if tombstone
    if (aip.getTombstoneSize() > 0) {
      aip.setTombstoneSize(size);
    } else {
      aip.setArchiveSize(size);
      aip.setArchiveFileNumber(fileNumber);
    }
    // For collection, add others AIPs
    if (aip.isCollection()) {
      // Remove update files & dlcm.xml
      fileNumber -= aip.getUpdateNumber() + 1;
      // Add info of each archives of collection
      for (final ArchivalInfoPackage aiu : aip.getCollection()) {
        if (aiu.isTombstone()) {
          size += aiu.getTombstoneSize();
        } else {
          size += aiu.getArchiveSize();
        }
        fileNumber += aiu.getArchiveFileNumber() - aiu.getUpdateNumber() - 1;
      }
      aip.setCollectionArchiveSize(size);
      aip.setCollectionFileNumber(fileNumber);
    }
  }

  private void updateAipFromArchive(ArchivalInfoPackage aip) throws IOException, NoSuchAlgorithmException, JAXBException {
    ChecksumOrigin checksumOrigin = null;
    // Get metadata from archive
    final Map<String, String> aipMetadata = this.storageService.getAIPInfo(aip);

    // AIP Metadata version
    if (!aipMetadata.containsKey(DLCMConstants.METADATA_VERSION_FIELD)) {
      throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.METADATA_VERSION_FIELD }));
    }
    aip.getInfo().setMetadataVersion(DLCMMetadataVersion.fromVersion(aipMetadata.get(DLCMConstants.METADATA_VERSION_FIELD)));

    // AIP unit
    if (!aipMetadata.containsKey(DLCMConstants.AIP_UNIT)) {
      throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.AIP_UNIT }));
    }
    aip.setArchivalUnit(Boolean.parseBoolean(aipMetadata.get(DLCMConstants.AIP_UNIT)));
    // AIP collection content
    if (aip.isCollection() && aip.getCollection().isEmpty()) {
      if (!aipMetadata.containsKey(DLCMConstants.AIP_COLLECTION)) {
        throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.AIP_COLLECTION }));
      }
      for (String archiveId : aipMetadata.get(DLCMConstants.AIP_COLLECTION).split(SolidifyConstants.FIELD_SEP)) {
        ArchivalInfoPackage archive = this.aipService.findOne(archiveId);
        aip.getCollection().add(archive);
      }
      if (aip.getCollection().stream().anyMatch(a -> !a.isCompleted())) {
        throw new SolidifyProcessingException(this.messageService.get("archival.reload.error.collection-in-progress"));
      }
    }

    // AIP access level
    if (!aipMetadata.containsKey(DLCMConstants.AIP_ACCESS_LEVEL)) {
      throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.AIP_ACCESS_LEVEL }));
    }
    aip.getInfo().setAccess(Access.valueOf(aipMetadata.get(DLCMConstants.AIP_ACCESS_LEVEL)));

    // AIP creation
    if (!aipMetadata.containsKey(DLCMConstants.CREATION_FIELD)) {
      throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.CREATION_FIELD }));
    }
    aip.getCreation().setWhen(OffsetDateTime.parse(aipMetadata.get(DLCMConstants.CREATION_FIELD)));

    // AIP retention
    if (!aipMetadata.containsKey(DLCMConstants.AIP_RETENTION)) {
      throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.AIP_RETENTION }));
    }
    aip.setRetention(Integer.parseInt(aipMetadata.get(DLCMConstants.AIP_RETENTION)));
    // AIP retention approval
    if (aipMetadata.containsKey(DLCMConstants.AIP_DISPOSITION_APPROVAL)) {
      aip.setDispositionApproval(Boolean.parseBoolean(aipMetadata.get(DLCMConstants.AIP_DISPOSITION_APPROVAL)));
    } else {
      aip.setDispositionApproval(Boolean.FALSE);
    }

    // AIP container type
    if (!aipMetadata.containsKey(DLCMConstants.AIP_CONTAINER)) {
      throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.AIP_CONTAINER }));
    }
    aip.setArchiveContainer(ArchiveContainer.valueOf(aipMetadata.get(DLCMConstants.AIP_CONTAINER)));

    // AIP Tombstone
    if (aipMetadata.containsKey(DLCMConstants.AIP_TOMBSTONE_SIZE)) {
      checksumOrigin = ChecksumOrigin.DLCM_TOMBSTONE;
      aip.setTombstoneSize(Long.parseLong(aipMetadata.get(DLCMConstants.AIP_TOMBSTONE_SIZE)));
      if (!aipMetadata.containsKey(DLCMConstants.AIP_FILE_NUMBER)) {
        throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.AIP_FILE_NUMBER }));
      }
      aip.setArchiveSize(Long.parseLong(aipMetadata.get(DLCMConstants.AIP_SIZE)));
      aip.setArchiveFileNumber(Long.parseLong(aipMetadata.get(DLCMConstants.AIP_FILE_NUMBER)));
    } else {
      checksumOrigin = ChecksumOrigin.DLCM;
    }
    // AIP Size & File Number
    this.setArchiveSizeAndFileNumber(aip);

    // AIP Title
    if (aipMetadata.containsKey(DLCMConstants.AIP_TITLE)) {
      aip.getInfo().setName(aipMetadata.get(DLCMConstants.AIP_TITLE));
    }

    if (!aipMetadata.containsKey(DLCMConstants.AIP_DATA_TAG)) {
      throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.AIP_DATA_TAG }));
    }
    aip.getInfo().setDataSensitivity(DataTag.valueOf(aipMetadata.get(DLCMConstants.AIP_DATA_TAG)));

    // DUAType
    if (aip.getInfo().getMetadataVersion().getVersionNumber() >= DLCMMetadataVersion.V3_1.getVersionNumber()) {
      if (!aipMetadata.containsKey(DLCMConstants.AIP_DATA_USE_POLICY)) {
        throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.AIP_DATA_USE_POLICY }));
      }
      aip.getInfo().setDataUsePolicy(DataUsePolicy.valueOf(aipMetadata.get(DLCMConstants.AIP_DATA_USE_POLICY)));
    } else {
      aip.getInfo().setDataUsePolicy(DataUsePolicy.NONE);
    }

    if (!aipMetadata.containsKey(DLCMConstants.AIP_COMPLIANCE_LEVEL)) {
      throw new SolidifyProcessingException(this.messageService.get(AIP_MISSING_INFO, new Object[] { DLCMConstants.AIP_COMPLIANCE_LEVEL }));
    }
    aip.getInfo().setComplianceLevel(ComplianceLevel.fromValue(Integer.parseInt(aipMetadata.get(DLCMConstants.AIP_COMPLIANCE_LEVEL))));

    if (aipMetadata.containsKey(DLCMConstants.AIP_LICENSE)) {
      License license = null;
      try {
        // Search by SPDX ID
        license = this.licenseRemoteResourceService.getByOpenLicenseId(aipMetadata.get(DLCMConstants.AIP_LICENSE));
      } catch (Exception e) {
        // Search by Title
        List<License> licenseList = this.licenseRemoteResourceService.getLicensesByTitle(aipMetadata.get(DLCMConstants.AIP_LICENSE));
        if (!licenseList.isEmpty()) {
          license = licenseList.get(0);
        }
      }
      // License found
      if (license != null) {
        aip.getInfo().setLicenseId(license.getResId());
      }
    }

    // TODO AIP SIP, publication date, embargo

    // Update checksums+FileFormat+VirusCheck
    try (InputStream aipStream = this.storageService.getAIPStream(aip)) {

      // Download once AIP
      this.logPackageMessage(LogLevel.INFO, aip, "archive will be downloaded from storage");
      aip.setPath(this.downloadAIP(aip, aipStream));

      // Checksum
      if (aip.getChecksums().isEmpty()) {
        try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(aip.getPath().toFile()),
                SolidifyConstants.BUFFER_SIZE)) {
          this.computeChecksums(aip, checksumOrigin, inputStream);
        }
      }

      // Check if there is a file
      if (aip.getDataFileNumber() > 0 && aip.getArchiveSize() < this.fileSizeLimit) {
        // File Format
        if (aip.getFileFormat() == null) {
          this.logPackageMessage(LogLevel.INFO, aip, "format will be checked (size: " + StringTool.formatSmartSize(aip.getFileSize()) + ")");
          this.fileFormatService.identifyFormat(aip.getResId(), aip.getDataFiles().get(0));
        }
        // Virus Check
        if (aip.getVirusCheck() == null) {
          try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(aip.getPath().toFile()),
                  SolidifyConstants.BUFFER_SIZE)) {
            this.logPackageMessage(LogLevel.INFO, aip, "will be checked for virus");
            this.virusCheckService.checkVirus(aip.getResId(), aip.getDataFiles().get(0), inputStream);
          }
        }
      }
    }
  }

  public void refreshAipInAccess(String aipId) {
    try {
      this.trustedDLCMRestClientService.postResourceAction(this.accessRefreshUrl + "/" + aipId);
    } catch (SolidifyResourceNotFoundException e) {
      log.info("AIP '{}' not present in access, no need to refresh it", aipId);
    }
  }

  public void updateOrcidProfile(String aipId) {
    this.trustedDLCMRestClientService.postResourceAction(this.orcidSynchroUrl + "/" + aipId);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - AbstractAipService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

public abstract class AbstractAipService extends DLCMService {

  private static final String NOTIFICATION_AIP_ERROR = "archival.notification.aip.error";

  protected final HistoryService historyService;
  private final TrustedNotificationRemoteResourceService notificationRemoteService;
  private final TrustedUserRemoteResourceService userRemoteService;
  private final TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService;

  protected AbstractAipService(
          MessageService messageService,
          HistoryService historyService,
          TrustedUserRemoteResourceService userRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService,
          TrustedNotificationRemoteResourceService notificationRemoteService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.historyService = historyService;
    this.userRemoteService = userRemoteService;
    this.organizationalUnitRemoteService = organizationalUnitRemoteService;
    this.notificationRemoteService = notificationRemoteService;
  }

  protected void sendErrorNotification(ArchivalInfoPackage aip) {
    // Send a notification
    String message = this.messageService.get(NOTIFICATION_AIP_ERROR, new Object[] { aip.getResId() });
    User updaterUser = this.userRemoteService.findByExternalUid(aip.getLastUpdate().getWho());
    OrganizationalUnit organizationalUnit = this.organizationalUnitRemoteService.findOne(aip.getInfo().getOrganizationalUnitId());
    this.notificationRemoteService.createNotification(updaterUser, message, NotificationType.IN_ERROR_AIP_INFO, organizationalUnit,
            aip.getResId());
  }

  protected void completeAip(ArchivalInfoPackage aip) {
    if (aip.isAlreadyDisposed()) {
      aip.setPackageStatus(PackageStatus.DISPOSED);
    } else if (this.historyService.hasBeenDisposableAndNotApproved(aip.getResId())) {
      aip.setPackageStatus(PackageStatus.DISPOSABLE);
    } else {
      aip.setPackageStatus(PackageStatus.COMPLETED);
    }
  }
}

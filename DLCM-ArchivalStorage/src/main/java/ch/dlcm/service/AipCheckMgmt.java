/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - AipCheckMgmt.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.SolidifyTime;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.message.AipCheckMessage;
import ch.dlcm.model.oais.ArchivalInfoPackage;

@Service
@ConditionalOnBean(ArchivalStorageController.class)
public class AipCheckMgmt extends MessageProcessorBySize<AipCheckMessage> {

  private static final Logger log = LoggerFactory.getLogger(AipCheckMgmt.class);

  private final ArchivalInfoPackageService aipService;
  private final AipCheckService checkService;

  public AipCheckMgmt(DLCMProperties dlcmProperties, ArchivalInfoPackageService aipService, AipCheckService checkService) {
    super(dlcmProperties);
    this.aipService = aipService;
    this.checkService = checkService;
  }

  @JmsListener(destination = "${dlcm.queue.aip-fixity}")
  @Override
  public void receiveMessage(AipCheckMessage aipCheckMessage) {
    if (this.isBigPackageSize(this.aipService.getSize(aipCheckMessage.getResId()))) {
      aipCheckMessage.setBigArchive(true);
    }
    this.sendForParallelProcessing(aipCheckMessage);
  }

  @Override
  public void processMessage(AipCheckMessage aipCheckMessage) {
    this.setSecurityContext();

    log.info("Reading message {}", aipCheckMessage);
    try {
      ArchivalInfoPackage aip = this.aipService.findOne(aipCheckMessage.getResId());

      while (aip.isInProgress()) {
        // check AIP
        aip = this.checkService.processAipCheck(aip.getResId(), aipCheckMessage.getFixityCheckLevel(), aipCheckMessage.getCreatedBy());
        SolidifyTime.waitInMilliSeconds(200);
      }
    } catch (NoSuchElementException e) {
      log.error("Cannot find AIP ({})", aipCheckMessage.getResId());
    }
  }

}

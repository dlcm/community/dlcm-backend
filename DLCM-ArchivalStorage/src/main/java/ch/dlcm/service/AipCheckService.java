/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - AipCheckService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.exception.FixityCheckLevelNotImplementedException;
import ch.dlcm.model.AipCheckLevel;
import ch.dlcm.model.ChecksumCheck;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.DataFileChecksum.ChecksumOrigin;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;
import ch.dlcm.storage.StorageService;

@Service
@ConditionalOnBean(ArchivalStorageController.class)
public class AipCheckService extends AbstractAipService {

  private static final Logger log = LoggerFactory.getLogger(AipCheckService.class);

  protected ArchivalInfoPackageService aipService;
  protected StorageService storageService;

  public AipCheckService(
          MessageService messageService,
          HistoryService historyService,
          TrustedUserRemoteResourceService userRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService,
          TrustedNotificationRemoteResourceService notificationRemoteService,
          StorageService storageService,
          ArchivalInfoPackageService aipService,
          DLCMProperties dlcmProperties) {
    super(messageService, historyService, userRemoteService, organizationalUnitRemoteService, notificationRemoteService, dlcmProperties);
    this.storageService = storageService;
    this.aipService = aipService;
  }

  @Transactional
  public ArchivalInfoPackage processAipCheck(String aipId, AipCheckLevel aipCheckLevel, String createdBy) {
    // Load AIP
    final ArchivalInfoPackage aip = this.aipService.findOne(aipId);
    try {

      this.logWillBeProcessedPackage(aip);

      switch (aip.getInfo().getStatus()) {
        case CHECK_PENDING -> this.startAipCheck(aip, aipCheckLevel, createdBy);
        case CHECKING -> this.executeAipCheck(aip, aipCheckLevel, createdBy);
        case FIX_PENDING -> this.startAipFix(aip, aipCheckLevel, createdBy);
        case FIXING -> this.executeAipFix(aip, createdBy);
        default -> throw new SolidifyRuntimeException(
                this.messageService.get("archival.aip.error.status", new Object[] { aip.getInfo().getStatus() }));
      }
    } catch (final SolidifyCheckingException e) {
      this.logPackageMessage(LogLevel.ERROR, aip, "archival.aip.error.check", e);
      this.inError(PackageStatus.PRESERVATION_ERROR, aip, this.messageService.get("archival.aip.error.check"), e.getMessage());
      this.sendErrorNotification(aip);
    } catch (final Exception e) {
      this.logPackageMessage(LogLevel.ERROR, aip, "processing error", e);
      this.inError(PackageStatus.PRESERVATION_ERROR, aip, e.getMessage());
      this.sendErrorNotification(aip);
    }

    this.logProcessedPackage(aip);

    return this.aipService.save(aip);
  }

  private void startAipCheck(ArchivalInfoPackage aip, AipCheckLevel aipCheckLevel, String createdBy) {
    // Start checking of stored AIP (i.e. archive)
    aip.setPackageStatus(PackageStatus.CHECKING);
    this.logPackageMessage(LogLevel.INFO, aip, "stored archive starting the checking: " + aipCheckLevel.toString() + " check by " + createdBy);
  }

  private void executeAipCheck(ArchivalInfoPackage aip, AipCheckLevel aipCheckLevel, String createdBy)
          throws IOException, NoSuchAlgorithmException, URISyntaxException {
    if (AipCheckLevel.DEEP_CHECK.equals(aipCheckLevel)) {
      this.checkAip(aip);
    } else {
      this.checkFixity(aip, aipCheckLevel, createdBy);
    }
  }

  private void executeAipFix(ArchivalInfoPackage aip, String createdBy) {
    final ZipTool zip = new ZipTool(aip.getArchiveUri());
    // Check if zip is correct
    if (!zip.checkZip()) {
      throw new SolidifyCheckingException(this.messageService.get("archival.aip.error.zip.check"));
    }
    // Get Update number
    final Path aipPackage = this.storageService.getAIPFolder(aip.getResId()).resolve(DLCMConstants.PACKAGE);
    final List<String> filesToExtract = List.of(this.storageService.getUpdatedMetadataRelativePathPattern(aip.getArchiveContainer()));
    // Unzip AIP
    if (!zip.unzipFiles(aipPackage, filesToExtract, true)) {
      throw new SolidifyCheckingException(this.messageService.get("archival.aip.error.zip.unzip"));
    }
    long updateNumber = this.storageService.getPackageUpdateNumber(aip.getArchiveContainer(), aipPackage);
    // Fix archive file number
    this.storageService.checkArchiveFileNumber(
            aip,
            this.storageService.getPackageFileNumber(aip.getArchiveContainer(), aip.getArchiveUri()),
            updateNumber,
            true);
    // Fix archive size
    this.storageService.checkArchiveSize(aip, FileTool.getSize(aip.getArchiveUri()), true);

    // Remove extracted files
    if (FileTool.checkFile(aipPackage) && !FileTool.deleteFolder(aipPackage)) {
      throw new SolidifyCheckingException(this.messageService.get("archival.aip.error.purge.files"));
    }
    aip.setPackageStatus(PackageStatus.COMPLETED);
    this.addHistoryMessage(aip, PackageStatus.COMPLETED, "AIP information fixed", createdBy);
  }

  private void startAipFix(ArchivalInfoPackage aip, AipCheckLevel aipCheckLevel, String createdBy) {
    if (aipCheckLevel != AipCheckLevel.FIX_AIP_INFO) {
      throw new SolidifyCheckingException(createdBy);
    }
    aip.setPackageStatus(PackageStatus.FIXING);
    this.logPackageMessage(LogLevel.INFO, aip, "starting the archive fix");
  }

  // AIP Check ~ DEEP_CHECK
  private void checkAip(ArchivalInfoPackage aip) throws IOException, URISyntaxException {
    // Check stored AIP (i.e. archive)
    this.logPackageMessage(LogLevel.INFO, aip, "stored archive will be checked");
    this.storageService.checkArchive(aip);
    this.completeAip(aip);
    this.addHistoryMessage(aip, PackageStatus.COMPLETED, "Archive check completed", null);
    this.logPackageMessage(LogLevel.INFO, aip, "stored archive check completed");
  }

  // AIP Fixity Check ~ DEEP_FIXITY or SIMPLE_FIXITY
  private void checkFixity(ArchivalInfoPackage aip, AipCheckLevel fixityCheckLevel, String createdBy)
          throws NoSuchAlgorithmException, IOException {
    this.logPackageMessage(LogLevel.INFO, aip, "archive fixity will be checked");
    String checksumAlgos = null;
    try {
      checksumAlgos = this.checkFixityChecksum(aip, fixityCheckLevel, createdBy);
    } catch (FixityCheckLevelNotImplementedException ex) {
      this.addHistoryMessage(aip, PackageStatus.FIXITY_ERROR,
              "Unable to check checksum since the fixityCheckLevel (" + fixityCheckLevel + ") is not implemented ", createdBy);
    }
    this.completeAip(aip);
    if (StringTool.isNullOrEmpty(checksumAlgos)) {
      this.addHistoryMessage(aip, PackageStatus.COMPLETED, "Unable to verify checksums", createdBy);
    } else {
      this.addHistoryMessage(aip, PackageStatus.COMPLETED, "All checksums that could be verified (" + checksumAlgos + ") are valid", createdBy);
    }
    this.logPackageMessage(LogLevel.INFO, aip, "archive fixity check completed");

  }

  private String checkFixityChecksum(ArchivalInfoPackage aip, AipCheckLevel fixityCheckLevel, String createdBy)
          throws NoSuchAlgorithmException, IOException {

    final List<ChecksumAlgorithm> algorithmsToRecalculate = new ArrayList<>();
    final List<DataFileChecksum> existingChecksums = aip
            .getChecksums(aip.getInfo().getStatus().equals(PackageStatus.DISPOSED) ? ChecksumOrigin.DLCM_TOMBSTONE : ChecksumOrigin.DLCM);

    for (final DataFileChecksum existingChecksum : existingChecksums) {
      algorithmsToRecalculate.add(existingChecksum.getChecksumAlgo());
    }

    final List<ChecksumAlgorithm> validChecksums = new ArrayList<>();
    final List<ChecksumAlgorithm> invalidChecksums = new ArrayList<>();

    this.addHistoryMessage(aip, PackageStatus.CHECKING,
            StringUtils.collectionToDelimitedString(algorithmsToRecalculate, ", ") + " checksums are about to be reverified with level "
                    + fixityCheckLevel.name(),
            createdBy);

    final List<String> recalculatedChecksumValues = this.storageService.checkFixity(aip, algorithmsToRecalculate, fixityCheckLevel);

    for (int i = 0; i < existingChecksums.size(); i++) {

      if (!StringTool.isNullOrEmpty(recalculatedChecksumValues.get(i))) {

        if (existingChecksums.get(i).getChecksum().equals(recalculatedChecksumValues.get(i))) {

          validChecksums.add(existingChecksums.get(i).getChecksumAlgo());
          log.info("{} checksum '{}}' verified for AIP {}", existingChecksums.get(i).getChecksumAlgo(), existingChecksums.get(i).getChecksum(),
                  aip.getResId());
        } else {

          invalidChecksums.add(existingChecksums.get(i).getChecksumAlgo());
          log.warn("{} checksum '{}' verification failed for AIP {}", existingChecksums.get(i).getChecksumAlgo(),
                  existingChecksums.get(i).getChecksum(), aip.getResId());
        }

      } else {
        log.warn("StorageService could not return a hash value for {}", existingChecksums.get(i).getChecksumAlgo());
      }
    }

    if (invalidChecksums.isEmpty() && validChecksums.isEmpty()) {
      /*
       * Something wrong happened during calculation
       */
      this.updateChecksumCheckToError(aip);
      throw new SolidifyCheckingException("No checksum could be verified");
    }
    if (!invalidChecksums.isEmpty()) {
      /*
       * Some checksum(s) are not valid anymore
       */
      this.updateChecksumCheckToError(aip);
      throw new SolidifyCheckingException(
              "The following checksums verification failed: " + StringUtils.collectionToDelimitedString(invalidChecksums, ", "));
    }
    /*
     * At least one checksum is still valid (some other ones may not have been recalculated and
     * compared) and no one is wrong
     */
    aip.setChecksumCheck(new ChecksumCheck(true));
    return StringUtils.collectionToDelimitedString(validChecksums, ", ");
  }

  private void addHistoryMessage(ArchivalInfoPackage aip, PackageStatus status, String message, String createdBy) {

    final StatusHistory startCheckingHistory = new StatusHistory(aip.getClass().getSimpleName(), aip.getResId(),
            OffsetDateTime.now(ZoneOffset.UTC),
            status.toString(), message, createdBy);

    SolidifyEventPublisher.getPublisher().publishEvent(startCheckingHistory);
  }

  private void updateChecksumCheckToError(ArchivalInfoPackage aip) {
    aip.setChecksumCheck(new ChecksumCheck(false));
  }

}

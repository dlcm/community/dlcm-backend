/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - AipMgmt.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.SolidifyTime;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.message.AipMessage;
import ch.dlcm.model.oais.ArchivalInfoPackage;

@Service
@ConditionalOnBean(ArchivalStorageController.class)
public class AipMgmt extends MessageProcessorBySize<AipMessage> {
  private static final Logger log = LoggerFactory.getLogger(AipMgmt.class);

  protected final AipStatusService aipStatusService;
  protected final ArchivalInfoPackageService aipService;

  public AipMgmt(AipStatusService aipStatusService, ArchivalInfoPackageService aipService, DLCMProperties dlcmProperties) {
    super(dlcmProperties);
    this.aipStatusService = aipStatusService;
    this.aipService = aipService;
  }

  @JmsListener(destination = "${dlcm.queue.aip}")
  @Override
  public void receiveMessage(AipMessage aipMessage) {
    if (this.isBigPackageSize(this.aipService.getSize(aipMessage.getResId()))) {
      aipMessage.setBigArchive(true);
    }
    this.sendForParallelProcessing(aipMessage);
  }

  @Override
  public void processMessage(AipMessage aipMessage) {
    this.setSecurityContext();
    log.info("Reading message {}", aipMessage);
    try {
      ArchivalInfoPackage aip = this.aipService.findOne(aipMessage.getResId());
      while (aip.isInProgress()) {
        // Process AIP
        aip = this.aipStatusService.processAipStatus(aip.getResId());
        SolidifyTime.waitInMilliSeconds(200);
      }
      if (aip.isCompleted()) {
        log.info("AIP '{}' ({}) datafiles will be purged", aip.getResId(), aip.getInfo().getName());
        this.aipStatusService.purgeAipDataFile(aip.getResId());
        if (aip.getInfo().getContainsUpdatedMetadata()) {
          log.info("AIP '{}' ({}) will be refreshed in Access module", aip.getResId(), aip.getInfo().getName());
          this.aipStatusService.refreshAipInAccess(aip.getResId());
          this.aipStatusService.updateOrcidProfile(aip.getResId());
        }
      }

      if (aip.isAlreadyDisposed()) {
        log.info("AIP '{}' ({}) will be refreshed in Access module", aip.getResId(), aip.getInfo().getName());
        this.aipStatusService.refreshAipInAccess(aip.getResId());
      }

      log.info("AIP '{}' ({}) processed", aip.getResId(), aip.getInfo().getName());

    } catch (NoSuchElementException e) {
      log.error("Cannot find AIP ({})", aipMessage.getResId());
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - StoredAIPService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.NoSqlResourceService;

import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.storage.StorageService;

@Service
@ConditionalOnBean(ArchivalStorageController.class)
public class StoredAIPService extends NoSqlResourceService<StoredAIP> {

  private final StorageService storageService;

  public StoredAIPService(StorageService storageService) {
    this.storageService = storageService;
  }

  @Override
  public boolean delete(StoredAIP t) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<StoredAIP> findAll(StoredAIP search) {
    return this.storageService.listStoredAip();
  }

  @Override
  public StoredAIP findOne(String id) {
    try {
      return this.storageService.getStoredAip(id);
    } catch (final URISyntaxException e) {
      return null;
    }
  }

  @Override
  public StoredAIP save(StoredAIP t) {
    throw new UnsupportedOperationException();
  }

  @Override
  public StoredAIP update(StoredAIP t) {
    throw new UnsupportedOperationException();
  }
}

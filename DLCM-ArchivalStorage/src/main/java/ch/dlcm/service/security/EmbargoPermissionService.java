/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - EmbargoPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.service.rest.trusted.TrustedDisseminationInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedSubmissionInfoPackageRemoteResourceService;

@Service
@ConditionalOnBean(ArchivalStorageController.class)
public class EmbargoPermissionService {

  protected final ArchivalInfoPackageService aipService;
  protected final TrustedDisseminationInfoPackageRemoteResourceService dipRemoteService;
  protected final TrustedSubmissionInfoPackageRemoteResourceService sipRemoteService;

  private final MessageService messageService;

  /****************************/

  public EmbargoPermissionService(ArchivalInfoPackageService aipService, MessageService messageService,
          TrustedSubmissionInfoPackageRemoteResourceService sipRemoteService,
          TrustedDisseminationInfoPackageRemoteResourceService dipRemoteService) {

    this.aipService = aipService;
    this.sipRemoteService = sipRemoteService;
    this.dipRemoteService = dipRemoteService;
    this.messageService = messageService;
  }

  /****************************/

  public boolean isPublic(String targetResId, String resourceType) {

    if (resourceType.equals(ArchivalInfoPackage.class.getSimpleName())) {
      return this.isAipPublic(targetResId);
    } else if (resourceType.equals(SubmissionInfoPackage.class.getSimpleName())) {
      return this.isSipPublic(targetResId);
    } else if (resourceType.equals(DisseminationInfoPackage.class.getSimpleName())) {
      return this.isDipPublic(targetResId);
    } else {
      throw new SolidifyRuntimeException(this.messageService.get("error.permission.evaluation.notimplemeted", new Object[] { resourceType }));
    }
  }

  /****************************/

  private boolean isAipPublic(String resId) {
    final ArchivalInfoPackage aip = this.aipService.findOne(resId);
    return aip.getInfo().isAccessCurrentlyPublic();
  }

  private boolean isDipPublic(String resId) {
    final DisseminationInfoPackage dip = this.dipRemoteService.findOne(resId);
    return dip.getInfo().isAccessCurrentlyPublic();
  }

  private boolean isSipPublic(String resId) {
    final SubmissionInfoPackage sip = this.sipRemoteService.findOne(resId);
    return sip.getInfo().isAccessCurrentlyPublic();
  }
}

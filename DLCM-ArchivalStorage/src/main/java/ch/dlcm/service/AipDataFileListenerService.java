/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - AipDataFileListenerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.AipDataFileService;
import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.message.AipMessage;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
@ConditionalOnBean(ArchivalStorageController.class)
public class AipDataFileListenerService extends AbstractAipDataFileListenerService {

  private static final String NOTIFICATION_AIP_DATAFILE_ERROR = "archival.notification.aip.datafile.error";

  public AipDataFileListenerService(DLCMProperties dlcmProperties,
                                    MessageService messageService,
                                    DataFileStatusService dfStatusService,
                                    TrustedUserRemoteResourceService userRemoteService,
                                    TrustedNotificationRemoteResourceService notificationRemoteService,
                                    TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService,
                                    AipDataFileService aipDataFileService,
                                    ArchivalInfoPackageService aipService) {
    super(dlcmProperties,
          messageService,
          dfStatusService,
          userRemoteService,
          notificationRemoteService,
          organizationalUnitRemoteService,
          aipDataFileService,
          aipService);
  }

  @Override
  protected void publishEvent(String aipId, boolean isBigPackage) {
    SolidifyEventPublisher.getPublisher().publishEvent(new AipMessage(aipId, isBigPackage));
  }

  @Override
  protected NotificationType getNotificationType() {
    return NotificationType.IN_ERROR_AIP_INFO;
  }

  @Override
  protected String getErrorMessage(String dataFileId) {
    return this.messageService.get(NOTIFICATION_AIP_DATAFILE_ERROR, new Object[] { dataFileId });
  }

}

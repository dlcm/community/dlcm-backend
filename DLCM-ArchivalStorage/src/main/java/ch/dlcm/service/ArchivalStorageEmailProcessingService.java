/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - ArchivalStorageEmailProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import jakarta.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.model.EmailParameters;
import ch.unige.solidify.service.EmailService;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.ArchivalStorageController;
import ch.dlcm.message.EmailMessage;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.User;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
@ConditionalOnBean(ArchivalStorageController.class)
@Profile("email-service")
public class ArchivalStorageEmailProcessingService extends EmailProcessingService {
  private static final Logger log = LoggerFactory.getLogger(ArchivalStorageEmailProcessingService.class);

  private final ArchivalInfoPackageService archivalInfoPackageService;
  private final TrustedUserRemoteResourceService userRemoteResourceService;

  protected ArchivalStorageEmailProcessingService(MessageService messageService, EmailService emailService,
          DLCMProperties dlcmProperties, ArchivalInfoPackageService archivalInfoPackageService,
          TrustedUserRemoteResourceService userRemoteResourceService) {
    super(messageService, emailService, dlcmProperties);
    this.archivalInfoPackageService = archivalInfoPackageService;
    this.userRemoteResourceService = userRemoteResourceService;
  }

  @Override
  protected void processEmailMessage(EmailMessage emailMessage) {
    log.info("Reading message by Storagion module {}", emailMessage);
    try {
      switch (emailMessage.getTemplate()) {
        case NEW_ARCHIVES_COMPLETED -> this.sendEmailWithArchiveAndCreatorList(emailMessage);
        case MY_ARCHIVES_COMPLETED -> this.sendEmailWithArchiveList(emailMessage);
        case NEW_REQUEST_TO_ACCESS_DATASET -> this.sendEmailWithArchiveAndEmitterList(emailMessage);
      }
    } catch (MessagingException e) {
      log.error("Cannot send email ({}): {}", emailMessage.getTemplate().getSubject(), e.getMessage(), e);
    }
  }

  private void sendEmailWithArchiveList(EmailMessage emailMessage) throws MessagingException {
    List<ArchivalInfoPackage> aips = new ArrayList<>();
    for (String aipId : emailMessage.getParameters().keySet()) {
      aips.add(this.archivalInfoPackageService.findOne(aipId));
    }

    Map<String, Object> parameters = this.getEmailDefaultParameters();
    parameters.put("aips", aips);

    this.sendEmailWithParameters(emailMessage, parameters);
  }

  private void sendEmailWithArchiveAndCreatorList(EmailMessage emailMessage) throws MessagingException {
    List<ArchivalInfoPackage> aips = new ArrayList<>();
    List<String> creators = new ArrayList<>();
    for (String aipId : emailMessage.getParameters().keySet()) {
      ArchivalInfoPackage aip = this.archivalInfoPackageService.findOne(aipId);
      aips.add(aip);
      creators.add(aip.getCreatorName());
    }

    Map<String, Object> parameters = this.getEmailDefaultParameters();
    parameters.put("aips", aips);
    parameters.put("creators", creators);

    this.sendEmailWithParameters(emailMessage, parameters);
  }

  private void sendEmailWithArchiveAndEmitterList(EmailMessage emailMessage) throws MessagingException {
    List<ArchivalInfoPackage> aips = new ArrayList<>();
    List<String> emitters = new ArrayList<>();

    for (String emitterId : emailMessage.getParameters().keySet()) {
      ArchivalInfoPackage aip = this.archivalInfoPackageService.findOne((String) emailMessage.getParameters().get(emitterId));
      aips.add(aip);
      List<User> shouldHaveOnePerson = this.userRemoteResourceService.findByPersonResId(emitterId);
      shouldHaveOnePerson.stream().findFirst().ifPresent(user -> emitters.add(user.getEmail()));
    }

    Map<String, Object> parameters = this.getEmailDefaultParameters();
    parameters.put("aips", aips);
    parameters.put("emitters", emitters);

    this.sendEmailWithParameters(emailMessage, parameters);
  }

  private void sendEmailWithParameters(EmailMessage emailMessage, Map<String, Object> parameters) throws MessagingException {
    EmailParameters emailParameters = new EmailParameters().setToList(Collections.singletonList(emailMessage.getTo())).setSubject(
            emailMessage.getTemplate().getSubject())
            .setTemplate(emailMessage.getTemplate().toString().toLowerCase()).setTemplateParameters(parameters).addCc(null).addBcc(null);

    this.emailService.sendEmailWithTemplate(emailParameters);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - DNAStorageServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.storage;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.GitInfoProperties;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.service.MetadataService;
import ch.dlcm.storage.dna.DNAStorageService;

@ActiveProfiles({ "arc-dna", "sec-noauth" })
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(SpringExtension.class)
class DNAStorageServiceTest extends StorageServiceTest {

  @BeforeEach
  public void setup() {
    super.setUp();
    // DLCM Properties
    this.dlcmProperties = new DLCMProperties(new SolidifyProperties(new GitInfoProperties()), new MultipartProperties());
    this.dlcmProperties.setHome(this.workingDir.toAbsolutePath().toString());
    this.dlcmProperties.getStorage().getDnaProperties().setCodecType("test-codec");
    this.dlcmProperties.getStorage().setUrl(this.storageDir.toAbsolutePath().toUri().toString());
    this.dlcmProperties.getSecuredStorage().getDnaProperties().setCodecType("test-codec");
    this.dlcmProperties.getSecuredStorage().setUrl(this.securedStorageDir.toAbsolutePath().toUri().toString());
    // DLCM Repository description
    this.repositoryDescription = new DLCMRepositoryDescription(this.gitInfoProperties);
    // Services
    this.metadataService = new MetadataService(
            this.fallbackLanguageRemoteService,
            this.fallbackOrgUnitRemoteService,
            this.fallbackAipRemoteService,
            this.historyService,
            this.dlcmProperties,
            this.repositoryDescription,
            this.messageService,
            this.gitInfoProperties,
            this.fileFormatService,
            this.fallbackMetadataTypeRemoteService,
            this.fallbackPersonRemoteService,
            this.fallbackLicenseRemoteService,
            this.fallbackArchivePublicMetadataRemoteService,
            this.fallbackArchivePrivateMetadataRemoteService,
            this.fallbackPreservationPolicyRemoteService,
            this.fallbackArchiveTypeResourceService,
            this.trustedDepositRemoteService);
    this.storageService = new DNAStorageService(
            this.dlcmProperties,
            this.repositoryDescription,
            this.messageService,
            this.metadataService,
            this.fallbackOrgUnitRemoteService,
            this.fallbackArchivePublicDataRemoteService,
            this.fallbackArchivePublicMetadataRemoteService,
            this.fallbackArchivePrivateMetadataRemoteService,
            this.historyService);
  }

  @Order(10)
  @ParameterizedTest
  @MethodSource("supportedVersions")
  void bagItStorageTest(DLCMMetadataVersion version) {
    final String prefix = "faust-1072055880-aip-bagit";
    int fileNumber = 10 + 8;
    assertDoesNotThrow(() -> this.runTestOnStorage(version, prefix, fileNumber));
  }

  @Order(20)
  @ParameterizedTest
  @MethodSource("supportedVersions")
  void zipStorageTest(DLCMMetadataVersion version) {
    final String prefix = "faust-1072055880-aip-zip";
    int fileNumber = 10;
    assertDoesNotThrow(() -> this.runTestOnStorage(version, prefix, fileNumber));
  }

}

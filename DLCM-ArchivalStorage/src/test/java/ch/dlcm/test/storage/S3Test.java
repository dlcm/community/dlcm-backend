/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - S3Test.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.storage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3Configuration;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Exception;
import software.amazon.awssdk.services.s3.model.S3Object;

@ExtendWith(SpringExtension.class)
@Disabled("Run these tests manually if you have an S3 account")
class S3Test {
  private static final String ACCESS_KEY = "change-it"; // May need to be base64 encoded value of your access key

  private static final String BUCKET = "name of an existing bucket from your s3 storage";
  private static final String DATA_DIRECTORY = "/example/path/";
  private static final String DATA_FILE = DATA_DIRECTORY + "example_file.txt";
  private static final boolean IS_PATH_STYLE_ACCESS = false; // false by default. Set to true if s3 storage requires path-style access
  private static final Logger log = LoggerFactory.getLogger(S3Test.class);
  private static final String OBJECT = "11111";
  private static final String SECRET_KEY = "change-it"; // May need to be MD5 hash value of your secret key
  private static final String TENANT = "s3 storage endpoint";

  @Test
  void createS3ObjectTest() throws AwsServiceException, SdkClientException {
    final S3Client s3Client = this.getS3Client();
    uploadObject(this.getObjectId(), s3Client);
  }

  @Test
  void dataLengthTest() throws AwsServiceException, SdkClientException {
    final S3Client s3Client = this.getS3Client();
    final File dataFile = new File(DATA_FILE);
    final String objectId = this.getObjectId();

    uploadObject(objectId, s3Client);

    HeadObjectRequest headObjectRequest = HeadObjectRequest.builder()
            .bucket(BUCKET)
            .key(objectId)
            .build();

    final long dataFromS3Length = s3Client.headObject(headObjectRequest).contentLength();
      assertEquals(dataFile.length(), dataFromS3Length);
  }

  @Test
  void deleteS3ObjectTest() throws AwsServiceException, SdkClientException {
    final S3Client s3Client = this.getS3Client();
    final String objectId = this.getObjectId();
    if (!this.doesBucketExist(s3Client)) {
      CreateBucketRequest bucketRequest = CreateBucketRequest.builder()
              .bucket(BUCKET)
              .build();

      s3Client.createBucket(bucketRequest);
    }

    uploadObject(objectId, s3Client);

    // Delete file
    DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder()
            .bucket(BUCKET)
            .key(objectId)
            .build();

    s3Client.deleteObject(deleteObjectRequest);

    HeadObjectRequest headObjectRequest = HeadObjectRequest.builder()
            .bucket(BUCKET)
            .key(objectId)
            .build();

    // test object has been deleted
    assertThrows(S3Exception.class, () -> s3Client.headObject(headObjectRequest));
  }

  @Test
  void getS3ObjectTest() throws AwsServiceException, SdkClientException, IOException {
    final S3Client s3Client = this.getS3Client();
    final String objectId = this.getObjectId();
    final String filePath = DATA_DIRECTORY + objectId;

    this.uploadObject(objectId, s3Client);

    // Build the GetObjectRequest
    GetObjectRequest getObjectRequest = GetObjectRequest.builder()
            .bucket(BUCKET)
            .key(objectId)
            .build();

    // Use try-with-resources to ensure streams are closed properly
    try (ResponseInputStream<GetObjectResponse> s3is = s3Client.getObject(getObjectRequest)) {
      Path target = Path.of(filePath);
      Files.copy(s3is, target);
      // Assert file exists
      assertTrue(Files.exists(target), "File should exist: " + filePath);
      log.info("Successfully downloaded object '{}' from bucket '{}' to '{}'.", objectId, BUCKET, filePath);
    }
  }

  @Test
  void listS3ObjectTest() throws AwsServiceException, SdkClientException {
    final S3Client s3Client = this.getS3Client();
    ListObjectsV2Request listObjectsRequest = ListObjectsV2Request.builder()
            .bucket(BUCKET)
            .maxKeys(1000) // Optional
            .build();

    // Make the API call
    ListObjectsV2Response listObjectsResponse = s3Client.listObjectsV2(listObjectsRequest);

    // Iterate through the returned S3 objects
    for (S3Object s3Object : listObjectsResponse.contents()) {
      log.info(" - {} (size = {})", s3Object.key(), s3Object.size());
    }
  }

  private String getObjectId() {
    return OBJECT + "-" + UUID.randomUUID();
  }

  private S3Client getS3Client() {
    AwsBasicCredentials awsCredentials = AwsBasicCredentials.create(ACCESS_KEY, SECRET_KEY);

    S3Configuration s3Config = S3Configuration.builder()
            .pathStyleAccessEnabled(IS_PATH_STYLE_ACCESS)
            .build();

    return S3Client.builder()
            .credentialsProvider(StaticCredentialsProvider.create(awsCredentials))
            .endpointOverride(URI.create(TENANT))
            .region(Region.of("GLOBAL"))
            .serviceConfiguration(s3Config)
            .build();
  }

  private boolean doesBucketExist(S3Client s3Client) {
    HeadBucketRequest headBucketRequest = HeadBucketRequest.builder()
            .bucket(BUCKET)
            .build();
    try {
      s3Client.headBucket(headBucketRequest);
      return true;
    } catch (NoSuchBucketException e) {
      return false;
    }
  }

  private void uploadObject(String objectId, S3Client s3Client) {
    File upload = new File(DATA_FILE);
    PutObjectRequest request = PutObjectRequest.builder()
            .bucket(BUCKET)
            .key(objectId)
            .contentLength(upload.length())
            .build();

    // Upload file
    s3Client.putObject(request, upload.toPath());
  }
}

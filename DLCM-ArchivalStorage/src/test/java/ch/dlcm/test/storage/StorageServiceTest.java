/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - StorageServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.storage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.when;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.params.provider.Arguments;
import org.mockito.Mock;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.Access;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.DataFileChecksum.ChecksumOrigin;
import ch.dlcm.model.DataFileChecksum.ChecksumType;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.FileFormat;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.Tool;
import ch.dlcm.model.VirusCheck;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicDataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchiveTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLanguageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLicenseRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackMetadataTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPersonRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPreservationPolicyRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;
import ch.dlcm.storage.StorageService;

public abstract class StorageServiceTest {

  private static final String ORG_UNIT_ID = DLCMConstants.DB_ORG_UNIT_ID;

  protected DLCMProperties dlcmProperties;
  protected DLCMRepositoryDescription repositoryDescription;
  protected Path storageDir = Paths.get("src", "test", "resources", "storage");
  protected Path securedStorageDir = Paths.get("src", "test", "resources", "securedStorage");
  protected Path workingDir = Paths.get("src", "test", "resources", "testing");

  protected StorageService storageService;
  protected MetadataService metadataService;
  protected GitInfoProperties gitInfoProperties = new GitInfoProperties();

  @Mock
  protected HistoryService historyService;
  @Mock
  protected MessageService messageService;
  @Mock
  protected FileFormatService fileFormatService;

  @Mock
  protected FallbackArchivalInfoPackageRemoteResourceService fallbackAipRemoteService;
  @Mock
  protected FallbackLanguageRemoteResourceService fallbackLanguageRemoteService;
  @Mock
  protected FallbackLicenseRemoteResourceService fallbackLicenseRemoteService;
  @Mock
  protected FallbackMetadataTypeRemoteResourceService fallbackMetadataTypeRemoteService;
  @Mock
  protected FallbackOrganizationalUnitRemoteResourceService fallbackOrgUnitRemoteService;
  @Mock
  protected FallbackPersonRemoteResourceService fallbackPersonRemoteService;
  @Mock
  protected FallbackArchivePublicDataRemoteResourceService fallbackArchivePublicDataRemoteService;
  @Mock
  protected FallbackArchivePublicMetadataRemoteResourceService fallbackArchivePublicMetadataRemoteService;
  @Mock
  protected FallbackArchivePrivateMetadataRemoteResourceService fallbackArchivePrivateMetadataRemoteService;
  @Mock
  protected FallbackSubmissionInfoPackageRemoteResourceService fallbackSipRemoteService;
  @Mock
  protected FallbackPreservationPolicyRemoteResourceService fallbackPreservationPolicyRemoteService;
  @Mock
  protected FallbackArchiveTypeRemoteResourceService fallbackArchiveTypeResourceService;

  @Mock
  protected TrustedDepositRemoteResourceService trustedDepositRemoteService;

  protected static Stream<Arguments> supportedVersions() {
    return Stream.of(
            arguments(DLCMMetadataVersion.V1_0),
            arguments(DLCMMetadataVersion.V1_1),
            arguments(DLCMMetadataVersion.V2_0),
            arguments(DLCMMetadataVersion.V2_1),
            arguments(DLCMMetadataVersion.V3_0),
            arguments(DLCMMetadataVersion.V3_1));
  }

  @AfterEach
  public void purgeData() {
    FileTool.deleteFolder(this.workingDir);
    FileTool.deleteFolder(this.storageDir);
    FileTool.deleteFolder(this.securedStorageDir);
  }

  public void setUp() {
    if (!FileTool.ensureFolderExists(this.storageDir)) {
      throw new SolidifyRuntimeException("Cannot create " + this.storageDir);
    }
    if (!FileTool.ensureFolderExists(this.securedStorageDir)) {
      throw new SolidifyRuntimeException("Cannot create " + this.securedStorageDir);
    }
    // Mock the findOne method for Organizational Unit
    final OrganizationalUnit orgUnit = new OrganizationalUnit();
    orgUnit.setResId(ORG_UNIT_ID);
    orgUnit.setName("Organization Unit");
    when(this.fallbackOrgUnitRemoteService.findOneWithCache(ORG_UNIT_ID)).thenReturn(orgUnit);
  }

  protected ArchivalInfoPackage getAip(DLCMMetadataVersion version, String aipName, Access accessLevel, DataTag dataTag,
          DataUsePolicy dataUsePolicy, Path source) {
    final ArchivalInfoPackage aip = new ArchivalInfoPackage();
    aip.init();
    aip.setResId(aipName + "-" + version.getVersion());
    aip.getCreation().setWhen(OffsetDateTime.now());
    aip.getLastUpdate().setWhen(OffsetDateTime.now());
    // Access Level
    aip.getInfo().setAccess(accessLevel);
    // Data tag
    aip.getInfo().setDataSensitivity(dataTag);
    // DUA Type
    aip.getInfo().setDataUsePolicy(dataUsePolicy);
    // Compliance level
    aip.getInfo().setComplianceLevel(ComplianceLevel.WEAK_COMPLIANCE);
    // Publication date
    aip.setPublicationDate(LocalDate.now());
    // AIU => Archive Unit
    aip.setArchivalUnit(true);
    // Define AIP container
    if (aipName.endsWith("bagit")) {
      aip.setArchiveContainer(ArchiveContainer.BAG_IT);
    } else if (aipName.endsWith("zip")) {
      aip.setArchiveContainer(ArchiveContainer.ZIP);
    } else {
      throw new SolidifyRuntimeException("AIP container unknown");
    }
    this.completeSip(aip, version);
    this.addDataFile(aip, source, aip.getResId());
    return aip;
  }

  protected void runTestOnStorage(DLCMMetadataVersion version, String filePrefix, int expectedFileNumber) {
    this.runTest(version, filePrefix, expectedFileNumber, Access.PUBLIC, DataTag.BLUE, DataUsePolicy.LICENSE);
  }

  protected void runTestForStoredAip() {
    final List<StoredAIP> listAip = this.storageService.listStoredAip();
    assertEquals(1, listAip.size());
    final StoredAIP storedAip = listAip.get(0);
    assertNotNull(storedAip.getResId());
    assertNotNull(storedAip.getOrganizationalUnitId());
    assertNotNull(storedAip.getAipId());
    assertNotNull(storedAip.getArchiveId());
    assertTrue(FileTool.checkFile(storedAip.getArchiveUri()));
  }

  protected void runTestOnSecuredStorage(DLCMMetadataVersion version, String filePrefix, int expectedFileNumber) {
    this.runTest(version, filePrefix, expectedFileNumber, Access.CLOSED, DataTag.RED, DataUsePolicy.EXTERNAL_DUA);
  }

  private void runTest(DLCMMetadataVersion version, String filePrefix, int expectedFileNumber, Access accessLevel, DataTag dataTag,
          DataUsePolicy dataUsePolicy) {
    try {
      // Support thumbnail from version 2.1
      if (DLCMMetadataVersion.V2_1.getVersionNumber() <= version.getVersionNumber()) {
        expectedFileNumber++;
      }
      this.runStorageTest(expectedFileNumber,
              this.getAip(version, filePrefix, accessLevel, dataTag, dataUsePolicy,
                      Paths.get("src", "test", "resources", "aip").toAbsolutePath()));
    } catch (final Exception e) {
      this.returnException(version, e);
    }
  }

  protected void returnException(DLCMMetadataVersion version, Exception e) {
    throw new SolidifyCheckingException("[" + version.toString() + "] " + e.getMessage(), e);
  }

  private void addDataFile(ArchivalInfoPackage aip, Path source, String aipName) {
    final Path aipFile = Paths.get(source.toString()).resolve(aipName + SolidifyConstants.ZIP_EXT);

    final AipDataFile aipDataFile = new AipDataFile(aip);
    aipDataFile.setDataCategory(DataCategory.Package);
    aipDataFile.setDataType(DataCategory.InformationPackage);
    aipDataFile.setSourceData(aipFile.toUri());

    if (!aip.addItem(aipDataFile)) {
      throw new SolidifyRuntimeException("Cannot add data file " + aipDataFile.getFileName());
    }
  }

  private void completeSip(ArchivalInfoPackage aip, DLCMMetadataVersion version) {
    aip.getInfo().setMetadataVersion(version);
    aip.setPackageStatus(PackageStatus.IN_PROGRESS);
    aip.getInfo().setName("Faust 1072055880");
    aip.getInfo().setDescription("Testing");
    aip.getInfo().setOrganizationalUnitId(ORG_UNIT_ID);
    aip.setDispositionApproval(true);
    aip.setRetention(1);
  }

  private Path getArchive(ArchivalInfoPackage aip) {
    final Path aipFile = this.storageService.getAIPFolder(aip.getResId()).resolve(ResourceName.AIP_FILE);
    try (InputStream aipStream = this.storageService.getAIPStream(aip)) {

      // check if folder exists
      if (!FileTool.ensureFolderExists(aipFile.getParent())) {
        throw new IOException("Cannot create " + aipFile.getParent());
      }

      try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(aipFile.toFile()), SolidifyConstants.BUFFER_SIZE)) {

        int byteRead;
        final byte[] data = new byte[SolidifyConstants.BUFFER_SIZE];
        while ((byteRead = aipStream.read(data, 0, data.length)) != -1) {
          outputStream.write(data, 0, byteRead);
        }
        outputStream.flush();

      } catch (final IOException e) {
        throw e;
      }
    } catch (final IOException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
    return aipFile;
  }

  private Tool getTool(String name) {
    final Tool t = new Tool();
    t.setName(name);
    t.setDescription("Testing tool");
    t.setVersion("1.0.0");
    return t;
  }

  private void processDataFiles(Path home, ArchivalInfoPackage aip) throws NoSuchAlgorithmException, IOException {
    for (final AipDataFile df : aip.getDataFiles()) {
      // Copy file
      final Path target = df.defineTargetFile(home.resolve(aip.getResId()).toString());
      FileTool.copyFile(Paths.get(df.getSourceData()), target);
      df.setFinalData(target.toUri());
      df.setFileSize(FileTool.getSize(target));
      df.setComplianceLevel(ComplianceLevel.FULL_COMPLIANCE);
      // generate checksum
      this.setChecksum(df);
      // generate tool info
      this.setFileFormat(df);
      this.setVirusCheck(df);
      df.setStatus(DataFileStatus.READY);
    }
  }

  protected void runStorageTest(int expectedFileNumber, ArchivalInfoPackage aip) throws Exception {
    final String prefix = "[Archive " + aip.getInfo().getMetadataVersion().getVersion() + "] ";
    this.processDataFiles(Paths.get(this.dlcmProperties.getArchivalLocation()), aip);
    this.storageService.checkAIP(aip);
    this.storageService.storeAIP(aip);
    String archiveId;
    if ((archiveId = this.storageService.submitAIP(aip)) == null) {
      throw new SolidifyRuntimeException(prefix + "Cannot get archive URI");
    }
    aip.setArchiveId(archiveId);
    aip.setPackageStatus(PackageStatus.STORED);
    final Path aipFile = this.getArchive(aip);
    assertTrue(FileTool.checkFile(aipFile), prefix + "Cannot find file");
    final ZipTool aipZip = new ZipTool(aipFile.toUri());
    assertTrue(aipZip.checkZip(), prefix + "Incorrect zip file");
    assertEquals(aipZip.getFileNumber(), expectedFileNumber, prefix + "Incorrect file number");
    this.storageService.checkIndexing(aip.getResId());
    this.storageService.indexAIP(aip);
  }

  private void setChecksum(AipDataFile df) throws NoSuchAlgorithmException, IOException {
    final String[] algoList = this.dlcmProperties.getParameters().getChecksumList();
    final String[] checksumList = ChecksumTool.computeChecksum(
            new BufferedInputStream(new FileInputStream(Paths.get(df.getFinalData()).toFile()), SolidifyConstants.BUFFER_SIZE),
            algoList);
    for (int i = 0; i < algoList.length; i++) {
      df.getChecksums()
              .add(new DataFileChecksum(ChecksumAlgorithm.valueOf(algoList[i]), ChecksumType.COMPLETE, ChecksumOrigin.DLCM, checksumList[i]));
    }
  }

  private void setFileFormat(AipDataFile df) {
    final FileFormat ff = new FileFormat();
    final String contentType = FileTool.getContentType(df.getFinalData()).toString();
    ff.setContentType(contentType);
    ff.setFormat(contentType);
    ff.setTool(this.getTool("FileFormat"));
    df.setFileFormat(ff);
  }

  private void setVirusCheck(AipDataFile df) {
    final VirusCheck vc = new VirusCheck();
    vc.setCheckDate(OffsetDateTime.now());
    vc.setTool(this.getTool("VirusCheck"));
    df.setVirusCheck(vc);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Archival Storage - FastaToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.dna;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.biojava.nbio.core.sequence.DNASequence;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.storage.dna.fasta.FastaTool;

class FastaToolTest {

  private static final Logger log = LoggerFactory.getLogger(FastaToolTest.class);

  private Path testFolder = Paths.get("src", "test", "resources", "dna").toAbsolutePath();

  @ParameterizedTest
  @ValueSource(strings = { "sample1.dna.fasta", "sample2.dna.fasta", "sample3.dna.fasta", "BRCA2-cds.dna.fasta", "sample4-wrong.dna.fasta",
          "sample5-wrong.dna.fasta", "crab.protein.fasta", "sample1.rna.fasta", "sample2.rna.fasta", "sample6.dnamic.dna.fasta" })
  void readTest(String file) {
    final Path sourcePath = this.getFilePath(this.testFolder, file);
    this.logFile(file, "log");
    FastaTool.logFasta(sourcePath);
  }

  @ParameterizedTest
  @ValueSource(strings = { "sample1.dna.fasta", "sample2.dna.fasta", "sample3.dna.fasta", "BRCA2-cds.dna.fasta", "sample4-wrong.dna.fasta",
          "sample5-wrong.dna.fasta", "crab.protein.fasta", "sample6.dnamic.dna.fasta" })
  void checkTest(String file) {
    final Path sourcePath = this.getFilePath(this.testFolder, file);
    boolean result = true;
    this.logFile(file, "check");
    if (file.contains("wrong")) {
      result = false;
    }
    if (file.endsWith(".dna.fasta")) {
      assertEquals(result, FastaTool.checkDNAFasta(sourcePath));
    } else if (file.endsWith(".rna.fasta")) {
      assertEquals(result, FastaTool.checkRNAFasta(sourcePath));
    } else if (file.endsWith(".protein.fasta")) {
      assertEquals(result, FastaTool.checkProteinFasta(sourcePath));
    } else {
      fail("Wrong file name");
    }
  }

  @ParameterizedTest
  @ValueSource(strings = { "sample1.dna.fasta", "sample2.dna.fasta", "sample3.dna.fasta", "BRCA2-cds.dna.fasta", "sample6.dnamic.dna.fasta" })
  void getInfoTest(String file) {
    final Path sourcePath = this.getFilePath(this.testFolder, file);
    this.logFile(file, "first description");
    if (file.endsWith(".dna.fasta")) {
      final DNASequence dnaSequence = FastaTool.getDynamicFirstSequence(sourcePath);
      log.info("Original header: {}", dnaSequence.getOriginalHeader());
      assertNotNull(dnaSequence.getOriginalHeader());
      if (!StringTool.isNullOrEmpty(dnaSequence.getDescription())) {
        log.info("Description: {}", dnaSequence.getDescription());
        log.info("Comment: {}", dnaSequence.getComments());
      }
    } else {
      fail("Wrong file name");
    }
  }

  private Path getFilePath(Path parentPath, String filename) {
    final Path file = parentPath.resolve(filename);
    assertNotNull(file);
    assertTrue(FileTool.checkFile(file));
    return file;
  }

  private void logFile(String file, String action) {
    log.info(
            "\n===================================================================\n{} [{}]\n===================================================================",
            file, action);

  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Security - TrustedTokenChecker.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.util.AbstractRestClientTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.rest.ResourceName;

@Profile("trusted-token-checker")
@Service
public class TrustedTokenChecker {
  private static final Logger log = LoggerFactory.getLogger(TrustedTokenChecker.class);

  private final AbstractRestClientTool restClientTool;
  private final String adminModuleUrl;

  public TrustedTokenChecker(AbstractRestClientTool restClientTool, DLCMProperties dlcmProperties) {
    this.restClientTool = restClientTool;
    this.adminModuleUrl = dlcmProperties.getModule().getAdmin().getUrl();
  }

  @Scheduled(fixedDelayString = "${dlcm.security.trustedTokenCheckerDelayMilliseconds:1000}")
  public void testTrustedToken() {
    String url = this.adminModuleUrl + "/" + ResourceName.USER;
    RestTemplate restTemplate = this.restClientTool.getClient();
    try {
      ResponseEntity<?> responseEntity = restTemplate.exchange(url, HttpMethod.HEAD, HttpEntity.EMPTY, Void.class);
      log.info(responseEntity.getStatusCode().toString());
    } catch (HttpClientErrorException.Unauthorized exception) {
      log.info(exception.getStatusCode().toString());
    }
  }
}

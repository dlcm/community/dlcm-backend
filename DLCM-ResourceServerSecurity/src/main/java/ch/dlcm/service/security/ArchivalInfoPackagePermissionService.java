/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Resource Server Security - ArchivalInfoPackagePermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import org.springframework.stereotype.Service;

import ch.dlcm.business.ArchivalInfoPackageService;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;

@Service
public class ArchivalInfoPackagePermissionService extends AbstractPermissionWithOrgUnitService {

  private final ArchivalInfoPackageService aipService;

  public ArchivalInfoPackagePermissionService(ArchivalInfoPackageService aipService,
          TrustedPersonRemoteResourceService personRemoteResourceService) {
    super(personRemoteResourceService);
    this.aipService = aipService;
  }

  @Override
  protected boolean isApproverAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    /**
     * So far, use same rights as Creator
     */
    return this.isCreatorAllowed(existingResource, action);
  }

  @Override
  protected boolean isManagerAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return true;
  }

  @Override
  protected boolean isStewardAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    return existingResource instanceof ArchivalInfoPackage &&
            (action == DLCMControllerAction.RESUME ||
                    action == DLCMControllerAction.APPROVE_DISPOSAL_BY_ORGUNIT ||
                    action == DLCMControllerAction.EXTEND_RETENTION ||
                    action == DLCMControllerAction.CHECK ||
                    action == DLCMControllerAction.DELETE);
  }

  @Override
  public ArchivalInfoPackage getExistingResource(String id) {
    return this.aipService.findOne(id);
  }

}

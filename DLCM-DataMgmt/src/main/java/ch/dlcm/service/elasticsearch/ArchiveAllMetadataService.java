/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Data Management - ArchiveAllMetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.elasticsearch;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.index.ElasticsearchClientProvider;
import ch.unige.solidify.index.indexing.elasticsearch.ElasticsearchIndexMetadataService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.DataMgmtController;
import ch.dlcm.model.MetadataVisibility;
import ch.dlcm.model.index.ArchiveMetadata;

@Profile("index-elasticsearch")
@Service("allArchiveMetadata")
@ConditionalOnBean(DataMgmtController.class)
public class ArchiveAllMetadataService extends ElasticsearchIndexMetadataService<ArchiveMetadata> {

  public ArchiveAllMetadataService(
          IndexConfigProperties indexConfigProperties,
          DLCMProperties dlcmProperties,
          ElasticsearchClientProvider elasticsearchClientProvider) {
    super(
            indexConfigProperties,
            elasticsearchClientProvider,
            dlcmProperties.getIndexing().getIndexName(MetadataVisibility.ALL),
            ArchiveMetadata.class);
  }

  @Override
  protected ArchiveMetadata getMetadataObject() {
    return new ArchiveMetadata();
  }

}

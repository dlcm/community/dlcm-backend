/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Data Management - DIController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.datamgmt;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.business.IndexFieldAliasService;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.dlcm.controller.DataMgmtController;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.rest.propagate.PropagateOrganizationalUnitRemoteResourceService;

@UserPermissions
@RestController
@ConditionalOnBean(DataMgmtController.class)
@RequestMapping(UrlPath.DATA_MGMT_DI)
public class DIController extends DLCMIndexingController {

  public DIController(
          @Qualifier("allArchiveMetadata") IndexingService<ArchiveMetadata> noSqlResourceService,
          IndexFieldAliasService indexFieldAliasService,
          PropagateOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService) {
    super(noSqlResourceService, indexFieldAliasService, organizationalUnitRemoteResourceService);
  }

  // **********************************
  // Override methods to apply security
  // **********************************
  @TrustedUserPermissions
  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> list(@ModelAttribute ArchiveMetadata search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<ArchiveMetadata> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<ArchiveMetadata> update(@PathVariable String id, @RequestBody ArchiveMetadata t2) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> search(
          @RequestParam(required = false) String query,
          Pageable pageable) {
    return super.search(query, pageable);
  }

  @Override
  public HttpEntity<RestCollection<ArchiveMetadata>> searchPost(
          @RequestBody(required = false) List<SearchCondition> searchConditions,
          Pageable pageable) {
    return super.searchPost(searchConditions, pageable);
  }

}

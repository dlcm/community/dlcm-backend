/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Data Management - DLCMIndexingController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.datamgmt;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import ch.unige.solidify.business.IndexFieldAliasService;
import ch.unige.solidify.controller.index.IndexDataWriteController;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.BooleanClauseType;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.Access;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.rest.propagate.PropagateOrganizationalUnitRemoteResourceService;

public abstract class DLCMIndexingController extends IndexDataWriteController<ArchiveMetadata> {

  private static final String NO_ORG_UNIT_AUTHORIZATION = "[no authorized org unit]";

  private final PropagateOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService;

  protected DLCMIndexingController(
          IndexingService<ArchiveMetadata> noSqlResourceService,
          IndexFieldAliasService indexFieldAliasService,
          PropagateOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService) {
    super(noSqlResourceService, indexFieldAliasService);
    this.organizationalUnitRemoteResourceService = organizationalUnitRemoteResourceService;
  }

  /**
   * Add search conditions to return only results belonging to org units the user has rights on or that are public
   *
   * @param searchConditions
   */
  @Override
  protected List<SearchCondition> beforeSearch(List<SearchCondition> searchConditions) {

    // Add a nested query condition that matches authorized org units OR public access level
    SearchCondition authorizedOrgUnitsOrPublicCondition = new SearchCondition();
    authorizedOrgUnitsOrPublicCondition.setBooleanClauseType(BooleanClauseType.FILTER);
    authorizedOrgUnitsOrPublicCondition.setType(SearchConditionType.NESTED_BOOLEAN);
    authorizedOrgUnitsOrPublicCondition.getNestedConditions().add(this.getAuthorizedOrgUnitCondition());
    authorizedOrgUnitsOrPublicCondition.getNestedConditions().add(this.getPublicAccessLevelCondition());
    authorizedOrgUnitsOrPublicCondition.getNestedConditions().add(this.getContentStructurePublicCondition());
    searchConditions.add(authorizedOrgUnitsOrPublicCondition);
    return searchConditions;
  }

  private SearchCondition getContentStructurePublicCondition() {
    SearchCondition publicAccessLevelCondition = new SearchCondition();
    publicAccessLevelCondition.setField(DLCMConstants.AIP_CONTENT_STRUCTURE_PUBLIC);
    publicAccessLevelCondition.setBooleanClauseType(BooleanClauseType.SHOULD);
    publicAccessLevelCondition.setType(SearchConditionType.TERM);
    publicAccessLevelCondition.setValue(Boolean.TRUE.toString());
    return publicAccessLevelCondition;
  }

  private SearchCondition getAuthorizedOrgUnitCondition() {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    List<OrganizationalUnit> authorizedOrgUnits = this.organizationalUnitRemoteResourceService.getAuthorizedOrganizationalUnit(authentication);

    SearchCondition orgUnitsCondition = new SearchCondition();
    orgUnitsCondition.setField(DLCMConstants.AIP_ORGA_UNIT + DLCMConstants.INDEXING_KEYWORD);
    orgUnitsCondition.setBooleanClauseType(BooleanClauseType.SHOULD);
    orgUnitsCondition.setType(SearchConditionType.TERM);

    if (!authorizedOrgUnits.isEmpty()) {
      List<String> authorizedOrgUnitIds = authorizedOrgUnits.stream().map(OrganizationalUnit::getResId).collect(Collectors.toList());
      orgUnitsCondition.getTerms().addAll(authorizedOrgUnitIds);
    } else {
      // if user is not authorized on any org unit --> add a never met condition to return nothing
      orgUnitsCondition.setValue(NO_ORG_UNIT_AUTHORIZATION);
    }
    return orgUnitsCondition;
  }

  private SearchCondition getPublicAccessLevelCondition() {
    SearchCondition publicAccessLevelCondition = new SearchCondition();
    publicAccessLevelCondition.setField(DLCMConstants.AIP_ACCESS_LEVEL + DLCMConstants.INDEXING_KEYWORD);
    publicAccessLevelCondition.setBooleanClauseType(BooleanClauseType.SHOULD);
    publicAccessLevelCondition.setType(SearchConditionType.TERM);
    publicAccessLevelCondition.setValue(Access.PUBLIC.toString());
    return publicAccessLevelCondition;
  }

}

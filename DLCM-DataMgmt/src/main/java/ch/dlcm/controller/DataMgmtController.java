/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Data Management - DataMgmtController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.business.IndexFieldAliasService;
import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.index.settings.IndexingSettingsService;
import ch.unige.solidify.model.index.IndexProperties;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.UrlPath;

@RestController
@ConditionalOnProperty(prefix = "dlcm.module.data-mgmt", name = "enable")
@RequestMapping(UrlPath.DATA_MGMT)
public class DataMgmtController extends ModuleController {

  private final IndexFieldAliasService indexFieldAliasService;

  DataMgmtController(DLCMProperties dlcmProperties, IndexingSettingsService<IndexProperties> indexSettingsService,
          IndexFieldAliasService indexFieldAliasService) {
    super(ModuleName.DATAMGMT);
    this.indexFieldAliasService = indexFieldAliasService;
    // Module initialization
    if (dlcmProperties.getData().isInitIndexes()) {
      // Index creation
      indexSettingsService.init(dlcmProperties.getIndexing().getIndexDefinitionList());
    }
    if (dlcmProperties.getData().isInit()) {
      // Index Field Alias creation
      this.initDataForPublicIndex(dlcmProperties.getIndexing().getIndexName());
      this.initDataForPrivateIndex(dlcmProperties.getIndexing().getPrivateIndexName());
    }
  }

  private void initDataForPublicIndex(String publicIndex) {
    // Facets for public index
    // @formatter:off
    this.indexFieldAliasService.createIfNotExists(publicIndex,  1, DLCMConstants.ORG_UNIT_FACET, DLCMConstants.AIP_ORGA_UNIT_NAME + DLCMConstants.INDEXING_KEYWORD, true, false, 1, 10);
    this.indexFieldAliasService.createIfNotExists(publicIndex,  2, DLCMConstants.ACCESS_LEVEL_FACET, DLCMConstants.AIP_ACCESS_LEVEL + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 3);
    this.indexFieldAliasService.createIfNotExists(publicIndex,  3, DLCMConstants.CREATOR_FACET, DLCMConstants.CREATOR_NAME_FIELD + DLCMConstants.INDEXING_KEYWORD, true, false, 1, 10);
    this.indexFieldAliasService.createIfNotExists(publicIndex,  4, DLCMConstants.FORMAT_FACET, DLCMConstants.FORMAT_FIELD + DLCMConstants.INDEXING_KEYWORD, true, false, 1, 10);
    this.indexFieldAliasService.createIfNotExists(publicIndex,  5, DLCMConstants.ARCHIVE_FACET, DLCMConstants.AIP_TYPE + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 2);
    this.indexFieldAliasService.createIfNotExists(publicIndex,  6, DLCMConstants.RETENTION_FACET, DLCMConstants.AIP_RETENTION_DURATION + DLCMConstants.INDEXING_KEYWORD, true, false, 1, 5);
    this.indexFieldAliasService.createIfNotExists(publicIndex,  7, DLCMConstants.INSTITUTION_FACET, DLCMConstants.AIP_INSTITUTIONS + DLCMConstants.INDEXING_KEYWORD, true, false, 1, 10);
    this.indexFieldAliasService.createIfNotExists(publicIndex, 10, DLCMConstants.METADATA_VERSION_FACET, DLCMConstants.METADATA_VERSION_FIELD + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 7);
    this.indexFieldAliasService.createIfNotExists(publicIndex, 11, DLCMConstants.DATA_TAG_FACET, DLCMConstants.AIP_DATA_TAG + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 7);
    this.indexFieldAliasService.createIfNotExists(publicIndex, 12, DLCMConstants.DATA_USE_POLICY_FACET, DLCMConstants.AIP_DATA_USE_POLICY + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 5);
    this.indexFieldAliasService.createIfNotExists(publicIndex, 13, DLCMConstants.PUBLICATION_DATE_ALIAS, DLCMConstants.AIP_ISSUED_DATE, false, true, null, null);
    // @formatter:on
  }

  private void initDataForPrivateIndex(String privateIndex) {
    // Facets for private index
    // @formatter:off
    this.indexFieldAliasService.createIfNotExists(privateIndex,  1, DLCMConstants.ORG_UNIT_FACET, DLCMConstants.AIP_ORGA_UNIT_NAME + DLCMConstants.INDEXING_KEYWORD, true, false, 1, 10);
    this.indexFieldAliasService.createIfNotExists(privateIndex,  2, DLCMConstants.ACCESS_LEVEL_FACET, DLCMConstants.AIP_ACCESS_LEVEL + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 3);
    this.indexFieldAliasService.createIfNotExists(privateIndex,  3, DLCMConstants.TYPE_FACET, "type" + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 3);
    this.indexFieldAliasService.createIfNotExists(privateIndex,  4, DLCMConstants.PATH_FACET, "file.path" + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 10);
    this.indexFieldAliasService.createIfNotExists(privateIndex,  5, DLCMConstants.FORMAT_FACET, "format.PRONOM" + DLCMConstants.INDEXING_KEYWORD, true, false, 1, 10);
    this.indexFieldAliasService.createIfNotExists(privateIndex,  6, DLCMConstants.INSTITUTION_FACET, DLCMConstants.AIP_INSTITUTIONS + DLCMConstants.INDEXING_KEYWORD, true, false, 1, 10);
    this.indexFieldAliasService.createIfNotExists(privateIndex, 10, DLCMConstants.METADATA_VERSION_FACET, DLCMConstants.METADATA_VERSION_FIELD + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 7);
    this.indexFieldAliasService.createIfNotExists(privateIndex, 11, DLCMConstants.DATA_TAG_FACET, DLCMConstants.AIP_DATA_TAG + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 7);
    this.indexFieldAliasService.createIfNotExists(privateIndex, 12, DLCMConstants.DATA_USE_POLICY_FACET, DLCMConstants.AIP_DATA_USE_POLICY + DLCMConstants.INDEXING_KEYWORD, true, true, 1, 5);
    this.indexFieldAliasService.createIfNotExists(privateIndex, 13, DLCMConstants.PUBLICATION_DATE_ALIAS, DLCMConstants.AIP_ISSUED_DATE, false, true, null, null);
    // @formatter:off
  }
}

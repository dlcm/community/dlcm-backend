/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Data Management - IndexResourceServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.indexing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import jakarta.xml.bind.JAXBException;

import org.mockito.Mock;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.index.settings.IndexingSettingsService;
import ch.unige.solidify.model.index.IndexProperties;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.IndexResourceService;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.SolidifyTime;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchiveTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLanguageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLicenseRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackMetadataTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPersonRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPreservationPolicyRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;

public abstract class IndexResourceServiceTest {

  private static final Path DLCM_METADATA = Paths.get("src", "test", "resources").resolve("dlcm.xml");
  private static final String METADATA_ID = "metadataId";

  protected IndexConfigProperties indexProperties;
  protected DLCMProperties dlcmProperties;
  protected DLCMRepositoryDescription repositoryDescription;
  protected IndexResourceService<ArchiveMetadata> publicMetadataService;
  protected IndexResourceService<ArchiveMetadata> privateMetadataService;

  protected MetadataService metadataService;
  protected IndexingSettingsService<IndexProperties> settingsService;

  @Mock
  protected MessageService messageService;
  @Mock
  protected GitInfoProperties gitInfoProperties;
  @Mock
  protected HistoryService historyService;
  @Mock
  protected FileFormatService fileFormatService;

  @Mock
  protected FallbackLanguageRemoteResourceService fallbackLanguageResourceService;
  @Mock
  protected FallbackLicenseRemoteResourceService fallbackLicenseRemoteResourceService;
  @Mock
  protected FallbackArchivalInfoPackageRemoteResourceService fallbackAipResourceService;
  @Mock
  protected FallbackMetadataTypeRemoteResourceService fallbackMetadataTypeResourceService;
  @Mock
  protected FallbackOrganizationalUnitRemoteResourceService fallbackOrgUnitResourceService;
  @Mock
  protected FallbackPersonRemoteResourceService fallbackPersonRemoteResourceService;
  @Mock
  protected FallbackArchivePublicMetadataRemoteResourceService fallbackArchivePublicMetadataRemoteService;
  @Mock
  protected FallbackArchivePrivateMetadataRemoteResourceService fallbackArchivePrivateMetadataRemoteService;
  @Mock
  protected FallbackSubmissionInfoPackageRemoteResourceService fallbackSubmissionInfoPackageRemoteResourceService;
  @Mock
  protected FallbackPreservationPolicyRemoteResourceService fallbackPreservationPolicyRemoteService;
  @Mock
  protected FallbackArchiveTypeRemoteResourceService fallbackArchiveTypeResourceService;

  @Mock
  protected TrustedDepositRemoteResourceService trustedDepositRemoteService;

  public void runIndexingTest() throws IOException, JAXBException {
    // Initialize indexes
    this.settingsService.init(this.dlcmProperties.getIndexing().getIndexDefinitionList());
    final List<IndexProperties> list = this.settingsService.findAll(null);
    assertEquals(2, list.size(), "Check index number");
    // Check public index
    this.testIndex(this.publicMetadataService);
    // Check private index
    this.testIndex(this.privateMetadataService);
    // Delete indexes
    for (final IndexProperties index : list) {
      assertTrue(this.settingsService.delete(index), "Delete index " + index.getResId());
    }
  }

  public void runSearchingTest() throws IOException, JAXBException {
    // Initialize indexes
    this.settingsService.init(this.dlcmProperties.getIndexing().getIndexDefinitionList());
    // Check public index
    this.testIndex(this.publicMetadataService);
    // Check private index
    this.testIndex(this.privateMetadataService);
    // Delete indexes
    for (final IndexProperties index : this.settingsService.findAll(null)) {
      assertTrue(this.settingsService.delete(index), "Delete index " + index.getResId());
    }
  }

  private void checkMetadata(ArchiveMetadata original, ArchiveMetadata metadata) {
    assertEquals(original.getResId(), metadata.getResId(), "[" + original.getIndex() + "] Check ID");
    assertEquals(original.getIndex(), metadata.getIndex(), "[" + original.getIndex() + "] Check index");
    assertEquals(original.getMetadata().size(), metadata.getMetadata().size(), "[" + original.getIndex() + "] Check metadata");
  }

  private ArchiveMetadata getMetadata(String indexName) throws IOException, JAXBException {
    final ArchiveMetadata am = new ArchiveMetadata();
    am.setResId(METADATA_ID);
    am.setIndex(indexName);
    return this.metadataService.completeArchiveIndex(DLCMMetadataVersion.getDefaultVersion(), am, DLCM_METADATA);
  }

  private void testIndex(IndexResourceService<ArchiveMetadata> indexService) throws IOException, JAXBException {
    // Test index if empty
    final List<ArchiveMetadata> metadataList = indexService.findAll(null);
    assertEquals(0, metadataList.size(), "Check '" + indexService.getIndexName() + "' index number");
    // Add metadata
    final ArchiveMetadata metadata0 = this.getMetadata(indexService.getIndexName());
    final ArchiveMetadata metadata1 = indexService.save(metadata0);
    SolidifyTime.waitOneSecond(); // wait for commit in elasticsearch
    this.checkMetadata(metadata0, metadata1);
    // Check if metadata exist
    final ArchiveMetadata metadata2 = indexService.findOne(metadata0.getResId());
    this.checkMetadata(metadata0, metadata2);
    // Delete metadata
    assertTrue(indexService.delete(metadata2), "[" + indexService.getIndexName() + "] Delete metadata");
    SolidifyTime.waitOneSecond(); // wait for commit in elasticsearch
    assertNull(indexService.findOne(metadata0.getResId()), "[" + indexService.getIndexName() + "] Check if metadata deleted");
  }

}

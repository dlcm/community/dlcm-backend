/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Data Management - ElasticsearchServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.indexing;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.index.ElasticsearchClientProvider;
import ch.unige.solidify.index.settings.elasticsearch.ElasticsearchSettingsService;
import ch.unige.solidify.model.index.IndexProperties;
import ch.unige.solidify.service.GitInfoProperties;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.elasticsearch.ArchivePrivateMetadataService;
import ch.dlcm.service.elasticsearch.ArchivePublicMetadataService;

@ActiveProfiles({ "index-elasticsearch", "sec-noauth" })
@ExtendWith(SpringExtension.class)
@Disabled("Run these tests manually if elasticseach cluster is available")
class ElasticsearchServiceTest extends IndexResourceServiceTest {
  final private static String ES_HOST = "localhost";
  final private static int ES_PORT = 9200;
  final private static String ES_USERNAME = "elastic";
  final private static String ES_PASSWORD = "change-this-password";

  @Test
  void deletingTest() {
    for (final IndexProperties index : this.settingsService.findAll(null, null)) {
      assertTrue(this.settingsService.delete(index), "Delete index " + index.getResId());
    }
  }

  @Test
  void indexingTest() throws Exception {
    this.runIndexingTest();
  }

  @Test
  void searchingTest() throws Exception {
    this.runSearchingTest();
  }

  @BeforeEach
  public void setup() {
    // DLCM Properties
    this.dlcmProperties = new DLCMProperties(new SolidifyProperties(new GitInfoProperties()), new MultipartProperties());
    // Index Properties
    this.indexProperties = new IndexConfigProperties();
    this.indexProperties.setIndexPrefix(this.dlcmProperties.getIndexing().getIndexName());
    this.indexProperties.getConfig().setHost(ES_HOST);
    this.indexProperties.getConfig().setPort(ES_PORT);
    this.indexProperties.getConfig().setUsername(ES_USERNAME);
    this.indexProperties.getConfig().setPassword(ES_PASSWORD);
    ElasticsearchClientProvider esClientProvider = new ElasticsearchClientProvider(this.indexProperties);
    this.settingsService = new ElasticsearchSettingsService(this.indexProperties, esClientProvider);
    this.publicMetadataService = new ArchivePublicMetadataService(this.indexProperties, this.dlcmProperties, esClientProvider);
    this.privateMetadataService = new ArchivePrivateMetadataService(this.indexProperties, this.dlcmProperties, esClientProvider);
    this.metadataService = new MetadataService(
            this.fallbackLanguageResourceService,
            this.fallbackOrgUnitResourceService,
            this.fallbackAipResourceService,
            this.historyService,
            this.dlcmProperties,
            this.repositoryDescription,
            this.messageService,
            this.gitInfoProperties,
            this.fileFormatService,
            this.fallbackMetadataTypeResourceService,
            this.fallbackPersonRemoteResourceService,
            this.fallbackLicenseRemoteResourceService,
            this.fallbackArchivePublicMetadataRemoteService,
            this.fallbackArchivePrivateMetadataRemoteService,
            this.fallbackPreservationPolicyRemoteService,
            this.fallbackArchiveTypeResourceService,
            this.trustedDepositRemoteService);
  }
}

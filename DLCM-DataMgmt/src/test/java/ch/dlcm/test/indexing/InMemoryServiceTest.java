/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Data Management - InMemoryServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.indexing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.test.context.ActiveProfiles;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.index.indexing.inmemory.IndexData;
import ch.unige.solidify.index.settings.inmemory.InMemorySettingsService;
import ch.unige.solidify.service.GitInfoProperties;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.inmemory.InMemoryPrivateArchiveMetadataService;
import ch.dlcm.service.inmemory.InMemoryPublicArchiveMetadataService;

@ActiveProfiles({ "index-inmemory", "sec-noauth" })
class InMemoryServiceTest extends IndexResourceServiceTest {

  @Test
  void indexingTest() throws Exception {
    this.runIndexingTest();
  }

  @Test
  void searchingTest() throws Exception {
    this.runSearchingTest();
  }

  @BeforeEach
  public void setup() {
    // DLCM Properties
    this.dlcmProperties = new DLCMProperties(new SolidifyProperties(new GitInfoProperties()), new MultipartProperties());
    // Index Properties
    this.indexProperties = new IndexConfigProperties();
    this.indexProperties.setIndexPrefix(this.dlcmProperties.getIndexing().getIndexName());
    // DLCM Repository description
    this.repositoryDescription = new DLCMRepositoryDescription(this.gitInfoProperties);
    // Services
    this.settingsService = new InMemorySettingsService(this.dlcmProperties.getIndexing().getIndexDefinitionList());
    this.publicMetadataService = new InMemoryPublicArchiveMetadataService(this.indexProperties, this.dlcmProperties,
            (InMemorySettingsService) this.settingsService, new IndexData());
    this.privateMetadataService = new InMemoryPrivateArchiveMetadataService(this.indexProperties, this.dlcmProperties,
            (InMemorySettingsService) this.settingsService, new IndexData());

    this.metadataService = new MetadataService(
            this.fallbackLanguageResourceService,
            this.fallbackOrgUnitResourceService,
            this.fallbackAipResourceService,
            this.historyService,
            this.dlcmProperties,
            this.repositoryDescription,
            this.messageService,
            this.gitInfoProperties,
            this.fileFormatService,
            this.fallbackMetadataTypeResourceService,
            this.fallbackPersonRemoteResourceService,
            this.fallbackLicenseRemoteResourceService,
            this.fallbackArchivePublicMetadataRemoteService,
            this.fallbackArchivePrivateMetadataRemoteService,
            this.fallbackPreservationPolicyRemoteService,
            this.fallbackArchiveTypeResourceService,
            this.trustedDepositRemoteService);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - SipDataFileRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.dlcm.controller.IngestController;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;

@Repository
@ConditionalOnBean(IngestController.class)
public interface SipDataFileRepository extends DataFileRepository<SipDataFile> {

  // *************
  // ** Queries **
  // *************

  SipDataFile findByResId(String id);

  List<SipDataFile> findByStatus(DataFileStatus status);

  List<SipDataFile> findByInfoPackageAndDataType(SubmissionInfoPackage sip, DataCategory dataType);

  List<SipDataFile> findByStatusIn(List<DataFileStatus> statusList);

  @Query("SELECT ddf FROM SipDataFile ddf "
          + "WHERE ddf.dataType=ch.dlcm.model.DataCategory.UpdatePackage "
          + "AND ddf.infoPackage.resId=:sipId "
          + "ORDER BY ddf.lastUpdate.when DESC")
  List<SipDataFile> findUpdatedMetadataFileOrderByDate(String sipId);

}

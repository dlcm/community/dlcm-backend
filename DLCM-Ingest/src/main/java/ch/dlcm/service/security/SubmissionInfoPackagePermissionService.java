/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - SubmissionInfoPackagePermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import static ch.dlcm.controller.DLCMControllerAction.GET_AIP;
import static ch.dlcm.controller.DLCMControllerAction.RESUME;
import static ch.dlcm.controller.DLCMControllerAction.SEND_FILES;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.dlcm.business.SubmissionInfoPackageService;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.controller.IngestController;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.security.Role;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;

@Service
@ConditionalOnBean(IngestController.class)
public class SubmissionInfoPackagePermissionService extends AbstractPermissionWithOrgUnitService {

  private final SubmissionInfoPackageService sipService;

  public SubmissionInfoPackagePermissionService(SubmissionInfoPackageService sipService,
          TrustedPersonRemoteResourceService personRemoteResourceService) {
    super(personRemoteResourceService);
    this.sipService = sipService;
  }

  @Override
  protected boolean isApproverAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    /**
     * So far, use same rights as Creator
     */
    return this.isCreatorAllowed(existingResource, action);
  }

  @Override
  protected boolean isCreatorAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    if (existingResource instanceof SubmissionInfoPackage sip) {
      final PackageStatus statusValue = sip.getInfo().getStatus();

      if ((statusValue == PackageStatus.IN_ERROR || statusValue == PackageStatus.IN_PREPARATION) && action == RESUME) {
        return true;
      }
      if (statusValue != PackageStatus.IN_ERROR && action == SEND_FILES) {
        return true;
      }
      return statusValue != PackageStatus.COMPLETED && action == DLCMControllerAction.DELETE;
    }
    return false;
  }

  @Override
  protected boolean isManagerAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    return true;
  }

  @Override
  protected boolean isRoleAllowed(Role role, OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    if (action == GET_AIP) {
      return true;
    }

    return super.isRoleAllowed(role, existingResource, action);
  }

  @Override
  protected boolean isStewardAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    /**
     * So far, use same rights as Creator
     */
    return this.isCreatorAllowed(existingResource, action);
  }

  @Override
  OrganizationalUnitAwareResource getExistingResource(String resId) {
    return this.sipService.findOne(resId);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - SipMgmt.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.SolidifyTime;

import ch.dlcm.business.SubmissionInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.IngestController;
import ch.dlcm.message.SipMessage;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.SubmissionInfoPackage;

@Service
@ConditionalOnBean(IngestController.class)
public class SipMgmt extends MessageProcessorBySize<SipMessage> {
  private static final Logger log = LoggerFactory.getLogger(SipMgmt.class);

  private final SipStatusService sipStatusService;
  private final SubmissionInfoPackageService sipService;

  public SipMgmt(SubmissionInfoPackageService sipService,
          SipStatusService sipStatusService,
          DLCMProperties dlcmProperties) {
    super(dlcmProperties);
    this.sipService = sipService;
    this.sipStatusService = sipStatusService;
  }

  @JmsListener(destination = "${dlcm.queue.sip}")
  @Override
  public void receiveMessage(SipMessage sipMessage) {
    this.sendForParallelProcessing(sipMessage);
  }

  @Override
  public void processMessage(SipMessage sipMessage) {
    this.setSecurityContext();
    log.info("Reading message {}", sipMessage);
    try {
      SubmissionInfoPackage sip = this.sipService.findOne(sipMessage.getResId());
      if (sip.getInfo().getStatus() == PackageStatus.CLEANING) {
        // clean SIP
        sip = this.sipStatusService.cleanSipStatus(sip.getResId());
      } else if (sip.getInfo().getStatus() == PackageStatus.EDITING_METADATA &&
              sip.getInfo().getContainsUpdatedMetadata()) {
        this.sendUpdatePackage(sip);
      } else {
        // Processing loop until an error or completion
        while (sip.isInProgress()) {
          // Process SIP
          sip = this.sipStatusService.processSipStatus(sip.getResId());
          SolidifyTime.waitInMilliSeconds(200);
        }
      }
      log.info("SIP '{}' ({}) (status: {}) processed", sip.getResId(), sip.getInfo().getName(), sip.getInfo().getStatus());
    } catch (NoSuchElementException e) {
      log.error("Cannot find SIP ({})", sipMessage.getResId());
    }
  }

  private void sendUpdatePackage(SubmissionInfoPackage sip) {
    try {
      log.info("AIP metadata update will be sent to Storage");
      this.sipService.sendAIPForUpdate(sip.getResId());
      sip.setPackageStatus(PackageStatus.COMPLETED);
    } catch (final Exception e) {
      sip.setPackageStatusWithMessage(PackageStatus.IN_ERROR, e.getMessage());
      log.error("Cannot sent update package to archival storage: {}", e.getMessage(), e);
    }
    this.sipService.save(sip);
  }

}

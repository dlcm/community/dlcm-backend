/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - SipDataFileListenerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.SipDataFileService;
import ch.dlcm.business.SubmissionInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.IngestController;
import ch.dlcm.message.DataFileMessage;
import ch.dlcm.message.SipMessage;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
@ConditionalOnBean(IngestController.class)
public class SipDataFileListenerService extends AbstractDataFileListenerService<SipDataFile> {
  private static final Logger log = LoggerFactory.getLogger(SipDataFileListenerService.class);

  private static final String NOTIFICATION_SIP_DATAFILE_ERROR = "ingest.notification.sip.datafile.error";

  private final String ingestLocation;

  private final SubmissionInfoPackageService sipService;

  public SipDataFileListenerService(DLCMProperties dlcmProperties,
          MessageService messageService,
          DataFileStatusService dfStatusService,
          TrustedUserRemoteResourceService userRemoteService,
          TrustedNotificationRemoteResourceService notificationRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService,
          SipDataFileService sipDataFileService,
          SubmissionInfoPackageService sipService) {
    super(dlcmProperties,
            messageService,
            dfStatusService,
            userRemoteService,
            notificationRemoteService,
            organizationalUnitRemoteService,
            sipDataFileService);
    this.ingestLocation = dlcmProperties.getIngestLocation();
    this.sipService = sipService;
  }

  @JmsListener(destination = "${dlcm.queue.sip-datafile}")
  @Override
  public void receiveDataFileMessage(DataFileMessage dataFileMessage) {
    super.receiveDataFileMessage(dataFileMessage);
  }

  @Override
  protected void processDataFile(DataFileMessage dataFileMessage) {
    log.info("Reading Deposit SIP Data File message {}", dataFileMessage);
    try {
      final SipDataFile sipDf = this
              .processDataFile(this.ingestLocation, dataFileMessage.getResId());
      if (sipDf.getStatus() == DataFileStatus.READY || sipDf.getStatus() == DataFileStatus.CLEANED) {
        boolean isBigPackage = this.sipService.getSize(sipDf.getInfoPackage().getResId()) > this.fileSizeLimit;
        SolidifyEventPublisher.getPublisher().publishEvent(new SipMessage(sipDf.getInfoPackage().getResId(), isBigPackage));
      } else if (sipDf.getStatus() == DataFileStatus.IN_ERROR) {
        this.sendDatafilePackageInErrorNotification(sipDf);
      }
    } catch (final NoSuchElementException e) {
      log.error(e.getMessage());
    }
  }

  private void sendDatafilePackageInErrorNotification(SipDataFile dataFile) {
    SubmissionInfoPackage sip = this.sipService.findOne(dataFile.getInfoPackage().getResId());
    String lastModifierId = sip.getLastUpdate().getWho();
    String orgUnitId = sip.getOrganizationalUnitId();
    String message = this.messageService.get(NOTIFICATION_SIP_DATAFILE_ERROR, new Object[] { dataFile.getResId() });
    NotificationType notificationType = NotificationType.IN_ERROR_SIP_INFO;
    this.sendDatafilePackageInErrorNotification(lastModifierId, message, notificationType, orgUnitId, dataFile.getInfoPackage().getResId());
  }

}

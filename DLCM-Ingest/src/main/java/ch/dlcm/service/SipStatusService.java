/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - SipStatusService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.io.IOException;

import jakarta.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.SipDataFileService;
import ch.dlcm.business.SubmissionInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.IngestController;
import ch.dlcm.ingestion.IngestionService;
import ch.dlcm.message.DataFileMessage;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
@ConditionalOnBean(IngestController.class)
public class SipStatusService extends DLCMService {
  private static final Logger log = LoggerFactory.getLogger(SipStatusService.class);

  private static final String NOTIFICATION_SIP_ERROR = "ingest.notification.sip.error";

  private final IngestionService ingestionService;
  private final SipDataFileService sipDataFileService;
  private final SubmissionInfoPackageService sipService;
  private final TrustedNotificationRemoteResourceService notificationRemoteService;
  private final TrustedUserRemoteResourceService userRemoteService;
  private final TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService;

  public SipStatusService(MessageService messageService,
          SubmissionInfoPackageService sipService, IngestionService ingestionService, SipDataFileService sipDataFileService,
          TrustedNotificationRemoteResourceService notificationRemoteService, TrustedUserRemoteResourceService userRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService, DLCMProperties dlcmProperties) {
    super(messageService, dlcmProperties);
    this.sipService = sipService;
    this.ingestionService = ingestionService;
    this.sipDataFileService = sipDataFileService;
    this.notificationRemoteService = notificationRemoteService;
    this.userRemoteService = userRemoteService;
    this.organizationalUnitRemoteService = organizationalUnitRemoteService;
  }

  @Transactional
  public SubmissionInfoPackage cleanSipStatus(String sipId) {
    // Load SIP
    final SubmissionInfoPackage sip = this.sipService.findOne(sipId);
    if (sip.getInfo().getStatus() == PackageStatus.CLEANING) {
      this.logPackageMessage(LogLevel.INFO, sip, "SIP datafiles status will be updated to CLEANING");
      // Trigger the archiving process for each data files: set status to CLEANING
      sip.getDataFiles()
              .stream()
              .filter(e -> e.getStatus() == DataFileStatus.READY)
              .forEach(sipDataFile -> {
                // Update status
                sipDataFile.setFileStatus(DataFileStatus.CLEANING);
                // Save it
                this.sipDataFileService.save(sipDataFile);
                // Trigger JMS message
                SolidifyEventPublisher.getPublisher().publishEvent(new DataFileMessage(sipDataFile.getClass(), sipDataFile.getResId()));
              });
      // Check if all data files are CLEANED => set SIP status to CLEANED
      // If one date file is IN_ERROR => set SIP status to IN_ERROR
      final boolean allDataFilesCleaned = sip.getDataFiles().stream()
              .allMatch(e -> e.getStatus() == DataFileStatus.CLEANED);
      final boolean anyDataFileInError = sip.getDataFiles().stream()
              .anyMatch(e -> e.getStatus() == DataFileStatus.IN_ERROR);
      if (allDataFilesCleaned) {
        try {
          this.logPackageMessage(LogLevel.INFO, sip, "Temporary metadata files will be purged");
          this.ingestionService.purgeTempMetadataFiles(sip);
          sip.setPackageStatus(PackageStatus.CLEANED);
        } catch (final IOException e) {
          this.logPackageMessage(LogLevel.ERROR, sip, "Unable to purge temporary metadata files", e);
        }
      } else if (anyDataFileInError) {
        this.logPackageMessage(LogLevel.WARN, sip, "has datafiles in error");
        sip.setPackageStatus(PackageStatus.IN_ERROR);
      }
    }
    return this.sipService.save(sip);
  }

  @Transactional
  public SubmissionInfoPackage processSipStatus(String sipId) {
    // Load SIP
    final SubmissionInfoPackage sip = this.sipService.findOne(sipId);
    try {

      this.logWillBeProcessedPackage(sip);

      switch (sip.getInfo().getStatus()) {
        case RESUBMITTING -> this.processResubmittedSip(sip);
        case IN_PREPARATION -> this.processInPrepartionSip(sip);
        case READY -> this.processReadySip(sip);
        case IN_PROGRESS -> this.processInProgressSip(sip);
        case CHECKED -> this.processCheckedSip(sip);
        case UPGRADING_METADATA -> this.processUgradingSip(sip);
        default -> this.logPackageMessage(LogLevel.INFO, sip, "has a status that does not require any processing");
      }
    } catch (final SolidifyCheckingException e) {
      this.inError(sip, this.messageService.get("ingest.sip.error.check"), e.getMessage());
      this.sendErrorNotification(sip);
    } catch (final SolidifyProcessingException e) {
      if (sip.getInfo().getStatus() != PackageStatus.CHECKED) {
        this.inError(sip, this.messageService.get("ingest.sip.error.processing"), e.getMessage());
      } else {
        this.inError(sip, this.messageService.get("ingest.sip.error.sending"), e.getMessage());
      }
      this.sendErrorNotification(sip);
    } catch (final Exception e) {
      this.logPackageMessage(LogLevel.ERROR, sip, "processing error", e);
      this.inError(sip, e.getMessage());
      this.sendErrorNotification(sip);
    }

    this.logProcessedPackage(sip);

    return this.sipService.save(sip);
  }

  private void processCheckedSip(SubmissionInfoPackage sip) {
    if (this.ingestionService.getAipPreparationStatus(sip) == PackageStatus.COMPLETED) {
      this.logPackageMessage(LogLevel.INFO, sip, "AIP file will be sent to Storage");
      if (this.ingestionService.sendAIP(sip, sip.getAipId()) != null) {
        sip.setPackageStatus(PackageStatus.COMPLETED);
      }
    }
  }

  private void processInProgressSip(SubmissionInfoPackage sip) throws IOException, JAXBException {
    this.logPackageMessage(LogLevel.INFO, sip, "AIP file will be generated");
    this.ingestionService.generateAIP(sip);
    this.logPackageMessage(LogLevel.INFO, sip, "AIP file generated");
    sip.setAip(this.ingestionService.getAIPUri(sip.getResId()));
    sip.setPackageStatus(PackageStatus.CHECKED);
  }

  private void processReadySip(SubmissionInfoPackage sip) throws IOException {
    this.logPackageMessage(LogLevel.INFO, sip, "metadata will be checked");
    this.ingestionService.checkSIP(sip);
    if (this.ingestionService.getAipPreparationStatus(sip) == PackageStatus.COMPLETED) {
      sip.setPackageStatus(PackageStatus.CHECKED);
    } else {
      // Generate AIP ID if not set
      if (StringTool.isNullOrEmpty(sip.getAipId())) {
        sip.setAipId(StringTool.generateResId());
      }
      sip.setPackageStatus(PackageStatus.IN_PROGRESS);
    }
  }

  private void processInPrepartionSip(SubmissionInfoPackage sip) {
    if (!sip.isValid()) {
      this.inError(sip, this.messageService.get("ingest.sip.error.invalid"));
    } else if (sip.isReady()) {
      sip.setPackageStatus(PackageStatus.READY);
    }
  }

  private void processResubmittedSip(SubmissionInfoPackage sip) throws IOException {
    this.ingestionService.purgeTempAip(sip);
    sip.setPackageStatus(PackageStatus.IN_PREPARATION);
  }

  private void processUgradingSip(SubmissionInfoPackage sip) {
    sip.getInfo().setMetadataVersion(DLCMMetadataVersion.getDefaultVersion());
    sip.getInfo().setStatus(PackageStatus.COMPLETED);
  }

  private void sendErrorNotification(SubmissionInfoPackage sip) {
    try {
      String message = this.messageService.get(NOTIFICATION_SIP_ERROR, new Object[] { sip.getResId() });
      User updaterUser = this.userRemoteService.findByExternalUid(sip.getLastUpdate().getWho());
      OrganizationalUnit organizationalUnit = this.organizationalUnitRemoteService.findOne(sip.getOrganizationalUnitId());
      this.notificationRemoteService.createNotification(updaterUser, message, NotificationType.IN_ERROR_SIP_INFO, organizationalUnit,
              sip.getResId());
    } catch (Exception e) {
      log.error("Cannot send notification for SIP {} in error: {}", sip.getResId(), e.getMessage());
    }
  }
}

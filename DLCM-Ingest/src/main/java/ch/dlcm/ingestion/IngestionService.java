/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - IngestionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.ingestion;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.util.FileSystemUtils;

import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.model.Package;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.DLCMService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;

public abstract class IngestionService extends DLCMService {

  private final String ingestLocation;

  protected final RemoteResourceService<OrganizationalUnit> orgUnitResourceService;
  private final MetadataService metadataService;
  private final FallbackArchivalInfoPackageRemoteResourceService aipRemoteResourceService;

  protected IngestionService(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          MetadataService metadataService,
          FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService,
          FallbackArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(messageService, dlcmProperties);
    this.ingestLocation = dlcmProperties.getIngestLocation();
    this.metadataService = metadataService;
    this.orgUnitResourceService = orgUnitResourceService;
    this.aipRemoteResourceService = aipRemoteResourceService;
  }

  // **************************
  // ** Methods to implement **
  // **************************

  // Transform SIP => AIP
  public abstract void buildAIP(SubmissionInfoPackage sip);

  // Inherited service to define AIP container type
  public abstract ArchiveContainer getAipContainer();

  // *********************
  // ** Default Methods **
  // *********************

  // Verify SIP
  public void checkSIP(SubmissionInfoPackage sip) throws IOException {
    final URI sipFile = sip.getInformationPackage().getFinalData();
    final ZipTool zipTool = new ZipTool(sipFile);
    if (zipTool.checkZip()) {
      final Path destination = this.getSIPFolder(sip.getResId());
      if (zipTool.unzipFiles(destination, Package.getMetadataAntPath(), false)) {
        // Check metadata
        final DLCMMetadataVersion metadataVersion = sip.getInfo().getMetadataVersion();
        final Path mdPath = Paths.get(destination.toString(), Package.METADATA_FILE.getName());
        this.metadataService.checkMetadataFile(metadataVersion, mdPath, new SipDataFile());
        // Purge extracted temporary metadata files
        this.purgeTempMetadataFiles(sip);
        return;
      } else {
        throw new SolidifyCheckingException(this.messageService.get("checking.zip.uncompress", new Object[] { sipFile.toString() }));
      }
    }
    throw new SolidifyCheckingException(this.messageService.get("checking.zip.notvalid", new Object[] { sipFile.toString() }));
  }

  // Generate the AIP
  public void generateAIP(SubmissionInfoPackage sip) throws IOException, JAXBException {
    this.prepareAIP(sip);
    this.buildAIP(sip);
    this.completeAIP(sip);
  }

  // Get URI of AIP
  public URI getAIPUri(String sipId) {
    return this.getSIPFolder(sipId).resolve(ResourceName.AIP).toUri();
  }

  // Get status of AIP
  public PackageStatus getAipPreparationStatus(SubmissionInfoPackage sip) {
    final Path aipFile = Paths.get(this.getAIPUri(sip.getResId()));
    if (FileTool.checkFile(aipFile)) {
      if (FileTool.isFile(aipFile)) {
        return PackageStatus.COMPLETED;
      }
      if (FileTool.isFolder(aipFile)) {
        return PackageStatus.IN_PROGRESS;
      }
    }
    return PackageStatus.READY;
  }

  // Purge temporary metadata files
  public void purgeTempMetadataFiles(SubmissionInfoPackage sip) throws IOException {
    final Path destination = this.getSIPFolder(sip.getResId());
    for (String file : Package.getMetadataStructure()) {
      // Remove starting '/'
      if (file.startsWith("/")) {
        file = file.substring(1);
      }
      final Path pathToDelete = destination.resolve(file);
      // Check if it is a file
      if (FileTool.isFile(pathToDelete)) {
        FileTool.deleteFile(pathToDelete);
      } else {
        if (FileTool.checkFile(pathToDelete)) {
          FileTool.deleteFolder(pathToDelete);
        }
      }
    }

  }

  // Send AIP to Archival Storage module
  public String sendAIP(SubmissionInfoPackage sip, String aipId) {
    return this.aipRemoteResourceService.sendAIP(sip, this.getAipContainer(), aipId);
  }

  // Purge temporary AIP files
  public void purgeTempAip(SubmissionInfoPackage sip) throws IOException {
    // AIP
    URI aip = this.getAIPUri(sip.getResId());
    if (FileTool.checkFile(aip)) {
      if (FileTool.isFile(aip)) {
        FileTool.deleteFile(aip);
      } else {
        FileTool.deleteFolder(Paths.get(aip));
      }
    }
    // Working AIP zip file
    URI aipZip = this.getZipAIPUri(sip.getResId());
    if (FileTool.checkFile(aipZip)) {
      FileTool.deleteFile(aipZip);
    }
  }

  private void completeAIP(SubmissionInfoPackage sip) throws IOException {
    final ZipTool zipPackage = new ZipTool(this.getZipAIPUri(sip.getResId()));
    if (!zipPackage.zipFiles(this.getAIPUri(sip.getResId()))) {
      throw new SolidifyProcessingException(this.messageService.get("ingest.sip.error.zip"));
    }
    if (!FileSystemUtils.deleteRecursively(Paths.get(this.getAIPUri(sip.getResId())).toFile())) {
      throw new SolidifyProcessingException(this.messageService.get("ingest.sip.error.purge"));
    }
    FileTool.renameFile(this.getZipAIPUri(sip.getResId()), this.getAIPUri(sip.getResId()), false);
  }

  // Get working directory for SIP
  private Path getSIPFolder(String sipID) {
    return Paths.get(this.ingestLocation).resolve(sipID);
  }

  private URI getZipAIPUri(String dlcmres) {
    return this.getSIPFolder(dlcmres).resolve(ResourceName.AIP + SolidifyConstants.ZIP_EXT).toUri();
  }

  // Prepare AIP
  protected void prepareAIP(SubmissionInfoPackage sip) throws JAXBException, IOException {
    // Unzip SIP package
    final ZipTool zipTool = new ZipTool(sip.getDataFiles().get(0).getFinalData());
    if (!zipTool.unzipFiles(this.getAIPUri(sip.getResId()), true)) {
      throw new SolidifyProcessingException(this.messageService.get("ingest.sip.error.processing"));
    }
    // Update metadata
    this.metadataService.updateMetadata(sip,
            Paths.get(this.getAIPUri(sip.getResId())).resolve(Package.METADATA_FILE.getName()));
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - ZipIngestionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.ingestion.zip;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.IngestController;
import ch.dlcm.ingestion.IngestionService;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;

@Profile("ing-zip")
@Service
@ConditionalOnBean(IngestController.class)
public class ZipIngestionService extends IngestionService {

  public ZipIngestionService(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          MetadataService metadataService,
          FallbackOrganizationalUnitRemoteResourceService orgUnitRemoteResourceService,
          FallbackArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(dlcmProperties, messageService, metadataService, orgUnitRemoteResourceService, aipRemoteResourceService);
  }

  @Override
  public void buildAIP(SubmissionInfoPackage sip) {
    // Do nothing because of ZIP implementation
  }

  @Override
  public ArchiveContainer getAipContainer() {
    return ArchiveContainer.ZIP;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - BagItIngestionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.ingestion.bagit;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.exception.SolidifyBagItException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.BagItTool;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.controller.IngestController;
import ch.dlcm.ingestion.IngestionService;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;

@Profile("ing-bagit")
@Service
@ConditionalOnBean(IngestController.class)
public class BagItIngestionService extends IngestionService {

  private final String repositoryName;
  private final String repositoryInstitution;
  private final String repositoryLocation;

  public BagItIngestionService(
          DLCMProperties dlcmProperties,
          DLCMRepositoryDescription repositoryDescription,
          MessageService messageService,
          MetadataService metadataService,
          FallbackOrganizationalUnitRemoteResourceService orgUnitResourceService,
          FallbackArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(dlcmProperties, messageService, metadataService, orgUnitResourceService, aipRemoteResourceService);
    this.repositoryName = repositoryDescription.getName();
    this.repositoryInstitution = repositoryDescription.getInstitution();
    this.repositoryLocation = repositoryDescription.getLocation();

  }

  @Override
  protected void prepareAIP(SubmissionInfoPackage sip) throws JAXBException, IOException {

    /*
     * Check that the bagIt "data" folder has not been created yet in the folder resulting from the SIP
     * extraction, which could be the case if the AIP build could not be finalized --> prevents data
     * duplication in the AIP Note: a "data" folder could exist in the deposit and/or SIP. In this case
     * the only drawback is that the SIP extraction could not be resumed but would be started from
     * scratch again
     */
    final Path tmpAipPath = Paths.get(this.getAIPUri(sip.getResId()));
    Path dataFolderPath = Paths.get(tmpAipPath + File.separator + DLCMConstants.DATA_FOLDER);
    if (FileTool.isFolder(dataFolderPath)) {
      this.purgeTempAip(sip);
    }

    super.prepareAIP(sip);
  }

  @Override
  public void buildAIP(SubmissionInfoPackage sip) {
    try {
      final Path outDir = Paths.get(this.getAIPUri(sip.getResId()));
      // Create BagIt
      BagItTool.create(outDir, this.supportedChecksumAlgo, true, this.getBagMetadata(sip));
    } catch (SolidifyBagItException e) {
      throw new SolidifyProcessingException(this.messageService.get("ingest.sip.error.bagit", new Object[] { e.getMessage() }), e);
    }
  }

  @Override
  public ArchiveContainer getAipContainer() {
    return ArchiveContainer.BAG_IT;
  }

  private MultiValueMap<String, String> getBagMetadata(SubmissionInfoPackage sip) {
    MultiValueMap<String, String> metadata = new LinkedMultiValueMap<>();
    metadata.add(DLCMConstants.BAGIT_SOURCE,
            this.repositoryName + ", " + this.repositoryInstitution);
    metadata.add(DLCMConstants.BAGIT_SOURCE_ADDR, this.repositoryLocation);
    metadata.add(DLCMConstants.BAGIT_METADATA_SCHEMA, sip.getInfo().getMetadataVersion().getMetsSchema());
    metadata.add(DLCMConstants.BAGIT_ARC_ID, sip.getAipId());
    metadata.add(DLCMConstants.BAGIT_ARC_NAME, sip.getInfo().getName());
    metadata.add(DLCMConstants.BAGIT_ARC_RETENTION, sip.getRetention().toString());
    metadata.add(DLCMConstants.BAGIT_ARC_RETENTION_DURATION, sip.getSmartRetention());
    metadata.add(DLCMConstants.BAGIT_ARC_DISPOSITION_APPROVAL, sip.getDispositionApproval().toString());
    metadata.add(DLCMConstants.BAGIT_ORG_UNIT_ID, sip.getInfo().getOrganizationalUnitId());
    metadata.add(DLCMConstants.BAGIT_ORG_UNIT_NAME,
            this.orgUnitResourceService.findOneWithCache(sip.getInfo().getOrganizationalUnitId()).getName());
    return metadata;
  }

}

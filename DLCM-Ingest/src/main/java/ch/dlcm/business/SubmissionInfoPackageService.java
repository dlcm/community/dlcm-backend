/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - SubmissionInfoPackageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.IngestController;
import ch.dlcm.message.SipMessage;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.repository.SubmissionInfoPackageRepository;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionPolicyRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivalInfoPackageRemoteResourceService;
import ch.dlcm.specification.SubmissionInfoPackageSpecification;

@Service
@ConditionalOnBean(IngestController.class)
public class SubmissionInfoPackageService extends InfoPackageService<SubmissionInfoPackage> {

  private static final Logger log = LoggerFactory.getLogger(SubmissionInfoPackageService.class);

  private static final String NOT_EXIST = "validation.resource.notexist";
  private static final String UNIT_CLOSED = "validation.organizationalUnit.closed";

  private final FallbackArchivalInfoPackageRemoteResourceService aipService;
  private final FallbackOrganizationalUnitRemoteResourceService organizationalUnitService;
  private final FallbackSubmissionPolicyRemoteResourceService submissionPolicyService;
  private final SipDataFileService sipDataFileService;
  private final TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService;

  public SubmissionInfoPackageService(DLCMProperties dlcmProperties, HttpRequestInfoProvider httpRequestInfoProvider,
          SpecificationPermissionsFilter<SubmissionInfoPackage> specificationPermissionFilter, HistoryService historyService,
          FallbackArchivalInfoPackageRemoteResourceService aipService, FallbackOrganizationalUnitRemoteResourceService organizationalUnitService,
          FallbackSubmissionPolicyRemoteResourceService submissionPolicyService, SipDataFileService sipDataFileService,
          TrustedArchivalInfoPackageRemoteResourceService aipRemoteResourceService) {
    super(dlcmProperties, httpRequestInfoProvider, specificationPermissionFilter, historyService);
    this.aipService = aipService;
    this.organizationalUnitService = organizationalUnitService;
    this.submissionPolicyService = submissionPolicyService;
    this.sipDataFileService = sipDataFileService;
    this.aipRemoteResourceService = aipRemoteResourceService;
  }

  @Override
  public void validateLinkedResources(SubmissionInfoPackage item, BindingResult errors) {

    /*
     * OrganizationalUnit
     */
    if (!StringTool.isNullOrEmpty(item.getInfo().getOrganizationalUnitId())) {
      final OrganizationalUnit subitem = this.organizationalUnitService.findOne(item.getInfo().getOrganizationalUnitId());
      if (subitem == null) {
        errors.addError(new FieldError(item.getClass().getSimpleName(), DLCMConstants.ORG_UNIT_ID_FIELD,
                this.messageService.get(NOT_EXIST, new Object[] { item.getInfo().getOrganizationalUnitId() })));
      } else if (!subitem.isOpen() &&
              (item.getInfo().getStatus().equals(PackageStatus.EDITING_METADATA)
                      || item.getInfo().getStatus().equals(PackageStatus.IN_PREPARATION))) {
        errors.addError(new FieldError(item.getClass().getSimpleName(), DLCMConstants.ORG_UNIT_ID_FIELD,
                this.messageService.get(UNIT_CLOSED, new Object[] { item.getInfo().getOrganizationalUnitId() })));
      }
    }
  }

  @Override
  protected Specification<SubmissionInfoPackage> addSpecificationFilter(Specification<SubmissionInfoPackage> spec) {

    return this.specificationPermissionFilter.addPermissionsFilter(spec, this.httpRequestInfoProvider.getPrincipal());
  }

  /*****************************************/

  @Override
  protected <S extends Resource> RemoteResourceService<S> getResourceService(Class<S> type) {

    if (type == OrganizationalUnit.class) {
      return (RemoteResourceService<S>) this.organizationalUnitService;
    } else if (type == ArchivalInfoPackage.class) {
      return (RemoteResourceService<S>) this.aipService;
    } else if (type == SubmissionPolicy.class) {
      return (RemoteResourceService<S>) this.submissionPolicyService;
    }
    throw new UnsupportedOperationException("Missing resource service for type " + type.getName());
  }

  public boolean putPackageInProcessingQueue(SubmissionInfoPackage sip) {
    switch (sip.getInfo().getStatus()) {
      case IN_PREPARATION, CLEANING, RESUBMITTING, EDITING_METADATA, UPGRADING_METADATA -> {
        boolean isBigPackage = this.getSize(sip.getResId()) > this.fileSizeLimit.toBytes();
        SolidifyEventPublisher.getPublisher().publishEvent(new SipMessage(sip.getResId(), isBigPackage));
        return true;
      }
      default -> {
        return false;
      }
    }
  }

  public long getSize(String sipId) {
    return ((SubmissionInfoPackageRepository) this.itemRepository).getSize(sipId);
  }

  public Result resumePackage(String sipId) {
    SubmissionInfoPackage sip = this.findOne(sipId);
    Result result = super.savePackageResumeStatus(sip);
    if (result.getStatus() == Result.ActionStatus.EXECUTED) {
      this.putPackageInProcessingQueue(sip);
    }
    return result;
  }

  public SubmissionInfoPackage deleteSipAip(String sipId) {
    try {
      SubmissionInfoPackage sip = this.findOne(sipId);
      if (sip.getAip() != null) {
        FileTool.deleteFile(sip.getAip());
        sip.setAip(null);
        sip = this.save(sip);
        log.info("AIP package purged for SIP: dlcmId={} status={}", sip.getResId(), sip.getInfo().getStatus());
      }
      return sip;
    } catch (IOException e) {
      throw new SolidifyRuntimeException(this.messageService.get("ingest.sip.error.cannotpurge", new Object[] { sipId }), e);
    }
  }

  @Transactional
  public void sendAIPForUpdate(String sipId) {
    final SubmissionInfoPackage sip = this.findOne(sipId);
    final String updatedMetadataId = this.sipDataFileService.findLastUpdatedMetadataId(sip.getResId());
    this.aipRemoteResourceService.sendAIPForUpdate(sip, updatedMetadataId);
  }

  @Override
  public SubmissionInfoPackageSpecification getSpecification(SubmissionInfoPackage sip) {
    return new SubmissionInfoPackageSpecification(sip);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - IngestDownloadTokenController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.ingest;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.model.security.DownloadToken;
import ch.unige.solidify.repository.DownloadTokenRepository;
import ch.unige.solidify.rest.ActionName;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.DLCMDownloadTokenController;
import ch.dlcm.controller.IngestController;
import ch.dlcm.model.security.DownloadTokenType;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

@RestController
@ConditionalOnBean(IngestController.class)
public class IngestDownloadTokenController extends DLCMDownloadTokenController {

  private final String ingestModulePath;

  public IngestDownloadTokenController(
          DownloadTokenRepository downloadTokenRepository,
          SolidifyProperties solidifyConfig,
          DLCMProperties dlcmProperties) throws URISyntaxException {
    super(solidifyConfig, downloadTokenRepository);
    this.ingestModulePath = new URI(dlcmProperties.getModule().getIngest().getPublicUrl()).getPath();
  }

  @GetMapping(UrlPath.INGEST_SIP + SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD_TOKEN)
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#sipId, 'DOWNLOAD_FILE')")
  public ResponseEntity<DownloadToken> getTokenForSip(@PathVariable("id") String sipId) {
    final String cookiePath = this.ingestModulePath
            + "/" + ResourceName.SIP
            + "/" + sipId
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(sipId, DownloadTokenType.SIP, cookiePath);
  }

  @GetMapping(UrlPath.INGEST_SIP + SolidifyConstants.URL_PARENT_ID + ResourceName.DATAFILE
          + SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD_TOKEN)
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'GET_FILE')")
  public ResponseEntity<DownloadToken> getTokenForSipDataFile(@PathVariable String parentid, @PathVariable String id) {
    final String cookiePath = this.ingestModulePath
            + "/" + ResourceName.SIP
            + "/" + parentid
            + "/" + ResourceName.DATAFILE
            + "/" + id
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(id, DownloadTokenType.SIP_DATAFILE, cookiePath);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - SIPController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.ingest;

import static ch.dlcm.model.PackageStatus.EDITING_METADATA;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.DataFileService;
import ch.dlcm.business.SubmissionInfoPackageService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.AbstractPackageController;
import ch.dlcm.controller.IngestController;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.DataCategoryService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.propagate.PropagateMetadataTypeRemoteResourceService;

@RestController
@ConditionalOnBean(IngestController.class)
@RequestMapping(UrlPath.INGEST_SIP)
public class SIPController extends AbstractPackageController<SipDataFile, SubmissionInfoPackage> {

  protected final String ingestTempLocation;

  public SIPController(DLCMProperties dlcmProperties, HistoryService historyService, MetadataService metadataService,
          PropagateMetadataTypeRemoteResourceService metadataTypeService, DataFileService<SipDataFile> dataFileService,
          DataCategoryService dataCategoryService) {
    super(historyService, metadataService, metadataTypeService, dataFileService, dataCategoryService);
    this.ingestTempLocation = dlcmProperties.getTempLocation(dlcmProperties.getIngestLocation());
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowedToCreate(#sip)")
  public HttpEntity<SubmissionInfoPackage> create(@RequestBody SubmissionInfoPackage sip) {
    return super.create(sip);
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#id, 'GET')")
  public HttpEntity<SubmissionInfoPackage> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<SubmissionInfoPackage>> list(@ModelAttribute SubmissionInfoPackage search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowedToUpdate(#id)")
  public HttpEntity<SubmissionInfoPackage> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#id, 'DELETE')")
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  @NoOnePermissions
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
  }

  @TrustedUserPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.CLEAN)
  public HttpEntity<Result> clean(@PathVariable String id) {
    final Result res = new Result(id);
    final SubmissionInfoPackage sip = this.itemService.findOne(id);
    if (sip.getInfo().getStatus() == PackageStatus.COMPLETED) {
      sip.setPackageStatus(PackageStatus.CLEANING);
      this.itemService.save(sip);
      res.setStatus(ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.sip.cleaned", new Object[] { id }));
    } else {
      res.setStatus(ActionStatus.NON_APPLICABLE);
      res.setMesssage(this.messageService.get("message.sip.error.cannotclean", new Object[] { id }));
    }
    if (res.getStatus() == ActionStatus.EXECUTED) {
      ((SubmissionInfoPackageService) this.itemService).putPackageInProcessingQueue(sip);
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @RootPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.RESUBMIT)
  public HttpEntity<Result> resubmit(@PathVariable String id) {
    final Result result = new Result(id);
    final SubmissionInfoPackage sip = this.itemService.findOne(id);
    if (sip.getInfo().getStatus() == PackageStatus.COMPLETED) {
      sip.setPackageStatus(PackageStatus.RESUBMITTING);
      this.itemService.save(sip);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.sip.resubmitted", new Object[] { id }));
    } else {
      result.setStatus(ActionStatus.NON_APPLICABLE);
      result.setMesssage(this.messageService.get("message.sip.error.cannotresubmit", new Object[] { id }));
    }
    if (result.getStatus() == ActionStatus.EXECUTED) {
      ((SubmissionInfoPackageService) this.itemService).putPackageInProcessingQueue(sip);
    }
    result.add(linkTo(this.getClass()).slash(id).slash(DLCMActionName.RESUBMIT).withSelfRel());
    result.add(linkTo(this.getClass()).slash(id).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @RootPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.PUT_IN_ERROR)
  public HttpEntity<Result> putInError(@PathVariable String id) {
    final SubmissionInfoPackage sip = this.itemService.findOne(id);
    final Result result = new Result(sip.getResId());
    if (sip.isCompleted()) {
      result.setMesssage(this.messageService.get("message.package.completed", new Object[] { id }));
    } else {
      sip.setPackageStatus(PackageStatus.IN_ERROR);
      this.itemService.save(sip);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.package.in_error", new Object[] { id }));
    }
    result.add(linkTo(this.getClass()).slash(id).slash(DLCMActionName.PUT_IN_ERROR).withSelfRel());
    result.add(linkTo(this.getClass()).slash(id).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @TrustedUserPermissions
  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.AIP_FILE)
  public ResponseEntity<Void> deleteAIP(@PathVariable String id) {
    // Check if the SIP exists
    final SubmissionInfoPackage sip = this.itemService.findOne(id);
    // Check if working AIP of the SIP is purged
    if (sip.getAip() == null) {
      throw new SolidifyHttpErrorException(HttpStatus.GONE, "Working AIP of SIP " + id + " is gone");
    }
    // Delete the working AIP of the SIP
    ((SubmissionInfoPackageService) this.itemService).deleteSipAip(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.AIP_FILE)
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#id, 'GET_AIP')")
  public HttpEntity<FileSystemResource> getAIP(@PathVariable String id) {
    // Check if the SIP exists
    final SubmissionInfoPackage sip = this.itemService.findOne(id);
    // Check if working AIP exists
    if (sip.getAip() == null) {
      // Check if the SIP is completed or cleaned
      if (sip.getInfo().getStatus() == PackageStatus.COMPLETED || sip.getInfo().getStatus() == PackageStatus.CLEANED) {
        // Working AIP purged
        return new ResponseEntity<>(HttpStatus.GONE);
      } else {
        // Working AIP not ready
        throw new SolidifyRuntimeException(
                this.messageService.get("ingest.sip.notready", new Object[] { id }));
      }
    }
    // Download the working AIP of the SIP
    final URI fileUri = sip.getAip();
    return this.buildDownloadResponseEntity(fileUri.getPath(), id + SolidifyConstants.ZIP_EXT, DLCMConstants.AIP_MIME_TYPE,
            FileTool.getSize(fileUri));
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD)
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#id, 'DOWNLOAD_FILE') "
          + "|| @downloadTokenPermissionService.isAllowed(#id, T(ch.dlcm.model.security.DownloadTokenType).SIP)")
  public HttpEntity<FileSystemResource> getFiles(@PathVariable String id) {
    final SubmissionInfoPackage item = this.itemService.findOne(id);
    final URI fileUri = item.getDownloadUri();
    return this.buildDownloadResponseEntity(fileUri.getPath(), id + SolidifyConstants.ZIP_EXT, DLCMConstants.SIP_MIME_TYPE,
            FileTool.getSize(fileUri));
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#id, 'HISTORY')")
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable String id, Pageable pageable) {
    return super.history(id, pageable);
  }

  @GetMapping("/" + DLCMActionName.LIST_STATUS)
  public HttpEntity<PackageStatus[]> listStatus() {
    return new ResponseEntity<>(PackageStatus.values(), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.RESUME)
  public HttpEntity<Result> resume(@PathVariable String id) {
    final Result res = ((SubmissionInfoPackageService) this.itemService).resumePackage(id);
    res.add(linkTo(this.getClass()).slash(id).slash(ActionName.RESUME).withSelfRel());
    res.add(linkTo(this.getClass()).slash(id).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping("/" + SolidifyConstants.SCHEMA)
  public HttpEntity<StreamingResponseBody> schema(@RequestParam(value = "version", required = false) String version) {
    return this.getSchemaStream(version, new SipDataFile());
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.UPLOAD)
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#id, 'SEND_FILES')")
  public HttpEntity<SipDataFile> sendFile(@PathVariable String id,
          @RequestParam(DLCMConstants.FILE) MultipartFile file) throws IOException {
    final SubmissionInfoPackage item = this.itemService.findOne(id);
    if (item.getInfo().getStatus() == PackageStatus.COMPLETED) {
      throw new SolidifyRuntimeException(
              this.messageService.get("ingest.sip.completed", new Object[] { id }));
    }

    final FileUploadDto<SipDataFile> fileUploadDto = new FileUploadDto<>(new SipDataFile(item));
    fileUploadDto.setOriginalFileName(file.getOriginalFilename());
    fileUploadDto.setInputStream(file.getInputStream());
    fileUploadDto.setDataCategory(DataCategory.Package);
    fileUploadDto.setDataType(DataCategory.InformationPackage);

    return this.upload(id, fileUploadDto, this.ingestTempLocation, UploadMode.STANDARD);
  }

  @TrustedUserPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.START_METADATA_EDITING)
  public HttpEntity<Result> putInMetadataEditing(@PathVariable String id) {
    // Purge AIP of the SIP
    ((SubmissionInfoPackageService) this.itemService).deleteSipAip(id);
    // Change SIP status
    return this.changeSipStatus(EDITING_METADATA, id);
  }

  @TrustedUserPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.START_METADATA_UPGRADE)
  public HttpEntity<Result> putInMetadataUpgrading(@PathVariable String id) {
    // Change SIP status
    return this.changeSipStatus(PackageStatus.UPGRADING_METADATA, id);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listStatus()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listVersions()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).schema(DLCMMetadataVersion.getDefaultVersion().getVersion())).withRel(SolidifyConstants.SCHEMA));
    w.add(linkTo(methodOn(this.getClass()).profile(DLCMMetadataVersion.getDefaultVersion().getVersion())).withRel(ResourceName.PROFILE));
  }

  private HttpEntity<Result> changeSipStatus(PackageStatus newStatus, String id) {

    SubmissionInfoPackage sip = this.itemService.findOne(id);
    final PackageStatus currentStatus = sip.getInfo().getStatus();
    final Result result = new Result(id);

    if ((newStatus == EDITING_METADATA && (currentStatus == PackageStatus.COMPLETED || currentStatus == PackageStatus.CLEANED))
            || (newStatus == PackageStatus.UPGRADING_METADATA
                    && currentStatus == PackageStatus.CLEANED
                    && sip.getInfo().getMetadataVersion() != DLCMMetadataVersion.getDefaultVersion())) {
      sip.getInfo().setStatus(newStatus);
      sip = this.itemService.save(sip);
      result.setStatus(Result.ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.sip.status"));
      if (newStatus == PackageStatus.UPGRADING_METADATA) {
        ((SubmissionInfoPackageService) this.itemService).putPackageInProcessingQueue(sip);
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
    } else {
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage(this.messageService.get("message.sip.status.error"));
      return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }
  }
}

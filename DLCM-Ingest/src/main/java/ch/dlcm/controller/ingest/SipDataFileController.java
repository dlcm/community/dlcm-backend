/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - SipDataFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.ingest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.lang.reflect.Method;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.RootPermissions;

import ch.dlcm.controller.AbstractDataFileController;
import ch.dlcm.controller.IngestController;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.HistoryService;

@RestController
@ConditionalOnBean(IngestController.class)
@RequestMapping(UrlPath.INGEST_SIP + SolidifyConstants.URL_PARENT_ID + ResourceName.DATAFILE)
public class SipDataFileController extends AbstractDataFileController<SubmissionInfoPackage, SipDataFile> {

  public SipDataFileController(HistoryService historyService) {
    super(historyService);
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#parentid, 'UPLOAD_FILE')")
  public HttpEntity<SipDataFile> create(@PathVariable final String parentid, final @Valid @RequestBody SipDataFile childItem) {
    return super.create(parentid, childItem);
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#parentid, 'GET_FILE')")
  public HttpEntity<SipDataFile> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#parentid, 'LIST_FILES')")
  public HttpEntity<RestCollection<SipDataFile>> list(@PathVariable String parentid, @ModelAttribute SipDataFile filterItem, Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#parentid, 'UPDATE_FILE')")
  public HttpEntity<SipDataFile> update(@PathVariable final String parentid, @PathVariable final String id,
          @RequestBody final Map<String, Object> newChildItem) {
    return super.update(parentid, id, newChildItem);
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#parentid, 'DELETE_FILE')")
  public ResponseEntity<Void> delete(@PathVariable final String parentid, @PathVariable final String id) {
    return super.delete(parentid, id);
  }

  @Override
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#parentid, 'GET_FILE')"
          + "|| @downloadTokenPermissionService.isAllowed(#id, T(ch.dlcm.model.security.DownloadTokenType).SIP_DATAFILE)")
  @EveryonePermissions
  public HttpEntity<FileSystemResource> download(@PathVariable String parentid, @PathVariable String id) {
    return super.download(parentid, id);
  }

  @GetMapping("/" + DLCMActionName.LIST_STATUS)
  @EveryonePermissions
  public HttpEntity<DataFileStatus[]> listStatus(@PathVariable String parentid) {
    return new ResponseEntity<>(DataFileStatus.values(), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.RESUME)
  @PreAuthorize("@submissionInfoPackagePermissionService.isAllowed(#parentid, 'RESUME_FILE')")
  public HttpEntity<Result> resume(@PathVariable String parentid, @PathVariable String id) {
    final Result res = this.resumeDataFile(parentid, id);
    if (res == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    try {
      final Method resumeMtd = SipDataFileController.class.getMethod(ActionName.RESUME, String.class, String.class);
      res.add(linkTo(resumeMtd, parentid, id).withSelfRel());
      final Method getMtd = SipDataFileController.class.getMethod(ActionName.READ, String.class, String.class);
      res.add(linkTo(getMtd, parentid, id).withRel(ActionName.PARENT));
    } catch (final NoSuchMethodException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.PUT_IN_ERROR)
  @RootPermissions
  public HttpEntity<Result> putInError(@PathVariable String parentid, @PathVariable String id) {
    final Result res = this.putInErrorDataFile(parentid, id);
    if (res == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    try {
      final Method getMtd = SipDataFileController.class.getMethod(ActionName.READ, String.class, String.class);
      res.add(linkTo(getMtd, parentid, id).withRel(ActionName.PARENT));
    } catch (final NoSuchMethodException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(String parentid, W w) {
    w.add(linkTo(methodOn(this.getClass()).listStatus(parentid)).withRel(ActionName.VALUES));
  }

  @Override
  protected SubmissionInfoPackage getParentResourceProperty(SipDataFile subResource) {
    return subResource.getInfoPackage();
  }

  @Override
  protected void setParentResourceProperty(SipDataFile subResource, SubmissionInfoPackage parentResource) {
    subResource.setInfoPackage(parentResource);
  }

}

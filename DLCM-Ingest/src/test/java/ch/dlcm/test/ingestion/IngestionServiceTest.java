/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - IngestionServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.ingestion;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;

import org.junit.jupiter.api.AfterEach;
import org.mockito.Mock;

import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.ingestion.IngestionService;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.DataFileChecksum.ChecksumOrigin;
import ch.dlcm.model.DataFileChecksum.ChecksumType;
import ch.dlcm.model.FileFormat;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.Tool;
import ch.dlcm.model.VirusCheck;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchiveTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLanguageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLicenseRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackMetadataTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPersonRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPreservationPolicyRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;

public abstract class IngestionServiceTest {

  private static final String ORG_UNIT_ID = DLCMConstants.DB_ORG_UNIT_ID;

  protected DLCMProperties dlcmProperties;
  protected DLCMRepositoryDescription repositoryDescription;
  protected int containerArchiveFileNumber = 0;
  protected int archiveFileNumber = 0;
  protected Path workingDir = Paths.get("src", "test", "resources", "testing");

  protected MetadataService metadataService;
  protected IngestionService ingestionService;

  @Mock
  protected GitInfoProperties gitInfoProperties;
  @Mock
  protected HistoryService historyService;
  @Mock
  protected MessageService messageService;
  @Mock
  protected FileFormatService fileFormatService;

  @Mock
  protected FallbackArchivalInfoPackageRemoteResourceService fallbackAipRemoteService;
  @Mock
  protected FallbackLanguageRemoteResourceService fallbackLanguageRemoteService;
  @Mock
  protected FallbackLicenseRemoteResourceService fallbackLicenseRemoteService;
  @Mock
  protected FallbackMetadataTypeRemoteResourceService fallbackMetadataTypeRemoteService;
  @Mock
  protected FallbackOrganizationalUnitRemoteResourceService fallbackOrgUnitRemoteService;
  @Mock
  protected FallbackPersonRemoteResourceService fallbackPersonRemoteService;
  @Mock
  protected FallbackArchivePublicMetadataRemoteResourceService fallbackArchivePublicMetadataRemoteService;
  @Mock
  protected FallbackArchivePrivateMetadataRemoteResourceService fallbackArchivePrivateMetadataRemoteService;
  @Mock
  protected FallbackSubmissionInfoPackageRemoteResourceService fallbackSipRemoteService;
  @Mock
  protected FallbackPreservationPolicyRemoteResourceService fallbackPreservationPolicyRemoteService;
  @Mock
  protected FallbackArchiveTypeRemoteResourceService fallbackArchiveTypeResourceService;

  @Mock
  protected TrustedDepositRemoteResourceService trustedDepositRemoteService;

  @AfterEach
  public void purgeData() {
    FileTool.deleteFolder(this.workingDir);
  }

  public void setUp() {
    // Mock the findOne method for Organizational Unit
    final OrganizationalUnit orgUnit = new OrganizationalUnit();
    orgUnit.setResId(ORG_UNIT_ID);
    orgUnit.setName("Organization Unit");
    when(this.fallbackOrgUnitRemoteService.findOneWithCache(ORG_UNIT_ID)).thenReturn(orgUnit);
  }

  protected void runTest(Path source, DLCMMetadataVersion version) throws Exception {
    final SubmissionInfoPackage sip = this.getSip(source, version);
    this.runSipTest(this.dlcmProperties.getIngestLocation(), sip);
  }

  protected void runTests(int containerFileNumber) {
    this.containerArchiveFileNumber = containerFileNumber;
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      try {
        this.runTest(Paths.get("src", "test", "resources", "sip").toAbsolutePath(), version);
      } catch (final Exception e) {
        this.returnException(version, e);
      }
    }
  }

  protected void returnException(DLCMMetadataVersion version, Exception e) {
    throw new SolidifyCheckingException("[" + version.toString() + "] " + e.getMessage(), e);
  }

  private void addDataFile(SubmissionInfoPackage sip, Path source) {
    final Path sipFile = Paths.get(source.toString()).resolve(sip.getResId() + "-sip" + SolidifyConstants.ZIP_EXT);
    // Get archive file number SIP
    final ZipTool zippedSip = new ZipTool(sipFile.toUri());
    this.archiveFileNumber = zippedSip.getFileNumber();
    // Create SIP data file
    final SipDataFile sipDataFile = new SipDataFile(sip);
    sipDataFile.setDataCategory(DataCategory.Package);
    sipDataFile.setDataType(DataCategory.InformationPackage);
    sipDataFile.setSourceData(sipFile.toUri());

    if (!sip.addItem(sipDataFile)) {
      throw new SolidifyRuntimeException("Cannot add data file " + sipDataFile.getFileName());
    }
  }

  private void completeSip(SubmissionInfoPackage sip, DLCMMetadataVersion version) {
    sip.setPackageStatus(PackageStatus.IN_PROGRESS);
    sip.getInfo().setMetadataVersion(version);
    sip.getInfo().setName("Faust 1072055880");
    sip.getInfo().setDescription("Testing");
    sip.getInfo().setOrganizationalUnitId(ORG_UNIT_ID);
    final SubmissionPolicy submissionPolicy = new SubmissionPolicy();
    submissionPolicy.setResId("submissionPolicyId");
    submissionPolicy.setName("test");
    submissionPolicy.setSubmissionApproval(false);
    sip.setSubmissionPolicy(submissionPolicy);
    sip.setSubmissionPolicyId(submissionPolicy.getResId());
    sip.setDispositionApproval(true);
    sip.setRetention(1);
  }

  private SubmissionInfoPackage getSip(Path source, DLCMMetadataVersion version) {
    final SubmissionInfoPackage sip = new SubmissionInfoPackage();
    sip.setResId("faust-1072055880-" + version.getVersion());
    this.completeSip(sip, version);
    this.addDataFile(sip, source);
    return sip;
  }

  private Tool getTool(String name) {
    final Tool t = new Tool();
    t.setName(name);
    t.setDescription("Testing tool");
    t.setVersion("1.0.0");
    return t;
  }

  private void processDataFiles(Path home, SubmissionInfoPackage sip) throws NoSuchAlgorithmException, IOException {
    for (final SipDataFile df : sip.getDataFiles()) {
      // Copy file
      final Path target = df.defineTargetFile(home.resolve(sip.getResId()).toString());
      FileTool.copyFile(Paths.get(df.getSourceData()), target);
      df.setFinalData(target.toUri());
      df.setFileSize(FileTool.getSize(target));
      df.setComplianceLevel(ComplianceLevel.FULL_COMPLIANCE);
      // generate checksum
      this.setChecksum(df);
      // generate tool info
      this.setFileFormat(df);
      this.setViruscheck(df);
      df.setStatus(DataFileStatus.READY);
    }
  }

  private void runSipTest(String home, SubmissionInfoPackage sip) throws IOException, JAXBException, NoSuchAlgorithmException {
    this.processDataFiles(Paths.get(home), sip);
    this.ingestionService.checkSIP(sip);
    sip.setAipId(StringTool.generateResId());
    this.ingestionService.generateAIP(sip);
    final Path aipFile = Paths.get(this.ingestionService.getAIPUri(sip.getResId()));
    assertTrue(FileTool.checkFile(aipFile), "[AIP] Cannot find file");
    final ZipTool aip = new ZipTool(aipFile.toUri());
    assertTrue(aip.checkZip(), "[AIP] Incorrect zip file");
    // assertEquals(aip.getEntryNumber(), this.containerArchiveFileNumber + this.archiveFileNumber, "[AIP] Incorrect file number");
  }

  private void setChecksum(SipDataFile df) throws NoSuchAlgorithmException, IOException {
    final String[] algoList = this.dlcmProperties.getParameters().getChecksumList();
    final String[] checksumList = ChecksumTool.computeChecksum(
            new BufferedInputStream(new FileInputStream(Paths.get(df.getFinalData()).toFile()), SolidifyConstants.BUFFER_SIZE),
            algoList);
    for (int i = 0; i < algoList.length; i++) {
      df.getChecksums()
              .add(new DataFileChecksum(ChecksumAlgorithm.valueOf(algoList[i]), ChecksumType.COMPLETE, ChecksumOrigin.DLCM, checksumList[i]));
    }
  }

  private void setFileFormat(SipDataFile df) {
    final FileFormat ff = new FileFormat();
    final String contentType = FileTool.getContentType(df.getFinalData()).toString();
    ff.setContentType(contentType);
    ff.setFormat(contentType);
    ff.setTool(this.getTool("FileFormat"));
    df.setFileFormat(ff);
  }

  private void setViruscheck(SipDataFile df) {
    final VirusCheck vc = new VirusCheck();
    vc.setCheckDate(OffsetDateTime.now());
    vc.setTool(this.getTool("VirusCheck"));
    df.setVirusCheck(vc);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - BagItIngestionServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.ingestion;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.GitInfoProperties;

import ch.dlcm.DLCMConstants;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.ingestion.bagit.BagItIngestionService;
import ch.dlcm.service.MetadataService;

@ActiveProfiles({ "ing-bagit", "sec-noauth" })
@ExtendWith(SpringExtension.class)
@Disabled
class BagItIngestionServiceTest extends IngestionServiceTest {

  @Test
  void ingestionTest() {
    // get the number of checksum algo and multiply by 2 , for every algo we have tw2 files : tagmanifest-XXX.txt, manifest-XXX.txt
    int totalFiles = (this.dlcmProperties.getParameters().getChecksumList().length * 2) + DLCMConstants.BAGIT_FIXED_FILES.size();
    this.runTests(totalFiles);
  }

  @BeforeEach
  public void setup() {
    super.setUp();
    // DLCM Properties
    this.containerArchiveFileNumber = 8;
    this.dlcmProperties = new DLCMProperties(new SolidifyProperties(new GitInfoProperties()), new MultipartProperties());
    this.dlcmProperties.setHome(this.workingDir.toAbsolutePath().toString());
    this.dlcmProperties.getParameters().setDefaultChecksum("MD5");
    this.dlcmProperties.getParameters().setChecksumList(new String[] { "MD5", "SHA1" });
    // DLCM Repository description
    this.repositoryDescription = new DLCMRepositoryDescription(this.gitInfoProperties);
    // Services
    this.metadataService = new MetadataService(
            this.fallbackLanguageRemoteService,
            this.fallbackOrgUnitRemoteService,
            this.fallbackAipRemoteService,
            this.historyService,
            this.dlcmProperties,
            this.repositoryDescription,
            this.messageService,
            this.gitInfoProperties,
            this.fileFormatService,
            this.fallbackMetadataTypeRemoteService,
            this.fallbackPersonRemoteService,
            this.fallbackLicenseRemoteService,
            this.fallbackArchivePublicMetadataRemoteService,
            this.fallbackArchivePrivateMetadataRemoteService,
            this.fallbackPreservationPolicyRemoteService,
            this.fallbackArchiveTypeResourceService,
            this.trustedDepositRemoteService);
    this.ingestionService = new BagItIngestionService(
            this.dlcmProperties,
            this.repositoryDescription,
            this.messageService,
            this.metadataService,
            this.fallbackOrgUnitRemoteService,
            this.fallbackAipRemoteService);
  }

}

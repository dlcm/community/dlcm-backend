/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Ingest - IngestPermissionEvaluatorTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test;

import static ch.dlcm.controller.DLCMControllerAction.DELETE;
import static ch.dlcm.controller.DLCMControllerAction.DOWNLOAD_FILE;
import static ch.dlcm.controller.DLCMControllerAction.GET;
import static ch.dlcm.controller.DLCMControllerAction.GET_AIP;
import static ch.dlcm.controller.DLCMControllerAction.GET_FILE;
import static ch.dlcm.controller.DLCMControllerAction.HISTORY;
import static ch.dlcm.controller.DLCMControllerAction.LIST_FILES;
import static ch.dlcm.controller.DLCMControllerAction.RESUME;
import static ch.dlcm.controller.DLCMControllerAction.SEND_FILES;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.dlcm.business.SubmissionInfoPackageService;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.service.security.SubmissionInfoPackagePermissionService;

@ExtendWith(SpringExtension.class)
class IngestPermissionEvaluatorTest extends AbstractPermissionEvaluatorTest {

  private final Map<Authentication, List<DLCMControllerAction>> mapAllowedSipActions = new LinkedHashMap<>();

  @Mock
  private SubmissionInfoPackageService sipService;

  private SubmissionInfoPackagePermissionService submissionInfoPackagePermissionService;

  @Override
  @BeforeEach
  void setUp() {
    super.setUp();

    // Set the map of allowed actions, valid with a Sip with InPreparation status
    this.mapAllowedSipActions.put(this.authManager, Arrays.asList(DLCMControllerAction.values()));
    this.mapAllowedSipActions
            .put(this.authApprover, Arrays.asList(GET_AIP, RESUME, SEND_FILES, GET, GET_FILE, LIST_FILES, DOWNLOAD_FILE, HISTORY, DELETE));
    this.mapAllowedSipActions
            .put(this.authSteward, Arrays.asList(GET_AIP, RESUME, SEND_FILES, GET, GET_FILE, LIST_FILES, DOWNLOAD_FILE, HISTORY, DELETE));
    this.mapAllowedSipActions
            .put(this.authCreator, Arrays.asList(GET_AIP, RESUME, SEND_FILES, GET, GET_FILE, LIST_FILES, DOWNLOAD_FILE, HISTORY, DELETE));
    this.mapAllowedSipActions.put(this.authVisitor, Arrays.asList(GET_AIP, GET, GET_FILE, LIST_FILES, DOWNLOAD_FILE, HISTORY));

    // Mock the findOne method on sipService
    final SubmissionInfoPackage sip = new SubmissionInfoPackage();
    sip.getInfo().setOrganizationalUnitId(ORGANIZATIONAL_UNIT_ID);
    sip.getInfo().setStatus(PackageStatus.IN_PREPARATION);
    when(this.sipService.findOne(EXISTENT_ID)).thenReturn(sip);

    // Construct the permissions service
    this.submissionInfoPackagePermissionService = new SubmissionInfoPackagePermissionService(this.sipService,
            this.trustedPersonRemoteResourceService);
  }

  @Test
  void sipAllowedActions() {
    this.allowedActionsTest(this.mapAllowedSipActions, this.submissionInfoPackagePermissionService);
  }

  @Test
  void sipForbiddenActions() {
    this.forbiddenActionsTest(this.mapAllowedSipActions, this.submissionInfoPackagePermissionService);
  }

  @Test
  void wrongAction() {
    this.setSecurityContextAuthentication(this.authManager);
    assertThrows(SolidifyRuntimeException.class, () -> this.submissionInfoPackagePermissionService.isAllowed(EXISTENT_ID, INEXISTENT_ACTION));
  }

  @Test
  void wrongResId() {
    this.setSecurityContextAuthentication(this.authManager);
    assertThrows(NoSuchElementException.class, () -> this.submissionInfoPackagePermissionService.isAllowed(INEXISTENT_ID, DELETE.toString()));

  }

}

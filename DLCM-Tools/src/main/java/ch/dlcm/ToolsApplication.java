/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - ToolsApplication.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import ch.unige.solidify.util.PropertiesLogger;

@SpringBootApplication(scanBasePackages = { "ch.dlcm", "ch.unige.solidify" })
public class ToolsApplication {
  public static void main(String[] args) {
    final SpringApplication springApplication = new SpringApplicationBuilder(ToolsApplication.class).listeners(new PropertiesLogger())
            .web(WebApplicationType.NONE).build(args);
    springApplication.run(args);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - BagitCheck.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyBagItCheckingException;
import ch.unige.solidify.util.BagItTool;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;

@Service
@Profile("bagit-check")
public class BagitCheck implements CommandLineRunner {
  private static final Logger log = LoggerFactory.getLogger(BagitCheck.class);

  @Value("${dlcm.load}")
  private String loadFolder;

  @Override
  public void run(String... arg0) {
    final String prefix = "[BagIt Check]";
    if (!this.loadFolder.isEmpty()) {
      log.info("{} Scanning folder : {}", prefix, this.loadFolder);
      int count = 0;
      // Scan deposit IDs list
      for (final Path aipFolder : FileTool.scanFolder(Paths.get(this.loadFolder), Files::isDirectory)) {
        count++;
        final Instant start = Instant.now();
        try {
          log.info("{} AIP {} check start", prefix, aipFolder);
          BagItTool.check(DLCMConstants.BAGIT_PROFILE, aipFolder.toString());
          log.info("{} AIP {} check successful", prefix, aipFolder);
        } catch (final SolidifyBagItCheckingException e) {
          log.error("{} AIP {} check failed:", prefix, aipFolder, e);
        } catch (final Exception e) {
          log.error("{} AIP {} check crashed:", prefix, aipFolder, e);
        }
        final Instant finish = Instant.now();
        log.info("{} AIP {} check duration: {}s", prefix, aipFolder, Duration.between(start, finish).getSeconds());
      }
      log.info("{} BagIt checking end {} : {} processed AIP(s)", prefix, this.loadFolder, count);
    }
  }

}

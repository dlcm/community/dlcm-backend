/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - Zip.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;

@Service
@Profile("zip")
public class Zip implements CommandLineRunner {
  private static final Logger log = LoggerFactory.getLogger(Zip.class);

  @Value("${dlcm.load}")
  private String loadFolder;

  @Override
  public void run(String... arg0) {
    final String prefix = "[Zip]";
    if (!this.loadFolder.isEmpty()) {
      log.info("{} Scanning folder : {}", prefix, this.loadFolder);
      int count = 0;
      // Scan folders
      for (final Path folder : FileTool.scanFolder(Paths.get(this.loadFolder), Files::isDirectory)) {
        count++;
        final Instant start = Instant.now();
        final Path zipFile = Paths.get(folder.toString() + SolidifyConstants.ZIP_EXT);
        try {
          final ZipTool zipPackage = new ZipTool(zipFile.toUri());
          if (!zipPackage.zipFiles(folder)) {
            log.error("{} Zipping error {}", prefix, folder);
          }
        } catch (final Exception e) {
          log.error("{} Error in zipping {}", prefix, folder, e);
        }
        final Instant finish = Instant.now();
        log.info("{} Compression duration of {}: {}s", prefix, folder, Duration.between(start, finish).getSeconds());
      }
      log.info("{} Zip process of {} : {} processed file(s)", prefix, this.loadFolder, count);
    }
  }

}

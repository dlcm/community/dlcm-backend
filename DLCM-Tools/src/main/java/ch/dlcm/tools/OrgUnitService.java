/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - OrgUnitService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.dlcm.tools.common.AIPUploader;
import ch.dlcm.tools.common.DepositUploader;
import ch.dlcm.tools.common.SIPUploader;
import ch.dlcm.tools.common.DLCMTools;

@Service
@Profile("import")
public class OrgUnitService extends DLCMTools implements CommandLineRunner {
  private static final Logger log = LoggerFactory.getLogger(OrgUnitService.class);

  @Value("${dlcm.import}")
  private String importFolder;

  public OrgUnitService(StandaloneRestClientTool restClientTool) {
    super(restClientTool);
  }

  @Override
  public void run(String... arg0) throws Exception {
    final String prefix = "[OrganizationalUnit]";
    if (!this.importFolder.isEmpty()) {
      log.info("{} Scanning folder : {}", prefix, this.importFolder);
      int count = 0;
      // Scan OrganizationalUnit level
      for (final Path organizationalUnit : FileTool.scanFolder(Paths.get(this.importFolder), Files::isDirectory)) {

        if (Files.isHidden(organizationalUnit)) {
          continue;
        }

        String orgUnitId = this.findOrganizationUnit(organizationalUnit.getFileName().toString());
        if (orgUnitId == null) {
          // Create organizationalUnit
          orgUnitId = this.createOrganizationUnit(prefix, organizationalUnit.getFileName().toString());
        } else {
          log.info("{} {} organizationalUnit already exist: {}", prefix, organizationalUnit.getFileName(), orgUnitId);
        }

        // Scan Object type
        for (final Path objType : FileTool.scanFolder(organizationalUnit, Files::isDirectory)) {

          if (objType.getFileName().toString().equalsIgnoreCase(DLCMTools.DLCM_TOOL_DEPOSIT)) {
            final DepositUploader depositUploader = new DepositUploader(this.restClientTool, this.preIngestUrl, this.licenseId);
            depositUploader.importDeposit(orgUnitId, objType);

          } else if (objType.getFileName().toString().equalsIgnoreCase(DLCMTools.DLCM_TOOL_SIP)) {
            final SIPUploader sipUploader = new SIPUploader(this.restClientTool, this.ingestUrl);
            sipUploader.importSIP(orgUnitId, objType);

          } else if (objType.getFileName().toString().equalsIgnoreCase(DLCMTools.DLCM_TOOL_AIP)) {
            final AIPUploader aipUploader = new AIPUploader(this.restClientTool, this.archivalStorageUrl);
            aipUploader.importAIP(orgUnitId, objType);
          }

        }

        count++;
      }
      log.info("{} Importing end {} : {} processed organizationalUnit(s)", prefix, this.importFolder, count);
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - DepositLocalLoad.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.tools.common.DepositUploader;
import ch.dlcm.tools.common.DLCMTools;

@Service
@Profile("local-load")
public class DepositLocalLoad extends DLCMTools implements CommandLineRunner {
  private static final Logger log = LoggerFactory.getLogger(DepositLocalLoad.class);

  @Value("${dlcm.load}")
  private String loadFolder;

  public DepositLocalLoad(StandaloneRestClientTool restClientTool) {
    super(restClientTool);
  }

  @Override
  public void run(String... arg0) throws Exception {
    final String prefix = "[Deposit Local Load]";
    if (!this.loadFolder.isEmpty()) {
      log.info("{} Scanning folder : {}", prefix, this.loadFolder);
      int count = 0;
      // Scan deposit IDs list
      for (final Path depositId : FileTool.scanFolder(Paths.get(this.loadFolder), Files::isDirectory)) {

        if (Files.isHidden(depositId)) {
          continue;
        }

        final Deposit deposit = this.findDeposit(depositId.getFileName().toString());
        if (deposit == null) {
          log.error("{} Deposit {} does not exist", prefix, depositId.getFileName());
          continue;
        }

        if (deposit.getStatus() != DepositStatus.IN_PROGRESS) {
          log.warn("{} Deposit {} not ready for loading: status={}", prefix,
                  depositId.getFileName(), deposit.getStatus());
          continue;
        }

        log.info("{} Deposit loading for {} started", prefix, depositId.getFileName());
        final DepositUploader depositUploader = new DepositUploader(this.restClientTool, this.preIngestUrl, this.licenseId);
        depositUploader.loadDepositDatafiles(prefix, deposit, depositId, true);
        log.info("{} Deposit loading for {} completed", prefix, depositId.getFileName());
      }
      count++;
      log.info("{} Deposit loading end {} : {} processed deposit(s)", prefix, this.loadFolder, count);
    }
  }

}

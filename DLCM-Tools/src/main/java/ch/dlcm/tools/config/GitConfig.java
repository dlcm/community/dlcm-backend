/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - GitConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools.config;

import jakarta.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties("dlcm.git")
@Validated
@Profile("git")
public class GitConfig {

  private String branch;

  private String commit;

  @NotEmpty
  private String dataCategory;

  @NotEmpty
  private String dataSubCategory;

  @NotEmpty
  private String organizationalUnit;

  @NotEmpty
  private String repositoryUrl;

  private String sshPassphrase;

  private String privateKeyFile;

  private String tag;

  public String getBranch() {
    return this.branch;
  }

  public String getCommit() {
    return this.commit;
  }

  public String getDataCategory() {
    return this.dataCategory;
  }

  public String getDataSubCategory() {
    return this.dataSubCategory;
  }

  public String getOrganizationalUnit() {
    return this.organizationalUnit;
  }

  public String getRepositoryUrl() {
    return this.repositoryUrl;
  }

  public String getSshPassphrase() {
    return this.sshPassphrase;
  }

  public String getPrivateKeyFile() {
    return this.privateKeyFile;
  }

  public String getTag() {
    return this.tag;
  }

  public void setBranch(String branch) {
    this.branch = branch;
  }

  public void setCommit(String commit) {
    this.commit = commit;
  }

  public void setDataCategory(String dataCategory) {
    this.dataCategory = dataCategory;
  }

  public void setDataSubCategory(String dataSubCategory) {
    this.dataSubCategory = dataSubCategory;
  }

  public void setOrganizationalUnit(String organizationalUnit) {
    this.organizationalUnit = organizationalUnit;
  }

  public void setRepositoryUrl(String repositoryUrl) {
    this.repositoryUrl = repositoryUrl;
  }

  public void setSshPassphrase(String sshPassphrase) {
    this.sshPassphrase = sshPassphrase;
  }

  public void setPrivateKeyFile(String privateKeyFile) {
    this.privateKeyFile = privateKeyFile;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }
}

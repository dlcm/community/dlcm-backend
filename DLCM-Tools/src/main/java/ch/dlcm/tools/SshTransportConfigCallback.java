/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - SshTransportConfigCallback.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools;

import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;
import org.eclipse.jgit.transport.ssh.jsch.JschConfigSessionFactory;
import org.eclipse.jgit.transport.ssh.jsch.OpenSshConfig;
import org.eclipse.jgit.util.FS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

public class SshTransportConfigCallback {
  private static final Logger log = LoggerFactory.getLogger(SshTransportConfigCallback.class);

  private SshTransportConfigCallback() {
    throw new IllegalStateException();
  }

  public static class SshWithPubKeyAndPassphrase implements TransportConfigCallback {
    private final String passphrase;
    private final String privateKeyFile;

    SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
      @Override
      protected void configure(OpenSshConfig.Host host, Session session) {
        session.setConfig("StrictHostKeyChecking", "yes");
        if (!StringTool.isNullOrEmpty(SshWithPubKeyAndPassphrase.this.privateKeyFile)) {
          try {
            JSch jsch = this.getJSch(host, FS.DETECTED);
            jsch.addIdentity(privateKeyFile);
            log.info("Successfully set identity using local file {}", privateKeyFile);
          } catch (JSchException e) {
            throw new SolidifyRuntimeException("Unable to set custom private key file to JSch", e);
          }
        }
        session.setUserInfo(new UserInfo() {
          @Override
          public String getPassphrase() {
            return SshWithPubKeyAndPassphrase.this.passphrase;
          }

          @Override
          public String getPassword() {
            return null;
          }

          @Override
          public boolean promptPassphrase(String message) {
            return true;
          }

          @Override
          public boolean promptPassword(String message) {
            return false;
          }

          @Override
          public boolean promptYesNo(String message) {
            return false;
          }

          @Override
          public void showMessage(String message) {
            // no-op
          }

        });
      }
    };

    public SshWithPubKeyAndPassphrase(String passphrase, String privateKeyFile) {
      this.passphrase = passphrase;
      this.privateKeyFile = privateKeyFile;
    }

    @Override
    public void configure(final Transport transport) {
      final SshTransport sshTransport = (SshTransport) transport;
      sshTransport.setSshSessionFactory(this.sshSessionFactory);
    }
  }
}

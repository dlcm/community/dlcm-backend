/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - Xml2Json.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;

@Service
@Profile("xml2json")
public class Xml2Json implements CommandLineRunner {
  private static final Logger log = LoggerFactory.getLogger(Xml2Json.class);

  @Value("${dlcm.load}")
  private String loadFolder;

  @Override
  public void run(String... arg0) {
    final String prefix = "[XML2JSON]";
    if (!this.loadFolder.isEmpty()) {
      log.info("{} Scanning folder : {}", prefix, this.loadFolder);
      int count = 0;
      // Scan deposit IDs list
      for (final Path file : FileTool.scanFolder(Paths.get(this.loadFolder), Files::isRegularFile)) {
        if (file.getFileName().toString().endsWith(SolidifyConstants.XML_EXT)) {
          count++;
          final Instant start = Instant.now();
          final String jsonFile = file.getParent().toString() + File.separatorChar
                  + file.getFileName().toString().replaceFirst(SolidifyConstants.XML_EXT, SolidifyConstants.JSON_EXT);

          try {
            log.info("{} XML {} validation", prefix, file);
            XMLTool.wellformed(file);
            log.info("{} XML {} conversion in JSON", prefix, file);
            JSONObject json = XMLTool.xml2Json(FileTool.toString(file));
            log.info("{} JSON {} save in {}", prefix, file, jsonFile);
            try (PrintWriter out = new PrintWriter(jsonFile)) {
              out.println(json);
            }
          } catch (final Exception e) {
            log.error("{} XML {}", prefix, file, e);
          }
          final Instant finish = Instant.now();
          log.info("{} XML {} procession duration: {}s", prefix, file, Duration.between(start, finish).getSeconds());
        }
      }
      log.info("{} Conversion process {} : {} processed XML(s)", prefix, this.loadFolder, count);
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - DepositPurge.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.tools.common.DepositPurger;
import ch.dlcm.tools.common.DLCMTools;

@Service
@Profile("purge")
public class DepositPurge extends DLCMTools implements CommandLineRunner {
  private static final Logger log = LoggerFactory.getLogger(DepositPurge.class);

  @Value("${dlcm.load}")
  private String loadFolder;

  @Value("${dlcm.relative-locations}")
  private String[] relativeLocationList;

  public DepositPurge(StandaloneRestClientTool restClientTool) {
    super(restClientTool);
  }

  @Override
  public void run(String... arg0) throws Exception {
    final String prefix = "[Deposit Purge]";
    if (!this.loadFolder.isEmpty()) {
      log.info("{} Scanning folder : {}", prefix, this.loadFolder);
      int count = 0;
      int fileCount = 0;
      // Scan deposit IDs list
      for (final Path depositId : FileTool.scanFolder(Paths.get(this.loadFolder), Files::isDirectory)) {

        if (Files.isHidden(depositId)) {
          continue;
        }

        // Check if deposit exists
        final Deposit deposit = this.findDeposit(depositId.getFileName().toString());
        if (deposit == null) {
          log.error("{} Deposit {} does not exist", prefix, depositId.getFileName());
          continue;
        }

        // Check if deposit modifiable
        if (deposit.getStatus() != DepositStatus.IN_PROGRESS) {
          log.warn("{} Deposit {} not ready for purging: status={}", prefix,
                  depositId.getFileName(), deposit.getStatus());
          continue;
        }

        // Find all folders of deposit
        final DepositPurger depositPurger = new DepositPurger(this.restClientTool, this.preIngestUrl);
        final List<String> listFolder = depositPurger.listFolders(prefix,
                depositId.getFileName().toString());

        // For each relative location to purge
        log.info("{} Deposit purge for {} started", prefix, depositId.getFileName());
        for (final String relativeLocation : this.relativeLocationList) {
          log.info("{} Deposit purging for {}: {}", prefix, depositId.getFileName(),
                  relativeLocation);
          // Find all folder starting by relative location
          final List<String> folderCandidates = listFolder.stream()
                  .filter(f -> f.startsWith(relativeLocation)).collect(Collectors.toList());
          for (final String folder : folderCandidates) {
            fileCount += depositPurger.deleteDepositDatafiles(prefix, deposit.getResId(), folder);
          }

        }
        log.info("{} Deposit purge for {} completed", prefix, depositId.getFileName());
      }
      count++;
      log.info("{} Deposit purging end {} : {} processed deposit(s), {} purged files", prefix,
              this.loadFolder, count, fileCount);
    }
  }

}

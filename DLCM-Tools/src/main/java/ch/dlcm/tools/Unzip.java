/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - Unzip.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;

@Service
@Profile("unzip")
public class Unzip implements CommandLineRunner {
  private static final Logger log = LoggerFactory.getLogger(Unzip.class);

  @Value("${dlcm.load}")
  private String loadFolder;

  @Override
  public void run(String... arg0) {
    final String prefix = "[Unzip]";
    if (!this.loadFolder.isEmpty()) {
      log.info("{} Scanning folder : {}", prefix, this.loadFolder);
      int count = 0;
      // Scan files
      for (final Path file : FileTool.scanFolder(Paths.get(this.loadFolder), Files::isRegularFile)) {
        count++;
        final Instant start = Instant.now();
        final Path unzipFolder = Paths.get(file.toString() + "_extracted");
        try {
          // Create folder for extraction
          FileTool.ensureFolderExists(unzipFolder);
          // Unzip file
          final ZipTool zipFile = new ZipTool(file.toUri());
          if (!zipFile.unzipFiles(unzipFolder, true)) {
            log.error("{} Unzipping error {}", prefix, file);
          }
        } catch (final Exception e) {
          log.error("{} Error in unzipping {}", prefix, file, e);
        }
        final Instant finish = Instant.now();
        log.info("{} Extraction duration of {}: {}s", prefix, file, Duration.between(start, finish).getSeconds());
      }
      log.info("{} Unzip process of {} : {} processed file(s)", prefix, this.loadFolder, count);
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - DownloadClient.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.dlcm.tools.common.DLCMTools;

@Service
@Profile("download")
public class DownloadClient extends DLCMTools implements CommandLineRunner {
  private static final Logger log = LoggerFactory.getLogger(DownloadClient.class);

  @Value("${dlcm.load}")
  private String loadFolder;

  @Value("${dlcm.download.url}")
  private String downloadUrl;

  public DownloadClient(StandaloneRestClientTool restClientTool) {
    super(restClientTool);
  }

  @Override
  public void run(String... arg0) {
    final String prefix = "[Download]";
    final Path destinationFile = Paths.get(this.loadFolder + "/downloaded-file");
    try {
      log.info("{} Downloading {} in {}", prefix, this.downloadUrl, this.loadFolder);
      this.downloadContentWithToken(this.downloadUrl, destinationFile);
      log.info("{} {} downloaded successfully", prefix, destinationFile);
    } catch (Exception e) {
      log.error("{} {} download failed", prefix, destinationFile, e);
    }
  }

}

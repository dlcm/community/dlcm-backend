/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - AIPUploader.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools.common;

import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.dlcm.model.oais.ArchiveContainer;

public class AIPUploader extends DLCMTools {
  private static final Logger log = LoggerFactory.getLogger(AIPUploader.class);

  public AIPUploader(StandaloneRestClientTool restClientTool, String arcStorageUrl) {
    super(restClientTool);
    this.archivalStorageUrl = arcStorageUrl;
  }

  /**
   * Import a Archival Information Package (AIP)
   *
   * @param organizationalUnitId the organizational unit id
   * @param folder The folder containing the AIP
   */
  public void importAIP(String organizationalUnitId, Path folder) {
    final String prefix = "[OrganizationalUnit=" + organizationalUnitId + " AIP] ";
    log.info("{} Scanning AIP folder: {}", prefix, folder);
    int count = 0;
    for (final Path aipType : FileTool.scanFolder(folder, Files::isDirectory)) {
      final ArchiveContainer aipContainer = this.getAipContainer(aipType.getFileName().toString());
      if (aipContainer != null) {
        for (final Path d : this.scanAll(aipType)) {
          count++;
          log.info("{} Importing {}", prefix, d);
          if (this.sendAIP(prefix, organizationalUnitId, d, aipContainer) == null) {
            log.error("{} Cannot import AIP {}", prefix, d);
          }
        }
      }
    }
    log.info("{} Importing end {} : {} uploaded AIP file(s)", prefix, folder, count);
  }

  private ArchiveContainer getAipContainer(String aipCoontainer) {
    if (aipCoontainer.equalsIgnoreCase(ArchiveContainer.ZIP.toString().replace("_", ""))) {
      return ArchiveContainer.ZIP;
    }
    if (aipCoontainer.equalsIgnoreCase(ArchiveContainer.BAG_IT.toString().replace("_", ""))) {
      return ArchiveContainer.BAG_IT;
    }
    return null;
  }

}

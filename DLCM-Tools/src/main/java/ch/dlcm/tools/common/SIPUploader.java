/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - SIPUploader.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools.common;

import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.util.StandaloneRestClientTool;

public class SIPUploader extends DLCMTools {
  private static final Logger log = LoggerFactory.getLogger(SIPUploader.class);

  public SIPUploader(StandaloneRestClientTool restClientTool, String ingestUrl) {
    super(restClientTool);
    this.ingestUrl = ingestUrl;
  }

  /**
   * Import a Submission Information Package (SIP)
   *
   * @param organizationalUnitId The organizational unit Id of the SIP
   * @param folder The folder containing the SIP
   */
  public void importSIP(String organizationalUnitId, Path folder) {
    final String prefix = "[OrganizationalUnit=" + organizationalUnitId + " SIP] ";
    log.info("{} Scanning folder: {}", prefix, folder);
    int count = 0;
    for (final Path d : this.scanAll(folder)) {
      count++;
      log.info("{} Importing {}", prefix, d);
      this.sendSIP(prefix, organizationalUnitId, d);
    }
    log.info("{} Importing end {}: {} uploaded SIP file(s)", prefix, folder, count);
  }

}

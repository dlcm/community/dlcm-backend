/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - DepositUploader.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools.common;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;

import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.dlcm.model.DataCategory;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.rest.ResourceName;

public class DepositUploader extends DLCMTools {
  private static final Logger log = LoggerFactory.getLogger(DepositUploader.class);

  public DepositUploader(StandaloneRestClientTool restClientTool, String preIngestUrl, String licenseId) {
    super(restClientTool);
    this.preIngestUrl = preIngestUrl;
    this.licenseId = licenseId;
  }

  /**
   * Check data of an existing deposit and compare on disk
   *
   * @param depositId Deposit ID
   * @param folder Load Location
   */
  public void checkDepositDatafiles(String prefix, String depositId, Path folder) {
    int error = 0;
    int fileTotal = 0;
    int dfTotal = 0;

    for (final Path dataCat : FileTool.scanFolder(folder, Files::isDirectory)) {
      for (final Path dataType : FileTool.scanFolder(dataCat, Files::isDirectory)) {
        final List<Path> folderList = this.scanAllFolders(dataType);
        for (final Path fld : folderList) {
          String relativeLocation = "";
          if (!fld.equals(dataType)) {
            relativeLocation = dataType.relativize(fld).toString();
          }
          final List<Path> fileList = FileTool.scanFolder(fld, Files::isRegularFile);
          fileTotal += fileList.size();
          final List<DepositDataFile> dataFileList = this.findDataFiles(depositId, relativeLocation);
          dfTotal += dataFileList.size();
          // Check item number by folder
          if (fileList.size() != dataFileList.size()) {
            error++;
            log.error("{} Wrong number of items in {}", prefix, relativeLocation);
          }
          for (final DepositDataFile dataFile : dataFileList) {
            // Check data category
            if (!dataFile.getDataCategory()
                    .equals(DataCategory.valueOf(dataCat.getFileName().toString()))) {
              log.warn("{} Wrong data category for {} in {}: {} <> {}", prefix,
                      dataFile.getFileName(), fld.getFileName(), dataCat,
                      dataFile.getDataCategory());
            }
            // Check data Type
            if (!dataFile.getDataType()
                    .equals(DataCategory.valueOf(dataType.getFileName().toString()))) {
              log.warn("{} Wrong data category for {} in {}: {} <> {}", prefix,
                      dataFile.getFileName(), fld.getFileName(), dataCat,
                      dataFile.getDataType());
            }
          }
        }
      }
    }
    log.info("{} Data files check for deposit {} : {}  error(s), {} file(s), {} data file(s)",
            prefix, depositId, error, fileTotal, dfTotal);
  }

  /**
   * Create a deposit
   *
   * @param organizationalUnitId Organizational Unit ID
   * @param folder Import Location
   */
  public void importDeposit(String organizationalUnitId, Path folder) {
    final String prefix = "[OrganizationalUnit=" + organizationalUnitId + " Deposit] ";
    log.info("Scanning deposit folder: {}", folder);
    for (final Path d : FileTool.scanFolder(folder, Files::isDirectory)) {
      log.info("{} Importing {}", prefix, d);
      final Deposit deposit = this.sendDeposit(prefix, organizationalUnitId, d.getFileName().toString());
      if (deposit != null) {
        this.loadDepositDatafiles(prefix, deposit, d, false);
      }
    }
  }

  /**
   * Load data for an existing deposit
   *
   * @param deposit Deposit
   * @param folder Load Location
   */
  public void loadDepositDatafiles(String prefix, Deposit deposit, Path folder, boolean local) {
    int count = 0;
    int total = 0;

    for (final Path dataCat : FileTool.scanFolder(folder, Files::isDirectory)) {
      for (final Path dataType : FileTool.scanFolder(dataCat, Files::isDirectory)) {
        // Custom Metadata
        if (dataType.getFileName().toString().equals(DataCategory.CustomMetadata.name())) {
          for (final Path metadataType : FileTool.scanFolder(dataType, Files::isDirectory)) {
            final List<Path> fileList = this.scanAll(metadataType);
            total += fileList.size();
            count += this.loadFiles(prefix, deposit, local, dataCat.getFileName().toString(), dataType.getFileName().toString(),
                    metadataType.getFileName().toString(), metadataType, fileList);
          }
        } else {
          // Other types
          final List<Path> fileList = this.scanAll(dataType);
          total += fileList.size();
          count += this.loadFiles(prefix, deposit, local, dataCat.getFileName().toString(), dataType.getFileName().toString(), null, dataType,
                  fileList);
        }
      }
    }
    log.info("{} Data files loading end {} : {} uploaded file(s) of {} data file(s)", prefix, folder, count, total);
  }

  public long loadFiles(String prefix, Deposit deposit, boolean local, String dataCategory, String dataType, String metadataType, Path parent,
          List<Path> fileList) {
    long count = 0;
    for (final Path f : fileList) {
      final String relLocation = "/" + parent.relativize(f.getParent());
      String dfId = null;
      if (local) {
        dfId = this.createLocalDepositDataFile(prefix, deposit, f.toUri(),
                dataCategory, dataType, relLocation, metadataType);
      } else {
        dfId = this.addDataFile(this.preIngestUrl + "/" + ResourceName.DEPOSIT + "/" + deposit.getResId() + "/" + ActionName.UPLOAD,
                f, dataCategory, dataType, relLocation, metadataType, new ParameterizedTypeReference<DepositDataFile>() {});
      }
      if (dfId == null) {
        log.error("{} Cannot add data file ({}) to deposit (ID={})", prefix, parent.relativize(f), deposit.getResId());
      } else {
        count++;
        log.info("{} Data file ({}) added to deposit (ID={})", prefix, parent.relativize(f), deposit.getResId());
      }
    }
    return count;
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - DepositPurger.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools.common;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.util.StandaloneRestClientTool;

import ch.dlcm.model.preingest.DepositDataFile;

public class DepositPurger extends DLCMTools {
  private static final Logger log = LoggerFactory.getLogger(DepositPurger.class);

  public DepositPurger(StandaloneRestClientTool restClientTool, String preIngestUrl) {
    super(restClientTool);
    this.preIngestUrl = preIngestUrl;
  }

  public int deleteDepositDatafiles(String prefix, String depositId, String relativeLocation) {
    int error = 0;
    int fileTotal = 0;
    int dfTotal = 0;

    if (!relativeLocation.startsWith("/")) {
      log.error("{} Relative location {} must start with a '/'", prefix, relativeLocation);
      return 0;
    }
    final List<DepositDataFile> dataFileList = this.findDataFiles(depositId,
            relativeLocation.substring(1));
    dfTotal += dataFileList.size();
    for (final DepositDataFile dataFile : dataFileList) {
      log.info("{} Purge deposit data file: {}, {}/{}", prefix, dataFile.getResId(),
              dataFile.getRelativeLocation(), dataFile.getFileName());
      if (this.deleteDataFile(depositId, dataFile.getResId())) {
        fileTotal++;
      } else {
        error++;
      }
    }

    log.info("{} Data files purge for deposit {} : {}  error(s), {} file(s), {} data file(s)",
            prefix, depositId, error, fileTotal, dfTotal);
    return fileTotal;
  }

  public List<String> listFolders(String prefix, String depositId) {
    try {
      return this.findFolders(depositId);
    } catch (final Exception e) {
      log.error("{} Error in getting folders list: {}", prefix, e.getMessage());
    }
    return new ArrayList<>();
  }

}

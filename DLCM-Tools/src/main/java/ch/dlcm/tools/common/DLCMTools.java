/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Tools - Tools.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.tools.common;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StandaloneRestClientTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

public abstract class DLCMTools {
  public static final String DLCM_TOOL_AIP = "aip";

  public static final String DLCM_TOOL_CREATION = "created by DLCM Tools";
  public static final String DLCM_TOOL_DEPOSIT = "deposit";
  public static final String DLCM_TOOL_SIP = "sip";
  private static final Logger log = LoggerFactory.getLogger(DLCMTools.class);

  @Value("${dlcm.module.admin.url}")
  protected String adminUrl;

  @Value("${dlcm.module.archival-storage.urls}[0]")
  protected String archivalStorageUrl;

  @Value("${dlcm.module.ingest.url}")
  protected String ingestUrl;

  @Value("${dlcm.license.default.id:CC-BY-4.0}")
  protected String licenseId;

  @Value("${dlcm.module.preingest.url}")
  protected String preIngestUrl;

  protected StandaloneRestClientTool restClientTool;

  protected DLCMTools(StandaloneRestClientTool restClientTool) {
    this.restClientTool = restClientTool;
  }

  /**
   * Send a Archival Information Package
   *
   * @param prefix The prefix used for logging messages
   * @param organizationalUnitId The organizational unit Id of the SIP
   * @param df The path to the data file
   * @param aipContainer AipContainer
   * @return the id of the AIP
   */
  public String sendAIP(String prefix, String organizationalUnitId, Path df,
          ArchiveContainer aipContainer) {
    try {
      final RestTemplate restClt = this.restClientTool.getClient();
      // Create AIP
      final ArchivalInfoPackage aip = new ArchivalInfoPackage();
      aip.setArchiveContainer(aipContainer);
      aip.getInfo().setName(df.getFileName().toString());
      aip.getInfo().setDescription(DLCM_TOOL_CREATION);
      aip.getInfo().setOrganizationalUnitId(organizationalUnitId);
      // Send AIP object
      final ArchivalInfoPackage resAip = restClt.postForObject(this.archivalStorageUrl + "/" + ResourceName.AIP, aip,
              ArchivalInfoPackage.class);
      if (resAip == null || resAip.getResId() == null) {
        log.error("{} Cannot create AIP for {}", prefix, df);
        return null;
      }

      log.info("{} AIP created: {} (ID={})", prefix, df.getFileName(), resAip.getResId());
      if (this.addDataFile(
              this.archivalStorageUrl + "/" + ResourceName.AIP + "/" + resAip.getResId() + "/"
                      + ActionName.UPLOAD,
              df, null, null, null, null, new ParameterizedTypeReference<AipDataFile>() {
              }) != null) {
        return resAip.getResId();
      } else {
        log.error("{} Cannot add package for AIP {}", prefix, resAip.getResId());
      }
    } catch (final Exception e) {
      log.error("{} Cannot send AIP ({}): {}", prefix, df, e.getMessage(), e);
    }
    return null;
  }

  /**
   * Send a Submission Information Package
   *
   * @param prefix The prefix used for logging messages
   * @param organizationalUnitId The organizational unit Id of the SIP
   * @param df The path to the data file
   */
  public void sendSIP(String prefix, String organizationalUnitId, Path df) {
    try {
      final RestTemplate restClt = this.restClientTool.getClient();
      final SubmissionInfoPackage sip = new SubmissionInfoPackage();
      sip.getInfo().setName(df.getFileName().toString());
      sip.getInfo().setDescription(DLCM_TOOL_CREATION);
      sip.getInfo().setOrganizationalUnitId(organizationalUnitId);
      final SubmissionInfoPackage resSip = restClt.postForObject(this.ingestUrl + "/" + ResourceName.SIP, sip,
              SubmissionInfoPackage.class);

      if (resSip == null || resSip.getResId() == null) {
        log.error("{} Cannot create SIP for {}", prefix, df);
        return;
      }

      log.info("{} SIP created: {} (ID={})", prefix, df.getFileName(), resSip.getResId());
      final String url = this.ingestUrl + "/" + ResourceName.SIP + "/" + resSip.getResId() + "/"
              + ActionName.UPLOAD;
      final ParameterizedTypeReference<SipDataFile> responseType = new ParameterizedTypeReference<>() {
      };

      if (this.addDataFile(url, df, null, null, null, null, responseType) == null) {
        log.error("{} Cannot add package for SIP {}", prefix, resSip.getResId());
      }

    } catch (final Exception e) {
      log.error("{} Cannot send SIP ({}): {}", prefix, df, e.getMessage(), e);
    }
  }

  /**
   * Upload file or Archive
   *
   * @param url url of the Rest API
   * @param df path of the file
   * @param dataCat the category of data
   * @param dataType the sub-category of data
   * @param relLocation
   * @param responseType
   * @param <T>
   * @return
   */
  protected <T extends AbstractDataFile> String addDataFile(String url, Path df, String dataCat,
          String dataType, String relLocation, String metadataType, ParameterizedTypeReference<T> responseType) {

    final LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
    map.add("file", new FileSystemResource(df.toFile()));
    if (!StringTool.isNullOrEmpty(dataCat)) {
      map.add("category", dataCat);
    }
    if (!StringTool.isNullOrEmpty(dataType)) {
      map.add("type", dataType);
    }
    if (!StringTool.isNullOrEmpty(relLocation)) {
      map.add("folder", SolidifyConstants.BASE64_PREFIX + StringTool.encode64(relLocation));
    }
    if (!StringTool.isNullOrEmpty(metadataType)) {
      map.add("metadataType", metadataType);
    }
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);
    final HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);

    T dataFile;
    try {
      final RestTemplate restClt = this.restClientTool.getClient();
      final ResponseEntity<T> responseEntity = restClt.exchange(url, HttpMethod.POST, requestEntity, responseType);
      dataFile = responseEntity.getBody();
    } catch (final Exception e) {
      log.error("Error adding data file " + url, e);
      return null;
    }
    if (dataFile != null) {
      return dataFile.getResId();
    }
    return null;
  }

  protected String createLocalDepositDataFile(String prefix, Deposit deposit, URI file, String dataCat,
          String dataType, String relLocation, String metadataType) {
    try {
      final RestTemplate restClt = this.restClientTool.getClient();
      final DepositDataFile df = new DepositDataFile(deposit);
      df.setDataCategory(DataCategory.valueOf(dataCat));
      df.setDataType(DataCategory.valueOf(dataType));
      df.setRelativeLocation(relLocation);
      if (!StringTool.isNullOrEmpty(metadataType)) {
        df.getMetadataType().setResId(metadataType);
      }
      df.setSourceData(file);
      // Send Deposit object
      final DepositDataFile resDf = restClt.postForObject(this.preIngestUrl + "/" + ResourceName.DEPOSIT + "/" + deposit.getResId() + "/" +
              ResourceName.DATAFILE, df, DepositDataFile.class);
      if (resDf == null || resDf.getResId() == null) {
        log.error("{} Cannot create deposit data file (URI={})", prefix, file);
        return null;
      }

      log.info("{} Deposit data file created: {} (ID={})", prefix, file,
              resDf.getResId());
      return resDf.getResId();

    } catch (final Exception e) {
      log.error("{} Cannot send deposit data file (name={}): {}", prefix, file,
              e.getMessage(), e);
    }
    return null;
  }

  /**
   * Create a organizational unit
   *
   * @param prefix prefix of the organizational unit name
   * @param orgUnitName the name of the organizational unit
   * @return the organizational unit Id
   */
  protected String createOrganizationUnit(String prefix, String orgUnitName) {
    try {
      final RestTemplate restClt = this.restClientTool.getClient();
      // Create OrganizationalUnit
      final OrganizationalUnit orgUnit = new OrganizationalUnit();
      orgUnit.setName(orgUnitName);
      orgUnit.setDescription(DLCM_TOOL_CREATION);
      // Send Deposit object
      final OrganizationalUnit resOrgUnit = restClt.postForObject(this.adminUrl + "/" + ResourceName.ORG_UNIT, orgUnit,
              OrganizationalUnit.class);

      if (resOrgUnit == null || resOrgUnit.getResId() == null) {
        log.error("{} Cannot create OrganizationalUnit (name={})", prefix, orgUnitName);
        return null;
      }

      log.info("{} OrganizationalUnit created: {} (ID={})", prefix, orgUnitName,
              resOrgUnit.getResId());
      return resOrgUnit.getResId();

    } catch (final Exception e) {
      log.error("{} Cannot send OrganizationalUnit (name={}): {}", prefix, orgUnitName,
              e.getMessage(), e);
    }
    return null;
  }

  /**
   * Delete data file for a deposit.
   *
   * @param depositId
   * @param depositDataFileId
   * @return true if deleted
   */
  protected boolean deleteDataFile(String depositId, String depositDataFileId) {
    final RestTemplate restClt = this.restClientTool.getClientWithPlusEncoderInterceptor();
    final String url = this.preIngestUrl + "/" + ResourceName.DEPOSIT + "/" + depositId + "/" + ResourceName.DATAFILE + "/" + depositDataFileId;
    restClt.delete(url);
    return true;
  }

  /**
   * Find data files of relative location for a deposit.
   *
   * @param depositId
   * @param relativeLocation without starting '/'
   * @return desposit data file list
   */
  protected List<DepositDataFile> findDataFiles(String depositId, String relativeLocation) {
    final RestTemplate restClt = this.restClientTool.getClientWithPlusEncoderInterceptor();
    final String url = this.preIngestUrl + "/" + ResourceName.DEPOSIT + "/" + depositId + "/" + ResourceName.DATAFILE + "?relativeLocation=/" +
            relativeLocation + "&size=" + RestCollectionPage.MAX_SIZE_PAGE;
    final String jsonResult = restClt.getForObject(url, String.class);
    return CollectionTool.getList(jsonResult, DepositDataFile.class);
  }

  /**
   * Check if deposit exist.
   *
   * @param depositId The deposit ID
   * @return Deposit if exists else null
   */
  protected Deposit findDeposit(String depositId) {
    final RestTemplate restClt = this.restClientTool.getClient();
    try {
      return restClt.getForObject(this.preIngestUrl + "/" + ResourceName.DEPOSIT + "/" + depositId, Deposit.class);
    } catch (final HttpClientErrorException e) {
      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
        return null;
      }
      throw e;
    }
  }

  /**
   * Find folder list for a deposit.
   *
   * @param depositId
   * @return folder list
   */
  protected List<String> findFolders(String depositId) {
    final RestTemplate restClt = this.restClientTool.getClientWithPlusEncoderInterceptor();
    final String url = this.preIngestUrl + "/" + ResourceName.DEPOSIT + "/" + depositId + "/" + ResourceName.DATAFILE + "/" +
            DLCMActionName.LIST_FOLDERS;
    return restClt.getForObject(url, List.class);
  }

  /**
   * Find if an organization unit exist and send back the ID.
   *
   * @param organizationalUnitName The organizational unit name
   * @return the organizational Unit Id
   */
  protected String findOrganizationUnit(String organizationalUnitName) {
    final RestTemplate restClt = this.restClientTool.getClient();
    final String jsonResult = restClt.getForObject(this.adminUrl + "/" + ResourceName.ORG_UNIT + "?name=" + organizationalUnitName,
            String.class);
    final List<OrganizationalUnit> list = CollectionTool.getList(jsonResult, OrganizationalUnit.class);
    if (list.isEmpty()) {
      return null;
    }
    for (final OrganizationalUnit unit : list) {
      if (unit.getName().equals(organizationalUnitName)) {
        return unit.getResId();
      }
    }
    return null;
  }

  protected List<Path> scanAll(Path root) {
    final List<Path> list = new ArrayList<>();
    for (final Path d : FileTool.scanFolder(root, Files::isDirectory)) {
      list.addAll(this.scanAll(d));
    }
    list.addAll(FileTool.scanFolder(root, Files::isRegularFile));
    return list;
  }

  protected List<Path> scanAllFolders(Path root) {
    final List<Path> list = new ArrayList<>();
    list.add(root);
    for (final Path d : FileTool.scanFolder(root, Files::isDirectory)) {
      list.addAll(this.scanAllFolders(d));
    }
    return list;
  }

  /**
   * Send a deposit
   *
   * @param prefix The prefix used for logging messages
   * @param organizationalUnitId The organizational unit Id of the deposit
   * @param title the title of the deposit
   * @return the id of the deposit
   */
  protected Deposit sendDeposit(String prefix, String organizationalUnitId, String title) {
    try {
      final RestTemplate restClt = this.restClientTool.getClient();
      final Deposit deposit = new Deposit();
      deposit.setTitle(title);
      deposit.setDescription("Data set " + DLCM_TOOL_CREATION + ": " + title);
      deposit.setOrganizationalUnitId(organizationalUnitId);
      deposit.setLicenseId(this.licenseId);
      // Send Deposit object
      final Deposit resDeposit = restClt.postForObject(this.preIngestUrl + "/" + ResourceName.DEPOSIT, deposit, Deposit.class);
      if (resDeposit == null || resDeposit.getResId() == null) {
        log.error("{} Cannot create deposit (name={})", prefix, title);
        return null;
      }

      log.info("{} Deposit created: {} (ID={})", prefix, title, resDeposit.getResId());
      return resDeposit;

    } catch (final Exception e) {
      log.error("{} Cannot send deposit (name={}): {}", prefix, title, e.getMessage(), e);
    }
    return null;
  }

  protected void downloadContentWithToken(String url, Path path) {
    this.restClientTool.downloadContent(URI.create(url), path, TokenUsage.WITH_TOKEN);
  }
}

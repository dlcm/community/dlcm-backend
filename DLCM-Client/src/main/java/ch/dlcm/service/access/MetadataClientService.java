/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - MetadataClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.access;

import java.net.URI;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.model.Citation;
import ch.unige.solidify.model.dto.CitationDto;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.FacetResult;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.service.NoSqlResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.access.OrderSubsetItem;
import ch.dlcm.model.dto.DisseminationPolicyDto;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class MetadataClientService extends NoSqlResourceClientService<ArchiveMetadata> {

  public static final String ALL_METADATA = "/" + ResourceName.ALL_METADATA;
  public static final String PRIVATE_METADATA = "/" + ResourceName.PRIVATE_METADATA;
  public static final String PUBLIC_METADATA = "/" + ResourceName.PUBLIC_METADATA;
  public static final String SEARCH = "/" + ActionName.SEARCH;

  private final RestTemplate restTemplate;

  public MetadataClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ACCESS,
            "",
            ArchiveMetadata.class,
            restClientTool,
            env);
    this.restTemplate = this.restClientTool.getClient(this.getBaseUrl());
  }

  public RestCollection<ArchiveMetadata> getData(String aipId) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String jsonResult = this.getResources(PUBLIC_METADATA + "/" + aipId + "/" + ResourceName.DATAFILE, pageable);
    return this.getJson(jsonResult);
  }

  public List<String> listFolders(String aipId) {
    try {
      final String jsonResult = this.restTemplate.getForObject(PUBLIC_METADATA + "/" + aipId + "/" + DLCMActionName.LIST_FOLDERS, String.class);
      ObjectMapper mapper = new ObjectMapper();
      return Collections.singletonList(mapper.readValue(jsonResult, String.class));
    } catch (JsonProcessingException e) {
      return null;
    }
  }

  public void downloadArchive(String dipId, Path path) {
    final String url = PUBLIC_METADATA + "/" + dipId + "/" + ActionName.DOWNLOAD;
    this.downloadWithToken(url, path);
  }

  public void downloadArchive(String dipId, Path path, String orderId) {
    final String url = PUBLIC_METADATA + "/" + dipId + "/" + ActionName.DOWNLOAD + "?orderId="+orderId;
    this.downloadWithToken(url, path);
  }

  public void downloadArchiveWithoutSpecifyingFileName(String dipId, Path path, String orderId) {
    final String url = PUBLIC_METADATA + "/" + dipId + "/" + ActionName.DOWNLOAD + "?orderId="+orderId;
    this.downloadContentToDirectory(url, path);
  }

  public void downloadThumbnail(String aipId, Path path) {
    final String url = this.getBaseUrl() + PUBLIC_METADATA + "/" + aipId + "/" + DLCMActionName.THUMBNAIL;
    this.restClientTool.downloadContent(URI.create(url), path, TokenUsage.WITHOUT_TOKEN);
  }

  public void downloadDua(String aipId, Path path) {
    final String url = this.getBaseUrl() + PUBLIC_METADATA + "/" + aipId + "/" + DLCMActionName.DUA;
    this.restClientTool.downloadContent(URI.create(url), path, TokenUsage.WITHOUT_TOKEN);
  }

  public void downloadReadme(String aipId, Path path) {
    final String url = this.getBaseUrl() + PUBLIC_METADATA + "/" + aipId + "/" + DLCMActionName.README;
    this.restClientTool.downloadContent(URI.create(url), path, TokenUsage.WITHOUT_TOKEN);
  }

  public OrderStatus getDownloadStatus(String orderId) {
    final String url = this.getBaseUrl() + PUBLIC_METADATA + "/" + orderId + "/" + DLCMActionName.DOWNLOAD_STATUS;
    return this.restTemplate.exchange(url, HttpMethod.GET, null, OrderStatus.class, orderId).getBody();
  }

  public RestCollection<ArchiveMetadata> listAllPrivate() {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String jsonResult = this.getResources(PRIVATE_METADATA + "/" + ActionName.SEARCH, pageable);
    return this.getJson(jsonResult);
  }

  public RestCollection<ArchiveMetadata> listAllPrivateWithoutSecurity() {

    final String url = this.getBaseUrl() + PRIVATE_METADATA;
    final String jsonResult = this.restTemplate.exchange(url, HttpMethod.GET, null, String.class).getBody();
    return this.getJson(jsonResult);
  }

  public RestCollection<ArchiveMetadata> listAllPublic() {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String jsonResult = this.getResources(PUBLIC_METADATA, pageable);
    return this.getJson(jsonResult);
  }

  public RestCollection<ArchiveMetadata> listAllPublicWithoutSecurity() {
    final String url = this.getBaseUrl() + PUBLIC_METADATA + "?size=" + RestCollectionPage.MAX_SIZE_PAGE;
    final String jsonResult = this.restTemplate.exchange(url, HttpMethod.GET, null, String.class).getBody();
    return this.getJson(jsonResult);
  }

  public RestCollection<ArchiveMetadata> listPublicAndPrivateMetadata() {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String jsonResult = this.getResources(ALL_METADATA + "/" + ActionName.SEARCH, pageable);
    return this.getJson(jsonResult);
  }

  public RestCollection<ArchiveMetadata> listPublicAndPrivateMetadataWithoutSecurity() {
    final String url = this.getBaseUrl() + ALL_METADATA;
    final String jsonResult = this.restTemplate.exchange(url, HttpMethod.GET, null, String.class).getBody();
    return this.getJson(jsonResult);
  }

  public ArchiveMetadata findOne(String resId) {
    final String url = this.getBaseUrl() + PUBLIC_METADATA + "/" + resId;
    return this.getObjectFromUrl(url);
  }

  public ArchiveMetadata searchByDoi(String doi) {
    final String url = this.getBaseUrl() + PUBLIC_METADATA + "/" + DLCMActionName.SEARCH_DOI + "?doi=" + doi;
    return this.getObjectFromUrl(url);
  }

  public RestCollection<ArchiveMetadata> searchPublicIndex(String query) {
    return this.searchIndex(query, PUBLIC_METADATA);
  }

  public RestCollection<ArchiveMetadata> searchPrivateIndex(String query) {
    return this.searchIndex(query, PRIVATE_METADATA);
  }

  public RestCollection<ArchiveMetadata> searchAllIndex(String query) {
    return this.searchIndex(query, ALL_METADATA);
  }

  public RestCollection<ArchiveMetadata> searchIndex(String query, String metadataUrl) {
    final String url = this.getBaseUrl() + metadataUrl + SEARCH + "?query=" + query;
    final String jsonResult = this.restTemplate.exchange(url, HttpMethod.GET, null, String.class, new Object[0]).getBody();
    return this.getJson(jsonResult);
  }

  public String prepareDownload(String resId, List<OrderSubsetItem> subsetItemList, String disseminationPolicyId) throws JsonProcessingException {
    final String url = this.getBaseUrl() + PUBLIC_METADATA + "/" + resId + "/" + DLCMActionName.PREPARE_DOWNLOAD;
    DisseminationPolicyDto disseminationPolicyDto = new DisseminationPolicyDto();
    disseminationPolicyDto.setDisseminationPolicyId(disseminationPolicyId);

    ObjectMapper mapper = new ObjectMapper();
    if (subsetItemList == null) {
      return this.restTemplate.postForObject(url, disseminationPolicyDto, String.class, resId);
    }
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    SimpleFilterProvider filterProvider = new SimpleFilterProvider();
    filterProvider.addFilter("userPropertyFilter", SimpleBeanPropertyFilter.serializeAllExcept("creation"));
    mapper.setFilterProvider(filterProvider);
    disseminationPolicyDto.setSubsetItemList(subsetItemList);
    String requestBody = mapper.writeValueAsString(disseminationPolicyDto);
    HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);
    return this.restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class).getBody();
  }

  public String prepareDownload(String resId, String disseminationPolicyId, String orgUnitDisseminationPolicyId) {
    final String url = this.getBaseUrl() + PUBLIC_METADATA + "/" + resId + "/" + DLCMActionName.PREPARE_DOWNLOAD;
    DisseminationPolicyDto disseminationPolicyDto = new DisseminationPolicyDto();
    disseminationPolicyDto.setDisseminationPolicyId(disseminationPolicyId);
    if (orgUnitDisseminationPolicyId != null) {
      disseminationPolicyDto.setOrganizationalUnitDisseminationPolicyId(orgUnitDisseminationPolicyId);
    }
    return this.restTemplate.postForObject(url, disseminationPolicyDto, String.class, resId);
  }

  public String prepareDownload(String resId, String disseminationPolicyId) {
    return this.prepareDownload(resId, disseminationPolicyId, null);
  }

  public Map<String, List<String>> getAccessSystemProperties() {
    final String url = this.getBaseUrl() + "/" + DLCMActionName.SYSTEM_PROPERTIES;
    return this.restTemplate.getForObject(url, Map.class);
  }

  public Map<String, List<String>> getCitationsConfig() {
    return (Map<String, List<String>>) this.getAccessSystemProperties().get("citations");
  }

  public Map<String, List<String>> getBibliographiesConfig() {
    return (Map<String, List<String>>) this.getAccessSystemProperties().get("bibliographies");
  }

  public List<CitationDto> getBibliographies(String resId) {
    return this.getCitationDtoList(
            this.getBaseUrl() + PUBLIC_METADATA + "/" + resId + "/" + DLCMActionName.BIBLIOGRAPHIES);
  }

  public List<CitationDto> getCitations(String resId) {
    return this.getCitationDtoList(
            this.getBaseUrl() + PUBLIC_METADATA + "/" + resId + "/" + DLCMActionName.CITATIONS);
  }

  private List<CitationDto> getCitationDtoList(String url) {
    Map<String, Object>[] citationDtoMaps = this.restTemplate.getForObject(url, Map[].class);
    List<CitationDto> citationDtos = new ArrayList<>();
    for (Map<String, Object> citationMap : citationDtoMaps) {
      Citation citation = new Citation();
      citation.setStyle(citationMap.get("style").toString());
      citation.setLanguage(citationMap.get("language").toString());
      Citation.OutputFormat outputFormat = Citation.OutputFormat.valueOf(citationMap.get("outputFormat").toString().toUpperCase());
      citation.setOutputFormat(outputFormat);
      citation.setText(citationMap.get("text").toString());
      citationDtos.add(new CitationDto(citation));
    }
    return citationDtos;
  }

  private RestCollection<ArchiveMetadata> getJson(String jsonResult) {
    final List<ArchiveMetadata> list = CollectionTool.getList(jsonResult, ArchiveMetadata.class);
    final RestCollectionPage page = CollectionTool.getPage(jsonResult);
    final List<FacetResult> facets = CollectionTool.getFacetResults(jsonResult);
    return new RestCollection<>(list, page, facets);
  }

}

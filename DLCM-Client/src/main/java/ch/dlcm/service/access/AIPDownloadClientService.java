/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - AIPDownloadClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.access;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class AIPDownloadClientService extends ResourceClientService<ArchivalInfoPackage> {

  public AIPDownloadClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ACCESS,
            ResourceName.AIP,
            ArchivalInfoPackage.class,
            restClientTool, env);
  }

  @Override
  public ArchivalInfoPackage create(ArchivalInfoPackage resource) {
    return null;
  }

  @Override
  public void delete(String resId) {
  }

  @Override
  public List<ArchivalInfoPackage> findAll() {
    return this.findAll(ArchivalInfoPackage.class);
  }

  @Override
  public ArchivalInfoPackage findOne(String resId) {
    return null;
  }

  @Override
  public List<ArchivalInfoPackage> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, ArchivalInfoPackage.class);
  }

  @Override
  public ArchivalInfoPackage update(String resId, ArchivalInfoPackage resource) {
    return null;
  }
}

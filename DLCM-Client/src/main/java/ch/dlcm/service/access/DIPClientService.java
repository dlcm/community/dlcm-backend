/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - DIPClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.access;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class DIPClientService extends ResourceClientService<DisseminationInfoPackage> {

  public DIPClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ACCESS,
            ResourceName.DIP,
            DisseminationInfoPackage.class,
            restClientTool,
            env);
  }

  public void downloadArchive(String resId, Path path) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + ActionName.DOWNLOAD;
    this.download(url, path);
  }

  @Override
  public List<DisseminationInfoPackage> findAll() {
    return this.findAll(DisseminationInfoPackage.class);
  }

  @Override
  public List<DisseminationInfoPackage> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, DisseminationInfoPackage.class);
  }

}

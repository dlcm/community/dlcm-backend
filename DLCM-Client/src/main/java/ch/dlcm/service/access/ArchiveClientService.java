/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - ArchiveClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.access;

import java.util.Collections;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.NoSqlResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class ArchiveClientService extends NoSqlResourceClientService<ArchiveMetadata> {

  public ArchiveClientService(SudoRestClientTool restClientTool, Environment env) {
    super(
            SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ACCESS,
            ResourceName.ARCHIVE,
            ArchiveMetadata.class,
            restClientTool,
            env);
  }

  public ModelAndView redirectByDOI(String doiId) {
    return this.redirect("doi:" + doiId);
  }

  public ModelAndView redirectByARK(String arkId) {
    return this.redirect(arkId);
  }

  public ModelAndView redirectByResId(String resId) {
    return this.redirect(resId);
  }

  public ArchiveMetadata getMetadataByDOI(String doiId) {
    return this.findOne("doi:" + doiId);
  }

  public ArchiveMetadata getMetadataByARK(String arkId) {
    return this.findOne(arkId);
  }

  public ArchiveMetadata getMetadataByResId(String resId) {
    return this.findOne(resId);
  }

  private ModelAndView redirect(String id) {
    final String url = this.getResourceUrl() + "/" + id;
    return this.getObjectFromUrl(url, Collections.emptyMap(), ModelAndView.class);
  }

  private ArchiveMetadata findOne(String id) {
    final String url = this.getResourceUrl() + "/" + ResourceName.PUBLIC_METADATA + "/" + id;
    return this.getObjectFromUrl(url);
  }

}

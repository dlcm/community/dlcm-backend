/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - OrgUnitClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.access;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.NoSqlResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.access.OrgUnit;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class OrgUnitClientService extends NoSqlResourceClientService<OrgUnit> {

  public OrgUnitClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ACCESS,
            ResourceName.ORG_UNIT,
            OrgUnit.class,
            restClientTool,
            env);
  }

  public RestCollection<OrgUnit> list() {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String jsonResult = this.getResources(this.getResourceUrl(), pageable);
    final List<OrgUnit> list = CollectionTool.getList(jsonResult, OrgUnit.class);
    final RestCollectionPage page = CollectionTool.getPage(jsonResult);
    return new RestCollection<>(list, page);
  }

  public RestCollection<ArchiveMetadata> listArchivesByOrgUnit(String id) {
    final String url = this.getResourceUrl() + SolidifyConstants.URL_PARENT_ID + ResourceName.ARCHIVE;
    final Map<String, String> params = new HashMap<>();
    params.put("parentid", id);

    final String jsonResult = this.getResources(url, params);

    final List<ArchiveMetadata> list = CollectionTool.getList(jsonResult, ArchiveMetadata.class);
    final RestCollectionPage page = CollectionTool.getPage(jsonResult);
    return new RestCollection<>(list, page);
  }

}

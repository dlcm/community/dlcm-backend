/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - OrderClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.access;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.access.Order;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class OrderClientService extends ResourceClientService<Order> {

  public OrderClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ACCESS,
            ResourceName.ORDER,
            Order.class,
            restClientTool,
            env);
  }

  @Override
  public List<Order> findAll() {
    return this.findAll(Order.class);
  }

  public List<Order> findAllCreatedByUser() {
    final String url = this.getResourceUrl() + "/" + DLCMActionName.ORDER_LIST_CREATED_BY_USER + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Order.class);
  }

  public List<Order> findAllPublic(Map<String, String> properties) {
    StringBuilder urlBuilder = new StringBuilder();
    urlBuilder.append(this.getResourceUrl() + "/" + DLCMActionName.ORDER_LIST_PUBLIC).append(SIZE_PARAM)
            .append(RestCollectionPage.MAX_SIZE_PAGE);
    for (final Map.Entry<String, String> entry : properties.entrySet()) {
      urlBuilder.append("&")
              .append(entry.getKey())
              .append("=")
              .append(entry.getValue());
    }

    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(urlBuilder.toString(), String.class);
    return CollectionTool.getList(jsonString, Order.class);
  }

  public Order save(String resId) {
    return this.doActionOnId(resId, ActionName.SAVE);
  }

  @Override
  public List<Order> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Order.class);
  }

  public Result setSubmitStatus(String resId) {
    return this.postAction(resId, DLCMActionName.SUBMIT);
  }

  public List<DisseminationInfoPackage> getDIP(String orderId) {
    return this.findAllLinkedResources(orderId, ResourceName.DIP, DisseminationInfoPackage.class);
  }

  public List<ArchivalInfoPackage> getAIP(String orderId) {
    return this.findAllLinkedResources(orderId, ResourceName.AIP, ArchivalInfoPackage.class);
  }

}

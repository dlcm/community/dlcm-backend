/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - DepositClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.preingest;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class DepositClientService extends ResourceClientService<Deposit> {

  public DepositClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.PREINGEST,
            ResourceName.DEPOSIT,
            Deposit.class,
            restClientTool,
            env);
  }

  public void addAip(String depositId, String aipId) {
    this.addItem(depositId, ResourceName.AIP, aipId, ArchivalInfoPackage.class);
  }

  public void addContributor(String depositId, String contributorId) {
    this.addItem(depositId, ResourceName.CONTRIBUTOR, contributorId, Person.class);
  }

  public void addDepositDataFile(String depositId, DepositDataFile df) {
    this.addItem(depositId, ResourceName.DATAFILE, df, DepositDataFile.class);
  }

  public Result submit(String depositId) {
    return this.postAction(depositId, DLCMActionName.SUBMIT_FOR_APPROVAL);
  }

  public Result approve(String depositId) {
    return this.postAction(depositId, DLCMActionName.APPROVE);
  }

  public Result reject(String depositId, String reason) {
    String url = this.getResourceUrl() + "/" + depositId + "/" + DLCMActionName.REJECT + "?reason=" + reason;
    RestTemplate restTemplate = this.restClientTool.getClient(url);
    return restTemplate.postForObject(url, null, Result.class);
  }

  public Deposit createFromJson(String depositJson) {

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    final HttpEntity<String> request = new HttpEntity<>(depositJson, headers);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(this.getResourceUrl(), request, Deposit.class);
  }

  public void downloadDataFile(String depositId, String dataFileId, Path path) {
    final String url = this.getResourceUrl()
            + "/" + depositId
            + "/" + ResourceName.DATAFILE
            + "/" + dataFileId
            + "/" + ActionName.DOWNLOAD;
    this.download(url, path);
  }

  public void downloadDeposit(String depositId, String folderName, Path path) {
    String url = this.getResourceUrl()
            + "/" + depositId
            + "/" + ActionName.DOWNLOAD;
    if (!StringTool.isNullOrEmpty(folderName)) {
      url += "?folder=" + folderName;
    }
    this.download(url, path);
  }

  @Override
  public List<Deposit> findAll() {
    return this.findAll(Deposit.class);
  }

  public List<Deposit> findAllWithOrgunitId(String orgUnitId) {
    final String url = this.getResourceUrl() + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE + "&organizationalUnitId=" + orgUnitId;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Deposit.class);
  }

  public List<Deposit> findAllWithLightVersion(boolean lightVersion) {
    final String url = this.getResourceUrl() + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE + "&lightVersion=" + lightVersion;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Deposit.class);
  }

  public List<Person> getContributors(String depositId) {
    return this.findAllLinkedResources(depositId, ResourceName.CONTRIBUTOR, Person.class);
  }

  public List<ArchivalInfoPackage> getDepositAip(String depositId) {
    return this.findAllLinkedResources(depositId, ResourceName.AIP, ArchivalInfoPackage.class);
  }

  public List<DepositDataFile> getDepositDataFile(String depositId) {
    return this.findAllLinkedResources(depositId, ResourceName.DATAFILE, DepositDataFile.class);
  }

  public void removeAip(String depositId, String aipId) {
    this.removeItem(depositId, ResourceName.AIP, aipId);
  }

  public void removeDepositDataFile(String depositId, String dataFileId) {
    this.removeItem(depositId, ResourceName.DATAFILE, dataFileId);
  }

  public void removeContributor(String depositId, String contributorId) {
    this.removeItem(depositId, ResourceName.CONTRIBUTOR, contributorId);
  }

  public String reserveDOI(String depositId) {
    final Deposit deposit = this.postAction(depositId, ResourceName.RESERVE_DOI, Deposit.class);
    if (deposit != null) {
      return deposit.getDoi();
    } else {
      throw new SolidifyRuntimeException("Response to POSTing deposit (" + depositId + ") doesn't contain a body");
    }
  }

  public List<Deposit> search(String search, String matchType) {
    String url = this.getResourceUrl() + "/" + "search?search=" + search;
    if (!StringTool.isNullOrEmpty(matchType)) {
      url += "&match=" + matchType;
    }
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Deposit.class);
  }

  public List<DepositDataFile> search(String depositId, String search, String matchType) {
    String url = this.getResourceUrl() + "/" + depositId + "/" + ResourceName.DATAFILE + "/search?search=" + search;
    if (!StringTool.isNullOrEmpty(matchType)) {
      url += "&match=" + matchType;
    }
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, DepositDataFile.class);
  }

  @Override
  public List<Deposit> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Deposit.class);
  }

  public DepositDataFile updateDepositDataFile(String depositId, String dataFileId, Map<String, Object> mapProperties) {
    return this.updateSubItem(depositId, ResourceName.DATAFILE, DepositDataFile.class, dataFileId, mapProperties);
  }

  public DepositDataFile uploadDepositDataFile(String resId, Resource resource) {
    return this.uploadFile(resId, resource, new FileUploadDto<>());
  }

  public DepositDataFile uploadDepositDataFile(String resId, Resource resource, FileUploadDto<DepositDataFile> fileUploadDto) {
    return this.uploadFile(resId, resource, fileUploadDto);
  }

  public List<DepositDataFile> uploadDepositDataFileArchive(String resId, Resource resource) {
    return this.uploadArchiveFile(resId, resource, new FileUploadDto<>());
  }

  public List<DepositDataFile> uploadDepositDataFileArchive(String resId, Resource resource, FileUploadDto<DepositDataFile> fileUploadDto) {
    return this.uploadArchiveFile(resId, resource, fileUploadDto);
  }

  public Result enableRevision(String resId) {
    return this.postAction(resId, DLCMActionName.ENABLE_REVISION);
  }

  public Result startEditingMetadata(String resId) {
    return this.postAction(resId, DLCMActionName.START_METADATA_EDITING);
  }

  public Result cancelEditingMetadata(String resId) {
    return this.postAction(resId, DLCMActionName.CANCEL_METADATA_EDITING);
  }

  public Result putDepositInError(String resId) {
    return this.postAction(resId, DLCMActionName.PUT_IN_ERROR);
  }

  public DepositDataFile uploadMetadataFile(String resId, org.springframework.core.io.Resource resource) {
    final String url = this.getResourceUrl()
            + "/" + resId
            + "/" + DLCMActionName.UPLOAD_METADATA;
    return this.uploadFile(url, resource, DepositDataFile.class);
  }

  public String getHistory(String resId) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + ResourceName.HISTORY;
    final RestTemplate restTemplate = this.restClientTool.getClient(url);
    return restTemplate.getForObject(url, String.class);
  }

  public String checkSubmissionAgreement(String resId) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + DLCMActionName.CHECK_SUBMISSION_AGREEMENT;
    final RestTemplate restTemplate = this.restClientTool.getClient(url);
    return restTemplate.postForObject(url, null, String.class);
  }

  public void approveSubmissionAgreement(String resId) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + DLCMActionName.APPROVE_SUBMISSION_AGREEMENT;
    final RestTemplate restTemplate = this.restClientTool.getClient(url);
    restTemplate.postForObject(url, null, String.class);
  }

  private List<DepositDataFile> uploadArchiveFile(String resId, org.springframework.core.io.Resource resource,
          FileUploadDto<DepositDataFile> fileUploadDto) {

    final String url = this.getResourceUrl()
            + "/" + resId
            + "/" + ActionName.UPLOAD_ARCHIVE;

    final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

    map.add("file", resource);

    if (fileUploadDto.getDataCategory() != null) {
      map.add("category", fileUploadDto.getDataCategory().name());
    }

    if (fileUploadDto.getDataType() != null) {
      map.add("type", fileUploadDto.getDataType().name());
    }

    if (fileUploadDto.getMetadataType() != null) {
      map.add("metadataType", fileUploadDto.getMetadataType());
    }

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);

    final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getBaseUrl());
    final ResponseEntity<DepositDataFile[]> res = restTemplate.exchange(url, HttpMethod.POST, requestEntity, DepositDataFile[].class);

    return new ArrayList<>(Arrays.asList(res.getBody()));

  }

  private DepositDataFile uploadFile(String resId, org.springframework.core.io.Resource resource, FileUploadDto<DepositDataFile> fileUploadDto) {

    final String url = this.getResourceUrl()
            + "/" + resId
            + "/" + ActionName.UPLOAD;

    final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

    if (fileUploadDto.getDataCategory() != null) {
      map.add("category", fileUploadDto.getDataCategory().name());
    }
    if (fileUploadDto.getDataType() != null) {
      map.add("type", fileUploadDto.getDataType().name());
    }
    if (fileUploadDto.getRelLocation() != null) {
      map.add("folder", fileUploadDto.getRelLocation());
    }
    if (fileUploadDto.getMetadataType() != null) {
      map.add("metadataType", fileUploadDto.getMetadataType());
    }

    if (fileUploadDto.getChecksums() != null) {
      Set<Map.Entry<ChecksumAlgorithm, String>> checksumsSet = fileUploadDto.getChecksums().entrySet();
      for (Map.Entry<ChecksumAlgorithm, String> checksum : checksumsSet) {
        if (!StringTool.isNullOrEmpty(checksum.getValue())) {
          map.add(this.getChecksumParamFromAlgo(checksum.getKey()), checksum.getValue());
        }
      }
    }

    if (fileUploadDto.getChecksumsOrigin() != null) {
      Set<Map.Entry<ChecksumAlgorithm, DataFileChecksum.ChecksumOrigin>> checksumsOriginSet = fileUploadDto.getChecksumsOrigin().entrySet();
      for (Map.Entry<ChecksumAlgorithm, DataFileChecksum.ChecksumOrigin> checksumOrigin : checksumsOriginSet) {
        map.add(this.getChecksumOriginParamFromAlgo(checksumOrigin.getKey()), checksumOrigin.getValue().name());
      }
    }

    return this.uploadFile(url, resource, DepositDataFile.class, map);
  }

  private String getChecksumParamFromAlgo(ChecksumAlgorithm algo) {
    return switch (algo) {
      case MD5 -> DLCMConstants.CHECKSUM_MD5;
      case SHA1 -> DLCMConstants.CHECKSUM_SHA1;
      case SHA256 -> DLCMConstants.CHECKSUM_SHA256;
      case SHA512 -> DLCMConstants.CHECKSUM_SHA512;
      case CRC32 -> DLCMConstants.CHECKSUM_CRC32;
      default -> throw new SolidifyRuntimeException("Unknow checksum algo");
    };

  }

  private String getChecksumOriginParamFromAlgo(ChecksumAlgorithm algo) {
    return switch (algo) {
      case MD5 -> DLCMConstants.CHECKSUM_MD5_ORIGIN;
      case SHA1 -> DLCMConstants.CHECKSUM_SHA1_ORIGIN;
      case SHA256 -> DLCMConstants.CHECKSUM_SHA256_ORIGIN;
      case SHA512 -> DLCMConstants.CHECKSUM_SHA512_ORIGIN;
      case CRC32 -> DLCMConstants.CHECKSUM_CRC32_ORIGIN;
      default -> throw new SolidifyRuntimeException("Unknow checksum algo");
    };
  }

}

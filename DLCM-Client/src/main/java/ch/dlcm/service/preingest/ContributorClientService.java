/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - ContributorClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.preingest;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.NoSqlResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.preingest.Contributor;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class ContributorClientService extends NoSqlResourceClientService<Contributor> {

  public ContributorClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.PREINGEST,
            ResourceName.CONTRIBUTOR,
            Contributor.class,
            restClientTool,
            env);
  }

  public RestCollection<Contributor> list() {
    return this.list(this.getResourceUrl());
  }

  public RestCollection<Deposit> listDeposits(String personId) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String jsonResult = this
            .getResources(this.getResourceUrl() + "/" + personId + "/" + ResourceName.DEPOSIT, pageable);
    return this.deserializeResponse(jsonResult, Deposit.class);
  }

  public RestCollection<Contributor> listWithDeposits() {
    return this.list(this.getResourceUrl() + "?depositNumber=1");
  }

  public List<Contributor> searchContributors(Map<String, String> properties) {
    return this.searchByProperties(this.getResourceUrl(), properties, Contributor.class);
  }

  public List<Deposit> searchDeposits(String personId, Map<String, String> properties) {
    final String url = this.getResourceUrl() + "/" + personId + "/" + ResourceName.DEPOSIT;
    return this.searchByProperties(url, properties, Deposit.class);
  }

  private RestCollection<Contributor> list(String url) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String jsonResult = this.getResources(url, pageable);
    return this.deserializeResponse(jsonResult);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - ArchiveTypeClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.settings.ArchiveType;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class ArchiveTypeClientService extends ResourceClientService<ArchiveType> {

  protected ArchiveTypeClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.ARCHIVE_TYPE,
            ArchiveType.class,
            restClientTool,
            env);
  }

  public List<ArchiveType> findAllMasterType() {
    LinkedMultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
    parameters.add(DLCMConstants.ARCHIVE_MASTER_TYPE_ID_PARAM, "true");
    return this.findAllWithParameters(ArchiveType.class, parameters);
  }

  public List<ArchiveType> findAllForArchiveType(String archiveType) {
    LinkedMultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
    parameters.add(DLCMConstants.ARCHIVE_MASTER_TYPE_ID_PARAM, archiveType.toUpperCase());
    return this.findAllWithParameters(ArchiveType.class, parameters);
  }

  @Override
  public List<ArchiveType> findAll() {
    return this.findAll(ArchiveType.class);
  }

  @Override
  public List<ArchiveType> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, ArchiveType.class);
  }

}

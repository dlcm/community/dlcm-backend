/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - InstitutionClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.admin;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.display.PersonRole;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.InstitutionPersonRole;
import ch.dlcm.model.settings.InstitutionPersonRoleKey;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class InstitutionClientService extends ResourceClientService<Institution> {

  public InstitutionClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.INSTITUTION,
            Institution.class,
            restClientTool,
            env);
  }

  public void addMember(String institutionId, String personId) {
    this.addItem(institutionId, ResourceName.MEMBER, personId, Person.class);
  }

  public void addPerson(String institutionId, String personId) {
    this.addItem(institutionId, ResourceName.PERSON, personId, Person.class);
  }

  public void addPerson(String institutionId, String personId, String roleId) {
    this.addRelationItem(institutionId, ResourceName.PERSON + "/" + personId, Role.class, roleId);
  }

  public void addPersonRole(String institutionId, String personId, String roleName) {
    this.addRelationItem(institutionId, ResourceName.PERSON + "/" + personId, Role.class, roleName);
  }

  public void addOrganizationalUnit(String institutionId, String orgUnitId) {
    this.addItem(institutionId, ResourceName.ORG_UNIT, orgUnitId, OrganizationalUnit.class);
  }

  public void updatePersonRole(String institutionId, String personId, String roleName) {
    InstitutionPersonRole joinResource = new InstitutionPersonRole();
    InstitutionPersonRoleKey compositeKey = new InstitutionPersonRoleKey();
    Role role = new Role();
    role.setName(roleName);
    compositeKey.setRole(role);
    joinResource.setCompositeKey(compositeKey);
    this.updateRelationItem(institutionId, ResourceName.PERSON + "/" + personId, Role.class, joinResource);
  }

  @Override
  public List<Institution> findAll() {
    return this.findAll(Institution.class);
  }

  public List<OrganizationalUnit> getOrganizationalUnits(String institutionId) {
    return this.findAllLinkedResources(institutionId, ResourceName.ORG_UNIT, OrganizationalUnit.class);
  }

  public OrganizationalUnit getOrganizationalUnit(String institutionId, String organizationalUnitId) {
    for (OrganizationalUnit organizationalUnit : this.findAllLinkedResources(institutionId, ResourceName.ORG_UNIT, OrganizationalUnit.class)) {
      if (organizationalUnitId.equals(organizationalUnit.getResId())) {
        return organizationalUnit;
      }
    }
    return null;
  }

  public List<Person> getMembers(String institutionId) {
    return this.findAllLinkedResources(institutionId, ResourceName.MEMBER, Person.class);
  }

  public List<Person> getPeople(String institutionId) {
    return this.findAllLinkedResources(institutionId, ResourceName.PERSON, Person.class);
  }

  public Role getPersonRole(String institutionId, String personId) {
    for (PersonRole personRole : this.findAllLinkedResources(institutionId, ResourceName.PERSON, PersonRole.class)) {
      if (personId.equals(personRole.getResId())) {
        return personRole.getItems().get(0);
      }
    }
    return null;
  }

  public void removeOrganizationalUnit(String institutionId, String organizationalUnitId) {
    this.removeItem(institutionId, ResourceName.ORG_UNIT, organizationalUnitId);
  }

  public void removeMember(String institutionId, String personId) {
    this.removeItem(institutionId, ResourceName.MEMBER, personId);
  }

  public void removePerson(String institutionId, String personId) {
    this.removeItem(institutionId, ResourceName.PERSON, personId);
  }

  @Override
  public List<Institution> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Institution.class);
  }

  public Institution uploadLogo(String resId, Resource file) {
    return this.uploadFile(resId, DLCMActionName.UPLOAD_LOGO, file);
  }

  public void downloadLogo(String resId, Path path) {
    this.downloadFile(resId, DLCMActionName.DOWNLOAD_LOGO, path);
  }

  public void deleteLogo(String resId) {
    this.deleteAction(resId, DLCMActionName.DELETE_LOGO);
  }

  public Institution findByRorId(String rorId) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.ROR_ID, rorId, Institution.class);
  }

  public Institution findByName(String name) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.NAME, name, Institution.class);
  }
}

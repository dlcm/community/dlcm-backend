/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - OrganizationalUnitClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.admin;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.display.OrganizationalUnitDisseminationPolicyDTO;
import ch.dlcm.model.display.OrganizationalUnitPreservationPolicyDTO;
import ch.dlcm.model.display.OrganizationalUnitSubmissionPolicyDTO;
import ch.dlcm.model.display.PersonRole;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.AdditionalFieldsForm;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.OrganizationalUnitPersonRole;
import ch.dlcm.model.settings.OrganizationalUnitPersonRoleKey;
import ch.dlcm.model.settings.Person;
import ch.dlcm.model.settings.SubjectArea;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class OrganizationalUnitClientService extends ResourceClientService<OrganizationalUnit> {

  private static final String DEFAULT_POLICY = "defaultPolicy";
  private static final Logger logger = LoggerFactory.getLogger(OrganizationalUnitClientService.class);

  public OrganizationalUnitClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.ORG_UNIT,
            OrganizationalUnit.class,
            restClientTool,
            env);
  }

  public void addDisseminationPolicy(String orgUnitId, String disseminationPolicyId) {
    this.addItem(orgUnitId, ResourceName.DIS_POLICY, disseminationPolicyId, DisseminationPolicy.class);
  }

  public OrganizationalUnitDisseminationPolicyDTO addDisseminationPolicyWithParameters(String orgUnitId, String disseminationPolicyId, DisseminationPolicy disseminationPolicyParameters) {
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, orgUnitId);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    return restTemplate.postForObject(this.getAssociationUrl(ResourceName.DIS_POLICY) + "/" + disseminationPolicyId,
            disseminationPolicyParameters, OrganizationalUnitDisseminationPolicyDTO.class, params);

  }

  public void addFundingAgency(String orgUnitId, String fundingAgencyId) {
    this.addItem(orgUnitId, ResourceName.FUNDING_AGENCY, fundingAgencyId, FundingAgency.class);
  }

  public void addInstitution(String orgUnitId, String institutionId) {
    this.addItem(orgUnitId, ResourceName.INSTITUTION, institutionId, Institution.class);
  }

  public void addSubjectArea(String orgUnitId, String subjectArea) {
    this.addItem(orgUnitId, ResourceName.SUBJECT_AREA, subjectArea, SubjectArea.class);
  }

  public void addPerson(String orgUnitId, String personId) {
    this.addItem(orgUnitId, ResourceName.PERSON, personId, Person.class);
  }

  public void addPersonRole(String orgUnitId, String personId, String roleName) {
    this.addRelationItem(orgUnitId, ResourceName.PERSON + "/" + personId, Role.class, roleName);
  }

  public void updatePersonRole(String orgUnitId, String personId, String roleName) {
    OrganizationalUnitPersonRole joinResource = new OrganizationalUnitPersonRole();
    OrganizationalUnitPersonRoleKey compositeKey = new OrganizationalUnitPersonRoleKey();
    Role role = new Role();
    role.setName(roleName);
    compositeKey.setRole(role);
    joinResource.setCompositeKey(compositeKey);
    this.updateRelationItem(orgUnitId, ResourceName.PERSON + "/" + personId, Role.class, joinResource);
  }

  public void addPreservationPolicy(String orgUnitId, String policyId) {
    this.addItem(orgUnitId, ResourceName.PRES_POLICY, policyId, PreservationPolicy.class);
  }

  public void addSubmissionPolicy(String orgUnitId, String policyId) {
    this.addItem(orgUnitId, ResourceName.SUB_POLICY, policyId, SubmissionPolicy.class);
  }

  public void setSubmissionPolicyDefaultValue(String orgUnitId, String policyId, boolean defaultPolicy) {
    Map<String, Object> properties = Map.of(DEFAULT_POLICY, defaultPolicy);
    this.updateSubItem(orgUnitId, ResourceName.SUB_POLICY, SubmissionPolicy.class, policyId, properties);
  }

  public void setPreservationPolicyDefaultValue(String orgUnitId, String policyId, boolean defaultPolicy) {
    Map<String, Object> properties = Map.of(DEFAULT_POLICY, defaultPolicy);
    this.updateSubItem(orgUnitId, ResourceName.PRES_POLICY, PreservationPolicy.class, policyId, properties);
  }

  public AdditionalFieldsForm addAdditionalFieldsForm(String orgUnitId, AdditionalFieldsForm form) {
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    String url = this.getResourceUrl() + "/{resId}/" + ResourceName.ADDITIONAL_FIELDS_FORMS;
    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.DB_RES_ID, orgUnitId);
    return restTemplate.postForObject(url, form, AdditionalFieldsForm.class, params);
  }

  public AdditionalFieldsForm getCurrentAdditionalFieldsForm(String orgUnitId) {
    String url = this.getResourceUrl() + "/{resId}/" + ResourceName.ADDITIONAL_FIELDS_FORMS + "/" + ResourceName.CURRENT_VERSION;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.DB_RES_ID, orgUnitId);

    try {
      final String jsonString = restTemplate.getForObject(url, String.class, params);
      if (jsonString == null) {
        return null;
      } else {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new JavaTimeModule());
        return mapper.readValue(jsonString, AdditionalFieldsForm.class);
      }
    } catch (final IOException e) {
      logger.error("Error getting object from " + url, e);
      return null;
    } catch (final HttpClientErrorException e) {
      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
        // No item found return null
        return null;
      } else {
        throw e;
      }
    }
  }

  public void close(String orgUnitId, String date) {
    final String url = this.getResourceUrl()
            + "/" + orgUnitId
            + "/" + ActionName.CLOSE
            + "?" + "closingDate=" + date;

    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());

    restTemplate.postForObject(url, null, Result.class);

  }

  @Override
  public List<OrganizationalUnit> findAll() {
    return this.findAll(OrganizationalUnit.class);
  }

  public List<OrganizationalUnitDisseminationPolicyDTO> getDisseminationPolicies(String orgUnitId) {
    return this.findAllLinkedResources(orgUnitId, ResourceName.DIS_POLICY, OrganizationalUnitDisseminationPolicyDTO.class);
  }

  public OrganizationalUnitDisseminationPolicyDTO getDisseminationPolicy(String orgUnitId, String disseminationPolicyId,
          String compositeKey) {
    final String url = this.getResourceUrl() + "/" + orgUnitId + "/" + ResourceName.DIS_POLICY + "/" + disseminationPolicyId + SIZE_PARAM +
            RestCollectionPage.MAX_SIZE_PAGE + "&" + SolidifyConstants.JOIN_RESOURCE_ID_PARAM + "=" + compositeKey;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    return restTemplate.getForObject(url, OrganizationalUnitDisseminationPolicyDTO.class);
  }

  public List<FundingAgency> getFundingAgencies(String orgUnitId) {
    return this.findAllLinkedResources(orgUnitId, ResourceName.FUNDING_AGENCY, FundingAgency.class);
  }

  public List<Institution> getInstitutions(String orgUnitId) {
    return this.findAllLinkedResources(orgUnitId, ResourceName.INSTITUTION, Institution.class);
  }

  public List<SubjectArea> getSubjectAreas(String orgUnitId) {
    return this.findAllLinkedResources(orgUnitId, ResourceName.SUBJECT_AREA, SubjectArea.class);
  }

  public List<Person> getPeople(String orgUnitId) {
    return this.findAllLinkedResources(orgUnitId, ResourceName.PERSON, Person.class);
  }

  public Role getPersonRole(String orgUnitId, String personId) {
    for (PersonRole personRole : this.findAllLinkedResources(orgUnitId, ResourceName.PERSON, PersonRole.class)) {
      if (personId.equals(personRole.getResId())) {
        return personRole.getItems().get(0);
      }
    }
    return null;
  }

  public List<OrganizationalUnitPreservationPolicyDTO> getPreservationPolicies(String orgUnitId) {
    return this.findAllLinkedResources(orgUnitId, ResourceName.PRES_POLICY, OrganizationalUnitPreservationPolicyDTO.class);
  }

  public List<OrganizationalUnitSubmissionPolicyDTO> getSubmissionPolicies(String orgUnitId) {
    return this.findAllLinkedResources(orgUnitId, ResourceName.SUB_POLICY, OrganizationalUnitSubmissionPolicyDTO.class);
  }

  public void removeDisseminationPolicies(String orgUnitId, String disseminationPolicyId) {
    this.removeItem(orgUnitId, ResourceName.DIS_POLICY, disseminationPolicyId);
  }

  public void removeDisseminationPolicy(String orgUnitId, String disseminationPolicyId, String orgUnitDisseminationPolicyId) {
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, orgUnitId);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.delete(this.getAssociationUrl(ResourceName.DIS_POLICY + "/" + disseminationPolicyId) +
            "?" + SolidifyConstants.JOIN_RESOURCE_ID_PARAM + "=" + orgUnitDisseminationPolicyId, params);
  }

  public void removeFundingAgency(String orgUnitId, String fundingAgencyId) {
    this.removeItem(orgUnitId, ResourceName.FUNDING_AGENCY, fundingAgencyId);
  }

  public void removeInstitution(String orgUnitId, String institutionId) {
    this.removeItem(orgUnitId, ResourceName.INSTITUTION, institutionId);
  }

  public void removeSubjectArea(String orgUnitId, String subjectAreaId) {
    this.removeItem(orgUnitId, ResourceName.SUBJECT_AREA, subjectAreaId);
  }

  public void removePerson(String orgUnitId, String personId) {
    this.removeItem(orgUnitId, ResourceName.PERSON, personId);
  }

  public void removePerson(String orgUnitId, String personId, String roleName) {
    this.removeItem(orgUnitId, ResourceName.PERSON, personId, roleName);
  }

  public void removePreservationPolicy(String orgUnitId, String policyId) {
    this.removeItem(orgUnitId, ResourceName.PRES_POLICY, policyId);
  }

  public void removeSubmissionPolicy(String orgUnitId, String policyId) {
    this.removeItem(orgUnitId, ResourceName.SUB_POLICY, policyId);
  }

  @Override
  public List<OrganizationalUnit> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, OrganizationalUnit.class);
  }

  public OrganizationalUnit uploadLogo(String resId, Resource file) {
    return this.uploadFile(resId, DLCMActionName.UPLOAD_LOGO, file);
  }

  public void downloadLogo(String resId, Path path) {
    this.downloadFile(resId, DLCMActionName.DOWNLOAD_LOGO, path);
  }

  public void deleteLogo(String resId) {
    this.deleteAction(resId, DLCMActionName.DELETE_LOGO);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - PersonClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.admin;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class PersonClientService extends ResourceClientService<Person> {

  public PersonClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.PERSON,
            Person.class,
            restClientTool,
            env);
  }

  public void addInstitution(String personId, String institutionId) {
    final String url = this.getBaseUrl() + "/" + ResourceName.MEMBER + "/" + personId + "/" + ResourceName.INSTITUTION;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.postForObject(url, new String[] { institutionId }, Object.class);
  }

  public void addOrganizationalUnit(String personResId, String organizationalUnitResId) {
    this.addItem(personResId, ResourceName.ORG_UNIT, organizationalUnitResId, OrganizationalUnit.class);
  }

  public void addOrganizationalUnit(String personId, String organizationalUnitId, String... roleNames) {
    this.addRelationItem(personId, ResourceName.ORG_UNIT + "/" + organizationalUnitId, Role.class, roleNames);
  }

  @Override
  public List<Person> findAll() {
    return this.findAll(Person.class);
  }

  public List<Institution> getInstitutions(String personId) {
    final String url = this.getBaseUrl() + "/" + ResourceName.MEMBER + "/" + personId + "/" + ResourceName.INSTITUTION + SIZE_PARAM +
            RestCollectionPage.MAX_SIZE_PAGE;
    final RestTemplate restTemplate = this.getRestClientTool().getClient();
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Institution.class);
  }

  public List<OrganizationalUnit> getOrganizationalUnits(String personId) {
    return this.findAllLinkedResources(personId, ResourceName.ORG_UNIT, OrganizationalUnit.class);
  }

  public void removeOrganizationalUnit(String personId, String organizationalUnitId) {
    this.removeItem(personId, ResourceName.ORG_UNIT, organizationalUnitId);
  }

  public void removeOrganizationalUnit(String personId, String organizationalUnitId, String roleName) {
    this.removeItem(personId, ResourceName.ORG_UNIT, organizationalUnitId, roleName);
  }

  public List<Person> searchWithUser(String search) {
    String url = this.getResourceUrl() + "/" + DLCMActionName.SEARCH_WITH_USER + "?search=" + search;

    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Person.class);
  }

  public List<Person> search(String search, String matchType) {
    return this.search(search, matchType, Person.class);
  }

  @Override
  public List<Person> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Person.class);
  }

  public Person uploadAvatar(String resId, Resource file) {
    return this.uploadFile(resId, DLCMActionName.UPLOAD_AVATAR, file);
  }

  public void downloadAvatar(String resId, Path path) {
    this.downloadFile(resId, DLCMActionName.DOWNLOAD_AVATAR, path);
  }

  public void deleteAvatar(String resId) {
    this.deleteAction(resId, DLCMActionName.DELETE_AVATAR);
  }

  public Optional<Person> findByOrcid(String orcid) {
    try {
      return Optional.of(this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.ORCID, orcid, Person.class));
    } catch (HttpClientErrorException.NotFound e) {
      return Optional.empty();
    }
  }
}

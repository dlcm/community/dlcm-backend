/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - SubmissionAgreementClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.admin;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.policies.SubmissionAgreementUser;
import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class SubmissionAgreementClientService extends ResourceClientService<SubmissionAgreement> {

  private static final String TITLE_FIELD = "title";
  private static final String DESCRIPTION_FIELD = "description";
  private static final String VERSION_FIELD = "version";

  public SubmissionAgreementClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.SUBMISSION_AGREEMENT,
            SubmissionAgreement.class,
            restClientTool,
            env);
  }

  @Override
  public List<SubmissionAgreement> findAll() {
    return this.findAll(SubmissionAgreement.class);
  }

  @Override
  public List<SubmissionAgreement> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, SubmissionAgreement.class);
  }

  public SubmissionAgreement createSubmissionAgreementWithFile(SubmissionAgreement submissionAgreement, Resource file) {
    final String url = this.getResourceUrl() + "/" + DLCMActionName.UPLOAD_SUBMISSION_AGREEMENT;
    final MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
    parameters.add(TITLE_FIELD, submissionAgreement.getTitle());
    parameters.add(DESCRIPTION_FIELD, submissionAgreement.getDescription());
    parameters.add(VERSION_FIELD, submissionAgreement.getVersion());
    return this.uploadFile(url, file, SubmissionAgreement.class, parameters);
  }

  public SubmissionAgreement uploadSubmissionAgreementFile(String submissionAgreementId, Resource file, String mimeType) {
    return this.uploadFile(submissionAgreementId, DLCMActionName.UPLOAD_FILE, file, mimeType);
  }

  public void deleteFile(String submissionAgreementId) {
    this.deleteAction(submissionAgreementId, DLCMActionName.DELETE_FILE);
  }

  public void downloadSubmissionAgreementFile(String submissionAgreementId, Path path) {
    this.downloadFile(submissionAgreementId, DLCMActionName.DOWNLOAD_FILE, path);
  }

  public List<SubmissionAgreementUser> getAllApprobations(String submissionAgreementId) {
    final String url = this.getResourceUrl() + "/" + DLCMActionName.GET_ALL_APPROBATIONS + "?submissionAgreementId=" + submissionAgreementId;
    final RestTemplate restTemplate = this.restClientTool.getClient(url);
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, SubmissionAgreementUser.class);
  }

  public List<SubmissionAgreementUser> getMyApprobations(String submissionAgreementId) {
    final String url = this.getResourceUrl() + "/" + submissionAgreementId + "/" + DLCMActionName.GET_MY_APPROBATIONS;
    final RestTemplate restTemplate = this.restClientTool.getClient(url);
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, SubmissionAgreementUser.class);
  }

  public long getMyApprobationsNumber(String submissionAgreementId) {
    final String url = this.getResourceUrl() + "/" + submissionAgreementId + "/" + DLCMActionName.GET_MY_APPROBATIONS;
    final RestTemplate restTemplate = this.restClientTool.getClient(url);
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getPage(jsonString).getTotalItems();
  }

  public void deleteUserAgreement(String submissionAgreementId, String userId) {
    final String url = this.getResourceUrl() + "/" + submissionAgreementId + "/" + ResourceName.USER + "/" + userId;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.delete(url);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - NotificationClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class NotificationClientService extends ResourceClientService<Notification> {

  public NotificationClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.NOTIFICATION,
            Notification.class,
            restClientTool,
            env);
  }

  @Override
  public List<Notification> findAll() {
    return this.findAllWithPagination(Notification.class);
  }

  public List<Notification> getInboxNotifications() {
    return this.doActionOnList(DLCMActionName.INBOX, Notification.class);
  }

  public List<Notification> getInboxNotifications(NotificationType notificationType) {
    final String type = notificationType.toString();
    return this.docActionOnListWithParameter(DLCMActionName.INBOX, Notification.class, "notificationType", type);
  }

  public List<Notification> getInboxNotificationsByOrgUnit(String orgUnitId) {
    return this.docActionOnListWithParameter(DLCMActionName.INBOX, Notification.class, "notifiedOrgUnit.resId", orgUnitId);
  }

  public Notification getInboxNotification(String notificationId) {
    try {
      return this.doActionWithId(notificationId, DLCMActionName.INBOX);
    } catch (HttpClientErrorException.NotFound e) {
      return null;
    }
  }

  public List<Notification> getSentNotifications() {
    return this.doActionOnList(DLCMActionName.SENT, Notification.class);
  }

  public List<Notification> getSentNotificationByStatus(NotificationStatus notificationStatus) {
    final String statusName = notificationStatus.toString();
    return this.docActionOnListWithParameter(DLCMActionName.SENT, Notification.class, "notificationStatus", statusName);
  }

  public List<Notification> getSentNotificationsByOrgUnit(String orgUnitId) {
    return this.docActionOnListWithParameter(DLCMActionName.SENT, Notification.class, "notifiedOrgUnit.resId", orgUnitId);
  }

  public Notification getSentNotification(String notificationId) {
    return this.doActionWithId(notificationId, DLCMActionName.SENT);
  }

  public Notification setProcessedStatus(String notificationId) {
    return this.applyAction(notificationId, DLCMActionName.SET_APPROVED);
  }

  public Notification setRefusedStatus(String notificationId, String reason) {
    final Map<String, String> params = new HashMap<>();
    params.put(DLCMConstants.REASON, reason);
    return this.applyAction(notificationId, DLCMActionName.SET_REFUSED, params);
  }

  public Notification setPendingStatus(String notificationId) {
    return this.applyAction(notificationId, DLCMActionName.SET_PENDING);
  }

  public Notification setReadMark(String notificationId) {
    return this.applyAction(notificationId, DLCMActionName.SET_READ);
  }

  public Notification setUnreadMark(String notificationId) {
    return this.applyAction(notificationId, DLCMActionName.SET_UNREAD);
  }

  @Override
  public List<Notification> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Notification.class);
  }

  private Notification applyAction(String notificationId, String actionName) {
    return this.applyAction(notificationId, actionName, new HashMap<>());
  }

  private Notification applyAction(String notificationId, String actionName, Map<String, String> params) {
    String url = this.getResourceUrl()
            + "/" + notificationId
            + "/" + actionName;

    if (!params.isEmpty()) {
      url += "?";
      for (String paramName : params.keySet()) {
        url += StringTool.encodeUrl(paramName) + "=" + StringTool.encodeUrl(params.get(paramName));
      }
    }

    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    return restTemplate.postForObject(url, null, Notification.class);
  }
}

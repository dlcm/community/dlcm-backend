/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - ArchiveACLClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class ArchiveACLClientService extends ResourceClientService<ArchiveACL> {

  public ArchiveACLClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.ARCHIVE_ACL,
            ArchiveACL.class,
            restClientTool,
            env);
  }

  @Override
  public List<ArchiveACL> findAll() {
    return this.findAll(ArchiveACL.class);
  }

  @Override
  public List<ArchiveACL> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, ArchiveACL.class);
  }

  public List<ArchiveACL> listAllArchiveAcl() {
    return this.doActionOnList(DLCMActionName.LIST_ALL_ARCHIVE_ACL, ArchiveACL.class);
  }

  public ArchiveACL createAndUploadDuaFile(ArchiveACL archiveACL, org.springframework.core.io.Resource file) {
    final MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);

    parameters.add(SolidifyConstants.FILE_PARAM, file);
    parameters.add("archiveId", archiveACL.getAipId());
    parameters.add("userId", archiveACL.getUser().getResId());
    parameters.add("organizationalUnitId", archiveACL.getOrganizationalUnit().getResId());

    final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(parameters, headers);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(this.getResourceUrl() + "/" + DLCMActionName.UPLOAD_DUA, requestEntity, ArchiveACL.class);
  }

  public void deleteDuaFile(String resId) {
    this.deleteAction(resId, DLCMActionName.DELETE_DUA);
  }
}

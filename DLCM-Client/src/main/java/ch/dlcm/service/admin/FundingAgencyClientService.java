/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - FundingAgencyClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.admin;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.ResourceIdentifierType;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class FundingAgencyClientService extends ResourceClientService<FundingAgency> {
  /********************************/

  public FundingAgencyClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.FUNDING_AGENCY,
            FundingAgency.class,
            restClientTool,
            env);
  }

  public void addOrganizationalUnit(String fundingAgencyId, String unitId) {
    this.addItem(fundingAgencyId, ResourceName.ORG_UNIT, unitId, OrganizationalUnit.class);
  }

  /********************************/

  @Override
  public List<FundingAgency> findAll() {
    return this.findAll(FundingAgency.class);
  }

  public List<OrganizationalUnit> getOrganizationalUnits(String fundingAgencyId) {
    return this.findAllLinkedResources(fundingAgencyId, ResourceName.ORG_UNIT, OrganizationalUnit.class);
  }

  public void removeOrganizationalUnit(String fundingAgencyId, String unitId) {
    this.removeItem(fundingAgencyId, ResourceName.ORG_UNIT, unitId);
  }

  @Override
  public List<FundingAgency> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, FundingAgency.class);
  }

  public FundingAgency uploadLogo(String resId, Resource file) {
    return this.uploadFile(resId, DLCMActionName.UPLOAD_LOGO, file);
  }

  public void downloadLogo(String resId, Path path) {
    this.downloadFile(resId, DLCMActionName.DOWNLOAD_LOGO, path);
  }

  public void deleteLogo(String resId) {
    this.deleteAction(resId, DLCMActionName.DELETE_LOGO);
  }

  public FundingAgency findByRorId(String rorId) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.ROR_ID, rorId, FundingAgency.class);
  }

  public FundingAgency findByAcronym(String acronym) {
    return this.findByIdentifierType(this.getResourceUrl(), ResourceIdentifierType.ACRONYM, acronym, FundingAgency.class);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - OrganizationalUnitDisseminationPolicyClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.admin;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.display.OrganizationalUnitDisseminationPolicyDTO;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class OrganizationalUnitDisseminationPolicyClientService {

  private static final Object SIZE_PARAM = "?" + ActionName.SIZE + "=";
  private AbstractRestClientTool restClientTool;
  private final Environment env;

  public OrganizationalUnitDisseminationPolicyClientService(SudoRestClientTool restClientTool, Environment env) {
    this.restClientTool = restClientTool;
    this.env = env;
  }

  public OrganizationalUnitDisseminationPolicyDTO find(String orgUnitId, String disseminatonPolicyId) {
    String url = this.getBaseUrl() + "/" + orgUnitId + "/" + ResourceName.DIS_POLICY + "/" + disseminatonPolicyId + SIZE_PARAM +
            RestCollectionPage.MAX_SIZE_PAGE;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.getForObject(url, OrganizationalUnitDisseminationPolicyDTO.class);
  }

  public OrganizationalUnitDisseminationPolicyDTO find(String orgUnitId, String disseminatonPolicyId, String compositeKey) {
    String url = this.getBaseUrl() + "/" + orgUnitId + "/" + ResourceName.DIS_POLICY + "/" + disseminatonPolicyId + SIZE_PARAM +
            RestCollectionPage.MAX_SIZE_PAGE + "&" + SolidifyConstants.JOIN_RESOURCE_ID_PARAM + "=" + compositeKey;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.getForObject(url, OrganizationalUnitDisseminationPolicyDTO.class);
  }

  private String getBaseUrl() {
    String propertyName = this.getModuleUrlPropertyName();
    String url = this.env.getProperty(propertyName);
    if (url == null) {
      throw new IllegalStateException("Missing property: " + propertyName);
    }
    return url + "/" + ResourceName.ORG_UNIT;
  }

  private String getModuleUrlPropertyName() {
    return SolidifyClientApplication.getApplicationPrefix() + ".module." + ModuleName.ADMIN + ".url";
  }

}

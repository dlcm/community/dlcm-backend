/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - MetadataTypeClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.MetadataType;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class MetadataTypeClientService extends ResourceClientService<MetadataType> {

  public MetadataTypeClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ADMIN,
            ResourceName.METADATA_TYPE,
            MetadataType.class,
            restClientTool,
            env);
  }

  @Override
  public List<MetadataType> findAll() {
    return this.findAll(MetadataType.class);
  }

  public String getSchema(String resId) {
    final String url = this.getResourceUrl()
            + "/" + resId
            + "/" + SolidifyConstants.SCHEMA;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    return restTemplate.getForEntity(url, String.class).getBody();
  }

  @Override
  public List<MetadataType> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, MetadataType.class);
  }

  public boolean validate(String resId, Resource file) {
    final String url = this.getResourceUrl()
            + "/" + resId
            + "/" + DLCMActionName.VALIDATE;

    final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

    map.add("file", file);

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);

    final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
    final RestTemplate restTemplate = this.getRestClientTool().getClient();

    return restTemplate.exchange(url, HttpMethod.POST, requestEntity, Boolean.class).getBody();
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - PreservationPlanningClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.preservationplanning;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.preservation.JobExecution;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class PreservationPlanningClientService extends ResourceClientService<PreservationJob> {

  public PreservationPlanningClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.PRES_PLANNING,
            ResourceName.PRES_JOB,
            PreservationJob.class,
            restClientTool,
            env);
  }

  @Override
  public List<PreservationJob> findAll() {
    return this.findAll(PreservationJob.class);
  }

  public String[] getArchivalStorageUrls() {
    final String propertyName = "dlcm.module." + ModuleName.ARCHIVALSTORAGE + ".urls";
    final String url = this.getEnv().getProperty(propertyName);
    if (url == null) {
      throw new IllegalStateException("Missing property: dlcm.module." + ModuleName.ARCHIVALSTORAGE + ".url");
    }
    return url.split(",");
  }

  public List<JobExecution> getJobExecutions(String jobId) {
    return this.findAllLinkedResources(jobId, ResourceName.PRES_JOB_EXECUTION, JobExecution.class);
  }

  public Result resume(String jobId) {
    return this.postAction(jobId, ActionName.RESUME);
  }

  @Override
  public List<PreservationJob> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, PreservationJob.class);
  }

  public PreservationJob start(String preservationId) {
    this.postAction(preservationId, ActionName.START);
    return this.findOne(preservationId);
  }

  public JobExecution updateExecutionJob(String jobId, String executionId, Map<String, Object> mapProperties) {
    return this.updateSubItem(jobId, ResourceName.PRES_JOB_EXECUTION, JobExecution.class, executionId, mapProperties);
  }
}

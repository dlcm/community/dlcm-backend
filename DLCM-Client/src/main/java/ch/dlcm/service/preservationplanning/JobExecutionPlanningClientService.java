/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - JobExecutionPlanningClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.preservationplanning;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.CompositionResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.preservation.JobExecution;
import ch.dlcm.model.preservation.JobExecutionReport;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class JobExecutionPlanningClientService extends CompositionResourceClientService<PreservationJob, JobExecution> {

  public JobExecutionPlanningClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.PRES_PLANNING,
            ResourceName.PRES_JOB + SolidifyConstants.URL_PARENT_ID + ResourceName.PRES_JOB_EXECUTION,
            JobExecution.class,
            restClientTool,
            env);
  }

  @Override
  public List<JobExecution> findAll(String parentId) {
    return this.findAll(parentId, JobExecution.class);
  }

  public List<JobExecutionReport> findReport(String parentId, String resId) {
    final String url = this.getResourceUrl()
            + "/" + resId
            + "/" + DLCMActionName.REPORT;

    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_PARENT_ID_FIELD, parentId);

    final String result = restTemplate.exchange(url, HttpMethod.GET, null, String.class, params).getBody();
    return CollectionTool.getList(result, JobExecutionReport.class);
  }

}

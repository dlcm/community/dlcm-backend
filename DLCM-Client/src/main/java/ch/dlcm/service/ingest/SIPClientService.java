/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - SIPClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.ingest;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class SIPClientService extends ResourceClientService<SubmissionInfoPackage> {

  public SIPClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.INGEST,
            ResourceName.SIP,
            SubmissionInfoPackage.class,
            restClientTool,
            env);
  }

  public void addSipDataFile(String sipId, SipDataFile df) {
    this.addItem(sipId, ResourceName.DATAFILE, df, SipDataFile.class);
  }

  @Override
  public List<SubmissionInfoPackage> findAll() {
    return this.findAll(SubmissionInfoPackage.class);
  }

  public List<SipDataFile> getSipDataFile(String sipId) {
    return this.findAllLinkedResources(sipId, ResourceName.DATAFILE, SipDataFile.class);
  }

  public List<ArchivalInfoPackage> getAip(String sipId) {
    return this.findAllLinkedResources(sipId, ResourceName.AIP, ArchivalInfoPackage.class);
  }

  public void removeSipDataFile(String sipId, String sipFileId) {
    this.removeItem(sipId, ResourceName.DATAFILE, sipFileId);
  }

  @Override
  public List<SubmissionInfoPackage> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, SubmissionInfoPackage.class);
  }

  public void changeName(String sipId, String newName) {
    final String url = this.getResourceUrl() + "/" + sipId;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    restTemplate.patchForObject(url, Map.of("info", Map.of("name", newName)), SubmissionInfoPackage.class);
  }

}

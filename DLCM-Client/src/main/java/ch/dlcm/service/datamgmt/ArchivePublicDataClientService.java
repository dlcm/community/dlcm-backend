/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - ArchivePublicDataClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.datamgmt;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.index.ArchivePublicData;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class ArchivePublicDataClientService extends ResourceClientService<ArchivePublicData> {
  private static final Logger log = LoggerFactory.getLogger(ArchivePublicDataClientService.class);

  public ArchivePublicDataClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.DATAMGMT,
            ResourceName.ARCHIVE_PUBLIC_DATA,
            ArchivePublicData.class,
            restClientTool,
            env);
  }

  @Override
  public List<ArchivePublicData> findAll() {
    return this.findAll(ArchivePublicData.class);
  }

  @Override
  public List<ArchivePublicData> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, ArchivePublicData.class);
  }

  public ArchivePublicData uploadArchiveDuaFile(String resId, Resource file, String mimeType) {
    return this.uploadFile(resId, DLCMActionName.UPLOAD_DATASET_FILE, file, mimeType);
  }

}

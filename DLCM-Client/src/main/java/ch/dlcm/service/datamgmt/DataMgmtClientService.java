/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - DataMgmtClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.datamgmt;

import java.util.List;

import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.FacetResult;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.NoSqlResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class DataMgmtClientService extends NoSqlResourceClientService<ArchiveMetadata> {

  private static final String DATAMGMT_ALL_PATH = "/" + ResourceName.DI + "/" + ActionName.SEARCH;
  private static final String DATAMGMT_PUBLIC_PATH = "/" + ResourceName.DI_PUBLIC + "/" + ActionName.SEARCH;
  private static final String DATAMGMT_PRIVATE_PATH = "/" + ResourceName.DI_PRIVATE + "/" + ActionName.SEARCH;

  public DataMgmtClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.DATAMGMT,
            "",
            ArchiveMetadata.class,
            restClientTool,
            env);
  }

  public RestCollection<ArchiveMetadata> listAllPrivateAIP() {
    final String url = this.getBaseUrl() + DATAMGMT_PRIVATE_PATH;
    return this.getListIndex(url);
  }

  public RestCollection<ArchiveMetadata> listAllPublicAIP() {
    final String url = this.getBaseUrl() + DATAMGMT_PUBLIC_PATH;
    return this.getListIndex(url);
  }

  public RestCollection<ArchiveMetadata> listAllPublicAndPrivateAIP() {
    final String url = this.getBaseUrl() + DATAMGMT_ALL_PATH;
    return this.getListIndex(url);
  }

  public RestCollection<ArchiveMetadata> listAllPublicCollectionAip() {
    final String url = this.getBaseUrl() + DATAMGMT_PUBLIC_PATH + "/" + ActionName.SEARCH + "?query=aip-unit:false";
    return this.getListIndex(url);
  }

  public RestCollection<ArchiveMetadata> listAllPublicUnitAip() {
    final String url = this.getBaseUrl() + DATAMGMT_PUBLIC_PATH + "/" + ActionName.SEARCH + "?query=aip-unit:true";
    return this.getListIndex(url);
  }

  private RestCollection<ArchiveMetadata> getListIndex(String url) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);

    final String jsonResult = this.getResources(url, pageable);

    final List<ArchiveMetadata> list = CollectionTool.getList(jsonResult, ArchiveMetadata.class);
    final RestCollectionPage page = CollectionTool.getPage(jsonResult);
    final List<FacetResult> facets = CollectionTool.getFacetResults(jsonResult);
    return new RestCollection<>(list, page, facets);
  }

}

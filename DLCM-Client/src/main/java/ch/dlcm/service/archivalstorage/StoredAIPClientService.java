/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - StoredAIPClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.archivalstorage;

import java.util.List;

import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.NoSqlResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Service
public class StoredAIPClientService extends NoSqlResourceClientService<StoredAIP> {

  public StoredAIPClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ARCHIVALSTORAGE,
            ResourceName.STORED_AIP,
            StoredAIP.class,
            restClientTool,
            env);
  }

  public RestCollection<StoredAIP> listStoredAIP() {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    final String jsonResult = this.getResources(this.getResourceUrl(), pageable);
    return this.getJson(jsonResult);
  }

  public StoredAIP findOne(String resId) {
    final String url = this.getResourceUrl() + "/" + resId;
    return this.getObjectFromUrl(url);
  }

  private RestCollection<StoredAIP> getJson(String jsonResult) {
    final List<StoredAIP> list = CollectionTool.getList(jsonResult, StoredAIP.class);
    final RestCollectionPage page = CollectionTool.getPage(jsonResult);
    return new RestCollection<>(list, page);
  }

  @Override
  protected String getModuleUrlPropertyName() {
    return SolidifyClientApplication.getApplicationPrefix() + ".module." + ModuleName.ARCHIVALSTORAGE + ".urls";
  }
}

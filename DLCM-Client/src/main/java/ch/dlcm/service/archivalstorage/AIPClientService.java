/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Client - AIPClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.archivalstorage;

import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.ResourceName;

@Primary
@Service("aipClientServiceForFirstStorage")
public class AIPClientService extends ResourceClientService<ArchivalInfoPackage> {

  public AIPClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(),
            ModuleName.ARCHIVALSTORAGE,
            ResourceName.AIP,
            ArchivalInfoPackage.class,
            restClientTool,
            env);
  }

  public void addAip(String aipId, ArchivalInfoPackage aip) {
    this.addItem(aipId, ResourceName.AIP, aip.getResId(), ArchivalInfoPackage.class);
  }

  public void addAipDataFile(String aipId, AipDataFile df) {
    this.addItem(aipId, ResourceName.DATAFILE, df, AipDataFile.class);
  }

  @Override
  public List<ArchivalInfoPackage> findAll() {
    return this.findAllWithPagination(ArchivalInfoPackage.class);
  }

  public List<ArchivalInfoPackage> findAllByNode(String baseUrl) {

    final String url = baseUrl + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE;
    final RestTemplate restTemplate = this.restClientTool.getClient(baseUrl);

    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, ArchivalInfoPackage.class);
  }

  public List<ArchivalInfoPackage> getAip(String aipId) {
    return this.findAllLinkedResources(aipId, ResourceName.AIP, ArchivalInfoPackage.class);
  }

  public List<AipDataFile> getAipDataFile(String aipId) {
    return this.findAllLinkedResources(aipId, ResourceName.DATAFILE, AipDataFile.class);
  }

  public void removeAipDataFile(String aipId, String aipFileId) {
    this.removeItem(aipId, ResourceName.DATAFILE, aipFileId);
  }

  public ArchivalInfoPackage resume(String aipId) {
    this.postAction(aipId, ActionName.RESUME);
    return this.findOne(aipId);
  }

  @Override
  public List<ArchivalInfoPackage> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, ArchivalInfoPackage.class);
  }

  @Override
  protected String getModuleUrlPropertyName() {
    return SolidifyClientApplication.getApplicationPrefix() + ".module." + ModuleName.ARCHIVALSTORAGE + ".urls";
  }

}

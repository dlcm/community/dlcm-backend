/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AuthorizationIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.auth;

import static ch.dlcm.rest.UrlPath.ACCESS;
import static ch.dlcm.rest.UrlPath.ACCESS_AIP;
import static ch.dlcm.rest.UrlPath.ACCESS_ALL_METADATA;
import static ch.dlcm.rest.UrlPath.ACCESS_DIP;
import static ch.dlcm.rest.UrlPath.ACCESS_ORDER;
import static ch.dlcm.rest.UrlPath.ACCESS_ORG_UNIT;
import static ch.dlcm.rest.UrlPath.ACCESS_PRIVATE_METADATA;
import static ch.dlcm.rest.UrlPath.ACCESS_PUBLIC_METADATA;
import static ch.dlcm.rest.UrlPath.ADMIN;
import static ch.dlcm.rest.UrlPath.ADMIN_AUTHORIZED_ORG_UNIT;
import static ch.dlcm.rest.UrlPath.ADMIN_FUNDING_AGENCY;
import static ch.dlcm.rest.UrlPath.ADMIN_INSTITUTION;
import static ch.dlcm.rest.UrlPath.ADMIN_LANGUAGE;
import static ch.dlcm.rest.UrlPath.ADMIN_LICENSE;
import static ch.dlcm.rest.UrlPath.ADMIN_LICENSE_IMPORT_FILE;
import static ch.dlcm.rest.UrlPath.ADMIN_LICENSE_IMPORT_LIST;
import static ch.dlcm.rest.UrlPath.ADMIN_OAUTH2_CLIENT;
import static ch.dlcm.rest.UrlPath.ADMIN_OPEN_LICENSE_IMPORT_FILE;
import static ch.dlcm.rest.UrlPath.ADMIN_PERSON;
import static ch.dlcm.rest.UrlPath.ADMIN_PRESERVATION_POLICY;
import static ch.dlcm.rest.UrlPath.ADMIN_ROLE;
import static ch.dlcm.rest.UrlPath.ADMIN_SUBMISSION_AGREEMENT;
import static ch.dlcm.rest.UrlPath.ADMIN_SUBMISSION_POLICY;
import static ch.dlcm.rest.UrlPath.ADMIN_ORG_UNIT;
import static ch.dlcm.rest.UrlPath.ADMIN_USER;
import static ch.dlcm.rest.UrlPath.ARCHIVAL_STORAGE;
import static ch.dlcm.rest.UrlPath.ARCHIVAL_STORAGE_AIP;
import static ch.dlcm.rest.UrlPath.ARCHIVAL_STORAGE_STORED_AIP;
import static ch.dlcm.rest.UrlPath.AUTH;
import static ch.dlcm.rest.UrlPath.AUTH_AUTHORIZE;
import static ch.dlcm.rest.UrlPath.AUTH_CHECK_TOKEN;
import static ch.dlcm.rest.UrlPath.AUTH_SHIBLOGIN;
import static ch.dlcm.rest.UrlPath.AUTH_TOKEN;
import static ch.dlcm.rest.UrlPath.DATA_MGMT;
import static ch.dlcm.rest.UrlPath.DATA_MGMT_ITEM;
import static ch.dlcm.rest.UrlPath.DATA_MGMT_PRIVATE_DI;
import static ch.dlcm.rest.UrlPath.DATA_MGMT_PUBLIC_DI;
import static ch.dlcm.rest.UrlPath.INGEST;
import static ch.dlcm.rest.UrlPath.INGEST_SIP;
import static ch.dlcm.rest.UrlPath.PREINGEST;
import static ch.dlcm.rest.UrlPath.PREINGEST_DEPOSIT;
import static ch.dlcm.rest.UrlPath.PRES_PLANNING;
import static ch.dlcm.rest.UrlPath.PRES_PLANNING_AIP;
import static ch.dlcm.rest.UrlPath.PRES_PLANNING_MONITOR;
import static ch.dlcm.rest.UrlPath.PRES_PLANNING_PRES_JOB;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.IndexUrlPath;
import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.OAIUrlPath;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;

import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;

@Disabled("Run this manually")
@ExtendWith(SpringExtension.class)
@SpringBootTest
class AuthorizationIT {
  private final String accessionUrl = "http://localhost:16108/dlcm";
  private String accessUrl;
  private final String administrationUrl = "http://localhost:16105/dlcm";
  private String adminModuleUrl;
  private String authModuleUrl;
  private final String authorizationUrl = "http://localhost:16100/dlcm";
  private String dataManagementUrl;
  @Autowired
  private Environment env;
  private final String ingestionUrl = "http://localhost:16106/dlcm";
  private String ingestModuleUrl;
  private String preingestModuleUrl;
  private String preservationPlanningUrl;
  private String storageModuleUrl;
  private final String storagionUrl = "http://localhost:16107/dlcm";

  private String userToken;

  private static final String LAST_ID = "/id";
  private static final String MIDDLE_ID = "/id/";

  private static final List<HttpMethod> httpMethodToTest = Arrays.asList(GET, POST, PUT, PATCH, DELETE);

  @BeforeEach
  public void init() {
    this.userToken = this.env.getProperty("solidify.oauth2.accesstoken");
    this.authModuleUrl = this.env.getProperty("dlcm.module.auth.url");
    this.adminModuleUrl = this.env.getProperty("dlcm.module.admin.url");
    this.preingestModuleUrl = this.env.getProperty("dlcm.module.preingest.url");
    this.ingestModuleUrl = this.env.getProperty("dlcm.module.ingest.url");
    this.storageModuleUrl = this.env.getProperty("dlcm.module.archival-storage.urls");
    this.storageModuleUrl = this.storageModuleUrl.substring(0, this.storageModuleUrl.indexOf(','));
    this.dataManagementUrl = this.env.getProperty("dlcm.module.data-mgmt.url");
    this.accessUrl = this.env.getProperty("dlcm.module.access.url");
    this.preservationPlanningUrl = this.env.getProperty("dlcm.module.preservation-planning.url");
  }

  @Test
  void testAccessModule() {
    final String[] resourceURLs = {
            ACCESS,

            ACCESS_AIP,
            ACCESS_AIP + MIDDLE_ID + ActionName.DOWNLOAD,
            ACCESS_AIP + "/" + DLCMActionName.LIST_STATUS,
            ACCESS_AIP + MIDDLE_ID + ActionName.RESUME,
            ACCESS_AIP + MIDDLE_ID + ResourceName.AIP,
            ACCESS_AIP + MIDDLE_ID + ResourceName.AIP + LAST_ID,
            ACCESS_AIP + MIDDLE_ID + ResourceName.DATAFILE,
            ACCESS_AIP + MIDDLE_ID + ResourceName.DATAFILE + LAST_ID,

            ACCESS_DIP,
            ACCESS_DIP + MIDDLE_ID + ActionName.DOWNLOAD,
            ACCESS_DIP + "/" + DLCMActionName.LIST_STATUS,
            ACCESS_DIP + MIDDLE_ID + ActionName.RESUME,
            ACCESS_DIP + "/" + SolidifyConstants.SCHEMA,
            ACCESS_DIP + MIDDLE_ID + ResourceName.AIP,
            ACCESS_DIP + MIDDLE_ID + ResourceName.AIP + LAST_ID,
            ACCESS_DIP + MIDDLE_ID + ResourceName.DATAFILE,
            ACCESS_DIP + MIDDLE_ID + ResourceName.DATAFILE + LAST_ID,

            ACCESS_ORDER,
            ACCESS_ORDER + MIDDLE_ID + ActionName.DOWNLOAD,
            ACCESS_ORDER + "/" + DLCMActionName.ORDER_LIST_CREATED_BY_USER,
            ACCESS_ORDER + "/" + DLCMActionName.ORDER_LIST_PUBLIC,
            ACCESS_ORDER + "/" + DLCMActionName.LIST_QUERY_TYPE,
            ACCESS_ORDER + "/" + DLCMActionName.LIST_STATUS,
            ACCESS_ORDER + MIDDLE_ID + ActionName.RESUME,
            ACCESS_ORDER + MIDDLE_ID + ActionName.SAVE,
            ACCESS_ORDER + MIDDLE_ID + DLCMActionName.SUBMIT,
            ACCESS_ORDER + MIDDLE_ID + ActionName.VIEW,
            ACCESS_ORDER + MIDDLE_ID + ResourceName.DIP,
            ACCESS_ORDER + MIDDLE_ID + ResourceName.DIP + LAST_ID,
            ACCESS_ORDER + MIDDLE_ID + ResourceName.AIP,
            ACCESS_ORDER + MIDDLE_ID + ResourceName.AIP + LAST_ID,

            OAIUrlPath.OAI_PROVIDER,
            OAIUrlPath.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE,
            OAIUrlPath.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE + "/" + SolidifyConstants.XSL,

            OAIUrlPath.OAI_SET,

            ACCESS_PUBLIC_METADATA,
            ACCESS_PUBLIC_METADATA + MIDDLE_ID + DLCMActionName.THUMBNAIL,
            ACCESS_PUBLIC_METADATA + MIDDLE_ID + DLCMActionName.STATISTICS,
            ACCESS_PUBLIC_METADATA + MIDDLE_ID + DLCMActionName.SEARCH_DOI,
            ACCESS_PUBLIC_METADATA + MIDDLE_ID + ActionName.DOWNLOAD,
            ACCESS_PUBLIC_METADATA + MIDDLE_ID + DLCMActionName.DOWNLOAD_STATUS,
            ACCESS_PUBLIC_METADATA + "/" + DLCMActionName.LIST_DOWNLOAD_STATUS,
            ACCESS_PUBLIC_METADATA + MIDDLE_ID + DLCMActionName.PREPARE_DOWNLOAD,
            ACCESS_PUBLIC_METADATA + "/" + ActionName.SEARCH,

            ACCESS_ALL_METADATA,
            ACCESS_ALL_METADATA + MIDDLE_ID + DLCMActionName.THUMBNAIL,
            ACCESS_ALL_METADATA + MIDDLE_ID + DLCMActionName.STATISTICS,
            ACCESS_ALL_METADATA + MIDDLE_ID + DLCMActionName.SEARCH_DOI,
            ACCESS_ALL_METADATA + MIDDLE_ID + ActionName.DOWNLOAD,
            ACCESS_ALL_METADATA + MIDDLE_ID + DLCMActionName.DOWNLOAD_STATUS,
            ACCESS_ALL_METADATA + "/" + DLCMActionName.LIST_DOWNLOAD_STATUS,
            ACCESS_ALL_METADATA + MIDDLE_ID + DLCMActionName.PREPARE_DOWNLOAD,
            ACCESS_ALL_METADATA + "/" + ActionName.SEARCH,

            ACCESS_PRIVATE_METADATA,
            ACCESS_PRIVATE_METADATA + MIDDLE_ID + DLCMActionName.THUMBNAIL,
            ACCESS_PRIVATE_METADATA + MIDDLE_ID + DLCMActionName.STATISTICS,
            ACCESS_PRIVATE_METADATA + MIDDLE_ID + DLCMActionName.SEARCH_DOI,
            ACCESS_PRIVATE_METADATA + MIDDLE_ID + ActionName.DOWNLOAD,
            ACCESS_PRIVATE_METADATA + MIDDLE_ID + DLCMActionName.DOWNLOAD_STATUS,
            ACCESS_PRIVATE_METADATA + "/" + DLCMActionName.LIST_DOWNLOAD_STATUS,
            ACCESS_PRIVATE_METADATA + MIDDLE_ID + DLCMActionName.PREPARE_DOWNLOAD,
            ACCESS_PRIVATE_METADATA + "/" + ActionName.SEARCH,

            ACCESS_ORG_UNIT,
            ACCESS_ORG_UNIT + MIDDLE_ID + DLCMActionName.DOWNLOAD_LOGO,
            ACCESS_ORG_UNIT + MIDDLE_ID + ResourceName.ARCHIVE,
            ACCESS_ORG_UNIT + MIDDLE_ID + ResourceName.ARCHIVE + LAST_ID

    };
    this.testModule(resourceURLs, this.accessionUrl);
  }

  @Test
  void testAdminModule() {
    final String[] resourceURLs = {
            ADMIN,

            ADMIN_INSTITUTION,
            ADMIN_INSTITUTION + MIDDLE_ID + ResourceName.ORG_UNIT,
            ADMIN_INSTITUTION + MIDDLE_ID + ResourceName.ORG_UNIT + LAST_ID,
            ADMIN_INSTITUTION + MIDDLE_ID + ResourceName.MEMBER,
            ADMIN_INSTITUTION + MIDDLE_ID + ResourceName.MEMBER + LAST_ID,

            ADMIN_PERSON,
            ADMIN_PERSON, "/" + DLCMActionName.SEARCH_WITH_USER,
            ADMIN_PERSON + MIDDLE_ID + ResourceName.ORG_UNIT,
            ADMIN_PERSON + MIDDLE_ID + ResourceName.ORG_UNIT + LAST_ID,
            ADMIN_PERSON + MIDDLE_ID + ResourceName.INSTITUTION,
            ADMIN_PERSON + MIDDLE_ID + ResourceName.INSTITUTION + LAST_ID,

            ADMIN_ORG_UNIT,
            ADMIN_ORG_UNIT + MIDDLE_ID + ActionName.CLOSE,
            ADMIN_ORG_UNIT + "/" + DLCMActionName.UPLOAD_LOGO,
            ADMIN_ORG_UNIT + "/" + DLCMActionName.DOWNLOAD_LOGO,
            ADMIN_ORG_UNIT + "/" + DLCMActionName.DELETE_LOGO,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.PRES_POLICY,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.PRES_POLICY + LAST_ID,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.ADDITIONAL_FIELDS_FORMS,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.ADDITIONAL_FIELDS_FORMS + LAST_ID,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.DIS_POLICY,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.DIS_POLICY + LAST_ID,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.PRES_POLICY,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.PRES_POLICY + LAST_ID,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.PERSON,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.PERSON + LAST_ID,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.FUNDING_AGENCY,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.FUNDING_AGENCY + LAST_ID,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.INSTITUTION,
            ADMIN_ORG_UNIT + MIDDLE_ID + ResourceName.INSTITUTION + LAST_ID,

            ADMIN_AUTHORIZED_ORG_UNIT,
            ADMIN_AUTHORIZED_ORG_UNIT + LAST_ID,

            ADMIN_ROLE,

            ADMIN_LICENSE,
            ADMIN_LICENSE + ADMIN_LICENSE_IMPORT_LIST,
            ADMIN_LICENSE + ADMIN_LICENSE_IMPORT_FILE,
            ADMIN_LICENSE + ADMIN_OPEN_LICENSE_IMPORT_FILE,

            ADMIN_SUBMISSION_AGREEMENT,

            ADMIN_LANGUAGE,

            ADMIN_FUNDING_AGENCY,
            ADMIN_FUNDING_AGENCY + MIDDLE_ID + ResourceName.ORG_UNIT,
            ADMIN_FUNDING_AGENCY + MIDDLE_ID + ResourceName.ORG_UNIT + LAST_ID,

            ADMIN_USER,
            ADMIN_USER + "/" + DLCMActionName.AUTHENTICATED,
            ADMIN_USER + "/" + DLCMActionName.REVOKE_ALL_TOKENS + LAST_ID,
            // Don't test REVOKE_MY_TOKENS otherwise test user will loose its token
            // ADMIN_USER + "/" + DLCMActionName.REVOKE_MY_TOKENS,
            ADMIN_USER + MIDDLE_ID + DLCMActionName.UPLOAD_AVATAR,
            ADMIN_USER + MIDDLE_ID + DLCMActionName.DOWNLOAD_AVATAR,
            ADMIN_USER + MIDDLE_ID + DLCMActionName.DELETE_AVATAR,

            ADMIN_OAUTH2_CLIENT,
            ADMIN_OAUTH2_CLIENT + "/" + DLCMActionName.LIST_OAUTH2_GRANT_TYPE,

            ADMIN_PRESERVATION_POLICY,

            ADMIN_SUBMISSION_POLICY
    };
    this.testModule(resourceURLs, this.administrationUrl);
  }

  @Test
  void testArchivalStorageModule() {
    final String[] resourceURLs = {
            ARCHIVAL_STORAGE,

            ARCHIVAL_STORAGE_AIP,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + DLCMActionName.REPLICATE_TOMBSTONE,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + DLCMActionName.REPLICATE_PACKAGE,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + DLCMActionName.APPROVE_DISPOSAL,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + DLCMActionName.APPROVE_DISPOSAL_BY_ORGUNIT,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + DLCMActionName.EXTEND_RETENTION,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + DLCMActionName.CHECK,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + DLCMActionName.CHECK_FIXITY,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + DLCMActionName.REINDEX,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + ActionName.RESUME,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + ActionName.UPLOAD,
            ARCHIVAL_STORAGE_AIP + "/" + DLCMActionName.LIST_AIP_CONTAINER,
            ARCHIVAL_STORAGE_AIP + "/" + DLCMActionName.LIST_STATUS,
            ARCHIVAL_STORAGE_AIP + "/" + DLCMActionName.LIST_AIP_WITH_STATUS,
            ARCHIVAL_STORAGE_AIP + "/" + SolidifyConstants.SCHEMA,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + ResourceName.AIP,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + ResourceName.AIP + LAST_ID,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + ResourceName.DATAFILE,
            ARCHIVAL_STORAGE_AIP + MIDDLE_ID + ResourceName.DATAFILE + LAST_ID,

            ARCHIVAL_STORAGE_STORED_AIP,
            ARCHIVAL_STORAGE_STORED_AIP + MIDDLE_ID + ActionName.DOWNLOAD
    };
    this.testModule(resourceURLs, this.storagionUrl);
  }

  @Test
  void testAuthorizeModule() {
    final String[] resourceURLs = {
            AUTH,
            AUTH_AUTHORIZE,
            AUTH_TOKEN,
            AUTH_CHECK_TOKEN,
            AUTH_SHIBLOGIN
    };
    this.testModule(resourceURLs, this.authorizationUrl);
  }

  @Test
  void testDataManagementModule() {
    final String[] resourceURLs = {
            DATA_MGMT,

            DATA_MGMT_PUBLIC_DI,
            DATA_MGMT_PUBLIC_DI + "/" + ActionName.SEARCH,

            DATA_MGMT_PRIVATE_DI,
            DATA_MGMT_PRIVATE_DI + "/" + ActionName.SEARCH,

            DATA_MGMT_ITEM
    };
    this.testModule(resourceURLs, this.administrationUrl);
  }

  @Test
  void testIndexModule() {
    final String[] resourceURLs = {
            IndexUrlPath.INDEX,

            IndexUrlPath.INDEX_SETTING,
            IndexUrlPath.INDEX_SETTING + "/" + IndexConstants.DELETE_ALL,

            IndexUrlPath.INDEX_INDEX_FIELD_ALIAS
    };
    this.testModule(resourceURLs, this.administrationUrl);
  }

  @Test
  void testIngestModule() {
    final String[] resourceURLs = {
            INGEST,

            INGEST_SIP,
            INGEST_SIP + "/" + DLCMActionName.LIST_STATUS,
            INGEST_SIP + "/" + SolidifyConstants.SCHEMA,
            INGEST_SIP + MIDDLE_ID + DLCMActionName.CLEAN,
            INGEST_SIP + MIDDLE_ID + DLCMActionName.RESUBMIT,
            INGEST_SIP + MIDDLE_ID + ActionName.RESUME,
            INGEST_SIP + MIDDLE_ID + ResourceName.AIP_FILE,
            INGEST_SIP + MIDDLE_ID + ActionName.DOWNLOAD,
            INGEST_SIP + MIDDLE_ID + ActionName.UPLOAD,
            INGEST_SIP + MIDDLE_ID + ResourceName.AIP,
            INGEST_SIP + MIDDLE_ID + ResourceName.AIP + LAST_ID,
            INGEST_SIP + MIDDLE_ID + ResourceName.DATAFILE,
            INGEST_SIP + MIDDLE_ID + ResourceName.DATAFILE + LAST_ID

    };
    this.testModule(resourceURLs, this.ingestionUrl);
  }

  @Test
  void testPreingestModule() {
    final String[] resourceURLs = {
            PREINGEST,

            PREINGEST_DEPOSIT,
            PREINGEST_DEPOSIT + "/" + DLCMActionName.LIST_ACCESS_TYPE,
            PREINGEST_DEPOSIT + "/" + DLCMActionName.LIST_STATUS,
            PREINGEST_DEPOSIT + "/" + SolidifyConstants.SCHEMA,
            PREINGEST_DEPOSIT + MIDDLE_ID + DLCMActionName.APPROVE,
            PREINGEST_DEPOSIT + MIDDLE_ID + DLCMActionName.CLEAN,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.SIP_FILE,
            PREINGEST_DEPOSIT + MIDDLE_ID + DLCMActionName.ENABLE_REVISION,
            PREINGEST_DEPOSIT + MIDDLE_ID + ActionName.UPLOAD,
            PREINGEST_DEPOSIT + MIDDLE_ID + ActionName.DOWNLOAD,
            PREINGEST_DEPOSIT + MIDDLE_ID + DLCMActionName.LIST_IGNORE_FILES,
            PREINGEST_DEPOSIT + MIDDLE_ID + DLCMActionName.REJECT,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.RESERVE_DOI,
            PREINGEST_DEPOSIT + MIDDLE_ID + DLCMActionName.SUBMIT_FOR_APPROVAL,
            PREINGEST_DEPOSIT + MIDDLE_ID + ActionName.UPLOAD_ARCHIVE,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.DATAFILE,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.DATAFILE + LAST_ID,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.AIP,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.AIP + LAST_ID,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.CONTRIBUTOR,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.CONTRIBUTOR + LAST_ID,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.LICENSE,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.LICENSE + LAST_ID,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.SUBMISSION_AGREEMENT,
            PREINGEST_DEPOSIT + MIDDLE_ID + ResourceName.SUBMISSION_AGREEMENT + LAST_ID,

    };
    this.testModule(resourceURLs, this.ingestionUrl);
  }

  @Test
  void testPreservationPlanningModule() {
    final String[] resourceURLs = {
            PRES_PLANNING,

            PRES_PLANNING_MONITOR,

            PRES_PLANNING_PRES_JOB,
            PRES_PLANNING_PRES_JOB + "/" + DLCMActionName.INIT,
            PRES_PLANNING_PRES_JOB + "/" + DLCMActionName.LIST_JOB_RECURRENCE,
            PRES_PLANNING_PRES_JOB + "/" + DLCMActionName.LIST_JOB_TYPE,
            PRES_PLANNING_PRES_JOB + MIDDLE_ID + ActionName.RESUME,
            PRES_PLANNING_PRES_JOB + MIDDLE_ID + ActionName.START,
            PRES_PLANNING_PRES_JOB + MIDDLE_ID + ResourceName.PRES_JOB_EXECUTION,
            PRES_PLANNING_PRES_JOB + MIDDLE_ID + ResourceName.PRES_JOB_EXECUTION + LAST_ID,

            PRES_PLANNING_AIP,
            PRES_PLANNING_AIP + "/" + ActionName.DELETE_CACHE
    };
    this.testModule(resourceURLs, this.administrationUrl);
  }

  private void testModule(String[] resourceURLs, String applicationUrl) {
    final String[] trailingSlashes = { "", "/" };
    ConnectionProvider fixedPool = ConnectionProvider.builder("fixedPool")
            .maxConnections(20000)
            .pendingAcquireTimeout(Duration.ofMinutes(3))
            .build();
    HttpClient httpClient = HttpClient.create(fixedPool);
    WebClient webClient = WebClient.builder()
            .clientConnector(new ReactorClientHttpConnector(httpClient)).build();
    final WebClient client = WebClient.builder()
            .baseUrl(applicationUrl)
            .clientConnector(new ReactorClientHttpConnector(httpClient))
            .codecs(clientCodecConfigurer -> clientCodecConfigurer
                    .defaultCodecs()
                    .jackson2JsonEncoder(new Jackson2JsonEncoder(
                            // Allow to send empty Json object for test purposes
                            new ObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false), MediaType.APPLICATION_JSON)))
            .build();
    try {
      final PrintWriter printWriter = new PrintWriter(new FileWriter("authResults.txt", true), true);
      for (final HttpMethod httpMethod : httpMethodToTest) {
        for (String resourceURL : resourceURLs) {
          if (httpMethod.equals(PATCH) || httpMethod.equals(PUT) || httpMethod.equals(DELETE)) {
            resourceURL = resourceURL + LAST_ID;
          }
          final List<ClientResponse> clientResponseList = new ArrayList<>();
          for (final String trailingSlash : trailingSlashes) {
            Object objectToSend = new Object();
            if (resourceURL.equals(ADMIN_LICENSE + ADMIN_LICENSE_IMPORT_LIST)) {
              objectToSend = Collections.emptyList();
            }
            Consumer<HttpHeaders> headersConsumer = headers -> headers.setBearerAuth(this.userToken);
            if ((resourceURL.equals(ADMIN_LICENSE + ADMIN_OPEN_LICENSE_IMPORT_FILE) ||
                    resourceURL.equals(ADMIN_LICENSE + ADMIN_LICENSE_IMPORT_FILE))) {
              headersConsumer = headersConsumer
                      .andThen(headers -> headers.add(HttpHeaders.CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA_VALUE + ";boundary=a"));
              MultipartBodyBuilder builder = new MultipartBodyBuilder();
              Resource image = new ClassPathResource("geneve.jpg");
              builder.part("file", image);
              objectToSend = builder.build();
            }
            final ClientResponse clientResponse;
            if (httpMethod.equals(POST) || httpMethod.equals(PATCH)) {
              clientResponse = this.restCallWithBody(client, httpMethod, resourceURL + trailingSlash, headersConsumer, objectToSend);
            } else {
              clientResponse = this.restCallWithoutBody(client, httpMethod, resourceURL + trailingSlash, headersConsumer);
            }
            clientResponseList.add(clientResponse);
          }
          this.registerResults(clientResponseList, httpMethod, applicationUrl, resourceURL, printWriter);
        }
      }
      printWriter.close();
    } catch (IOException e) {
      System.out.println(e);
    }
  }

  private ClientResponse restCallWithBody(WebClient client, HttpMethod httpMethod, String uri, Consumer<HttpHeaders> headersConsumer,
          Object objectToSend) {
    return client.method(httpMethod).uri(uri).headers(headersConsumer).bodyValue(objectToSend).exchange().block();
  }

  private ClientResponse restCallWithoutBody(WebClient client, HttpMethod httpMethod, String uri, Consumer<HttpHeaders> headersConsumer) {
    return client.method(httpMethod).uri(uri).headers(headersConsumer).exchange().block();
  }

  private void registerResults(List<ClientResponse> clientResponseList, HttpMethod httpMethod, String applicationUrl, String resourceURL,
          PrintWriter printWriter) {
    final ClientResponse responseWithoutSlash = clientResponseList.get(0);
    final ClientResponse responseWithSlash = clientResponseList.get(1);
    if (responseWithoutSlash.statusCode().equals(responseWithSlash.statusCode())) {
      String output = httpMethod + " " + applicationUrl + resourceURL + " " + responseWithoutSlash.statusCode();
      System.out.println(output);
      printWriter.println(output);
    } else {
      String output = "WARNING response with and without slash differ\n"
              + httpMethod + " " + applicationUrl + resourceURL + " " + responseWithoutSlash.statusCode() + "\n"
              + httpMethod + " " + applicationUrl + resourceURL + "/ " + responseWithSlash.statusCode();
      System.out.println(output);
      printWriter.println(output);
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractPreingestIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.params.provider.Arguments;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

abstract class AbstractPreingestIT extends AbstractIT {

  protected OrgUnitITService orgUnitITService;
  protected DepositITService depositITService;
  protected DepositClientService depositClientService;

  protected AbstractPreingestIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService) {
    super(env, restClientTool);
    this.orgUnitITService = orgUnitITService;
    this.depositITService = depositITService;
    this.depositClientService = depositClientService;
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.orgUnitITService.cleanTestData();
    this.depositITService.cleanTestData();
    this.restClientTool.exitSudo();
  }

  protected void checkMetadata(ArchiveMetadata archiveMetadata, int totalFileNumber, int fileNumber, int metadataFileNumber, long archiveSize) {
    assertEquals(totalFileNumber, archiveMetadata.getMetadata().get(DLCMConstants.AIP_TOTAL_FILE_NUMBER), "[Metadata] Wrong total file number");
    assertEquals(fileNumber, archiveMetadata.getMetadata().get(DLCMConstants.AIP_FILE_NUMBER), "[Metadata] Wrong file number");
    assertEquals(metadataFileNumber, archiveMetadata.getMetadata().get(DLCMConstants.AIP_METADATA_FILE_NUMBER),
            "[Metadata] Wrong metadata file number");
    assertEquals(archiveSize, Integer.valueOf((int) archiveMetadata.getMetadata().get(DLCMConstants.AIP_SIZE)).longValue(),
            "[Metadata] Wrong archive size");
  }

  protected void checkAip(ArchivalInfoPackage aip, int totalFileNumber, int updateFileNumber, int collectionFileNumber, long collectionSite) {
    assertEquals(PackageStatus.COMPLETED, aip.getInfo().getStatus(), "[AIP] Wrong AIP status");
    assertEquals(totalFileNumber, aip.getArchiveFileNumber(), "[AIP] Wrong total file number");
    assertEquals(updateFileNumber, aip.getUpdateNumber(), "[AIP] Wrong update number");
    assertEquals(collectionFileNumber, aip.getCollectionFileNumber(), "[AIP] Wrong collection file number");
    assertEquals(collectionSite, aip.getCollectionArchiveSize(), "[AIP] Wrong collection size");
  }

  protected static Stream<Arguments> allVersions() {
    return Arrays.stream(DLCMMetadataVersion.values()).map(v -> arguments(v)).toList().stream();
  }

  private static Stream<Arguments> metadataFiles() {
    return Stream.of(
            arguments(DataCategory.ArchiveThumbnail, "geneve.jpg", "/deposit/bodmer/thumbnail/Faust.timg"),
            arguments(DataCategory.ArchiveReadme, "sample-readme-file.txt", "sample-readme-file2.txt"),
            arguments(DataCategory.ArchiveDataUseAgreement, "sample-dua-file.pdf", "sample-dua-file2.pdf"));
  }
}

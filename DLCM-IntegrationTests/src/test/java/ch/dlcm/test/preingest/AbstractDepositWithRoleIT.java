/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractDepositWithRoleIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.core.env.Environment;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

abstract class AbstractDepositWithRoleIT extends AbstractPreingestIT {

  protected AbstractDepositWithRoleIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
  }

  protected abstract String role();

  protected boolean inheritedRole() {
    return false;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @BeforeAll
  void setUp() {
    this.restClientTool.sudoAdmin();
    this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT, DLCMTestConstants.OrganizationalUnitStatus.OPEN,
            this.role(), AuthApplicationRole.USER_ID, this.inheritedRole());
    this.restClientTool.exitSudo();
  }

  protected Deposit createRemoteDepositForCurrentRole() {
    return this.createRemoteDepositForCurrentRole(DEFAULT_TITLE);
  }

  protected Deposit createRemoteDepositForCurrentRole(String title) {
    return this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, title, DEFAULT_DESCRIPTION, this.role(), this.inheritedRole(),
            DLCMMetadataVersion.getDefaultVersion());
  }

  protected Deposit createLocalDepositForCurrentRole() {
    return this.createLocalDepositForCurrentRole(DEFAULT_TITLE);
  }

  protected Deposit createLocalDepositForCurrentRole(String title) {
    return this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, title, DEFAULT_DESCRIPTION, this.role(), this.inheritedRole(),
            DLCMMetadataVersion.getDefaultVersion());
  }

  protected OrganizationalUnit getPermanentOrganizationalUnitWithRole() {
    return this.getPermanentOrganizationalUnitWithRole(DLCMTestConstants.OrganizationalUnitStatus.OPEN);
  }

  protected OrganizationalUnit getPermanentOrganizationalUnitWithRole(DLCMTestConstants.OrganizationalUnitStatus organizationalUnitStatus) {
    this.restClientTool.sudoAdmin();
    OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            organizationalUnitStatus, this.role(), AuthApplicationRole.USER_ID, this.inheritedRole());
    this.restClientTool.exitSudo();
    return organizationalUnit;
  }

  @Override
  protected void deleteFixtures() {
    super.deleteFixtures();
  }
}

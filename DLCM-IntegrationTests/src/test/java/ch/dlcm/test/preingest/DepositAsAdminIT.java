/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static ch.unige.solidify.SolidifyConstants.DB_DEFAULT_STRING_LENGTH;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.AdditionalFieldsForm;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class DepositAsAdminIT extends AbstractPreingestIT {

  private static final Logger log = LoggerFactory.getLogger(DepositAsAdminIT.class);

  private static final String DEPOSIT_JSON_TEMPLATE = "{\"title\" : \"%s\","
          + " \"description\" : \"lorem ipsum\","
          + " \"organizationalUnitId\" : \"%s\","
          + " \"collectionBegin\" : \"%s\","
          + " \"collectionEnd\" : \"%s\","
          + " \"contentStructurePublic\" : true }";

  private final PersonITService personService;

  @Autowired
  public DepositAsAdminIT(Environment env, SudoRestClientTool restClientTool, OrgUnitITService orgUnitITService, DepositITService depositITService,
          PersonITService personITService, DepositClientService depositClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.personService = personITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void contributorTest() {
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

    // Create a new Person
    final Person person1 = this.personService.getOrCreateRemotePerson();

    // Add the person as contributor
    this.depositClientService.addContributor(deposit.getResId(), person1.getResId());
    final List<Person> listContributors = this.depositClientService.getContributors(deposit.getResId());

    assertEquals(1, listContributors.size());
    final Person person2 = listContributors.get(0);

    // Test the contributor
    assertEquals(person1.getResId(), person2.getResId());
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
  }

  @Test
  void creationDateFormatsTest() {
    final String[] validDates = {
            "2000-08-01T13:20:00+01:00", "2000-08-01T13:20:00+0100", "2000-08-01T13:20:00.000+01:00", "2000-08-01T13:20:00.000+0100",
            "2000-08-01T13:20+01:00", "2000-08-01T13:20+0100", "2000-08-01T12:20Z"
    };

    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);

    for (final String validDate : validDates) {
      final String title = DLCMTestConstants.getRandomNameWithTemporaryLabel("test date format");
      final String depositJson = String.format(DEPOSIT_JSON_TEMPLATE, title, organizationalUnit.getResId(), validDate,
              "2040-08-01T13:20:00+01:00");
      final Deposit depositReturned = this.depositClientService.createFromJson(depositJson);

      final Deposit createdDeposit = this.depositClientService.findOne(depositReturned.getResId());
      final OffsetDateTime expectedDatetime = OffsetDateTime
              .parse("2000-08-01T13:20:00.000+0100", DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT));
      assertTrue(createdDeposit.getCollectionBegin().isEqual(expectedDatetime));
    }
  }

  @Test
  void creationOnClosedOrganizationalUnitTest() {

    final String title = DLCMTestConstants.getNameWithPermanentLabel("deposit that should not be created");

    // Try to create a deposit
    final Deposit deposit1 = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, title);
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.CLOSED);
    deposit1.setOrganizationalUnitId(organizationalUnit.getResId());

    Deposit deposit2 = null;
    try {
      deposit2 = this.depositClientService.create(deposit1);
      fail("An Http error response should be received");
    } catch (final Exception ex) {
      assertTrue(ex instanceof HttpClientErrorException);
    }

    // Test the creation has failed
    assertNull(deposit2);

    // Check that the deposit has not been created
    final List<Deposit> deposits = this.depositClientService.searchByProperties(Map.of("title", title,
            DLCMConstants.ORG_UNIT_ID_FIELD, organizationalUnit.getResId()));
    assertTrue(deposits.isEmpty());
  }

  @Test
  void creationTest() {
    log.debug("Starting DepositIT#creationTest");
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);
    log.debug("Using org unit {} ({})", organizationalUnit.getName(), organizationalUnit.getResId());
    final String baseTitle = "deposit created by test - 有朋自遠方來";

    // Create a deposit
    final Deposit deposit1 = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, baseTitle);
    deposit1.setKeywords(Arrays.asList("semiology", "phenomenology", "teleology", "dissolved organic matter"));
    final Deposit deposit2 = this.depositClientService.create(deposit1);

    // Test the creation
    assertEquals(deposit1.getTitle(), deposit2.getTitle());
    assertThat(deposit2.getKeywords(), is(deposit1.getKeywords()));

    // Check if pending ( InProgress, inValidation, inError )
    assertEquals(DepositStatus.IN_PROGRESS, deposit2.getStatus());

    // Wait one second before testing creation
    SolidifyTime.waitOneSecond();

    // Check that the deposit has been created
    final List<Deposit> deposits = this.depositClientService.searchByProperties(Map.of("title", deposit2.getTitle(),
            DLCMConstants.ORG_UNIT_ID_FIELD, organizationalUnit.getResId()));
    assertEquals(1, deposits.size());
    log.debug("Stopping DepositIT#creationTest");
  }

  @Test
  void creationWithExistingResIdTest() {
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);

    final String title1 = DLCMTestConstants.getRandomNameWithTemporaryLabel("creationWithExistingResIdTest");
    final String title2 = DLCMTestConstants.getRandomNameWithTemporaryLabel("should not be saved");

    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, title1, DEFAULT_DESCRIPTION);
    final String existingResId = deposit.getResId();

    final Deposit newDeposit = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, title2);
    newDeposit.setResId(existingResId);

    Deposit savedDeposit = null;
    try {
      savedDeposit = this.depositClientService.create(newDeposit);
      fail("A 500 Http response should be received");
    } catch (final HttpServerErrorException ex) {
      assertEquals(500, ex.getRawStatusCode());
    }

    assertNull(savedDeposit);
    deposit.setCollectionBegin(OffsetDateTime.now(ZoneOffset.UTC).minusYears(5));

    // Check that the deposit has been created
    final List<Deposit> deposits = this.depositClientService.searchByProperties(Map.of("title", title1,
            DLCMConstants.ORG_UNIT_ID_FIELD, organizationalUnit.getResId()));
    assertEquals(1, deposits.size());

    // Check that the deposit has been created
    final List<Deposit> deposits2 = this.depositClientService.searchByProperties(Map.of("title", title2,
            DLCMConstants.ORG_UNIT_ID_FIELD, organizationalUnit.getResId()));
    assertEquals(0, deposits2.size());
  }

  @Test
  void creationWithResIdTest() {

    final Deposit deposit = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE);
    final String resId = StringTool.generateResId();
    deposit.setResId(resId);
    final Deposit savedDeposit = this.depositClientService.create(deposit);

    assertNotNull(savedDeposit);
    assertEquals(savedDeposit.getResId(), resId);
  }

  @Test
  void defaultKeywordsTest() {
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);

    // Create a deposit without keywords
    final Deposit deposit1 = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, "Test default keywords");
    final Deposit deposit2 = this.depositClientService.create(deposit1);

    // Test that the keywords are set from the orgunit's default
    assertThat(deposit2.getKeywords(), is(organizationalUnit.getKeywords()));

    // Set different keywords on deposit
    deposit1.setKeywords(Arrays.asList("tomography", "hysterosalpingography", "cholecystography", "dissolved organic matter"));
    final Deposit deposit3 = this.depositClientService.update(deposit1.getResId(), deposit1);
    assertThat(deposit3.getKeywords(), is(deposit1.getKeywords()));

    // Set the deposit keywords to default
    deposit1.setKeywords(null);
    final Deposit deposit4 = this.depositClientService.update(deposit1.getResId(), deposit1);
    assertNull(deposit4.getKeywords());

  }

  @Test
  void deleteTest() {
    Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

    // Delete the deposit
    final String resId = deposit.getResId();
    this.depositClientService.delete(resId);

    assertTrue(this.depositITService.waitDepositIsDeleted(resId));
  }

  @Test
  void dontUpdateResIdTest() {
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);
    final String currentResId = deposit.getResId();

    final String newTitle = DLCMTestConstants.getRandomNameWithTemporaryLabel("B");
    final String newResId = "xyz-xyz-xyz";

    // Update the deposit with a new resId
    deposit.setTitle(newTitle);
    deposit.setResId(newResId);
    final Deposit savedDeposit = this.depositClientService.update(currentResId, deposit);

    assertNotNull(savedDeposit);
    assertEquals(savedDeposit.getResId(), currentResId); // resId has not changed
    assertEquals(savedDeposit.getTitle(), newTitle); // title has changed
  }

  /**
   * Updating the deposit status with a PATCH is forbidden (neither for ROOT and TRUSTED_USER)
   * Current user is ADMIN (no bypass) with Manager role on the org unit of the created deposit
   * DepositPermissionService send 403 if there is an attempt to change the status with such a user
   */
  @Test
  void dontUpdateStatusTest() {
    assertThrows(HttpClientErrorException.BadRequest.class, () -> {
      final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

      deposit.setStatus(DepositStatus.IN_VALIDATION);
      this.depositClientService.update(deposit.getResId(), deposit);
    });
  }

  @Test
  void downloadDepositByFolderTest() throws IOException {
    final String file1ToUpload = "person.json";
    final String tempFolder1 = "/test1";
    final String file2ToUpload = "empty.txt";
    final String tempFolder2 = "/test1/test2";

    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "download-by-folder", "deposit download");

    List<DepositDataFile> depositDataFiles = new ArrayList<>();

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setRelLocation(tempFolder1);
    DepositDataFile depositDataFile1 = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
            new ClassPathResource(file1ToUpload),
            fileUploadDto);
    depositDataFiles.add(depositDataFile1);

    FileUploadDto<DepositDataFile> fileUploadDto2 = new FileUploadDto<>();
    fileUploadDto2.setRelLocation(tempFolder2);
    DepositDataFile depositDataFile2 = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
            new ClassPathResource(file2ToUpload),
            fileUploadDto2);
    depositDataFiles.add(depositDataFile2);

    this.depositITService.waitDataFileIsReady(deposit.getResId(), 2);

    final List<DepositDataFile> list = this.depositClientService.getDepositDataFile(deposit.getResId());

    // Test the upload
    assertEquals(2, list.size());

    // check both list contains same elements regardless the order
    for (final DepositDataFile depositDataFile : depositDataFiles) {
      assertFalse(list.contains(depositDataFile));
    }

    // test download by each folder
    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.depositClientService.downloadDeposit(deposit.getResId(), tempFolder1, path);
    ZipInputStream zip = new ZipInputStream(new ByteArrayInputStream(Files.readAllBytes(path)));
    final String name = zip.getNextEntry().getName();
    assertTrue(name.endsWith(file1ToUpload) || name.endsWith(file2ToUpload));

    this.depositClientService.downloadDeposit(deposit.getResId(), tempFolder2, path);
    zip = new ZipInputStream(new ByteArrayInputStream(Files.readAllBytes(path)));
    assertTrue(zip.getNextEntry().getName().endsWith(file2ToUpload));
  }

  @Test
  void downloadDepositTest() throws IOException {
    final String file1ToUpload = "person.json";
    final String tempFolder1 = "test1";
    final String file2ToUpload = "empty.txt";
    final String tempFolder2 = "test2";

    final List<Resource> files = new ArrayList<>();
    files.add(new ClassPathResource(file1ToUpload));
    files.add(new ClassPathResource(file2ToUpload));

    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "download", "deposit download");

    List<DepositDataFile> depositDataFiles = new ArrayList<>();

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setRelLocation(tempFolder1);
    DepositDataFile depositDataFile1 = this.depositClientService.uploadDepositDataFile(deposit.getResId(), files.get(0),
            fileUploadDto);
    depositDataFiles.add(depositDataFile1);

    FileUploadDto<DepositDataFile> fileUploadDto2 = new FileUploadDto<>();
    fileUploadDto.setRelLocation(tempFolder2);
    DepositDataFile depositDataFile2 = this.depositClientService.uploadDepositDataFile(deposit.getResId(), files.get(1),
            fileUploadDto2);
    depositDataFiles.add(depositDataFile2);

    this.depositITService.waitDataFileIsReady(deposit.getResId(), 2);

    final List<DepositDataFile> list = this.depositClientService.getDepositDataFile(deposit.getResId());

    // Test the upload
    assertEquals(2, list.size());

    // check both list contains same elements regardless the order
    for (final DepositDataFile depositDataFile : depositDataFiles) {
      assertFalse(list.contains(depositDataFile));
    }

    // test that the zip contains all the files
    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.depositClientService.downloadDeposit(deposit.getResId(), null, path);
    final ZipInputStream zip = new ZipInputStream(new ByteArrayInputStream(Files.readAllBytes(path)));
    ZipEntry entry;
    while ((entry = zip.getNextEntry()) != null) {
      final String file = entry.getName();
      assertTrue(files.stream().anyMatch(f -> file.endsWith(f.getFilename())));
    }
  }

  @Test
  void reserveDOITest() {
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);
    final String depositId = deposit.getResId();

    final String newDOI = this.depositClientService.reserveDOI(depositId);

    /*
     * Fetch again Deposit
     */
    final Deposit fetchedDeposit = this.depositClientService.findOne(depositId);
    assertNotNull(fetchedDeposit.getDoi());
    assertEquals(newDOI, fetchedDeposit.getDoi());
  }

  @Test
  void unicityTest() {

    // Creating a new deposit
    final Deposit deposit1 = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE);
    final String resId = StringTool.generateResId();
    deposit1.setResId(resId);
    this.depositClientService.create(deposit1);

    // Creating a second deposit with the same resID as the first's
    final Deposit deposit2 = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE);
    deposit2.setResId(resId);

    // Testing unicity of resID
    try {
      this.depositClientService.create(deposit2);
      fail("An INTERNAL_SERVER_ERROR Http response should be received");
    } catch (final HttpServerErrorException e) {
      assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatusCode(), "Unicity constraint not satisfied");
    }
  }

  @Test
  void updateTest() {
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

    // Update the deposit
    deposit.setTitle(DLCMTestConstants.getRandomNameWithTemporaryLabel("B"));
    final Deposit actualDeposit = this.depositClientService.update(deposit.getResId(), deposit);
    final Deposit deposit4 = this.depositClientService.findOne(deposit.getResId());

    // Test the update
    assertEquals(deposit.getTitle(), deposit4.getTitle());
    this.assertEqualsWithoutNanoSeconds(deposit.getCreationTime(), deposit4.getCreationTime());
    assertNotNull(actualDeposit);
  }

  @Test
  void updateKeywordsTest() {
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

    List<String> keywords = deposit.getKeywords();
    keywords.add("newKeyword");

    this.depositClientService.update(deposit.getResId(), deposit);
    final Deposit deposit2 = this.depositClientService.findOne(deposit.getResId());
    List<String> keywords2 = deposit2.getKeywords();

    assertEquals(keywords.size(), keywords2.size());
    for (int i = 0; i < keywords.size(); i++) {
      assertEquals(keywords.get(i), keywords2.get(i));
    }

    keywords2.remove("newKeyword");
    this.depositClientService.update(deposit2.getResId(), deposit2);
    final Deposit deposit3 = this.depositClientService.findOne(deposit2.getResId());
    List<String> keywords3 = deposit3.getKeywords();

    assertEquals(keywords2.size(), keywords3.size());
    for (int i = 0; i < keywords2.size(); i++) {
      assertEquals(keywords2.get(i), keywords3.get(i));
    }
  }

  @Test
  void customMetadataValidJsonTest() {
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);
    final AdditionalFieldsForm additionalFieldsForm = this.orgUnitITService
            .getPermanentAdditionalFieldsFormForOrganizationalUnit(organizationalUnit.getResId());

    String validJson = "{\"myCustomProperty\" : \"some value\"}";

    Deposit deposit1 = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, "custom metadata");
    deposit1.setAdditionalFieldsValues(validJson);
    deposit1.setAdditionalFieldsFormId(additionalFieldsForm.getResId());

    Deposit deposit2 = this.depositClientService.create(deposit1);
    assertEquals(deposit1.getTitle(), deposit2.getTitle());
    assertEquals(deposit1.getAdditionalFieldsValues(), deposit2.getAdditionalFieldsValues());
    assertEquals(deposit1.getAdditionalFieldsFormId(), deposit2.getAdditionalFieldsFormId());
    assertEquals(validJson, deposit2.getAdditionalFieldsValues());

  }

  /**
   * Custom metadata must be valid JSON
   */
  @Test
  void customMetadataInvalidJsonTest() {
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);
    final AdditionalFieldsForm additionalFieldsForm = this.orgUnitITService
            .getPermanentAdditionalFieldsFormForOrganizationalUnit(organizationalUnit.getResId());

    String invalidJson = "{\"myCustomProperty : \"some value\"}";

    Deposit deposit1 = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, "custom metadata");
    deposit1.setAdditionalFieldsValues(invalidJson);
    deposit1.setAdditionalFieldsFormId(additionalFieldsForm.getResId());

    try {
      this.depositClientService.create(deposit1);
      fail("Deposit has been saved successfully with invalid custom metadata. An Http error response should have be received");
    } catch (HttpClientErrorException e) {
      assertTrue(e.getResponseBodyAsString().contains("Validation failed"));
      assertTrue(e.getResponseBodyAsString().contains("customMetadata"));
    }
  }

  /**
   * Empty string for custom metadata is saved as a null value
   */
  @Test
  void customMetadataEmptyValueTest() {
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);
    final AdditionalFieldsForm additionalFieldsForm = this.orgUnitITService
            .getPermanentAdditionalFieldsFormForOrganizationalUnit(organizationalUnit.getResId());

    Deposit deposit1 = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, "custom metadata");
    deposit1.setAdditionalFieldsValues("");
    deposit1.setAdditionalFieldsFormId(additionalFieldsForm.getResId());

    Deposit deposit2 = this.depositClientService.create(deposit1);
    assertEquals(deposit1.getTitle(), deposit2.getTitle());
    assertNull(deposit2.getAdditionalFieldsValues());
  }

  @Test
  void tooLongTitleTest() {
    final int maxTitleLength = DB_DEFAULT_STRING_LENGTH;
    final String tooLongTitle = "X".repeat(maxTitleLength + 1);

    final Deposit depositWithOkTitle = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, tooLongTitle);
    // Truncate generated title, to keep the temporary label on the title
    depositWithOkTitle.setTitle(depositWithOkTitle.getTitle().substring(0, maxTitleLength));
    this.depositClientService.create(depositWithOkTitle);

    final Deposit depositWithTooLongTitle = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, tooLongTitle);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositClientService.create(depositWithTooLongTitle));
  }

  @Test
  void canListAllWithoutOrgUnitIdTest() {
    assertDoesNotThrow(() -> this.depositClientService.findAll());
  }

  @Test
  void listWithParamsTest() {
    final List<Deposit> depositList = this.depositClientService.findAll();
    // Check that organizational unit object has a value
    if (!depositList.isEmpty()) {
      assertNotNull(depositList.get(0).getOrganizationalUnit());
      assertEquals(depositList.get(0).getOrganizationalUnit().getResId(), depositList.get(0).getOrganizationalUnitId());
    }

    final List<Deposit> depositList2 = this.depositClientService.findAllWithLightVersion(false);
    // Check that organizational unit object has a value
    if (!depositList2.isEmpty()) {
      assertNotNull(depositList2.get(0).getOrganizationalUnit());
      assertEquals(depositList2.get(0).getOrganizationalUnit().getResId(), depositList2.get(0).getOrganizationalUnitId());
      assertEquals(depositList, depositList2);
    }

    final List<Deposit> depositList3 = this.depositClientService.findAllWithLightVersion(true);
    // Check that organizational unit object has a value
    if (!depositList3.isEmpty()) {
      assertNotNull(depositList3.get(0).getOrganizationalUnitId());
      assertNull(depositList3.get(0).getOrganizationalUnit());
    }

  }

  @Test
  void canListOnAnyAuthorizedOrgUnitsTest() {
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getTestOrganizationalUnitAsUser(
            DLCMTestConstants.PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN, Role.VISITOR_ID, false, false);
    assertDoesNotThrow(() -> this.depositClientService.findAllWithOrgunitId(organizationalUnit.getOrganizationalUnitId()));

    OrganizationalUnit organizationalUnit2 = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(
            DLCMTestConstants.PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN,
            NO_ONE, null, false);
    assertDoesNotThrow(() -> this.depositClientService.findAllWithOrgunitId(organizationalUnit2.getOrganizationalUnitId()));

  }

}

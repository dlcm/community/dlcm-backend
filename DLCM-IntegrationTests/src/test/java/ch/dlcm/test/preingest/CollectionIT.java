/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - CollectionIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class CollectionIT extends AbstractPreingestIT {

  private final PersonITService personITService;
  private AipITService aipITService;

  private final static int AIC_NUM = 1;
  private final static int AIU_NUM = 3;

  @Autowired
  public CollectionIT(Environment env, SudoRestClientTool restClientTool, OrgUnitITService orgUnitITService,
          DepositITService depositITService, DepositClientService depositClientService, PersonITService personITService,
          AipITService aipITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.personITService = personITService;
    this.aipITService = aipITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void CollectionCreationTest() {
    // Create a deposit object
    Deposit deposit = this.createDeposit(DLCMTestConstants.ARCHIVAL_COLLECTION_LABEL);
    // Create an AIC with AIU
    final List<ArchivalInfoPackage> aipList = this.depositITService.createAIC(deposit, AIU_NUM, false);
    assertEquals(AIU_NUM, aipList.size(), "Error in collection list");

    // Approve deposit
    final Deposit deposit1 = this.depositITService.approveDeposit(deposit);
    assertNotNull(deposit1, "Error deposit approval");

    // Check status
    assertTrue(this.depositITService.waitDepositIsCompleted(deposit1.getResId()), "Deposit not completed without error");
  }

  @Test
  void CollectionOfCollectionCreationTest() {
    // Create a deposit object
    Deposit deposit = this.createDeposit("Archival Collection of Collection");
    // Create an AIC with AIC
    final List<ArchivalInfoPackage> aipList = this.depositITService.createAIC(deposit, AIC_NUM, true);
    assertEquals(AIC_NUM, aipList.size(), "Error in collection list");

    // Approve deposit
    final Deposit deposit1 = this.depositITService.approveDeposit(deposit);
    assertNotNull(deposit1);

    // Check status
    assertTrue(this.depositITService.waitDepositIsCompleted(deposit1.getResId()), "Deposit not completed without error");
  }

  @Test
  void HybridCollectionCreationTest() {
    // Create a deposit object
    Deposit deposit = this.createDeposit("Archival Collection of Collection");
    // Create an AIC with AIC
    List<ArchivalInfoPackage> aipList = this.depositITService.createAIC(deposit, AIC_NUM, true);
    assertEquals(AIC_NUM, aipList.size(), "Error in collection list for adding collection");
    aipList = this.depositITService.createAIC(deposit, AIU_NUM, false);
    assertEquals(AIC_NUM + AIU_NUM, aipList.size(), "Error in collection list for adding units");

    // Approve deposit
    final Deposit deposit1 = this.depositITService.approveDeposit(deposit);
    assertNotNull(deposit1);

    // Check status
    assertTrue(this.depositITService.waitDepositIsCompleted(deposit1.getResId()), "Deposit not completed without error");
  }

  @Test
  void DuaCollectionCreationTest() {
    // Create a collection object
    Deposit collection = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, DLCMTestConstants.ARCHIVAL_COLLECTION_LABEL,
            DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    // set DUA Type fot the collection
    collection.setDataSensitivity(DataTag.GREEN);
    collection.setDataUsePolicy(DataUsePolicy.LICENSE);
    collection.setAccess(Access.RESTRICTED);
    collection = this.depositITService.createDeposit(collection);
    final Person person = this.personITService.getOrCreateRemotePerson();
    this.depositITService.addContributor(collection.getResId(), person.getResId());
    // add dua file
    collection.addItem(this.depositITService.createDuaFile(collection));
    final Deposit finalCollection = this.depositITService.createRemoteDepositWithData(collection);
    assertThrows(HttpClientErrorException.class, () -> this.depositITService.createAIC(finalCollection, AIC_NUM, true));
  }

  @Test
  void CollectionCreationWithAccessLevelMinorToArchiveTest() {
    // Create a collection object
    Deposit collection = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, DLCMTestConstants.ARCHIVAL_COLLECTION_LABEL,
            DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    // set Access level for collection to public which is minor to the archives that will contain
    collection.setAccess(Access.PUBLIC);
    collection = this.depositITService.createDeposit(collection);
    final Person person = this.personITService.getOrCreateRemotePerson();
    this.depositITService.addContributor(collection.getResId(), person.getResId());

    final Deposit finalCollection = this.depositITService.createRemoteDepositWithData(collection);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositITService.createAIC(finalCollection, AIC_NUM, true));
  }

  @Test
  void CollectionCreationWithSensibilityMinorToArchiveTest() {
    // Create a collection object
    Deposit collection = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, DLCMTestConstants.ARCHIVAL_COLLECTION_LABEL,
            DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    // set sensitivity for collection to blue which is minor to the archives(Yellow) that will contain
    collection.setDataSensitivity(DataTag.BLUE);
    collection = this.depositITService.createDeposit(collection);
    final Person person = this.personITService.getOrCreateRemotePerson();
    this.depositITService.addContributor(collection.getResId(), person.getResId());

    final Deposit finalCollection = this.depositITService.createRemoteDepositWithData(collection);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositITService.createAIC(finalCollection, AIC_NUM, true));
  }

  @Test
  void CollectionCreationWithDuaMinorToArchiveTest() {
    // Create a collection object
    Deposit collection = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, DLCMTestConstants.ARCHIVAL_COLLECTION_LABEL,
            DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    // set dua for collection to Licence which is minor to the archives(Click-through) that will contain
    collection.setDataUsePolicy(DataUsePolicy.LICENSE);

    collection = this.depositITService.createDeposit(collection);
    final Person person = this.personITService.getOrCreateRemotePerson();
    this.depositITService.addContributor(collection.getResId(), person.getResId());

    final Deposit finalCollection = this.depositITService.createRemoteDepositWithData(collection);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositITService.createAIC(finalCollection, AIC_NUM, true));
  }

  @Test
  void CollectionCreationWithArchiveWithEmbargoTest() {
    final Person person = this.personITService.getOrCreateRemotePerson();
    // Create an archive with embargo
    Deposit deposit = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, "Archive with embargo",
            DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    deposit.setEmbargoAccess(Access.RESTRICTED);
    deposit.setEmbargoMonths(5);
    final Deposit depositToAdd = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.addContributorToDeposit(depositToAdd.getResId(), person.getResId());
    this.depositITService.addDataFileToDeposit(depositToAdd.getResId(), this.depositITService.createDataFile(deposit));
    this.depositITService.waitDepositWithDataIsReady(depositToAdd.getResId());
    this.depositITService.approveDeposit(depositToAdd);
    this.depositITService.waitDepositIsCompleted(depositToAdd.getResId());
    this.aipITService.waitAIPIsReady(depositToAdd.getOrganizationalUnitId(), depositToAdd.getTitle());

    final List<ArchivalInfoPackage> aipList = this.aipITService.getAipClientService().searchByProperties(Map.of("info.name", deposit.getTitle()));
    assertFalse(aipList.isEmpty());

    // Create a collection object with deposit
    Deposit collection = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, DLCMTestConstants.ARCHIVAL_COLLECTION_LABEL,
            DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    collection.setAccess(Access.PUBLIC);
    collection = this.depositITService.createDeposit(collection);
    this.depositITService.addContributor(collection.getResId(), person.getResId());

    final Deposit finalCollection = this.depositITService.createRemoteDepositWithData(collection);

    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositITService.addArchive(finalCollection.getResId(), aipList.get(0).getResId()));
  }

  @Test
  void CollectionCreationWithEmbargoMinorToArchiveTest() {
    final Person person = this.personITService.getOrCreateRemotePerson();
    // Create an archive with embargo
    Deposit deposit = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, "Archive with embargo",
            DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    deposit.setEmbargoAccess(Access.RESTRICTED);
    deposit.setEmbargoMonths(5);
    final Deposit depositToAdd = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.addContributorToDeposit(depositToAdd.getResId(), person.getResId());
    this.depositITService.addDataFileToDeposit(depositToAdd.getResId(), this.depositITService.createDataFile(deposit));
    this.depositITService.waitDepositWithDataIsReady(depositToAdd.getResId());
    this.depositITService.approveDeposit(depositToAdd);
    this.depositITService.waitDepositIsCompleted(depositToAdd.getResId());
    this.aipITService.waitAIPIsReady(depositToAdd.getOrganizationalUnitId(), depositToAdd.getTitle());

    final List<ArchivalInfoPackage> aipList = this.aipITService.getAipClientService().searchByProperties(Map.of("info.name", deposit.getTitle()));
    assertFalse(aipList.isEmpty());

    // Create a collection object with deposit
    Deposit collection = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, DLCMTestConstants.ARCHIVAL_COLLECTION_LABEL,
            DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    collection.setAccess(Access.PUBLIC);
    collection.setEmbargoAccess(Access.RESTRICTED);
    collection.setEmbargoMonths(2);
    collection = this.depositITService.createDeposit(collection);
    this.depositITService.addContributor(collection.getResId(), person.getResId());

    final Deposit finalCollection = this.depositITService.createRemoteDepositWithData(collection);

    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositITService.addArchive(finalCollection.getResId(), aipList.get(0).getResId()));
  }

  private Deposit createDeposit(String title) {
    Deposit deposit = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, title, DEFAULT_DESCRIPTION,
            Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    // set DUA Type fot the collection
    deposit.setDataSensitivity(DataTag.YELLOW);
    deposit.setDataUsePolicy(DataUsePolicy.CLICK_THROUGH_DUA);
    deposit.setAccess(Access.RESTRICTED);
    deposit = this.depositITService.createDeposit(deposit);
    final Person person = this.personITService.getOrCreateRemotePerson();
    this.depositITService.addContributor(deposit.getResId(), person.getResId());
    // add dua file
    deposit.addItem(this.depositITService.createDuaFile(deposit));
    deposit = this.depositITService.createRemoteDepositWithData(deposit);
    return deposit;
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.depositITService.cleanTestData();
    this.restClientTool.exitSudo();
  }
}

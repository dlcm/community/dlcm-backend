/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositFileNumberAndSizeIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.security.Role;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.MetadataITService;
import ch.dlcm.test.service.OrgUnitITService;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DepositWithAdministrativeDataIT extends AbstractDepositWithRoleIT {

  private static final String YARETA_DATA = "deposit/dlcm/Yareta-small.zip";
  private static final int YARETA_FILE_NUM = 3;

  private AipITService aipITService;
  private MetadataITService metadataITService;

  @Autowired
  public DepositWithAdministrativeDataIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService,
          AipITService aipITService,
          MetadataITService metadataITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.aipITService = aipITService;
    this.metadataITService = metadataITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Order(10)
  @Test
  void testDepositWithAdministrativeDataFile() {
    final Deposit deposit = this.depositITService.createRemoteDepositWithContributor(PersistenceMode.TEMPORARY,
            "Deposit with administrative data", DEFAULT_DESCRIPTION);
    final String depositId = deposit.getResId();
    // Add data file
    DepositDataFile df = this.depositITService.createDataFile(deposit);
    df.setDataCategory(DataCategory.Administrative);
    df.setDataType(DataCategory.Document);
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.depositITService.addDataFileToDeposit(depositId, df));
    // Upload data file
    FileUploadDto<DepositDataFile> fileInfo = new FileUploadDto<>();
    fileInfo.setDataCategory(DataCategory.Administrative);
    fileInfo.setDataType(DataCategory.Document);
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.depositITService.uploadDataFileToDeposit(depositId, new ClassPathResource(YARETA_DATA), fileInfo));
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.depositITService.uploadArchiveToDeposit(depositId, new ClassPathResource(YARETA_DATA), fileInfo));
  }

  @Order(20)
  @Test
  void testDepositWithoutResearchDataFile() {
    final Deposit deposit = this.createDeposit(DLCMMetadataVersion.getDefaultVersion(), "Deposit without research data", DataCategory.Secondary,
            DataCategory.DataPaper, false);
    assertEquals(DepositStatus.IN_ERROR, deposit.getStatus());
    assertTrue(deposit.getStatusMessage().contains("Missing research data"));
  }

  @Order(50)
  @ParameterizedTest
  @MethodSource("supportedVersions")
  @Disabled("To run when administrative data is enable")
  void testDepositCreationWithAdministrativeDataByVersion(DLCMMetadataVersion version) {
    // Create
    final Deposit deposit = this.createDeposit(version, "Deposit with administrative data (version" + version + ")", DataCategory.Administrative,
            DataCategory.Document, true);
    final ArchivalInfoPackage aip = this.getAIP(deposit);
    this.checkAip(aip, YARETA_FILE_NUM + 1, 0, 0, 0);
    // Archive metadata
    final ArchiveMetadata archiveMetadata = this.metadataITService.getArchiveMetadata(aip.getResId());
    this.checkMetadata(archiveMetadata, YARETA_FILE_NUM, YARETA_FILE_NUM + 1, 1, aip.getArchiveSize());
  }

  @Order(60)
  @ParameterizedTest
  @MethodSource("unsupportedVersions")
  @Disabled("To run when administrative data is enable")
  void createDepositWithWrongDataSensitivity(DLCMMetadataVersion version) {
    final String title = "Deposit with administrative data & wrong version";
    final Deposit deposit = this.createDeposit(version, title);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositClientService.create(deposit));
  }

  @Override
  protected String role() {
    return Role.CREATOR_ID;
  }

  private Deposit createDeposit(DLCMMetadataVersion version, String title) {
    final Deposit deposit = this.createLocalDepositForCurrentRole(title);
    deposit.setMetadataVersion(version);
    return deposit;
  }

  private ArchivalInfoPackage getAIP(Deposit deposit) {
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
    final String aipId = this.metadataITService.getAipIdFromDepositId(deposit.getResId());
    return this.aipITService.findAIP(aipId);
  }

  private Deposit createDeposit(DLCMMetadataVersion version, String title, DataCategory dataCategory, DataCategory dataType,
          boolean testResult) {
    final Deposit deposit = this.depositITService.createRemoteDepositWithContributor(PersistenceMode.TEMPORARY, title + " ",
            DEFAULT_DESCRIPTION);
    final String depositId = deposit.getResId();
    deposit.setMetadataVersion(version);
    this.depositITService.updateDeposit(depositId, deposit);
    FileUploadDto<DepositDataFile> fileInfo = new FileUploadDto<>();
    fileInfo.setDataCategory(dataCategory);
    fileInfo.setDataType(dataType);
    this.depositITService.uploadArchiveToDeposit(depositId, new ClassPathResource(YARETA_DATA), fileInfo);
    this.depositITService.waitDepositWithDataIsReady(depositId);
    this.depositITService.approveDeposit(deposit);
    return this.waitDepositIsCompleted(deposit, testResult);
  }

  private Deposit waitDepositIsCompleted(Deposit deposit, boolean testResult) {
    final boolean result = this.depositITService.waitDepositIsCompleted(deposit.getResId());
    if (testResult) {
      assertTrue(result, "Deposit not completed: " + deposit.getResId());
    }
    return this.depositITService.findDeposit(deposit.getResId());
  }

  private static Stream<Arguments> supportedVersions() {
    return Stream.of(
            arguments(DLCMMetadataVersion.V3_1),
            arguments(DLCMMetadataVersion.V4_0));

  }

  private static Stream<Arguments> unsupportedVersions() {
    return Stream.of(
            arguments(DLCMMetadataVersion.V1_0),
            arguments(DLCMMetadataVersion.V1_1),
            arguments(DLCMMetadataVersion.V2_0),
            arguments(DLCMMetadataVersion.V2_1),
            arguments(DLCMMetadataVersion.V3_0));
  }

}

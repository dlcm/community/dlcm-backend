/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractDepositAsCreatorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

abstract class AbstractDepositAsCreatorIT extends AbstractDepositWithRoleIT {

  protected PersonITService personITService;
  protected DepositClientService depositClientService;

  protected AbstractDepositAsCreatorIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitService,
          DepositITService depositService,
          DepositClientService depositClientService,
          PersonITService personITService) {
    super(env, restClientTool, orgUnitService, depositService, depositClientService);
    this.personITService = personITService;
    this.depositClientService = depositClientService;
  }

  @Test
  void findAllTest() {
    final OrganizationalUnit organizationalUnit = this.getPermanentOrganizationalUnitWithRole();

    // Create deposit 1
    final String title1 = "deposit created by findAllTest";
    final Deposit deposit1 = this.createLocalDepositForCurrentRole(title1);
    this.depositClientService.create(deposit1);
    final String tmpTitle1 = deposit1.getTitle();

    // Create deposit 2
    final String title2 = "deposit created by findAllTest";
    final Deposit deposit2 = this.createLocalDepositForCurrentRole(title2);
    this.depositClientService.create(deposit2);
    final String tmpTitle2 = deposit2.getTitle();

    // Check that the deposits have been created
    List<Deposit> deposits = this.depositClientService
            .searchByProperties(Map.of(DLCMConstants.ORG_UNIT_ID_FIELD, organizationalUnit.getResId()));
    assertTrue(deposits.stream().anyMatch(deposit -> deposit.getTitle().equals(tmpTitle1)));
    assertTrue(deposits.stream().anyMatch(deposit -> deposit.getTitle().equals(tmpTitle2)));

    // Filter on title
    deposits = this.depositClientService
            .searchByProperties(Map.of("title", tmpTitle1, DLCMConstants.ORG_UNIT_ID_FIELD, organizationalUnit.getResId()));
    assertTrue(deposits.stream().anyMatch(deposit -> deposit.getTitle().equals(tmpTitle1)));
    assertFalse(deposits.stream().anyMatch(deposit -> deposit.getTitle().equals(tmpTitle2)));

    // Filter on title + statusList=IN_PROGRESS,COMPLETED
    deposits = this.depositClientService
            .searchByProperties(Map.of("title", tmpTitle1, "statusList", "IN_PROGRESS,COMPLETED", DLCMConstants.ORG_UNIT_ID_FIELD,
                    organizationalUnit.getResId()));
    assertTrue(deposits.stream().anyMatch(deposit -> deposit.getTitle().equals(tmpTitle1)));
    assertFalse(deposits.stream().anyMatch(deposit -> deposit.getTitle().equals(tmpTitle2)));
  }

  @Test
  void creationTest() {
    final OrganizationalUnit organizationalUnit = this.getPermanentOrganizationalUnitWithRole();
    final String title = "deposit created by test";

    // Create a deposit
    final Deposit deposit1 = this.createLocalDepositForCurrentRole(title);
    final Deposit deposit2 = this.depositClientService.create(deposit1);

    // Test the creation
    assertEquals(deposit1.getTitle(), deposit2.getTitle());

    // Check if pending ( InProgress, inValidation, inError )
    assertEquals(Deposit.DepositStatus.IN_PROGRESS, deposit2.getStatus());

    final Deposit deposit3 = this.depositClientService.findOne(deposit2.getResId());

    assertEquals(deposit1.getTitle(), deposit3.getTitle());

    // Check that the deposit has been created
    final List<Deposit> deposits = this.depositClientService.searchByProperties(Map.of("title", deposit1.getTitle(),
            DLCMConstants.ORG_UNIT_ID_FIELD, organizationalUnit.getResId()));
    assertEquals(1, deposits.size());
  }

  @Test
  void creationWithExistingResIdTest() {
    final OrganizationalUnit organizationalUnit = this.getPermanentOrganizationalUnitWithRole();

    final String title1 = "creationWithExistingResIdTest";
    final String title2 = "should not be saved";

    final Deposit deposit = this.createRemoteDepositForCurrentRole(title1);
    final String existingResId = deposit.getResId();

    final Deposit newDeposit = this.createLocalDepositForCurrentRole(title2);
    newDeposit.setResId(existingResId);

    Deposit savedDeposit = null;
    try {
      savedDeposit = this.depositClientService.create(newDeposit);
      fail("An 500 Http response should be received");
    } catch (final HttpServerErrorException ex) {
      assertEquals(500, ex.getRawStatusCode());
    }

    assertNull(savedDeposit);
    deposit.setCollectionBegin(OffsetDateTime.now(ZoneOffset.UTC).minusYears(5));

    // Check that the deposit has been created
    final List<Deposit> deposits = this.depositClientService.searchByProperties(Map.of("title", deposit.getTitle(),
            DLCMConstants.ORG_UNIT_ID_FIELD, organizationalUnit.getResId()));
    assertEquals(1, deposits.size());

    // Check that the deposit has been created
    final List<Deposit> deposits2 = this.depositClientService.searchByProperties(Map.of("title", newDeposit.getTitle(),
            DLCMConstants.ORG_UNIT_ID_FIELD, organizationalUnit.getResId()));
    assertEquals(0, deposits2.size());
  }

  @Test
  void contributorTest() {
    final Deposit deposit = this.createRemoteDepositForCurrentRole();

    // Create a new Person
    final Person person1 = this.personITService.getOrCreateRemotePerson();

    // Add the person as contributor
    this.depositClientService.addContributor(deposit.getResId(), person1.getResId());
    final List<Person> listContributors = this.depositClientService.getContributors(deposit.getResId());

    assertEquals(1, listContributors.size());
    final Person person2 = listContributors.get(0);

    // Test the contributor
    assertEquals(person1.getResId(), person2.getResId());
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
  }

  @Test
  void contributorWithOrcidTest() {
    final Deposit deposit = this.createRemoteDepositForCurrentRole();

    // Create a new Person
    final Person person1 = this.personITService.getOrCreateRemotePerson();

    // Add the person as contributor
    this.depositClientService.addContributor(deposit.getResId(), person1.getResId());
    List<Person> listContributors = this.depositClientService.getContributors(deposit.getResId());

    assertEquals(1, listContributors.size());
    Person fetchedPerson1 = listContributors.get(0);

    // Test the contributor
    assertEquals(person1.getResId(), fetchedPerson1.getResId());
    assertEquals(person1.getFirstName(), fetchedPerson1.getFirstName());
    assertEquals(person1.getLastName(), fetchedPerson1.getLastName());

    // Create and add a new Person with ORCID
    final Person person2 = this.personITService.getOrCreateRemotePersonWithOrcid();
    this.depositClientService.addContributor(deposit.getResId(), person2.getResId());
    listContributors = this.depositClientService.getContributors(deposit.getResId());

    assertEquals(2, listContributors.size());
    fetchedPerson1 = listContributors.get(0);
    final Person fetchedPerson2 = listContributors.get(1);

    // Test the contributors
    assertEquals(person1.getResId(), fetchedPerson1.getResId());
    assertEquals(person1.getFirstName(), fetchedPerson1.getFirstName());
    assertEquals(person1.getLastName(), fetchedPerson1.getLastName());
    assertNull(person1.getOrcid());
    assertEquals(person2.getResId(), fetchedPerson2.getResId());
    assertEquals(person2.getFirstName(), fetchedPerson2.getFirstName());
    assertEquals(person2.getLastName(), fetchedPerson2.getLastName());
    assertNotNull(fetchedPerson2.getOrcid());
    assertEquals(person2.getOrcid(), fetchedPerson2.getOrcid());
  }

  @Test
  void creationWithResIdTest() {
    final Deposit deposit = this.createLocalDepositForCurrentRole();
    final String resId = StringTool.generateResId();
    deposit.setResId(resId);
    final Deposit savedDeposit = this.depositClientService.create(deposit);

    assertNotNull(savedDeposit);
    assertEquals(savedDeposit.getResId(), resId);
  }

  @Test
  void deleteTest() {
    Deposit deposit = this.createRemoteDepositForCurrentRole();

    // Delete the deposit
    final String resId = deposit.getResId();
    this.depositClientService.delete(resId);
    // Wait one second to let asynchronous deletion complete before to check
    SolidifyTime.waitOneSecond();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.depositClientService.findOne(resId));
  }

  @Test
  void deleteWithContributorTest() {
    Deposit deposit = this.createRemoteDepositForCurrentRole();

    // Create and add a new Person
    final Person person1 = this.personITService.getOrCreateRemotePerson();
    this.depositClientService.addContributor(deposit.getResId(), person1.getResId());
    List<Person> listContributors = this.depositClientService.getContributors(deposit.getResId());
    assertEquals(1, listContributors.size());
    Person fetchedPerson1 = listContributors.get(0);
    assertEquals(person1.getResId(), fetchedPerson1.getResId());

    // Delete the deposit
    final String resId = deposit.getResId();
    this.depositClientService.delete(resId);
    // Wait one second to let asynchronous deletion complete before to check
    SolidifyTime.waitOneSecond();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.depositClientService.findOne(resId));
  }

  @Test
  void deleteWithDeletedContributorTest() {
    Deposit deposit = this.createRemoteDepositForCurrentRole();

    // Create and add a new Person
    String firstName = DLCMTestConstants.getRandomNameWithTemporaryLabel("John");
    String lastName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Doe");
    final Person person1 = this.personITService.getOrCreateRemotePerson(firstName, lastName, null);
    final String personId = person1.getResId();
    this.depositClientService.addContributor(deposit.getResId(), personId);
    List<Person> listContributors = this.depositClientService.getContributors(deposit.getResId());
    assertEquals(1, listContributors.size());
    Person fetchedPerson1 = listContributors.get(0);
    assertEquals(personId, fetchedPerson1.getResId());

    // Check Person exists
    assertNotNull(this.personITService.findOne(personId));

    // Delete the person
    this.restClientTool.sudoAdmin();
    this.personITService.delete(personId);
    this.restClientTool.exitSudo();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.personITService.findOne(personId));

    // Delete the deposit
    final String resId = deposit.getResId();
    this.depositClientService.delete(resId);
    // Wait one second to let asynchronous deletion complete before to check
    SolidifyTime.waitOneSecond();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.depositClientService.findOne(resId));
  }

  @Test
  void deleteWithDeletedContributorWithOrcidTest() {
    Deposit deposit = this.createRemoteDepositForCurrentRole();

    // Create and add a new Person
    String firstName = DLCMTestConstants.getRandomNameWithTemporaryLabel("John");
    String lastName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Doe");
    String orcid = "1234-1234-1234-4567";
    final Person person1 = this.personITService.getOrCreateRemotePerson(firstName, lastName, orcid);
    final String personId = person1.getResId();
    this.depositClientService.addContributor(deposit.getResId(), personId);
    List<Person> listContributors = this.depositClientService.getContributors(deposit.getResId());
    assertEquals(1, listContributors.size());
    Person fetchedPerson1 = listContributors.get(0);
    assertEquals(personId, fetchedPerson1.getResId());

    // Check Person exists
    Person foundPerson1 = this.personITService.findOne(personId);
    assertNotNull(foundPerson1);
    assertEquals(orcid, foundPerson1.getOrcid());

    // Delete the person
    this.restClientTool.sudoAdmin();
    this.personITService.delete(personId);
    this.restClientTool.exitSudo();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.personITService.findOne(personId));

    // Delete the deposit
    final String resId = deposit.getResId();
    this.depositClientService.delete(resId);
    // Wait one second to let asynchronous deletion complete before to check
    SolidifyTime.waitOneSecond();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.depositClientService.findOne(resId));
  }

  @Test
  void dontUpdateResIdTest() {
    final Deposit deposit = this.createRemoteDepositForCurrentRole();
    final String currentResId = deposit.getResId();

    final String newTitle = DLCMTestConstants.getRandomNameWithTemporaryLabel("B");
    final String newResId = "xyz-xyz-xyz";

    // Update the deposit with a new resId
    deposit.setTitle(newTitle);
    deposit.setResId(newResId);
    final Deposit savedDeposit = this.depositClientService.update(currentResId, deposit);

    assertNotNull(savedDeposit);
    assertEquals(savedDeposit.getResId(), currentResId); // resId has not changed
    assertEquals(savedDeposit.getTitle(), newTitle); // title has changed
  }

  /**
   * Updating the deposit status with an update is forbidden
   */
  @Test
  void dontUpdateStatusTest() {
    assertThrows(HttpClientErrorException.class, () -> {
      final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

      deposit.setStatus(Deposit.DepositStatus.IN_VALIDATION);
      this.depositClientService.update(deposit.getResId(), deposit);
    });
  }

  @Test
  void reserveDOITest() {
    final Deposit deposit = this.createRemoteDepositForCurrentRole();
    final String depositId = deposit.getResId();

    final String newDOI = this.depositClientService.reserveDOI(depositId);

    /*
     * Fetch again Deposit
     */
    final Deposit fetchedDeposit = this.depositClientService.findOne(depositId);
    assertNotNull(fetchedDeposit.getDoi());
    assertEquals(newDOI, fetchedDeposit.getDoi());
  }

  @Test
  void updateTest() {
    final Deposit deposit = this.createRemoteDepositForCurrentRole();

    // Update the deposit
    deposit.setTitle(DLCMTestConstants.getRandomNameWithTemporaryLabel("B"));
    final Deposit actualDeposit = this.depositClientService.update(deposit.getResId(), deposit);
    final Deposit deposit4 = this.depositClientService.findOne(deposit.getResId());

    // Test the update
    assertEquals(deposit.getTitle(), deposit4.getTitle());
    this.assertEqualsWithoutNanoSeconds(deposit.getCreationTime(), deposit4.getCreationTime());
    assertNotNull(actualDeposit);
  }

  @Test
  void cannotListAllWithoutOrgUnitIdTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.depositClientService.findAll());
  }

  @Test
  void cannotListInNonAuthorizedOrgUnitsTest() {
    this.restClientTool.sudoAdmin();
    OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(
            DLCMTestConstants.PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN,
            NO_ONE, null, false);
    this.restClientTool.exitSudo();
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.depositClientService.findAllWithOrgunitId(organizationalUnit.getOrganizationalUnitId()));
  }
}

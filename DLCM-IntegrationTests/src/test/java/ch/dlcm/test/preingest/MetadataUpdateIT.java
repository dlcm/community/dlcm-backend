/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - MetadataUpdateIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.model.dto.CitationDto;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SudoRestClientTool;
import ch.unige.solidify.util.XMLTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.EmbargoInfo;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrderITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class MetadataUpdateIT extends AbstractMetadataEditionIT {

  @Autowired
  public MetadataUpdateIT(Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService,
          AipITService aipITService,
          MetadataClientService metadataClientService,
          OrderITService orderITService,
          PersonITService personITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService, aipITService, metadataClientService, orderITService,
            personITService);
  }

  @Override
  protected void deleteFixtures() {
    super.deleteFixtures();
    this.restClientTool.sudoRoot();
    this.personITService.clearPeopleFixtures();
    this.restClientTool.exitSudo();
  }

  @Test
  void cannotUpdateCompletedDepositTest() {
    Deposit deposit = this.createDepositWithCompletedAip();
    deposit.setTitle(DLCMTestConstants.getRandomNameWithTemporaryLabel("Deposit v1"));
    deposit.setDescription("Deposit corrected (version 1)");

    String depositId = deposit.getResId();
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositClientService.update(depositId, deposit));
  }

  @Test
  void updateEditingMetadataDepositTest() {
    Deposit deposit = this.createDepositWithCompletedAip();
    deposit = this.depositITService.startEditingMetadata(deposit);
    String title = DLCMTestConstants.getRandomNameWithTemporaryLabel("Deposit v1");
    deposit.setTitle(title);
    deposit.setDescription("Deposit corrected (version 1)");

    this.depositClientService.update(deposit.getResId(), deposit);
    Map<String, String> properties = new HashMap<>();
    properties.put("title", title);
    properties.put("organizationalUnitId", deposit.getOrganizationalUnitId());
    Deposit depositChanged = this.depositClientService.searchByProperties(properties).get(0);

    assertEquals(deposit.getTitle(), depositChanged.getTitle());
    assertEquals(deposit.getDescription(), depositChanged.getDescription());
  }

  @Test
  void updateAndValidateArchive() throws IOException, ParserConfigurationException, SAXException {
    this.restClientTool.sudoAdmin();
    Deposit deposit = this.depositClientService.searchByProperties(Map.of("title", DEFAULT_TITLE_TO_UPDATE)).get(0);
    assertTrue(deposit.getStatus() == DepositStatus.COMPLETED
            || deposit.getStatus() == DepositStatus.EDITING_METADATA);
    if (deposit.getStatus() == DepositStatus.COMPLETED) {
      deposit = this.depositITService.startEditingMetadata(deposit);
    }
    final String newTitle = this.getNewArchiveToUpdateTitle(deposit.getTitle());
    deposit.setTitle(newTitle);

    this.depositClientService.update(deposit.getResId(), deposit);
    this.depositITService.approveDeposit(deposit);
    this.depositITService.waitDepositIsCompleted(deposit.getResId());
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), newTitle);

    // Check that the AIP has the new title
    final List<ArchivalInfoPackage> aipsList = this.aipITService.getAipClientService().searchByProperties(Map.of("info.name", newTitle));
    assertEquals(1, aipsList.size());

    // check if order has been already created
    final ArchivalInfoPackage aip = aipsList.get(0);
    String aipId = aip.getResId();
    String orderId = this.metadataClientService.prepareDownload(aipId, DLCMConstants.DISSEMINATION_POLICY_OAIS_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    // Working folders
    final Path packagedArchivePath = Paths.get(System.getProperty("java.io.tmpdir"), "archiveToUpdate.tmp");
    final Path extractedArchivePath = Paths.get(System.getProperty("java.io.tmpdir"), "archiveToUpdate");
    // Clean files if exists
    if (FileTool.checkFile(packagedArchivePath)) {
      assertDoesNotThrow(() -> FileTool.deleteFile(packagedArchivePath));
    }
    if (FileTool.checkFile(extractedArchivePath)) {
      assertDoesNotThrow(() -> FileTool.deleteFolder(extractedArchivePath));
    }

    // Download updated archive from Access module
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);
    this.metadataClientService.downloadArchive(aipId, packagedArchivePath, orderId);
    assertNotNull(packagedArchivePath.toFile());

    // Extract archive
    final ZipTool zipAip = new ZipTool(packagedArchivePath.toString());
    if (aip.getArchiveContainer() == ArchiveContainer.ZIP) {
      assertTrue(zipAip.unzipFiles(extractedArchivePath, true));
    } else if (aip.getArchiveContainer() == ArchiveContainer.BAG_IT) {
      assertTrue(zipAip.unzipFolder(extractedArchivePath, DLCMConstants.DATA_FOLDER + "/", true));
    }

    // Check that the dlcm.xml file in the archive contains the new updated title
    final Path dlcmXmlPath = extractedArchivePath.resolve(aipId).resolve(DLCMConstants.METADATA_FILE);
    final Document dlcmDoc = XMLTool.parseXML(FileTool.toString(dlcmXmlPath));
    final String titleFromArchive = XMLTool.getFirstTextContent(dlcmDoc, "//datacite:titles/datacite:title",
            Map.of("datacite", DLCMConstants.DATACITE_NAMESPACE_4));
    assertEquals(newTitle, titleFromArchive);

    // Check that the number of update metadata files matches the number of updates
    final int numberOfUpdates = this.getNumberBetweenSquareBrackets(newTitle);
    final Path folderPath = extractedArchivePath.resolve(aipId);
    final long filesNumber = FileTool.findFiles(folderPath).stream()
            .filter(path -> path.getFileName().toString().contains(DLCMConstants.REPRESENTATION_INFO_METADATA_FILE_PREFIX)).count();
    assertEquals(numberOfUpdates, filesNumber);

    // Clean files
    assertDoesNotThrow(() -> FileTool.deleteFile(packagedArchivePath));
    assertDoesNotThrow(() -> FileTool.deleteFolder(extractedArchivePath));

    // Check that citations have been modified as well
    List<CitationDto> citations = this.metadataClientService.getBibliographies(aipId);
    assertNotNull(citations);
    if (!citations.isEmpty()) {
      CitationDto citationDto = citations.get(0);
      assertTrue(citationDto.getText().contains(newTitle), "'" + citationDto.getText() + "' does not contain '" + newTitle + "'");
    }
  }

  @Test
  void cannotUpdateDepositInErrorTest() {
    Deposit deposit = this.createDepositWithCompletedAip();

    this.restClientTool.sudoRoot();
    this.depositClientService.putDepositInError(deposit.getResId());
    this.restClientTool.exitSudo();

    String depositId = deposit.getResId();
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositClientService.update(depositId, deposit));
  }

  @Test
  void cannotStartEditingDepositInErrorTest() {
    this.restClientTool.sudoRoot();
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, TEMP_DEPOSIT_TITLE, DEFAULT_DESCRIPTION);
    this.depositClientService.putDepositInError(deposit.getResId());
    Deposit depositInError = this.depositClientService.findOne(deposit.getResId());
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.depositITService.startEditingMetadata(depositInError));
  }

  @Test
  void cannotStartEditingDepositInProgressTest() {
    this.restClientTool.sudoAdmin();
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, TEMP_DEPOSIT_TITLE, DEFAULT_DESCRIPTION);
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositITService.startEditingMetadata(deposit));
  }

  @Test
  void updateFromDataciteTest() {
    Deposit deposit = this.createDepositWithCompletedAip();

    Deposit depositEditing = this.depositITService.startEditingMetadata(deposit);

    // upload data from a datacite xml file
    this.depositClientService.uploadMetadataFile(depositEditing.getResId(), new ClassPathResource("datacite.xml"));

    this.depositITService.waitDepositWithDataIsReady(depositEditing.getResId());
    this.restClientTool.sudoAdmin();
    this.depositITService.approveDeposit(depositEditing);
    this.depositITService.waitDepositIsCompleted(depositEditing.getResId());

    Deposit depositUploaded = this.depositClientService
            .searchByProperties(Map.of("title", TEMP_DEPOSIT_WITH_COMPLETED_AIP_TITLE)).get(0);
    this.restClientTool.exitSudo();
    // title is not updated from datacite file content (Deposit properties have precedence over datacite file)
    assertEquals(deposit.getTitle(), depositUploaded.getTitle());
    assertEquals(DepositStatus.COMPLETED, depositUploaded.getStatus());
    assertEquals(deposit.getDataFileNumber() + 1, depositUploaded.getDataFileNumber());
  }

  @Test
  void updateFromDataciteThenDepositTest() {
    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN, Role.MANAGER_ID, AuthApplicationRole.USER_ID, false);
    Deposit deposit = this.createDepositWithCompletedAip();

    Deposit depositEditing = this.depositITService.startEditingMetadata(deposit);

    // upload data from a datacite xml file and wait until the file is ready
    this.depositClientService.uploadMetadataFile(depositEditing.getResId(), new ClassPathResource("datacite.xml"));
    this.depositITService.waitDepositWithDataIsReady(depositEditing.getResId());
    this.restClientTool.sudoAdmin();
    this.depositITService.approveDeposit(depositEditing);
    this.depositITService.waitDepositIsCompleted(deposit.getResId());
    this.aipITService.waitAIPIsReady(orgUnit.getResId(), deposit.getTitle());
    this.restClientTool.exitSudo();

    Deposit depositEditing2 = this.depositITService.startEditingMetadata(depositEditing);

    depositEditing2.setTitle(DLCMTestConstants.getRandomNameWithTemporaryLabel("Deposit title changed"));
    depositEditing2.setDescription("Deposit title changed (version 1)");

    Deposit depositChanged = this.depositClientService.update(depositEditing2.getResId(), depositEditing2);

    this.restClientTool.sudoAdmin();
    this.depositITService.approveDeposit(depositChanged);
    this.depositITService.waitDepositIsCompleted(depositChanged.getResId());

    Deposit depositUploaded2 = this.depositClientService.searchByProperties(Map.of("title", "Deposit title changed")).get(0);
    this.restClientTool.exitSudo();

    assertEquals(depositEditing2.getTitle(), depositUploaded2.getTitle());
    assertEquals(depositEditing2.getDescription(), depositUploaded2.getDescription());
  }

  @Test
  void updateDepositKeywordsTest() {
    Deposit deposit = this.createDepositWithCompletedAip();
    deposit = this.depositITService.startEditingMetadata(deposit);
    deposit.setKeywords(Arrays.asList("Science", "Computer Science"));

    this.depositClientService.update(deposit.getResId(), deposit);
    Map<String, String> properties = new HashMap<>();
    properties.put("title", TEMP_DEPOSIT_WITH_COMPLETED_AIP_TITLE);
    properties.put("organizationalUnitId", deposit.getOrganizationalUnitId());
    Deposit depositChanged = this.depositClientService.searchByProperties(properties).get(0);

    assertEquals(deposit.getTitle(), depositChanged.getTitle());
    assertEquals(deposit.getKeywords(), depositChanged.getKeywords());
  }

  @Test
  void updateDepositAccessLevelTest() {
    Deposit deposit = this.createDepositWithCompletedAip();
    deposit = this.depositITService.startEditingMetadata(deposit);
    deposit.setAccess(Access.CLOSED);

    this.depositClientService.update(deposit.getResId(), deposit);
    Deposit depositChanged = this.depositClientService.findOne(deposit.getResId());

    assertEquals(deposit.getTitle(), depositChanged.getTitle());
    assertEquals(deposit.getAccess(), depositChanged.getAccess());
  }

  @Test
  void updateDepositEmbargoTest() {
    Deposit deposit = this.createDepositWithCompletedAip();
    deposit = this.depositITService.startEditingMetadata(deposit);
    EmbargoInfo embargo = new EmbargoInfo();
    embargo.setAccess(Access.CLOSED);
    embargo.setMonths(10);
    deposit.setEmbargo(embargo);

    this.depositClientService.update(deposit.getResId(), deposit);
    Deposit depositChanged = this.depositClientService.findOne(deposit.getResId());

    assertEquals(deposit.getEmbargo(), depositChanged.getEmbargo());
    assertEquals(deposit.getEmbargoAccess(), depositChanged.getEmbargoAccess());
    assertEquals(deposit.getEmbargoMonths(), depositChanged.getEmbargoMonths());
  }

  @Test
  void cannotUpdateFromCorruptedDataciteTest() {
    Deposit deposit = this.createDepositWithCompletedAip();

    Deposit depositEditing = this.depositITService.startEditingMetadata(deposit);

    // upload corrupted datacite xml file
    this.depositClientService.uploadMetadataFile(depositEditing.getResId(), new ClassPathResource("datacite-corrupted.xml"));

    Map<String, String> properties = new HashMap<>();
    properties.put("title", TEMP_DEPOSIT_WITH_COMPLETED_AIP_TITLE);
    properties.put("organizationalUnitId", deposit.getOrganizationalUnitId());
    Deposit deposit1 = this.depositClientService.searchByProperties(properties).get(0);

    // Approving the deposit fails as the corrupted Datacite datafile is in error
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.depositITService.approveDeposit(deposit1));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    this.assertFieldContainsValidationErrorMessage(e, "status", "Data files of deposit are not ready or not cleaned");
  }

  @Test
  void cannotUpdateFromEmptyDataciteTest() {
    Deposit deposit = this.createDepositWithCompletedAip();

    Deposit depositEditing = this.depositITService.startEditingMetadata(deposit);

    String depositId = depositEditing.getResId();

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
            () -> this.depositClientService.uploadMetadataFile(depositId, null));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertNotNull(e.getMessage());
    assertTrue(e.getMessage().contains("Required part 'file' is not present"));
  }

  @Test
  void cancelMetadataEditingTest() {
    this.testCancelMetadataEditing(false);
  }

  @Test
  void cancelMetadataEditingWithDeletedContributorsTest() {
    this.testCancelMetadataEditing(true);
  }

  private void testCancelMetadataEditing(boolean withDeletedContributors) {
    Deposit deposit = this.createDepositWithCompletedAip();
    final String depositId = deposit.getResId();
    final String originalTitle = deposit.getTitle();
    final String originalDescription = deposit.getDescription();
    this.depositITService.waitDepositIsCompleted(depositId);
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), originalTitle);

    List<Person> listContributors = this.depositClientService.getContributors(depositId);
    assertEquals(2, listContributors.size());

    deposit = this.depositITService.startEditingMetadata(deposit);
    assertEquals(originalDescription, deposit.getDescription());
    final String newTitle = DLCMTestConstants.getRandomNameWithTemporaryLabel("Deposit v1");
    final String newDescription = "Deposit corrected (version 1)";
    deposit.setTitle(newTitle);
    deposit.setDescription(newDescription);

    this.depositClientService.update(depositId, deposit);
    Deposit updatedDeposit = this.depositClientService.findOne(depositId);
    assertEquals(newTitle, updatedDeposit.getTitle());
    assertEquals(newDescription, updatedDeposit.getDescription());

    // Create and add a new Person with ORCID
    final String firstName3 = DLCMTestConstants.getRandomNameWithTemporaryLabel("John");
    final String lastName3 = DLCMTestConstants.getRandomNameWithTemporaryLabel("Doe");
    final String orcid = "1234-1234-1234-4567";
    final Person person3 = this.personITService.getOrCreateRemotePerson(firstName3, lastName3, orcid);
    final String personId3 = person3.getResId();
    this.depositClientService.addContributor(depositId, personId3);

    // Create and add a new Person
    final String firstName4 = DLCMTestConstants.getRandomNameWithTemporaryLabel("Jane");
    final String lastName4 = DLCMTestConstants.getRandomNameWithTemporaryLabel("Doe");
    final Person person4 = this.personITService.getOrCreateRemotePerson(firstName4, lastName4, null);
    final String personId4 = person4.getResId();
    this.depositClientService.addContributor(depositId, personId4);

    // Check contributors are added to the deposit
    listContributors = this.depositClientService.getContributors(depositId);
    assertEquals(4, listContributors.size());
    final Person fetchedPerson3 = listContributors.get(2);
    assertEquals(personId3, fetchedPerson3.getResId());
    assertEquals(orcid, fetchedPerson3.getOrcid());
    final Person fetchedPerson4 = listContributors.get(3);
    assertEquals(personId4, fetchedPerson4.getResId());
    assertNull(fetchedPerson4.getOrcid());

    if (withDeletedContributors) {
      // Delete contributors before cancelling the update
      this.restClientTool.sudoAdmin();
      this.personITService.delete(personId3);
      this.personITService.delete(personId4);
      this.restClientTool.exitSudo();
    }

    this.depositClientService.cancelEditingMetadata(deposit.getResId());
    this.depositITService.waitDepositIsCompleted(deposit.getResId());

    // Find deposit with original values by searching
    Map<String, String> properties = new HashMap<>();
    properties.put("title", originalTitle);
    properties.put("organizationalUnitId", deposit.getOrganizationalUnitId());
    updatedDeposit = this.depositClientService.searchByProperties(properties).get(0);

    assertEquals(originalTitle, updatedDeposit.getTitle());
    assertEquals(originalDescription, updatedDeposit.getDescription());
    listContributors = this.depositClientService.getContributors(depositId);
    assertEquals(2, listContributors.size());
  }

  @Test
  void restoreMetadataWithContributorThatHasNoMoreOrcid() {
    this.restClientTool.sudoAdmin();
    Deposit deposit = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, TEMP_DEPOSIT_WITH_COMPLETED_AIP_TITLE,
            DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    deposit.setMetadataVersion(DLCMMetadataVersion.getDefaultVersion());
    deposit = this.depositClientService.create(deposit);
    final String depositId = deposit.getResId();

    final String firstName = DLCMTestConstants.getRandomNameWithTemporaryLabel("John");
    final String lastName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Doe");
    final String orcid = "1234-1234-1234-4567";
    Person person = this.personITService.getOrCreateRemotePerson(firstName, lastName, orcid);
    final String personId = person.getResId();
    this.depositClientService.addContributor(depositId, personId);

    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrganizationalUnit(deposit.getOrganizationalUnitId());
    deposit = this.getDepositWithCompleteAip(orgUnit, deposit);
    this.restClientTool.exitSudo();

    // Start metadata edition
    deposit = this.depositITService.startEditingMetadata(deposit);

    // Remove ORCID from contributor
    person = this.personITService.findOne(personId);
    assertEquals(orcid, person.getOrcid());

    this.restClientTool.sudoAdmin();
    person.setOrcid(null);
    this.personITService.update(personId, person);
    this.restClientTool.exitSudo();

    person = this.personITService.findOne(personId);
    assertNull(person.getOrcid());

    this.depositClientService.cancelEditingMetadata(deposit.getResId());
    this.depositITService.waitDepositIsCompleted(deposit.getResId());

    deposit = this.depositClientService.findOne(depositId);
    assertEquals(DepositStatus.COMPLETED, deposit.getStatus());

    // Check that even if ORCID is removed, the contributor is found back
    // Note: it works only if only one Person with the same name exists.
    List<Person> listContributors = this.depositClientService.getContributors(depositId);
    assertEquals(1, listContributors.size());
    Person person1 = listContributors.get(0);
    assertEquals(firstName, person1.getFirstName());
    assertEquals(lastName, person1.getLastName());
    assertEquals(personId, person1.getResId());
    assertNull(person1.getOrcid());
  }

  @Test
  void rejectMetadataEditingTest() {
    Deposit deposit = this.createDepositWithApproveAndCompletedAip();
    String originalTitle = deposit.getTitle();
    String originalDescription = deposit.getDescription();
    this.depositITService.waitDepositIsCompleted(deposit.getResId());
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), originalTitle);

    deposit = this.depositITService.startEditingMetadata(deposit);
    String title = DLCMTestConstants.getRandomNameWithTemporaryLabel("Deposit v1");
    deposit.setTitle(title);
    deposit.setDescription("Deposit corrected (version 1)");

    // submit deposit
    this.depositClientService.submit(deposit.getResId());

    this.depositClientService.reject(deposit.getResId(), "missing detail");

    Map<String, String> properties = new HashMap<>();
    properties.put("title", originalTitle);
    properties.put("organizationalUnitId", deposit.getOrganizationalUnitId());
    Deposit depositChanged = this.depositClientService.searchByProperties(properties).get(0);

    assertEquals(originalTitle, depositChanged.getTitle());
    assertEquals(originalDescription, depositChanged.getDescription());
    assertEquals(DepositStatus.EDITING_METADATA_REJECTED, depositChanged.getStatus());
  }

  @ParameterizedTest
  @MethodSource("allVersions")
  void updateDepositTitleCreateWithSpecifMetadataVersionTest(DLCMMetadataVersion version) {
    Deposit deposit = this.createDepositWithCompletedAip(version);
    deposit = this.depositITService.startEditingMetadata(deposit);
    deposit.setTitle(DLCMTestConstants.getRandomNameWithTemporaryLabel("Deposit v" + version.getVersion()));
    deposit.setDescription("Deposit corrected (v" + version.getVersion() + ")");

    this.depositClientService.update(deposit.getResId(), deposit);
    Deposit depositChanged = this.depositClientService.findOne(deposit.getResId());

    assertEquals(deposit.getTitle(), depositChanged.getTitle());
    assertEquals(deposit.getAccess(), depositChanged.getAccess());
  }

}

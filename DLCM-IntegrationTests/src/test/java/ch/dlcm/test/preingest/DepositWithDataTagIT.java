/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositWithDataTagIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static ch.dlcm.model.DataTag.BLUE;
import static ch.dlcm.model.DataTag.CRIMSON;
import static ch.dlcm.model.DataTag.GREEN;
import static ch.dlcm.model.DataTag.ORANGE;
import static ch.dlcm.model.DataTag.RED;
import static ch.dlcm.model.DataTag.UNDEFINED;
import static ch.dlcm.model.DataTag.YELLOW;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.Access;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.Role;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

class DepositWithDataTagIT extends AbstractDepositWithRoleIT {

  @Autowired
  public DepositWithDataTagIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
  }

  @ParameterizedTest
  @MethodSource("validCombinations")
  void createDepositWithRightDataTag(Access accessLevel, DataTag dataTag) {
    final String title = "Deposit with valid accessLevel/dataTag combination";
    final Deposit deposit = this.createDataTagDeposit(accessLevel, dataTag, title);
    assertNotNull(this.depositClientService.create(deposit));
  }

  @ParameterizedTest
  @MethodSource("invalidCombinations")
  void createDepositWithWrongDataSensitivity(Access accessLevel, DataTag dataTag) {
    final String title = "Deposit with invalid accessLevel/dataTag combination";
    final Deposit deposit = this.createDataTagDeposit(accessLevel, dataTag, title);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositClientService.create(deposit));
  }

  private Deposit createDataTagDeposit(Access accessLevel, DataTag dataTag, String title) {
    final Deposit deposit = this.createLocalDepositForCurrentRole(title);
    deposit.setAccess(accessLevel);
    deposit.setDataSensitivity(dataTag);
    if ((dataTag.equals(UNDEFINED) || dataTag.equals(BLUE)) && accessLevel.equals(Access.PUBLIC)) {
      deposit.setDataUsePolicy(DataUsePolicy.LICENSE);
      deposit.setContentStructurePublic(true);
    }
    if ((dataTag.equals(UNDEFINED) || dataTag.equals(BLUE) || dataTag.equals(GREEN)) && accessLevel.equals(Access.RESTRICTED)) {
      deposit.setDataUsePolicy(DataUsePolicy.NONE);
    }
    if ((dataTag.equals(YELLOW)) && (accessLevel.equals(Access.CLOSED) || accessLevel.equals(Access.RESTRICTED))) {
      deposit.setDataUsePolicy(DataUsePolicy.CLICK_THROUGH_DUA);
    }
    if ((dataTag.equals(ORANGE) || dataTag.equals(RED) || dataTag.equals(CRIMSON)) && accessLevel.equals(Access.CLOSED)) {
      deposit.setDataUsePolicy(DataUsePolicy.EXTERNAL_DUA);
    }
    return deposit;
  }

  private static Stream<Arguments> validCombinations() {
    return Stream.of(
            arguments(Access.PUBLIC, UNDEFINED),
            arguments(Access.PUBLIC, DataTag.BLUE),
            arguments(Access.RESTRICTED, DataTag.BLUE),
            arguments(Access.RESTRICTED, DataTag.GREEN),
            arguments(Access.RESTRICTED, DataTag.YELLOW),
            arguments(Access.CLOSED, DataTag.BLUE),
            arguments(Access.CLOSED, DataTag.GREEN),
            arguments(Access.CLOSED, DataTag.YELLOW),
            arguments(Access.CLOSED, DataTag.ORANGE),
            arguments(Access.CLOSED, DataTag.RED),
            arguments(Access.CLOSED, DataTag.CRIMSON));
  }

  private static Stream<Arguments> invalidCombinations() {
    return Stream.of(
            arguments(Access.PUBLIC, DataTag.GREEN),
            arguments(Access.PUBLIC, DataTag.YELLOW),
            arguments(Access.PUBLIC, DataTag.ORANGE),
            arguments(Access.PUBLIC, DataTag.RED),
            arguments(Access.PUBLIC, DataTag.CRIMSON),
            arguments(Access.RESTRICTED, DataTag.ORANGE),
            arguments(Access.RESTRICTED, DataTag.RED),
            arguments(Access.RESTRICTED, DataTag.CRIMSON));
  }

  @Override
  protected String role() {
    return Role.CREATOR_ID;
  }
}

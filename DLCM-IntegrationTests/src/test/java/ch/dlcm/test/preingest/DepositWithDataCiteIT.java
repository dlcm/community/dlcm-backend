/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositFileNumberAndSizeIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DepositWithDataCiteIT extends AbstractPreingestIT {

  private static final String DATACITE_DATA = "deposit/datacite";

  @Autowired
  public DepositWithDataCiteIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Order(10)
  @ParameterizedTest
  @MethodSource("allVersionsWithDataCiteVersion")
  void testDataciteExampleFull(DLCMMetadataVersion version, String dataciteVersion) {
    final String fileToUpload = DATACITE_DATA + "/datacite-example-full-v" + dataciteVersion + SolidifyConstants.XML_EXT;
    final Deposit deposit = this.depositITService.createRemoteDepositWithVersion(PersistenceMode.TEMPORARY,
            "Deposit v" + version.getVersion() + " DataCite v" + dataciteVersion + " ", DEFAULT_DESCRIPTION, version);

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setDataCategory(DataCategory.Package);
    fileUploadDto.setDataType(DataCategory.Metadata);
    final DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
            new ClassPathResource(fileToUpload),
            fileUploadDto);
    assertNotNull(depositDataFile);
    // Wait until data file downloaded
    this.depositITService.waitDataFileIsReady(deposit.getResId(), 1);
  }

  @Order(10)
  @ParameterizedTest
  @MethodSource("allVersions")
  void testDatacite45(DLCMMetadataVersion version) {
    final String fileToUpload = DATACITE_DATA + "/datacite-example-full-v4.5" + SolidifyConstants.XML_EXT;
    final Deposit deposit = this.depositITService.createRemoteDepositWithVersion(PersistenceMode.TEMPORARY, "Deposit DataCite ", DEFAULT_DESCRIPTION, version);

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setDataCategory(DataCategory.Package);
    fileUploadDto.setDataType(DataCategory.Metadata);
    final DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
            new ClassPathResource(fileToUpload),
            fileUploadDto);
    assertNotNull(depositDataFile);
    // Wait until data file downloaded
    assertTrue(this.depositITService.waitDepositWithDataIsReady(deposit.getResId()));
    final List<DepositDataFile> dataFileList = this.depositClientService.getDepositDataFile(deposit.getResId());
    if (version.getVersionNumber() >= 40) {
      assertTrue(dataFileList.stream().allMatch(df -> df.getStatus() == DataFileStatus.READY));
    } else {
      assertTrue(dataFileList.stream().allMatch(df -> df.getStatus() == DataFileStatus.IN_ERROR));
    }
  }

  private static Stream<Arguments> allVersionsWithDataCiteVersion() {
    return Stream.of(
            arguments(DLCMMetadataVersion.V1_0, "4.0"),
            arguments(DLCMMetadataVersion.V1_1, "4.0"),
            arguments(DLCMMetadataVersion.V2_0, "4.3"),
            arguments(DLCMMetadataVersion.V2_1, "4.3"),
            arguments(DLCMMetadataVersion.V3_0, "4.4"),
            arguments(DLCMMetadataVersion.V3_1, "4.4"),
            arguments(DLCMMetadataVersion.V4_0, "4.5"));
  }
}

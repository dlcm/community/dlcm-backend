/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ContributorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.preingest.Contributor;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class ContributorIT extends AbstractPreingestIT {
  private static final Logger log = LoggerFactory.getLogger(ContributorIT.class);

  private final String role = Role.CREATOR_ID;

  private PersonClientService personClientService;
  private PersonITService personITService;
  private DepositClientService depositClientService;

  @Autowired
  public ContributorIT(Environment env, SudoRestClientTool restClientTool, OrgUnitITService orgUnitITService, DepositITService depositITService,
          DepositClientService depositClientService, PersonClientService personClientService, PersonITService personITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.personClientService = personClientService;
    this.personITService = personITService;
    this.depositClientService = depositClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void filterContributorsTest() {
    final Person person = this.personClientService.findAll().get(0);

    // Filter on first name
    int endIndexFirstName = Math.min(person.getFirstName().length(), 4);
    final Map<String, String> properties = Map.of("firstName", person.getFirstName().substring(1, endIndexFirstName));
    List<Person> personList = this.personClientService.searchByProperties(properties);
    List<Contributor> contributorList = this.depositITService.getContributorClientService().searchContributors(properties);
    assertEquals(contributorList.size(), personList.size());

    // Filter on last name
    int endIndexLastName = Math.min(person.getLastName().length(), 4);
    Map<String, String> newProperties = Map.of("lastName", person.getLastName().substring(1, endIndexLastName));
    personList = this.personClientService.searchByProperties(newProperties);
    contributorList = this.depositITService.getContributorClientService().searchContributors(newProperties);
    assertEquals(contributorList.size(), personList.size());
    for (final Contributor contributor : contributorList) {
      final Person person2 = this.personClientService.findOne(contributor.getResId());
      assertEquals(contributor.getFirstName(), person2.getFirstName());
      assertEquals(contributor.getLastName(), person2.getLastName());
      assertEquals(contributor.getOrcid(), person2.getOrcid());
    }
  }

  @Test
  void filterContributorsWithDepositsTest() {
    // Create a deposit with a contributor
    final String title = "deposit contributor";
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, title, DEFAULT_DESCRIPTION, this.role, false,
            DLCMMetadataVersion.getDefaultVersion());
    // Create a new Person
    final Person person = this.personITService.getOrCreateRemotePerson();
    // Add the person as contributor
    this.depositClientService.addContributor(deposit.getResId(), person.getResId());
    final List<Person> listContributors = this.depositClientService.getContributors(deposit.getResId());
    assertEquals(1, listContributors.size());
    assertEquals(person.getResId(), listContributors.get(0).getResId());

    // Filter on deposit title
    final List<Deposit> depositList = this.depositITService.getContributorClientService().searchDeposits(person.getResId(),
            Map.of("deposit.title", title));
    assertTrue(depositList.size() > 0);
    for (final Deposit d : depositList) {
      assertTrue(d.getTitle().contains(title));
    }
  }

  @Test
  void listContributorsTest() {
    final RestCollection<Contributor> contributorList = this.depositITService.getContributorClientService().list();
    final List<Person> personList = this.personClientService.findAll();
    assertEquals(contributorList.getPage().getTotalItems(), personList.size());
  }

  @Test
  void listContributorsWithDepositsTest() {
    log.debug("Starting listContributorsWithDepositsTest");

    // Create a deposit with a contributor
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION, this.role,
            false, DLCMMetadataVersion.getDefaultVersion());
    log.debug("Deposit {} created on org unit {}", deposit.getResId(), deposit.getOrganizationalUnitId());

    // Create a new Person
    final Person person1 = this.personITService.getOrCreateRemotePerson();
    log.debug("Use person {} {} ({})", person1.getFirstName(), person1.getLastName(), person1.getResId());

    // Add the person as contributor
    this.depositClientService.addContributor(deposit.getResId(), person1.getResId());
    final List<Person> listContributors = this.depositClientService.getContributors(deposit.getResId());
    assertEquals(1, listContributors.size());
    assertEquals(person1.getResId(), listContributors.get(0).getResId());

    // List deposits of the contributor
    final RestCollection<Deposit> depositList = this.depositITService.getContributorClientService().listDeposits(person1.getResId());
    log.debug("Person {} has {} deposit", person1.getResId(), depositList.getPage().getTotalItems());

    // Test if not empty
    assertNotEquals(0, depositList.getPage().getTotalItems());

    // Test that the number of deposits of the contributor is consistent
    final RestCollection<Contributor> contributorList = this.depositITService.getContributorClientService().listWithDeposits();
    log.debug("{} contributors have at least one deposit", contributorList.getPage().getTotalItems());
    for (final Contributor contributor : contributorList.getData()) {
      if (contributor.getResId().equals(person1.getResId())) {
        // Check if deposit number is correct
        assertEquals(depositList.getPage().getTotalItems(), contributor.getDepositNumber().longValue());
      }
    }
    log.debug("Stopping listContributorsWithDepositsTest");
  }

  @Override
  protected void deleteFixtures() {
    // Wait before to delete
    SolidifyTime.waitOneSecond();

    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN, this.role, AuthApplicationRole.USER_ID, false);
    this.depositITService.clearDepositsFixtures(organizationalUnit);
    this.personITService.cleanTestData();
    ;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositFileNumberAndSizeIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.MetadataITService;
import ch.dlcm.test.service.OrgUnitITService;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DepositWithMetadataVersionIT extends AbstractPreingestIT {

  private static final String YARETA_DATA = "deposit/dlcm/Yareta-small.zip";
  private static final int YARETA_FILE_NUM = 3;

  private AipITService aipITService;
  private MetadataITService metadataITService;

  @Autowired
  public DepositWithMetadataVersionIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService,
          AipITService aipITService,
          MetadataITService metadataITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.aipITService = aipITService;
    this.metadataITService = metadataITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Order(10)
  @ParameterizedTest
  @MethodSource("allVersions")
  void testDepositByVersion(DLCMMetadataVersion version) {
    // Create
    final Deposit deposit0 = this.createDeposit(version);
    final ArchivalInfoPackage aip0 = this.getAIP(deposit0);
    this.checkAip(aip0, YARETA_FILE_NUM + 1, 0, 0, 0);
    // Archive metadata
    final ArchiveMetadata archiveMetadata0 = this.metadataITService.getArchiveMetadata(aip0.getResId());
    this.checkMetadata(archiveMetadata0, YARETA_FILE_NUM, YARETA_FILE_NUM + 1, 1, aip0.getArchiveSize());
    // Update #1
    final Deposit deposit1 = this.updateDeposit(deposit0, false);
    final ArchivalInfoPackage aip1 = this.getAIP(deposit1);
    this.checkAip(aip1, YARETA_FILE_NUM + 2, 1, 0, 0);
    // Archive metadata
    final ArchiveMetadata archiveMetadata1 = this.metadataITService.getArchiveMetadata(aip1.getResId());
    this.checkMetadata(archiveMetadata1, YARETA_FILE_NUM, YARETA_FILE_NUM + 2, 2, aip1.getArchiveSize());
    // Update #2
    final Deposit deposit2 = this.updateDeposit(deposit1, false);
    final ArchivalInfoPackage aip2 = this.getAIP(deposit2);
    this.checkAip(aip2, YARETA_FILE_NUM + 3, 2, 0, 0);
    // Archive metadata
    final ArchiveMetadata archiveMetadata2 = this.metadataITService.getArchiveMetadata(aip2.getResId());
    this.checkMetadata(archiveMetadata2, YARETA_FILE_NUM, YARETA_FILE_NUM + 3, 3, aip2.getArchiveSize());
    // Cancel Update #3
    final Deposit deposit3 = this.updateDeposit(deposit2, true);
    final ArchivalInfoPackage aip3 = this.getAIP(deposit3);
    this.checkAip(aip3, YARETA_FILE_NUM + 3, 2, 0, 0);
    // Archive metadata
    final ArchiveMetadata archiveMetadata3 = this.metadataITService.getArchiveMetadata(aip3.getResId());
    this.checkMetadata(archiveMetadata3, YARETA_FILE_NUM, YARETA_FILE_NUM + 3, 3, aip3.getArchiveSize());

  }

  private Deposit updateDeposit(Deposit deposit, boolean cancelUpdate) {
    Deposit workingDeposit = this.depositITService.startEditingMetadata(deposit);
    workingDeposit.setTitle(workingDeposit.getTitle() + "-");
    this.depositITService.updateDeposit(workingDeposit.getResId(), workingDeposit);
    if (cancelUpdate) {
      this.depositITService.cancelEditingMetadata(workingDeposit);
    } else {
      this.depositITService.approveDeposit(workingDeposit);
    }
    return this.waitDepositIsCompleted(deposit);
  }

  private ArchivalInfoPackage getAIP(Deposit deposit) {
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
    final String aipId = this.metadataITService.getAipIdFromDepositId(deposit.getResId());
    return this.aipITService.findAIP(aipId);
  }

  private Deposit createDeposit(DLCMMetadataVersion version) {
    final Deposit deposit = this.depositITService.createRemoteDepositWithContributorAndVersion(PersistenceMode.TEMPORARY,
            "Deposit by version " + version.toString() + " ", DEFAULT_DESCRIPTION, version);
    this.depositITService.uploadArchiveToDeposit(deposit.getResId(), new ClassPathResource(YARETA_DATA));
    this.depositITService.waitDepositWithDataIsReady(deposit.getResId());
    this.depositITService.approveDeposit(deposit);
    return this.waitDepositIsCompleted(deposit);
  }

  private Deposit waitDepositIsCompleted(Deposit deposit) {
    assertTrue(this.depositITService.waitDepositIsCompleted(deposit.getResId()), "Deposit not completed: " + deposit.getResId());
    return this.depositITService.findDeposit(deposit.getResId());
  }

}

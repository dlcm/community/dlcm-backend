/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositAsVisitorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

class DepositAsVisitorIT extends AbstractDepositWithRoleIT {

  @Autowired
  public DepositAsVisitorIT(Environment env, SudoRestClientTool restClientTool, OrgUnitITService orgUnitITService,
          DepositITService depositITService, DepositClientService depositClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);

  }

  @Override
  protected String role() {
    return Role.VISITOR_ID;
  }

  @Test
  void creationTest() {
    this.getPermanentOrganizationalUnitWithRole();
    DLCMTestConstants.getRandomNameWithTemporaryLabel("deposit created by test");

    // Create a deposit
    final Deposit deposit1 = this.createLocalDepositForCurrentRole();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.depositClientService.create(deposit1));
  }

  @Test
  void cannotListAllWithoutOrgUnitIdTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.depositClientService.findAll());
  }

  @Test
  void canListOnlyOnAuthorizedOrgUnitsTest() {
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getTestOrganizationalUnitAsUser(
            DLCMTestConstants.PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN, Role.VISITOR_ID, false, this.inheritedRole());
    assertDoesNotThrow(() -> this.depositClientService.findAllWithOrgunitId(organizationalUnit.getOrganizationalUnitId()));
  }

  @Test
  void cannotListInNonAuthorizedOrgUnitsTest() {
    this.restClientTool.sudoAdmin();
    OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(
            DLCMTestConstants.PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN,
            NO_ONE, null, false);
    this.restClientTool.exitSudo();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.depositClientService.findAllWithOrgunitId(organizationalUnit.getOrganizationalUnitId()));
  }

  @Test
  void getAnyDepositTest() {
    this.restClientTool.sudoAdmin();
    this.getPermanentOrganizationalUnitWithRole();
    DLCMTestConstants.getRandomNameWithTemporaryLabel("deposit created by test");

    // Create a deposit
    final Deposit deposit1 = this.createLocalDepositForCurrentRole();
    this.depositClientService.create(deposit1);
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.depositClientService.findOne(deposit1.getResId()));
  }

  @Test
  void deleteTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, this::createRemoteDepositForCurrentRole);
  }

  @Test
  void updateTest() {
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositClientService.update("test", null));
  }

  @Override
  protected void deleteFixtures() {
  }
}

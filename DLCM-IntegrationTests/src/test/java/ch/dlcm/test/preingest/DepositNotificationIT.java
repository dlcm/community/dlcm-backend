/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositNotificationIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.NotificationITService;
import ch.dlcm.test.service.OrgUnitITService;

class DepositNotificationIT extends AbstractPreingestIT {

  private final NotificationITService notificationITService;

  @Autowired
  public DepositNotificationIT(Environment env, SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService, DepositITService depositITService,
          DepositClientService depositClientService, NotificationITService notificationITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.notificationITService = notificationITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Test
  void checkNotificationsDuringSubmissionProcess() {

    // Check inbox for each notification before creating a deposit
    List<Notification> toApproveDepositNotificationList = this.notificationITService.getInboxNotifications(
            NotificationType.VALIDATE_DEPOSIT_REQUEST);
    List<Notification> approvedDepositNotificationList = this.notificationITService.getInboxNotifications(
            NotificationType.MY_APPROVED_DEPOSIT_INFO);
    List<Notification> completedDepositNotificationList = this.notificationITService.getInboxNotifications(
            NotificationType.MY_COMPLETED_DEPOSIT_INFO);

    // Create a deposit and wait until it is completed
    final Deposit deposit = this.depositITService.createRemoteDepositWithContributorsAndSubmissionApprove(
            DLCMTestConstants.PersistenceMode.TEMPORARY,
            "Test creation of notifications when creating and submitting deposit", DEFAULT_DESCRIPTION);
    deposit.addItem(this.depositITService.createLocalDepositDataFile(deposit));
    final Deposit depositRemote = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.waitDepositWithDataIsReady(deposit.getResId());

    this.depositITService.submitDeposit(depositRemote);
    this.restClientTool.sudoAdmin();
    this.depositITService.approveDeposit(deposit);
    this.restClientTool.exitSudo();
    this.depositITService.waitDepositIsCompleted(deposit.getResId());

    // Check notification's inbox after creating the deposit
    List<Notification> toApproveDepositNotificationListAfterNewDeposit = this.notificationITService.getInboxNotifications(
            NotificationType.VALIDATE_DEPOSIT_REQUEST);
    List<Notification> approvedDepositNotificationListAfterNewDeposit = this.notificationITService.getInboxNotifications(
            NotificationType.MY_APPROVED_DEPOSIT_INFO);
    List<Notification> completedDepositNotificationListAfterNewDeposit = this.notificationITService.getInboxNotifications(
            NotificationType.MY_COMPLETED_DEPOSIT_INFO);

    assertEquals(toApproveDepositNotificationList.size() + 1, toApproveDepositNotificationListAfterNewDeposit.size());
    assertEquals(approvedDepositNotificationList.size() + 1, approvedDepositNotificationListAfterNewDeposit.size());
    assertEquals(completedDepositNotificationList.size() + 1, completedDepositNotificationListAfterNewDeposit.size());

  }
}

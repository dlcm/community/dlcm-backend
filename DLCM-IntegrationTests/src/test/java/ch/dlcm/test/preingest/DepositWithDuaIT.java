/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositWithDuaIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class DepositWithDuaIT extends AbstractDepositWithRoleIT {

  private static final String DUA_SHOULD_NOT_BE_PRESENT = "Data Usage Agreement should not be present";
  private static final String MISSING_DUA = "Missing Data Usage Agreement";

  private enum DataFileType {
    DATA_FILE, DUA_FILE
  }

  private final PersonITService personITService;

  @Autowired
  public DepositWithDuaIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService,
          PersonITService personITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.personITService = personITService;
  }

  @ParameterizedTest
  @MethodSource("validCombinations")
  void createDepositWithRightDataSensitivity(Access access, DataTag dataTag, DataUsePolicy dataUsePolicy) {
    final String title = "Deposit with valid dataTag/duaType combination";
    final Deposit deposit = this.createDuaDeposit(access, dataTag, dataUsePolicy, title);
    assertNotNull(this.depositClientService.create(deposit));
  }

  @ParameterizedTest
  @MethodSource("invalidCombinations")
  void createDepositWithWrongDataSensitivity(Access access, DataTag dataTag, DataUsePolicy dataUsePolicy) {
    final String title = "Deposit with invalid dataTag/dataUsePolicy combination";
    final Deposit deposit = this.createDuaDeposit(access, dataTag, dataUsePolicy, title);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositClientService.create(deposit));
  }

  private Deposit createDuaDeposit(Access access, DataTag dataTag, DataUsePolicy dataUsePolicy, String title) {
    final Deposit deposit = this.createLocalDepositForCurrentRole(title);
    deposit.setDataSensitivity(dataTag);
    deposit.setDataUsePolicy(dataUsePolicy);
    deposit.setAccess(access);
    return deposit;
  }

  @Test
  void createDepositWithUnneededDua() {
    this.testDuaPresenceOrAbsence(DataUsePolicy.EXTERNAL_DUA, DataFileType.DUA_FILE, DUA_SHOULD_NOT_BE_PRESENT);
  }

  @Test
  void createDepositWithoutNeededDua() {
    this.testDuaPresenceOrAbsence(DataUsePolicy.SIGNED_DUA, DataFileType.DATA_FILE, MISSING_DUA);
  }

  void testDuaPresenceOrAbsence(DataUsePolicy dataUsePolicy, DataFileType dataFileType, String historyValue) {
    final Deposit deposit = this.createDeposit(Access.CLOSED, DataTag.ORANGE, dataUsePolicy, dataFileType);
    this.depositITService.waitDepositWithDataIsReady(deposit.getResId());
    this.depositITService.approveDeposit(deposit);
    this.depositITService.waitDepositIsCompleted(deposit.getResId());
    final Deposit processedDeposit = this.depositITService.findDeposit(deposit.getResId());
    assertEquals(DepositStatus.IN_ERROR, processedDeposit.getStatus());
    assertTrue(processedDeposit.getStatusMessage().contains(historyValue));
  }

  @Test
  void createDepositWithWrongMetadaVersion() {
    final Deposit deposit = this.createLocalDepositForCurrentRole("Deposit with DataUsePolicy and wrong Metadata version");
    deposit.setAccess(Access.RESTRICTED);
    deposit.setDataSensitivity(DataTag.YELLOW);
    deposit.setDataUsePolicy(DataUsePolicy.CLICK_THROUGH_DUA);
    deposit.setMetadataVersion(DLCMMetadataVersion.V3_0);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositClientService.create(deposit));
  }

  private static Stream<Arguments> validCombinations() {
    return Stream.of(
            arguments(Access.PUBLIC, DataTag.UNDEFINED, DataUsePolicy.LICENSE),
            arguments(Access.PUBLIC, DataTag.BLUE, DataUsePolicy.LICENSE),
            arguments(Access.RESTRICTED, DataTag.UNDEFINED, DataUsePolicy.NONE),
            arguments(Access.RESTRICTED, DataTag.GREEN, DataUsePolicy.LICENSE),
            arguments(Access.RESTRICTED, DataTag.BLUE, DataUsePolicy.NONE),
            arguments(Access.CLOSED, DataTag.YELLOW, DataUsePolicy.CLICK_THROUGH_DUA),
            arguments(Access.CLOSED, DataTag.ORANGE, DataUsePolicy.SIGNED_DUA),
            arguments(Access.CLOSED, DataTag.RED, DataUsePolicy.EXTERNAL_DUA),
            arguments(Access.CLOSED, DataTag.CRIMSON, DataUsePolicy.SIGNED_DUA));
  }

  private static Stream<Arguments> invalidCombinations() {
    return Stream.of(
            arguments(Access.PUBLIC, DataTag.UNDEFINED, DataUsePolicy.NONE),
            arguments(Access.PUBLIC, DataTag.UNDEFINED, DataUsePolicy.CLICK_THROUGH_DUA),
            arguments(Access.PUBLIC, DataTag.UNDEFINED, DataUsePolicy.SIGNED_DUA),
            arguments(Access.PUBLIC, DataTag.BLUE, DataUsePolicy.NONE),
            arguments(Access.PUBLIC, DataTag.BLUE, DataUsePolicy.SIGNED_DUA),
            arguments(Access.PUBLIC, DataTag.GREEN, DataUsePolicy.EXTERNAL_DUA),
            arguments(Access.PUBLIC, DataTag.YELLOW, DataUsePolicy.EXTERNAL_DUA),
            arguments(Access.PUBLIC, DataTag.ORANGE, DataUsePolicy.NONE),
            arguments(Access.PUBLIC, DataTag.RED, DataUsePolicy.LICENSE),
            arguments(Access.PUBLIC, DataTag.CRIMSON, DataUsePolicy.CLICK_THROUGH_DUA));
  }

  private Deposit createDeposit(Access access, DataTag dataTag, DataUsePolicy dataUsePolicy, DataFileType dataFileType) {
    final String title = "Deposit without needed DUA";
    final Deposit deposit = this.createLocalDepositForCurrentRole(title);
    final Person person = this.personITService.getOrCreateRemotePerson();
    deposit.setDataSensitivity(dataTag);
    deposit.setDataUsePolicy(dataUsePolicy);
    deposit.setAccess(access);
    final Deposit remoteDeposit = this.depositClientService.create(deposit);
    this.depositClientService.addContributor(remoteDeposit.getResId(), person.getResId());
    switch (dataFileType) {
      case DATA_FILE -> this.depositClientService.addDepositDataFile(remoteDeposit.getResId(),
              this.depositITService.createLocalDepositDataFile(deposit));
      case DUA_FILE -> this.depositClientService.addDepositDataFile(remoteDeposit.getResId(),
              this.depositITService.createDuaFile(deposit));
    }
    return remoteDeposit;
  }

  @Override
  protected String role() {
    return Role.CREATOR_ID;
  }
}

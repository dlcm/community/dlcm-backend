/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositTestFitsFormatIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DepositTestFitsFormatIT extends AbstractPreingestIT {

  protected static final String FITS_FILES_DATA = "deposit/dlcm/fits-format.zip";

  @Autowired
  public DepositTestFitsFormatIT(Environment env, SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService, DepositITService depositITService,
          DepositClientService depositClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  private final List<String> contentTypes = List.of("text/csv", "text/vtt", "image/vnd.adobe.photoshop", "image/vnd.dwg",
          "application/octet-stream");
  private final List<String> puids = List.of("x-fmt/18", "fmt/1454", "fmt/1037", "fmt/434", "x-fmt/92");

  @Test
  void testFitsFileFormat() {
    // Create
    final Deposit deposit = this.createDeposit();
    List<DepositDataFile> depositDataFile = this.depositClientService.getDepositDataFile(deposit.getResId());
    depositDataFile.forEach(df -> {
      assertEquals(this.hasItem(this.contentTypes, df.getFileFormat().getContentType()), df.getFileFormat().getContentType());
      assertEquals(this.hasItem(this.puids, df.getFileFormat().getPuid()), df.getFileFormat().getPuid());
    });
  }

  private Deposit createDeposit() {
    final Deposit deposit = this.depositITService.createRemoteDeposit(DLCMTestConstants.PersistenceMode.TEMPORARY,
            "Deposit to test fits metadata", DEFAULT_DESCRIPTION);
    final String depositId = deposit.getResId();
    this.depositITService.uploadArchiveToDeposit(depositId, new ClassPathResource(FITS_FILES_DATA));
    this.depositITService.waitDepositWithDataIsReady(depositId);
    return deposit;
  }

  private String hasItem(List<String> src, String item) {
    return src.stream()
            .filter(item::equals)
            .findFirst()
            .orElse(null);
  }

}

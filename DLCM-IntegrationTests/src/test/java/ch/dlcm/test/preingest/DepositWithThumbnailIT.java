/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositWithThumbnailIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class DepositWithThumbnailIT extends AbstractDepositWithRoleIT {

  private static final String DUA_SHOULD_NOT_BE_PRESENT = "Data Usage Agreement should not be present";
  private static final String MISSING_DUA = "Missing Data Usage Agreement";

  private enum DataFileType {
    DATA_FILE, DUA_FILE
  }

  private final PersonITService personITService;

  @Autowired
  public DepositWithThumbnailIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService,
          PersonITService personITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.personITService = personITService;
  }

  @ParameterizedTest
  @MethodSource("validCombinations")
  void createDepositWithRightThumbnail(DLCMMetadataVersion version, DataCategory thumbnailCategory) {
    final String title = "Deposit with thumbnail supported";
    final Deposit deposit = this.createRemoteDepositWithThumbnail(version, title, thumbnailCategory);
    this.submit(deposit, true);
  }

  @ParameterizedTest
  @MethodSource("invalidCombinations")
  void createDepositWithWrongThumbnail(DLCMMetadataVersion version, DataCategory thumbnailCategory) {
    final String title = "Deposit with thumbnail not supported";
    final Deposit deposit = this.createRemoteDepositWithThumbnail(version, title, thumbnailCategory);
    this.submit(deposit, false);
  }

  private Deposit createRemoteDepositWithThumbnail(DLCMMetadataVersion version, String title, DataCategory thumbnailCategory) {
    final Deposit deposit = this.createLocalDepositForCurrentRole(title);
    final Person person = this.personITService.getOrCreateRemotePerson();
    deposit.setMetadataVersion(version);
    deposit.setDataSensitivity(DataTag.BLUE);
    deposit.setDataUsePolicy(DataUsePolicy.LICENSE);
    deposit.setAccess(Access.PUBLIC);
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.addContributorToDeposit(remoteDeposit.getResId(), person.getResId());
    this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createDataFile(deposit));
    if (thumbnailCategory.equals(DataCategory.DatasetThumbnail)) {
      this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createDatasetThumbnailFile(deposit));
    } else {
      this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createArchiveThumbnailFile(deposit));
    }
    return remoteDeposit;
  }

  private void submit(Deposit deposit, boolean result) {
    assertTrue(this.depositITService.waitDepositWithDataIsReady(deposit.getResId()), "Deposit data files in error");
    this.depositITService.approveDeposit(deposit);
    assertEquals(this.depositITService.waitDepositIsCompleted(deposit.getResId()), result, "Deposit not correct: " + deposit.getResId());
  }

  private static Stream<Arguments> validCombinations() {
    return Stream.of(
            arguments(DLCMMetadataVersion.V2_1, DataCategory.DatasetThumbnail),
            arguments(DLCMMetadataVersion.V3_0, DataCategory.DatasetThumbnail),
            arguments(DLCMMetadataVersion.V3_1, DataCategory.ArchiveThumbnail));
  }

  private static Stream<Arguments> invalidCombinations() {
    return Stream.of(
            arguments(DLCMMetadataVersion.V2_0, DataCategory.DatasetThumbnail),
            arguments(DLCMMetadataVersion.V2_0, DataCategory.ArchiveThumbnail),
            arguments(DLCMMetadataVersion.V2_1, DataCategory.ArchiveThumbnail),
            arguments(DLCMMetadataVersion.V3_0, DataCategory.ArchiveThumbnail),
            arguments(DLCMMetadataVersion.V3_1, DataCategory.DatasetThumbnail));
  }

  @Override
  protected String role() {
    return Role.CREATOR_ID;
  }
}

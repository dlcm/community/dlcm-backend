/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositSubmissionAgreementIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static ch.dlcm.test.DLCMTestConstants.SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST;
import static ch.dlcm.test.DLCMTestConstants.SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST;
import static ch.dlcm.test.DLCMTestConstants.SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST;
import static ch.dlcm.test.DLCMTestConstants.SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.display.OrganizationalUnitSubmissionPolicyDTO;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;
import ch.dlcm.test.service.SubmissionAgreementITService;

@Order(Integer.MIN_VALUE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DepositSubmissionAgreementIT extends AbstractPreingestIT {

  private final SubmissionAgreementITService submissionAgreementITService;
  private final PersonITService personITService;

  @Autowired
  public DepositSubmissionAgreementIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService,
          SubmissionAgreementITService submissionAgreementITService,
          PersonITService personITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.submissionAgreementITService = submissionAgreementITService;
    this.personITService = personITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Order(10)
  @Test
  void getAllMyApprobationsTest() {
    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(
            DLCMTestConstants.PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN, Role.MANAGER_ID, AuthApplicationRole.USER_ID, false);
    List<OrganizationalUnitSubmissionPolicyDTO> orgUnitSubmissionPolicyList = this.orgUnitITService.getSubmissionPoliciesFromOrgUnit(orgUnit);
    List<SubmissionPolicy> submissionWithAgreementForAllDepoList = orgUnitSubmissionPolicyList.stream()
            .filter(sp -> sp.getName()
                    .equals(DLCMTestConstants.getNameWithPermanentLabel(SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST)))
            .collect(Collectors.toList());
    if (!submissionWithAgreementForAllDepoList.isEmpty()) {
      final long userApprobationsNumber = this.submissionAgreementITService
              .getMyApprobationsNumber(submissionWithAgreementForAllDepoList.get(0).getSubmissionAgreement());

      final Deposit deposit = this.depositITService.createLocalDeposit(DLCMTestConstants.PersistenceMode.TEMPORARY,
              "Temporary deposit for testing number of submission agreement by user created", DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
      deposit.setSubmissionPolicyId(submissionWithAgreementForAllDepoList.get(0).getResId());
      deposit.addItem(this.depositITService.createLocalDepositDataFile(deposit));
      final Deposit firstDepositCreated = this.depositITService.createRemoteDepositWithData(deposit);
      this.depositITService.waitDepositWithDataIsReady(firstDepositCreated.getResId());
      HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
              () -> this.depositITService.checkSubmissionAgreement(firstDepositCreated));
      assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode(), "Check submission agreement failed");
      this.depositITService.approveSubmissionAgreement(firstDepositCreated);
      this.depositITService.checkSubmissionAgreement(firstDepositCreated);
      final long userNewApprobationsNumber = this.submissionAgreementITService
              .getMyApprobationsNumber(submissionWithAgreementForAllDepoList.get(0).getSubmissionAgreement());
      assertEquals(userApprobationsNumber + 1, userNewApprobationsNumber, "Missing submission agreement approval");
    } else {
      fail("Missing submission policies");
    }
  }

  @Order(20)
  @Test
  void submitDepositWithSubmissionAgreementForFirstDepositTest() {
    // Create first deposit
    final Deposit firstDepositCreated = this.archiveFirstDeposit(
            "Temporary deposit without approval and with submission agreement type for first deposit",
            false,
            SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST);
    assertNotNull(firstDepositCreated, "First deposit creation failed");

    // Create a new deposit and see how there is not need to approveSubmissionAgreement
    final Deposit secondDeposit = this.depositITService.createLocalDeposit(DLCMTestConstants.PersistenceMode.TEMPORARY,
            "Temporary deposit 2 without approval & with submission agreement type for first deposit", DEFAULT_DESCRIPTION, Role.MANAGER_ID, false,
            DLCMMetadataVersion.getDefaultVersion());
    secondDeposit.setSubmissionPolicyId(firstDepositCreated.getSubmissionPolicyId());
    secondDeposit.addItem(this.depositITService.createLocalDepositDataFile(secondDeposit));
    final Deposit secondDepositCreated = this.depositITService.createRemoteDepositWithData(secondDeposit);
    this.depositITService.waitDepositWithDataIsReady(secondDepositCreated.getResId());
    this.depositITService.addContributorToDeposit(secondDepositCreated.getResId(), this.personITService.getOrCreateRemotePerson().getResId());
    this.depositITService.checkSubmissionAgreement(secondDepositCreated);
    this.submitAndApproveDeposit(false, secondDepositCreated);
    assertTrue(this.depositITService.waitDepositIsCompleted(secondDepositCreated.getResId()), "Second deposit in error");
  }

  @Order(21)
  @Test
  void submitDepositWithSubmissionAgreementForAllDepositTest() {
    // Create first deposit
    final Deposit firstDepositCreated = this.archiveFirstDeposit(
            "Temporary deposit without approval and with submission agreement type for all deposit",
            false,
            SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST);
    assertNotNull(firstDepositCreated, "First deposit creation failed");

    // Create a new deposit and see how there is not need to approveSubmissionAgreement
    final Deposit secondDeposit = this.depositITService.createLocalDeposit(DLCMTestConstants.PersistenceMode.TEMPORARY,
            "Temporary deposit 2 without approval & with submission agreement type for all deposit", DEFAULT_DESCRIPTION, Role.MANAGER_ID, false,
            DLCMMetadataVersion.getDefaultVersion());
    secondDeposit.setSubmissionPolicyId(firstDepositCreated.getSubmissionPolicyId());
    secondDeposit.addItem(this.depositITService.createLocalDepositDataFile(secondDeposit));
    final Deposit secondDepositCreated = this.depositITService.createRemoteDepositWithData(secondDeposit);
    this.depositITService.waitDepositWithDataIsReady(secondDepositCreated.getResId());
    this.depositITService.addContributorToDeposit(secondDepositCreated.getResId(), this.personITService.getOrCreateRemotePerson().getResId());
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.depositITService.checkSubmissionAgreement(secondDepositCreated));
    assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode(), "Check submission agreement failed");
    e = assertThrows(HttpClientErrorException.class, () -> this.depositITService.approveDeposit(secondDepositCreated));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Deposit approval failed");
    this.depositITService.approveSubmissionAgreement(secondDepositCreated);
    this.submitAndApproveDeposit(false, secondDepositCreated);
    assertTrue(this.depositITService.waitDepositIsCompleted(secondDepositCreated.getResId()), "Second deposit in error");
  }

  @Order(30)
  @Test
  void approveDepositWithSubmissionAgreementForFirstDepositTest() {
    // Create first deposit
    final Deposit firstDepositCreated = this.archiveFirstDeposit(
            "Temporary deposit with submission agreement type for first deposit",
            true,
            SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST);
    assertNotNull(firstDepositCreated, "First deposit creation failed");

    // Create a new deposit and see how there is not need to approveSubmissionAgreement
    final Deposit secondDeposit = this.depositITService.createLocalDepositWithSubmissionApprove(DLCMTestConstants.PersistenceMode.TEMPORARY,
            "Temporary deposit 2 with submission agreement type for first deposit", DEFAULT_DESCRIPTION, Role.MANAGER_ID, false);
    secondDeposit.setSubmissionPolicyId(firstDepositCreated.getSubmissionPolicyId());
    secondDeposit.addItem(this.depositITService.createLocalDepositDataFile(secondDeposit));
    final Deposit secondDepositCreated = this.depositITService.createRemoteDepositWithData(secondDeposit);
    this.depositITService.waitDepositWithDataIsReady(secondDepositCreated.getResId());
    this.depositITService.addContributorToDeposit(secondDepositCreated.getResId(), this.personITService.getOrCreateRemotePerson().getResId());
    this.depositITService.checkSubmissionAgreement(secondDepositCreated);
    this.submitAndApproveDeposit(true, secondDepositCreated);
    assertTrue(this.depositITService.waitDepositIsCompleted(secondDepositCreated.getResId()), "Second deposit in error");
  }

  @Order(31)
  @Test
  void approveDepositWithSubmissionAgreementForAllDepositTest() {
    // Create first deposit
    final Deposit firstDepositCreated = this.archiveFirstDeposit(
            "Temporary deposit with submission agreement type for all deposit",
            true,
            SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST);
    assertNotNull(firstDepositCreated, "First deposit creation failed");

    // Create a new deposit and see how there is not need to approveSubmissionAgreement
    final Deposit secondDeposit = this.depositITService.createLocalDepositWithSubmissionApprove(DLCMTestConstants.PersistenceMode.TEMPORARY,
            "Temporary deposit 2 with submission agreement type for all deposit", DEFAULT_DESCRIPTION, Role.MANAGER_ID, false);
    secondDeposit.setSubmissionPolicyId(firstDepositCreated.getSubmissionPolicyId());
    secondDeposit.addItem(this.depositITService.createLocalDepositDataFile(secondDeposit));
    final Deposit secondDepositCreated = this.depositITService.createRemoteDepositWithData(secondDeposit);
    this.depositITService.waitDepositWithDataIsReady(secondDepositCreated.getResId());
    this.depositITService.addContributorToDeposit(secondDepositCreated.getResId(), this.personITService.getOrCreateRemotePerson().getResId());
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.depositITService.checkSubmissionAgreement(secondDepositCreated));
    assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode(), "Check submission agreement failed");
    e = assertThrows(HttpClientErrorException.class, () -> this.depositITService.submitDeposit(secondDepositCreated));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Deposit submission failed");
    this.depositITService.approveSubmissionAgreement(secondDepositCreated);
    this.submitAndApproveDeposit(true, secondDepositCreated);
    assertTrue(this.depositITService.waitDepositIsCompleted(secondDepositCreated.getResId()), "Second deposit in error");
  }

  private Deposit createFirstDeposit(String depositTile, boolean withApproval, String submissionPolicyName) {
    final OrganizationalUnit orgUnit = withApproval
            ? this.orgUnitITService.getOrCreateOrganizationalUnitWithApproval(DLCMTestConstants.PersistenceMode.PERMANENT, DLCMTestConstants.OrganizationalUnitStatus.OPEN,
                    Role.MANAGER_ID, AuthApplicationRole.USER_ID, false)
            : this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(DLCMTestConstants.PersistenceMode.PERMANENT, DLCMTestConstants.OrganizationalUnitStatus.OPEN,
                    Role.MANAGER_ID, AuthApplicationRole.USER_ID, false);
    final Deposit deposit = withApproval
            ? this.depositITService.createLocalDepositWithSubmissionApprove(DLCMTestConstants.PersistenceMode.TEMPORARY, depositTile, DEFAULT_DESCRIPTION, Role.MANAGER_ID, false)
            : this.depositITService.createLocalDeposit(DLCMTestConstants.PersistenceMode.TEMPORARY, depositTile, DEFAULT_DESCRIPTION, Role.MANAGER_ID, false,
                    DLCMMetadataVersion.getDefaultVersion());
    List<OrganizationalUnitSubmissionPolicyDTO> orgUnitSubmissionPolicyList = this.orgUnitITService.getSubmissionPoliciesFromOrgUnit(orgUnit);
    List<SubmissionPolicy> submissionList = orgUnitSubmissionPolicyList.stream()
            .filter(sp -> sp.getName().equals(DLCMTestConstants.getNameWithPermanentLabel(submissionPolicyName)))
            .collect(Collectors.toList());
    if (!submissionList.isEmpty()) {
      deposit.setSubmissionPolicyId(submissionList.get(0).getResId());
      deposit.addItem(this.depositITService.createLocalDepositDataFile(deposit));
      final Deposit firstDepositCreated = this.depositITService.createRemoteDepositWithData(deposit);
      this.depositITService.waitDepositWithDataIsReady(firstDepositCreated.getResId());
      this.depositITService.addContributorToDeposit(firstDepositCreated.getResId(), this.personITService.getOrCreateRemotePerson().getResId());
      return this.depositITService.findDeposit(firstDepositCreated.getResId());
    } else {
      fail("Missing submission policies");
    }
    return null;
  }

  private Deposit archiveFirstDeposit(String depositTile, boolean withApproval, String submissionPolicyName) {
    final Deposit firstDepositCreated = this.createFirstDeposit(depositTile, withApproval, submissionPolicyName);
    assertNotNull(firstDepositCreated, "First deposit creation failed");
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.depositITService.checkSubmissionAgreement(firstDepositCreated));
    assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode(), "Check submission agreement failed");
    // Cannot submit without approving the submission agreement
    e = assertThrows(HttpClientErrorException.class, () -> this.depositITService.submitDeposit(firstDepositCreated));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Deposit submission failed");
    this.depositITService.approveSubmissionAgreement(firstDepositCreated);
    this.depositITService.checkSubmissionAgreement(firstDepositCreated);
    this.submitAndApproveDeposit(withApproval, firstDepositCreated);
    assertTrue(this.depositITService.waitDepositIsCompleted(firstDepositCreated.getResId()), "Deposit in error");
    return this.depositITService.findDeposit(firstDepositCreated.getResId());
  }

  private void submitAndApproveDeposit(boolean withApproval, Deposit deposit) {
    if (withApproval) {
      this.depositITService.submitDeposit(deposit);
      this.restClientTool.sudoAdmin();
      this.depositITService.approveDeposit(deposit);
      this.restClientTool.exitSudo();
    } else {
      this.depositITService.approveDeposit(deposit);
    }

  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.submissionAgreementITService.cleanTestData();
    this.depositITService.cleanTestData();
    this.restClientTool.exitSudo();
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

@Disabled("Run these tests manually")
class MultipleDepositsIT extends AbstractPreingestIT {

  @Autowired
  public MultipleDepositsIT(Environment env, SudoRestClientTool restClientTool, OrgUnitITService orgUnitITService,
          DepositITService depositITService, DepositClientService depositClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
  }

  private static final Logger log = LoggerFactory.getLogger(MultipleDepositsIT.class);

  private final static int NB_DEPOSITS = 100;
  private final static int NB_THREADS = 10;
  private final static String DEPOSIT_TITLE = "Multiple deposit ";

  @Test
  void createMultipleDeposits() throws InterruptedException {
    this.runParallelCalls(this::createDeposit, NB_THREADS, NB_DEPOSITS);
  }

  private void createDeposit() {
    log.info("Creating new deposit");
    Deposit deposit = this.depositITService.createRemoteDepositWithContributor(PersistenceMode.TEMPORARY, DEPOSIT_TITLE, DEPOSIT_TITLE);
    deposit.addItem(this.depositITService.createLocalDepositDataFile(deposit));
    deposit = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.waitDepositWithDataIsReady(deposit.getResId());
    this.depositITService.approveDeposit(deposit);
  }
}

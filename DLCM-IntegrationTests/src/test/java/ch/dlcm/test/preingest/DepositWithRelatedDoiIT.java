/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositWithRelatedDoiIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.Role;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

class DepositWithRelatedDoiIT extends AbstractDepositWithRoleIT {

  @Autowired
  public DepositWithRelatedDoiIT(Environment env, SudoRestClientTool restClientTool, OrgUnitITService orgUnitITService,
          DepositITService depositITService, DepositClientService depositClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
  }

  @ParameterizedTest
  @MethodSource("validCombinations")
  void createDepositWithRelatedDoi(String identicalToDoi, String obsoletedByDoi, List<String> referencedByDoi) {
    final String title = "Deposit with valid related DOIs";
    final Deposit deposit = this.createDepositWithDoi(title, identicalToDoi, obsoletedByDoi, referencedByDoi);
    Deposit newDeposit = this.depositClientService.create(deposit);
    assertNotNull(newDeposit);
    if (!StringTool.isNullOrEmpty(identicalToDoi)) {
      assertFalse(StringTool.isNullOrEmpty(newDeposit.getIsIdenticalTo()));
    }
    if (!StringTool.isNullOrEmpty(obsoletedByDoi)) {
      assertFalse(StringTool.isNullOrEmpty(newDeposit.getIsObsoletedBy()));
    }
    assertEquals(referencedByDoi.size(), newDeposit.getIsReferencedBy().size());
  }

  @ParameterizedTest
  @MethodSource("invalidCombinations")
  void createDepositWithWrongRelatedDoi(String identicalToDoi, String obsoletedByDoi, List<String> referencedByDoi) {
    final String title = "Deposit with invalid related DOIs";
    final Deposit deposit = this.createDepositWithDoi(title, identicalToDoi, obsoletedByDoi, referencedByDoi);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.depositClientService.create(deposit));
  }

  private Deposit createDepositWithDoi(String title, String identicalToDoi, String obsoletedByDoi, List<String> referencedByDoi) {
    final Deposit deposit = this.createLocalDepositForCurrentRole(title);
    if (!StringTool.isNullOrEmpty(identicalToDoi)) {
      deposit.setIsIdenticalTo(identicalToDoi);
    }
    if (!StringTool.isNullOrEmpty(obsoletedByDoi)) {
      deposit.setIsObsoletedBy(obsoletedByDoi);
      ;
    }
    if (!referencedByDoi.isEmpty()) {
      deposit.setIsReferencedBy(referencedByDoi);
    }
    return deposit;
  }

  private static Stream<Arguments> validCombinations() {
    return Stream.of(
            arguments(null, null, Collections.emptyList()),
            arguments("10.12345/dlcm:identicalTo", null, Collections.emptyList()),
            arguments(null, "10.12345/dlcm:obsoletedBy", Collections.emptyList()),
            arguments(null, null, List.of("10.12345/dlcm:referencedBy1")),
            arguments(null, null, List.of("10.12345/dlcm:referencedBy1", "10.12345/dlcm:referencedBy2")),
            arguments(null, null, List.of("10.12345/dlcm:referencedBy1", "10.12345/dlcm:referencedBy2", "10.12345/dlcm:referencedBy3")));
  }

  private static Stream<Arguments> invalidCombinations() {
    return Stream.of(
            arguments("10.345/dlcm:identicalTo", null, Collections.emptyList()),
            arguments(null, "10.345/dlcm:obsoletedBy", Collections.emptyList()),
            arguments(null, null, List.of("10.345/dlcm:referencedBy1")),
            arguments(null, null, List.of("10.345/dlcm:referencedBy1", "10.345/dlcm:referencedBy2")),
            arguments(null, null, List.of("10.345/dlcm:referencedBy1", "10.345/dlcm:referencedBy2", "10.345/dlcm:referencedBy3")),
            arguments(null, null, List.of("10.12345/dlcm:referencedBy1", "10.12345/dlcm:referencedBy2", "10.345/dlcm:referencedBy3")));
  }

  @Override
  protected String role() {
    return Role.CREATOR_ID;
  }
}

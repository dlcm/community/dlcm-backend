/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositFileNumberAndSizeIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.MetadataITService;
import ch.dlcm.test.service.OrgUnitITService;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DepositFileNumberAndSizeIT extends AbstractPreingestIT {

  private static final String YARETA_DATA = "deposit/dlcm/Yareta-small.zip";
  private static final int YARETA_FILE_NUM = 3;

  private AipITService aipITService;
  private MetadataITService metadataITService;

  @Autowired
  public DepositFileNumberAndSizeIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService,
          AipITService aipITService,
          MetadataITService metadataITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.aipITService = aipITService;
    this.metadataITService = metadataITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Order(10)
  @Test
  void testArchiveFileNumberAndSize() {
    // Create
    final Deposit deposit = this.createDeposit();
    final ArchivalInfoPackage aip = this.getAIP(deposit);
    // Check AIP
    this.checkAip(aip, YARETA_FILE_NUM + 1, 0, 0, 0);
    // Check metadata
    final ArchiveMetadata archiveMetadata = this.metadataITService.getArchiveMetadata(aip.getResId());
    this.checkMetadata(archiveMetadata, YARETA_FILE_NUM, YARETA_FILE_NUM + 1, 1, aip.getArchiveSize());
  }

  @Order(20)
  @Test
  void testUpdateFileNumberAndSize() {
    // Create
    final Deposit deposit0 = this.createDeposit();
    final ArchivalInfoPackage aip0 = this.getAIP(deposit0);
    this.checkAip(aip0, YARETA_FILE_NUM + 1, 0, 0, 0);
    // Archive metadata
    final ArchiveMetadata archiveMetadata0 = this.metadataITService.getArchiveMetadata(aip0.getResId());
    this.checkMetadata(archiveMetadata0, YARETA_FILE_NUM, YARETA_FILE_NUM + 1, 1, aip0.getArchiveSize());
    // Update #1
    final Deposit deposit1 = this.updateDeposit(deposit0);
    final ArchivalInfoPackage aip1 = this.getAIP(deposit1);
    this.checkAip(aip1, YARETA_FILE_NUM + 2, 1, 0, 0);
    // Archive metadata
    final ArchiveMetadata archiveMetadata1 = this.metadataITService.getArchiveMetadata(aip1.getResId());
    this.checkMetadata(archiveMetadata1, YARETA_FILE_NUM, YARETA_FILE_NUM + 2, 2, aip1.getArchiveSize());
    // Update #2
    final Deposit deposit2 = this.updateDeposit(deposit1);
    final ArchivalInfoPackage aip2 = this.getAIP(deposit2);
    this.checkAip(aip2, YARETA_FILE_NUM + 3, 2, 0, 0);
    // Archive metadata
    final ArchiveMetadata archiveMetadata2 = this.metadataITService.getArchiveMetadata(aip2.getResId());
    this.checkMetadata(archiveMetadata2, YARETA_FILE_NUM, YARETA_FILE_NUM + 3, 3, aip2.getArchiveSize());
  }

  @Order(30)
  @Test
  void testCollectionWithOneArchiveFileNumberAndSize() {
    List<ArchivalInfoPackage> aipList = new ArrayList<>();
    // Create
    final Deposit deposit = this.createDeposit();
    final ArchivalInfoPackage aip = this.getAIP(deposit);
    aipList.add(aip);
    this.checkAip(aip, YARETA_FILE_NUM + 1, 0, 0, 0);
    // Check metadata
    final ArchiveMetadata archiveMetadata = this.metadataITService.getArchiveMetadata(aip.getResId());
    this.checkMetadata(archiveMetadata, YARETA_FILE_NUM, YARETA_FILE_NUM + 1, 1, aip.getArchiveSize());
    // Create collection
    final Deposit collectionDeposit = this.createCollection(aipList);
    final ArchivalInfoPackage collectionAip = this.getAIP(collectionDeposit);
    final long totalSize = collectionAip.getArchiveSize() + aip.getArchiveSize();
    this.checkAip(collectionAip, 1, 0, YARETA_FILE_NUM, totalSize);
    // Check metadata
    final ArchiveMetadata collectionMetadata = this.metadataITService.getArchiveMetadata(collectionAip.getResId());
    this.checkMetadata(collectionMetadata, YARETA_FILE_NUM, 1, 1, totalSize);
  }

  @Order(40)
  @Test
  void testCollectionWithTwoArchivesFileNumberAndSize() {
    List<ArchivalInfoPackage> aipList = new ArrayList<>();
    // Create #1
    final Deposit deposit1 = this.createDeposit();
    final ArchivalInfoPackage aip1 = this.getAIP(deposit1);
    aipList.add(aip1);
    // Create #2
    final Deposit deposit2 = this.createDeposit();
    final ArchivalInfoPackage aip2 = this.getAIP(deposit2);
    aipList.add(aip2);
    // Create collection
    final Deposit collectionDeposit = this.createCollection(aipList);
    final ArchivalInfoPackage collectionAip = this.getAIP(collectionDeposit);
    final long totalSize = collectionAip.getArchiveSize() + aip1.getArchiveSize() + aip2.getArchiveSize();
    this.checkAip(collectionAip, 1, 0, YARETA_FILE_NUM * 2, totalSize);
    // Check metadata
    final ArchiveMetadata collectionMetadata = this.metadataITService.getArchiveMetadata(collectionAip.getResId());
    this.checkMetadata(collectionMetadata, YARETA_FILE_NUM * 2, 1, 1, totalSize);
  }

  @Order(50)
  @Test
  void testUpdateCollectionWithTwoArchivesFileNumberAndSize() {
    List<ArchivalInfoPackage> aipList = new ArrayList<>();
    // Create #1
    final Deposit deposit1 = this.createDeposit();
    final ArchivalInfoPackage aip1 = this.getAIP(deposit1);
    aipList.add(aip1);
    // Create #2
    final Deposit deposit2 = this.createDeposit();
    final ArchivalInfoPackage aip2 = this.getAIP(deposit2);
    aipList.add(aip2);
    // Create collection
    final Deposit collectionDeposit0 = this.createCollection(aipList);
    final ArchivalInfoPackage collectionAip0 = this.getAIP(collectionDeposit0);
    final long totalSize0 = collectionAip0.getArchiveSize() + aip1.getArchiveSize() + aip2.getArchiveSize();
    this.checkAip(collectionAip0, 1, 0, YARETA_FILE_NUM * 2, totalSize0);
    // Check metadata
    final ArchiveMetadata collectionMetadata0 = this.metadataITService.getArchiveMetadata(collectionAip0.getResId());
    this.checkMetadata(collectionMetadata0, YARETA_FILE_NUM * 2, 1, 1, totalSize0);
    // Update #1
    final Deposit collectionDeposit1 = this.updateDeposit(collectionDeposit0);
    final ArchivalInfoPackage collectionAip1 = this.getAIP(collectionDeposit1);
    final long totalSize1 = collectionAip1.getArchiveSize() + aip1.getArchiveSize() + aip2.getArchiveSize();
    this.checkAip(collectionAip1, 2, 1, YARETA_FILE_NUM * 2, totalSize1);
    // Check metadata
    final ArchiveMetadata collectionMetadata1 = this.metadataITService.getArchiveMetadata(collectionAip1.getResId());
    this.checkMetadata(collectionMetadata1, YARETA_FILE_NUM * 2, 2, 2, totalSize1);
    // Update #2
    final Deposit collectionDeposit2 = this.updateDeposit(collectionDeposit1);
    final ArchivalInfoPackage collectionAip2 = this.getAIP(collectionDeposit2);
    final long totalSize2 = collectionAip2.getArchiveSize() + aip1.getArchiveSize() + aip2.getArchiveSize();
    this.checkAip(collectionAip2, 3, 2, YARETA_FILE_NUM * 2, totalSize2);
    // Check metadata
    final ArchiveMetadata collectionMetadata2 = this.metadataITService.getArchiveMetadata(collectionAip2.getResId());
    this.checkMetadata(collectionMetadata2, YARETA_FILE_NUM * 2, 3, 3, totalSize2);
  }

  @Order(60)
  @Test
  void testCollectionWithTwoUpdatedArchivesFileNumberAndSize() {
    List<ArchivalInfoPackage> aipList = new ArrayList<>();
    // Create #1
    final Deposit deposit1 = this.createDeposit();
    this.getAIP(deposit1);
    // Update x2
    final Deposit deposit11 = this.updateDeposit(deposit1);
    this.getAIP(deposit11);
    final Deposit deposit12 = this.updateDeposit(deposit11);
    ArchivalInfoPackage aip1 = this.getAIP(deposit12);
    aipList.add(aip1);
    // Create #2
    final Deposit deposit2 = this.createDeposit();
    this.getAIP(deposit2);
    // Update x1
    final Deposit deposit21 = this.updateDeposit(deposit2);
    final ArchivalInfoPackage aip2 = this.getAIP(deposit21);
    aipList.add(aip2);
    // Create collection
    final Deposit collectionDeposit = this.createCollection(aipList);
    final ArchivalInfoPackage collectionAip = this.getAIP(collectionDeposit);
    final long totalSize = collectionAip.getArchiveSize() + aip1.getArchiveSize() + aip2.getArchiveSize();
    this.checkAip(collectionAip, 1, 0, YARETA_FILE_NUM * 2, totalSize);
    // Check metadata
    final ArchiveMetadata collectionMetadata = this.metadataITService.getArchiveMetadata(collectionAip.getResId());
    this.checkMetadata(collectionMetadata, YARETA_FILE_NUM * 2, 1, 1, totalSize);
  }

  private Deposit updateDeposit(Deposit deposit) {
    Deposit workingDeposit = this.depositITService.startEditingMetadata(deposit);
    workingDeposit.setTitle(workingDeposit.getTitle() + "-");
    Deposit updatedDeposit = this.depositITService.updateDeposit(workingDeposit.getResId(), workingDeposit);
    return this.approveDeposit(updatedDeposit);
  }

  private ArchivalInfoPackage getAIP(Deposit deposit) {
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
    final String aipId = this.metadataITService.getAipIdFromDepositId(deposit.getResId());
    return this.aipITService.findAIP(aipId);
  }

  private Deposit createDeposit() {
    final Deposit deposit = this.depositITService.createRemoteDepositWithContributor(PersistenceMode.TEMPORARY, "Deposit for file number",
            DEFAULT_DESCRIPTION);
    final String depositId = deposit.getResId();
    this.depositITService.uploadArchiveToDeposit(depositId, new ClassPathResource(YARETA_DATA));
    this.depositITService.waitDepositWithDataIsReady(depositId);
    return this.approveDeposit(deposit);
  }

  private Deposit createCollection(List<ArchivalInfoPackage> aipList) {
    final Deposit deposit = this.depositITService.createRemoteDepositWithContributor(PersistenceMode.TEMPORARY,
            "Deposit Collection for file number", DEFAULT_DESCRIPTION);
    final String depositId = deposit.getResId();
    for (ArchivalInfoPackage aip : aipList) {
      this.depositITService.addArchive(depositId, aip.getResId());
    }
    this.depositITService.waitDepositWithDataIsReady(depositId);
    return this.approveDeposit(deposit);
  }

  private Deposit approveDeposit(Deposit deposit) {
    this.depositITService.approveDeposit(deposit);
    assertTrue(this.depositITService.waitDepositIsCompleted(deposit.getResId()), "Deposit not completed: " + deposit.getResId());
    return this.depositITService.findDeposit(deposit.getResId());
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - MetadataFileUpdateIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.ArchiveContainer;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrderITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class MetadataFileUpdateIT extends AbstractMetadataEditionIT {
  @Autowired
  public MetadataFileUpdateIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService,
          AipITService aipITService,
          MetadataClientService metadataClientService,
          OrderITService orderITService,
          PersonITService personITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService, aipITService, metadataClientService, orderITService, personITService);
  }

  @Test
  void updateFilesDepositTest() {
    Deposit deposit = this.createDepositWithCompletedAip();
    List<DepositDataFile> listDepositDataFile = this.depositClientService.getDepositDataFile(deposit.getResId());
    int depositFilesBeforeUpdate = listDepositDataFile.size();

    List<ArchivalInfoPackage> aipsListBeforeUpdate = this.aipITService.getAipClientService().searchByProperties(Map.of("info.name", deposit.getTitle()));
    assertEquals(1, aipsListBeforeUpdate.size());

    deposit = this.depositITService.startEditingMetadata(deposit);
    String title = DLCMTestConstants.getRandomNameWithTemporaryLabel("Deposit v1");
    deposit.setTitle(title);

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setDataCategory(DataCategory.Internal);
    fileUploadDto.setDataType(DataCategory.ArchiveThumbnail);
    this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource("deposit/dlcm/thumbnail/OLOS.timg"), fileUploadDto);

    this.depositClientService.update(deposit.getResId(), deposit);
    this.depositITService.waitDepositWithDataIsReady(deposit.getResId());
    this.depositITService.approveDeposit(deposit);
    this.depositITService.waitDepositIsCompleted(deposit.getResId());
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());

    Map<String, String> properties = new HashMap<>();
    properties.put("title", title);
    properties.put("organizationalUnitId", deposit.getOrganizationalUnitId());
    Deposit depositChanged = this.depositClientService.searchByProperties(properties).get(0);

    listDepositDataFile = this.depositClientService.getDepositDataFile(deposit.getResId());

    assertEquals(deposit.getTitle(), depositChanged.getTitle());
    assertEquals(depositFilesBeforeUpdate + 2, listDepositDataFile.size());

    List<ArchivalInfoPackage> aipsListAfterUpdate = this.aipITService.getAipClientService().searchByProperties(Map.of("info.name", deposit.getTitle()));
    assertEquals(1, aipsListAfterUpdate.size());

    // Check aip file
    assertEquals(aipsListBeforeUpdate.get(0).getResId(), aipsListAfterUpdate.get(0).getResId());
    assertEquals(aipsListBeforeUpdate.get(0).getMetadataVersion(), aipsListAfterUpdate.get(0).getMetadataVersion());
    assertEquals(aipsListBeforeUpdate.get(0).getArchiveId(), aipsListAfterUpdate.get(0).getArchiveId());
    assertEquals(aipsListBeforeUpdate.get(0).getArchiveUri(), aipsListAfterUpdate.get(0).getArchiveUri());

    // check if order has been already created
    ArchivalInfoPackage aip = aipsListAfterUpdate.get(0);
    String aipId = aip.getResId();
    String orderId = this.metadataClientService.prepareDownload(aipId, DLCMConstants.DISSEMINATION_POLICY_OAIS_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    // Download updated archive from Access module
    SolidifyTime.waitInSeconds(8);
    Path packagedArchivePath = Paths.get(System.getProperty("java.io.tmpdir"), "archiveToUpdate.tmp");
    this.metadataClientService.downloadArchive(aipId, packagedArchivePath, orderId);
    assertNotNull(packagedArchivePath.toFile());

    // Extract archive
    Path extractedArchivePath = Paths.get(System.getProperty("java.io.tmpdir"), "archiveToUpdate");
    ZipTool zipAip = new ZipTool(packagedArchivePath.toString());
    if (aip.getArchiveContainer() == ArchiveContainer.ZIP) {
      zipAip.unzipFiles(extractedArchivePath, true);
    } else if (aip.getArchiveContainer() == ArchiveContainer.BAG_IT) {
      zipAip.unzipFolder(extractedArchivePath, DLCMConstants.DATA_FOLDER + "/", true);
    }

    // Check that there is a file called archive.thumbnail
    Path folderPath = Paths.get(extractedArchivePath + "/" + aipId);
    long filesNumber = FileTool.findFiles(folderPath).stream()
            .filter(path -> path.getFileName().toString().contains(DLCMConstants.ARCHIVE_THUMBNAIL)).count();
    assertEquals(1, filesNumber);

    FileTool.deleteFolder(extractedArchivePath);
  }

  @Test
  void cannotUpdateCertainFileTypesDepositTest() {
    Deposit deposit = this.createDepositWithCompletedAip();
    deposit = this.depositITService.startEditingMetadata(deposit);
    String resId = deposit.getResId();

    String title = DLCMTestConstants.getRandomNameWithTemporaryLabel("Deposit v1");
    deposit.setTitle(title);

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setDataCategory(DataCategory.Administrative);
    fileUploadDto.setDataType(DataCategory.Document);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
            () -> this.depositClientService.uploadDepositDataFile(resId, new ClassPathResource("geneve.jpg"), fileUploadDto));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
  }

  @ParameterizedTest
  @MethodSource("allVersions")
  void cannotUploadPrimaryDataTest(DLCMMetadataVersion version) {
    Deposit deposit = this.createDepositWithCompletedAip(version);
    final String depositId = deposit.getResId();
    // Cannot add when deposit completed
    DepositDataFile df1 = this.depositITService.createDataFile(deposit);
    // Cannot upload when deposit completed
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.depositITService.addDataFileToDeposit(depositId, df1));
    final FileUploadDto<DepositDataFile> fileInfo = new FileUploadDto<>();
    fileInfo.setDataCategory(DataCategory.Primary);
    fileInfo.setDataType(DataCategory.Experimental);
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.depositITService.uploadDataFileToDeposit(depositId, new ClassPathResource(YARETA_DATA), fileInfo));

    deposit = this.depositITService.startEditingMetadata(deposit);

    // Cannot add when editing metadata
    DepositDataFile df2 = this.depositITService.createDataFile(deposit);
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.depositITService.addDataFileToDeposit(depositId, df2));
    // Cannot upload data file
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.depositITService.uploadDataFileToDeposit(depositId, new ClassPathResource(YARETA_DATA), fileInfo));
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.depositITService.uploadArchiveToDeposit(depositId, new ClassPathResource(YARETA_DATA), fileInfo));
  }

  @ParameterizedTest
  @MethodSource("allVersions")
  void uploadMetadataFileTest(DLCMMetadataVersion version) {
    Deposit deposit = this.createDepositWithCompletedAip(version);
    final String depositId = deposit.getResId();
    deposit = this.depositITService.startEditingMetadata(deposit);
    // Thumbnail
    final FileUploadDto<DepositDataFile> thumbnaiInfo = new FileUploadDto<>();
    thumbnaiInfo.setDataCategory(DataCategory.Internal);
    thumbnaiInfo.setDataType(DataCategory.ArchiveThumbnail);
    // Readme
    final FileUploadDto<DepositDataFile> readmeInfo = new FileUploadDto<>();
    readmeInfo.setDataCategory(DataCategory.Internal);
    readmeInfo.setDataType(DataCategory.ArchiveReadme);
    // DUA
    final FileUploadDto<DepositDataFile> duaInfo = new FileUploadDto<>();
    duaInfo.setDataCategory(DataCategory.Internal);
    duaInfo.setDataType(DataCategory.ArchiveDataUseAgreement);
    // Add file
    if (version.getVersionNumber() >= DLCMMetadataVersion.V4_0.getVersionNumber()) {
      assertDoesNotThrow(() -> this.depositITService.uploadDataFileToDeposit(depositId, new ClassPathResource(DLCMTestConstants.THUMBNAIL_FILE), thumbnaiInfo));
      assertDoesNotThrow(() -> this.depositITService.uploadDataFileToDeposit(depositId, new ClassPathResource(DLCMTestConstants.README_FILE), readmeInfo));
      assertDoesNotThrow(() -> this.depositITService.uploadDataFileToDeposit(depositId, new ClassPathResource(DLCMTestConstants.SAMPLE_DUA_FILE), duaInfo));
    } else {
      assertThrows(HttpClientErrorException.BadRequest.class,
              () -> this.depositITService.uploadDataFileToDeposit(depositId, new ClassPathResource(DLCMTestConstants.THUMBNAIL_FILE), thumbnaiInfo));
      assertThrows(HttpClientErrorException.BadRequest.class,
              () -> this.depositITService.uploadDataFileToDeposit(depositId, new ClassPathResource(DLCMTestConstants.README_FILE), readmeInfo));
      assertThrows(HttpClientErrorException.BadRequest.class,
              () -> this.depositITService.uploadDataFileToDeposit(depositId, new ClassPathResource(DLCMTestConstants.SAMPLE_DUA_FILE), duaInfo));
    }

  }

  @ParameterizedTest
  @MethodSource("metadataFiles")
  void updatedMetadataFiles(DataCategory dataType, String fileName, String secondFile) {
    Deposit deposit = this.createDepositWithCompletedAip();
    final String depositId = deposit.getResId();

    this.depositITService.startEditingMetadata(deposit);

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setDataCategory(DataCategory.Internal);
    fileUploadDto.setDataType(DataCategory.ArchiveThumbnail);
    assertDoesNotThrow(
            () -> this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource(fileName), fileUploadDto));

    this.depositITService.waitDepositWithDataIsReady(deposit.getResId());
    this.depositITService.approveDeposit(deposit);
    this.depositITService.waitDepositIsCompleted(deposit.getResId());
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());

    // Update deposit once completed
    this.depositITService.startEditingMetadata(deposit);

    // Cannot upload duplicate file
    assertThrows(HttpServerErrorException.InternalServerError.class,
            () -> this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource(fileName), fileUploadDto));

    assertDoesNotThrow(() -> this.depositClientService.uploadDepositDataFile(depositId, new ClassPathResource(secondFile), fileUploadDto));

  }

}

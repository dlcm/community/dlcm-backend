/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - MetadataFileUpdateIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrderITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

abstract class AbstractMetadataEditionIT extends AbstractPreingestIT {

  protected static final String TEMP_DEPOSIT_WITH_COMPLETED_AIP_TITLE = "Temporary deposit with completed AIP ";
  protected static final String TEMP_DEPOSIT_TITLE = "Temporary deposit ";

  private static final Logger log = LoggerFactory.getLogger(AbstractMetadataEditionIT.class);

  protected final AipITService aipITService;
  protected final OrderITService orderITService;
  protected final MetadataClientService metadataClientService;
  protected final PersonITService personITService;

  protected AbstractMetadataEditionIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          DepositClientService depositClientService,
          AipITService aipITService,
          MetadataClientService metadataClientService,
          OrderITService orderITService,
          PersonITService personITService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
    this.aipITService = aipITService;
    this.metadataClientService = metadataClientService;
    this.orderITService = orderITService;
    this.personITService = personITService;
  }

  protected Deposit createDepositWithCompletedAip() {
    return this.createDepositWithCompletedAip(DLCMMetadataVersion.getDefaultVersion());
  }

  protected Deposit createDepositWithCompletedAip(DLCMMetadataVersion metadataVersion) {
    // Create and complete a deposit
    this.restClientTool.sudoAdmin();
    Deposit deposit = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, TEMP_DEPOSIT_WITH_COMPLETED_AIP_TITLE,
            DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
    deposit.setMetadataVersion(metadataVersion);
    deposit = this.depositClientService.create(deposit);

    final Person person1 = this.personITService.getOrCreateRemotePerson();
    this.depositClientService.addContributor(deposit.getResId(), person1.getResId());
    final Person person2 = this.personITService.getOrCreateRemotePersonWithOrcid();
    this.depositClientService.addContributor(deposit.getResId(), person2.getResId());

    // TODO: this.restClientTool.exitSudo();
    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrganizationalUnit(deposit.getOrganizationalUnitId());
    return this.getDepositWithCompleteAip(orgUnit, deposit);
  }

  protected Deposit getDepositWithCompleteAip(OrganizationalUnit orgUnit, Deposit deposit) {
    this.restClientTool.sudoAdmin();
    deposit.addItem(this.depositITService.createLocalDepositDataFile(deposit));
    final Deposit deposit1 = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.waitDepositWithDataIsReady(deposit1.getResId());
    this.depositITService.approveDeposit(deposit1);
    this.depositITService.waitDepositIsCompleted(deposit1.getResId());
    this.aipITService.waitAIPIsReady(orgUnit.getResId(), deposit.getTitle());
    log.info("Creating a Deposit : {} ({}) with status in : {}", deposit1.getTitle(), deposit1.getResId(), deposit1.getStatus());
    Deposit depositToReturn = this.depositClientService.searchByProperties(Map.of("title", TEMP_DEPOSIT_WITH_COMPLETED_AIP_TITLE)).get(0);
    this.restClientTool.exitSudo();
    return depositToReturn;
  }

  protected Deposit createDepositWithApproveAndCompletedAip() {
    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithApproval(PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN, Role.MANAGER_ID, AuthApplicationRole.USER_ID, false);
    // Create and complete a deposit
    this.restClientTool.sudoAdmin();
    final Deposit deposit = this.depositITService.createRemoteDepositWithContributorsAndSubmissionApprove(PersistenceMode.TEMPORARY,
            TEMP_DEPOSIT_WITH_COMPLETED_AIP_TITLE, DEFAULT_DESCRIPTION);
    deposit.addItem(this.depositITService.createLocalDepositDataFile(deposit));
    final Deposit deposit1 = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.waitDepositWithDataIsReady(deposit1.getResId());
    this.depositITService.submitDeposit(deposit1);
    this.depositITService.waitDepositIsInValidation(deposit1.getResId());
    this.depositITService.approveDeposit(deposit1);
    this.depositITService.waitDepositIsCompleted(deposit1.getResId());
    this.aipITService.waitAIPIsReady(orgUnit.getResId(), deposit.getTitle());
    log.info("Creating a Deposit : {} ({}) with status in : {}", deposit1.getTitle(), deposit1.getResId(), deposit1.getStatus());
    Deposit depositToReturn = this.depositClientService.searchByProperties(Map.of("title", TEMP_DEPOSIT_WITH_COMPLETED_AIP_TITLE)).get(0);
    this.restClientTool.exitSudo();
    return depositToReturn;
  }

}

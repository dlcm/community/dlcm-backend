/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositDataFileIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preingest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.util.SearchOperation;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

class DepositDataFileIT extends AbstractPreingestIT {

  private static final Logger log = LoggerFactory.getLogger(DepositDataFileIT.class);

  @Autowired
  public DepositDataFileIT(Environment env, SudoRestClientTool restClientTool, OrgUnitITService orgUnitITService,
          DepositITService depositITService, DepositClientService depositClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, depositClientService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  @Disabled("Test not ready, to be refactored")
  void approveAllTest() {
    boolean error = false;
    for (final Deposit deposit : this.depositClientService.findAll()) {
      if (deposit.getStatus() == DepositStatus.IN_PROGRESS || deposit.getStatus() == DepositStatus.IN_VALIDATION) {
        final List<DepositDataFile> dataFileList = this.depositClientService.getDepositDataFile(deposit.getResId());
        if (dataFileList.size() > 0 && !this.isErrorInDataFileList(dataFileList)) {
          // Wait until all data file downloaded
          this.depositITService.waitDataFileIsReady(deposit.getResId(), dataFileList.size());
          deposit.setStatus(DepositStatus.APPROVED);
          final Deposit deposit2 = this.depositClientService.update(deposit.getResId(), deposit);
          assertNotNull(deposit2);
          // Check status
          if (!this.depositITService.waitDepositIsCompleted(deposit.getResId())) {
            error = true;
            log.info("Not approved " + deposit.getTitle() + " (" + deposit.getResId() + ")");
          } else {
            log.info("Approved " + deposit.getTitle() + " (" + deposit.getResId() + ")");
          }
        } else {
          log.info("Not processed " + deposit.getTitle() + " (" + deposit.getResId() + ")");
        }
      }
    }
    // Check if error
    assertTrue(error);
  }

  @Test
  void approveTest() {

    final String[] filesToUpload = { "bootstrap.yml" };
    final String[] metadataFilesToUpload = { "dlcm.xml" };

    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Submission", DEFAULT_DESCRIPTION);
    // Upload files & approve deposit
    this.uploadAndApproveDeposit(deposit, metadataFilesToUpload, filesToUpload, DepositStatus.COMPLETED);
  }

  @Test
  void collectionBeginTest() {

    final String[] filesToUpload = { "bootstrap.yml" };
    final String[] metadataFilesToUpload = { "dlcm.xml" };

    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Collection Begin Range", DEFAULT_DESCRIPTION);
    deposit.setCollectionBegin(OffsetDateTime.now(ZoneOffset.UTC).minusYears(5));

    // Upload files & approve deposit
    this.uploadAndApproveDeposit(deposit, metadataFilesToUpload, filesToUpload, DepositStatus.COMPLETED);
  }

  @Test
  void collectionEndTest() {

    final String[] filesToUpload = { "bootstrap.yml" };
    final String[] metadataFilesToUpload = { "dlcm.xml" };

    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Collection End Range", DEFAULT_DESCRIPTION);
    deposit.setCollectionEnd(OffsetDateTime.now(ZoneOffset.UTC).minusYears(5));

    // Upload files & approve deposit
    this.uploadAndApproveDeposit(deposit, metadataFilesToUpload, filesToUpload, DepositStatus.COMPLETED);
  }

  @Test
  void collectionRangeTest() {

    final String[] filesToUpload = { "bootstrap.yml" };
    final String[] metadataFilesToUpload = { "dlcm.xml" };

    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Collection Range", DEFAULT_DESCRIPTION);
    deposit.setCollectionBegin(OffsetDateTime.now(ZoneOffset.UTC).minusYears(5));
    deposit.setCollectionEnd(deposit.getCollectionBegin().plusYears(3));

    // Upload files & approve deposit
    this.uploadAndApproveDeposit(deposit, metadataFilesToUpload, filesToUpload, DepositStatus.COMPLETED);
  }

  @Test
  void creationTest() {
    // Create a local deposit with data file
    final Deposit localDeposit = this.createLocalDepositWithDataFile();

    // Create remote deposit with data file
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(localDeposit);

    // Fetch the deposit data file list from the server
    final List<DepositDataFile> listDepositDataFile = this.depositClientService.getDepositDataFile(remoteDeposit.getResId());

    // Test the creation
    assertEquals(localDeposit.getDataFiles().get(0).getResId(), listDepositDataFile.get(0).getResId());
  }

  /**
   * Tries to create a new DepositDataFile with a existing resId corresponding to a DepositDataFile linked to another deposit
   */
  @Test
  void creationWithExistingResIdLinkedToAnotherDepositTest() {

    // Create remote deposit with data file
    final Deposit localDeposit1 = this.createLocalDepositWithDataFile();
    final Deposit remoteDeposit1 = this.depositITService.createRemoteDepositWithData(localDeposit1);

    // Create remote deposit with data file
    final Deposit localDeposit2 = this.createLocalDepositWithDataFile();
    final Deposit remoteDeposit2 = this.depositITService.createRemoteDepositWithData(localDeposit2);

    // Get existing data file resId on the first deposit
    final String existingResId = this.depositClientService.getDepositDataFile(remoteDeposit1.getResId()).get(0).getResId();

    // Create a new data file with the same resId on the second deposit
    final DepositDataFile newDatafile = this.depositITService.createLocalDepositDataFile(remoteDeposit2);
    newDatafile.setResId(existingResId);

    boolean success;
    try {
      this.depositClientService.addDepositDataFile(remoteDeposit2.getResId(), newDatafile);
      success = true;
    } catch (final Exception ex) {
      success = false;
    }

    assertFalse(success);
  }

  /**
   * Tries to create a new DepositDataFile with a existing resId corresponding to a DepositDataFile already linked to the same deposit
   */
  @Test
  void creationWithExistingResIdLinkedToSameDepositTest() {

    // Create a local deposit with data file
    final Deposit localDeposit = this.createLocalDepositWithDataFile();

    // Create remote deposit with data file
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(localDeposit);

    // Fetch the deposit data file list from the server
    final List<DepositDataFile> listDepositDataFile = this.depositClientService.getDepositDataFile(remoteDeposit.getResId());

    final String existingResId = listDepositDataFile.get(0).getResId();

    // Create a new data file with the same resId
    final DepositDataFile newDatafile = this.depositITService.createLocalDepositDataFile(remoteDeposit);
    newDatafile.setResId(existingResId);

    boolean success;
    try {
      this.depositClientService.addDepositDataFile(remoteDeposit.getResId(), newDatafile);
      success = true;
    } catch (final Exception ex) {
      success = false;
    }

    assertFalse(success);
  }

  @Test
  void creationWithResIdTest() {
    // Create a local deposit with data file
    final Deposit localDeposit = this.createLocalDepositWithDataFile();

    final String resId = StringTool.generateResId();

    localDeposit.getDataFiles().get(0).setResId(resId);

    // Create remote deposit with data file
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(localDeposit);

    // Fetch the deposit data file list from the server
    final List<DepositDataFile> listDepositDataFile = this.depositClientService.getDepositDataFile(remoteDeposit.getResId());

    // Test the creation
    assertEquals(1, listDepositDataFile.size());
    assertEquals(resId, listDepositDataFile.get(0).getResId());
  }

  @Test
  void deleteTest() {
    // Create a local deposit with data file
    final Deposit localDeposit = this.createLocalDepositWithDataFile();

    // Create remote deposit with data file
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(localDeposit);

    // Test if data file is present
    int depositListSize = this.depositClientService.getDepositDataFile(remoteDeposit.getResId()).size();
    assertEquals(1, depositListSize);

    // Remove the data file
    final String depositDataFileId = localDeposit.getDataFiles().get(0).getResId();

    // Delete the data file
    this.depositClientService.removeDepositDataFile(remoteDeposit.getResId(), depositDataFileId);

    // Test if data file is absent
    depositListSize = this.depositClientService.getDepositDataFile(remoteDeposit.getResId()).size();
    assertEquals(0, depositListSize);

  }

  @Test
  void deleteWhenReadyTest() {
    // Create a local deposit with data file
    final Deposit localDeposit = this.createLocalDepositWithDataFile();

    // Create remote deposit with data file
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(localDeposit);

    // Wait until data file is ready
    this.depositITService.waitDataFileIsReady(remoteDeposit.getResId(), 1);

    // Remove the data file
    final String depositDataFileId = localDeposit.getDataFiles().get(0).getResId();

    // Delete the data file
    this.depositClientService.removeDepositDataFile(remoteDeposit.getResId(), depositDataFileId);

    // Test if data file is absent
    final int depositListSize = this.depositClientService.getDepositDataFile(remoteDeposit.getResId()).size();
    assertEquals(0, depositListSize);

  }

  @Test
  void downloadTest() {
    // Create a local deposit with data file
    final Deposit localDeposit = this.createLocalDepositWithDataFile();

    // Create remote deposit with data file
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(localDeposit);

    // Wait that the file is processed
    this.depositITService.waitDataFileIsReady(remoteDeposit.getResId(), 1);

    // Download the file
    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.depositClientService.downloadDataFile(remoteDeposit.getResId(), localDeposit.getDataFiles().get(0).getResId(), path);
    assertEquals(REFERENCE_FILE_SIZE, path.toFile().length());
  }

  @Test
  void uploadDuplicateDataFiles() {
    final String fileToUpload = "empty.txt";

    // Create a local deposit with data file
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Testing", DEFAULT_DESCRIPTION);

    this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource(fileToUpload));

    this.depositITService.waitDataFileIsReady(deposit.getResId(), 1);
    this.uploadDuplicateDataFile(deposit.getResId(), fileToUpload, null);
  }

  @ParameterizedTest
  @MethodSource("metadataFiles")
  void uploadDuplicateMetadataFiles(DataCategory dataType, String fileName) {
    // Create a local deposit with data file
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Testing", DEFAULT_DESCRIPTION);

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setDataCategory(DataCategory.Internal);
    fileUploadDto.setDataType(DataCategory.ArchiveThumbnail);
    assertDoesNotThrow(
            () -> this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource(fileName), fileUploadDto));
    this.depositITService.waitDataFileIsReady(deposit.getResId(), 1);
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource(fileName), fileUploadDto));
  }

  @Test
  void duplicateDataFilesWithSameRelativePaths() {
    final String fileToUpload = "empty.txt";
    final String relativeLocation = "/sub-folder/test";

    // Create a local deposit with data file
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Duplicate", DEFAULT_DESCRIPTION);

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setRelLocation(relativeLocation);
    this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource(fileToUpload), fileUploadDto);

    this.depositITService.waitDataFileIsReady(deposit.getResId(), 1);

    fileUploadDto.setRelLocation(relativeLocation + "/");
    this.uploadDuplicateDataFile(deposit.getResId(), fileToUpload, fileUploadDto);
    fileUploadDto.setRelLocation(relativeLocation + "//");
    this.uploadDuplicateDataFile(deposit.getResId(), fileToUpload, fileUploadDto);
    fileUploadDto.setRelLocation(relativeLocation + "///");
    this.uploadDuplicateDataFile(deposit.getResId(), fileToUpload, fileUploadDto);
    fileUploadDto.setRelLocation("/" + relativeLocation);
    this.uploadDuplicateDataFile(deposit.getResId(), fileToUpload, fileUploadDto);
    fileUploadDto.setRelLocation("//" + relativeLocation);
    this.uploadDuplicateDataFile(deposit.getResId(), fileToUpload, fileUploadDto);
    fileUploadDto.setRelLocation("///" + relativeLocation);
    this.uploadDuplicateDataFile(deposit.getResId(), fileToUpload, fileUploadDto);
    fileUploadDto.setRelLocation("/" + relativeLocation + "/");
    this.uploadDuplicateDataFile(deposit.getResId(), fileToUpload, fileUploadDto);
    fileUploadDto.setRelLocation("//" + relativeLocation + "//");
    this.uploadDuplicateDataFile(deposit.getResId(), fileToUpload, fileUploadDto);
    fileUploadDto.setRelLocation("/sub-folder/////test");
    this.uploadDuplicateDataFile(deposit.getResId(), fileToUpload, fileUploadDto);
  }

  @Test
  void createDuplicateDataFiles() {
    // Create new deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Testing duplicate datafiles",
            DEFAULT_DESCRIPTION);

    String depositId = deposit.getResId();
    String relativeLocation1 = "/folder1";
    String relativeLocation2 = "/folder2";

    // add dataFile in relativeLocation1
    final DepositDataFile dataFile1 = new DepositDataFile(deposit);
    dataFile1.setSourceData(URI.create(REFERENCE_FILE));
    dataFile1.setRelativeLocation(relativeLocation1);
    this.depositClientService.addDepositDataFile(depositId, dataFile1);
    List<DepositDataFile> dataFilesList = this.depositClientService.getDepositDataFile(deposit.getResId());
    assertEquals(1, dataFilesList.size());

    // add same dataFile in relativeLocation2
    final DepositDataFile dataFile2 = new DepositDataFile(deposit);
    dataFile2.setSourceData(URI.create(REFERENCE_FILE));
    dataFile2.setRelativeLocation(relativeLocation2);
    this.depositClientService.addDepositDataFile(depositId, dataFile2);
    dataFilesList = this.depositClientService.getDepositDataFile(deposit.getResId());
    assertEquals(2, dataFilesList.size());

    // trying to add same file in relativeLocation1 again fails
    final DepositDataFile dataFile3 = new DepositDataFile(deposit);
    dataFile3.setSourceData(URI.create(REFERENCE_FILE));
    dataFile3.setRelativeLocation(relativeLocation1);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
            () -> this.depositClientService.addDepositDataFile(depositId, dataFile3));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    this.assertFieldContainsValidationErrorMessage(e, "fileName",
            "The file named 'budget-2013.pdf' already exists in the relative location '/folder1'");

    dataFilesList = this.depositClientService.getDepositDataFile(deposit.getResId());
    assertEquals(2, dataFilesList.size());
  }

  @Test
  void createDataFilesInSameRelativeLocationDifferentCase() {
    // Create new deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Testing duplicate datafiles",
            DEFAULT_DESCRIPTION);

    String depositId = deposit.getResId();
    String relativeLocation1 = "/folder";
    String relativeLocation2 = "/FOLDER";
    String relativeLocation3 = "/folDER";

    // add dataFile in relativeLocation1
    final DepositDataFile dataFile1 = new DepositDataFile(deposit);
    dataFile1.setSourceData(URI.create(REFERENCE_FILE));
    dataFile1.setRelativeLocation(relativeLocation1);
    this.depositClientService.addDepositDataFile(depositId, dataFile1);
    List<DepositDataFile> dataFilesList = this.depositClientService.getDepositDataFile(deposit.getResId());
    assertEquals(1, dataFilesList.size());

    // add same dataFile in relativeLocation2
    final DepositDataFile dataFile2 = new DepositDataFile(deposit);
    dataFile2.setSourceData(URI.create(REFERENCE_FILE));
    dataFile2.setRelativeLocation(relativeLocation2);
    this.depositClientService.addDepositDataFile(depositId, dataFile2);
    dataFilesList = this.depositClientService.getDepositDataFile(deposit.getResId());
    assertEquals(2, dataFilesList.size());

    // add same dataFile in relativeLocation3
    final DepositDataFile dataFile3 = new DepositDataFile(deposit);
    dataFile3.setSourceData(URI.create(REFERENCE_FILE));
    dataFile3.setRelativeLocation(relativeLocation3);
    this.depositClientService.addDepositDataFile(depositId, dataFile3);
    dataFilesList = this.depositClientService.getDepositDataFile(deposit.getResId());
    assertEquals(3, dataFilesList.size());
  }

  @Test
  void createDataFilesSameFileNameDifferentCase() {
    // Create new deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Testing duplicate datafiles",
            DEFAULT_DESCRIPTION);

    String depositId = deposit.getResId();
    String relativeLocation = "/folder";
    String sourceData1 = REFERENCE_FILE;
    String sourceData2 = REFERENCE_FILE.toUpperCase();

    final DepositDataFile dataFile1 = new DepositDataFile(deposit);
    dataFile1.setSourceData(URI.create(sourceData1));
    dataFile1.setRelativeLocation(relativeLocation);
    this.depositClientService.addDepositDataFile(depositId, dataFile1);
    List<DepositDataFile> dataFilesList = this.depositClientService.getDepositDataFile(deposit.getResId());
    assertEquals(1, dataFilesList.size());

    final DepositDataFile dataFile2 = new DepositDataFile(deposit);
    dataFile2.setSourceData(URI.create(sourceData2));
    dataFile2.setRelativeLocation(relativeLocation);
    this.depositClientService.addDepositDataFile(depositId, dataFile2);
    dataFilesList = this.depositClientService.getDepositDataFile(deposit.getResId());
    assertEquals(2, dataFilesList.size());
  }

  @Test
  void duplicateDataFilesInDifferentRelativeLocation() {
    final String fileToUpload = "empty.txt";
    final String relativeLocation = "/sub-folder";

    // Create a local deposit with data file
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Duplicate", DEFAULT_DESCRIPTION);

    this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource(fileToUpload),
            new FileUploadDto<>());

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setRelLocation(relativeLocation);
    this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource(fileToUpload), fileUploadDto);

    final List<DepositDataFile> list = this.depositClientService.getDepositDataFile(deposit.getResId());

    // Test the upload
    assertEquals(2, list.size());
  }

  @Test
  void duplicateDataFilesSpecifyingRelLocation() {
    final String fileToUpload = "empty.txt";
    final String relativeLocation = "/sub-folder";

    // Create a local deposit with data file
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Testing", DEFAULT_DESCRIPTION);

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setRelLocation(relativeLocation);
    this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource(fileToUpload), fileUploadDto);

    this.depositITService.waitDataFileIsReady(deposit.getResId(), 1);
    boolean success;
    try {
      this.depositClientService.uploadDepositDataFile(deposit.getResId(), new ClassPathResource(fileToUpload), fileUploadDto);
      success = true;
    } catch (final Exception ex) {
      success = false;
    }

    assertFalse(success);
  }

  @Test
  void sameFileNameSameRelativeLocationDifferentDeposit() {
    final String fileToUpload = "empty.txt";

    // Create a local deposit with data file
    final Deposit deposit1 = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Deposit1", DEFAULT_DESCRIPTION);
    final Deposit deposit2 = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Deposit2", DEFAULT_DESCRIPTION);

    final String depositId1 = deposit1.getResId();
    final ClassPathResource file = new ClassPathResource(fileToUpload);
    final FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();

    // upload file in deposit 1
    this.depositClientService.uploadDepositDataFile(depositId1, new ClassPathResource(fileToUpload), new FileUploadDto<>());

    // upload same file a second time fails
    assertThrows(HttpServerErrorException.InternalServerError.class,
            () -> this.depositClientService.uploadDepositDataFile(depositId1, file, fileUploadDto));

    // Test the upload in deposit 1
    final List<DepositDataFile> list1 = this.depositClientService.getDepositDataFile(deposit1.getResId());
    assertEquals(1, list1.size());

    // same file in deposit 2 succeeds
    final String depositId2 = deposit2.getResId();
    assertDoesNotThrow(() -> this.depositClientService.uploadDepositDataFile(depositId2, new ClassPathResource(fileToUpload),
            new FileUploadDto<>()));

    // Test the upload in deposit 2
    final List<DepositDataFile> list2 = this.depositClientService.getDepositDataFile(deposit2.getResId());
    assertEquals(1, list2.size());
  }

  @Test
  void emptyFileTest() {

    final String[] filesToUpload = { "bootstrap.yml", "empty.txt" };
    final String[] metadataFilesToUpload = { "dlcm.xml" };

    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Empty file", DEFAULT_DESCRIPTION);
    // Upload files & approve deposit
    this.uploadAndApproveDeposit(deposit, metadataFilesToUpload, filesToUpload, DepositStatus.COMPLETED);
  }

  @Test
  void invalidMetadataTest() {

    final String[] filesToUpload = { "bootstrap.yml" };
    final String[] metadataFilesToUpload = { "dlcm-invalid.xml" };

    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Submission", DEFAULT_DESCRIPTION);
    // Upload files & approve deposit
    this.uploadAndApproveDeposit(deposit, metadataFilesToUpload, filesToUpload, DepositStatus.IN_ERROR);
  }

  @Test
  void partialUpdateDepositDataFileTest() {
    // Create a new archive
    List<DepositDataFile> depositDataFileList = this.uploadArchiveWithTypeTest("problematicDatafiles.zip", 22);
    final DepositDataFile depositDataFile = depositDataFileList.get(0);
    final String depositId = depositDataFile.getInfoPackage().getResId();
    this.depositITService.waitDataFileIsReady(depositId, 22);

    final DepositDataFile result = this.depositClientService.updateDepositDataFile(depositId,
            depositDataFile.getResId(), Map.of("dataCategory", "Primary", "dataType", "Simulation"));

    assertEquals("Primary", result.getDataCategory().toString());
    assertEquals("Simulation", result.getDataType().toString());
  }

  @Test
  void uploadSameFileTest() {
    // Test Bug DLCM-267

    // Create a local deposit with data file
    final Deposit localDeposit = this.createLocalDepositWithDataFile();

    // Create remote deposit with data file
    Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(localDeposit);

    // Wait until data file downloaded
    this.depositITService.waitDataFileIsReady(remoteDeposit.getResId(), 1);

    // Remove the data file
    final String depositDataFileId = localDeposit.getDataFiles().get(0).getResId();

    // Delete the data file
    this.depositClientService.removeDepositDataFile(remoteDeposit.getResId(), depositDataFileId);

    // Test if data file is absent
    final int depositListSize = this.depositClientService.getDepositDataFile(remoteDeposit.getResId()).size();
    assertEquals(0, depositListSize);

    // upload the same deposit
    remoteDeposit = this.depositITService.createRemoteDepositWithData(localDeposit);

    // Wait until data file downloaded
    this.depositITService.waitDataFileIsReady(remoteDeposit.getResId(), 1);

  }

  @Test
  void searchTest() {
    Deposit deposit = this.depositITService.getRemotePermanentDeposit(DLCMTestConstants.PERMANENT_DEPOSIT);
    String depositId = deposit.getResId();
    DepositDataFile depositDataFile = this.depositClientService.getDepositDataFile(depositId).get(0);

    assertTrue(this.isDepositDataFilePresent("dataCategory:PRIMARY", null, depositId, depositDataFile),
            "No Primary datafile found");
    assertTrue(this.isDepositDataFilePresent("status:READY", null, depositId, depositDataFile),
            "No READY datafile found");
    assertTrue(this.isDepositDataFilePresent("dataCategory:PRIMARY, status:READY", SearchOperation.OR_PREDICATE_FLAG, depositId,
            depositDataFile), "No Primary or READY datafile found");

  }

  @Test
  @Disabled("See https://issues.unige.ch/browse/DLCM-2699")
  void searchCaseInsensitiveTest() {
    Deposit deposit = this.depositITService.getRemotePermanentDeposit(DLCMTestConstants.PERMANENT_DEPOSIT);
    String depositId = deposit.getResId();
    DepositDataFile depositDataFile = this.depositClientService.getDepositDataFile(depositId).get(0);

    assertTrue(this.isDepositDataFilePresent("i-dataCategory:Primary", null, depositId, depositDataFile),
            "No Primary datafile found");
  }

  private void updateRelativeLocation(String targetFolder, String fileName) {
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Deposit for relativeLocation",
            DEFAULT_DESCRIPTION);

    final String depositId = deposit.getResId();

    final DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(depositId,
            new ClassPathResource(fileName));

    this.depositITService.waitDataFileIsReady(depositId, 1);

    final DepositDataFile dataFileMoved1 = this.depositClientService.updateDepositDataFile(depositId, depositDataFile.getResId(),
            Map.of("relativeLocation", targetFolder));

    assertEquals(new ClassPathResource("/" + targetFolder), new ClassPathResource(dataFileMoved1.getRelativeLocation()));

    this.depositITService.waitDataFileIsReady(depositId, 1);

    final DepositDataFile dataFileMoved2 = this.depositClientService.updateDepositDataFile(deposit.getResId(),
            depositDataFile.getResId(),
            Map.of("relativeLocation", "/"));

    assertEquals(new ClassPathResource("/"), new ClassPathResource(dataFileMoved2.getRelativeLocation()));
  }

  @Test
  void updateRelativeLocationTest() {
    this.updateRelativeLocation("other", "bootstrap.yml");
  }

  @Test
  void updateRelativeLocationWithSpaceInFolderTest() {
    this.updateRelativeLocation("other folder", "bootstrap.yml");
  }

  @Test
  void updateRelativeLocationWithSpecificCharInFolderTest() {
    this.updateRelativeLocation("otheréàè%$£^&*()[]{}folder", "bootstrap.yml");
  }

  @Test
  void updateRelativeLocationWithPlusInFolderTest() {
    this.updateRelativeLocation("other+folder with plus", "bootstrap.yml");
  }

  @Test
  void updateRelativeLocationWithSpaceInFileTest() {
    this.updateRelativeLocation("other", "file with space.txt");
  }

  @Test
  void updateRelativeLocationWithSpecificCharInFileTest() {
    this.updateRelativeLocation("other", "file with éàè%$£^&()[]{}.txt");
  }

  @Test
  void updateRelativeLocationWithPlusInFileTest() {
    this.updateRelativeLocation("other", "file with +.txt");
  }

  @Test
  void updateDataCategoryTest() {

    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Deposit for updateDataCategory",
            DEFAULT_DESCRIPTION);

    final String depositId = deposit.getResId();

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setDataCategory(DataCategory.Primary);
    fileUploadDto.setDataType(DataCategory.Reference);
    final DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(depositId,
            new ClassPathResource("bootstrap.yml"),
            fileUploadDto);

    this.depositITService.waitDataFileIsReady(depositId, 1);

    final DepositDataFile dataFile2 = this.depositClientService.updateDepositDataFile(depositId, depositDataFile.getResId(),
            Map.of("dataCategory", DataCategory.Secondary,
                    "dataType", DataCategory.Documentation));

    this.depositITService.waitDataFileIsReady(depositId, 1);

    assertEquals(DataCategory.Secondary.toString(), dataFile2.getDataCategory().toString());
    assertEquals(DataCategory.Documentation.toString(), dataFile2.getDataType().toString());

    final DepositDataFile dataFileMoved2 = this.depositClientService.updateDepositDataFile(depositId, depositDataFile.getResId(),
            Map.of("dataCategory", DataCategory.Software,
                    "dataType", DataCategory.Code));

    this.depositITService.waitDataFileIsReady(depositId, 1);

    assertEquals(DataCategory.Software, dataFileMoved2.getDataCategory());
    assertEquals(DataCategory.Code, dataFileMoved2.getDataType());
  }

  @Test
  void uploadArchiveTest() {
    // Create a new deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Deposit for uploadArchive",
            DEFAULT_DESCRIPTION);

    final String depositId = deposit.getResId();

    /*
     * datafiles.zip contains 3 files
     */
    this.depositClientService.uploadDepositDataFileArchive(depositId, new ClassPathResource("datafiles.zip"),
            new FileUploadDto<>());

    // Wait until data files are extracted, uploaded and treated
    this.depositITService.waitDataFileIsReady(deposit.getResId(), 3);

    /*
     * Checks that relative locations reflect the ZIP structure
     */
    final List<DepositDataFile> dataFilesList = this.depositClientService.getDepositDataFile(depositId);
    for (final DepositDataFile file : dataFilesList) {

      ClassPathResource expectedRelativeLocation = switch (file.getFileName()) {
        case "Logo-DLCM.png" -> new ClassPathResource("/datafiles");
        case "datafile-1.txt" -> new ClassPathResource("/datafiles/subfolder-1");
        case "lorem-ipsum.pdf" -> new ClassPathResource("/datafiles/subfolder-1/subfolder-2");
        default -> null;
      };

      assertEquals(expectedRelativeLocation, new ClassPathResource(file.getRelativeLocation()));
    }
  }

  @Test
  void uploadInvalidArchiveTest() {
    // Create a new deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Deposit for uploadArchive",
            DEFAULT_DESCRIPTION);

    final String depositId = deposit.getResId();
    // Corrupted ZIP file
    this.uploadInvalidArchive(depositId, "datafiles-corrupted.zip");
    // ZIP file with accentuated characters
    // this.uploadInvalidArchive(depositId, "datafiles-encoded.zip");
  }

  @Test
  void uploadArchiveWithCustomMetadataTest() {
    // Create a new deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Deposit for uploadArchive",
            DEFAULT_DESCRIPTION);

    /*
     * datafiles.zip contains 3 files
     */
    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setDataCategory(DataCategory.Package);
    fileUploadDto.setDataType(DataCategory.CustomMetadata);
    fileUploadDto.setMetadataType("xml-1.0");
    this.depositClientService.uploadDepositDataFileArchive(deposit.getResId(), new ClassPathResource("metadata.zip"),
            fileUploadDto);

    // Wait until data files are extracted, uploaded and processed
    this.depositITService.waitDataFileIsReady(deposit.getResId(), 2);

    /*
     * Checks that relative locations reflect the ZIP structure
     */
    final List<DepositDataFile> dataFilesList = this.depositClientService.getDepositDataFile(deposit.getResId());
    // Test the upload
    assertEquals(2, dataFilesList.size());
  }

  @Test
  void uploadArchiveWithTypeTest() {
    this.uploadArchiveWithTypeTest("datafiles.zip", 3);
  }

  @Test
  void uploadFileWithValidUserChecksumTest() {
    final String file1ToUpload = "empty.txt";
    final String tempFolder1 = "test3";
    final String checksumMd5 = "d41d8cd98f00b204e9800998ecf8427e";
    final String checksumSha1 = "da39a3ee5e6b4b0d3255bfef95601890afd80709";
    final String checksumSha256 = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";
    final String checksumCrc32 = "00000000";

    final Map<ChecksumAlgorithm, String> checksums = new EnumMap<>(ChecksumAlgorithm.class);
    Map<ChecksumAlgorithm, DataFileChecksum.ChecksumOrigin> checksumsOrigin = new EnumMap<>(ChecksumAlgorithm.class);
    checksums.put(ChecksumAlgorithm.MD5, checksumMd5);
    checksumsOrigin.put(ChecksumAlgorithm.MD5, DataFileChecksum.ChecksumOrigin.USER);
    checksums.put(ChecksumAlgorithm.SHA1, checksumSha1);
    checksumsOrigin.put(ChecksumAlgorithm.SHA1, DataFileChecksum.ChecksumOrigin.USER);
    checksums.put(ChecksumAlgorithm.SHA256, checksumSha256);
    checksumsOrigin.put(ChecksumAlgorithm.SHA256, DataFileChecksum.ChecksumOrigin.PORTAL);
    checksums.put(ChecksumAlgorithm.CRC32, checksumCrc32);
    checksumsOrigin.put(ChecksumAlgorithm.CRC32, DataFileChecksum.ChecksumOrigin.PORTAL);

    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

    // Upload file
    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setRelLocation(tempFolder1);
    fileUploadDto.setChecksums(checksums);
    fileUploadDto.setChecksumsOrigin(checksumsOrigin);
    DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
            new ClassPathResource(file1ToUpload),
            fileUploadDto);

    final List<DepositDataFile> list = this.depositClientService.getDepositDataFile(deposit.getResId());

    // Test the upload
    assertEquals(1, list.size());

    assertFalse(list.contains(depositDataFile));
  }

  @Test
  void uploadFileWithInvalidUserChecksumTest() {
    final String file1ToUpload = "empty.txt";
    final String tempFolder1 = "test4";
    final String invalidChecksumMd5 = "11111111111111111111111111111111";
    final String invalidChecksumSha1 = "1111111111111111111111111111111111111111";
    final String invalidChecksumSha256 = "1111111111111111111111111111111111111111111111111111111111111111";
    final String invalidChecksumCrc32 = "11111111";

    final Map<ChecksumAlgorithm, String> checksums = new EnumMap<>(ChecksumAlgorithm.class);
    Map<ChecksumAlgorithm, DataFileChecksum.ChecksumOrigin> checksumsOrigin = new EnumMap<>(ChecksumAlgorithm.class);
    checksums.put(ChecksumAlgorithm.MD5, invalidChecksumMd5);
    checksumsOrigin.put(ChecksumAlgorithm.MD5, DataFileChecksum.ChecksumOrigin.USER);
    checksums.put(ChecksumAlgorithm.SHA1, invalidChecksumSha1);
    checksumsOrigin.put(ChecksumAlgorithm.SHA1, DataFileChecksum.ChecksumOrigin.USER);
    checksums.put(ChecksumAlgorithm.SHA256, invalidChecksumSha256);
    checksumsOrigin.put(ChecksumAlgorithm.SHA256, DataFileChecksum.ChecksumOrigin.PORTAL);
    checksums.put(ChecksumAlgorithm.CRC32, invalidChecksumCrc32);
    checksumsOrigin.put(ChecksumAlgorithm.CRC32, DataFileChecksum.ChecksumOrigin.PORTAL);

    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

    // Upload file
    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setRelLocation(tempFolder1);
    fileUploadDto.setChecksums(checksums);
    fileUploadDto.setChecksumsOrigin(checksumsOrigin);
    DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
            new ClassPathResource(file1ToUpload),
            fileUploadDto);

    final List<DepositDataFile> list = this.depositClientService.getDepositDataFile(deposit.getResId());

    // Test the upload
    assertEquals(1, list.size());

    assertFalse(list.contains(depositDataFile));
  }

  @Test
  void uploadProblematicArchiveWithTypeTest() {
    this.uploadArchiveWithTypeTest("problematicDatafiles.zip", 22);
  }

  @Test
  void uploadTest() {
    final String fileToUpload = "bootstrap.yml";
    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

    // Upload file
    final DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
            new ClassPathResource(fileToUpload));

    final List<DepositDataFile> list = this.depositClientService.getDepositDataFile(deposit.getResId());

    // Test the upload
    assertEquals(1, list.size());
    assertEquals(list.get(0).getResId(), depositDataFile.getResId());
    assertTrue(list.get(0).getSourceData().toString().endsWith(fileToUpload));
    assertEquals(list.get(0).getFileName(), fileToUpload);
  }

  @Test
  void uploadWithRelativeLocationTest() {
    final String fileToUpload = "bootstrap.yml";
    final String relativeLocation = "/sub-folder";
    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

    // Upload file
    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setRelLocation(relativeLocation);
    final DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
            new ClassPathResource(fileToUpload), fileUploadDto);

    final List<DepositDataFile> list = this.depositClientService.getDepositDataFile(deposit.getResId());

    // Test the upload
    assertEquals(1, list.size());
    assertEquals(list.get(0).getResId(), depositDataFile.getResId());
    assertTrue(list.get(0).getSourceData().toString().endsWith(fileToUpload));
    assertEquals(list.get(0).getFileName(), fileToUpload);
    assertEquals(list.get(0).getRelativeLocation(), relativeLocation);
  }

  @Test
  void validateRelativeLocationWithEndingSlashTest() {
    final String fileToUpload = "bootstrap.yml";
    final String relativeLocation = "/sub-folder/";
    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

    // Upload file
    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setRelLocation(relativeLocation);
    final DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
            new ClassPathResource(fileToUpload), fileUploadDto);

    final List<DepositDataFile> list = this.depositClientService.getDepositDataFile(deposit.getResId());

    // Test the upload
    assertEquals(1, list.size());
    assertEquals(list.get(0).getResId(), depositDataFile.getResId());
    assertTrue(list.get(0).getSourceData().toString().endsWith(fileToUpload));
    assertEquals(list.get(0).getFileName(), fileToUpload);
    assertEquals(list.get(0).getRelativeLocation(), relativeLocation.substring(0, relativeLocation.length() - 1));

  }

  @Test
  void validateRelativeLocationWithoutStartingSlashTest() {
    final String fileToUpload = "bootstrap.yml";
    final String relativeLocation = "sub-folder";
    // Create a deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE, DEFAULT_DESCRIPTION);

    // Upload file
    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setRelLocation(relativeLocation);
    final DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
            new ClassPathResource(fileToUpload), fileUploadDto);

    final List<DepositDataFile> list = this.depositClientService.getDepositDataFile(deposit.getResId());

    // Test the upload
    assertEquals(1, list.size());
    assertEquals(list.get(0).getResId(), depositDataFile.getResId());
    assertTrue(list.get(0).getSourceData().toString().endsWith(fileToUpload));
    assertEquals(list.get(0).getFileName(), fileToUpload);
    assertEquals(list.get(0).getRelativeLocation(), "/" + relativeLocation);
  }

  private Deposit createLocalDepositWithDataFile() {
    // Create a deposit object
    final Deposit deposit = this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, DEFAULT_TITLE);

    // Add the deposit data file to the deposit object
    deposit.addItem(this.depositITService.createLocalDepositDataFile(deposit));
    return deposit;
  }

  private boolean isDepositDataFilePresent(String search, String matchType, String depositId, DepositDataFile depositDataFile) {
    final List<DepositDataFile> depositDataFileList = this.depositClientService.search(depositId, search, matchType);
    for (final DepositDataFile df : depositDataFileList) {
      if (df.getResId().equals(depositDataFile.getResId())) {
        return true;
      }
    }
    return false;
  }

  private boolean isErrorInDataFileList(List<DepositDataFile> dataFileList) {
    return dataFileList.stream().anyMatch(df -> df.getStatus() == DataFileStatus.IN_ERROR);
  }

  private void uploadAndApproveDeposit(Deposit deposit, String[] metadataFilesToUpload, String[] filesToUpload, DepositStatus endStatus) {

    // Upload files
    for (final String fileToUpload : filesToUpload) {
      final DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
              new ClassPathResource(fileToUpload));
      assertNotNull(depositDataFile);
    }

    for (final String fileToUpload : metadataFilesToUpload) {
      FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
      fileUploadDto.setDataCategory(DataCategory.Package);
      fileUploadDto.setDataType(DataCategory.Metadata);
      final DepositDataFile depositDataFile = this.depositClientService.uploadDepositDataFile(deposit.getResId(),
              new ClassPathResource(fileToUpload),
              fileUploadDto);
      assertNotNull(depositDataFile);
    }

    // Test the uploads
    final List<DepositDataFile> list = this.depositClientService.getDepositDataFile(deposit.getResId());
    assertEquals(list.size(), (filesToUpload.length + metadataFilesToUpload.length));

    // Wait until data file downloaded
    this.depositITService.waitDataFileIsReady(deposit.getResId(), filesToUpload.length + metadataFilesToUpload.length);

    // Approve deposit
    this.depositClientService.approve(deposit.getResId());

    // Check status
    assertTrue(this.depositITService.waitDepositIsProcessed(deposit.getResId(), endStatus), "Deposit not completed without error");
  }

  private List<DepositDataFile> uploadArchiveWithTypeTest(String archiveFileName, int dataFileNumber) {
    // Create a new deposit
    final Deposit deposit = this.depositITService.createRemoteDeposit(PersistenceMode.TEMPORARY, "Deposit for uploadArchive", null);

    FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
    fileUploadDto.setDataCategory(DataCategory.Secondary);
    fileUploadDto.setDataType(DataCategory.Publication);
    final List<DepositDataFile> depositDataFileList = this.depositClientService.uploadDepositDataFileArchive(deposit.getResId(),
            new ClassPathResource(archiveFileName), fileUploadDto);

    // Wait until data files are extracted, uploaded and treated
    this.depositITService.waitDataFileIsReady(deposit.getResId(), dataFileNumber);

    /*
     * Check that all files are uploaded with the given DataCategory and DataType
     */
    final List<DepositDataFile> dataFileList = this.depositClientService.getDepositDataFile(deposit.getResId());
    for (final DepositDataFile file : dataFileList) {
      assertEquals(DataCategory.Secondary, file.getDataCategory());
      assertEquals(DataCategory.Publication, file.getDataType());
    }

    return depositDataFileList;
  }

  private void uploadDuplicateDataFile(String depositId, String fileToUpload, FileUploadDto<DepositDataFile> fileUploadDto) {
    try {
      if (fileUploadDto == null) {
        this.depositClientService.uploadDepositDataFile(depositId, new ClassPathResource(fileToUpload));
      } else {
        this.depositClientService.uploadDepositDataFile(depositId, new ClassPathResource(fileToUpload), fileUploadDto);
      }
      assertTrue(false, "Must have an exception");
    } catch (final HttpServerErrorException e) {
      assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatusCode());
      assertTrue(e.getMessage().contains("duplicate"));
    }
  }

  private void uploadInvalidArchive(String depositId, String archiveToUpload) {
    try {
      this.depositClientService.uploadDepositDataFileArchive(depositId, new ClassPathResource(archiveToUpload));
      assertTrue(false, "Must have an exception");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      assertTrue(e.getMessage().contains("not a valid ZIP file"));
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AipStorageIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.archivalstorage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.MetadataITService;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AipStorageIT extends AbstractIT {

  private static final String YARETA_DATA = "deposit/dlcm/Yareta-small.zip";

  private DepositITService depositITService;
  private AipITService aipITService;
  private MetadataITService metadataITService;

  @Autowired
  public AipStorageIT(
          Environment env,
          SudoRestClientTool restClientTool,
          DepositITService depositITService,
          AipITService aipITService,
          MetadataITService metadataITService) {
    super(env, restClientTool);
    this.depositITService = depositITService;
    this.aipITService = aipITService;
    this.metadataITService = metadataITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  private static Stream<Arguments> updateScenario() {
    return Stream.of(
            arguments("Deposit to secured storage",
                    DataTag.YELLOW, DataUsePolicy.CLICK_THROUGH_DUA,
                    DataTag.ORANGE, DataUsePolicy.SIGNED_DUA),
            arguments("Deposit from secured storage",
                    DataTag.ORANGE, DataUsePolicy.SIGNED_DUA,
                    DataTag.YELLOW, DataUsePolicy.CLICK_THROUGH_DUA));
  }

  private static Stream<Arguments> versionsToMigrate() {
    return Stream.of(
            arguments(DLCMMetadataVersion.V1_0),
            arguments(DLCMMetadataVersion.V1_1),
            arguments(DLCMMetadataVersion.V2_0),
            arguments(DLCMMetadataVersion.V2_1),
            arguments(DLCMMetadataVersion.V3_0));
  }

  @Order(10)
  @ParameterizedTest
  @MethodSource("updateScenario")
  void testUpdateSecureStorage(String title, DataTag dataTagSource, DataUsePolicy dataUsePolicySource, DataTag dataTagTarget,
          DataUsePolicy dataUsePolicyTarget) {
    this.runUpdateTest(title, dataTagSource, dataUsePolicySource, dataTagTarget, dataUsePolicyTarget);
  }

  @Order(20)
  @ParameterizedTest
  @MethodSource("versionsToMigrate")
  void testVersionUpdate(DLCMMetadataVersion version) {
    this.runVersionUpdateTest(version, "Deposit for updating version " + version + " ");
  }

  @Order(30)
  @ParameterizedTest
  @Disabled("Must address the migration of archives in the future")
  @MethodSource("versionsToMigrate")
  void testVersionMigration(DLCMMetadataVersion version) {
    this.runVersionMigrationTest(version, "Deposit for migrating version");
  }

  private void runUpdateTest(String title, DataTag dataTagSource, DataUsePolicy dataUsePolicySource, DataTag dataTagTarget,
          DataUsePolicy dataUsePolicyTarget) {
    Deposit deposit = this.depositITService.createRemoteDepositWithDuaType(PersistenceMode.TEMPORARY, title, DEFAULT_DESCRIPTION, dataTagSource,
            dataUsePolicySource, Access.CLOSED, true);
    deposit = this.submitDeposit(deposit);
    final ArchivalInfoPackage aip1 = this.getAIP(deposit);
    // Update deposit
    deposit = this.updateDepositSensitivity(deposit, dataTagTarget, dataUsePolicyTarget);
    final ArchivalInfoPackage aip2 = this.getAIP(deposit);
    this.checkAip(aip1, aip2);
  }

  private void runVersionUpdateTest(DLCMMetadataVersion version, String title) {
    Deposit deposit = this.depositITService.createRemoteDepositWithContributor(PersistenceMode.TEMPORARY, title, DEFAULT_DESCRIPTION);
    final String originalTitle = deposit.getTitle();
    deposit.setMetadataVersion(version);
    deposit = this.depositITService.updateDeposit(deposit.getResId(), deposit);
    deposit = this.submitDeposit(deposit);
    assertEquals(DepositStatus.COMPLETED, deposit.getStatus());
    this.getAIP(deposit);
    // Update deposit
    deposit = this.depositITService.startEditingMetadata(deposit);
    deposit.setTitle(deposit.getTitle() + " => " + DLCMMetadataVersion.getDefaultVersion());
    deposit.setMetadataVersion(DLCMMetadataVersion.getDefaultVersion());
    deposit = this.depositITService.updateDeposit(deposit.getResId(), deposit);
    // Submit deposit
    deposit = this.depositITService.approveDeposit(deposit);
    // Deposit in error because version update forbidden
    assertTrue(this.depositITService.waitDepositIsProcessed(deposit.getResId(), DepositStatus.IN_ERROR),
            "Deposit [" + deposit.getResId() + "] must be in error");
    deposit = this.depositITService.findDeposit(deposit.getResId());
    // Edit deposit
    deposit = this.depositITService.enableRevision(deposit);
    assertEquals(DepositStatus.EDITING_METADATA, deposit.getStatus());
    // Cancel edition
    deposit = this.depositITService.cancelEditingMetadata(deposit);
    assertTrue(this.depositITService.waitDepositIsCompleted(deposit.getResId()), "Deposit [" + deposit.getResId() + "] must be completed");
    deposit = this.depositITService.findDeposit(deposit.getResId());
    assertEquals(version, deposit.getMetadataVersion(), "Deposit [" + deposit.getResId() + "] has a wrong metadata version");
    assertEquals(originalTitle, deposit.getTitle(), "Deposit [" + deposit.getResId() + "] has a wrong title");
  }

  private void runVersionMigrationTest(DLCMMetadataVersion version, String title) {
    Deposit deposit = this.depositITService.createRemoteDepositWithContributor(PersistenceMode.TEMPORARY, title, DEFAULT_DESCRIPTION);
    deposit.setMetadataVersion(version);
    deposit = this.depositITService.updateDeposit(deposit.getResId(), deposit);
    deposit = this.submitDeposit(deposit);
    final ArchivalInfoPackage aip1 = this.getAIP(deposit);
    // Update deposit
    deposit = this.updateDepositVersion(deposit, DLCMMetadataVersion.getDefaultVersion());
    final ArchivalInfoPackage aip2 = this.getAIP(deposit);
    this.checkAip(aip1, aip2);
  }

  private void checkAip(ArchivalInfoPackage aip1, ArchivalInfoPackage aip2) {
    assertEquals(aip1.getResId(), aip2.getResId());
    assertEquals(aip1.getMetadataVersion(), aip2.getMetadataVersion());
    assertNotEquals(aip1.getArchiveId(), aip2.getArchiveId());
    assertNotEquals(aip1.getArchiveUri(), aip2.getArchiveUri());
  }

  private Deposit updateDepositVersion(Deposit deposit, DLCMMetadataVersion version) {
    deposit = this.depositITService.startEditingMetadata(deposit);
    deposit.setMetadataVersion(version);
    deposit = this.depositITService.updateDeposit(deposit.getResId(), deposit);
    return this.approveDeposit(deposit);
  }

  private Deposit updateDepositSensitivity(Deposit deposit, DataTag dataTag, DataUsePolicy dataUsePolicy) {
    deposit = this.depositITService.startEditingMetadata(deposit);
    deposit.setDataSensitivity(dataTag);
    deposit.setDataUsePolicy(dataUsePolicy);
    deposit = this.depositITService.updateDeposit(deposit.getResId(), deposit);
    return this.approveDeposit(deposit);
  }

  private ArchivalInfoPackage getAIP(Deposit deposit) {
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
    final String aipId = this.metadataITService.getAipIdFromDepositId(deposit.getResId());
    return this.aipITService.findAIP(aipId);
  }

  private Deposit submitDeposit(Deposit deposit) {
    final String depositId = deposit.getResId();
    this.depositITService.uploadArchiveToDeposit(depositId, new ClassPathResource(YARETA_DATA));
    this.depositITService.waitDepositWithDataIsReady(depositId);
    return this.approveDeposit(deposit);
  }

  private Deposit approveDeposit(Deposit deposit) {
    this.depositITService.approveDeposit(deposit);
    assertTrue(this.depositITService.waitDepositIsCompleted(deposit.getResId()), "Deposit not completed: " + deposit.getResId());
    return this.depositITService.findDeposit(deposit.getResId());
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.depositITService.cleanTestData();
    this.restClientTool.exitSudo();
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - StoredAIPIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.archivalstorage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preservation.StoredAIP;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.archivalstorage.StoredAIPClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.MetadataITService;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class StoredAIPIT extends AbstractIT {

  private static final String YARETA_DATA = "deposit/dlcm/Yareta-small.zip";

  private final DepositITService depositITService;
  private final AipITService aipITService;
  private final MetadataITService metadataITService;

  private final AIPClientService aipClientService;
  private final StoredAIPClientService storedAIPClientService;

  @Autowired
  public StoredAIPIT(
          Environment env,
          SudoRestClientTool restClientTool,
          DepositITService depositITService,
          AipITService aipITService,
          MetadataITService metadataITService,
          AIPClientService aipClientService,
          StoredAIPClientService storedAIPClientService) {
    super(env, restClientTool);
    this.depositITService = depositITService;
    this.aipITService = aipITService;
    this.metadataITService = metadataITService;
    this.aipClientService = aipClientService;
    this.storedAIPClientService = storedAIPClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Order(10)
  @Test
  void testAIPList() {
    // List AIP
    List<ArchivalInfoPackage> aipList = this.aipClientService.findAll();
    // List stored AIP
    RestCollection<StoredAIP> storedAipList = this.storedAIPClientService.listStoredAIP();
    assertEquals(aipList.size(), storedAipList.getPage().getTotalItems(), "Number of AIP in the database not the same as the stored AIP");
  }

  @Order(20)
  @Test
  void testStoreAIPList() {
    // List stored AIP
    RestCollection<StoredAIP> storedAipList = this.storedAIPClientService.listStoredAIP();
    final long initialTotal = storedAipList.getPage().getTotalItems();
    // Create
    final Deposit deposit = this.createDeposit();
    this.getAIP(deposit);
    storedAipList = this.storedAIPClientService.listStoredAIP();
    assertEquals(initialTotal + 1, storedAipList.getPage().getTotalItems(),
            "Initial AIPs: " + initialTotal + " doesn't correspond to all AIPs after adding another AIP");
  }

  @Order(30)
  @Test
  void testStoreAIPCheck() {
    for (StoredAIP storedAip : this.storedAIPClientService.listStoredAIP().getData()) {
      this.checkStoredAip(storedAip);
      this.checkStoredAip(this.storedAIPClientService.findOne(storedAip.getResId()));
    }
  }

  private void checkStoredAip(StoredAIP storedAip) {
    assertNotNull(storedAip.getResId(), "AIP resId : " + storedAip.getResId() + " has a NULL resId");
    assertNotNull(storedAip.getOrganizationalUnitId(), "AIP resId : " + storedAip.getResId() + " has a NULL organizationalUnitId");
    assertNotNull(storedAip.getAipId(), "AIP resId : " + storedAip.getResId() + " has a NULL aipId");
    assertNotNull(storedAip.getArchiveId(), "AIP resId : " + storedAip.getResId() + " has a NULL archiveId");
    assertNotNull(storedAip.getArchiveUri(), "AIP resId : " + storedAip.getResId() + " has NULL archiveUri");
  }

  private ArchivalInfoPackage getAIP(Deposit deposit) {
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
    final String aipId = this.metadataITService.getAipIdFromDepositId(deposit.getResId());
    return this.aipITService.findAIP(aipId);
  }

  private Deposit createDeposit() {
    final Deposit deposit = this.depositITService.createRemoteDepositWithContributor(PersistenceMode.TEMPORARY, "Deposit for stored AIP",
            DEFAULT_DESCRIPTION);
    final String depositId = deposit.getResId();
    this.depositITService.uploadArchiveToDeposit(depositId, new ClassPathResource(YARETA_DATA));
    this.depositITService.waitDepositWithDataIsReady(depositId);
    return this.approveDeposit(deposit);
  }

  private Deposit approveDeposit(Deposit deposit) {
    this.depositITService.approveDeposit(deposit);
    assertTrue(this.depositITService.waitDepositIsCompleted(deposit.getResId()), "Deposit not completed: " + deposit.getResId());
    return this.depositITService.findDeposit(deposit.getResId());
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.depositITService.cleanTestData();
    this.restClientTool.exitSudo();
  }

}

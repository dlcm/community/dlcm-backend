/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - CleanIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.clean;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.PersonITService;

@Order(Integer.MAX_VALUE)
class CleanIT extends AbstractIT {

  private final DepositITService depositITService;
  private final InstitutionITService institutionITService;
  private final PersonITService personITService;

  @Autowired
  public CleanIT(Environment env, SudoRestClientTool restClientTool, DepositITService depositITService, InstitutionITService institutionITService,
          PersonITService personITService) {
    super(env, restClientTool);
    this.depositITService = depositITService;
    this.institutionITService = institutionITService;
    this.personITService = personITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void cleanTest() {
    // Need to have at least one test
    assertTrue(true);
  }

  @Override
  protected void deleteFixtures() {
    this.depositITService.cleanTestData();
    this.personITService.cleanTestData();
    this.institutionITService.cleanTestData();
  }
}

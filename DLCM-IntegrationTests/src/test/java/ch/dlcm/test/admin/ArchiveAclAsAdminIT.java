/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveAclAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.admin.ArchiveACLClientService;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.service.admin.UserClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.ArchiveAclITService;
import ch.dlcm.test.service.OrgUnitITService;

class ArchiveAclAsAdminIT extends AbstractIT {

  protected String archiveNameForDeletedTest = DLCMTestConstants.ARCHIVAL_UNIT_LABEL + " 2";
  protected String depositTitle = DLCMTestConstants.DEPOSIT_WITH_SENSITIVE_DATA + " " + DataTag.RED.name();

  private final AIPClientService aipClientService;
  private final MetadataClientService metadataClientService;
  private final UserClientService userClientService;
  private final ArchiveACLClientService archiveACLClientService;
  private final OrganizationalUnitClientService organizationalUnitClientService;
  private final OrgUnitITService orgUnitITService;
  private final ArchiveAclITService archiveAclITService;

  @Autowired
  public ArchiveAclAsAdminIT(Environment env, SudoRestClientTool restClientTool, ArchiveACLClientService archiveACLClientService,
          AIPClientService aipClientService, MetadataClientService metadataClientService, UserClientService userClientService,
          OrganizationalUnitClientService organizationalUnitClientService, OrgUnitITService orgUnitITService,
          ArchiveAclITService archiveAclITService) {
    super(env, restClientTool);
    this.archiveACLClientService = archiveACLClientService;
    this.aipClientService = aipClientService;
    this.metadataClientService = metadataClientService;
    this.userClientService = userClientService;
    this.organizationalUnitClientService = organizationalUnitClientService;
    this.orgUnitITService = orgUnitITService;
    this.archiveAclITService = archiveAclITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    final String aipId = this.getPermanentAip().getResId();
    IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);
    String orgUnitId = (String) (indexMetadata.getMetadata(DLCMConstants.AIP_ORGA_UNIT));
    OrganizationalUnit organizationalUnit = this.organizationalUnitClientService.findOne(orgUnitId);

    // obtain connected user
    User user = this.userClientService.getAuthenticatedUser();

    // Delete already existing archiveAcl, if there is one
    this.archiveAclITService.deleteArchiveAcl(aipId, user.getResId());

    ArchiveACL archiveACL = new ArchiveACL();
    archiveACL.setAipId(aipId);
    archiveACL.setUser(user);
    archiveACL.setOrganizationalUnit(organizationalUnit);
    ArchiveACL archiveACL1 = this.archiveACLClientService.create(archiveACL);

    // Test the creation
    assertEquals(archiveACL.getUser().getResId(), archiveACL1.getUser().getResId());
    assertEquals(archiveACL.getOrganizationalUnit().getResId(), archiveACL1.getOrganizationalUnit().getResId());
    assertEquals(archiveACL.getAipId(), archiveACL1.getAipId());
  }

  @Test
  void creationAclForArchiveWithDuaTypeSignedWithSignedDuaTest() {
    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService
            .searchByProperties(Map.of("info.name", this.depositTitle));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);
      String orgUnitId = (String) (indexMetadata.getMetadata(DLCMConstants.AIP_ORGA_UNIT));
      OrganizationalUnit orgUnit = this.organizationalUnitClientService.findOne(orgUnitId);

      // obtain connected user
      User user = this.userClientService.getAuthenticatedUser();

      // Delete already existing archiveAcl, if there is one
      this.archiveAclITService.deleteArchiveAcl(aipId, user.getResId());

      this.archiveAclITService.testArchiveAclCreation(user, orgUnit, aipId);
    }
  }

  @Test
  void cannotDeleteSignedDuaFromAclForArchiveWithDuaTypeTest() {
    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.depositTitle));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);
      String orgUnitId = (String) (indexMetadata.getMetadata(DLCMConstants.AIP_ORGA_UNIT));
      OrganizationalUnit orgUnit = this.organizationalUnitClientService.findOne(orgUnitId);

      // obtain connected user
      User user = this.userClientService.getAuthenticatedUser();

      // Delete already existing archiveAcl, if there is one
      this.archiveAclITService.deleteArchiveAcl(aipId, user.getResId());

      final ArchiveACL remoteArchiveAcl = this.archiveAclITService.testArchiveAclCreation(user, orgUnit, aipId);

      assertThrows(HttpClientErrorException.Forbidden.class, () -> this.archiveACLClientService.deleteDuaFile(remoteArchiveAcl.getResId()));
    }
  }

  @Test
  void cannotCreateAclForArchiveWithoutDuaTypeWithDuaFileTest() {
    final String aipId = this.getPermanentAip().getResId();
    IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);
    String orgUnitId = (String) (indexMetadata.getMetadata(DLCMConstants.AIP_ORGA_UNIT));
    OrganizationalUnit organizationalUnit = this.organizationalUnitClientService.findOne(orgUnitId);

    // obtain connected user
    User user = this.userClientService.getAuthenticatedUser();

    // Delete already existing archiveAcl, if there is one
    this.archiveAclITService.deleteArchiveAcl(aipId, user.getResId());

    ArchiveACL archiveACL = new ArchiveACL();
    archiveACL.setAipId(aipId);
    archiveACL.setUser(user);
    archiveACL.setOrganizationalUnit(organizationalUnit);
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.archiveACLClientService.createAndUploadDuaFile(archiveACL, new ClassPathResource("bootstrap.yml")));
  }

  @Test
  void creationAclForArchiveWithDuaTypeWithoutSignedDuaTest() {
    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.depositTitle));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);
      String orgUnitId = (String) (indexMetadata.getMetadata(DLCMConstants.AIP_ORGA_UNIT));
      OrganizationalUnit organizationalUnit = this.organizationalUnitClientService.findOne(orgUnitId);

      // obtain connected user
      User user = this.userClientService.getAuthenticatedUser();

      // Delete already existing archiveAcl, if there is one
      this.archiveAclITService.deleteArchiveAcl(aipId, user.getResId());

      ArchiveACL archiveACL = new ArchiveACL();
      archiveACL.setAipId(aipId);
      archiveACL.setUser(user);
      archiveACL.setOrganizationalUnit(organizationalUnit);

      assertThrows(HttpClientErrorException.BadRequest.class, () -> this.archiveACLClientService.create(archiveACL));
    }
  }

  @Test
  void updateTest() {
    final String aipId = this.getPermanentAip().getResId();
    IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);
    String orgUnitId = (String) (indexMetadata.getMetadata(DLCMConstants.AIP_ORGA_UNIT));
    OrganizationalUnit organizationalUnit = this.organizationalUnitClientService.findOne(orgUnitId);

    // Obtain connected user
    User user = this.userClientService.getAuthenticatedUser();

    // Delete already existing archiveAcl, if there is one
    this.archiveAclITService.deleteArchiveAcl(aipId, user.getResId());

    ArchiveACL archiveACL = new ArchiveACL();
    archiveACL.setAipId(aipId);
    archiveACL.setUser(user);
    archiveACL.setOrganizationalUnit(organizationalUnit);
    archiveACL = this.archiveACLClientService.create(archiveACL);

    // Update archiveACL
    archiveACL.setExpiration(OffsetDateTime.now(ZoneOffset.UTC).plusYears(1));
    this.archiveACLClientService.update(archiveACL.getResId(), archiveACL);
    final ArchiveACL archiveACL1 = this.archiveACLClientService.findOne(archiveACL.getResId());

    // Test the update
    assertEquals(archiveACL.getUser().getResId(), archiveACL1.getUser().getResId());
    this.assertEqualsWithoutNanoSeconds(archiveACL.getExpiration(), archiveACL1.getExpiration());
    assertEquals(archiveACL.getAipId(), archiveACL1.getAipId());
  }

  @Test
  void listTest() {
    OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN);
    assertDoesNotThrow(() -> this.archiveACLClientService.searchByProperties(Map.of("organizationalUnit.resId", organizationalUnit.getResId())));
  }

  @Test
  void doNotGetDeletedArchiveACLTest() {
    List<ArchiveACL> archiveACLList = this.archiveACLClientService.findAll();
    this.createAndDeleteArchiveACL();
    List<ArchiveACL> archiveACLAfterAddingOneDeleted = this.archiveACLClientService.findAll();
    assertEquals(archiveACLList.size(), archiveACLAfterAddingOneDeleted.size());
  }

  @Test
  void listAllArchiveACLTest() {
    List<ArchiveACL> archiveACLList = this.archiveACLClientService.listAllArchiveAcl();
    this.createAndDeleteArchiveACL();
    List<ArchiveACL> archiveACLAfterAddingDeletedOne = this.archiveACLClientService.listAllArchiveAcl();
    assertEquals(archiveACLList.size() + 1, archiveACLAfterAddingDeletedOne.size());
  }

  @Test
  void cannotUseFakeUserId() {
    final User fakeUser = new User();
    fakeUser.setResId("FAKE_USER_ID");
    final OrganizationalUnit orgUnit = new OrganizationalUnit();
    orgUnit.setResId("FAKE_ORG_UNIT_ID");
    final String aipId = this.getPermanentAip().getResId();

    // Delete already existing archiveAcl, if there is one
    this.archiveAclITService.deleteArchiveAcl(aipId, fakeUser.getResId());
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.archiveAclITService.testArchiveAclCreation(fakeUser, orgUnit, aipId));
  }

  @Test
  void cannotCreateArchiveAclDeleted() {
    final ArchiveACL archiveACL = new ArchiveACL();

    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.archiveNameForDeletedTest));
    if (!aipList.isEmpty()) {
      // check if archive ACL was already created
      final String aipId = aipList.get(0).getResId();

      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);
      String orgUnitId = (String) (indexMetadata.getMetadata(DLCMConstants.AIP_ORGA_UNIT));
      OrganizationalUnit organizationalUnit = this.organizationalUnitClientService.findOne(orgUnitId);

      // obtain connected user
      User user = this.userClientService.getAuthenticatedUser();
      archiveACL.setAipId(aipId);
      archiveACL.setUser(user);
      archiveACL.setOrganizationalUnit(organizationalUnit);
      archiveACL.setDeleted(true);
      assertThrows(HttpClientErrorException.BadRequest.class, () -> this.archiveACLClientService.create(archiveACL));
    }
  }

  @Test
  void cannotUpdateADeleteArchiveACL() {
    ArchiveACL archiveACL = this.createAndDeleteArchiveACL();
    archiveACL.setExpiration(OffsetDateTime.now());
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.archiveACLClientService.update(archiveACL.getResId(), archiveACL));
  }

  @Override
  protected void deleteFixtures() {
  }

  protected ArchiveACL createAndDeleteArchiveACL() {
    ArchiveACL archiveACL = new ArchiveACL();

    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.archiveNameForDeletedTest));
    if (!aipList.isEmpty()) {
      // check if archive ACL was already created
      final String aipId = aipList.get(0).getResId();

      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);
      String orgUnitId = (String) (indexMetadata.getMetadata(DLCMConstants.AIP_ORGA_UNIT));
      OrganizationalUnit organizationalUnit = this.organizationalUnitClientService.findOne(orgUnitId);

      // obtain connected user
      User user = this.userClientService.getAuthenticatedUser();

      // Delete already existing archiveAcl, if there is one
      this.archiveAclITService.deleteArchiveAcl(aipId, user.getResId());

      archiveACL.setAipId(aipId);
      archiveACL.setUser(user);
      archiveACL.setOrganizationalUnit(organizationalUnit);
      archiveACL.setDeleted(false);

      archiveACL = this.archiveACLClientService.create(archiveACL);

      this.archiveACLClientService.delete(archiveACL.getResId());
    }
    return archiveACL;
  }

  private ArchivalInfoPackage getPermanentAip() {
    final String archiveName = DLCMTestConstants.ARCHIVAL_UNIT_LABEL + " 1";
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", archiveName));
    if (aipList.isEmpty()) {
      throw new SolidifyResourceNotFoundException("Missing permanent AIP");
    }
    return aipList.get(0);
  }
}

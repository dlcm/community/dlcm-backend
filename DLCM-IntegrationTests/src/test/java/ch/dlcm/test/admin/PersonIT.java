/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - PersonIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SearchOperation;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.OrgUnitITService;

class PersonIT extends AbstractIT {

  private final PersonClientService personClientService;
  private final InstitutionITService institutionITService;
  private final OrgUnitITService orgUnitITService;

  @Autowired
  public PersonIT(Environment env, SudoRestClientTool restClientTool, PersonClientService personClientService,
          InstitutionITService institutionITService, OrgUnitITService orgUnitITService) {
    super(env, restClientTool);
    this.personClientService = personClientService;
    this.institutionITService = institutionITService;
    this.orgUnitITService = orgUnitITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Thierry", "Lhermitte", false);
    person1 = this.personClientService.create(person1);
    final Person person2 = this.personClientService.findOne(person1.getResId());

    // Test the creation
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person2.getCreationTime());
    assertNull(person2.getOrcid());
  }

  @Test
  void creationTestEmptyOrcid() {
    Person person1 = this.createTemporaryPerson("Gilbert", "Seldman", false);
    person1.setOrcid("");
    Person person2 = this.personClientService.create(person1);
    assertEquals(person1.getFirstName(), person2.getFirstName(), "First name not set correctly");
    assertEquals(person1.getLastName(), person2.getLastName(), "Last name not set correctly");
    assertEquals(person1.getOrcid(), person2.getOrcid(), "Orcid not set correctly");
  }

  @Test
  void creationTestNullOrcid() {
    Person person1 = this.createTemporaryPerson("Marius", "Seldman", false);
    person1.setOrcid(null);
    Person person2 = this.personClientService.create(person1);
    assertEquals(person1.getFirstName(), person2.getFirstName(), "First name not set correctly");
    assertEquals(person1.getLastName(), person2.getLastName(), "Last name not set correctly");
    assertEquals(person1.getOrcid(), person2.getOrcid(), "Orcid not set correctly");
  }

  @Test
  void creationTestUniqueOrcid() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Gilbert", "Seldman", true);
    final String orcid = person1.getOrcid();
    person1 = this.personClientService.create(person1);

    Person person2 = this.createTemporaryPerson("Marius", "Franceschini", false);
    person2.setOrcid(orcid);
    try {
      this.personClientService.create(person2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }
  }

  @Test
  void creationTestWithEmptyName() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Jean-Claude", "Dusse", false);
    person1.setFirstName("");
    try {
      this.personClientService.create(person1);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void creationTestWithOrcid() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Bernard", "Morin", true);
    person1 = this.personClientService.create(person1);
    final Person person2 = this.personClientService.findOne(person1.getResId());

    // Test the creation
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person2.getCreationTime());
    assertEquals(person1.getOrcid(), person2.getOrcid());
  }

  @Test
  void creationTestWithOrcidX() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Bernard", "Morin", true);
    person1.setOrcid(person1.getOrcid().substring(0, person1.getOrcid().length() - 1) + "X");
    person1 = this.personClientService.create(person1);
    final Person person2 = this.personClientService.findOne(person1.getResId());

    // Test the creation
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person2.getCreationTime());
    assertEquals(person1.getOrcid(), person2.getOrcid());
  }

  @Test
  void getTestWithOrcid() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Bernard", "Morin", true);
    person1 = this.personClientService.create(person1);
    final Person person2 = this.personClientService.findOne(person1.getResId());

    // Test the creation
    assertEquals(person1.getFirstName(), person2.getFirstName());
    assertEquals(person1.getLastName(), person2.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person2.getCreationTime());
    assertEquals(person1.getOrcid(), person2.getOrcid());

    // Get Person by ORCID
    Person person3 = this.personClientService.findByOrcid(person1.getOrcid()).get();
    // Test the get
    assertEquals(person1.getFirstName(), person3.getFirstName());
    assertEquals(person1.getLastName(), person3.getLastName());
    this.assertEqualsWithoutNanoSeconds(person1.getCreationTime(), person3.getCreationTime());
    assertEquals(person1.getOrcid(), person3.getOrcid());
  }

  @Test
  void creationTestWithWrongOrcid() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Gisèle", "André", false);
    person1.setOrcid("1234");
    try {
      this.personClientService.create(person1);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void deleteListTest() {
    // Create persons
    final int personNumber = 10;
    final String firstName = "Haskell";
    final String lastName = "Curry";
    for (int i = 0; i < personNumber; i++) {
      this.personClientService.create(this.createTemporaryPerson(firstName + i, lastName, false));
    }
    // Search Persons
    final List<Person> personList = this.personClientService.searchByProperties(Map.of("lastName", lastName));
    assertEquals(personList.size(), personNumber);

    // Delete Persons
    final String[] ids = new String[personNumber];
    int i = 0;
    for (final Person person : personList) {
      ids[i] = person.getResId();
      i++;
    }
    this.personClientService.deleteList(ids);

    // Check if person deleted
    for (final Person person : personList) {
      assertThrows(HttpClientErrorException.NotFound.class, () -> this.personClientService.findOne(person.getResId()), "Person not deleted");
    }
  }

  @Test
  void deleteTest() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Jérôme", "Tarayre", false);
    person1 = this.personClientService.create(person1);

    // Delete Person
    this.personClientService.delete(person1.getResId());

    // Check if person deleted
    final String person1Id = person1.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.personClientService.findOne(person1Id), "Person not deleted");
  }

  @Test
  void institutionTest() {
    // Create a person
    Person person = this.createTemporaryPerson("Miguel", "Weissmuller", false);
    person = this.personClientService.create(person);

    // Create an institution
    Institution institution1 = this.institutionITService.createRemoteInstitution("institution");

    // Add the person to the institution
    this.personClientService.addInstitution(person.getResId(), institution1.getResId());
    final List<Institution> listInstitutions = this.personClientService.getInstitutions(person.getResId());
    final Optional<Institution> optionalInstitution = listInstitutions.stream().filter(ins -> ins.getName().equals(institution1.getName()))
            .findFirst();
    assertTrue(optionalInstitution.isPresent());

    // Test the institution
    assertEquals(institution1.getResId(), optionalInstitution.get().getResId());
    assertEquals(institution1.getName(), optionalInstitution.get().getName());
    assertEquals(institution1.getDescription(), optionalInstitution.get().getDescription());
  }

  @Test
  void removeOrganizationalUnitFromPersonWithDefaultRoleTest() {

    // Create a person
    Person person = this.createTemporaryPerson("Nathalie", "Morin", false);
    person = this.personClientService.create(person);

    // Create an OrganizationalUnit
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);
    List<OrganizationalUnit> organizationalUnits = this.personClientService.getOrganizationalUnits(person.getResId());
    final int sizeBeforeAddingPerson = organizationalUnits.size();

    // Add the person to the OrganizationalUnit with default role
    this.personClientService.addOrganizationalUnit(person.getResId(), organizationalUnit.getResId());
    organizationalUnits = this.personClientService.getOrganizationalUnits(person.getResId());

    assertEquals(sizeBeforeAddingPerson + 1, organizationalUnits.size());

    final OrganizationalUnit expectedOrganizationalUnit = organizationalUnits.get(0);

    // Test added person on the OrganizationalUnit
    assertEquals(organizationalUnit.getResId(), expectedOrganizationalUnit.getResId());
    assertEquals(organizationalUnit.getDescription(), expectedOrganizationalUnit.getDescription());

    // Test remove OrganizationalUnit from the person
    this.personClientService.removeOrganizationalUnit(person.getResId(), expectedOrganizationalUnit.getResId());
    organizationalUnits = this.personClientService.getOrganizationalUnits(person.getResId());
    assertTrue(organizationalUnits.isEmpty());
  }

  @Test
  void removeOrganizationalUnitFromPersonTest() {

    // Create a person
    Person person = this.createTemporaryPerson("Nathalie", "Morin", false);
    person = this.personClientService.create(person);

    // Create an OrganizationalUnit
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);
    List<OrganizationalUnit> organizationalUnits = this.personClientService.getOrganizationalUnits(person.getResId());
    final int sizeBeforeAddingPerson = organizationalUnits.size();

    // Add the person to the OrganizationalUnit with others roles
    this.personClientService.addOrganizationalUnit(person.getResId(), organizationalUnit.getResId(), Role.CREATOR.getResId());
    organizationalUnits = this.personClientService.getOrganizationalUnits(person.getResId());
    assertEquals(sizeBeforeAddingPerson + 1, organizationalUnits.size());

    final OrganizationalUnit expectedOrganizationalUnit = organizationalUnits.get(0);

    // Test added person on the OrganizationalUnit
    assertEquals(organizationalUnit.getResId(), expectedOrganizationalUnit.getResId());
    assertEquals(organizationalUnit.getDescription(), expectedOrganizationalUnit.getDescription());

    // Test remove a role from the person in OrganizationalUnit
    this.personClientService.removeOrganizationalUnit(person.getResId(), expectedOrganizationalUnit.getResId(), Role.CREATOR.getResId());
    organizationalUnits = this.personClientService.getOrganizationalUnits(person.getResId());
    assertTrue(organizationalUnits.isEmpty());

  }

  @Test
  void searchTest() {
    // Create a person
    Person person = new Person();
    person.setFirstName(DLCMTestConstants.getRandomNameWithTemporaryLabel("SearchFirstName"));
    person.setLastName(DLCMTestConstants.getRandomNameWithTemporaryLabel("SearchLastName"));
    person = this.personClientService.create(person);

    /*
     * Test case sensitive research REM: as the database stores firstname and lastname in
     * utf8_general_ci, the searchTerm finds results even with wrong case
     */
    String searchTerm = "archfirs"; // substring of firstname
    assertTrue(this.found(String.format("firstName~%s, lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
    searchTerm = "archlas"; // substring of lastname
    assertTrue(this.found(String.format("firstName~%s, lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));

    /*
     * Test case insensitive research
     */
    searchTerm = "archfirs"; // substring of firstname
    assertTrue(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
    searchTerm = "archlas"; // substring of lastname
    assertTrue(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));

    /*
     * Search on both firstName AND lastName (by default on backend)
     */
    assertFalse(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), null, person));

    /*
     * search on not existing term
     */
    searchTerm = "--notexisting--";
    assertFalse(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
  }

  @Test
  void updateTest() {
    // Create a person
    Person person1 = this.createTemporaryPerson("Jérôme", "Tarayre", false);
    person1 = this.personClientService.create(person1);

    // Update the person
    final Person person2 = this.createTemporaryPerson("Robert", "Lespinass", true);
    final Person person3 = this.personClientService.update(person1.getResId(), person2);

    // Test the update
    assertEquals(person2.getFirstName(), person3.getFirstName());
    assertEquals(person2.getLastName(), person3.getLastName());
    assertEquals(person2.getOrcid(), person3.getOrcid());
  }

  @Override
  protected void deleteFixtures() {
    // Do nothing. Delegates to CleanIT
  }

  private Person createTemporaryPerson(String firstName, String lastName, boolean hasOrcid) {
    final Person person = new Person();
    person.setFirstName(DLCMTestConstants.getRandomNameWithTemporaryLabel(firstName));
    person.setLastName(DLCMTestConstants.getRandomNameWithTemporaryLabel(lastName));
    if (hasOrcid) {
      person.setOrcid(DLCMTestConstants.getRandomOrcId());
    }
    return person;
  }

  private boolean found(String search, String matchType, Person personToFound) {
    final List<Person> people = this.personClientService.search(search, matchType);
    for (final Person p : people) {
      if (p.getResId().equals(personToFound.getResId())) {
        return true;
      }
    }
    return false;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveUserRatingIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveUserRating;
import ch.dlcm.model.settings.RatingType;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.admin.ArchiveUserRatingClientService;
import ch.dlcm.service.admin.UserClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

class ArchiveUserRatingIT extends AbstractIT {

  protected String archiveName = DLCMTestConstants.ARCHIVAL_UNIT_LABEL + " 1";

  private final AIPClientService aipClientService;
  private final MetadataClientService metadataClientService;
  private final ArchiveUserRatingClientService archiveUserRatingClientService;
  private final UserClientService userService;

  @Autowired
  public ArchiveUserRatingIT(
          Environment env,
          SudoRestClientTool restClientTool,
          ArchiveUserRatingClientService archiveUserRatingClientService,
          AIPClientService aipClientService,
          MetadataClientService metadataClientService,
          UserClientService userService) {
    super(env, restClientTool);
    this.archiveUserRatingClientService = archiveUserRatingClientService;
    this.aipClientService = aipClientService;
    this.metadataClientService = metadataClientService;
    this.userService = userService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void createArchiveUserRatingTest() {
    ArchiveUserRating archiveUserRating = new ArchiveUserRating();
    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.archiveName));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);

      User user = this.userService.getAuthenticatedUser();
      archiveUserRating.setUser(user);
      archiveUserRating.setArchiveId(indexMetadata.getResId());
      archiveUserRating.setRatingType(RatingType.QUALITY);
      archiveUserRating.setGrade(5);

      ArchiveUserRating archiveUserRating2 = this.archiveUserRatingClientService.create(archiveUserRating);

      // Test the creation
      assertEquals(archiveUserRating.getUser().getResId(), archiveUserRating2.getUser().getResId());
      assertEquals(archiveUserRating.getRatingType().getResId(), archiveUserRating2.getRatingType().getResId());
      assertEquals(archiveUserRating.getArchiveId(), archiveUserRating2.getArchiveId());
      assertEquals(archiveUserRating.getGrade(), archiveUserRating2.getGrade());
    }
  }

  @Test
  void failCreateDuplicateTest() {
    ArchiveUserRating archiveUserRating = new ArchiveUserRating();
    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.archiveName));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);

      User user = this.userService.getAuthenticatedUser();
      archiveUserRating.setUser(user);
      archiveUserRating.setArchiveId(indexMetadata.getResId());
      archiveUserRating.setRatingType(RatingType.QUALITY);
      archiveUserRating.setGrade(5);

      ArchiveUserRating archiveUserRating2 = this.archiveUserRatingClientService.create(archiveUserRating);

      // Test the creation
      assertEquals(archiveUserRating.getUser().getResId(), archiveUserRating2.getUser().getResId());
      assertEquals(archiveUserRating.getRatingType().getResId(), archiveUserRating2.getRatingType().getResId());
      assertEquals(archiveUserRating.getArchiveId(), archiveUserRating2.getArchiveId());
      assertEquals(archiveUserRating.getGrade(), archiveUserRating2.getGrade());

      ArchiveUserRating archiveUserRating3 = new ArchiveUserRating();
      archiveUserRating3.setUser(user);
      archiveUserRating3.setArchiveId(indexMetadata.getResId());
      archiveUserRating3.setRatingType(RatingType.QUALITY);
      archiveUserRating3.setGrade(5);

      try {
        this.archiveUserRatingClientService.create(archiveUserRating3);
        fail("A BAD_REQUEST Http response should be received");
      } catch (HttpClientErrorException e) {
        assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      }
    }
  }

  @Test
  void failCreateWithNoUserTest() {
    ArchiveUserRating archiveUserRating = new ArchiveUserRating();
    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.archiveName));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);

      archiveUserRating.setUser(null);
      archiveUserRating.setArchiveId(indexMetadata.getResId());
      archiveUserRating.setRatingType(RatingType.QUALITY);
      archiveUserRating.setGrade(3);

      try {
        this.archiveUserRatingClientService.create(archiveUserRating);
        fail("A BAD_REQUEST Http response should be received");
      } catch (HttpClientErrorException e) {
        assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      }
    }
  }

  @Test
  void failCreateWithNoRatingTest() {
    ArchiveUserRating archiveUserRating = new ArchiveUserRating();
    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.archiveName));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);

      User user = this.userService.getAuthenticatedUser();
      archiveUserRating.setUser(user);
      archiveUserRating.setArchiveId(indexMetadata.getResId());
      archiveUserRating.setRatingType(null);
      archiveUserRating.setGrade(4);

      try {
        this.archiveUserRatingClientService.create(archiveUserRating);
        fail("A BAD_REQUEST Http response should be received");
      } catch (HttpClientErrorException e) {
        assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      }
    }
  }

  @Test
  void failGradeNotValidTest() {
    ArchiveUserRating archiveUserRating = new ArchiveUserRating();
    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.archiveName));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);

      User user = this.userService.getAuthenticatedUser();
      archiveUserRating.setUser(user);
      archiveUserRating.setArchiveId(indexMetadata.getResId());
      archiveUserRating.setRatingType(null);
      archiveUserRating.setGrade(7);

      try {
        this.archiveUserRatingClientService.create(archiveUserRating);
        fail("A BAD_REQUEST Http response should be received");
      } catch (HttpClientErrorException e) {
        assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      }
    }
  }

  @Test
  void failUpdateArchiveUserRatingTest() {
    ArchiveUserRating archiveUserRating = new ArchiveUserRating();
    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.archiveName));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);

      User user = this.userService.getAuthenticatedUser();
      archiveUserRating.setUser(user);
      archiveUserRating.setArchiveId(indexMetadata.getResId());
      archiveUserRating.setRatingType(RatingType.USEFULNESS);
      archiveUserRating.setGrade(5);

      ArchiveUserRating archiveUserRating2 = this.archiveUserRatingClientService.create(archiveUserRating);

      // Test the creation
      assertEquals(archiveUserRating.getUser().getResId(), archiveUserRating2.getUser().getResId());
      assertEquals(archiveUserRating.getRatingType().getResId(), archiveUserRating2.getRatingType().getResId());
      assertEquals(archiveUserRating.getArchiveId(), archiveUserRating2.getArchiveId());
      assertEquals(archiveUserRating.getGrade(), archiveUserRating2.getGrade());

      // TEST Update
      archiveUserRating2.setRatingType(RatingType.QUALITY);
      try {
        this.archiveUserRatingClientService.update(archiveUserRating2.getResId(), archiveUserRating2);
        fail("A BAD_REQUEST Http response should be received");
      } catch (HttpClientErrorException e) {
        assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      }
    }
  }

  @Test
  void updateTest() {
    ArchiveUserRating archiveUserRating = new ArchiveUserRating();
    // Obtain aip of archive already created by init data test
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.archiveName));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);

      User user = this.userService.getAuthenticatedUser();
      archiveUserRating.setUser(user);
      archiveUserRating.setArchiveId(indexMetadata.getResId());
      archiveUserRating.setRatingType(RatingType.USEFULNESS);
      archiveUserRating.setGrade(5);

      ArchiveUserRating archiveUserRating2 = this.archiveUserRatingClientService.create(archiveUserRating);

      // TEST Update
      archiveUserRating2.setGrade(3);
      ArchiveUserRating archiveUserRating3 = this.archiveUserRatingClientService.update(archiveUserRating2.getResId(), archiveUserRating2);
      // Test the creation
      assertEquals(archiveUserRating2.getUser().getResId(), archiveUserRating3.getUser().getResId());
      assertEquals(archiveUserRating2.getRatingType().getResId(), archiveUserRating3.getRatingType().getResId());
      assertEquals(archiveUserRating2.getArchiveId(), archiveUserRating3.getArchiveId());
      assertEquals(archiveUserRating2.getGrade(), archiveUserRating3.getGrade());
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearArchiveUserRatingFixtures();
  }

  private void clearArchiveUserRatingFixtures() {
    // clear all ArchiveUserRating by this user to the archive used in this specific tests
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", this.archiveName));
    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      IndexMetadata indexMetadata = this.metadataClientService.findOne(aipId);

      // obtain connected user
      User user = this.userService.getAuthenticatedUser();

      List<ArchiveUserRating> archiveUserRatingList = this.archiveUserRatingClientService
              .searchByProperties(Map.of("archiveId", indexMetadata.getResId(),
                      "user.resId", user.getResId()));
      for (ArchiveUserRating archiveUserRating : archiveUserRatingList) {
        this.archiveUserRatingClientService.delete(archiveUserRating.getResId());
      }
    }

  }
}

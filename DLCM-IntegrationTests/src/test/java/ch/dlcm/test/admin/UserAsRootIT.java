/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - UserAsRootIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.User;
import ch.dlcm.service.admin.UserClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

class UserAsRootIT extends AbstractIT {

  private final UserClientService userClientService;

  @Autowired
  public UserAsRootIT(Environment env, SudoRestClientTool restClientTool, UserClientService userClientService) {
    super(env, restClientTool);
    this.userClientService = userClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void testCreate() {

    final String externalUid = DLCMTestConstants.getRandomNameWithTemporaryLabel("12345@example.com");
    final String firstname = DLCMTestConstants.getRandomNameWithTemporaryLabel("John");
    final String lastname = DLCMTestConstants.getRandomNameWithTemporaryLabel("Smith");
    final String email = DLCMTestConstants.getRandomNameWithTemporaryLabel("john@example.com");

    /*
     * Create a new User
     */
    final User newUser = new User();

    newUser.setExternalUid(externalUid);
    newUser.setFirstName(firstname);
    newUser.setLastName(lastname);
    newUser.setEmail(email);
    newUser.setHomeOrganization("ungie.ch");

    this.userClientService.create(newUser);

    /*
     * Fetch it from server and test it has been created
     */
    final User fetchedUser = this.userClientService.findOne(newUser.getResId());
    assertNotNull(fetchedUser.getResId());
    assertEquals(firstname, fetchedUser.getFirstName());
  }

  @Test
  void testDelete() {

    final String externalUid = DLCMTestConstants.getRandomNameWithTemporaryLabel("12345@example.com");
    final String firstname = DLCMTestConstants.getRandomNameWithTemporaryLabel("John");
    final String lastname = DLCMTestConstants.getRandomNameWithTemporaryLabel("Smith");
    final String email = DLCMTestConstants.getRandomNameWithTemporaryLabel("john@example.com");

    /*
     * Create a new User
     */
    final User newUser = new User();

    newUser.setExternalUid(externalUid);
    newUser.setFirstName(firstname);
    newUser.setLastName(lastname);
    newUser.setEmail(email);
    newUser.setHomeOrganization("unige.ch");

    this.userClientService.create(newUser);

    /*
     * Checks saving succeeded
     */
    final User fetchedUser = this.userClientService.findOne(newUser.getResId());
    assertEquals(fetchedUser.getFirstName(), firstname);

    /*
     * Delete it
     */
    this.userClientService.delete(fetchedUser.getResId());

    /*
     * Fetching again returns nothing
     */
    final String newUserId = newUser.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.userClientService.findOne(newUserId));
  }

  @Test
  void testUnicity() {

    final String externalUid = DLCMTestConstants.getRandomNameWithTemporaryLabel("12345@example.com");
    final String firstname = DLCMTestConstants.getRandomNameWithTemporaryLabel("John");
    final String lastname = DLCMTestConstants.getRandomNameWithTemporaryLabel("Smith");
    final String email = DLCMTestConstants.getRandomNameWithTemporaryLabel("john@example.com");

    // Creating a new User
    final User newUser = new User();

    newUser.setExternalUid(externalUid);
    newUser.setFirstName(firstname);
    newUser.setLastName(lastname);
    newUser.setEmail(email);
    newUser.setHomeOrganization("unige.ch");

    this.userClientService.create(newUser);

    final String firstname2 = DLCMTestConstants.getRandomNameWithTemporaryLabel("Billy");
    final String lastname2 = DLCMTestConstants.getRandomNameWithTemporaryLabel("Bob");
    final String email2 = DLCMTestConstants.getRandomNameWithTemporaryLabel("billybob@example.com");

    // Creating second user with the same externalUID as the first's
    final User newUser2 = new User();
    newUser2.setExternalUid(externalUid);
    newUser2.setFirstName(firstname2);
    newUser2.setLastName(lastname2);
    newUser2.setEmail(email2);
    newUser2.setHomeOrganization("unige.ch");

    // Testing the unicity of the UIDs
    try {
      this.userClientService.create(newUser2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }
  }

  @Test
  void testUpdate() {

    final String externalUid = DLCMTestConstants.getRandomNameWithTemporaryLabel("12345@example.com");
    final String firstname = DLCMTestConstants.getRandomNameWithTemporaryLabel("John");
    final String lastname = DLCMTestConstants.getRandomNameWithTemporaryLabel("Smith");
    final String email = DLCMTestConstants.getRandomNameWithTemporaryLabel("john@example.com");

    /*
     * Create a new User
     */
    final User newUser = new User();

    newUser.setExternalUid(externalUid);
    newUser.setFirstName(firstname);
    newUser.setLastName(lastname);
    newUser.setEmail(email);
    newUser.setHomeOrganization("unige.ch");

    this.userClientService.create(newUser);

    /*
     * Checks saving succeeded
     */
    final User fetchedUser = this.userClientService.findOne(newUser.getResId());
    assertEquals(fetchedUser.getFirstName(), firstname);

    /*
     * Does the update
     */
    final String firstname2 = DLCMTestConstants.getRandomNameWithTemporaryLabel("Albert");
    final String lastname2 = DLCMTestConstants.getRandomNameWithTemporaryLabel("Levert");

    fetchedUser.setFirstName(firstname2);
    fetchedUser.setLastName(lastname2);
    this.userClientService.update(fetchedUser.getResId(), fetchedUser);

    /*
     * Checks update succeeded
     */
    final User refetchedUser = this.userClientService.findOne(fetchedUser.getResId());
    assertEquals(refetchedUser.getFirstName(), firstname2);
    assertEquals(refetchedUser.getLastName(), lastname2);
  }

  @Test
  void shouldSeeSomeAttribute() {
    User user = this.userClientService.getAuthenticatedUser();
    assertNotNull(user.getExternalUid());
    assertNotNull(user.getApplicationRole());
  }

  /*************************************************/

  protected void clearUserFixtures() {
    final List<User> users = this.userClientService.findAll();

    for (final User u : users) {
      if (u.getFirstName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.userClientService.delete(u.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearUserFixtures();
  }
}

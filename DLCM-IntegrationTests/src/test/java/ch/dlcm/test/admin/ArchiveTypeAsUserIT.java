/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveTypeAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.ArchiveType;
import ch.dlcm.service.admin.ArchiveTypeClientService;
import ch.dlcm.test.AbstractIT;

class ArchiveTypeAsUserIT extends AbstractIT {

  private final ArchiveTypeClientService archiveTypeService;

  @Autowired
  public ArchiveTypeAsUserIT(Environment env, SudoRestClientTool restClientTool, ArchiveTypeClientService archiveTypeClientService) {
    super(env, restClientTool);
    this.archiveTypeService = archiveTypeClientService;
  }

  @Test
  void listMasterTypes() {
    List<ArchiveType> list = this.archiveTypeService.findAllMasterType();
    assertEquals(2, list.size());
  }

  @Test
  void listArchiveTypes() {
    List<ArchiveType> list = this.archiveTypeService.findAll();
    assertTrue(list.size() >= 4);
  }

  @Test
  void listDatasetTypes() {
    List<ArchiveType> list = this.archiveTypeService.findAllForArchiveType("Dataset");
    assertTrue(list.size() >= 1);
    assertEquals(ArchiveType.DATASET_ID, list.get(0).getMasterType().getResId());
  }

  @Test
  void listCollectionTypes() {
    List<ArchiveType> list = this.archiveTypeService.findAllForArchiveType("Collection");
    assertEquals(1, list.size());
    assertEquals(ArchiveType.COLLECTION_ID, list.get(0).getMasterType().getResId());
  }

  @Override
  protected void deleteFixtures() {
    // No update
  }

}

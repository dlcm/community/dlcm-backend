/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - SubmissionPolicyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static ch.dlcm.test.DLCMTestConstants.SAMPLE_SUBMISSION_AGREEMENT_FILE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.policies.SubmissionAgreementType;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.service.admin.SubmissionPolicyClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.SubmissionAgreementITService;

class SubmissionPolicyIT extends AbstractIT {

  private final SubmissionPolicyClientService submissionPolicyClientService;
  private final SubmissionAgreementITService submissionAgreementITService;

  @Autowired
  public SubmissionPolicyIT(Environment env, SudoRestClientTool restClientTool,
          SubmissionPolicyClientService submissionPolicyClientService,
          SubmissionAgreementITService submissionAgreementITService) {
    super(env, restClientTool);
    this.submissionPolicyClientService = submissionPolicyClientService;
    this.submissionAgreementITService = submissionAgreementITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create a policy
    final SubmissionPolicy policy1 = new SubmissionPolicy();
    policy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("submission policy "));
    policy1.setSubmissionApproval(true);
    final SubmissionPolicy policy2 = this.submissionPolicyClientService.create(policy1);

    // Test the creation
    assertEquals(policy1.getName(), policy2.getName());
    assertEquals(policy1.getSubmissionApproval(), policy2.getSubmissionApproval());
    // Check default submission agreement type
    assertEquals(SubmissionAgreementType.WITHOUT, policy2.getSubmissionAgreementType());
  }

  @Test
  void createInvalidSubmissionPolicyWithSubmissionAgreement() {
    // Create a policy with a submission agreement but with a WITHOUT submission agreement type
    final SubmissionPolicy submissionPolicy = new SubmissionPolicy();
    submissionPolicy.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("invalid submission policy "));
    final Resource submissionAgreementFile = new ClassPathResource(SAMPLE_SUBMISSION_AGREEMENT_FILE);
    final SubmissionAgreement submissionAgreement =  this.submissionAgreementITService.createSubmissionAgreementWithFile(submissionAgreementFile);
    submissionPolicy.setSubmissionAgreement(submissionAgreement);
    submissionPolicy.setSubmissionAgreementType(SubmissionAgreementType.WITHOUT);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.submissionPolicyClientService.create(submissionPolicy));
  }

  @Test
  void createInvalidSubmissionPolicyWithoutSubmissionAgreement() {
    // Create a policy without a submission agreement but with a FOR_EACH_DEPOSIT submission agreement type
    final SubmissionPolicy submissionPolicy = new SubmissionPolicy();
    submissionPolicy.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("invalid submission policy "));
    submissionPolicy.setSubmissionAgreementType(SubmissionAgreementType.FOR_EACH_DEPOSIT);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.submissionPolicyClientService.create(submissionPolicy));
  }

  @Test
  void updateTest() {
    // Create a policy
    SubmissionPolicy policy1 = new SubmissionPolicy();
    policy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("A"));
    policy1.setSubmissionApproval(true);
    policy1 = this.submissionPolicyClientService.create(policy1);

    // Update the submission policy
    policy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("B"));
    policy1.setSubmissionApproval(false);
    policy1.setSubmissionAgreementType(SubmissionAgreementType.WITHOUT);
    this.submissionPolicyClientService.update(policy1.getResId(), policy1);
    final SubmissionPolicy policy2 = this.submissionPolicyClientService.findOne(policy1.getResId());

    // Test the update
    assertEquals(policy1.getName(), policy2.getName());
    assertEquals(policy1.getSubmissionApproval(), policy2.getSubmissionApproval());
    assertEquals(SubmissionAgreementType.WITHOUT, policy2.getSubmissionAgreementType());
    this.assertEqualsWithoutNanoSeconds(policy1.getCreationTime(), policy2.getCreationTime());
  }

  @Test
  void addSubmissionAgreement() {
    // Create submission policy
    final SubmissionPolicy submissionPolicy = new SubmissionPolicy();
    submissionPolicy.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Submission policy with submission agreement"));
    final Resource submissionAgreementFile = new ClassPathResource(SAMPLE_SUBMISSION_AGREEMENT_FILE);
    final SubmissionAgreement submissionAgreement =  this.submissionAgreementITService.createSubmissionAgreementWithFile(submissionAgreementFile);
    submissionPolicy.setSubmissionAgreement(submissionAgreement);
    submissionPolicy.setSubmissionAgreementType(SubmissionAgreementType.ON_FIRST_DEPOSIT);
    this.submissionPolicyClientService.create(submissionPolicy);

    // Test presence of submission agreement
    final SubmissionPolicy refetchedSubmissionPolicy = this.submissionPolicyClientService.findOne(submissionPolicy.getResId());
    assertEquals(submissionPolicy.getName(), refetchedSubmissionPolicy.getName());
    assertEquals(submissionPolicy.getSubmissionAgreementType(), refetchedSubmissionPolicy.getSubmissionAgreementType());
    assertEquals(submissionPolicy.getSubmissionAgreement().getResId(), refetchedSubmissionPolicy.getSubmissionAgreement().getResId());
  }

  @Test
  void deleteSubmissionAgreement() {
    // Create submission policy
    final SubmissionPolicy submissionPolicy = new SubmissionPolicy();
    submissionPolicy.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Submission policy with submission agreement"));
    final Resource submissionAgreementFile = new ClassPathResource(SAMPLE_SUBMISSION_AGREEMENT_FILE);
    final SubmissionAgreement submissionAgreement =  this.submissionAgreementITService.createSubmissionAgreementWithFile(submissionAgreementFile);
    submissionPolicy.setSubmissionAgreement(submissionAgreement);
    submissionPolicy.setSubmissionAgreementType(SubmissionAgreementType.ON_FIRST_DEPOSIT);
    this.submissionPolicyClientService.create(submissionPolicy);

    // Delete submission agreement
    SubmissionPolicy refetchedSubmissionPolicy = this.submissionPolicyClientService.findOne(submissionPolicy.getResId());
    refetchedSubmissionPolicy.setSubmissionAgreementType(SubmissionAgreementType.WITHOUT);
    refetchedSubmissionPolicy.setSubmissionAgreement(null);
    refetchedSubmissionPolicy = this.submissionPolicyClientService.update(refetchedSubmissionPolicy.getResId(), refetchedSubmissionPolicy);

    // Test deletion
    refetchedSubmissionPolicy = this.submissionPolicyClientService.findOne(refetchedSubmissionPolicy.getResId());
    assertEquals(submissionPolicy.getName(), refetchedSubmissionPolicy.getName());
    assertEquals(SubmissionAgreementType.WITHOUT, refetchedSubmissionPolicy.getSubmissionAgreementType());
    assertNull(refetchedSubmissionPolicy.getSubmissionAgreement());
  }

  protected void clearSubmissionPolicyFixtures() {
    final List<SubmissionPolicy> spList = this.submissionPolicyClientService.findAll();

    for (final SubmissionPolicy sp : spList) {
      if (sp.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.submissionPolicyClientService.delete(sp.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearSubmissionPolicyFixtures();
  }
}

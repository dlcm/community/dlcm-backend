/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - UserAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static ch.dlcm.test.DLCMTestConstants.THIRD_PARTY_USER_ID;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.User;
import ch.dlcm.service.admin.UserClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.service.PersonITService;

class UserAsUserIT extends AbstractIT {

  private final UserClientService userClientService;
  private final PersonITService personITService;

  @Autowired
  public UserAsUserIT(Environment env, SudoRestClientTool restClientTool, UserClientService userClientService, PersonITService personITService) {
    super(env, restClientTool);
    this.userClientService = userClientService;
    this.personITService = personITService;
  }

  @Test
  void userCannotCreateUser() {
    try {
      User user = new User();
      this.userClientService.create(user);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void userCannotUpdateUser() {
    try {
      User user = this.personITService.getUserWithRole(AuthApplicationRole.USER);
      user.setLastName("Grogou");
      this.userClientService.update(user.getResId(), user);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void userCannotDeleteUser() {
    try {
      User user = this.personITService.getUserWithRole(AuthApplicationRole.USER);
      this.userClientService.delete(user.getResId());
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void shouldNotSeeSomeAttributeOfOtherUser() {
    User user = this.userClientService.findAll().stream().filter(u -> u.getFirstName().contains(THIRD_PARTY_USER_ID + "_firstName"))
            .findFirst().orElseThrow();
    assertNull(user.getExternalUid());
    assertNull(user.getLastLoginIpAddress());
    assertNull(user.getLastLoginTime());
    assertNull(user.getApplicationRole());
  }

  @Test
  void shouldSeeSomeAttributeOfMyself() {
    this.restClientTool.sudoThirdPartyUser();
    User user1 = this.userClientService.getAuthenticatedUser();
    assertNotNull(user1.getExternalUid());
    assertNotNull(user1.getLastLoginIpAddress());
    assertNotNull(user1.getLastLoginTime());
    assertNotNull(user1.getApplicationRole());

    User user2 = this.userClientService.findAll().stream().filter(u -> u.getFirstName().contains(THIRD_PARTY_USER_ID + "_firstName"))
            .findFirst().orElseThrow();
    assertNotNull(user2.getExternalUid());
    assertNotNull(user2.getLastLoginIpAddress());
    assertNotNull(user2.getLastLoginTime());
    assertNotNull(user2.getApplicationRole());
    this.restClientTool.exitSudo();
  }

  @Override
  protected void deleteFixtures() {
    // Do nothing
  }
}

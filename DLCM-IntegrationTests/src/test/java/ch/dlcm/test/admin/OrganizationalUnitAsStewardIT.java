/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrganizationalUnitAsStewardIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.display.OrganizationalUnitDisseminationPolicyDTO;
import ch.dlcm.model.display.OrganizationalUnitPreservationPolicyDTO;
import ch.dlcm.model.display.OrganizationalUnitSubmissionPolicyDTO;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.FundingAgencyITService;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.OrgUnitITService;

class OrganizationalUnitAsStewardIT extends AbstractOrganizationalUnitWithRoleIT {

  @Autowired
  public OrganizationalUnitAsStewardIT(Environment env, SudoRestClientTool restClientTool,
          OrganizationalUnitClientService orgUnitClientService, OrgUnitITService orgUnitITService, InstitutionITService institutionITService,
          FundingAgencyITService fundingAgencyITService) {
    super(env, restClientTool, orgUnitClientService, orgUnitITService, institutionITService, fundingAgencyITService);
  }

  @Override
  protected String role() {
    return Role.STEWARD_ID;
  }

  @Test
  void updateOrgUnitNotAllowed() {
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Update org unit as " + this.role());
    this.restClientTool.exitSudo();

    organizationalUnit.setDescription("Try to update description");
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.organizationalUnitClientService.update(organizationalUnit.getResId(), organizationalUnit));
  }

  @Test
  void addAndRemoveDisseminationPolicyNotAllowed() {
    // Get test orgUnit
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Swiss Research Data Preservation #DISSEMINATION POLICY");
    this.restClientTool.exitSudo();

    List<OrganizationalUnitDisseminationPolicyDTO> policies = this.organizationalUnitClientService
            .getDisseminationPolicies(organizationalUnit.getResId());
    final int initialState = policies.size();

    // Create policy
    this.restClientTool.sudoRoot();
    final DisseminationPolicy policy = this.orgUnitITService.createRemoteDisseminationPolicy();
    this.restClientTool.exitSudo();

    // Add policy to orgUnit
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.organizationalUnitClientService.addDisseminationPolicy(organizationalUnit.getResId(), policy.getResId()));

    this.restClientTool.sudoAdmin();
    this.organizationalUnitClientService.addDisseminationPolicy(organizationalUnit.getResId(), policy.getResId());
    this.restClientTool.exitSudo();

    policies = this.organizationalUnitClientService.getDisseminationPolicies(organizationalUnit.getResId());
    assertEquals(policies.size(), initialState + 1);

    // Test add
    for (final OrganizationalUnitDisseminationPolicyDTO policy2 : policies) {
      if (policy2.getResId().equals(policy.getResId())) {
        assertNotNull(policy2);
        assertEquals(policy.getName(), policy2.getName());
        assertEquals(policy.getType(), policy2.getType());
      }
    }

    // Remove policy
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.organizationalUnitClientService.removeDisseminationPolicies(organizationalUnit.getResId(), policy.getResId()));
  }

  @Test
  void addAndRemoveSubmissionPolicyNotAllowed() {
    // Get test orgUnit
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Swiss Research Data Preservation #SUBMISSION POLICY");
    this.restClientTool.exitSudo();

    List<OrganizationalUnitSubmissionPolicyDTO> policies = this.organizationalUnitClientService
            .getSubmissionPolicies(organizationalUnit.getResId());
    final int initialState = policies.size();

    // Create policy
    this.restClientTool.sudoAdmin();
    final SubmissionPolicy policy = this.orgUnitITService.createRemoteSubmissionPolicy();
    this.restClientTool.exitSudo();

    // Add policy to orgUnit
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.organizationalUnitClientService.addSubmissionPolicy(organizationalUnit.getResId(), policy.getResId()));

    this.restClientTool.sudoAdmin();
    this.organizationalUnitClientService.addSubmissionPolicy(organizationalUnit.getResId(), policy.getResId());
    this.restClientTool.exitSudo();

    policies = this.organizationalUnitClientService.getSubmissionPolicies(organizationalUnit.getResId());
    assertEquals(policies.size(), initialState + 1);

    // Test added person on the OrganizationalUnit
    for (final OrganizationalUnitSubmissionPolicyDTO policy2 : policies) {
      if (policy2.getResId().equals(policy.getResId())) {
        assertNotNull(policy2);
        assertEquals(policy.getName(), policy2.getName());
        assertEquals(policy.getSubmissionApproval(), policy2.getSubmissionApproval());
      }
    }

    // Remove policy
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.organizationalUnitClientService.removeSubmissionPolicy(organizationalUnit.getResId(), policy.getResId()));
  }

  @Test
  void addAndRemovePreservationPolicyNotAllowed() {
    // Get test orgUnit
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Swiss Research Data Preservation #PRESERVATION POLICY");
    this.restClientTool.exitSudo();

    List<OrganizationalUnitPreservationPolicyDTO> policies = this.organizationalUnitClientService
            .getPreservationPolicies(organizationalUnit.getResId());
    final int initialState = policies.size();

    // Create policy
    this.restClientTool.sudoAdmin();
    final PreservationPolicy policy = this.orgUnitITService.createRemotePreservationPolicy();
    this.restClientTool.exitSudo();

    // Add policy to orgUnit
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.organizationalUnitClientService.addPreservationPolicy(organizationalUnit.getResId(), policy.getResId()));

    this.restClientTool.sudoAdmin();
    this.organizationalUnitClientService.addPreservationPolicy(organizationalUnit.getResId(), policy.getResId());
    this.restClientTool.exitSudo();

    policies = this.organizationalUnitClientService.getPreservationPolicies(organizationalUnit.getResId());
    assertEquals(policies.size(), initialState + 1);

    // Test add
    for (final OrganizationalUnitPreservationPolicyDTO policy2 : policies) {
      if (policy2.getResId().equals(policy.getResId())) {
        assertNotNull(policy2);
        assertEquals(policy.getName(), policy2.getName());
        assertEquals(policy.getRetention(), policy2.getRetention());
        assertEquals(policy.getDispositionApproval(), policy2.getDispositionApproval());
      }
    }

    // Remove policy
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.organizationalUnitClientService.removePreservationPolicy(organizationalUnit.getResId(), policy.getResId()));
  }

  @Test
  void uploadLogoTest() {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final OrganizationalUnit organizationalUnit1 = this.getOrCreateOrganizationalUnitForRole(PersistenceMode.TEMPORARY);
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.organizationalUnitClientService.uploadLogo(organizationalUnit1.getResId(), logoToUpload));
  }

  @Test
  void downloadLogoTest() throws IOException {
    this.downloadLogoShouldSuccessTest();
  }

  @Test
  void deleteLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit1 = this.getOrCreateOrganizationalUnitForRole(PersistenceMode.TEMPORARY);
    OrganizationalUnit fetchedOrganizationalUnit = this.organizationalUnitClientService.uploadLogo(organizationalUnit1.getResId(), logoToUpload);

    OrganizationalUnit refetchedOrganizationalUnit = this.organizationalUnitClientService.findOne(fetchedOrganizationalUnit.getResId());
    assertEquals(logoName, refetchedOrganizationalUnit.getResourceFile().getFileName());
    assertEquals(sizeLogo, refetchedOrganizationalUnit.getResourceFile().getFileSize());
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.organizationalUnitClientService.deleteLogo(organizationalUnit1.getResId()));
  }
}

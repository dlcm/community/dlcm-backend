/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrganizationalUnitAsManagerInheritedIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.FundingAgencyITService;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class OrganizationalUnitAsManagerInheritedIT extends OrganizationalUnitAsManagerIT {

  @Autowired
  public OrganizationalUnitAsManagerInheritedIT(Environment env, SudoRestClientTool restClientTool,
          OrganizationalUnitClientService organizationalUnitClientService, OrgUnitITService orgUnitITService, InstitutionITService institutionITService,
          FundingAgencyITService fundingAgencyITService, PersonITService personITService) {
    super(env, restClientTool, organizationalUnitClientService, orgUnitITService, institutionITService, fundingAgencyITService, personITService);
  }

  @Override()
  protected boolean inheritedRole() {
    return true;
  }

  @Test
  void creationOrgUnitTest() {
    Institution institution = this.institutionITService.getOrCreatePermanentInstitution(PersistenceMode.PERMANENT, this.role(),
            this.restClientTool.getCurrentRole());
    final OrganizationalUnit orgUnit = this.createRemoteOrgUnit("Swiss Research Data", institution);
    assertNotNull(orgUnit);
  }

  @Test
  void creationOrgUnitNotPossibleIfNoInstitutionTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.createRemoteOrgUnit("Swiss Research Data"));
  }

  @Test
  void creationOrgUnitNotPossibleIfInstitutionNotContainsManagedOneTest() {
    Institution institution = this.institutionITService.getTestInstitutionAsUser(PersistenceMode.PERMANENT, NO_ONE);
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.createRemoteOrgUnit("Swiss Research Data", institution));
  }

  @Test
  void creationOrgUnitWithMultiInstitutionAllowIfManagedOneTest() {
    Institution institutionNoOne = this.institutionITService.getTestInstitutionAsUser(PersistenceMode.PERMANENT, NO_ONE);
    Institution institutionManager = this.institutionITService.getOrCreatePermanentInstitution(PersistenceMode.PERMANENT, this.role(),
            this.restClientTool.getCurrentRole());
    List<Institution> institutionList = new ArrayList<>();
    institutionList.add(institutionManager);
    institutionList.add(institutionNoOne);
    final OrganizationalUnit orgUnit = this.createRemoteOrgUnit("Swiss Research Data", institutionManager);
    assertNotNull(orgUnit);
  }
}

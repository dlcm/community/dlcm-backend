/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveAclAsApproverIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.ArchiveACLClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.OrgUnitITService;

class ArchiveAclAsApproverIT extends AbstractArchiveAclUserIT {

  private final ArchiveACLClientService archiveACLClientService;

  @Autowired
  public ArchiveAclAsApproverIT(Environment env, SudoRestClientTool restClientTool, OrgUnitITService orgUnitITService,
          ArchiveACLClientService archiveACLClientService) {
    super(env, restClientTool, orgUnitITService);
    this.archiveACLClientService = archiveACLClientService;
  }

  @Override
  protected String role() {
    return Role.APPROVER_ID;
  }

  @Test
  void listFailTest() {
    OrganizationalUnit organizationalUnit = this.getPermanentOrganizationalUnitWithCurrentRole();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.archiveACLClientService.searchByProperties(
            Map.of("organizationalUnit.resId", organizationalUnit.getResId())));
  }

  @Test
  void listNoOneFailTest() {
    OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrgUnitForNoOne(PersistenceMode.PERMANENT);
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.archiveACLClientService.searchByProperties(Map.of("organizationalUnit.resId", organizationalUnit.getResId())));
  }
}

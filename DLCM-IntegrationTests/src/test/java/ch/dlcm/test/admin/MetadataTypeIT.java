/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - MetadataTypeIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.MetadataFormat;
import ch.dlcm.model.settings.MetadataType;
import ch.dlcm.service.admin.MetadataTypeClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

class MetadataTypeIT extends AbstractIT {

  public final static String AUTHOR_INVALID_XML = "author-invalid.xml";
  public final static String AUTHOR_SCHEMA = "author.xsd";
  public final static String AUTHOR_XML = "author.xml";

  public final static String PERSON_INVALID_JSON = "person-invalid.json";
  public final static String PERSON_JSON = "person.json";
  public final static String PERSON_SCHEMA = "person.schema.json";

  private final MetadataTypeClientService metadataTypeService;

  @Autowired
  public MetadataTypeIT(Environment env, SudoRestClientTool restClientTool, MetadataTypeClientService metadataTypeClientService) {
    super(env, restClientTool);
    this.metadataTypeService = metadataTypeClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create
    final MetadataType mt1 = this.createMetadataType(MetadataFormat.XML, "XML 1 Metadata", "1.0", null);
    final MetadataType mt2 = this.metadataTypeService.create(mt1);

    // Test the creation
    assertEquals(mt1.getName(), mt2.getName());
    assertEquals(mt1.getVersion(), mt2.getVersion());
    assertEquals(mt1.getMetadataFormat(), mt2.getMetadataFormat());
  }

  @Test
  void deleteTest() {
    // Create
    MetadataType mt1 = this.createMetadataType(MetadataFormat.XML, "XML 3 Metadata", "1.0", null);
    mt1 = this.metadataTypeService.create(mt1);

    // Delete
    final String resId = mt1.getResId();
    this.metadataTypeService.delete(resId);

    // Test deletion
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.metadataTypeService.findOne(resId));
  }

  @Test
  void schemaTest() throws IOException {
    // Create
    final String authorSchema = FileTool.toString(new ClassPathResource(AUTHOR_SCHEMA).getInputStream());
    MetadataType mt1 = this.createMetadataType(MetadataFormat.XML, "XML 4 Metadata", "1.0", authorSchema);
    mt1 = this.metadataTypeService.create(mt1);

    final MetadataType mt2 = this.metadataTypeService.findOne(mt1.getResId());
    assertEquals(authorSchema, mt1.getMetadataSchema());
    assertEquals(authorSchema, mt2.getMetadataSchema());

    final String schema = this.metadataTypeService.getSchema(mt1.getResId());
    assertEquals(authorSchema, schema);
  }

  @Test
  void unicityTest() {
    // Create
    MetadataType mt1 = this.createMetadataType(MetadataFormat.XML, "XML 2 Metadata", "1.0", null);
    mt1 = this.metadataTypeService.create(mt1);

    // Create others
    MetadataType mt2 = this.createMetadataType(MetadataFormat.XML, "XML 2 Metadata", "1.0", null);
    mt2.setName(mt1.getName());
    MetadataType mt3 = this.createMetadataType(MetadataFormat.XML, "XML 2 Metadata", "2.0", null);
    mt3.setName(mt1.getName());
    MetadataType mt4 = this.createMetadataType(MetadataFormat.XML, "XML 2 Metadata", "1.0", null);

    // Testing the unicity
    try {
      this.metadataTypeService.create(mt2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }
    mt3 = this.metadataTypeService.create(mt3);
    mt4 = this.metadataTypeService.create(mt4);
    assertNotNull(this.metadataTypeService.findOne(mt1.getResId()));
    assertNotNull(this.metadataTypeService.findOne(mt3.getResId()));
    assertNotNull(this.metadataTypeService.findOne(mt4.getResId()));
  }

  @Test
  void updateTest() {
    // Create
    MetadataType mt1 = this.createMetadataType(MetadataFormat.XML, "A Metadata", "1.0", null);
    mt1 = this.metadataTypeService.create(mt1);

    // Update
    mt1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("B"));
    this.metadataTypeService.update(mt1.getResId(), mt1);
    final MetadataType mt2 = this.metadataTypeService.findOne(mt1.getResId());

    // Test the update
    assertEquals(mt1.getName(), mt2.getName());
    assertEquals(mt1.getVersion(), mt2.getVersion());
    assertEquals(mt1.getMetadataFormat(), mt2.getMetadataFormat());
    this.assertEqualsWithoutNanoSeconds(mt1.getCreationTime(), mt2.getCreationTime());
  }

  @Test
  void validateAnyMetadataTest() {
    // Create
    MetadataType mt1 = this.createMetadataType(MetadataFormat.SCHEMA_LESS, "10 Metadata", "1.0", null);
    mt1 = this.metadataTypeService.create(mt1);
    // Test
    assertTrue(this.metadataTypeService.validate(mt1.getResId(), new ClassPathResource(AUTHOR_XML)), "Not valid");
    assertTrue(this.metadataTypeService.validate(mt1.getResId(), new ClassPathResource(AUTHOR_INVALID_XML)), "Not valid");
    assertTrue(this.metadataTypeService.validate(mt1.getResId(), new ClassPathResource(PERSON_JSON)), "Not valid");
    assertTrue(this.metadataTypeService.validate(mt1.getResId(), new ClassPathResource(PERSON_INVALID_JSON)), "Not valid");
  }

  @Test
  void validateJSONTest() throws IOException {
    // Create
    MetadataType mt1 = this.createMetadataType(MetadataFormat.JSON, "JSON 9 Metadata", "1.0", null);
    mt1 = this.metadataTypeService.create(mt1);
    // Test without schema
    assertTrue(this.metadataTypeService.validate(mt1.getResId(), new ClassPathResource(PERSON_JSON)), "Not well-formed");
    // Test with schema
    mt1.setMetadataSchema(FileTool.toString(new ClassPathResource(PERSON_SCHEMA).getInputStream()));
    this.metadataTypeService.update(mt1.getResId(), mt1);
    assertTrue(this.metadataTypeService.validate(mt1.getResId(), new ClassPathResource(PERSON_JSON)), "Not valid");
    assertFalse(this.metadataTypeService.validate(mt1.getResId(), new ClassPathResource(PERSON_INVALID_JSON)), "Must not be valid");
  }

  @Test
  void validateXMLTest() throws IOException {
    // Create
    MetadataType mt1 = this.createMetadataType(MetadataFormat.XML, "XML 8 Metadata", "1.0", null);
    mt1 = this.metadataTypeService.create(mt1);
    // Test without schema
    assertTrue(this.metadataTypeService.validate(mt1.getResId(), new ClassPathResource(AUTHOR_XML)), "Not well-formed");
    // Test with schema
    mt1.setMetadataSchema(FileTool.toString(new ClassPathResource(AUTHOR_SCHEMA).getInputStream()));
    this.metadataTypeService.update(mt1.getResId(), mt1);
    assertTrue(this.metadataTypeService.validate(mt1.getResId(), new ClassPathResource(AUTHOR_XML)), "Not valid");
    assertFalse(this.metadataTypeService.validate(mt1.getResId(), new ClassPathResource(AUTHOR_INVALID_XML)), "Must not be valid");
  }

  protected void clearMetadataTypeFixtures() {
    final List<MetadataType> mtList = this.metadataTypeService.findAll();

    for (final MetadataType mt : mtList) {
      if (mt.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.metadataTypeService.delete(mt.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearMetadataTypeFixtures();
  }

  private MetadataType createMetadataType(MetadataFormat format, String name, String version, String schema) {
    final MetadataType mt = new MetadataType();
    mt.setMetadataFormat(format);
    mt.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel(name));
    mt.setVersion(version);
    mt.setMetadataSchema(schema);
    return mt;
  }
}

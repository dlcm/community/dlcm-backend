/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrganizationalUnitAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.display.OrganizationalUnitDisseminationPolicyDTO;
import ch.dlcm.model.display.OrganizationalUnitPreservationPolicyDTO;
import ch.dlcm.model.display.OrganizationalUnitSubmissionPolicyDTO;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.License;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.model.settings.SubjectArea;
import ch.dlcm.service.admin.FundingAgencyClientService;
import ch.dlcm.service.admin.InstitutionClientService;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.service.admin.SubjectAreaClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class OrganizationalUnitAsAdminIT extends AbstractIT {

  private final OrganizationalUnitClientService organizationalUnitClientService;
  private final FundingAgencyClientService fundingAgencyClientService;
  private final InstitutionClientService institutionClientService;
  private final SubjectAreaClientService subjectAreaClientService;
  private final PersonITService personITService;
  private final OrgUnitITService orgUnitITService;

  @Autowired
  public OrganizationalUnitAsAdminIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrganizationalUnitClientService organizationalUnitClientService,
          FundingAgencyClientService fundingAgencyClientService,
          InstitutionClientService institutionClientService,
          SubjectAreaClientService subjectAreaClientService,
          PersonITService personITService,
          OrgUnitITService orgUnitITService) {
    super(env, restClientTool);
    this.organizationalUnitClientService = organizationalUnitClientService;
    this.fundingAgencyClientService = fundingAgencyClientService;
    this.institutionClientService = institutionClientService;
    this.subjectAreaClientService = subjectAreaClientService;
    this.personITService = personITService;
    this.orgUnitITService = orgUnitITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void addAndRemoveFundingAgency() {
    /*
     * Get test orgUnit
     */
    final OrganizationalUnit orgUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #FUNDING AGENCY");
    List<FundingAgency> fundingAgencies = this.organizationalUnitClientService.getFundingAgencies(orgUnit.getResId());

    /*
     * Create a new FundingAgency
     */
    final String fundingAgencyAcronym = DLCMTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String fundingAgencyName = DLCMTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency");
    FundingAgency newAgency = new FundingAgency();
    newAgency.setAcronym(fundingAgencyAcronym);
    newAgency.setName(fundingAgencyName);
    newAgency = this.fundingAgencyClientService.create(newAgency);

    /*
     * Add the FundingAgency to the OrganizationalUnit
     */
    this.organizationalUnitClientService.addFundingAgency(orgUnit.getResId(), newAgency.getResId());

    /*
     * Check it has been added
     */
    fundingAgencies = this.organizationalUnitClientService.getFundingAgencies(orgUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(1, fundingAgencies.size());
    final Optional<FundingAgency> optionalFA = fundingAgencies.stream().filter(fa -> fa.getName().equals(fundingAgencyName)).findFirst();
    assertTrue(optionalFA.isPresent());
    assertEquals(fundingAgencyName, optionalFA.get().getName());

    /*
     * Remove the FundingAgency
     */
    this.organizationalUnitClientService.removeFundingAgency(orgUnit.getResId(), newAgency.getResId());

    /*
     * Check it has been removed
     */
    fundingAgencies = this.organizationalUnitClientService.getFundingAgencies(orgUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(0, fundingAgencies.size());

    /*
     * Create 3 FundingAgencies and link them to the OrganizationalUnit
     */
    final FundingAgency subFA1 = new FundingAgency();
    subFA1.setAcronym(DLCMTestConstants.getRandomNameWithTemporaryLabel("Sub FA 1"));
    subFA1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Sub FundingAgency 1"));

    final FundingAgency subFA2 = new FundingAgency();
    subFA2.setAcronym(DLCMTestConstants.getRandomNameWithTemporaryLabel("Sub FA 2"));
    subFA2.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Sub FundingAgency 2"));

    final FundingAgency subFA3 = new FundingAgency();
    subFA3.setAcronym(DLCMTestConstants.getRandomNameWithTemporaryLabel("Sub FA 3"));
    subFA3.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Sub FundingAgency 3"));

    final FundingAgency[] subAgencies = new FundingAgency[] { subFA1, subFA2, subFA3 };

    String agencyIdToDelete = null;

    int i = 0;
    for (FundingAgency subAgency : subAgencies) {
      subAgency = this.fundingAgencyClientService.create(subAgency);
      this.organizationalUnitClientService.addFundingAgency(orgUnit.getResId(), subAgency.getResId());

      if (i == 1) {
        agencyIdToDelete = subAgency.getResId();
      }

      i++;
    }

    /*
     * Check they are all linked correctly
     */
    fundingAgencies = this.organizationalUnitClientService.getFundingAgencies(orgUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(3, fundingAgencies.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.organizationalUnitClientService.removeFundingAgency(orgUnit.getResId(), agencyIdToDelete);
    fundingAgencies = this.organizationalUnitClientService.getFundingAgencies(orgUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(2, fundingAgencies.size());
    for (final FundingAgency fetched_agency : fundingAgencies) {
      assertNotEquals(agencyIdToDelete, fetched_agency.getResId());
    }

  }

  @Test
  void addAndRemoveInstitutionTest() {
    // Get test orgUnit
    final OrganizationalUnit orgUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #INSTITUTION");
    List<Institution> list = this.organizationalUnitClientService.getInstitutions(orgUnit.getResId());

    // Create an institution
    Institution institution1 = new Institution();
    institution1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("institution "));
    institution1.setDescription("description" + ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE));
    institution1 = this.institutionClientService.create(institution1);

    // Add the institution to the OrganizationalUnit
    this.organizationalUnitClientService.addInstitution(orgUnit.getResId(), institution1.getResId());
    list = this.organizationalUnitClientService.getInstitutions(orgUnit.getResId());
    assertEquals(1, list.size());
    final String institutionName = institution1.getName();
    final Optional<Institution> optionalInstitution = list.stream().filter(institution -> institution.getName().equals(institutionName))
            .findFirst();
    assertTrue(optionalInstitution.isPresent());

    // Test the institution
    assertEquals(institution1.getResId(), optionalInstitution.get().getResId());
    assertEquals(institution1.getName(), optionalInstitution.get().getName());
    assertEquals(institution1.getDescription(), optionalInstitution.get().getDescription());

    this.organizationalUnitClientService.removeInstitution(orgUnit.getResId(), institution1.getResId());
    list = this.organizationalUnitClientService.getInstitutions(orgUnit.getResId());
    assertEquals(0, list.size());
  }

  @Test
  void addAndRemoveSubjectAreaTest() {
    // Get test orgUnit
    final OrganizationalUnit orgUnit = this.createRemoteOrgUnit(
            "Swiss Research Data Preservation #SUBJECTAREA, Test Method : OrganizationalUnitIT.addAndRemoveSubjectAreaTest");

    // Create a SubjectArea
    SubjectArea subjectArea1 = new SubjectArea();
    subjectArea1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("subjectArea "));
    subjectArea1.setCode("Z" + ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE));
    subjectArea1.setSource("test1");
    subjectArea1 = this.subjectAreaClientService.create(subjectArea1);

    // Add the subjectArea1 to the OrganizationalUnit
    this.organizationalUnitClientService.addSubjectArea(orgUnit.getResId(), subjectArea1.getResId());
    List<SubjectArea> subjectAreasList = this.organizationalUnitClientService.getSubjectAreas(orgUnit.getResId());
    assertEquals(1, subjectAreasList.size());
    final String subjectAreaName = subjectArea1.getName();
    final Optional<SubjectArea> optionalSubjectArea = subjectAreasList.stream()
            .filter(subjectArea -> subjectArea.getName().equals(subjectAreaName))
            .findFirst();
    assertTrue(optionalSubjectArea.isPresent());

    // Test the institution
    assertEquals(subjectArea1.getResId(), optionalSubjectArea.get().getResId());
    assertEquals(subjectArea1.getName(), optionalSubjectArea.get().getName());
    assertEquals(subjectArea1.getCode(), optionalSubjectArea.get().getCode());
    assertEquals(subjectArea1.getSource(), optionalSubjectArea.get().getSource());

    this.organizationalUnitClientService.removeSubjectArea(orgUnit.getResId(), subjectArea1.getResId());
    subjectAreasList = this.organizationalUnitClientService.getSubjectAreas(orgUnit.getResId());
    assertEquals(0, subjectAreasList.size());
  }

  @Test
  void addAndRemoveDisseminationPolicyTest() {
    // Get test orgUnit
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Dissemination #dissemination policy");
    final String orgUnitId = organizationalUnit.getResId();

    // Create policy
    this.restClientTool.sudoRoot();
    final DisseminationPolicy policy1 = this.orgUnitITService.createRemoteDisseminationPolicy();
    this.restClientTool.exitSudo();
    final String policy1ResId = policy1.getResId();

    // Add policy to orgUnit
    this.organizationalUnitClientService.addDisseminationPolicy(orgUnitId, policy1.getResId());
    List<OrganizationalUnitDisseminationPolicyDTO> policies = this.organizationalUnitClientService.getDisseminationPolicies(orgUnitId);
    assertEquals(1, policies.size());

    // Test added dissemination policy on the OrganizationalUnit. It automatically gets the default one
    OrganizationalUnitDisseminationPolicyDTO oupOpt1 = policies.stream().
            filter(oup -> oup.getResId().equals(policy1ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Dissemination policy not found"));

    assertEquals(policy1.getName(), oupOpt1.getName());

    // Check that nothing has changed
    policies = this.organizationalUnitClientService.getDisseminationPolicies(orgUnitId);
    assertEquals(1, policies.size());

    // create another dissemination policy and set this new one as default
    this.restClientTool.sudoRoot();
    final DisseminationPolicy policy2 = this.orgUnitITService.createRemoteDisseminationPolicy();
    this.restClientTool.exitSudo();
    this.organizationalUnitClientService.addDisseminationPolicy(orgUnitId, policy2.getResId());

    // Check that the 1st policy is not the default one anymore and that it has been replaced by the 2nd one
    policies = this.organizationalUnitClientService.getDisseminationPolicies(orgUnitId);
    assertEquals(2, policies.size());
    oupOpt1 = policies.stream().filter(oup -> oup.getResId().equals(policy1ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Dissemination policy not found"));
    OrganizationalUnitDisseminationPolicyDTO oupOpt2 = policies.stream()
            .filter(oup -> oup.getResId().equals(policy2.getResId()))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Dissemination policy not found"));

    assertEquals(policy1.getName(), oupOpt1.getName());
    assertEquals(policy2.getName(), oupOpt2.getName());

    // create a 3rd dissemination policy and set it as default
    this.restClientTool.sudoRoot();
    final DisseminationPolicy policy3 = this.orgUnitITService.createRemoteDisseminationPolicy();
    this.restClientTool.exitSudo();
    final String policy3ResId = policy3.getResId();
    this.organizationalUnitClientService.addDisseminationPolicy(orgUnitId, policy3ResId);

    // Check that the 1st and 2nd policies are not the default one anymore and that it has been replaced by the 3rd one
    policies = this.organizationalUnitClientService.getDisseminationPolicies(orgUnitId);
    assertEquals(3, policies.size());
    oupOpt1 = policies.stream().filter(oup -> oup.getResId().equals(policy1ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Dissemination policy not found"));

    oupOpt2 = policies.stream()
            .filter(oup -> oup.getResId().equals(policy2.getResId()))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Dissemination policy not found"));

    final OrganizationalUnitDisseminationPolicyDTO oupOpt3 = policies.stream().filter(oup -> oup.getResId().equals(policy3ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Dissemination policy not found"));

    assertEquals(policy1.getName(), oupOpt1.getName());
    assertEquals(policy2.getName(), oupOpt2.getName());
    assertEquals(policy3.getName(), oupOpt3.getName());

    // Remove policy
    this.organizationalUnitClientService.removeDisseminationPolicies(orgUnitId, policy1.getResId());
    this.organizationalUnitClientService.removeDisseminationPolicies(orgUnitId, policy2.getResId());
    this.organizationalUnitClientService.removeDisseminationPolicies(orgUnitId, policy3ResId);

    // Test remove
    policies = this.organizationalUnitClientService.getDisseminationPolicies(orgUnitId);
    assertEquals(0, policies.size());
  }

  @Test
  void addAndRemovePreservationPolicyTest() {
    // Get test orgUnit
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #PRESERVATION POLICY");
    final String orgUnitId = organizationalUnit.getResId();

    // Create policy
    final PreservationPolicy policy1 = this.orgUnitITService.createRemotePreservationPolicy();
    final String policy1ResId = policy1.getResId();

    // Add policy to orgUnit
    this.organizationalUnitClientService.addPreservationPolicy(orgUnitId, policy1.getResId());
    List<OrganizationalUnitPreservationPolicyDTO> policies = this.organizationalUnitClientService.getPreservationPolicies(orgUnitId);
    assertEquals(1, policies.size());

    // Test added preservation policy on the OrganizationalUnit. It automatically gets the default one
    OrganizationalUnitPreservationPolicyDTO oupOpt1 = policies.stream()
            .filter(oup -> oup.getResId().equals(policy1ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Preservation policy not found"));
    assertEquals(policy1.getName(), oupOpt1.getName());
    assertEquals(policy1.getRetention(), oupOpt1.getRetention());
    assertEquals(policy1.getDispositionApproval(), oupOpt1.getDispositionApproval());
    assertTrue(oupOpt1.getJoinResource().getDefaultPolicy());

    // Try to set the 1st policy as not the default one. It should fail.
    HttpClientErrorException e = assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.organizationalUnitClientService.setPreservationPolicyDefaultValue(orgUnitId, policy1ResId, false));
    this.assertFieldContainsValidationErrorMessage(e, "defaultPolicy", "A default preservation policy is required for each organizational unit");

    // Check that nothing has changed
    policies = this.organizationalUnitClientService.getPreservationPolicies(orgUnitId);
    assertEquals(1, policies.size());
    oupOpt1 = policies.stream()
            .filter(oup -> oup.getResId().equals(policy1ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Preservation policy not found"));

    assertTrue(oupOpt1.getJoinResource().getDefaultPolicy());

    // create another preservation policy and set this new one as default
    final PreservationPolicy policy2 = this.orgUnitITService.createRemotePreservationPolicy();
    this.organizationalUnitClientService.addPreservationPolicy(orgUnitId, policy2.getResId());
    this.organizationalUnitClientService.setPreservationPolicyDefaultValue(orgUnitId, policy2.getResId(), true);

    // Check that the 1st policy is not the default one anymore and that it has been replaced by the 2nd one
    policies = this.organizationalUnitClientService.getPreservationPolicies(orgUnitId);
    assertEquals(2, policies.size());
    oupOpt1 = policies.stream()
            .filter(oup -> oup.getResId().equals(policy1ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Preservation policy not found"));

    OrganizationalUnitPreservationPolicyDTO oupOpt2 = policies.stream()
            .filter(oup -> oup.getResId().equals(policy2.getResId()))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Preservation policy not found"));

    assertEquals(policy1.getName(), oupOpt1.getName());
    assertFalse(oupOpt1.getJoinResource().getDefaultPolicy());
    assertEquals(policy2.getName(), oupOpt2.getName());
    assertTrue(oupOpt2.getJoinResource().getDefaultPolicy());

    // create a 3rd preservation policy and set it as default
    final PreservationPolicy policy3 = this.orgUnitITService.createRemotePreservationPolicy();
    final String policy3ResId = policy3.getResId();
    this.organizationalUnitClientService.addPreservationPolicy(orgUnitId, policy3ResId);
    this.organizationalUnitClientService.setPreservationPolicyDefaultValue(orgUnitId, policy3ResId, true);

    // Check that the 1st and 2nd policies are not the default one anymore and that it has been replaced by the 3rd one
    policies = this.organizationalUnitClientService.getPreservationPolicies(orgUnitId);
    assertEquals(3, policies.size());
    oupOpt1 = policies.stream()
            .filter(oup -> oup.getResId().equals(policy1ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Preservation policy not found"));

    oupOpt2 = policies.stream()
            .filter(oup -> oup.getResId().equals(policy2.getResId()))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Preservation policy not found"));

    final OrganizationalUnitPreservationPolicyDTO oupOpt3 = policies.stream()
            .filter(oup -> oup.getResId().equals(policy3ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Preservation policy not found"));

    assertEquals(policy1.getName(), oupOpt1.getName());
    assertEquals(policy2.getName(), oupOpt2.getName());
    assertFalse(oupOpt2.getJoinResource().getDefaultPolicy());
    assertEquals(policy3.getName(), oupOpt3.getName());
    assertTrue(oupOpt3.getJoinResource().getDefaultPolicy());

    // Try to set the 3rd policy as not the default one. It should fail.
    HttpClientErrorException e3 = assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.organizationalUnitClientService.setPreservationPolicyDefaultValue(orgUnitId, policy3ResId, false));
    this.assertFieldContainsValidationErrorMessage(e3, "defaultPolicy",
            "A default preservation policy is required for each organizational unit");

    // Remove policy
    this.organizationalUnitClientService.removePreservationPolicy(orgUnitId, policy1.getResId());
    this.organizationalUnitClientService.removePreservationPolicy(orgUnitId, policy2.getResId());
    this.organizationalUnitClientService.removePreservationPolicy(orgUnitId, policy3ResId);

    // Test remove
    policies = this.organizationalUnitClientService.getPreservationPolicies(orgUnitId);
    assertEquals(0, policies.size());
  }

  @Test
  void addAndRemoveSubmissionPolicyTest() {
    // Get test orgUnit
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #SUBMISSION POLICY");
    final String orgUnitId = organizationalUnit.getResId();

    // Create policy
    final SubmissionPolicy policy1 = this.orgUnitITService.createRemoteSubmissionPolicy();
    final String policy1ResId = policy1.getResId();

    // Add 1st policy to orgUnit. It automatically gets the default one
    this.organizationalUnitClientService.addSubmissionPolicy(orgUnitId, policy1ResId);

    List<OrganizationalUnitSubmissionPolicyDTO> orgUnitPolicies = this.organizationalUnitClientService.getSubmissionPolicies(orgUnitId);
    assertEquals(1, orgUnitPolicies.size());

    // Test added submission policy on the OrganizationalUnit
    OrganizationalUnitSubmissionPolicyDTO oupOpt1 = orgUnitPolicies.stream().filter(oup -> oup.getResId().equals(policy1ResId))
            .findFirst().orElseThrow();
    assertEquals(policy1.getName(), oupOpt1.getName());
    assertEquals(policy1.getSubmissionApproval(), policy1.getSubmissionApproval());
    assertTrue(oupOpt1.getJoinResource().getDefaultPolicy());

    // Try to set the 1st policy as not the default one. It should fail.
    HttpClientErrorException e = assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.organizationalUnitClientService.setSubmissionPolicyDefaultValue(orgUnitId, policy1ResId, false));
    this.assertFieldContainsValidationErrorMessage(e, "defaultPolicy", "A default submission policy is required for each organizational unit");

    // Check that nothing has changed
    orgUnitPolicies = this.organizationalUnitClientService.getSubmissionPolicies(orgUnitId);
    assertEquals(1, orgUnitPolicies.size());
    oupOpt1 = orgUnitPolicies.stream().filter(oup -> oup.getResId().equals(policy1ResId)).findFirst().orElseThrow();
    assertTrue(oupOpt1.getJoinResource().getDefaultPolicy());

    // create another submission policy and set this new one as default
    final SubmissionPolicy policy2 = this.orgUnitITService.createRemoteSubmissionPolicy();
    this.organizationalUnitClientService.addSubmissionPolicy(orgUnitId, policy2.getResId());
    this.organizationalUnitClientService.setSubmissionPolicyDefaultValue(orgUnitId, policy2.getResId(), true);

    // Check that the 1st policy is not the default one anymore and that it has been replaced by the 2nd one
    orgUnitPolicies = this.organizationalUnitClientService.getSubmissionPolicies(orgUnitId);
    assertEquals(2, orgUnitPolicies.size());
    oupOpt1 = orgUnitPolicies.stream().filter(oup -> oup.getResId().equals(policy1ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Submission policy not found"));

    OrganizationalUnitSubmissionPolicyDTO oupOpt2 = orgUnitPolicies.stream()
            .filter(oup -> oup.getResId().equals(policy2.getResId()))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Submission policy not found"));

    assertEquals(policy1.getName(), oupOpt1.getName());
    assertFalse(oupOpt1.getJoinResource().getDefaultPolicy());
    assertEquals(policy2.getName(), oupOpt2.getName());
    assertTrue(oupOpt2.getJoinResource().getDefaultPolicy());

    // create a 3rd submission policy and set it as default
    final SubmissionPolicy policy3 = this.orgUnitITService.createRemoteSubmissionPolicy();
    final String policy3ResId = policy3.getResId();
    this.organizationalUnitClientService.addSubmissionPolicy(orgUnitId, policy3ResId);
    this.organizationalUnitClientService.setSubmissionPolicyDefaultValue(orgUnitId, policy3ResId, true);

    // Check that the 1st and 2nd policies are not the default one anymore and that it has been replaced by the 3rd one
    orgUnitPolicies = this.organizationalUnitClientService.getSubmissionPolicies(orgUnitId);
    assertEquals(3, orgUnitPolicies.size());
    oupOpt1 = orgUnitPolicies.stream().filter(oup -> oup.getResId().equals(policy1ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Submission policy not found"));

    oupOpt2 = orgUnitPolicies.stream()
            .filter(oup -> oup.getResId().equals(policy2.getResId()))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Submission policy not found"));

    final OrganizationalUnitSubmissionPolicyDTO oupOpt3 = orgUnitPolicies.stream()
            .filter(oup -> oup.getResId().equals(policy3ResId))
            .findFirst()
            .orElseThrow(() -> new AssertionError("Submission policy not found"));

    assertEquals(policy1.getName(), oupOpt1.getName());
    assertFalse(oupOpt1.getJoinResource().getDefaultPolicy());
    assertEquals(policy2.getName(), oupOpt2.getName());
    assertFalse(oupOpt2.getJoinResource().getDefaultPolicy());
    assertEquals(policy3.getName(), oupOpt3.getName());
    assertTrue(oupOpt3.getJoinResource().getDefaultPolicy());

    // Try to set the 3rd policy as not the default one. It should fail.
    HttpClientErrorException e3 = assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.organizationalUnitClientService.setSubmissionPolicyDefaultValue(orgUnitId, policy3ResId, false));
    this.assertFieldContainsValidationErrorMessage(e3, "defaultPolicy", "A default submission policy is required for each organizational unit");

    // Remove policies
    this.organizationalUnitClientService.removeSubmissionPolicy(orgUnitId, policy1ResId);
    this.organizationalUnitClientService.removeSubmissionPolicy(orgUnitId, policy2.getResId());
    this.organizationalUnitClientService.removeSubmissionPolicy(orgUnitId, policy3ResId);

    // Test remove
    orgUnitPolicies = this.organizationalUnitClientService.getSubmissionPolicies(orgUnitId);
    assertEquals(0, orgUnitPolicies.size());
  }

  @Test
  void addPersonToTheOrganizationalUnitTest() {
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #PERSON");
    final Person person = this.personITService.createRemotePerson();
    final int peopleBeforeAdding = this.organizationalUnitClientService.getPeople(organizationalUnit.getResId()).size();

    // Add the person to the OrganizationalUnit with default role
    this.organizationalUnitClientService.addPerson(organizationalUnit.getResId(), person.getResId());
    final List<Person> people = this.organizationalUnitClientService.getPeople(organizationalUnit.getResId());
    assertEquals(people.size(), peopleBeforeAdding + 1);

    // Test added person on the OrganizationalUnit
    for (final Person person2 : people) {
      if (person2.getResId().equals(person.getResId())) {
        assertNotNull(person2);
        assertEquals(person.getFirstName(), person2.getFirstName());
        assertEquals(person.getLastName(), person2.getLastName());
        assertEquals(person.getOrcid(), person2.getOrcid());
      }
    }
  }

  @Test
  void addUnknownPersonToTheOrganizationalUnitTest() {
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #EXISTENCE");
    assertThrows(HttpClientErrorException.NotFound.class,
            () -> this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), "unknown-person", Role.CREATOR_ID));
  }

  @Test
  void addUnknownRoleToTheOrganizationalUnitTest() {
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #EXISTENCE");
    final Person person = this.personITService.createRemotePerson();
    assertThrows(HttpClientErrorException.NotFound.class,
            () -> this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person.getResId(), "unknown-role"));
  }

  @Test
  void addUnknownOrgUnitToTheOrganizationalUnitTest() {
    final Person person = this.personITService.createRemotePerson();
    assertThrows(HttpClientErrorException.NotFound.class,
            () -> this.organizationalUnitClientService.addPersonRole("unknown-org-unit", person.getResId(), Role.CREATOR_ID));
  }

  @Test
  void addMultipleRolesForPersonToTheOrganizationalUnitTest() {
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #ROLES");
    final Person person1 = this.personITService.createRemotePerson();
    final Person person2 = this.personITService.createRemotePerson();
    final int peopleBeforeAdding = this.organizationalUnitClientService.getPeople(organizationalUnit.getResId()).size();

    // Add the persons to the OrganizationalUnit with roles
    this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person1.getResId(), Role.STEWARD_ID);
    this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person2.getResId(), Role.CREATOR_ID);
    final List<Person> people = this.organizationalUnitClientService.getPeople(organizationalUnit.getResId());
    assertEquals(peopleBeforeAdding + 2, people.size());
    // Add the same role
    this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person1.getResId(), Role.STEWARD_ID);
    // Add the new role
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person1.getResId(), Role.CREATOR_ID));
  }

  @Test
  void changeRoleOfAPersonInsideAnOrganizationalUnitTest() {
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #ROLE Person");
    final Person person = this.personITService.createRemotePerson();

    // Add the person to the OrganizationalUnit with default role (VISITOR)
    this.organizationalUnitClientService.addPerson(organizationalUnit.getResId(), person.getResId());
    this.organizationalUnitClientService.removePerson(organizationalUnit.getResId(), person.getResId());

    // Add the person to the OrganizationalUnit with other roles
    this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person.getResId(), Role.MANAGER_ID);
    final Role role = this.organizationalUnitClientService.getPersonRole(organizationalUnit.getResId(), person.getResId());
    assertEquals(Role.MANAGER_ID, role.getResId());
  }

  @Test
  void closeTest() {
    OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #CLOSE");

    // Close
    final String resId = organizationalUnit.getResId();
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringTool.DATE_FORMAT);
    final String closingDate = LocalDate.now().plusDays(1).format(formatter);

    this.organizationalUnitClientService.close(resId, closingDate);
    organizationalUnit = this.organizationalUnitClientService.findOne(resId);

    // Test close
    assertEquals(organizationalUnit.getClosingDate().toString(), closingDate);

    this.organizationalUnitClientService.delete(organizationalUnit.getResId());
  }

  @Test
  void creationTest() {
    // Create an OrganizationalUnit
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #CREATE");

    final OrganizationalUnit organizationalUnit2 = this.organizationalUnitClientService.findOne(organizationalUnit.getResId());

    // Test the creation
    assertEquals(organizationalUnit.getName(), organizationalUnit2.getName());
    assertEquals(organizationalUnit.getDescription(), organizationalUnit2.getDescription());
    assertThat(organizationalUnit.getKeywords(), is(organizationalUnit2.getKeywords()));

    this.organizationalUnitClientService.delete(organizationalUnit.getResId());
  }

  @Test
  void creationWithOpeningClosingDatesTest() {
    // Create an OrganizationalUnit
    OrganizationalUnit organizationalUnit1 = new OrganizationalUnit();
    organizationalUnit1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("DLCM project"));
    this.orgUnitITService.setRandomUrlOrganizationalUnit(organizationalUnit1);
    organizationalUnit1.setDescription("Swiss Research Data Preservation #CREATE");

    final LocalDate openingDate = LocalDate.now();
    final LocalDate closingDate = openingDate.plusDays(7);

    organizationalUnit1.setOpeningDate(openingDate);
    organizationalUnit1.setClosingDate(closingDate);

    organizationalUnit1 = this.organizationalUnitClientService.create(organizationalUnit1);
    final OrganizationalUnit organizationalUnit2 = this.organizationalUnitClientService.findOne(organizationalUnit1.getResId());

    // Test the creation
    assertEquals(organizationalUnit1.getName(), organizationalUnit2.getName(), "Names must match");
    assertEquals(organizationalUnit1.getDescription(), organizationalUnit2.getDescription(), "Descriptions must match");
    this.assertEqualsWithoutNanoSeconds("Creation times must match", organizationalUnit1.getCreationTime(),
            organizationalUnit2.getCreationTime());
    assertEquals(openingDate, organizationalUnit2.getOpeningDate(), "Opening dates must match");
    assertEquals(closingDate, organizationalUnit2.getClosingDate(), "Closing dates must match");

    this.organizationalUnitClientService.delete(organizationalUnit1.getResId());

  }

  @Test
  void creationWithWrongOpeningClosingDatesTest() {

    // Create an OrganizationalUnit
    OrganizationalUnit organizationalUnit1 = new OrganizationalUnit();
    organizationalUnit1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("DLCM project"));
    this.orgUnitITService.setRandomUrlOrganizationalUnit(organizationalUnit1);
    organizationalUnit1.setDescription("Swiss Research Data Preservation #CREATE");

    final LocalDate closingDate = LocalDate.now();
    final LocalDate openingDate = closingDate.plusDays(7);
    organizationalUnit1.setOpeningDate(openingDate);
    organizationalUnit1.setClosingDate(closingDate);

    boolean success = true;
    try {
      organizationalUnit1 = this.organizationalUnitClientService.create(organizationalUnit1);
    } catch (final Exception e) {
      success = false;
    }
    assertFalse(success);
    final String orgUnit1Id = organizationalUnit1.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.organizationalUnitClientService.findOne(orgUnit1Id));
  }

  @Test
  void defaultLicenseTest() {
    final String orgUnitDefaultLicenseId = "CC-BY-NC-ND-4.0";
    final String orgUnitDefaultLicenseTitle = "Creative Commons Attribution Non Commercial No Derivatives 4.0 International";

    // Test the presence of the system wide default license on a newly created organizational unit
    OrganizationalUnit orgUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);
    final String orgUnitId = orgUnit.getResId();

    // Test the setting of a organizational unit specific license
    License newDefaultLicense = new License();
    newDefaultLicense.setResId(orgUnitDefaultLicenseId);
    orgUnit.setDefaultLicense(newDefaultLicense);
    this.organizationalUnitClientService.update(orgUnitId, orgUnit);
    orgUnit = this.organizationalUnitClientService.findOne(orgUnitId);
    assertEquals(orgUnitDefaultLicenseId, orgUnit.getDefaultLicense().getResId());
    assertEquals(orgUnitDefaultLicenseTitle, orgUnit.getDefaultLicense().getTitle());

    // Test that setting a garbage license title implies 500 internal error
    OrganizationalUnit orgUnitWithLicenseTitle = new OrganizationalUnit();
    newDefaultLicense.setTitle("KKKKKKKKKK");
    orgUnitWithLicenseTitle.setDefaultLicense(newDefaultLicense);
    assertThrows(HttpServerErrorException.InternalServerError.class,
            () -> this.organizationalUnitClientService.update(orgUnitId, orgUnitWithLicenseTitle, List.of("defaultLicense.title")));

    orgUnit = this.organizationalUnitClientService.findOne(orgUnitId);
    assertEquals(orgUnitDefaultLicenseId, orgUnit.getDefaultLicense().getResId());
    assertEquals(orgUnitDefaultLicenseTitle, orgUnit.getDefaultLicense().getTitle());

    // Try to reset the default license
    orgUnit.setDefaultLicense(null);
    this.organizationalUnitClientService.update(orgUnitId, orgUnit);
    orgUnit = this.organizationalUnitClientService.findOne(orgUnitId);
    assertNull(orgUnit.getDefaultLicense());

    // Try to add an non-existing license
    newDefaultLicense = new License();
    newDefaultLicense.setResId("KKKKKKKKKK");
    newDefaultLicense.setTitle("KKKKKKKKKKK");
    orgUnit.setDefaultLicense(newDefaultLicense);
    try {
      this.organizationalUnitClientService.update(orgUnitId, orgUnit);
      fail("A NOT_FOUND Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
    }
  }

  @Test
  void deleteTest() {
    // Create a new OrganizationalUnit
    OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation");

    // Delete the organizationalUnit
    final String resId = organizationalUnit.getResId();
    this.organizationalUnitClientService.delete(resId);
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.organizationalUnitClientService.findOne(resId));
  }

  @Test
  void removePersonFromAnOrganizationalUnitTest() {
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnit("Swiss Research Data Preservation #REMOVE PERSON");
    final Person person = this.personITService.createRemotePerson();

    // Add the person to the OrganizationalUnit with default role
    this.organizationalUnitClientService.addPerson(organizationalUnit.getResId(), person.getResId());
    List<Person> people = this.organizationalUnitClientService.getPeople(organizationalUnit.getResId());

    final int nbPepole = people.size();
    this.organizationalUnitClientService.removePerson(organizationalUnit.getResId(), person.getResId());
    people = this.organizationalUnitClientService.getPeople(organizationalUnit.getResId());
    assertEquals(nbPepole - 1, people.size());
  }

  @Test
  void unicityTest() {
    // Creating a new Organizational Unit
    OrganizationalUnit organizationalUnit1 = this.createRemoteOrgUnit("Swiss Research Data Preservation #CREATE");
    organizationalUnit1 = this.organizationalUnitClientService.findOne(organizationalUnit1.getResId());

    // Creating a second Organizational unit with the same name as the first's
    OrganizationalUnit organizationalUnit2 = new OrganizationalUnit();
    organizationalUnit2.setName(organizationalUnit1.getName());
    this.orgUnitITService.setRandomUrlOrganizationalUnit(organizationalUnit2);
    organizationalUnit2.setDescription("Dufourian Research Data Preservation #CREATE");

    // Testing the unicity of name/**/
    try {
      this.organizationalUnitClientService.create(organizationalUnit2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }
  }

  @Test
  void updateTest() {
    // Create an OrganizationalUnit
    final OrganizationalUnit organizationalUnit1 = this.createRemoteOrgUnit("Swiss Research Data Preservation #UPDATE");

    // Update the person
    final OrganizationalUnit organizationalUnit2 = new OrganizationalUnit();
    organizationalUnit2.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("DLCM project 2.0"));
    this.orgUnitITService.setRandomUrlOrganizationalUnit(organizationalUnit2);
    organizationalUnit2.setDescription("Swiss Research Data Preservation (v2) #UPDATE");

    // Update the keywords
    organizationalUnit2.setKeywords(Arrays.asList("astrochemistry ", "immunohistochemistry ", "phytochemistry"));
    final OrganizationalUnit organizationalUnit3 = this.organizationalUnitClientService.update(organizationalUnit1.getResId(),
            organizationalUnit2);

    // Test the update
    assertEquals(organizationalUnit1.getResId(), organizationalUnit3.getResId());
    assertEquals(organizationalUnit2.getDescription(), organizationalUnit3.getDescription());
    assertThat(organizationalUnit3.getKeywords(), is(organizationalUnit2.getKeywords()));

  }

  @Test
  void uploadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an org unit
     */
    final OrganizationalUnit organizationalUnit1 = this.createRemoteOrgUnit("Swiss Research Data Preservation #LOGO");

    /*
     * Add logo
     */
    OrganizationalUnit fetchedOrgUnit = this.organizationalUnitClientService.uploadLogo(organizationalUnit1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    OrganizationalUnit refetchedOrgUnit = this.organizationalUnitClientService.findOne(fetchedOrgUnit.getResId());
    assertEquals(logoName, refetchedOrgUnit.getResourceFile().getFileName());
    assertEquals(sizeLogo, refetchedOrgUnit.getResourceFile().getFileSize());

    /*
     * Update org unit again
     */
    String newName = DLCMTestConstants.getRandomNameWithTemporaryLabel("DLCM project");
    refetchedOrgUnit.setName(newName);
    refetchedOrgUnit = this.organizationalUnitClientService.update(refetchedOrgUnit.getResId(), refetchedOrgUnit);
    assertEquals(refetchedOrgUnit.getName(), newName);

  }

  @Test
  void downloadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an org unit
     */
    final OrganizationalUnit organizationalUnit1 = this.createRemoteOrgUnit("Swiss Research Data Preservation #LOGO");

    /*
     * Add logo
     */
    OrganizationalUnit organizationalUnit2 = this.organizationalUnitClientService.uploadLogo(organizationalUnit1.getResId(), logoToUpload);
    /*
     * Check logo has been uploaded
     */
    final OrganizationalUnit refetchedOrgUnit = this.organizationalUnitClientService.findOne(organizationalUnit2.getResId());
    assertEquals(logoName, refetchedOrgUnit.getResourceFile().getFileName());
    assertEquals(sizeLogo, refetchedOrgUnit.getResourceFile().getFileSize());

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.organizationalUnitClientService.downloadLogo(refetchedOrgUnit.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedOrgUnit.getResourceFile().getFileSize());
  }

  @Test
  void deleteLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an org unit
     */
    final OrganizationalUnit organizationalUnit1 = this.createRemoteOrgUnit("Swiss Research Data Preservation #LOGO");

    /*
     * Add logo
     */
    OrganizationalUnit fetchedOrgUnit = this.organizationalUnitClientService.uploadLogo(organizationalUnit1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    OrganizationalUnit refetchedOrgUnit = this.organizationalUnitClientService.findOne(fetchedOrgUnit.getResId());
    assertEquals(logoName, refetchedOrgUnit.getResourceFile().getFileName());
    assertEquals(sizeLogo, refetchedOrgUnit.getResourceFile().getFileSize());

    this.organizationalUnitClientService.deleteLogo(organizationalUnit1.getResId());
    refetchedOrgUnit = this.organizationalUnitClientService.findOne(fetchedOrgUnit.getResId());
    assertNull(refetchedOrgUnit.getResourceFile());
  }

  /*************************************************/

  @Override
  protected void deleteFixtures() {
    this.orgUnitITService.cleanTestData();
    this.personITService.cleanTestData();
  }

  private OrganizationalUnit createRemoteOrgUnit(String description) {
    // Create a new OrganizationalUnit
    OrganizationalUnit organizationalUnit = new OrganizationalUnit();
    organizationalUnit.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("DLCM project"));
    this.orgUnitITService.setRandomUrlOrganizationalUnit(organizationalUnit);
    organizationalUnit.setDescription(description);
    organizationalUnit.setKeywords(Arrays.asList("magnetostatics", "photonics", "oceanography"));
    organizationalUnit = this.organizationalUnitClientService.create(organizationalUnit);
    return organizationalUnit;
  }
}

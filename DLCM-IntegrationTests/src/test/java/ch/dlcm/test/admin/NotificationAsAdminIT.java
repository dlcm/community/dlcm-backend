/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - NotificationAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.NotificationClientService;
import ch.dlcm.service.admin.UserClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class NotificationAsAdminIT extends AbstractNotificationIT {

  @Autowired
  public NotificationAsAdminIT(Environment env, SudoRestClientTool restClientTool, NotificationClientService notificationClientService,
          UserClientService userClientService, OrgUnitITService orgUnitITService, PersonITService personITService,
          DepositITService depositITService) {
    super(env, restClientTool, notificationClientService, userClientService, orgUnitITService, personITService, depositITService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void changeStatusTest() {
    final OrganizationalUnit notifiedOrgUnit = this.createOrgUnitForCurrentUser(Role.MANAGER.getResId());
    final Notification localNotification = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, notifiedOrgUnit);
    Notification remoteNotification = this.createRemoteNotification(localNotification);
    assertEquals(NotificationStatus.PENDING, remoteNotification.getNotificationStatus(), "Initial notification status should be PENDING");
    final String notificationId = remoteNotification.getResId();

    this.notificationClientService.setProcessedStatus(notificationId);
    remoteNotification = this.notificationClientService.findOne(notificationId);
    assertEquals(NotificationStatus.APPROVED, remoteNotification.getNotificationStatus(), "Notification status should be approved");

    final Notification localNotification2 = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, notifiedOrgUnit);

    Notification remoteNotification2 = this.createRemoteNotification(localNotification2);
    this.notificationClientService.setRefusedStatus(remoteNotification2.getResId(), REFUSAL_REASON);
    remoteNotification2 = this.notificationClientService.findOne(remoteNotification2.getResId());
    assertEquals(NotificationStatus.REFUSED, remoteNotification2.getNotificationStatus(), "Notification status should be refused");
    assertEquals(REFUSAL_REASON, remoteNotification2.getResponseMessage());
  }

  @Test
  void cannotRefuseWithoutReasonTest() {
    final OrganizationalUnit notifiedOrgUnit = this.createOrgUnitForCurrentUser(Role.MANAGER.getResId());
    final Notification localNotification = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, notifiedOrgUnit);
    Notification remoteNotification = this.createRemoteNotification(localNotification);
    assertEquals(NotificationStatus.PENDING, remoteNotification.getNotificationStatus(), "Initial notification status should be PENDING");
    final String notificationId = remoteNotification.getResId();

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
            () -> this.notificationClientService.setRefusedStatus(notificationId, ""));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    this.assertFieldContainsValidationErrorMessage(e, "responseMessage",
            "An explanatory message is mandatory to refuse the access to an organizational unit");
  }

  @Test
  void canChangeAnotherUserNotification() {
    OrganizationalUnit notifiedOrgUnit = this.orgUnitITService.getOrgUnitForNoOne(PersistenceMode.PERMANENT);

    this.restClientTool.sudoRoot();
    final Notification localNotification = this.createLocalNotification(NotificationType.VALIDATE_DEPOSIT_REQUEST, notifiedOrgUnit);
    Notification remoteNotification = this.createRemoteNotification(localNotification);
    assertEquals(NotificationStatus.PENDING, remoteNotification.getNotificationStatus(), "Initial notification status should be PENDING");
    this.restClientTool.exitSudo();

    remoteNotification = this.notificationClientService.setRefusedStatus(remoteNotification.getResId(), REFUSAL_REASON);
    assertEquals(NotificationStatus.REFUSED, remoteNotification.getNotificationStatus());
    assertEquals(REFUSAL_REASON, remoteNotification.getResponseMessage());
  }

  @Test
  void cannotCreateMultiplesNotificationsAccessDatasetRequestType() {
    this.checkUnableToCreateMultiplesNotificationsForSameUser(NotificationType.ACCESS_DATASET_REQUEST);
  }

  @Test
  void cannotCreateMultiplesNotificationsJoinOrgUnitType() {
    this.checkUnableToCreateMultiplesNotificationsForSameUser(NotificationType.JOIN_ORGUNIT_REQUEST);
  }

  @Test
  void findAllTest() {
    final OrganizationalUnit notifiedOrgUnit = this.createOrgUnitForCurrentUser(Role.APPROVER.getResId());
    final Notification localNotification = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, notifiedOrgUnit);
    final Notification remoteNotification = this.createRemoteNotification(localNotification);
    List<Notification> notificationsList = this.notificationClientService.findAll();
    assertFalse(notificationsList.isEmpty());
    assertTrue(notificationsList.stream().anyMatch(notification -> notification.getResId().equals(remoteNotification.getResId())));
  }

  @Test
  void filterListTest() {
    String message = "filter me !";
    final OrganizationalUnit notifiedOrgUnit = this.createOrgUnitForCurrentUser(Role.APPROVER.getResId());
    Notification localNotification = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, notifiedOrgUnit, message);
    this.createRemoteNotification(localNotification);
    localNotification = this.createLocalNotification(NotificationType.ACCESS_DATASET_REQUEST, notifiedOrgUnit);
    this.createRemoteNotification(localNotification);

    List<Notification> notificationsList = this.notificationClientService.searchByProperties(Map.of("message", message));
    assertEquals(1, notificationsList.size());
  }

  @Test
  void creationTest() {
    final OrganizationalUnit notifiedOrgUnit = this.createOrgUnitForCurrentUser(Role.APPROVER.getResId());
    final Notification localNotification = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, notifiedOrgUnit);
    final Notification remoteNotification = this.createRemoteNotification(localNotification);
    assertEquals(localNotification.getEmitter().getFirstName(), remoteNotification.getEmitter().getFirstName());
    assertEquals(localNotification.getEmitter().getLastName(), remoteNotification.getEmitter().getLastName());
    assertEquals(localNotification.getNotifiedOrgUnit().getName(), remoteNotification.getNotifiedOrgUnit().getName());
    assertEquals(localNotification.getMessage(), remoteNotification.getMessage());
  }

  @Test
  void creationNotificationWithoutOrgUnitTest() {
    try {
      this.createRemoteNotificationWithoutOrgUnit(NotificationType.JOIN_ORGUNIT_REQUEST, null);
      fail("Should raise exception 400 bad request");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }

    // creating a notification without org unit doesn't fail for type CREATE_ORGUNIT_REQUEST (but needs a name in objectId)
    this.createRemoteNotificationWithoutOrgUnit(NotificationType.CREATE_ORGUNIT_REQUEST,
            DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL + " New org unit name");
  }

  @Test
  void deletionTest() {
    final OrganizationalUnit notifiedOrgUnit = this.createOrgUnitForCurrentUser(Role.APPROVER.getResId());
    final Notification localNotification = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, notifiedOrgUnit);
    final Notification remoteNotification = this.createRemoteNotification(localNotification);

    List<Notification> notificationsList = this.notificationClientService.findAll();
    assertTrue(notificationsList.stream().anyMatch(notification -> notification.getResId().equals(remoteNotification.getResId())));

    this.notificationClientService.delete(remoteNotification.getResId());

    notificationsList = this.notificationClientService.findAll();
    assertFalse(notificationsList.stream().anyMatch(notification -> notification.getResId().equals(remoteNotification.getResId())));

    assertThrows(HttpClientErrorException.NotFound.class, () -> this.notificationClientService.findOne(remoteNotification.getResId()));
  }

  @Test
  void inboxTest() {
    final OrganizationalUnit managerOrgUnit = this.createOrgUnitForCurrentUser(Role.MANAGER_ID);
    final OrganizationalUnit stewardOrgUnit = this.createOrgUnitForCurrentUser(Role.STEWARD_ID);
    final OrganizationalUnit approverOrgUnit = this.createOrgUnitForCurrentUser(Role.APPROVER_ID);
    final OrganizationalUnit creatorOrgUnit = this.createOrgUnitForCurrentUser(Role.CREATOR_ID);
    final OrganizationalUnit orgUnitForNoOne = this.orgUnitITService.getOrgUnitForNoOne(PersistenceMode.PERMANENT);

    // As an ADMIN, all notifications are visible in the inbox

    this.inboxTest(NotificationType.JOIN_ORGUNIT_REQUEST, managerOrgUnit, true);
    this.inboxTest(NotificationType.JOIN_ORGUNIT_REQUEST, stewardOrgUnit, true);

    this.inboxTest(NotificationType.ACCESS_DATASET_REQUEST, stewardOrgUnit, true);

    this.inboxTest(NotificationType.VALIDATE_DEPOSIT_REQUEST, approverOrgUnit, true);
    this.inboxTest(NotificationType.VALIDATE_DEPOSIT_REQUEST, creatorOrgUnit, true);

    // This message is seen because the current user is a ApplicationRole ADMIN
    this.inboxTest(NotificationType.CREATE_ORGUNIT_REQUEST, orgUnitForNoOne, DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL + " New org unit name",
            true);
  }

  @Test
  void sentBoxTest() {
    final OrganizationalUnit notifiedOrgUnit = this.createOrgUnitForCurrentUser(Role.APPROVER.getResId());
    final Notification localNotification = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, notifiedOrgUnit);
    final Notification remoteNotification = this.createRemoteNotification(localNotification);
    final Notification sentNotification = this.notificationClientService.getSentNotification(remoteNotification.getResId());
    assertNotNull(sentNotification, "Sent notification should be in sent box");
  }

  @Test
  void cannotRefuseInformationNotificationTest() {
    final OrganizationalUnit notifiedOrgUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN, Role.MANAGER_ID, AuthApplicationRole.USER_ID, false);
    final Notification localNotification = this.createLocalNotification(NotificationType.IN_ERROR_AIP_INFO, notifiedOrgUnit, "", "", null);
    final Notification remoteNotification = this.createRemoteNotification(localNotification);
    final String remoteNotificationId = remoteNotification.getResId();
    assertEquals(NotificationStatus.NON_APPLICABLE, remoteNotification.getNotificationStatus(),
            "Initial notification status should be NON_APPLICABLE");
    try {
      this.notificationClientService.setRefusedStatus(remoteNotificationId, REFUSAL_REASON);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      assertNotNull(e.getMessage());
      assertTrue(e.getMessage().contains("cannot be refused"));
    }
  }

  @Test
  void approvedNotificationWhenApprovingDeposit() {
    List<Notification> notificationList = this.notificationClientService.getInboxNotifications(NotificationType.VALIDATE_DEPOSIT_REQUEST);
    int countValidatedDepositRequest = notificationList.size();

    final Deposit deposit = this.depositITService.createRemoteDepositWithContributorsAndSubmissionApprove(PersistenceMode.TEMPORARY,
            "Deposit notification", DEFAULT_DESCRIPTION);
    final DepositDataFile depositDataFile = this.depositITService.createDataFile(deposit);
    this.depositITService.addDataFileToDeposit(deposit.getResId(), depositDataFile);

    this.depositITService.submitDeposit(deposit);

    // Wait until the notification has been created
    SolidifyTime.waitInSeconds(5);

    // Check number of notifications of type: validate_deposit_request has been increased
    notificationList = this.notificationClientService.getInboxNotifications(NotificationType.VALIDATE_DEPOSIT_REQUEST);
    assertEquals(countValidatedDepositRequest + 1, notificationList.size());
    assertTrue(notificationList.stream().anyMatch(n -> deposit.getResId().equals(n.getObjectId())));

    this.depositITService.approveDeposit(deposit);

    // Check that the notification has been change the status
    notificationList = this.notificationClientService.getInboxNotifications(NotificationType.VALIDATE_DEPOSIT_REQUEST);
    Optional<Notification> notificationFromDepositOpt = notificationList.stream().filter(n -> deposit.getResId().equals(n.getObjectId()))
            .findFirst();
    assertTrue(notificationFromDepositOpt.isPresent());
    assertEquals(NotificationStatus.APPROVED, notificationFromDepositOpt.get().getNotificationStatus());
  }

  @Test
  void rejectNotificationWhenRejectingDeposit() {
    List<Notification> notificationList = this.notificationClientService.getInboxNotifications(NotificationType.VALIDATE_DEPOSIT_REQUEST);
    int countValidatedDepositRequest = notificationList.size();

    final Deposit deposit = this.depositITService.createRemoteDepositWithContributorsAndSubmissionApprove(PersistenceMode.TEMPORARY,
            "Deposit notification", DEFAULT_DESCRIPTION);
    final DepositDataFile depositDataFile = this.depositITService.createDataFile(deposit);
    this.depositITService.addDataFileToDeposit(deposit.getResId(), depositDataFile);

    this.depositITService.submitDeposit(deposit);

    // Wait until the notification has been created
    SolidifyTime.waitInSeconds(5);

    // Check number of notifications of type: validate_deposit_request has been increased
    notificationList = this.notificationClientService.getInboxNotifications(NotificationType.VALIDATE_DEPOSIT_REQUEST);
    assertEquals(countValidatedDepositRequest + 1, notificationList.size());
    assertTrue(notificationList.stream().anyMatch(n -> deposit.getResId().equals(n.getObjectId())));

    this.depositITService.rejectDeposit(deposit, "Not compliant");

    // Check that the notification has been change the status
    notificationList = this.notificationClientService.getInboxNotifications(NotificationType.VALIDATE_DEPOSIT_REQUEST);
    Optional<Notification> notificationFromDepositOpt = notificationList.stream().filter(n -> deposit.getResId().equals(n.getObjectId()))
            .findFirst();
    assertTrue(notificationFromDepositOpt.isPresent());
    assertEquals(NotificationStatus.REFUSED, notificationFromDepositOpt.get().getNotificationStatus());
  }
}

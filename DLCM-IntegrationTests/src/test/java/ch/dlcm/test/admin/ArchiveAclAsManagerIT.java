/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveAclAsManagerIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.ArchiveACLClientService;
import ch.dlcm.service.admin.UserClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.OrgUnitITService;

class ArchiveAclAsManagerIT extends AbstractArchiveAclUserIT {

  private final ArchiveACLClientService archiveACLService;
  private final AIPClientService aipClientService;
  private final UserClientService userClientService;

  @Autowired
  public ArchiveAclAsManagerIT(Environment env, SudoRestClientTool restClientTool, OrgUnitITService orgUnitITService,
          ArchiveACLClientService archiveACLClientService, AIPClientService aipClientService, UserClientService userClientService) {
    super(env, restClientTool, orgUnitITService);
    this.archiveACLService = archiveACLClientService;
    this.aipClientService = aipClientService;
    this.userClientService = userClientService;
  }

  @Override
  protected String role() {
    return Role.MANAGER_ID;
  }

  @Test
  void listTest() {
    OrganizationalUnit organizationalUnit = this.getPermanentOrganizationalUnitWithCurrentRole();
    assertDoesNotThrow(() -> this.archiveACLService.searchByProperties(Map.of("organizationalUnit.resId", organizationalUnit.getResId())));
  }

  @Test
  void listNoOneFailTest() {
    OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrgUnitForNoOne(PersistenceMode.PERMANENT);
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.archiveACLService.searchByProperties(Map.of("organizationalUnit.resId", organizationalUnit.getResId())));
  }

  @Test
  void createAclOnOrgUnitWithRole() {
    OrganizationalUnit organizationalUnit = this.getPermanentOrganizationalUnitWithCurrentRole();
    final ArchivalInfoPackage aip = this.getClosedArchiveForRole(this.role(), this.inheritedRole());
    final User user = this.getUser();
    final ArchiveACL archiveAcl = new ArchiveACL();
    archiveAcl.setOrganizationalUnit(organizationalUnit);
    archiveAcl.setAipId(aip.getResId());
    archiveAcl.setUser(user);
    ArchiveACL createdArchiveAcl = null;
    try {
      createdArchiveAcl = this.archiveACLService.create(archiveAcl);
    } catch (RuntimeException e) {
      fail("Cannot create ACL : " + e);
    }
    this.restClientTool.sudoAdmin();
    this.archiveACLService.delete(createdArchiveAcl.getResId());
    this.restClientTool.exitSudo();
  }

  @Test
  void tryToCreateAclOnOrgUnitWithNoManagerNorStewardRole() {
    final OrganizationalUnit organizationalUnit = this.getPermanentOrganizationalUnitWithRole(Role.VISITOR_ID);
    final ArchivalInfoPackage aip = this.getClosedArchiveForRole(Role.VISITOR_ID, this.inheritedRole());
    final User user = this.getUser();
    final ArchiveACL archiveAcl = new ArchiveACL();
    archiveAcl.setOrganizationalUnit(organizationalUnit);
    archiveAcl.setAipId(aip.getResId());
    archiveAcl.setUser(user);
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.archiveACLService.create(archiveAcl));
  }

  private ArchivalInfoPackage getClosedArchiveForRole(String role, boolean inheritedRole) {
    final String inheritedString = inheritedRole ? " inherited " : " ";
    final String archiveName = "[Permanent Test Data] Closed Deposit" + inheritedString + role;
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", archiveName));
    if (aipList.size() != 1) {
      throw new SolidifyRuntimeException("Exactly one archive for [" + archiveName + " ] should be present");
    }
    return aipList.get(0);
  }

  private User getUser() {
    final String userLastName = "[Permanent Test Data] USER_lastName";
    final List<User> userList = this.userClientService.searchByProperties(Map.of("lastName", userLastName));
    if (userList.size() != 1) {
      throw new SolidifyRuntimeException("Exactly one user for [" + userLastName + " ] should be present");
    }
    return userList.get(0);
  }
}

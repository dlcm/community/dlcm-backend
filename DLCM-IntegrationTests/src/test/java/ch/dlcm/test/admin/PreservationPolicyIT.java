/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - PreservationPolicyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.service.admin.PreservationPolicyClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

class PreservationPolicyIT extends AbstractIT {

  private final PreservationPolicyClientService preservationPolicyClientService;

  @Autowired
  public PreservationPolicyIT(Environment env, SudoRestClientTool restClientTool,
          PreservationPolicyClientService preservationPolicyClientService) {
    super(env, restClientTool);
    this.preservationPolicyClientService = preservationPolicyClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create a policy
    final PreservationPolicy presPolicy1 = new PreservationPolicy();
    presPolicy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Keep It for "));
    presPolicy1.setRetention(10 * DLCMConstants.DAYS_BY_YEAR);
    final PreservationPolicy presPolicy2 = this.preservationPolicyClientService.create(presPolicy1);

    // Test the creation
    assertEquals(presPolicy1.getName(), presPolicy2.getName());
    assertEquals(presPolicy1.getRetention(), presPolicy2.getRetention());
  }

  @Test
  void updateTest() {
    // Create a policy
    PreservationPolicy presPolicy1 = new PreservationPolicy();
    presPolicy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("A"));
    presPolicy1.setRetention(10 * DLCMConstants.DAYS_BY_YEAR);
    presPolicy1 = this.preservationPolicyClientService.create(presPolicy1);

    // Update the policy
    presPolicy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("B"));
    presPolicy1.setRetention(15 * DLCMConstants.DAYS_BY_YEAR);
    this.preservationPolicyClientService.update(presPolicy1.getResId(), presPolicy1);
    final PreservationPolicy presPolicy2 = this.preservationPolicyClientService.findOne(presPolicy1.getResId());

    // Test the update
    assertEquals(presPolicy1.getName(), presPolicy2.getName());
    assertEquals(presPolicy1.getRetention(), presPolicy2.getRetention());
    this.assertEqualsWithoutNanoSeconds(presPolicy1.getCreationTime(), presPolicy2.getCreationTime());
  }

  protected void clearPreservationPolicyFixtures() {
    final List<PreservationPolicy> ppList = this.preservationPolicyClientService.findAll();

    for (final PreservationPolicy pp : ppList) {
      if (pp.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.preservationPolicyClientService.delete(pp.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearPreservationPolicyFixtures();
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractArchiveAclUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.core.env.Environment;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.OrgUnitITService;

abstract class AbstractArchiveAclUserIT extends AbstractIT {

  protected OrgUnitITService orgUnitITService;

  protected AbstractArchiveAclUserIT(Environment env, SudoRestClientTool restClientTool, OrgUnitITService orgUnitITService) {
    super(env, restClientTool);
    this.orgUnitITService = orgUnitITService;
  }

  protected abstract String role();

  protected boolean inheritedRole() {
    return false;
  }

  @BeforeAll
  void setUp() {
    this.getPermanentOrganizationalUnitWithCurrentRole();
  }

  protected OrganizationalUnit getPermanentOrganizationalUnitWithCurrentRole() {
    return this.getPermanentOrganizationalUnitWithRole(this.role());
  }

  protected OrganizationalUnit getPermanentOrganizationalUnitWithRole(String role) {
    this.restClientTool.sudoAdmin();
    OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN, role, AuthApplicationRole.USER_ID, this.inheritedRole());
    this.restClientTool.exitSudo();
    return organizationalUnit;
  }

  @Override
  protected void deleteFixtures() {
  }
}

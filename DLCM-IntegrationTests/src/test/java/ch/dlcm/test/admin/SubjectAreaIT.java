/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - SubjectAreaIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.SubjectArea;
import ch.dlcm.service.admin.SubjectAreaClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

class SubjectAreaIT extends AbstractIT {

  private final SubjectAreaClientService subjectAreaClientService;

  @Autowired
  public SubjectAreaIT(Environment env, SudoRestClientTool restClientTool, SubjectAreaClientService subjectAreaClientService) {
    super(env, restClientTool);
    this.subjectAreaClientService = subjectAreaClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create a research domain
    final SubjectArea rd1 = this.createSubjectArea("test", "Z0000", "research domain ");
    final SubjectArea rd2 = this.subjectAreaClientService.create(rd1);

    // Test the creation
    assertEquals(rd1.getName(), rd2.getName());
    assertEquals(rd1.getCode(), rd2.getCode());
  }

  @Test
  void deleteTest() {
    // Create a new research domain
    SubjectArea rd1 = this.createSubjectArea("test", "Z0900", "research domain ");
    rd1 = this.subjectAreaClientService.create(rd1);

    // Delete
    final String resId = rd1.getResId();
    this.subjectAreaClientService.delete(resId);
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.subjectAreaClientService.findOne(resId));
  }

  @Test
  void searchTest() {
    // Create a research domain
    for (int i = 0; i < 10; i++) {
      this.subjectAreaClientService.create(this.createSubjectArea("test", "Z030" + i, "research domain "));
      if (i % 2 == 0) {
        this.subjectAreaClientService.create(this.createSubjectArea("test", "Z040" + i, "research domain "));
      }
    }

    List<SubjectArea> list1 = this.subjectAreaClientService.searchByProperties(Map.of("code", "Z03"));
    assertEquals(10, list1.size(), "Category search (long list)");

    list1 = this.subjectAreaClientService.searchByProperties(Map.of("code", "Z04"));
    assertEquals(5, list1.size(), "Category search (short list)");

    list1 = this.subjectAreaClientService.searchByProperties(Map.of("code", "Z0301"));
    assertEquals(1, list1.size(), "Research domain search");
  }

  @Test
  void unicityTest() {
    // Create a research domain
    SubjectArea rd1 = this.createSubjectArea("test", "Z0200", "research domain ");
    this.subjectAreaClientService.create(rd1);

    // Create a research domain
    SubjectArea rd2 = this.createSubjectArea("test", "Z0200", "research domain ");

    // Testing the unicity
    try {
      this.subjectAreaClientService.create(rd2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }
  }

  @Test
  void updateTest() {
    // Create a research domain
    SubjectArea rd1 = this.createSubjectArea("test", "Z0100", "A ");
    rd1 = this.subjectAreaClientService.create(rd1);

    // Update the research domain
    rd1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("B"));
    this.subjectAreaClientService.update(rd1.getResId(), rd1);
    final SubjectArea rd2 = this.subjectAreaClientService.findOne(rd1.getResId());

    // Test the update
    assertEquals(rd1.getName(), rd2.getName());
    assertEquals(rd1.getCode(), rd2.getCode());
    this.assertEqualsWithoutNanoSeconds(rd1.getCreationTime(), rd2.getCreationTime());
  }

  protected void clearSubjectAreaFixtures() {
    final List<SubjectArea> rdList = this.subjectAreaClientService.findAll();

    for (final SubjectArea rd : rdList) {
      if (rd.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.subjectAreaClientService.delete(rd.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearSubjectAreaFixtures();
  }

  private SubjectArea createSubjectArea(String source, String code, String name) {
    final SubjectArea rd = new SubjectArea();
    rd.setSource(source);
    rd.setCode(code);
    rd.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel(name));
    return rd;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DisseminationPolicyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.service.admin.DisseminationPolicyClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

class DisseminationPolicyIT extends AbstractIT {

  private final DisseminationPolicyClientService disseminationPolicyClientService;

  @Autowired
  public DisseminationPolicyIT(Environment env, SudoRestClientTool restClientTool,
          DisseminationPolicyClientService disseminationPolicyClientService) {
    super(env, restClientTool);
    this.disseminationPolicyClientService = disseminationPolicyClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void creationTestWithMap() {
    // Create a policy
    final DisseminationPolicy policy1 = new DisseminationPolicy();
    policy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("dissemination policy "));
    policy1.setType(DisseminationPolicy.DisseminationPolicyType.IIIF);
    policy1.setDownloadFileName(DisseminationPolicy.DownloadFileName.ARCHIVE_NAME);
    Map<String, String> params = getParamsAsMap();
    policy1.setParametersMap(params);

    final DisseminationPolicy policy2 = this.disseminationPolicyClientService.create(policy1);

    // Test the creation
    assertEquals(policy1.getName(), policy2.getName());
    assertEquals(policy1.getType(), policy2.getType());
    assertEquals(policy1.getParameters(), policy2.getParameters());
  }

  @Test
  void creationTestWithJson() throws JsonProcessingException {
    // Create a policy
    final DisseminationPolicy policy1 = new DisseminationPolicy();
    policy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("dissemination policy "));
    policy1.setType(DisseminationPolicy.DisseminationPolicyType.IIIF);
    policy1.setDownloadFileName(DisseminationPolicy.DownloadFileName.ARCHIVE_NAME);

    String params = this.getParamsAsJson();
    policy1.setParameters(params);

    final DisseminationPolicy policy2 = this.disseminationPolicyClientService.create(policy1);

    // Test the creation
    assertEquals(policy1.getName(), policy2.getName());
    assertEquals(policy1.getType(), policy2.getType());
    assertEquals(policy1.getParameters(), policy2.getParameters());
  }

  @Test
  void updateTestWithMap() {
    // Create a policy
    DisseminationPolicy policy1 = new DisseminationPolicy();
    policy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("A"));
    policy1.setType(DisseminationPolicy.DisseminationPolicyType.IIIF);
    policy1.setDownloadFileName(DisseminationPolicy.DownloadFileName.ARCHIVE_NAME);
    Map<String, String> params1 = getParamsAsMap();
    policy1.setParametersMap(params1);

    policy1 = this.disseminationPolicyClientService.create(policy1);

    // Update the policy
    policy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("B"));
    policy1.setType(DisseminationPolicy.DisseminationPolicyType.IIIF);
    Map<String, String> params2 = getParamsAsMap();
    params2.put("contextPath", "/iiif/V3");
    policy1.setParametersMap(params2);

    this.disseminationPolicyClientService.update(policy1.getResId(), policy1);

    final DisseminationPolicy policy2 = this.disseminationPolicyClientService.findOne(policy1.getResId());

    // Test the update
    assertEquals(policy1.getName(), policy2.getName());
    assertEquals(policy1.getType(), policy2.getType());
    assertEquals(policy1.getParameters(), policy2.getParameters());
    this.assertEqualsWithoutNanoSeconds(policy1.getCreationTime(), policy2.getCreationTime());
  }

  @Test
  void updateTestWithJson() throws JsonProcessingException {
    // Create a policy
    DisseminationPolicy policy1 = new DisseminationPolicy();
    policy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("A"));
    policy1.setType(DisseminationPolicy.DisseminationPolicyType.IIIF);
    policy1.setDownloadFileName(DisseminationPolicy.DownloadFileName.ARCHIVE_NAME);

    String params1 = this.getParamsAsJson();
    policy1.setParameters(params1);

    policy1 = this.disseminationPolicyClientService.create(policy1);

    // Update the policy
    policy1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("B"));
    policy1.setType(DisseminationPolicy.DisseminationPolicyType.IIIF);
    String params2 = getUpdateParamsAsJson();
    policy1.setParameters(params2);

    this.disseminationPolicyClientService.update(policy1.getResId(), policy1);

    final DisseminationPolicy policy2 = this.disseminationPolicyClientService.findOne(policy1.getResId());

    // Test the update
    assertEquals(policy1.getName(), policy2.getName());
    assertEquals(policy1.getType(), policy2.getType());
    assertEquals(policy1.getParameters(), policy2.getParameters());
    this.assertEqualsWithoutNanoSeconds(policy1.getCreationTime(), policy2.getCreationTime());
  }

  protected void clearDisseminationPolicyFixtures() {
    final List<DisseminationPolicy> dpList = this.disseminationPolicyClientService.findAll();

    for (final DisseminationPolicy dp : dpList) {
      if (dp.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.disseminationPolicyClientService.delete(dp.getResId());
      }
    }
  }

  private Map<String, String> getParamsAsMap() {
    final Map<String, String> params = new HashMap<>();
    params.put("hostname", "http://unige.ch");
    params.put("contextPath", "/iiif");

    return params;
  }

  private String getParamsAsJson() throws JsonProcessingException {
    final Map<String, String> params = this.getParamsAsMap();
    ObjectMapper objectMapper = new ObjectMapper();

    return objectMapper.writeValueAsString(params);
  }

  private String getUpdateParamsAsJson() throws JsonProcessingException {
    final Map<String, String> params = this.getParamsAsMap();
    ObjectMapper objectMapper = new ObjectMapper();

    return objectMapper.writeValueAsString(params);
  }

  @Override
  protected void deleteFixtures() {
    this.clearDisseminationPolicyFixtures();
  }

}

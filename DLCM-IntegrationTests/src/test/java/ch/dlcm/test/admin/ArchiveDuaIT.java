/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - LicenseIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static ch.dlcm.test.DLCMTestConstants.SAMPLE_DUA_FILE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.index.ArchivePublicData;
import ch.dlcm.model.index.ArchivePublicData.PublicDataFileType;
import ch.dlcm.service.datamgmt.ArchivePublicDataClientService;
import ch.dlcm.test.AbstractIT;

class ArchiveDuaIT extends AbstractIT {

  private final ArchivePublicDataClientService archivePublicDataClientService;

  @Autowired
  public ArchiveDuaIT(Environment env, SudoRestClientTool restClientTool, ArchivePublicDataClientService archivePublicDataClientService) {
    super(env, restClientTool);
    this.archivePublicDataClientService = archivePublicDataClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Override
  protected void deleteFixtures() {

  }

  @Test
  void createAndDeleteTest() throws IOException {

    // Create an Archive DUA with a file
    final ArchivePublicData archiveDua = new ArchivePublicData();
    archiveDua.setAipId("testId");
    archiveDua.setDataFileType(PublicDataFileType.ARCHIVE_DUA);
    final Resource archiveDuaFile = new ClassPathResource(SAMPLE_DUA_FILE);
    final ArchivePublicData createdArchiveDua = this.archivePublicDataClientService.create(archiveDua);
    this.archivePublicDataClientService.uploadArchiveDuaFile(createdArchiveDua.getResId(), archiveDuaFile, MediaType.APPLICATION_PDF_VALUE);
    final ArchivePublicData retrievedArchiveDua = this.archivePublicDataClientService.findOne(createdArchiveDua.getResId());

    // Test creation
    assertEquals(SAMPLE_DUA_FILE, retrievedArchiveDua.getDataFile().getFileName());
    assertEquals(MediaType.APPLICATION_PDF_VALUE, retrievedArchiveDua.getDataFile().getMimeType());
    assertEquals(archiveDuaFile.contentLength(), retrievedArchiveDua.getDataFile().getFileSize());

    // Delete the archive DUA
    this.archivePublicDataClientService.delete(retrievedArchiveDua.getResId());

    // Test deletion
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.archivePublicDataClientService.findOne(createdArchiveDua.getResId()));

  }

}

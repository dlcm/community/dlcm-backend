/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrganizationalUnitManagerIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.FundingAgencyITService;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.OrgUnitITService;

@Deprecated
class OrganizationalUnitManagerIT extends AbstractIT {

  private OrganizationalUnitClientService orgUnitClientService;
  private OrgUnitITService orgUnitITService;
  private InstitutionITService institutionITService;
  private FundingAgencyITService fundingAgencyITService;

  @Autowired
  public OrganizationalUnitManagerIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrganizationalUnitClientService orgUnitClientService,
          OrgUnitITService orgUnitITService,
          InstitutionITService institutionITService,
          FundingAgencyITService fundingAgencyITService) {
    super(env, restClientTool);
    this.orgUnitClientService = orgUnitClientService;
    this.orgUnitITService = orgUnitITService;
    this.institutionITService = institutionITService;
    this.fundingAgencyITService = fundingAgencyITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Test
  public void creationOrgUnitAndAddInstitutionsTest() {
    /*
     * Get test orgUnit
     */
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getTestOrganizationalUnitAsUser(PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN, Role.MANAGER_ID, false, false);
    List<Institution> list = this.orgUnitClientService.getInstitutions(organizationalUnit.getResId());
    final int initialState = list.size();

    /*
     * Create a new Institution
     */
    this.restClientTool.sudoAdmin();
    Institution institution = this.institutionITService.createRemoteInstitution("Institution");
    this.restClientTool.exitSudo();

    /*
     * Add the Institution to the OrganizationalUnit
     */
    this.orgUnitClientService.addInstitution(organizationalUnit.getResId(), institution.getResId());
    list = this.orgUnitClientService.getInstitutions(organizationalUnit.getResId());
    assertEquals(initialState + 1, list.size());
    final String institutionName = institution.getName();
    final Optional<Institution> optionalInstitution = list.stream().filter(ins -> ins.getName().equals(institutionName))
            .findFirst();
    assertTrue(optionalInstitution.isPresent());

    /*
     * Check it has been added
     */
    assertEquals(institution.getResId(), optionalInstitution.get().getResId());
    assertEquals(institution.getName(), optionalInstitution.get().getName());
    assertEquals(institution.getDescription(), optionalInstitution.get().getDescription());

    /*
     * Remove the Institution
     */
    this.orgUnitClientService.removeInstitution(organizationalUnit.getResId(), institution.getResId());
    /*
     * Check it has been removed
     */
    list = this.orgUnitClientService.getInstitutions(organizationalUnit.getResId());
    assertEquals(initialState, list.size());

    /*
     * Create 3 Institutions and link them to the OrganizationalUnit
     */
    List<String> institutions = Arrays.asList("Institution Sub 1", "Institution Sub 2", "Institution Sub 3");
    String institutionsToDelete = null;

    int i = 0;
    for (String ins : institutions) {
      this.restClientTool.sudoAdmin();
      Institution newInstitution = this.institutionITService.createRemoteInstitution(ins);
      this.restClientTool.exitSudo();
      this.orgUnitClientService.addInstitution(organizationalUnit.getResId(), newInstitution.getResId());

      if (i == 1) {
        institutionsToDelete = newInstitution.getResId();
      }

      i++;
    }

    /*
     * Check they are all linked correctly
     */
    list = this.orgUnitClientService.getInstitutions(organizationalUnit.getResId());
    assertNotNull(list);
    assertEquals(initialState + 3, list.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.orgUnitClientService.removeInstitution(organizationalUnit.getResId(), institutionsToDelete);
    list = this.orgUnitClientService.getInstitutions(organizationalUnit.getResId());
    assertNotNull(list);
    assertEquals(initialState + 2, list.size());
    for (final Institution fetched_institution : list) {
      assertNotEquals(institutionsToDelete, fetched_institution.getResId());
    }
  }

  @Test
  void addAndRemoveFundingAgency() {
    /*
     * Get test orgUnit
     */
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getTestOrganizationalUnitAsUser(PersistenceMode.PERMANENT,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN, Role.MANAGER_ID, false, false);
    List<FundingAgency> fundingAgencies = this.orgUnitClientService.getFundingAgencies(organizationalUnit.getResId());
    final int initialState = fundingAgencies.size();

    /*
     * Create a new FundingAgency
     */
    this.restClientTool.sudoAdmin();
    FundingAgency new_agency = this.fundingAgencyITService.createRemoteFundingAgency("FA", "My FundingAgency");
    this.restClientTool.exitSudo();

    /*
     * Add the FundingAgency to the OrganizationalUnit
     */
    this.orgUnitClientService.addFundingAgency(organizationalUnit.getResId(), new_agency.getResId());

    /*
     * Check it has been added
     */
    fundingAgencies = this.orgUnitClientService.getFundingAgencies(organizationalUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 1, fundingAgencies.size());
    final Optional<FundingAgency> optionalFA = fundingAgencies.stream().filter(fa -> fa.getName().equals(new_agency.getName())).findFirst();
    assertTrue(optionalFA.isPresent());
    assertEquals(new_agency.getName(), optionalFA.get().getName());
    assertEquals(new_agency.getAcronym(), optionalFA.get().getAcronym());

    /*
     * Remove the FundingAgency
     */
    this.orgUnitClientService.removeFundingAgency(organizationalUnit.getResId(), new_agency.getResId());

    /*
     * Check it has been removed
     */
    fundingAgencies = this.orgUnitClientService.getFundingAgencies(organizationalUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState, fundingAgencies.size());

    /*
     * Create 3 FundingAgencies and link them to the OrganizationalUnit
     */
    List<String> subAgencies = Arrays.asList("Sub FundingAgency 1", "Sub FundingAgency 2", "Sub FundingAgency 3");

    String agencyIdToDelete = null;

    int i = 0;
    for (String subAgencyName : subAgencies) {
      this.restClientTool.sudoAdmin();
      FundingAgency subAgency = this.fundingAgencyITService.createRemoteFundingAgency("FA" + i, subAgencyName);
      this.restClientTool.exitSudo();
      this.orgUnitClientService.addFundingAgency(organizationalUnit.getResId(), subAgency.getResId());

      if (i == 1) {
        agencyIdToDelete = subAgency.getResId();
      }

      i++;
    }

    /*
     * Check they are all linked correctly
     */
    fundingAgencies = this.orgUnitClientService.getFundingAgencies(organizationalUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 3, fundingAgencies.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.orgUnitClientService.removeFundingAgency(organizationalUnit.getResId(), agencyIdToDelete);
    fundingAgencies = this.orgUnitClientService.getFundingAgencies(organizationalUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 2, fundingAgencies.size());
    for (final FundingAgency fetched_agency : fundingAgencies) {
      assertNotEquals(agencyIdToDelete, fetched_agency.getResId());
    }

  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoAdmin();
    this.orgUnitITService.cleanTestData();
    this.institutionITService.cleanTestData();
    this.fundingAgencyITService.cleanTestData();
    this.restClientTool.exitSudo();
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - RoleIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.Role;
import ch.dlcm.service.admin.RoleClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

class RoleIT extends AbstractIT {

  private final RoleClientService roleClientService;

  @Autowired
  public RoleIT(Environment env, SudoRestClientTool restClientTool, RoleClientService roleClientService) {
    super(env, restClientTool);
    this.roleClientService = roleClientService;
  }

  private static final int TOTAL_NUMBER_OF_ROLES = 5;

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void creationTest() {
    final Role role = new Role();
    role.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Role"));

    try {
      this.roleClientService.create(role);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void deleteTest() {
    final List<Role> actualRoles = this.roleClientService.findAll();

    // Delete
    final String resId = actualRoles.get(0).getResId();
    try {
      this.roleClientService.delete(resId);
      fail("A NOT_FOUND Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void findAllTest() {
    // Find all
    final List<Role> actualRoles = this.roleClientService.findAll();

    // Test all roles
    assertEquals(TOTAL_NUMBER_OF_ROLES, actualRoles.size());
  }

  @Test
  void filterListTest() {
    List<Role> actualRoles = this.roleClientService.searchByProperties(Map.of("name", Role.APPROVER.getName()));
    assertEquals(1, actualRoles.size());
    assertTrue(actualRoles.stream().anyMatch(role -> role.getName().equals(Role.APPROVER.getName())));
  }

  @Test
  void updateTest() {
    // Get an existing role
    Role localRole = this.roleClientService.findAll().get(0);

    final String initialRoleName = localRole.getName();

    // Update the role name
    final String newRoleName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Role");
    localRole.setName(newRoleName);
    Role actualRole = this.roleClientService.update(localRole.getResId(), localRole);

    // Test the update
    assertNotNull(actualRole);
    this.assertExpectedRole(localRole, actualRole);

    // Reset default value
    localRole = actualRole;
    localRole.setName(initialRoleName);
    actualRole = this.roleClientService.update(localRole.getResId(), localRole);
    this.assertExpectedRole(localRole, actualRole);
  }

  @Test
  void tryToChangeRoleLevel() {
    Role localRole = this.roleClientService.findAll().get(0);
    Role newRole = new Role(localRole.getName(), localRole.getLevel() + 1);

    try {
      this.roleClientService.update(localRole.getResId(), newRole);
      fail("Should raise exception 400 BAD REQUEST");
    } catch (HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Override
  protected void deleteFixtures() {
  }

  private void assertExpectedRole(Role expectedRole, Role actualRole) {
    assertEquals(expectedRole.getResId(), actualRole.getResId());
    assertEquals(expectedRole.getName(), actualRole.getName());
    this.assertEqualsWithoutNanoSeconds(expectedRole.getCreationTime(), actualRole.getCreationTime());
  }
}

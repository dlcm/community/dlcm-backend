/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - FundingAgencyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.FundingAgencyClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.OrgUnitITService;

class FundingAgencyIT extends AbstractIT {

  private final FundingAgencyClientService fundingAgencyClientService;
  private final OrgUnitITService orgUnitITService;

  @Autowired
  public FundingAgencyIT(Environment env, SudoRestClientTool restClientTool, FundingAgencyClientService fundingAgencyClientService,
          OrgUnitITService orgUnitITService) {
    super(env, restClientTool);
    this.fundingAgencyClientService = fundingAgencyClientService;
    this.orgUnitITService = orgUnitITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void testAddAndRemoveOrganizationalUnit() {

    final String fundingAgencyAcronym = DLCMTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String fundingAgencyName = DLCMTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency with Unit");

    /*
     * Create a new FundingAgency
     */
    FundingAgency newAgency = new FundingAgency();
    newAgency.setAcronym(fundingAgencyAcronym);
    newAgency.setName(fundingAgencyName);
    newAgency = this.fundingAgencyClientService.create(newAgency);

    /*
     * Create a new OrganizationalUnit and check number of org units
     */
    final OrganizationalUnit newUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN);
    List<OrganizationalUnit> units = this.fundingAgencyClientService.getOrganizationalUnits(newAgency.getResId());
    final int sizeBefore = units.size();

    /*
     * Add the OrganizationalUnit to the FundingAgency
     */
    this.fundingAgencyClientService.addOrganizationalUnit(newAgency.getResId(), newUnit.getResId());

    /*
     * Check it has been added
     */

    units = this.fundingAgencyClientService.getOrganizationalUnits(newAgency.getResId());
    assertNotNull(units);
    assertEquals(sizeBefore + 1, units.size());
    final Optional<OrganizationalUnit> optionalU = units.stream().filter(orgUnit -> orgUnit.getName().equals(newUnit.getName())).findFirst();
    assertTrue(optionalU.isPresent());
    assertEquals(newUnit.getName(), optionalU.get().getName());

    /*
     * Remove the OrganizationalUnit
     */
    this.fundingAgencyClientService.removeOrganizationalUnit(newAgency.getResId(), newUnit.getResId());

    /*
     * Check it has been removed
     */
    units = this.fundingAgencyClientService.getOrganizationalUnits(newAgency.getResId());
    assertNotNull(units);
    assertEquals(sizeBefore, units.size());

    /*
     * Add two organizational unit and link them to the FundingAgency
     */
    final int beforeAddingFundingAgencies = this.fundingAgencyClientService.getOrganizationalUnits(newAgency.getResId()).size();
    this.fundingAgencyClientService.addOrganizationalUnit(newAgency.getResId(), newUnit.getResId());

    final OrganizationalUnit organizationalUnit2 = this.orgUnitITService.getSecondPermanentOrganizationalUnit();

    this.fundingAgencyClientService.addOrganizationalUnit(newAgency.getResId(), organizationalUnit2.getResId());

    /*
     * Check they are all linked correctly
     */
    units = this.fundingAgencyClientService.getOrganizationalUnits(newAgency.getResId());
    assertNotNull(units);
    assertEquals(beforeAddingFundingAgencies + 2, units.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.fundingAgencyClientService.removeOrganizationalUnit(newAgency.getResId(), organizationalUnit2.getResId());
    units = this.fundingAgencyClientService.getOrganizationalUnits(newAgency.getResId());
    assertNotNull(units);
    assertEquals(beforeAddingFundingAgencies + 1, units.size());
    for (final OrganizationalUnit fetched_u : units) {
      assertNotEquals(organizationalUnit2.getResId(), fetched_u.getResId());
    }
  }

  @Test
  void testCreate() {

    final String agencyAcronym = DLCMTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String agencyName = DLCMTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency");

    /*
     * Create a new FundingAgency
     */
    FundingAgency new_agency = new FundingAgency();
    new_agency.setAcronym(agencyAcronym);
    new_agency.setName(agencyName);
    new_agency = this.fundingAgencyClientService.create(new_agency);

    /*
     * Fetch it from server and test it has been created
     */
    final FundingAgency fetched_agency = this.fundingAgencyClientService.findOne(new_agency.getResId());
    assertNotNull(fetched_agency.getResId());
    assertEquals(agencyName, fetched_agency.getName());
  }

  @Test
  void getTestWithRorId() {
    // Create an institution
    final String agencyAcronym = DLCMTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String agencyName = DLCMTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency");

    /*
     * Create a new FundingAgency
     */
    FundingAgency new_agency = new FundingAgency();
    new_agency.setAcronym(agencyAcronym);
    new_agency.setName(agencyName);
    new_agency.setRorId("04p405e02");
    new_agency = this.fundingAgencyClientService.create(new_agency);

    /*
     * Fetch it from server and test it has been created
     */
    final FundingAgency fetched_agency = this.fundingAgencyClientService.findOne(new_agency.getResId());
    assertNotNull(fetched_agency.getResId());
    assertEquals(new_agency.getAcronym(), fetched_agency.getAcronym());
    assertEquals(new_agency.getName(), fetched_agency.getName());
    assertEquals(new_agency.getRorId(), fetched_agency.getRorId());

    // Get License by ROR ID
    final FundingAgency ror_agency = this.fundingAgencyClientService.findByRorId(new_agency.getRorId());
    // Test the get
    assertEquals(new_agency.getAcronym(), ror_agency.getAcronym());
    assertEquals(new_agency.getName(), ror_agency.getName());
    assertEquals(new_agency.getRorId(), ror_agency.getRorId());
  }

  @Test
  void testDelete() {

    final String agencyAcronym = DLCMTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String agencyName = DLCMTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency");

    /*
     * Create a new FundingAgency
     */
    FundingAgency newAgency = new FundingAgency();
    newAgency.setAcronym(agencyAcronym);
    newAgency.setName(agencyName);
    newAgency = this.fundingAgencyClientService.create(newAgency);

    /*
     * Check it has been created
     */
    final FundingAgency fetched_agency = this.fundingAgencyClientService.findOne(newAgency.getResId());
    assertNotNull(fetched_agency.getResId());

    /*
     * Delete it
     */
    this.fundingAgencyClientService.delete(fetched_agency.getResId());

    /*
     * Fetching again returns nothing
     */
    final String newAgencyId = newAgency.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.fundingAgencyClientService.findOne(newAgencyId));
  }

  @Test
  void testUnicity() {

    final String agencyAcronym = DLCMTestConstants.getRandomNameWithTemporaryLabel("FA");
    final String agencyName = DLCMTestConstants.getRandomNameWithTemporaryLabel("My FundingAgency");

    // Creating a new agency
    FundingAgency fundingAgency1 = new FundingAgency();
    fundingAgency1.setAcronym(agencyAcronym);
    fundingAgency1.setName(agencyName);
    fundingAgency1 = this.fundingAgencyClientService.create(fundingAgency1);

    // Creating a second agency with the same name and acronym as the first one
    FundingAgency fundingAgency2 = new FundingAgency();
    fundingAgency2.setAcronym(fundingAgency1.getAcronym());
    fundingAgency2.setName(fundingAgency1.getName());

    // Testing unicity of the name and acronym
    try {
      this.fundingAgencyClientService.create(fundingAgency2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }
  }

  @Test
  void testUpdate() {

    final String agencyAcronym1 = DLCMTestConstants.getRandomNameWithTemporaryLabel("FA1");
    final String agencyName1 = DLCMTestConstants.getRandomNameWithTemporaryLabel("Funding Agency unit");
    final String agencyAcronym2 = DLCMTestConstants.getRandomNameWithTemporaryLabel("FA2");
    final String agencyName2 = DLCMTestConstants.getRandomNameWithTemporaryLabel("Funding Agency updated");

    FundingAgency new_agency = new FundingAgency();
    new_agency.setAcronym(agencyAcronym1);
    new_agency.setName(agencyName1);
    new_agency = this.fundingAgencyClientService.create(new_agency);

    /*
     * Checks saving succeeded
     */
    final FundingAgency fetched_agency = this.fundingAgencyClientService.findOne(new_agency.getResId());
    assertEquals(fetched_agency.getName(), agencyName1, "names are equal");

    /*
     * Does the update
     */
    fetched_agency.setAcronym(agencyAcronym2);
    fetched_agency.setName(agencyName2);
    this.fundingAgencyClientService.update(fetched_agency.getResId(), fetched_agency);

    /*
     * Checks update succeeded
     */
    final FundingAgency refetched_agency = this.fundingAgencyClientService.findOne(fetched_agency.getResId());
    assertEquals(refetched_agency.getName(), agencyName2, "names are equal");
    assertEquals(refetched_agency.getAcronym(), agencyAcronym2, "acronyms are equal");
  }

  @Test
  void uploadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an fundingAgency
     */
    final FundingAgency fundingAgency1 = this.createRemoteFundingAgency("DLCM Funding Agency #LOGO");

    /*
     * Add logo
     */
    FundingAgency fetchedFundingAgency = this.fundingAgencyClientService.uploadLogo(fundingAgency1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final FundingAgency refetchedFundingAgency = this.fundingAgencyClientService.findOne(fetchedFundingAgency.getResId());
    assertEquals(refetchedFundingAgency.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedFundingAgency.getResourceFile().getFileSize(), sizeLogo);
  }

  @Test
  void downloadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an fundingAgency
     */
    final FundingAgency fundingAgency1 = this.createRemoteFundingAgency("DLCM Funding Agency #LOGO");

    /*
     * Add logo
     */
    FundingAgency fundingAgency2 = this.fundingAgencyClientService.uploadLogo(fundingAgency1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final FundingAgency refetchedFundingAgency = this.fundingAgencyClientService.findOne(fundingAgency2.getResId());
    assertEquals(refetchedFundingAgency.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedFundingAgency.getResourceFile().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.fundingAgencyClientService.downloadLogo(refetchedFundingAgency.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedFundingAgency.getResourceFile().getFileSize());
  }

  @Test
  void deleteLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an fundingAgency
     */
    final FundingAgency fundingAgency1 = this.createRemoteFundingAgency("DLCM Funding Agency #LOGO");

    /*
     * Add logo
     */
    FundingAgency fetchedFundingAgency = this.fundingAgencyClientService.uploadLogo(fundingAgency1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    FundingAgency refetchedFundingAgency = this.fundingAgencyClientService.findOne(fetchedFundingAgency.getResId());
    assertEquals(refetchedFundingAgency.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedFundingAgency.getResourceFile().getFileSize(), sizeLogo);

    this.fundingAgencyClientService.deleteLogo(fundingAgency1.getResId());
    refetchedFundingAgency = this.fundingAgencyClientService.findOne(fetchedFundingAgency.getResId());
    assertNull(refetchedFundingAgency.getResourceFile());
  }

  /*************************************************/

  protected void clearFundingAgencyFixtures() {
    final List<FundingAgency> agencies = this.fundingAgencyClientService.findAll();

    for (final FundingAgency a : agencies) {

      if (a.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {

        /*
         * Remove eventual links with OrganizationalUnits first
         */
        final List<OrganizationalUnit> child_units = this.fundingAgencyClientService.getOrganizationalUnits(a.getResId());
        if (child_units != null) {
          for (final OrganizationalUnit u : child_units) {
            this.fundingAgencyClientService.removeOrganizationalUnit(a.getResId(), u.getResId());
          }
        }

        this.fundingAgencyClientService.delete(a.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearFundingAgencyFixtures();
  }

  private FundingAgency createRemoteFundingAgency(String description) {
    // Create a new FundingAgency
    FundingAgency fundingAgency = new FundingAgency();
    fundingAgency.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel(description));
    fundingAgency.setAcronym(DLCMTestConstants.getRandomNameWithTemporaryLabel(description));
    fundingAgency = this.fundingAgencyClientService.create(fundingAgency);
    return fundingAgency;
  }
}

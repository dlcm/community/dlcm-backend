/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrganizationalUnitAsVisitorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.Role;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.test.service.FundingAgencyITService;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.OrgUnitITService;

public class OrganizationalUnitAsVisitorIT extends OrganizationalUnitAsCreatorIT {

  @Autowired
  public OrganizationalUnitAsVisitorIT(Environment env, SudoRestClientTool restClientTool,
          OrganizationalUnitClientService orgUnitClientService, OrgUnitITService orgUnitITService, InstitutionITService institutionITService,
          FundingAgencyITService fundingAgencyITService) {
    super(env, restClientTool, orgUnitClientService, orgUnitITService, institutionITService, fundingAgencyITService);
  }

  @Override
  protected String role() {
    return Role.VISITOR_ID;
  }

}

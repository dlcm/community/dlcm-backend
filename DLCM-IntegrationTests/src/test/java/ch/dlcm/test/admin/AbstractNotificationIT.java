/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractNotificationIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.NotificationClientService;
import ch.dlcm.service.admin.UserClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

abstract class AbstractNotificationIT extends AbstractIT {

  protected final static String REFUSAL_REASON = "Refused in the test";

  protected final NotificationClientService notificationClientService;
  protected final UserClientService userClientService;
  protected final OrgUnitITService orgUnitITService;
  protected final PersonITService personITService;
  protected final DepositITService depositITService;

  protected AbstractNotificationIT(
          Environment env,
          SudoRestClientTool restClientTool,
          NotificationClientService notificationClientService,
          UserClientService userClientService,
          OrgUnitITService orgUnitITService,
          PersonITService personITService,
          DepositITService depositITService) {
    super(env, restClientTool);
    this.notificationClientService = notificationClientService;
    this.userClientService = userClientService;
    this.orgUnitITService = orgUnitITService;
    this.personITService = personITService;
    this.depositITService = depositITService;
  }

  private static final String DEFAULT_MESSAGE = " A message";

  Notification createLocalNotification(NotificationType notificationType, OrganizationalUnit notifiedOrgUnit) {
    return this.createLocalNotification(notificationType, notifiedOrgUnit, DEFAULT_MESSAGE);
  }

  Notification createLocalNotification(NotificationType notificationType, OrganizationalUnit notifiedOrgUnit, String message) {
    return this.createLocalNotification(notificationType, notifiedOrgUnit, message, null);
  }

  Notification createLocalNotification(NotificationType notificationType, OrganizationalUnit notifiedOrgUnit, String message, String objectId) {
    User emitter = this.userClientService.getAuthenticatedUser();
    return this.createLocalNotification(notificationType, notifiedOrgUnit, message, objectId, emitter);
  }

  Notification createLocalNotification(NotificationType notificationType, OrganizationalUnit notifiedOrgUnit, String message, String objectId,
          User emitter) {
    Notification localNotification = new Notification();
    localNotification.setEmitter(emitter);
    localNotification.setMessage(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL + " " + message);
    localNotification.setObjectId(objectId);
    localNotification.setNotificationType(notificationType);
    localNotification.setNotifiedOrgUnit(notifiedOrgUnit);
    return localNotification;
  }

  Notification createRemoteNotification(Notification localNotification) {
    return this.notificationClientService.create(localNotification);
  }

  OrganizationalUnit createOrgUnitForCurrentUser(String role) {
    final User currentUser = this.userClientService.getAuthenticatedUser();
    return this.createOrgUnitForPerson(role, currentUser.getPerson());
  }

  OrganizationalUnit createOrgUnitForPerson(String role, Person person) {
    final OrganizationalUnit remoteNotifiedOrgUnit = this.orgUnitITService.createTemporaryRemoteOrgUnit("OrgUnit", Collections.emptyList());
    this.orgUnitITService.enforcePersonHasRoleInOrgUnit(remoteNotifiedOrgUnit, person, role);
    return remoteNotifiedOrgUnit;
  }

  Notification createRemoteNotificationWithoutOrgUnit(NotificationType notificationType, String objectId) {
    final Notification localNotification = this.createLocalNotification(notificationType, null, null, objectId);
    return this.notificationClientService.create(localNotification);
  }

  void inboxTest(NotificationType notificationType, OrganizationalUnit notifiedOrgUnit, boolean shouldBeInInbox) {
    this.inboxTest(notificationType, notifiedOrgUnit, null, shouldBeInInbox);
  }

  void inboxTest(NotificationType notificationType, OrganizationalUnit notifiedOrgUnit, String objectId, boolean shouldBeInInbox) {
    this.checkNotificationInInboxOrSentRequest(notificationType, notifiedOrgUnit, objectId, shouldBeInInbox, true);
  }

  void sentRequestTest(NotificationType notificationType, OrganizationalUnit notifiedOrgUnit, String objectId, boolean shouldBeInSentRequest) {
    this.checkNotificationInInboxOrSentRequest(notificationType, notifiedOrgUnit, objectId, shouldBeInSentRequest, false);
  }

  void checkNotificationInInboxOrSentRequest(NotificationType notificationType, OrganizationalUnit notifiedOrgUnit, String objectId,
          boolean shouldBeInInboxOrSentRequest, boolean checkInbox) {
    final Notification localNotification = this.createLocalNotification(notificationType, notifiedOrgUnit, DEFAULT_MESSAGE, objectId);
    this.restClientTool.sudoAdmin();
    final Notification remoteNotification = this.createRemoteNotification(localNotification);
    this.restClientTool.exitSudo();
    Notification notificationToCheck;
    List<Notification> orgUnitNotifications;
    if (checkInbox) {
      notificationToCheck = this.notificationClientService.getInboxNotification(remoteNotification.getResId());
      orgUnitNotifications = this.notificationClientService.getInboxNotificationsByOrgUnit(notifiedOrgUnit.getResId());
    } else {
      notificationToCheck = this.notificationClientService.getSentNotification(remoteNotification.getResId());
      if (notifiedOrgUnit != null) {
        orgUnitNotifications = this.notificationClientService.getSentNotificationsByOrgUnit(notifiedOrgUnit.getResId());
      } else {
        // note: org unit is null for instance when the notification relates to the creation of an org unit
        orgUnitNotifications = this.notificationClientService.getSentNotificationByStatus(NotificationStatus.PENDING);
      }
    }
    assertEquals(shouldBeInInboxOrSentRequest, notificationToCheck != null, "Notification should be in inbox/sent request");
    if (shouldBeInInboxOrSentRequest) {
      assertFalse(orgUnitNotifications.isEmpty(), "There should be at least one notification found");
      List<String> notificationsIds = orgUnitNotifications.stream().map(Notification::getResId).toList();
      assertTrue(notificationsIds.contains(notificationToCheck.getResId()));
      assertEquals(NotificationStatus.PENDING,
              orgUnitNotifications.stream().filter(n -> n.getResId().equals(notificationToCheck.getResId())).findFirst().get()
                      .getNotificationStatus());
    }
  }

  protected void checkUnableToCreateMultiplesNotificationsForSameUser(NotificationType notificationType) {
    final Person currentPerson = this.personITService.getLinkedPersonToCurrentUser();
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.createTemporaryRemoteOrgUnit(
            "Org unit for testing creation of notifications of type for same user not already treated", new ArrayList<>());
    this.orgUnitITService.enforcePersonHasRoleInOrgUnit(organizationalUnit, currentPerson, Role.MANAGER_ID);
    this.restClientTool.exitSudo();

    final Notification localNotification = this.createLocalNotification(notificationType, organizationalUnit);
    if (NotificationType.ACCESS_DATASET_REQUEST.equals(notificationType)) {
      final int fakeArchiveId = ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE);
      localNotification.setObjectId(Integer.toString(fakeArchiveId));
    }
    Notification remoteNotification = this.createRemoteNotification(localNotification);
    assertEquals(NotificationStatus.PENDING, remoteNotification.getNotificationStatus(), "Initial notification status should be PENDING");

    final Notification secondLocalNotification = this.createLocalNotification(notificationType, organizationalUnit);
    assertThrows(HttpClientErrorException.BadRequest.class, () -> this.createRemoteNotification(secondLocalNotification));
  }

  protected void clearNotificationFixtures() {
    final List<Notification> notificationList = this.notificationClientService.findAll();

    for (final Notification notification : notificationList) {
      if (notification.getMessage().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.notificationClientService.delete(notification.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoAdmin();
    this.clearNotificationFixtures();
    this.orgUnitITService.cleanTestData();
    this.restClientTool.exitSudo();
  }
}

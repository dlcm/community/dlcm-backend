/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - SubmissionAgreementIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static ch.dlcm.test.DLCMTestConstants.SAMPLE_SUBMISSION_AGREEMENT_FILE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.service.admin.SubmissionAgreementClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.SubmissionAgreementITService;

class SubmissionAgreementIT extends AbstractIT {
  private final SubmissionAgreementClientService submissionAgreementClientService;
  private final SubmissionAgreementITService submissionAgreementITService;

  @Autowired
  public SubmissionAgreementIT(Environment env, SudoRestClientTool restClientTool,
          SubmissionAgreementClientService submissionAgreementClientService,
          SubmissionAgreementITService submissionAgreementITService) {
    super(env, restClientTool);
    this.submissionAgreementClientService = submissionAgreementClientService;
    this.submissionAgreementITService = submissionAgreementITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void uploadSubmissionAgreementTest() throws IOException {
    // Create submission agreement with file
    final Resource submissionAgreementFile = new ClassPathResource(SAMPLE_SUBMISSION_AGREEMENT_FILE);
    final SubmissionAgreement submissionAgreement = this.submissionAgreementITService.createSubmissionAgreementWithFile(submissionAgreementFile,
            MediaType.APPLICATION_PDF_VALUE);

    // Test presence of submission agreement file
    final SubmissionAgreement refetchSubmissionAgreement = this.submissionAgreementClientService.findOne(submissionAgreement.getResId());
    assertEquals(SAMPLE_SUBMISSION_AGREEMENT_FILE, refetchSubmissionAgreement.getResourceFile().getFileName());
    assertEquals(submissionAgreementFile.contentLength(), refetchSubmissionAgreement.getResourceFile().getFileSize());
    assertEquals(MediaType.APPLICATION_PDF_VALUE, refetchSubmissionAgreement.getResourceFile().getMimeType());
    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.submissionAgreementClientService.downloadSubmissionAgreementFile(refetchSubmissionAgreement.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchSubmissionAgreement.getResourceFile().getFileSize());
  }

  @Test
  void deleteSubmissionAgreementFileTest() throws IOException {
    // Create submission agreement with file
    final Resource submissionAgreementFile = new ClassPathResource(SAMPLE_SUBMISSION_AGREEMENT_FILE);
    final SubmissionAgreement submissionAgreement = this.submissionAgreementITService.createSubmissionAgreementWithFile(submissionAgreementFile);

    // Test presence of submission agreement file
    SubmissionAgreement refetchSubmissionAgreement = this.submissionAgreementClientService.findOne(submissionAgreement.getResId());
    assertEquals(SAMPLE_SUBMISSION_AGREEMENT_FILE, refetchSubmissionAgreement.getResourceFile().getFileName());
    assertEquals(submissionAgreementFile.contentLength(), refetchSubmissionAgreement.getResourceFile().getFileSize());

    // Deleting submission agreement file should return 400 BAD_REQUEST
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.submissionAgreementClientService.deleteFile(refetchSubmissionAgreement.getResId()));
  }

  @Test
  void deleteTest() {
    // Create a new submission agreement
    SubmissionAgreement expectedSubmissionAgreement = new SubmissionAgreement();
    final Resource submissionAgreementFile = new ClassPathResource(SAMPLE_SUBMISSION_AGREEMENT_FILE);
    expectedSubmissionAgreement.setTitle(DLCMTestConstants.getRandomNameWithTemporaryLabel("Deposit "));
    expectedSubmissionAgreement.setVersion(DLCMTestConstants.getRandomNameWithTemporaryLabel("1"));
    expectedSubmissionAgreement = this.submissionAgreementClientService.createSubmissionAgreementWithFile(expectedSubmissionAgreement,
            submissionAgreementFile);

    // Delete
    final String resId = expectedSubmissionAgreement.getResId();
    this.submissionAgreementClientService.delete(resId);
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.submissionAgreementClientService.findOne(resId));
  }

  @Test
  void findAllTest() {
    // Create a new submission agreement
    final SubmissionAgreement expectedSubmissionAgreement = new SubmissionAgreement();
    final Resource submissionAgreementFile = new ClassPathResource(SAMPLE_SUBMISSION_AGREEMENT_FILE);
    expectedSubmissionAgreement.setTitle(DLCMTestConstants.getRandomNameWithTemporaryLabel("Deposit "));
    expectedSubmissionAgreement.setVersion(DLCMTestConstants.getRandomNameWithTemporaryLabel("1"));
    this.submissionAgreementClientService.createSubmissionAgreementWithFile(expectedSubmissionAgreement, submissionAgreementFile);

    // Find all
    final List<SubmissionAgreement> actualSubmissionAgreements = this.submissionAgreementClientService.findAll();

    // Test at least
    assertFalse(actualSubmissionAgreements.isEmpty());
  }

  @Test
  void updateTest() {
    // Create a submission agreement
    final SubmissionAgreement submissionAgreement = new SubmissionAgreement();
    final Resource submissionAgreementFile = new ClassPathResource(SAMPLE_SUBMISSION_AGREEMENT_FILE);
    submissionAgreement.setTitle(DLCMTestConstants.getRandomNameWithTemporaryLabel("A"));
    submissionAgreement.setVersion(DLCMTestConstants.getRandomNameWithTemporaryLabel("1"));
    final SubmissionAgreement expectedSubmissionAgreement = this.submissionAgreementClientService.createSubmissionAgreementWithFile(
            submissionAgreement,
            submissionAgreementFile);

    // Try to update the submission agreement, it should be forbidden, even for ADMIN
    expectedSubmissionAgreement.setTitle(DLCMTestConstants.getRandomNameWithTemporaryLabel("B"));
    expectedSubmissionAgreement.setVersion(DLCMTestConstants.getRandomNameWithTemporaryLabel("2"));
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.submissionAgreementClientService.update(expectedSubmissionAgreement.getResId(), expectedSubmissionAgreement));
  }

  protected void clearSubmissionAgreementFixtures() {
    final List<SubmissionAgreement> agreements = this.submissionAgreementClientService.findAll();

    for (final SubmissionAgreement sa : agreements) {
      if (sa.getTitle().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.submissionAgreementClientService.delete(sa.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearSubmissionAgreementFixtures();
  }
}

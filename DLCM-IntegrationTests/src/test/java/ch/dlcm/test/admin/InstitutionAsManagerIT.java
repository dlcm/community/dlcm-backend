/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - InstitutionAsManagerIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.InstitutionUserClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.PersonITService;

class InstitutionAsManagerIT extends AbstractInstitutionUserIT {

  protected InstitutionUserClientService institutionUserService;

  @Autowired
  public InstitutionAsManagerIT(Environment env, SudoRestClientTool restClientTool, InstitutionITService institutionITService,
          PersonITService personITService, InstitutionUserClientService institutionUserClientService) {
    super(env, restClientTool, institutionITService, personITService);
    this.institutionUserService = institutionUserClientService;
  }

  @Override
  protected String role() {
    return Role.MANAGER_ID;
  }

  @Test
  void institutionForCurrentRoleIsAccessibleTest() {
    List<Institution> institutionList = this.institutionUserService.findAll();
    List<Institution> institutionsForCurrentRole = institutionList.stream()
            .filter(i -> i.getName().equals(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + "Institution for " + this.role().toLowerCase()))
            .collect(Collectors.toList());
    assertEquals(1, institutionsForCurrentRole.size());
  }

  @Test
  void creationNotPossibleTest() {
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.institutionITService.createRemoteInstitution("Institution created by user manager"));
  }

  @Test
  void updateForManagedInstitutionTest() {
    Institution institution = this.createRemoteInstitutionAsAdminWithCurrentUserWithRole("Institution created by admin for manager");

    institution.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Institution updated by admin for manager"));
    institution.setDescription("Update description by user manager");
    institution.setEmailSuffixes(Arrays.asList("test1.ch", "test2.ch", "test3.ch"));

    Institution institutionUpdated = this.institutionITService.updateInstitution(institution.getResId(), institution);

    assertEquals(institution.getName(), institutionUpdated.getName());
    assertEquals(institution.getDescription(), institutionUpdated.getDescription());
    assertTrue(institutionUpdated.getEmailSuffixes().contains("test1.ch"));
    assertTrue(institutionUpdated.getEmailSuffixes().contains("test2.ch"));
    assertTrue(institutionUpdated.getEmailSuffixes().contains("test3.ch"));

    // Create a person
    Person person = this.personITService.createRemotePerson();

    // Add a person to the institution
    this.institutionITService.addPersonRole(institutionUpdated.getResId(), person.getResId(), Role.VISITOR_ID);
    final Role role = this.institutionITService.getPersonRole(institutionUpdated.getResId(), person.getResId());
    assertEquals(Role.VISITOR_ID, role.getResId());
  }

  @Test
  void updateImpossibleForUnmanagedInstitutionTest() {
    Institution institution = this.createRemoteInstitutionAsAdmin("Institution created by admin");

    institution.setDescription("Update description");
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.institutionITService.updateInstitution(institution.getResId(), institution));
  }

  @Test
  void uploadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    final Institution institution1 = this.createRemoteInstitutionAsAdminWithCurrentUserWithRole("DLCM Institution #LOGO");
    final Institution refetchedInstitution = this.institutionITService.uploadLogo(institution1.getResId(), logoToUpload);
    assertEquals(refetchedInstitution.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedInstitution.getResourceFile().getFileSize(), sizeLogo);
  }

  @Test
  void downloadLogoTest() throws IOException {
    this.downloadLogoShouldSuccessTest();
  }

  @Test
  void deleteLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    final Institution institution1 = this.createRemoteInstitutionAsAdminWithCurrentUserWithRole("DLCM Institution #LOGO");
    Institution fetchedInstitution = this.institutionITService.uploadLogo(institution1.getResId(), logoToUpload);

    Institution refetchedInstitution = this.institutionITService.getInstitution(fetchedInstitution.getResId());
    assertEquals(refetchedInstitution.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedInstitution.getResourceFile().getFileSize(), sizeLogo);

    this.institutionITService.deleteLogo(institution1.getResId());
    refetchedInstitution = this.institutionITService.getInstitution(fetchedInstitution.getResId());
    assertNull(refetchedInstitution.getResourceFile());
  }
}

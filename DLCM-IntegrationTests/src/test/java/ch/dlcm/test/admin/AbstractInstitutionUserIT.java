/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractInstitutionUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.Person;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.PersonITService;

abstract class AbstractInstitutionUserIT extends AbstractIT {

  protected InstitutionITService institutionITService;
  protected PersonITService personITService;

  protected AbstractInstitutionUserIT(Environment env, SudoRestClientTool restClientTool, InstitutionITService institutionITService,
          PersonITService personITService) {
    super(env, restClientTool);
    this.institutionITService = institutionITService;
    this.personITService = personITService;
  }

  protected abstract String role();

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @BeforeAll
  void setUp() {
    this.restClientTool.sudoAdmin();
    this.institutionITService.getOrCreatePermanentInstitution(PersistenceMode.PERMANENT, this.role(), AuthApplicationRole.ADMIN_ID);
    this.restClientTool.exitSudo();
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoAdmin();
    this.institutionITService.cleanTestData();
    this.restClientTool.exitSudo();
  }

  void downloadLogoShouldSuccessTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    this.restClientTool.sudoAdmin();
    final Institution institution1 = this.createRemoteInstitutionAsAdminWithCurrentUserWithRole("DLCM Institution #LOGO");
    final Institution refetchedInstitution = this.institutionITService.uploadLogo(institution1.getResId(), logoToUpload);
    assertEquals(refetchedInstitution.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedInstitution.getResourceFile().getFileSize(), sizeLogo);
    this.restClientTool.exitSudo();

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.institutionITService.downloadLogo(refetchedInstitution.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedInstitution.getResourceFile().getFileSize());
  }

  protected Institution createRemoteInstitutionAsAdminWithCurrentUserWithRole(String description) {
    this.restClientTool.sudoAdmin();
    Institution institution = this.institutionITService.createRemoteInstitution(description);
    final Person person = this.personITService.getOrCreateRemotePerson();
    this.institutionITService.enforcePersonHasRoleInInstitution(institution, person, this.role());
    this.restClientTool.exitSudo();
    return institution;
  }

  protected Institution createRemoteInstitutionAsAdmin(String description) {
    this.restClientTool.sudoAdmin();
    Institution institution = this.institutionITService.createRemoteInstitution(description);
    this.restClientTool.exitSudo();
    return institution;
  }

}

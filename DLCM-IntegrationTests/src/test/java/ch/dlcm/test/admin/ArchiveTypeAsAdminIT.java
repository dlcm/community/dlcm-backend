/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveTypeAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.ArchiveType;
import ch.dlcm.service.admin.ArchiveTypeClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

class ArchiveTypeAsAdminIT extends AbstractIT {

  private final ArchiveTypeClientService archiveTypeService;

  @Autowired
  public ArchiveTypeAsAdminIT(Environment env, SudoRestClientTool restClientTool, ArchiveTypeClientService archiveTypeClientService) {
    super(env, restClientTool);
    this.archiveTypeService = archiveTypeClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    final ArchiveType archiveType1 = new ArchiveType();
    archiveType1.setMasterType(this.archiveTypeService.findOne(ArchiveType.DATASET_ID));
    archiveType1.setTypeName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Archive Type"));
    final ArchiveType archiveType2 = this.archiveTypeService.create(archiveType1);

    // Test the creation
    assertEquals(archiveType1.getMasterType().getTypeName(), archiveType2.getMasterType().getTypeName());
    assertEquals(archiveType1.getTypeName(), archiveType2.getTypeName());
  }

  protected void clearArchiveTypeFixtures() {
    final List<ArchiveType> rdList = this.archiveTypeService.findAll();

    for (final ArchiveType at : rdList) {
      if (at.getTypeName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.archiveTypeService.delete(at.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearArchiveTypeFixtures();
  }

}

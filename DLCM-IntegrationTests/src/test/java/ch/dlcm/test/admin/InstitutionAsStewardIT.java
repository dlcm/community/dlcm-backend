/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - InstitutionAsStewardIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.service.admin.InstitutionUserClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.PersonITService;

class InstitutionAsStewardIT extends AbstractInstitutionUserIT {

  protected InstitutionUserClientService institutionUserService;

  @Autowired
  public InstitutionAsStewardIT(Environment env, SudoRestClientTool restClientTool, InstitutionITService institutionITService,
          PersonITService personITService, InstitutionUserClientService institutionUserClientService) {
    super(env, restClientTool, institutionITService, personITService);
    this.institutionUserService = institutionUserClientService;
  }

  @Override
  protected String role() {
    return Role.STEWARD_ID;
  }

  @Test
  void institutionForCurrentRoleIsAccessibleTest() {
    List<Institution> institutionList = this.institutionUserService.findAll();
    List<Institution> institutionsForCurrentRole = institutionList.stream()
            .filter(i -> i.getName().equals(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + "Institution for " + this.role().toLowerCase()))
            .collect(Collectors.toList());
    assertEquals(1, institutionsForCurrentRole.size());
  }

  @Test
  void creationNotPossibleTest() {
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.institutionITService.createRemoteInstitution("Institution created by user " + this.role()));
  }

  @Test
  void updateForStewardInstitutionNotPossibleTest() {
    Institution institution = this.createRemoteInstitutionAsAdminWithCurrentUserWithRole("Institution created by admin for " + this.role());

    institution.setDescription("Update description by user manager");
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.institutionITService.updateInstitution(institution.getResId(), institution));
  }

  @Test
  void updateImpossibleForUnmanagedInstitutionTest() {
    Institution institution = this.createRemoteInstitutionAsAdmin("Institution created by admin");

    institution.setDescription("Update description");
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.institutionITService.updateInstitution(institution.getResId(), institution));
  }

  @Test
  void uploadLogoTest() {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Institution institution1 = this.createRemoteInstitutionAsAdminWithCurrentUserWithRole("DLCM Institution #LOGO");
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.institutionITService.uploadLogo(institution1.getResId(), logoToUpload));
  }

  @Test
  void downloadLogoTest() throws IOException {
    this.downloadLogoShouldSuccessTest();
  }

  @Test
  void deleteLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    this.restClientTool.sudoAdmin();
    final Institution institution1 = this.createRemoteInstitutionAsAdminWithCurrentUserWithRole("DLCM Institution #LOGO");
    Institution fetchedInstitution = this.institutionITService.uploadLogo(institution1.getResId(), logoToUpload);

    Institution refetchedInstitution = this.institutionITService.getInstitution(fetchedInstitution.getResId());
    assertEquals(refetchedInstitution.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedInstitution.getResourceFile().getFileSize(), sizeLogo);
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.institutionITService.deleteLogo(institution1.getResId()));
  }
}

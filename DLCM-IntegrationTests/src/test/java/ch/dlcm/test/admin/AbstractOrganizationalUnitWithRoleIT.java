/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractOrganizationalUnitWithRoleIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.FundingAgencyITService;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.OrgUnitITService;

abstract class AbstractOrganizationalUnitWithRoleIT extends AbstractIT {

  protected OrganizationalUnitClientService organizationalUnitClientService;
  protected OrgUnitITService orgUnitITService;
  protected InstitutionITService institutionITService;
  protected FundingAgencyITService fundingAgencyITService;

  protected AbstractOrganizationalUnitWithRoleIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrganizationalUnitClientService organizationalUnitClientService,
          OrgUnitITService orgUnitITService,
          InstitutionITService institutionITService,
          FundingAgencyITService fundingAgencyITService) {
    super(env, restClientTool);
    this.organizationalUnitClientService = organizationalUnitClientService;
    this.orgUnitITService = orgUnitITService;
    this.institutionITService = institutionITService;
    this.fundingAgencyITService = fundingAgencyITService;
  }

  protected abstract String role();

  protected boolean inheritedRole() {
    return false;
  }

  @BeforeAll
  void setUp() {
    this.getOrCreateOrganizationalUnitForRole(PersistenceMode.PERMANENT);
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoAdmin();
    this.orgUnitITService.cleanTestData();
    this.institutionITService.cleanTestData();
    this.fundingAgencyITService.cleanTestData();
    this.restClientTool.exitSudo();
  }

  protected OrganizationalUnit getOrCreateOrganizationalUnitForRole(PersistenceMode persistenceMode) {
    this.restClientTool.sudoAdmin();
    OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(persistenceMode,
            DLCMTestConstants.OrganizationalUnitStatus.OPEN, this.role(), AuthApplicationRole.USER_ID, this.inheritedRole());
    this.restClientTool.exitSudo();
    return organizationalUnit;
  }

  protected OrganizationalUnit createRemoteOrgUnitWithInstitutionWithCurrentRole(PersistenceMode persistenceMode, String description) {
    Institution institution = this.institutionITService.getOrCreatePermanentInstitution(persistenceMode, this.role(), AuthApplicationRole.USER_ID);
    return this.createRemoteOrgUnit(description, institution);
  }

  protected OrganizationalUnit createRemoteOrgUnitWithInstitutionForSpecificRole(PersistenceMode persistenceMode, String description,
          String role) {
    Institution institution = this.institutionITService.getOrCreatePermanentInstitution(persistenceMode, role, AuthApplicationRole.USER_ID);
    return this.createRemoteOrgUnit(description, institution);
  }

  protected OrganizationalUnit createRemoteOrgUnit(String description) {
    return this.orgUnitITService.createTemporaryRemoteOrgUnit(description, new ArrayList<>());
  }

  protected OrganizationalUnit createRemoteOrgUnit(String description, Institution institution) {
    List<Institution> institutionList = new ArrayList<>();
    institutionList.add(institution);
    return this.orgUnitITService.createTemporaryRemoteOrgUnit(description, institutionList);
  }

  public void downloadLogoShouldSuccessTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit1 = this.getOrCreateOrganizationalUnitForRole(PersistenceMode.TEMPORARY);
    OrganizationalUnit organizationalUnit2 = this.orgUnitITService.uploadLogo(organizationalUnit1.getResId(), logoToUpload);

    final OrganizationalUnit refetchedOrganizationalUnit = this.orgUnitITService.getOrganizationalUnit(organizationalUnit2.getResId());
    assertEquals(refetchedOrganizationalUnit.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedOrganizationalUnit.getResourceFile().getFileSize(), sizeLogo);
    this.restClientTool.exitSudo();

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.orgUnitITService.downloadLogo(refetchedOrganizationalUnit.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedOrganizationalUnit.getResourceFile().getFileSize());
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - PersonAsRootIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.service.PersonITService;

class PersonAsRootIT extends AbstractIT {

  private final PersonClientService personClientService;
  private final PersonITService personITService;

  @Autowired
  public PersonAsRootIT(Environment env, SudoRestClientTool restClientTool, PersonClientService personClientService, PersonITService personITService) {
    super(env, restClientTool);
    this.personClientService = personClientService;
    this.personITService = personITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void uploadLogo() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Find the person linked to the user connected
     */
    Person myPerson = this.personITService.getLinkedPersonToCurrentUser();

    final Person fetchedPerson = this.personClientService.uploadAvatar(myPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personClientService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);
  }

  @Test
  void uploadLogoToThirdPartyUser() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    Person thirdPartyPerson = this.personITService.getLinkedPersonToThirdPartyUser();

    final Person fetchedPerson = this.personClientService.uploadAvatar(thirdPartyPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personClientService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);
  }

  @Test
  void downloadLogo() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();
    /*
     * Find the person linked to the user connected
     */
    Person myPerson = this.personITService.getLinkedPersonToCurrentUser();

    final Person fetchedPerson = this.personClientService.uploadAvatar(myPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personClientService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.personClientService.downloadAvatar(refetchedPerson.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedPerson.getAvatar().getFileSize());
  }

  @Test
  void downloadLogoToThirdPartyUser() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    Person thirdPartyPerson = this.personITService.getLinkedPersonToThirdPartyUser();

    final Person fetchedPerson = this.personClientService.uploadAvatar(thirdPartyPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personClientService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.personClientService.downloadAvatar(refetchedPerson.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedPerson.getAvatar().getFileSize());
  }

  @Test
  void deleteLogo() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    Person myPerson = this.personITService.getLinkedPersonToCurrentUser();

    this.personClientService.uploadAvatar(myPerson.getResId(), logoToUpload);
    assertNotNull(myPerson.getAvatar());
    assertEquals(myPerson.getAvatar().getFileName(), logoName);
    assertEquals(myPerson.getAvatar().getFileSize(), sizeLogo);
    this.personClientService.deleteAvatar(myPerson.getResId());

    Person person = this.personClientService.findOne(myPerson.getResId());
    assertNull(person.getAvatar());
  }

  @Test
  void deleteLogoToThirdPartyUser() {
    Person thirdPartyPerson = this.personITService.getLinkedPersonToThirdPartyUser();

    this.personClientService.deleteAvatar(thirdPartyPerson.getResId());

    Person person = this.personClientService.findOne(thirdPartyPerson.getResId());
    assertNull(person.getAvatar());
  }

  @Override
  protected void deleteFixtures() {

  }
}

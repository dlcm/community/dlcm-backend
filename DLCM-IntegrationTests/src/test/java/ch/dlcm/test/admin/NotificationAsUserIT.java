/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - NotificationAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationMark;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.NotificationClientService;
import ch.dlcm.service.admin.UserClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class NotificationAsUserIT extends AbstractNotificationIT {

  @Autowired
  public NotificationAsUserIT(Environment env, SudoRestClientTool restClientTool, NotificationClientService notificationClientService,
          UserClientService userClientService, OrgUnitITService orgUnitITService, PersonITService personITService,
          DepositITService depositITService) {
    super(env, restClientTool, notificationClientService, userClientService, orgUnitITService, personITService, depositITService);
  }

  @Test
  void sendTest() {
    this.sentRequestTest(NotificationType.CREATE_ORGUNIT_REQUEST, null, DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL + " New org unit name",
            true);

    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.createTemporaryRemoteOrgUnit(
            "Org unit for testing changes in notification status ", new ArrayList<>());
    this.restClientTool.exitSudo();

    this.sentRequestTest(NotificationType.JOIN_ORGUNIT_REQUEST, organizationalUnit,
            DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL + " New org unit name", true);
  }

  @Test
  void inboxTest() {
    final User currentUser = this.userClientService.getAuthenticatedUser();

    this.restClientTool.sudoAdmin();
    final OrganizationalUnit managerOrgUnit = this.createOrgUnitForPerson(Role.MANAGER_ID, currentUser.getPerson());
    final OrganizationalUnit stewardOrgUnit = this.createOrgUnitForPerson(Role.STEWARD_ID, currentUser.getPerson());
    final OrganizationalUnit approverOrgUnit = this.createOrgUnitForPerson(Role.APPROVER_ID, currentUser.getPerson());
    final OrganizationalUnit creatorOrgUnit = this.createOrgUnitForPerson(Role.CREATOR_ID, currentUser.getPerson());
    final OrganizationalUnit orgUnitForNoOne = this.orgUnitITService.getOrgUnitForNoOne(PersistenceMode.PERMANENT);
    this.restClientTool.exitSudo();

    this.inboxTest(NotificationType.JOIN_ORGUNIT_REQUEST, managerOrgUnit, true);
    this.inboxTest(NotificationType.JOIN_ORGUNIT_REQUEST, stewardOrgUnit, false);

    this.inboxTest(NotificationType.ACCESS_DATASET_REQUEST, stewardOrgUnit, true);

    this.inboxTest(NotificationType.VALIDATE_DEPOSIT_REQUEST, approverOrgUnit, true);
    this.inboxTest(NotificationType.VALIDATE_DEPOSIT_REQUEST, creatorOrgUnit, false);

    // This message is seen because the current user is a ApplicationRole ADMIN
    this.inboxTest(NotificationType.CREATE_ORGUNIT_REQUEST, orgUnitForNoOne, DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL + " New org unit name",
            false);
  }

  @Test
  void newOrgUnitNotificationWithoutObject() {
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
            () -> this.inboxTest(NotificationType.CREATE_ORGUNIT_REQUEST, null, false));
    this.assertFieldContainsValidationErrorMessage(e, "objectId",
            "Notifications of requests to create new organizational units require an organizational unit name");
  }

  @Test
  void changeStatusAndMarkTest() {
    final Person currentPerson = this.personITService.getLinkedPersonToCurrentUser();
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.createTemporaryRemoteOrgUnit(
            "Org unit for testing changes in notification status ", new ArrayList<>());
    this.orgUnitITService.enforcePersonHasRoleInOrgUnit(organizationalUnit, currentPerson, Role.MANAGER_ID);
    this.restClientTool.exitSudo();

    final Notification localNotification = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, organizationalUnit);

    Notification remoteNotification = this.createRemoteNotification(localNotification);
    assertEquals(NotificationStatus.PENDING, remoteNotification.getNotificationStatus(), "Initial notification status should be PENDING");
    assertEquals(NotificationMark.UNREAD, remoteNotification.getNotificationMark(), "Initial notification should be UNREAD");

    this.notificationClientService.setProcessedStatus(remoteNotification.getResId());
    remoteNotification = this.notificationClientService.getInboxNotification(remoteNotification.getResId());
    assertEquals(NotificationStatus.APPROVED, remoteNotification.getNotificationStatus(), "Notification status should be APPROVED");

    final Notification localNotification2 = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, organizationalUnit);

    Notification remoteNotification2 = this.createRemoteNotification(localNotification2);
    this.notificationClientService.setPendingStatus(remoteNotification2.getResId());
    remoteNotification2 = this.notificationClientService.getInboxNotification(remoteNotification2.getResId());
    assertEquals(NotificationStatus.PENDING, remoteNotification2.getNotificationStatus(), "Notification status should be PENDING");

    this.notificationClientService.setRefusedStatus(remoteNotification2.getResId(), REFUSAL_REASON);
    remoteNotification2 = this.notificationClientService.getInboxNotification(remoteNotification2.getResId());
    assertEquals(NotificationStatus.REFUSED, remoteNotification2.getNotificationStatus(), "Notification status should be REFUSED");
    assertEquals(REFUSAL_REASON, remoteNotification2.getResponseMessage());

    this.notificationClientService.setReadMark(remoteNotification2.getResId());
    remoteNotification2 = this.notificationClientService.getInboxNotification(remoteNotification2.getResId());
    assertEquals(NotificationMark.READ, remoteNotification2.getNotificationMark(), "Notification mark should be READ");

    this.notificationClientService.setUnreadMark(remoteNotification2.getResId());
    remoteNotification2 = this.notificationClientService.getInboxNotification(remoteNotification2.getResId());
    assertEquals(NotificationMark.UNREAD, remoteNotification2.getNotificationMark(), "Notification mark should be UNREAD");
  }

  @Test
  void cannotChangeAnotherUserNotification() {
    OrganizationalUnit notifiedOrgUnit = this.orgUnitITService.getOrgUnitForNoOne(PersistenceMode.PERMANENT);

    this.restClientTool.sudoAdmin();
    final Notification localNotification = this.createLocalNotification(NotificationType.VALIDATE_DEPOSIT_REQUEST, notifiedOrgUnit);
    Notification remoteNotification = this.createRemoteNotification(localNotification);
    assertEquals(NotificationStatus.PENDING, remoteNotification.getNotificationStatus(), "Initial notification status should be PENDING");
    this.restClientTool.exitSudo();

    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.notificationClientService.setProcessedStatus(remoteNotification.getResId()));
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.notificationClientService.setRefusedStatus(remoteNotification.getResId(), REFUSAL_REASON));
  }

  @Test
  void cannotCreateMultiplesNotificationsAccessDatasetRequestType() {
    this.checkUnableToCreateMultiplesNotificationsForSameUser(NotificationType.ACCESS_DATASET_REQUEST);
  }

  @Test
  void cannotCreateMultiplesNotificationsJoinOrgUnitType() {
    this.checkUnableToCreateMultiplesNotificationsForSameUser(NotificationType.JOIN_ORGUNIT_REQUEST);
  }

  @Test
  void cannotChangeNotificationStatusAlreadyAcceptedOrRefused() {
    final Person currentPerson = this.personITService.getLinkedPersonToCurrentUser();
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.createTemporaryRemoteOrgUnit(
            "Org unit for testing changes in notification status ", new ArrayList<>());
    this.orgUnitITService.enforcePersonHasRoleInOrgUnit(organizationalUnit, currentPerson, Role.MANAGER_ID);
    this.restClientTool.exitSudo();

    final Notification localNotification = this.createLocalNotification(NotificationType.JOIN_ORGUNIT_REQUEST, organizationalUnit);

    Notification remoteNotification = this.createRemoteNotification(localNotification);
    assertEquals(NotificationStatus.PENDING, remoteNotification.getNotificationStatus(), "Initial notification status should be PENDING");
    assertEquals(NotificationMark.UNREAD, remoteNotification.getNotificationMark(), "Initial notification should be UNREAD");

    this.notificationClientService.setProcessedStatus(remoteNotification.getResId());
    remoteNotification = this.notificationClientService.getInboxNotification(remoteNotification.getResId());
    assertEquals(NotificationStatus.APPROVED, remoteNotification.getNotificationStatus(), "Notification status should be APPROVED");

    remoteNotification.setNotificationStatus(NotificationStatus.REFUSED);
    final String notificationId = remoteNotification.getResId();
    final Notification updatedNotification = remoteNotification;
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.notificationClientService.update(notificationId, updatedNotification));
  }
}

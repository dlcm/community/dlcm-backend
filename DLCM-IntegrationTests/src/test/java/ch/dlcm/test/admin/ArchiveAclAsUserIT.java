/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveAclAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.ArchiveACLClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.ArchiveAclITService;
import ch.dlcm.test.service.OrgUnitITService;

class ArchiveAclAsUserIT extends AbstractIT {

  private final ArchiveACLClientService archiveACLService;
  private final OrgUnitITService orgUnitService;
  private final ArchiveACL permanentAclForThirdPartyUser;

  @Autowired
  public ArchiveAclAsUserIT(Environment env, SudoRestClientTool restClientTool, ArchiveACLClientService archiveACLClientService,
          OrgUnitITService orgUnitITService, ArchiveAclITService archiveAclITService) {
    super(env, restClientTool);
    this.archiveACLService = archiveACLClientService;
    this.orgUnitService = orgUnitITService;
    this.restClientTool.sudoAdmin();
    this.permanentAclForThirdPartyUser = archiveAclITService.getPermanentAclForThirdPartyUserOnClosedArchive();
    this.restClientTool.exitSudo();
  }

  @Test
  void creationWithoutAipIdTest() {
    this.restClientTool.sudoAdmin();
    OrganizationalUnit managerOrgUnit = this.orgUnitService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN, Role.MANAGER_ID, AuthApplicationRole.USER_ID, false);
    this.restClientTool.exitSudo();
    ArchiveACL acl = new ArchiveACL();
    acl.setOrganizationalUnit(managerOrgUnit);
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.archiveACLService.create(acl));
  }

  @Test
  void creationWithMissingAip() {
    OrganizationalUnit orgUnitForNoOne = this.orgUnitService.getOrgUnitForNoOne(PersistenceMode.PERMANENT);
    ArchiveACL acl = new ArchiveACL();
    acl.setOrganizationalUnit(orgUnitForNoOne);
    acl.setAipId("000");
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.archiveACLService.create(acl));
  }

  @Test
  void failUpdateTest() {
    final String archiveAclId = this.permanentAclForThirdPartyUser.getResId();
    final ArchiveACL updatedAcl = new ArchiveACL();
    updatedAcl.setUser(this.permanentAclForThirdPartyUser.getUser());
    updatedAcl.setAipId(this.permanentAclForThirdPartyUser.getAipId());
    updatedAcl.setOrganizationalUnit(this.permanentAclForThirdPartyUser.getOrganizationalUnit());
    updatedAcl.setExpiration(this.permanentAclForThirdPartyUser.getExpiration().plusDays(100));
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.archiveACLService.update(archiveAclId, updatedAcl));
  }

  @Test
  void failDeleteTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.archiveACLService.delete(this.permanentAclForThirdPartyUser.getResId()));
  }

  @Test
  void failListTest() {
    OrganizationalUnit orgUnitForNoOne = this.orgUnitService.getOrgUnitForNoOne(PersistenceMode.PERMANENT);
    ArchiveACL acl = new ArchiveACL();
    acl.setOrganizationalUnit(orgUnitForNoOne);
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.archiveACLService.searchByProperties(Map.of("organizationalUnit.resId", orgUnitForNoOne.getResId())));
  }

  @Override
  protected void deleteFixtures() {
    // Do nothing
  }
}

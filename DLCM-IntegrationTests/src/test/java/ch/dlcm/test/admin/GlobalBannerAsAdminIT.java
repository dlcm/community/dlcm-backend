/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - GlobalBannerAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.OffsetDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.model.GlobalBanner;
import ch.unige.solidify.model.Label;
import ch.unige.solidify.model.Language;
import ch.unige.solidify.model.LargeLabel;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.service.admin.GlobalBannerClientService;

class GlobalBannerAsAdminIT extends AbstractGlobalBannerIT {

  @Autowired
  public GlobalBannerAsAdminIT(Environment env, SudoRestClientTool restClientTool, GlobalBannerClientService globalBannerClientService) {
    super(env, restClientTool, globalBannerClientService);
  }

  private final String ENGLISH = "EN";
  private final String FRENCH = "FR";
  private final String ENGLISH_TEXT = "Text in English";
  private final String FRENCH_TEXT = "Texte en français";

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create a globalBanner
    final GlobalBanner savedGlobalBanner = this.createGlobalBanner("My banner", GlobalBanner.GlobalBannerType.CRITICAL, false,
            OffsetDateTime.now(), OffsetDateTime.now().plusDays(1));

    // Test the creation
    final GlobalBanner foundGlobalBanner = this.globalBannerClientService.findOne(savedGlobalBanner.getResId());
    assertNotNull(foundGlobalBanner);
    this.assertsGlobalBanner(savedGlobalBanner, foundGlobalBanner);

    // Test unique name
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.createGlobalBanner("My banner", GlobalBanner.GlobalBannerType.INFO, false, OffsetDateTime.now(),
            OffsetDateTime.now().plusDays(1)));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Name is unique");
  }

  @Test
  void creationTestConflictingPeriod() {
    OffsetDateTime now = OffsetDateTime.now();
    OffsetDateTime startDate = now.minusYears(10);
    OffsetDateTime endDate = startDate.plusDays(1);
    // Create a globalBanner
    final GlobalBanner savedOriginalGlobalBanner = this.createGlobalBanner("Original banner", GlobalBanner.GlobalBannerType.CRITICAL, true,
            startDate, endDate);

    // Test the creation
    final GlobalBanner foundOriginalGlobalBanner = this.globalBannerClientService.findOne(savedOriginalGlobalBanner.getResId());
    assertNotNull(foundOriginalGlobalBanner);
    this.assertsGlobalBanner(savedOriginalGlobalBanner, foundOriginalGlobalBanner);

    // Test conflicting date inside original global banner
    OffsetDateTime startDateConflicting1 = startDate.plusHours(2);
    OffsetDateTime endDateConflicting1 = endDate.minusHours(1);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.createGlobalBanner("Conflicting banner", GlobalBanner.GlobalBannerType.INFO, true, startDateConflicting1, endDateConflicting1));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Conflicting date 1");

    // Test conflicting date end during original global banner
    OffsetDateTime startDateConflicting2 = startDate.minusHours(2);
    OffsetDateTime endDateConflicting2 = endDate.minusHours(2);
    HttpClientErrorException e2 = assertThrows(HttpClientErrorException.class, () -> this.createGlobalBanner("Conflicting banner 2", GlobalBanner.GlobalBannerType.INFO, true, startDateConflicting2, endDateConflicting2));
    assertEquals(HttpStatus.BAD_REQUEST, e2.getStatusCode(), "Conflicting date 2");

    // Test conflicting date start during original global banner
    OffsetDateTime startDateConflicting3 = startDate.plusHours(2);
    OffsetDateTime endDateConflicting3 = endDate.plusHours(2);
    HttpClientErrorException e3 = assertThrows(HttpClientErrorException.class, () -> this.createGlobalBanner("Conflicting banner 3", GlobalBanner.GlobalBannerType.INFO, true, startDateConflicting3, endDateConflicting3));
    assertEquals(HttpStatus.BAD_REQUEST, e3.getStatusCode(), "Conflicting date 3");

    // Test non-conflicting date with original global banner
    OffsetDateTime startDateNonConflicting = startDate.plusDays(2);
    OffsetDateTime endDateNonConflicting = endDate.plusDays(2);
    final GlobalBanner savedNonConflictingBanner = this.createGlobalBanner("Non conflicting banner", GlobalBanner.GlobalBannerType.INFO,
            true, startDateNonConflicting, endDateNonConflicting);
    final GlobalBanner foundNonConflictingBanner = this.globalBannerClientService.findOne(savedNonConflictingBanner.getResId());
    assertNotNull(foundNonConflictingBanner);
    this.assertsGlobalBanner(savedNonConflictingBanner, foundNonConflictingBanner);
  }

  @Test
  void creationWithTitlesTest() {
    // Create a globalBanner
    final GlobalBanner savedGlobalBanner = this.createGlobalBanner("My banner with title", GlobalBanner.GlobalBannerType.CRITICAL, false,
            OffsetDateTime.now(), OffsetDateTime.now().plusDays(1));

    // Test the creation
    GlobalBanner foundGlobalBanner = this.globalBannerClientService.findOne(savedGlobalBanner.getResId());
    assertNotNull(foundGlobalBanner);
    this.assertsGlobalBanner(savedGlobalBanner, foundGlobalBanner);

    // Add titles
    this.addTitles(foundGlobalBanner);

    // Save global banner
    GlobalBanner latestGlobalBanner = this.globalBannerClientService.update(savedGlobalBanner.getResId(), foundGlobalBanner);
    this.assertsGlobalBannerWithDescriptionCheck(savedGlobalBanner, latestGlobalBanner, false);

    // Check titles
    for (Label title : latestGlobalBanner.getTitleLabels()) {
      if (title.getLanguage().getResId().equals(this.ENGLISH)) {
        assertEquals(this.ENGLISH_TEXT, title.getText());
      } else if (title.getLanguage().getResId().equals(this.FRENCH)) {
        assertEquals(this.FRENCH_TEXT, title.getText());
      }
    }
  }

  @Test
  void creationWithDescriptionsTest() {
    // Create a globalBanner
    final GlobalBanner savedGlobalBanner = this.createGlobalBanner("My banner with descriptions", GlobalBanner.GlobalBannerType.CRITICAL, false,
            OffsetDateTime.now(), OffsetDateTime.now().plusDays(1));

    // Test the creation
    GlobalBanner foundGlobalBanner = this.globalBannerClientService.findOne(savedGlobalBanner.getResId());
    assertNotNull(foundGlobalBanner);
    this.assertsGlobalBanner(savedGlobalBanner, foundGlobalBanner);

    // Add description
    this.addDescriptions(foundGlobalBanner);

    // Save global banner
    GlobalBanner latestGlobalBanner = this.globalBannerClientService.update(savedGlobalBanner.getResId(), foundGlobalBanner);
    this.assertsGlobalBannerWithDescriptionCheck(savedGlobalBanner, latestGlobalBanner, true);
    // Check descriptions
    for (LargeLabel desc : latestGlobalBanner.getDescriptionLabels()) {
      if (desc.getLanguage().getResId().equals(this.ENGLISH)) {
        assertEquals(this.ENGLISH_TEXT, desc.getText());
      } else if (desc.getLanguage().getResId().equals(this.FRENCH)) {
        assertEquals(this.FRENCH_TEXT, desc.getText());
      }
    }
  }

  @Test
  void creationWithTitleAndDescriptionsTest() {
    // Create a globalBanner
    final GlobalBanner savedGlobalBanner = this.createGlobalBanner("My banner with descriptions", GlobalBanner.GlobalBannerType.CRITICAL, false,
            OffsetDateTime.now(), OffsetDateTime.now().plusDays(1));

    // Test the creation
    GlobalBanner foundGlobalBanner = this.globalBannerClientService.findOne(savedGlobalBanner.getResId());
    assertNotNull(foundGlobalBanner);
    this.assertsGlobalBanner(savedGlobalBanner, foundGlobalBanner);

    // Add titles
    this.addTitles(foundGlobalBanner);
    // Add description
    this.addDescriptions(foundGlobalBanner);

    // Save global banner
    GlobalBanner latestGlobalBanner = this.globalBannerClientService.update(savedGlobalBanner.getResId(), foundGlobalBanner);
    this.assertsGlobalBannerWithDescriptionCheck(savedGlobalBanner, latestGlobalBanner, true);

    // Check titles
    for (Label title : latestGlobalBanner.getTitleLabels()) {
      if (title.getLanguage().getResId().equals(this.ENGLISH)) {
        assertEquals(this.ENGLISH_TEXT, title.getText());
      } else if (title.getLanguage().getResId().equals(this.FRENCH)) {
        assertEquals(this.FRENCH_TEXT, title.getText());
      }
    }

    // Check descriptions
    for (LargeLabel desc : latestGlobalBanner.getDescriptionLabels()) {
      if (desc.getLanguage().getResId().equals(this.ENGLISH)) {
        assertEquals(this.ENGLISH_TEXT, desc.getText());
      } else if (desc.getLanguage().getResId().equals(this.FRENCH)) {
        assertEquals(this.FRENCH_TEXT, desc.getText());
      }
    }
  }

  private void addTitles(GlobalBanner globalBanner) {
    // Title in English
    Language english = this.getLanguage(this.ENGLISH);
    Label englishTitle = new Label();
    englishTitle.setLanguage(english);
    englishTitle.setText(this.ENGLISH_TEXT);
    globalBanner.getTitleLabels().add(englishTitle);
    // Title in French
    Language french = this.getLanguage(this.FRENCH);
    Label frenchTitle = new Label();
    frenchTitle.setLanguage(french);
    frenchTitle.setText(this.FRENCH_TEXT);
    globalBanner.getTitleLabels().add(frenchTitle);
  }

  private void addDescriptions(GlobalBanner globalBanner) {
    // Description in English
    Language english = this.getLanguage(this.ENGLISH);
    LargeLabel englishDesc = new LargeLabel();
    englishDesc.setLanguage(english);
    englishDesc.setText(this.ENGLISH_TEXT);
    globalBanner.getDescriptionLabels().add(englishDesc);
    // Description in French
    Language french = this.getLanguage(this.FRENCH);
    LargeLabel frenchDesc = new LargeLabel();
    frenchDesc.setLanguage(french);
    frenchDesc.setText(this.FRENCH_TEXT);
    globalBanner.getDescriptionLabels().add(frenchDesc);
  }

  private Language getLanguage(String languageId) {
    Language language = new Language();
    language.setResId(languageId);
    return language;
  }
}

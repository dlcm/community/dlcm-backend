/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - IndexFieldAliasAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.service.datamgmt.IndexFieldAliasClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

class IndexFieldAliasAsUserIT extends AbstractIT {

  private final IndexFieldAliasClientService indexFieldAliasClientService;

  @Autowired
  public IndexFieldAliasAsUserIT(Environment env, SudoRestClientTool restClientTool, IndexFieldAliasClientService indexFieldAliasClientService) {
    super(env, restClientTool);
    this.indexFieldAliasClientService = indexFieldAliasClientService;
  }

  @Test
  void creationTest() {
    IndexFieldAlias indexFieldAlias = this.getBasicIndexFieldAlias();
    try {
      this.indexFieldAliasClientService.create(indexFieldAlias);
      fail("An Http error response should be received");
    } catch (HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void findAllTest() {
    try {
      this.indexFieldAliasClientService.findAll();
      fail("An Http error response should be received");
    } catch (HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  private IndexFieldAlias getBasicIndexFieldAlias() {
    IndexFieldAlias indexFieldAlias = new IndexFieldAlias();
    indexFieldAlias.setIndexName(DLCMConstants.TEST_RES_ID);
    indexFieldAlias.setAlias(DLCMTestConstants.getRandomNameWithTemporaryLabel("alias"));
    indexFieldAlias.setField(DLCMTestConstants.getRandomNameWithTemporaryLabel("path.to.value"));
    return indexFieldAlias;
  }

  @Override
  protected void deleteFixtures() {
  }
}

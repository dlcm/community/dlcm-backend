/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrganizationalUnitAsManagerIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.display.OrganizationalUnitDisseminationPolicyDTO;
import ch.dlcm.model.display.OrganizationalUnitPreservationPolicyDTO;
import ch.dlcm.model.display.OrganizationalUnitSubmissionPolicyDTO;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.FundingAgencyITService;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

class OrganizationalUnitAsManagerIT extends AbstractOrganizationalUnitWithRoleIT {

  protected PersonITService personITService;

  @Autowired
  public OrganizationalUnitAsManagerIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrganizationalUnitClientService orgUnitClientService,
          OrgUnitITService orgUnitITService,
          InstitutionITService institutionITService,
          FundingAgencyITService fundingAgencyITService,
          PersonITService personITService) {
    super(env, restClientTool, orgUnitClientService, orgUnitITService, institutionITService, fundingAgencyITService);
    this.personITService = personITService;
  }

  @Override
  protected String role() {
    return Role.MANAGER_ID;
  }

  @Test
  void updateOrgUnit() {
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Update org unit as " + this.role());
    this.restClientTool.exitSudo();

    String newName = organizationalUnit.getName() + " successfully updated";
    String newDescription = "Description successfully updated";
    this.orgUnitITService.setRandomUrlOrganizationalUnit(organizationalUnit);
    URL newUrl = organizationalUnit.getUrl();
    LocalDate newOpeningDate = LocalDate.now().minusMonths(1);
    LocalDate newClosingDate = LocalDate.now().plusMonths(1);
    List<String> newKeywords = new ArrayList<>();
    newKeywords.add("UPDATE");

    organizationalUnit.setName(newName);
    organizationalUnit.setDescription(newDescription);
    organizationalUnit.setOpeningDate(newOpeningDate);
    organizationalUnit.setClosingDate(newClosingDate);
    organizationalUnit.setKeywords(newKeywords);

    final OrganizationalUnit organizationalUnitUpdated = this.organizationalUnitClientService.update(organizationalUnit.getResId(),
            organizationalUnit);

    assertEquals(newName, organizationalUnitUpdated.getName());
    assertEquals(newDescription, organizationalUnitUpdated.getDescription());
    assertEquals(newUrl, organizationalUnitUpdated.getUrl());
    assertEquals(newOpeningDate, organizationalUnitUpdated.getOpeningDate());
    assertEquals(newClosingDate, organizationalUnitUpdated.getClosingDate());
    assertEquals(newKeywords.size(), organizationalUnitUpdated.getKeywords().size());
    assertEquals(newKeywords.get(0), organizationalUnitUpdated.getKeywords().get(0));
  }

  @Test
  void addAndRemoveDisseminationPolicyTest() {
    this.restClientTool.sudoRoot();
    // Get test orgUnit
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Swiss Research Data Preservation #DISSEMINATION POLICY");
    // Create policy
    final DisseminationPolicy disseminationPolicy = this.orgUnitITService.createRemoteDisseminationPolicy();
    this.restClientTool.exitSudo();

    final DisseminationPolicy disseminationPolicyWithParameters1 = this.orgUnitITService.createLocalDisseminationPolicyWithParameters(
            DisseminationPolicy.DisseminationPolicyType.IIIF,
            Map.of(DLCMConstants.DP_NAME_PARAM, "rougemont iiif 1",
                    DLCMConstants.DP_IIIF_SERVER_PARAM, "https://iiif.unige.ch",
                    DLCMConstants.DP_IIIF_CONTEXT_PARAM, "rougemont",
                    DLCMConstants.DP_IIIF_MEDIA_FOLDER_PARAM, "media",
                    DLCMConstants.DP_IIIF_MANIFESTS_FOLDER_PARAM, "manifests"));
    final DisseminationPolicy disseminationPolicyWithParameters2 = this.orgUnitITService.createLocalDisseminationPolicyWithParameters(
            DisseminationPolicy.DisseminationPolicyType.IIIF,
            Map.of(DLCMConstants.DP_NAME_PARAM, "rougemont iiif 2",
                    DLCMConstants.DP_IIIF_SERVER_PARAM, "https://iiif.unige.ch",
                    DLCMConstants.DP_IIIF_CONTEXT_PARAM, "achilleid",
                    DLCMConstants.DP_IIIF_MEDIA_FOLDER_PARAM, "media",
                    DLCMConstants.DP_IIIF_MANIFESTS_FOLDER_PARAM, "manifests"));

    // Add policy to orgUnit
    final OrganizationalUnitDisseminationPolicyDTO orgUnitDisseminationPolicy1 = this.organizationalUnitClientService
            .addDisseminationPolicyWithParameters(organizationalUnit.getResId(), disseminationPolicy.getResId(), disseminationPolicyWithParameters1);
    final OrganizationalUnitDisseminationPolicyDTO orgUnitDisseminationPolicy2 = this.organizationalUnitClientService
            .addDisseminationPolicyWithParameters(organizationalUnit.getResId(), disseminationPolicy.getResId(), disseminationPolicyWithParameters2);
    List<OrganizationalUnitDisseminationPolicyDTO> disseminationPolicies = this.organizationalUnitClientService.getDisseminationPolicies(organizationalUnit.getResId());
    assertEquals(2, disseminationPolicies.size());

    // Test the retrieved policies
    for (final OrganizationalUnitDisseminationPolicyDTO testedPolicy : disseminationPolicies) {
      assertNotNull(testedPolicy);
      assertEquals(disseminationPolicy.getName(), testedPolicy.getName());
      assertEquals(disseminationPolicy.getType(), testedPolicy.getType());
    }

    // Retrieve the first parametrized dissemination policy
    OrganizationalUnitDisseminationPolicyDTO retrievedOrgUnitDisseminationPolicy1 = this.organizationalUnitClientService.getDisseminationPolicy(organizationalUnit.getResId(),
            disseminationPolicy.getResId(),
            orgUnitDisseminationPolicy1.getJoinResource().getCompositeKey());
    assertEquals(orgUnitDisseminationPolicy1.getResId(), retrievedOrgUnitDisseminationPolicy1.getResId());
    assertEquals(orgUnitDisseminationPolicy1.getName(), retrievedOrgUnitDisseminationPolicy1.getName());
    assertEquals(orgUnitDisseminationPolicy1.getJoinResource().getOrganizationalUnit().getResId(),
            retrievedOrgUnitDisseminationPolicy1.getJoinResource().getOrganizationalUnit().getResId());

    // Remove the first parametrized dissemination policy
    this.organizationalUnitClientService.removeDisseminationPolicy(organizationalUnit.getResId(), disseminationPolicy.getResId(),
            orgUnitDisseminationPolicy1.getJoinResource().getCompositeKey());

    // The second parametrized dissemination policy should still be there
    disseminationPolicies = this.organizationalUnitClientService.getDisseminationPolicies(organizationalUnit.getResId());
    assertEquals(1, disseminationPolicies.size());
    assertEquals(orgUnitDisseminationPolicy2.getJoinResource().getCompositeKey(), disseminationPolicies.get(0).getJoinResource().getCompositeKey());
    assertEquals(orgUnitDisseminationPolicy2.getResId(), disseminationPolicies.get(0).getResId());
    assertEquals(orgUnitDisseminationPolicy2.getJoinResource().getDisseminationPolicy().getResId(), disseminationPolicies.get(0).getResId());

    this.orgUnitITService.createLocalDisseminationPolicyWithParameters(
            DisseminationPolicy.DisseminationPolicyType.IIIF,
            Map.of(DLCMConstants.DP_NAME_PARAM, "rougemont iiif 2",
                    DLCMConstants.DP_IIIF_SERVER_PARAM, "https://iiif.unige.ch",
                    DLCMConstants.DP_IIIF_CONTEXT_PARAM, "/",
                    DLCMConstants.DP_IIIF_MEDIA_FOLDER_PARAM, "images",
                    DLCMConstants.DP_IIIF_MANIFESTS_FOLDER_PARAM, "manifests"));

    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.organizationalUnitClientService.addDisseminationPolicyWithParameters(organizationalUnit.getResId(), disseminationPolicy.getResId(),
                    disseminationPolicyWithParameters2));
  }

  @Test
  void addAndRemovePreservationPolicyTest() {
    // Get test orgUnit
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Swiss Research Data Preservation #PRESERVATION POLICY");
    this.restClientTool.exitSudo();

    List<OrganizationalUnitPreservationPolicyDTO> policies = this.organizationalUnitClientService
            .getPreservationPolicies(organizationalUnit.getResId());
    final int initialState = policies.size();

    // Create policy
    this.restClientTool.sudoAdmin();
    final PreservationPolicy policy = this.orgUnitITService.createRemotePreservationPolicy();
    this.restClientTool.exitSudo();

    // Add policy to orgUnit
    this.organizationalUnitClientService.addPreservationPolicy(organizationalUnit.getResId(), policy.getResId());
    policies = this.organizationalUnitClientService.getPreservationPolicies(organizationalUnit.getResId());
    assertEquals(policies.size(), initialState + 1);

    // Test add
    for (final OrganizationalUnitPreservationPolicyDTO policy2 : policies) {
      if (policy2.getResId().equals(policy.getResId())) {
        assertNotNull(policy2);
        assertEquals(policy.getName(), policy2.getName());
        assertEquals(policy.getRetention(), policy2.getRetention());
        assertEquals(policy.getDispositionApproval(), policy2.getDispositionApproval());
      }
    }

    // Remove policy
    this.organizationalUnitClientService.removePreservationPolicy(organizationalUnit.getResId(), policy.getResId());

    // Test remove
    policies = this.organizationalUnitClientService.getPreservationPolicies(organizationalUnit.getResId());
    assertEquals(policies.size(), initialState);
  }

  @Test
  void addAndRemoveSubmissionPolicyTest() {
    // Get test orgUnit
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Swiss Research Data Preservation #SUBMISSION POLICY");
    this.restClientTool.exitSudo();

    List<OrganizationalUnitSubmissionPolicyDTO> policies = this.organizationalUnitClientService
            .getSubmissionPolicies(organizationalUnit.getResId());
    final int initialState = policies.size();

    // Create policy
    this.restClientTool.sudoAdmin();
    final SubmissionPolicy policy = this.orgUnitITService.createRemoteSubmissionPolicy();
    this.restClientTool.exitSudo();

    // Add policy to orgUnit
    this.organizationalUnitClientService.addSubmissionPolicy(organizationalUnit.getResId(), policy.getResId());
    policies = this.organizationalUnitClientService.getSubmissionPolicies(organizationalUnit.getResId());

    assertEquals(policies.size(), initialState + 1);

    // Test added person on the OrganizationalUnit
    for (final OrganizationalUnitSubmissionPolicyDTO policy2 : policies) {
      if (policy2.getResId().equals(policy.getResId())) {
        assertNotNull(policy2);
        assertEquals(policy.getName(), policy2.getName());
        assertEquals(policy.getSubmissionApproval(), policy2.getSubmissionApproval());
      }
    }

    // Remove policy
    this.organizationalUnitClientService.removeSubmissionPolicy(organizationalUnit.getResId(), policy.getResId());

    // Test remove
    policies = this.organizationalUnitClientService.getSubmissionPolicies(organizationalUnit.getResId());
    assertEquals(policies.size(), initialState);
  }

  @Test
  void addAndRemoveInstitutionsTest() {
    /*
     * Get test orgUnit
     */
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.getOrCreateOrganizationalUnitForRole(PersistenceMode.TEMPORARY);
    this.restClientTool.exitSudo();

    List<Institution> list = this.organizationalUnitClientService.getInstitutions(organizationalUnit.getResId());
    final int initialState = list.size();

    /*
     * Create a new Institution
     */
    this.restClientTool.sudoAdmin();
    Institution institution = this.institutionITService.createRemoteInstitution("Institution");
    this.restClientTool.exitSudo();

    /*
     * Add the Institution to the OrganizationalUnit
     */
    this.organizationalUnitClientService.addInstitution(organizationalUnit.getResId(), institution.getResId());
    list = this.organizationalUnitClientService.getInstitutions(organizationalUnit.getResId());
    assertEquals(initialState + 1, list.size());
    final String institutionName = institution.getName();
    final Optional<Institution> optionalInstitution = list.stream().filter(ins -> ins.getName().equals(institutionName))
            .findFirst();
    assertTrue(optionalInstitution.isPresent());

    /*
     * Check it has been added
     */
    assertEquals(institution.getResId(), optionalInstitution.get().getResId());
    assertEquals(institution.getName(), optionalInstitution.get().getName());
    assertEquals(institution.getDescription(), optionalInstitution.get().getDescription());

    /*
     * Remove the Institution
     */
    this.organizationalUnitClientService.removeInstitution(organizationalUnit.getResId(), institution.getResId());
    /*
     * Check it has been removed
     */
    list = this.organizationalUnitClientService.getInstitutions(organizationalUnit.getResId());
    assertEquals(initialState, list.size());

    /*
     * Create 3 Institutions and link them to the OrganizationalUnit
     */
    List<String> institutions = Arrays.asList("Institution Sub 1", "Institution Sub 2", "Institution Sub 3");

    String institutionsToDelete = null;

    int i = 0;
    for (String ins : institutions) {
      this.restClientTool.sudoAdmin();
      Institution newInstitution = this.institutionITService.createRemoteInstitution(ins);
      this.restClientTool.exitSudo();
      this.organizationalUnitClientService.addInstitution(organizationalUnit.getResId(), newInstitution.getResId());

      if (i == 1) {
        institutionsToDelete = newInstitution.getResId();
      }

      i++;
    }

    /*
     * Check they are all linked correctly
     */
    list = this.organizationalUnitClientService.getInstitutions(organizationalUnit.getResId());
    assertNotNull(list);
    assertEquals(initialState + 3, list.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.organizationalUnitClientService.removeInstitution(organizationalUnit.getResId(), institutionsToDelete);
    list = this.organizationalUnitClientService.getInstitutions(organizationalUnit.getResId());
    assertNotNull(list);
    assertEquals(initialState + 2, list.size());
    for (final Institution fetched_institution : list) {
      assertNotEquals(institutionsToDelete, fetched_institution.getResId());
    }
  }

  @Test
  void addAndRemoveFundingAgency() {
    /*
     * Get test orgUnit
     */
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Add and remove funding agency as " + this.role());
    this.restClientTool.exitSudo();

    List<FundingAgency> fundingAgencies = this.organizationalUnitClientService.getFundingAgencies(organizationalUnit.getResId());
    final int initialState = fundingAgencies.size();

    /*
     * Create a new FundingAgency
     */
    this.restClientTool.sudoAdmin();
    FundingAgency newAgency = this.fundingAgencyITService.createRemoteFundingAgency("FA", "My FundingAgency");
    this.restClientTool.exitSudo();

    /*
     * Add the FundingAgency to the OrganizationalUnit
     */
    this.organizationalUnitClientService.addFundingAgency(organizationalUnit.getResId(), newAgency.getResId());

    /*
     * Check it has been added
     */
    fundingAgencies = this.organizationalUnitClientService.getFundingAgencies(organizationalUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 1, fundingAgencies.size());
    final Optional<FundingAgency> optionalFA = fundingAgencies.stream().filter(fa -> fa.getName().equals(newAgency.getName())).findFirst();
    assertTrue(optionalFA.isPresent());
    assertEquals(newAgency.getName(), optionalFA.get().getName());
    assertEquals(newAgency.getAcronym(), optionalFA.get().getAcronym());

    /*
     * Remove the FundingAgency
     */
    this.organizationalUnitClientService.removeFundingAgency(organizationalUnit.getResId(), newAgency.getResId());

    /*
     * Check it has been removed
     */
    fundingAgencies = this.organizationalUnitClientService.getFundingAgencies(organizationalUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState, fundingAgencies.size());

    /*
     * Create 3 FundingAgencies and link them to the OrganizationalUnit
     */
    List<String> subAgencies = Arrays.asList("Sub FundingAgency 1", "Sub FundingAgency 2", "Sub FundingAgency 3");

    String agencyIdToDelete = null;

    int i = 0;
    for (String subAgencyName : subAgencies) {
      this.restClientTool.sudoAdmin();
      FundingAgency subAgency = this.fundingAgencyITService.createRemoteFundingAgency("FA" + i, subAgencyName);
      this.restClientTool.exitSudo();
      this.organizationalUnitClientService.addFundingAgency(organizationalUnit.getResId(), subAgency.getResId());

      if (i == 1) {
        agencyIdToDelete = subAgency.getResId();
      }

      i++;
    }

    /*
     * Check they are all linked correctly
     */
    fundingAgencies = this.organizationalUnitClientService.getFundingAgencies(organizationalUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 3, fundingAgencies.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.organizationalUnitClientService.removeFundingAgency(organizationalUnit.getResId(), agencyIdToDelete);
    fundingAgencies = this.organizationalUnitClientService.getFundingAgencies(organizationalUnit.getResId());
    assertNotNull(fundingAgencies);
    assertEquals(initialState + 2, fundingAgencies.size());
    for (final FundingAgency fetched_agency : fundingAgencies) {
      assertNotEquals(agencyIdToDelete, fetched_agency.getResId());
    }

  }

  @Test
  void addPersonToTheOrganizationalUnitTest() {
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Swiss Research Data Preservation #PERSON");
    final Person person = this.personITService.createRemotePerson();
    this.restClientTool.exitSudo();

    final int peopleBeforeAdding = this.organizationalUnitClientService.getPeople(organizationalUnit.getResId()).size();

    // Add the person to the OrganizationalUnit with default role
    this.organizationalUnitClientService.addPerson(organizationalUnit.getResId(), person.getResId());
    final List<Person> people = this.organizationalUnitClientService.getPeople(organizationalUnit.getResId());

    // If a person is linked to the basic auth account who created the organizational unit
    // then this person is automatically added to the organizational unit.
    // So one or two people should be present in the list
    assertTrue(people.size() == peopleBeforeAdding + 1 || people.size() == peopleBeforeAdding + 2);

    // Test added person on the OrganizationalUnit
    for (final Person person2 : people) {
      if (person2.getResId().equals(person.getResId())) {
        assertNotNull(person2);
        assertEquals(person.getFirstName(), person2.getFirstName());
        assertEquals(person.getLastName(), person2.getLastName());
        assertEquals(person.getOrcid(), person2.getOrcid());
      }
    }
  }

  @Test
  void addMultipleRolesForPersonToTheOrganizationalUnitTest() {
    this.restClientTool.sudoAdmin();
    final OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(
            PersistenceMode.TEMPORARY, "Swiss Research Data Preservation #ROLES");
    this.restClientTool.exitSudo();

    final Person person1 = this.personITService.createRemotePerson();
    final Person person2 = this.personITService.createRemotePerson();
    final int peopleBeforeAdding = this.organizationalUnitClientService.getPeople(organizationalUnit.getResId()).size();

    // Add the persons to the OrganizationalUnit with roles
    this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person1.getResId(), Role.STEWARD_ID);
    this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person2.getResId(), Role.CREATOR_ID);
    final List<Person> people = this.organizationalUnitClientService.getPeople(organizationalUnit.getResId());
    assertEquals(peopleBeforeAdding + 2, people.size());
    // Add the same role
    this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person1.getResId(), Role.STEWARD_ID);
    // Add the new role
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person1.getResId(), Role.CREATOR_ID));
  }

  @Test
  void closeTest() {
    this.restClientTool.sudoAdmin();
    OrganizationalUnit organizationalUnit = this.createRemoteOrgUnitWithInstitutionWithCurrentRole(PersistenceMode.TEMPORARY,
            "Swiss Research Data Preservation #CLOSE");
    this.restClientTool.exitSudo();

    // Close
    final String resId = organizationalUnit.getResId();
    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringTool.DATE_FORMAT);
    final String closingDate = LocalDate.now().plusDays(1).format(formatter);

    this.organizationalUnitClientService.close(resId, closingDate);
    organizationalUnit = this.organizationalUnitClientService.findOne(resId);

    // Test close
    assertEquals(organizationalUnit.getClosingDate().toString(), closingDate);

    this.organizationalUnitClientService.delete(organizationalUnit.getResId());
  }

  @Test
  void uploadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    final OrganizationalUnit organizationalUnit1 = this.getOrCreateOrganizationalUnitForRole(PersistenceMode.TEMPORARY);
    OrganizationalUnit fetchedOrganizationalUnit = this.organizationalUnitClientService.uploadLogo(organizationalUnit1.getResId(), logoToUpload);

    final OrganizationalUnit refetchedOrganizationalUnit = this.organizationalUnitClientService.findOne(fetchedOrganizationalUnit.getResId());
    assertEquals(logoName, refetchedOrganizationalUnit.getResourceFile().getFileName());
    assertEquals(sizeLogo, refetchedOrganizationalUnit.getResourceFile().getFileSize());
  }

  @Test
  void downloadLogoTest() throws IOException {
    super.downloadLogoShouldSuccessTest();
  }

  @Test
  void deleteLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    final OrganizationalUnit organizationalUnit1 = this.getOrCreateOrganizationalUnitForRole(PersistenceMode.TEMPORARY);
    OrganizationalUnit fetchedOrganizationalUnit = this.organizationalUnitClientService.uploadLogo(organizationalUnit1.getResId(), logoToUpload);

    OrganizationalUnit refetchedOrganizationalUnit = this.organizationalUnitClientService.findOne(fetchedOrganizationalUnit.getResId());
    assertEquals(logoName, refetchedOrganizationalUnit.getResourceFile().getFileName());
    assertEquals(sizeLogo, refetchedOrganizationalUnit.getResourceFile().getFileSize());

    this.organizationalUnitClientService.deleteLogo(organizationalUnit1.getResId());
    refetchedOrganizationalUnit = this.organizationalUnitClientService.findOne(fetchedOrganizationalUnit.getResId());
    assertNull(refetchedOrganizationalUnit.getResourceFile());
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - InstitutionIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.InstitutionClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.PersonITService;

class InstitutionIT extends AbstractIT {

  protected InstitutionClientService institutionService;
  protected PersonITService personService;

  @Autowired
  public InstitutionIT(Environment env, SudoRestClientTool restClientTool, InstitutionClientService institutionClientService,
          PersonITService personITService) {
    super(env, restClientTool);
    this.institutionService = institutionClientService;
    this.personService = personITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create an institution
    final Institution institution1 = new Institution();
    institution1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Institut Du Du "));
    institution1.setDescription("C'est très joli");
    institution1.setEmailSuffixes(Arrays.asList("test1.ch", "test2.ch"));
    final Institution institution2 = this.institutionService.create(institution1);

    // Test the creation
    assertEquals(institution1.getName(), institution2.getName());
    assertEquals(institution1.getDescription(), institution2.getDescription());
    assertTrue(institution2.getEmailSuffixes().contains("test1.ch"));
    assertTrue(institution2.getEmailSuffixes().contains("test2.ch"));
  }

  @Test
  void creationTestWithCorrectURL() throws MalformedURLException {
    this.creationWithURL("https://wwww.dlcm.ch");
  }

  @Test
  void creationTestWithMalformedURL() {
    assertThrows(MalformedURLException.class, () -> this.creationWithURL("httppps://www.dlcm.ch"));
  }

  public void creationWithURL(String url) throws MalformedURLException {
    // Create an institution
    final Institution intstitution1 = new Institution();
    intstitution1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Institut von Ürl"));
    intstitution1.setDescription("ça c'est de l'insitut");
    try {
      intstitution1.setUrl(new URL(url));
    } catch (final MalformedURLException e) {
      throw e;
    }
    final Institution institution2 = this.institutionService.create(intstitution1);

    // Test the creation
    assertEquals(intstitution1.getName(), institution2.getName());
    assertEquals(intstitution1.getDescription(), institution2.getDescription());
    assertEquals(intstitution1.getUrl(), institution2.getUrl());
  }

  @Test
  void getTestWithRorId() {
    // Create an institution
    final Institution institution1 = new Institution();
    institution1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Institut Du Du "));
    institution1.setDescription("C'est très joli");
    institution1.setRorId("04v1bf639");
    final Institution institution2 = this.institutionService.create(institution1);

    // Test the creation
    assertEquals(institution1.getName(), institution2.getName());
    assertEquals(institution1.getDescription(), institution2.getDescription());
    assertEquals(institution1.getRorId(), institution2.getRorId());

    // Get License by ROR ID
    final Institution institution3 = this.institutionService.findByRorId(institution1.getRorId());
    // Test the get
    assertEquals(institution1.getName(), institution3.getName());
    assertEquals(institution1.getDescription(), institution3.getDescription());
    assertEquals(institution1.getRorId(), institution3.getRorId());
  }

  @Test
  void memberTest() {
    // Create an institution
    Institution institution = new Institution();
    institution.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Institut Du Du "));
    institution = this.institutionService.create(institution);

    // Create a person
    Person person = this.personService.createRemotePerson();

    // Add a person to the institution
    this.institutionService.addMember(institution.getResId(), person.getResId());
    final List<Person> listPersons = this.institutionService.getMembers(institution.getResId());
    final String resIdPersonToSearch = person.getResId();
    final Optional<Person> optionalPerson = listPersons.stream().filter(p -> p.getResId().equals(resIdPersonToSearch)).findFirst();
    assertTrue(optionalPerson.isPresent());

    // Test that the person is added to the institution
    assertEquals(person.getResId(), optionalPerson.get().getResId());
    assertEquals(person.getFirstName(), optionalPerson.get().getFirstName());
    assertEquals(person.getLastName(), optionalPerson.get().getLastName());
  }

  @Test
  void unicityTest() {
    // Create an institution
    Institution institution1 = new Institution();
    institution1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Institut Du Du "));
    institution1.setDescription("C'est très joli");
    institution1 = this.institutionService.create(institution1);

    // Create a second institution with the same name
    Institution institution2 = new Institution();
    institution2.setName(institution1.getName());
    institution2.setDescription("Wow, such description");

    // Testing unicity of the name
    try {
      this.institutionService.create(institution2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }

  }

  @Test
  void updateTest() {
    // Create an institution
    Institution institution1 = new Institution();
    institution1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("A"));
    institution1.setDescription("A");
    institution1.setEmailSuffixes(Arrays.asList("test1.ch", "test2.ch"));
    institution1 = this.institutionService.create(institution1);

    // Update the institution
    institution1.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("B"));
    institution1.setDescription("B");
    institution1.setEmailSuffixes(Arrays.asList("test1.ch", "test2.ch", "test3.ch"));
    this.institutionService.update(institution1.getResId(), institution1);
    final Institution institution2 = this.institutionService.findOne(institution1.getResId());

    // Test the update
    assertEquals(institution1.getName(), institution2.getName());
    assertEquals(institution1.getDescription(), institution2.getDescription());
    assertTrue(institution2.getEmailSuffixes().contains("test1.ch"));
    assertTrue(institution2.getEmailSuffixes().contains("test2.ch"));
    assertTrue(institution2.getEmailSuffixes().contains("test3.ch"));
    this.assertEqualsWithoutNanoSeconds(institution1.getCreationTime(), institution2.getCreationTime());
  }

  @Test
  void uploadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an institution
     */
    final Institution institution1 = this.createRemoteInstitution("DLCM Institution #LOGO");

    /*
     * Add logo
     */
    Institution fetchedInstitution = this.institutionService.uploadLogo(institution1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Institution refetchedInstitution = this.institutionService.findOne(fetchedInstitution.getResId());
    assertEquals(refetchedInstitution.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedInstitution.getResourceFile().getFileSize(), sizeLogo);
  }

  @Test
  void downloadLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an institution
     */
    final Institution institution1 = this.createRemoteInstitution("DLCM Institution #LOGO");

    /*
     * Add logo
     */
    Institution institution2 = this.institutionService.uploadLogo(institution1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Institution refetchedInstitution = this.institutionService.findOne(institution2.getResId());
    assertEquals(refetchedInstitution.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedInstitution.getResourceFile().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.institutionService.downloadLogo(refetchedInstitution.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedInstitution.getResourceFile().getFileSize());
  }

  @Test
  void deleteLogoTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Create an institution
     */
    final Institution institution1 = this.createRemoteInstitution("DLCM Institution #LOGO");

    /*
     * Add logo
     */
    Institution fetchedInstitution = this.institutionService.uploadLogo(institution1.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    Institution refetchedInstitution = this.institutionService.findOne(fetchedInstitution.getResId());
    assertEquals(refetchedInstitution.getResourceFile().getFileName(), logoName);
    assertEquals(refetchedInstitution.getResourceFile().getFileSize(), sizeLogo);

    this.institutionService.deleteLogo(institution1.getResId());
    refetchedInstitution = this.institutionService.findOne(fetchedInstitution.getResId());
    assertNull(refetchedInstitution.getResourceFile());
  }

  @Test
  void addUnknownPersonToTheInstitutionTest() {
    final Institution institution = this.createRemoteInstitution("Swiss Research Data Preservation #EXISTENCE");
    assertThrows(HttpClientErrorException.NotFound.class,
            () -> this.institutionService.addPerson(institution.getResId(), "unknown-person", Role.CREATOR_ID));
  }

  @Test
  void addUnknownRoleToTheInstitutionTest() {
    final Institution institution = this.createRemoteInstitution("Swiss Research Data Preservation #EXISTENCE");
    final Person person = this.personService.createRemotePerson();
    assertThrows(HttpClientErrorException.NotFound.class,
            () -> this.institutionService.addPerson(institution.getResId(), person.getResId(), "unknown-role"));
  }

  @Test
  void addUnknownInstitutionToTheInstitutionTest() {
    final Person person = this.personService.createRemotePerson();
    assertThrows(HttpClientErrorException.NotFound.class,
            () -> this.institutionService.addPerson("unknown-institution", person.getResId(), Role.CREATOR_ID));
  }

  @Test
  void addPersonWithDefaultRoleToTheInstituionTest() {
    final Institution institution = this.createRemoteInstitution("Swiss Research Data Preservation #PERSON");
    final Person person = this.personService.createRemotePerson();
    final int peopleBeforeAdding = this.institutionService.getPeople(institution.getResId()).size();

    // Add the person to the OrganizationalUnit with default role
    this.institutionService.addPerson(institution.getResId(), person.getResId());
    final List<Person> people = this.institutionService.getPeople(institution.getResId());

    assertEquals(peopleBeforeAdding + 1, people.size());

    // Test added person on the OrganizationalUnit
    for (final Person person2 : people) {
      if (person2.getResId().equals(person.getResId())) {
        assertNotNull(person2);
        assertEquals(person.getFirstName(), person2.getFirstName());
        assertEquals(person.getLastName(), person2.getLastName());
        assertEquals(person.getOrcid(), person2.getOrcid());
      }
    }
  }

  @Test
  void addPersonWithARoleToTheInstituionTest() {
    final Institution institution = this.createRemoteInstitution("Swiss Research Data Preservation #PERSON");
    final Person person = this.personService.createRemotePerson();
    final int peopleBeforeAdding = this.institutionService.getPeople(institution.getResId()).size();

    // Add the person to the OrganizationalUnit with default role
    this.institutionService.addPerson(institution.getResId(), person.getResId(), Role.MANAGER_ID);
    final List<Person> people = this.institutionService.getPeople(institution.getResId());

    assertEquals(peopleBeforeAdding + 1, people.size());

    // Test added person on the OrganizationalUnit
    for (final Person person2 : people) {
      if (person2.getResId().equals(person.getResId())) {
        assertNotNull(person2);
        assertEquals(person.getFirstName(), person2.getFirstName());
        assertEquals(person.getLastName(), person2.getLastName());
        assertEquals(person.getOrcid(), person2.getOrcid());
      }
    }
  }

  @Test
  void addMultipleRolesForPersonToTheInstitutionTest() {
    final Institution institution = this.createRemoteInstitution("Swiss Research Data Preservation #ROLES");
    final Person person1 = this.personService.createRemotePerson();
    final Person person2 = this.personService.createRemotePerson();
    final int peopleBeforeAdding = this.institutionService.getPeople(institution.getResId()).size();

    // Add the persons to the OrganizationalUnit with roles
    this.institutionService.addPerson(institution.getResId(), person1.getResId(), Role.STEWARD_ID);
    this.institutionService.addPerson(institution.getResId(), person2.getResId(), Role.CREATOR_ID);
    final List<Person> people = this.institutionService.getPeople(institution.getResId());
    assertEquals(peopleBeforeAdding + 2, people.size());
    // Add the same role
    this.institutionService.addPerson(institution.getResId(), person1.getResId(), Role.STEWARD_ID);
    // Add the new role
    assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.institutionService.addPerson(institution.getResId(), person1.getResId(), Role.CREATOR_ID));
  }

  protected void clearInstitutionsFixtures() {

    final List<Institution> institutions = this.institutionService.findAll();

    for (final Institution institution : institutions) {

      if (institution.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {

        /*
         * Remove eventual links with people first
         */
        final List<Person> people = this.institutionService.getMembers(institution.getResId());
        if (people != null && !people.isEmpty()) {
          for (final Person person : people) {
            this.institutionService.removeMember(institution.getResId(), person.getResId());
          }
        }

        /*
         * Remove eventual links with organizational units first
         */
        final List<OrganizationalUnit> units = this.institutionService.getOrganizationalUnits(institution.getResId());
        if (units != null && !units.isEmpty()) {
          for (final OrganizationalUnit unit : units) {
            this.institutionService.removeOrganizationalUnit(institution.getResId(), unit.getResId());
          }
        }

        this.institutionService.delete(institution.getResId());
      }
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearInstitutionsFixtures();
  }

  private Institution createRemoteInstitution(String description) {
    // Create a new Institution
    Institution institution = new Institution();
    institution.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel(description));
    institution = this.institutionService.create(institution);
    return institution;
  }
}

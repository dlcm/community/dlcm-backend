/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - PreservationJobReloadIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preservationplanning;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preservation.JobExecution;
import ch.dlcm.model.preservation.JobExecutionReport;
import ch.dlcm.model.preservation.JobType;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.datamgmt.DataMgmtClientService;
import ch.dlcm.service.preservationplanning.JobExecutionPlanningClientService;
import ch.dlcm.service.preservationplanning.PreservationPlanningClientService;
import ch.dlcm.test.DLCMTestConstants;

@Disabled("Run it manually")
class PreservationJobReloadIT extends AbstractPreservationPlanningIT {

  @Autowired
  public PreservationJobReloadIT(Environment env, SudoRestClientTool restClientTool,
          PreservationPlanningClientService preservationPlanningClientService, JobExecutionPlanningClientService jobExecutionPlanningClientService,
          DataMgmtClientService dataMgmtClientService, AIPClientService aipClientService) {
    super(env, restClientTool, preservationPlanningClientService, jobExecutionPlanningClientService, dataMgmtClientService, aipClientService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void reloadAipTest() {

    final String reloadAip = DLCMTestConstants.getRandomNameWithTemporaryLabel("Reload");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reloadAip);
    presJob.setJobType(JobType.RELOAD);

    presJob = this.preservationPlanningClientService.create(presJob);

    // Only aip with status completed will be reload
    final List<ArchivalInfoPackage> listAipBefore = this.aipClientService.findAll().stream()
            .filter(aip -> aip.getInfo().getStatus() == PackageStatus.COMPLETED).collect(Collectors.toList());

    this.preservationPlanningClientService.start(presJob.getResId());
    this.waitPreservationJobIsCompleted(presJob.getResId());
    this.waitUntilAllAipAreCompleted();

    presJob = this.preservationPlanningClientService.findOne(presJob.getResId());

    final List<ArchivalInfoPackage> listAipAfter = this.aipClientService.findAll().stream()
            .filter(aip -> aip.getInfo().getStatus() == PackageStatus.COMPLETED).collect(Collectors.toList());

    final List<JobExecution> executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());
    final JobExecution jobExecution = executionList.stream().max(Comparator.comparing(JobExecution::getRunNumber))
            .orElseThrow(NoSuchElementException::new);

    final List<JobExecutionReport> reports = this.jobExecutionPlanningClientService.findReport(presJob.getResId(), jobExecution.getResId());

    assertEquals(reports.get(0).getProcessedItems() + listAipBefore.size(), listAipAfter.size());

    // Check number of items created
    final RestCollection<ArchiveMetadata> aipIndex = this.dataMgmtClientService.listAllPublicAIP();
    assertEquals(listAipAfter.size(), aipIndex.getPage().getTotalItems());
  }

  @Override
  protected void deleteFixtures() {
    this.clearPreservationJobFixtures();
  }
}

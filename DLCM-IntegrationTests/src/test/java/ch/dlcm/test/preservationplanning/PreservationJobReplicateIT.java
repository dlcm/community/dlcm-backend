/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - PreservationJobReplicateIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preservationplanning;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.core.env.Environment;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.preservation.JobExecution;
import ch.dlcm.model.preservation.JobExecutionReport;
import ch.dlcm.model.preservation.JobType;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.datamgmt.DataMgmtClientService;
import ch.dlcm.service.preservationplanning.JobExecutionPlanningClientService;
import ch.dlcm.service.preservationplanning.PreservationPlanningClientService;
import ch.dlcm.test.DLCMTestConstants;

// @FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Disabled("Run it manually(replicate units before collections)")
class PreservationJobReplicateIT extends AbstractPreservationPlanningIT {

  public PreservationJobReplicateIT(Environment env, SudoRestClientTool restClientTool,
          PreservationPlanningClientService preservationPlanningClientService, JobExecutionPlanningClientService jobExecutionPlanningClientService,
          DataMgmtClientService dataMgmtClientService, AIPClientService aipClientService) {
    super(env, restClientTool, preservationPlanningClientService, jobExecutionPlanningClientService, dataMgmtClientService, aipClientService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  /**
   * This test has to be executed before the collection.
   */
  @Test
  void replicateTest() {

    final String reloadAip = DLCMTestConstants.getRandomNameWithTemporaryLabel("Replicate-units");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reloadAip);
    presJob.setJobType(JobType.REPLICATION);

    presJob = this.preservationPlanningClientService.create(presJob);

    // check aip indexed
    final RestCollection<ArchiveMetadata> aipIndex = this.dataMgmtClientService.listAllPublicUnitAip();

    final long totalIndexToReplicate = aipIndex.getPage().getTotalItems();

    this.preservationPlanningClientService.start(presJob.getResId());
    this.waitPreservationJobIsCompleted(presJob.getResId());

    presJob = this.preservationPlanningClientService.findOne(presJob.getResId());
    final List<JobExecution> executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());
    final JobExecution jobExecution = executionList.stream().max(Comparator.comparing(JobExecution::getRunNumber))
            .orElseThrow(NoSuchElementException::new);

    final List<JobExecutionReport> reports = this.jobExecutionPlanningClientService.findReport(presJob.getResId(), jobExecution.getResId());
    assertEquals(1, reports.size());
    assertEquals(0, reports.get(0).getInErrorItems());

    // check in all storage nodes that the aip are created
    for (final String url : this.preservationPlanningClientService.getArchivalStorageUrls()) {
      this.waitUntilAllAipAreReplicated(url, true, totalIndexToReplicate);
      final long totalAipUnit = this.aipClientService.findAllByNode(url).stream()
              .filter(aip -> !aip.isCollection() && aip.getInfo().getStatus() == PackageStatus.COMPLETED).count();
      assertEquals(totalIndexToReplicate, totalAipUnit);
    }
  }

  @Test
  void replicateVerifyCheckTest() {
    final String reloadAip = DLCMTestConstants.getRandomNameWithTemporaryLabel("Replicate-check");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reloadAip);
    presJob.setJobType(JobType.REPLICATION_CHECK);

    presJob = this.preservationPlanningClientService.create(presJob);

    final RestCollection<ArchiveMetadata> aipIndex = this.dataMgmtClientService.listAllPublicAIP();

    final long totalIndexToReplicate = aipIndex.getPage().getTotalItems();

    this.preservationPlanningClientService.start(presJob.getResId());
    this.waitPreservationJobIsCompleted(presJob.getResId());

    presJob = this.preservationPlanningClientService.findOne(presJob.getResId());
    final List<JobExecution> executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());
    final JobExecution jobExecution = executionList.stream().max(Comparator.comparing(JobExecution::getRunNumber))
            .orElseThrow(NoSuchElementException::new);

    final List<JobExecutionReport> reports = this.jobExecutionPlanningClientService.findReport(presJob.getResId(), jobExecution.getResId());
    assertEquals(1, reports.size());
    assertEquals(0, reports.get(0).getInErrorItems());
    assertEquals(totalIndexToReplicate, reports.get(0).getProcessedItems());

    // check in all storage nodes that the aip are created
    for (final String url : this.preservationPlanningClientService.getArchivalStorageUrls()) {
      final long total = this.aipClientService.findAllByNode(url).stream().filter(aip -> aip.getInfo().getStatus() == PackageStatus.COMPLETED).count();
      assertEquals(totalIndexToReplicate, total);
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearPreservationJobFixtures();
  }
}

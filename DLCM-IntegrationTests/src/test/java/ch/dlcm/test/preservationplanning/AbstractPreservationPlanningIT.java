/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractPreservationPlanningIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preservationplanning;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preservation.JobExecution;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.datamgmt.DataMgmtClientService;
import ch.dlcm.service.preservationplanning.JobExecutionPlanningClientService;
import ch.dlcm.service.preservationplanning.PreservationPlanningClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

abstract class AbstractPreservationPlanningIT extends AbstractIT {

  protected PreservationPlanningClientService preservationPlanningClientService;
  protected JobExecutionPlanningClientService jobExecutionPlanningClientService;
  protected DataMgmtClientService dataMgmtClientService;
  protected AIPClientService aipClientService;

  protected AbstractPreservationPlanningIT(
          Environment env,
          SudoRestClientTool restClientTool,
          PreservationPlanningClientService preservationPlanningClientService,
          JobExecutionPlanningClientService jobExecutionPlanningClientService,
          DataMgmtClientService dataMgmtClientService,
          AIPClientService aipClientService) {
    super(env, restClientTool);
    this.preservationPlanningClientService = preservationPlanningClientService;
    this.jobExecutionPlanningClientService = jobExecutionPlanningClientService;
    this.dataMgmtClientService = dataMgmtClientService;
    this.aipClientService = aipClientService;
  }

  protected boolean areAllAipIndexed(List<ArchivalInfoPackage> aipList) {
    return aipList.stream().noneMatch(
            aip -> aip.getInfo().getStatus() == PackageStatus.REINDEXING || aip.getInfo().getStatus() == PackageStatus.INDEXING
                    || aip.getInfo().getStatus() == PackageStatus.STORED);
  }

  protected boolean areAllAipStored(List<ArchivalInfoPackage> aipList) {
    return aipList.stream()
            .allMatch(aip -> aip.getInfo().getStatus() == PackageStatus.COMPLETED || aip.getInfo().getStatus() == PackageStatus.IN_ERROR);
  }

  protected void clearPreservationJobFixtures() {
    this.preservationPlanningClientService.findAll().stream()
            .filter(pres_job -> pres_job.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(pres_job -> this.preservationPlanningClientService.delete(pres_job.getResId()));
  }

  @Override
  protected void deleteFixtures() {
  }

  protected boolean waitPreservationJobIsCompleted(String presJobId) {
    final int waitInSeconds = 3;
    int count = 0;
    JobExecution jobExe;

    do {
      jobExe = this.preservationPlanningClientService.getJobExecutions(presJobId).stream().max(Comparator.comparing(JobExecution::getRunNumber))
              .orElseThrow((NoSuchElementException::new));
      DLCMTestConstants.failAfterNAttempt(count, waitInSeconds,
              "PreservationJob " + presJobId + " should have status Completed or InError instead of " + jobExe.getStatus());
      count++;

    } while (jobExe.getStatus() != PreservationJob.JobStatus.COMPLETED
            && jobExe.getStatus() != PreservationJob.JobStatus.IN_ERROR);

    return (PreservationJob.JobStatus.COMPLETED == jobExe.getStatus());

  }

  protected void waitUntilAllAipAreCompleted() {
    int count = 0;
    List<ArchivalInfoPackage> aipList;

    do {
      aipList = this.aipClientService.findAll();

      DLCMTestConstants.failAfterNAttempt(count, "Not all AIP are reload");
      count++;

    } while (!this.areAllAipStored(aipList) && aipList.size() > 0);
  }

  protected void waitUntilAllAipAreIndexed() {
    int count = 0;
    List<ArchivalInfoPackage> aipList;

    do {
      aipList = this.aipClientService.findAll();

      DLCMTestConstants.failAfterNAttempt(count, "Not all AIP are indexed");
      count++;

    } while (!this.areAllAipIndexed(aipList) && aipList.size() > 0);
  }

  protected void waitUntilAllAipAreReplicated(String url, boolean isUnit, long totalIndexToReplicate) {
    int count = 0;
    List<ArchivalInfoPackage> aipList;
    do {
      aipList = this.aipClientService.findAllByNode(url).stream().filter(aip -> !aip.isCollection() == isUnit).collect(Collectors.toList());

      DLCMTestConstants.failAfterNAttempt(count, "Not all AIP are replicated");
      count++;

    } while (aipList.stream().filter(aip -> aip.getInfo().getStatus() == PackageStatus.COMPLETED).count() != totalIndexToReplicate
            && aipList.size() > 0);
  }
}

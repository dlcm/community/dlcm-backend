/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - PreservationJobFixityIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preservationplanning;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preservation.JobExecution;
import ch.dlcm.model.preservation.JobExecutionReport;
import ch.dlcm.model.preservation.JobRecurrence;
import ch.dlcm.model.preservation.JobType;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.archivalstorage.AIPClientServiceSecondStorage;
import ch.dlcm.service.datamgmt.DataMgmtClientService;
import ch.dlcm.service.preservationplanning.JobExecutionPlanningClientService;
import ch.dlcm.service.preservationplanning.PreservationPlanningClientService;
import ch.dlcm.test.DLCMTestConstants;

class PreservationJobFixityIT extends AbstractPreservationPlanningIT {

  private final AIPClientServiceSecondStorage aipClientServiceSecondStorage;

  @Autowired
  public PreservationJobFixityIT(
          Environment env,
          SudoRestClientTool restClientTool,
          PreservationPlanningClientService preservationPlanningClientService,
          JobExecutionPlanningClientService jobExecutionPlanningClientService,
          DataMgmtClientService dataMgmtClientService,
          AIPClientService aipClientService,
          AIPClientServiceSecondStorage aipClientServiceSecondStorage) {
    super(env, restClientTool, preservationPlanningClientService, jobExecutionPlanningClientService, dataMgmtClientService, aipClientService);
    this.aipClientServiceSecondStorage = aipClientServiceSecondStorage;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void checkAipFixityTest() {

    final String aipFixity = DLCMTestConstants.getRandomNameWithTemporaryLabel("Fixity");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(aipFixity);
    presJob.setJobType(JobType.FIXITY);
    presJob.setJobRecurrence(JobRecurrence.DAILY);

    presJob = this.preservationPlanningClientService.create(presJob);

    // First execution
    this.preservationPlanningClientService.start(presJob.getResId());
    assertTrue(this.waitPreservationJobIsCompleted(presJob.getResId()), "Job#1 not completed");
    this.waitUntilAllAipAreCompleted();

    // Second execution
    this.preservationPlanningClientService.start(presJob.getResId());
    assertTrue(this.waitPreservationJobIsCompleted(presJob.getResId()), "Job#2 not completed");
    this.waitUntilAllAipAreCompleted();

    // Check processed items
    final List<JobExecution> jobExeList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());
    assertEquals(2, jobExeList.size(), "Wrong number of execution");

    // Compute number of all AIPs and number of not in progress AIPs
    // Fixity job should succeed on all not in progress AIPs
    final List<ArchivalInfoPackage> aipFromFirstStorage = this.aipClientService.findAll();
    final List<ArchivalInfoPackage> aipFromSecondStorage = this.aipClientServiceSecondStorage.findAll();
    final int nbAllAips = aipFromFirstStorage.size() + aipFromSecondStorage.size();
    final int nbNotInProgressAips = aipFromFirstStorage.stream().filter(aip -> !aip.isInProgress()).toList().size() +
                                aipFromSecondStorage.stream().filter(aip -> !aip.isInProgress()).toList().size();

    for (JobExecution jobExe : jobExeList) {
      final List<JobExecutionReport> reports = this.jobExecutionPlanningClientService.findReport(presJob.getResId(), jobExe.getResId());
      assertEquals(nbAllAips, reports.get(0).getTotalItems(),
              "Wrong number of total items for #" + jobExe.getRunNumber() + " execution");
      assertEquals(nbNotInProgressAips, reports.get(0).getProcessedItems(),
              "Wrong number of processed items for #" + jobExe.getRunNumber() + " execution");
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearPreservationJobFixtures();
  }
}

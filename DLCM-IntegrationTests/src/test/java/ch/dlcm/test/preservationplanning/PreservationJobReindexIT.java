/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - PreservationJobReindexIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preservationplanning;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preservation.JobExecution;
import ch.dlcm.model.preservation.JobExecutionReport;
import ch.dlcm.model.preservation.JobType;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.datamgmt.DataMgmtClientService;
import ch.dlcm.service.preservationplanning.JobExecutionPlanningClientService;
import ch.dlcm.service.preservationplanning.PreservationPlanningClientService;
import ch.dlcm.test.DLCMTestConstants;

@Disabled("Run these tests manually")
class PreservationJobReindexIT extends AbstractPreservationPlanningIT {

  @Autowired
  public PreservationJobReindexIT(Environment env, SudoRestClientTool restClientTool,
          PreservationPlanningClientService preservationPlanningClientService, JobExecutionPlanningClientService jobExecutionPlanningClientService,
          DataMgmtClientService dataMgmtClientService, AIPClientService aipClientService) {
    super(env, restClientTool, preservationPlanningClientService, jobExecutionPlanningClientService, dataMgmtClientService, aipClientService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    final String reindexJob = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJob);
    presJob.setJobType(JobType.REINDEX);

    presJob = this.preservationPlanningClientService.create(presJob);

    final PreservationJob presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());

    assertEquals(presJob.getName(), presJob2.getName());
    assertEquals(presJob.getJobType(), presJob2.getJobType());
    this.assertEqualsWithoutNanoSeconds(presJob.getCreationTime(), presJob2.getCreationTime());
  }

  @Test
  void reindexTest() {

    final String reindexJob = DLCMTestConstants.getRandomNameWithTemporaryLabel("Reindex");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJob);

    presJob.setJobType(JobType.REINDEX);

    presJob = this.preservationPlanningClientService.create(presJob);

    this.preservationPlanningClientService.start(presJob.getResId());
    this.waitPreservationJobIsCompleted(presJob.getResId());
    this.waitUntilAllAipAreIndexed();

    presJob = this.preservationPlanningClientService.findOne(presJob.getResId());

    final List<ArchivalInfoPackage> listAip = this.aipClientService.findAll();

    final long aipIgnored = listAip.stream()
            .filter(aip -> aip.getInfo().getStatus() != PackageStatus.STORED && aip.getInfo().getStatus() != PackageStatus.COMPLETED).count();

    final List<JobExecution> executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());
    final JobExecution jobExecution = executionList.stream().max(Comparator.comparing(JobExecution::getRunNumber))
            .orElseThrow(NoSuchElementException::new);

    final List<JobExecutionReport> reports = this.jobExecutionPlanningClientService.findReport(presJob.getResId(), jobExecution.getResId());
    assertEquals(1, reports.size());
    assertEquals(reports.get(0).getTotalItems(), listAip.size());
    assertEquals(reports.get(0).getProcessedItems(), listAip.size());
    assertEquals(reports.get(0).getIgnoredItems(), aipIgnored);

    assertEquals(reports.get(0).getInErrorItems() + reports.get(0).getProcessedItems(), listAip.size() - aipIgnored);

    // check number of index created
    final RestCollection<ArchiveMetadata> aipIndex = this.dataMgmtClientService.listAllPublicAIP();

    assertEquals(reports.get(0).getProcessedItems(), aipIndex.getPage().getTotalItems());
  }

  @Override
  protected void deleteFixtures() {
    this.clearPreservationJobFixtures();
  }
}

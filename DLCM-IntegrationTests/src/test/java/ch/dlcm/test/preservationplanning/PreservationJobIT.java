/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - PreservationJobIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preservationplanning;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.rest.Result;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preservation.JobExecution;
import ch.dlcm.model.preservation.JobExecutionReport;
import ch.dlcm.model.preservation.JobRecurrence;
import ch.dlcm.model.preservation.JobScheduling;
import ch.dlcm.model.preservation.JobType;
import ch.dlcm.model.preservation.PreservationJob;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.datamgmt.DataMgmtClientService;
import ch.dlcm.service.preservationplanning.JobExecutionPlanningClientService;
import ch.dlcm.service.preservationplanning.PreservationPlanningClientService;
import ch.dlcm.test.DLCMTestConstants;

@Disabled("Run these tests manually")
class PreservationJobIT extends AbstractPreservationPlanningIT {

  @Autowired
  public PreservationJobIT(Environment env, SudoRestClientTool restClientTool, PreservationPlanningClientService preservationPlanningClientService,
          JobExecutionPlanningClientService jobExecutionPlanningClientService, DataMgmtClientService dataMgmtClientService, AIPClientService aipClientService) {
    super(env, restClientTool, preservationPlanningClientService, jobExecutionPlanningClientService, dataMgmtClientService, aipClientService);
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void createJobInvalidDayOfMonthInScheduleForYearlyTest() {
    final String reindexJobName = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");
    final PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJobName);
    presJob.setJobType(JobType.FIXITY);
    presJob.setJobRecurrence(JobRecurrence.YEARLY);
    presJob.setScheduling(new JobScheduling(null, null, 31, 4));

    PreservationJob savedJob = null;
    try {
      savedJob = this.preservationPlanningClientService.create(presJob);
      fail("An Http error response should be received");
    } catch (final HttpClientErrorException e) {
      assertTrue(e.getResponseBodyAsString().contains("The scheduling data is invalid: day: 31 not valid for month: 4"));
    }
    assertNull(savedJob);
    final PreservationJob presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());
    assertNull(presJob2);
  }

  @Test
  void createJobInvalidDayOfWeekScheduleDataTest() {
    final String reindexJobName = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");
    final PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJobName);
    presJob.setJobType(JobType.FIXITY);
    presJob.setJobRecurrence(JobRecurrence.WEEKLY);
    presJob.setScheduling(new JobScheduling(10, 10, null, null));

    PreservationJob savedJob = null;
    try {
      savedJob = this.preservationPlanningClientService.create(presJob);
    } catch (final HttpClientErrorException e) {
      fail("An Http error response should be received");
      assertTrue(e.getResponseBodyAsString().contains("The scheduling data is invalid: day of week: 10"));
    }
    assertNull(savedJob);
    final PreservationJob presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());
    assertNull(presJob2);
  }

  @Test
  void createJobInvalidHourScheduleDataTest() {
    final String reindexJobName = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");
    final PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJobName);
    presJob.setJobType(JobType.FIXITY);
    presJob.setJobRecurrence(JobRecurrence.DAILY);
    presJob.setScheduling(new JobScheduling(25, null, null, null));

    PreservationJob savedJob = null;
    try {
      savedJob = this.preservationPlanningClientService.create(presJob);
    } catch (final HttpClientErrorException e) {
      fail("An Http error response should be received");
      assertTrue(e.getResponseBodyAsString().contains("The scheduling data is invalid: hour: 25"));
    }
    assertNull(savedJob);
    final PreservationJob presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());
    assertNull(presJob2);
  }

  @Test
  void createJobMissingHourInScheduleForDailyTest() {
    final String reindexJobName = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");
    final PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJobName);
    presJob.setJobType(JobType.FIXITY);
    presJob.setJobRecurrence(JobRecurrence.DAILY);
    presJob.setScheduling(new JobScheduling(null, 10, null, null));

    PreservationJob savedJob = null;
    try {
      savedJob = this.preservationPlanningClientService.create(presJob);
      fail("An Http error response should be received");
    } catch (final HttpClientErrorException e) {
      assertTrue(e.getResponseBodyAsString().contains("The scheduling data is not valid for the recurrence level \\\"Daily\\\""));
    }
    assertNull(savedJob);
    final PreservationJob presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());
    assertNull(presJob2);
  }

  @Test
  void createJobScheduleTest() {

    /**
     * Jobs with ONCE recurrence do not have any scheduling
     */
    String reindexJobName = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");
    PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJobName);
    presJob.setJobType(JobType.FIXITY);
    presJob.setJobRecurrence(JobRecurrence.ONCE);
    presJob.setScheduling(new JobScheduling());
    presJob = this.preservationPlanningClientService.create(presJob);
    PreservationJob presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());
    assertNull(presJob2.getScheduling());

    /**
     * Jobs with DAILY recurrence do not have any scheduling value for day of week, day of month and
     * month
     */
    reindexJobName = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");
    presJob = new PreservationJob();
    presJob.setName(reindexJobName);
    presJob.setJobType(JobType.FIXITY);
    presJob.setJobRecurrence(JobRecurrence.DAILY);
    presJob.setScheduling(new JobScheduling(5, 5, 5, 5));
    presJob = this.preservationPlanningClientService.create(presJob);
    presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());
    assertNotNull(presJob2.getScheduling());
    assertNotNull(presJob2.getScheduling().getHour());
    assertNull(presJob2.getScheduling().getWeekDay());
    assertNull(presJob2.getScheduling().getMonthDay());
    assertNull(presJob2.getScheduling().getMonth());

    /**
     * Jobs with WEEKLY recurrence do not have any scheduling value for day of month and month
     */
    reindexJobName = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");
    presJob = new PreservationJob();
    presJob.setName(reindexJobName);
    presJob.setJobType(JobType.FIXITY);
    presJob.setJobRecurrence(JobRecurrence.WEEKLY);
    presJob.setScheduling(new JobScheduling(5, 5, 5, 5));
    presJob = this.preservationPlanningClientService.create(presJob);
    presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());
    assertNotNull(presJob2.getScheduling());
    assertNotNull(presJob2.getScheduling().getHour());
    assertNotNull(presJob2.getScheduling().getWeekDay());
    assertNull(presJob2.getScheduling().getMonthDay());
    assertNull(presJob2.getScheduling().getMonth());

    /**
     * Jobs with MONTHLY recurrence do not have any scheduling value for month and day of week
     */
    reindexJobName = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");
    presJob = new PreservationJob();
    presJob.setName(reindexJobName);
    presJob.setJobType(JobType.FIXITY);
    presJob.setJobRecurrence(JobRecurrence.MONTHLY);
    presJob.setScheduling(new JobScheduling(5, 5, 5, 5));
    presJob = this.preservationPlanningClientService.create(presJob);
    presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());
    assertNotNull(presJob2.getScheduling());
    assertNotNull(presJob2.getScheduling().getHour());
    assertNull(presJob2.getScheduling().getWeekDay());
    assertNotNull(presJob2.getScheduling().getMonthDay());
    assertNull(presJob2.getScheduling().getMonth());

    /**
     * Jobs with YEARLY recurrence do not have any scheduling value for day of week
     */
    reindexJobName = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");
    presJob = new PreservationJob();
    presJob.setName(reindexJobName);
    presJob.setJobType(JobType.FIXITY);
    presJob.setJobRecurrence(JobRecurrence.YEARLY);
    presJob.setScheduling(new JobScheduling(5, 5, 5, 5));
    presJob = this.preservationPlanningClientService.create(presJob);
    presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());
    assertNotNull(presJob2.getScheduling());
    assertNotNull(presJob2.getScheduling().getHour());
    assertNull(presJob2.getScheduling().getWeekDay());
    assertNotNull(presJob2.getScheduling().getMonthDay());
    assertNotNull(presJob2.getScheduling().getMonth());
  }

  @Test
  void createJobTest() {
    final String reindexJob = DLCMTestConstants.getRandomNameWithTemporaryLabel("create");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJob);
    presJob.setJobType(JobType.REINDEX);
    presJob.setJobRecurrence(JobRecurrence.ONCE);

    presJob = this.preservationPlanningClientService.create(presJob);
    final PreservationJob presJob2 = this.preservationPlanningClientService.findOne(presJob.getResId());

    assertEquals(presJob.getName(), presJob2.getName());
    assertEquals(presJob.getJobType(), presJob2.getJobType());
    this.assertEqualsWithoutNanoSeconds(presJob.getCreationTime(), presJob2.getCreationTime());
  }

  @Test
  void createStartJobTest() {
    final String reindexJob = DLCMTestConstants.getRandomNameWithTemporaryLabel("create and start");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJob);
    presJob.setJobType(JobType.REINDEX);
    presJob.setJobRecurrence(JobRecurrence.ONCE);
    presJob = this.preservationPlanningClientService.create(presJob);

    presJob = this.preservationPlanningClientService.findOne(presJob.getResId());

    this.preservationPlanningClientService.start(presJob.getResId());
    this.waitPreservationJobIsCompleted(presJob.getResId());
    this.waitUntilAllAipAreIndexed();

    final List<JobExecution> executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());
    assertEquals(1, executionList.size());
    assertEquals(PreservationJob.JobStatus.COMPLETED, executionList.get(0).getStatus());
  }

  @Test
  void executionReportTest() {

    final String reindexJob = DLCMTestConstants.getRandomNameWithTemporaryLabel("execution report job");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJob);
    presJob.setJobType(JobType.REINDEX);
    presJob.setJobRecurrence(JobRecurrence.ONCE);
    presJob = this.preservationPlanningClientService.create(presJob);

    presJob = this.preservationPlanningClientService.findOne(presJob.getResId());

    this.preservationPlanningClientService.start(presJob.getResId());
    this.waitPreservationJobIsCompleted(presJob.getResId());
    this.waitUntilAllAipAreIndexed();

    final List<JobExecution> executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());
    final JobExecution jobExecution = executionList.stream().max(Comparator.comparing(JobExecution::getRunNumber))
            .orElseThrow(NoSuchElementException::new);

    final List<ArchivalInfoPackage> listAip = this.aipClientService.findAll().stream()
            .filter(aip -> aip.getInfo().getStatus() == PackageStatus.COMPLETED).collect(Collectors.toList());

    final List<JobExecutionReport> reports = this.jobExecutionPlanningClientService.findReport(presJob.getResId(), jobExecution.getResId());
    assertEquals(1, reports.size());
    assertEquals(reports.get(0).getTotalItems(), listAip.size());
    assertEquals(reports.get(0).getProcessedItems(), listAip.size());
  }

  @Test
  void reStartNonRecurrenceJobTest() {
    final String reindexJob = DLCMTestConstants.getRandomNameWithTemporaryLabel("start non recurrence job");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJob);
    presJob.setJobType(JobType.REINDEX);
    presJob.setJobRecurrence(JobRecurrence.ONCE);
    presJob = this.preservationPlanningClientService.create(presJob);

    presJob = this.preservationPlanningClientService.findOne(presJob.getResId());

    this.preservationPlanningClientService.start(presJob.getResId());
    this.waitPreservationJobIsCompleted(presJob.getResId());
    this.waitUntilAllAipAreIndexed();

    List<JobExecution> executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());

    assertEquals(1, executionList.size());
    assertEquals(PreservationJob.JobStatus.COMPLETED, executionList.get(0).getStatus());

    presJob = this.preservationPlanningClientService.start(presJob.getResId());
    executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());

    assertEquals(1, executionList.size());
  }

  @Test
  void reStartRecurrenceJobTest() {
    final String reindexJob = DLCMTestConstants.getRandomNameWithTemporaryLabel("start recurrence job");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJob);
    presJob.setJobType(JobType.REPLICATION_CHECK);
    presJob.setJobRecurrence(JobRecurrence.WEEKLY);
    presJob = this.preservationPlanningClientService.create(presJob);

    presJob = this.preservationPlanningClientService.findOne(presJob.getResId());

    this.preservationPlanningClientService.start(presJob.getResId());
    this.waitPreservationJobIsCompleted(presJob.getResId());
    this.waitUntilAllAipAreIndexed();

    List<JobExecution> executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());

    assertEquals(1, executionList.size());
    assertEquals(PreservationJob.JobStatus.COMPLETED, executionList.get(0).getStatus());

    presJob = this.preservationPlanningClientService.start(presJob.getResId());
    executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());

    assertEquals(2, executionList.size());
  }

  @Test
  void resumeRecurrenceJobTest() {
    final String reindexJob = DLCMTestConstants.getRandomNameWithTemporaryLabel("resume recurrence job");

    PreservationJob presJob = new PreservationJob();
    presJob.setName(reindexJob);
    presJob.setJobType(JobType.REINDEX);
    presJob.setJobRecurrence(JobRecurrence.ONCE);
    presJob = this.preservationPlanningClientService.create(presJob);

    presJob = this.preservationPlanningClientService.findOne(presJob.getResId());

    this.preservationPlanningClientService.start(presJob.getResId());
    this.waitPreservationJobIsCompleted(presJob.getResId());
    this.waitUntilAllAipAreIndexed();

    final List<JobExecution> executionList = this.preservationPlanningClientService.getJobExecutions(presJob.getResId());

    assertEquals(1, executionList.size());
    assertEquals(PreservationJob.JobStatus.COMPLETED, executionList.get(0).getStatus());

    try {
      executionList.get(0).setStatus(PreservationJob.JobStatus.IN_ERROR);
      this.preservationPlanningClientService.updateExecutionJob(presJob.getResId(), executionList.get(0).getResId(),
              Map.of("status", PreservationJob.JobStatus.IN_ERROR));
    } catch (final HttpClientErrorException e) {
      fail("An Http METHOD_NOT_ALLOWED response should be received");
      assertEquals(HttpStatus.METHOD_NOT_ALLOWED, e.getStatusCode());
    }
    final Result result = this.preservationPlanningClientService.resume(presJob.getResId());
    assertEquals(Result.ActionStatus.NON_APPLICABLE, result.getStatus());
  }

}

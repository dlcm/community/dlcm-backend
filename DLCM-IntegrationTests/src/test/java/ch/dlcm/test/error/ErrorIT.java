/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ErrorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.error;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.ResourceName;

@ActiveProfiles({"client-sudo", "test"})
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = { "spring.cloud.bootstrap.enabled=true" })
class ErrorIT {

  private static final Logger log = LoggerFactory.getLogger(ErrorIT.class);

  @Autowired
  protected SudoRestClientTool restClientTool;

  @Value("${dlcm.module.admin.url}")
  private String adminUrl;

  @Value("${dlcm.module.preingest.url}")
  private String ingestionUrl;

  @Test
  void badRequest() {
    final String url = this.ingestionUrl + "/" + ResourceName.DEPOSIT;
    try {
      final RestTemplate restTemplate = this.restClientTool.getClient();
      restTemplate.postForObject(url, null, String.class);
      fail("A BAD_REQUEST should be received");
    } catch (final HttpClientErrorException e) {
      log.info("400 test : response body : " + e.getResponseBodyAsString());
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void forbidden() {
    final String url = this.adminUrl + "/" + ResourceName.ORG_UNIT;
    try {
      final RestTemplate restTemplate = this.restClientTool.getClient();
      restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(new OrganizationalUnit()), OrganizationalUnit.class);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      log.info("403 test : response body : " + e.getResponseBodyAsString());
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void methodNotAllowed() {
    final String url = this.adminUrl;
    try {
      final RestTemplate restTemplate = this.restClientTool.getClient();
      restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(""), String.class);
      fail("A METHOD_NOT_ALLOWED Http response should be received");
    } catch (final HttpClientErrorException e) {
      log.info("405 test : response body : " + e.getResponseBodyAsString());
      assertEquals(HttpStatus.METHOD_NOT_ALLOWED, e.getStatusCode());
    }
  }

  @Test
  void notFound() {
    final String url = this.adminUrl + "doesntexists";
    try {
      final RestTemplate restTemplate = this.restClientTool.getClient();
      restTemplate.getForObject(url, String.class);
      fail("A NOT_FOUND Http response should be received");
    } catch (final HttpClientErrorException e) {
      log.info("404 test : response body : " + e.getResponseBodyAsString());
      assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
    }
  }

  @Test
  void unauthorized() {
    try {
      final URL url = new URL(this.adminUrl);
      String port = "";
      final int portNumber = url.getPort();
      if (portNumber != -1) {
        port = String.valueOf(portNumber);
      }
      final String urlWithoutUserInfo = url.getProtocol() + "://" + url.getHost() + ":" + port + url.getPath();
      final RestTemplate restTemplate = new RestTemplate();
      restTemplate.getForObject(urlWithoutUserInfo, String.class);
      fail("An UNAUTHORIZED Http response should be received");
    } catch (final HttpClientErrorException e) {
      log.info("401 test : response body : " + e.getResponseBodyAsString());
      assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
    } catch (final MalformedURLException e) {
      e.printStackTrace();
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.SudoRestClientTool;

@ActiveProfiles({ "client-sudo", "test" })
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = { "spring.cloud.bootstrap.enabled=true" })
@EnableAutoConfiguration(exclude = SecurityAutoConfiguration.class)
public abstract class AbstractIT {

  protected static final String DEFAULT_DESCRIPTION = "Default description";
  protected static final String DEFAULT_TITLE = "Default title";
  protected static final String DEFAULT_TITLE_TO_UPDATE = "Archive to update";
  protected static final String DEFAULT_TITLE_TO_DOWNLOAD = "Archive to download";
  protected static final String DEFAULT_TITLE_TO_DOWNLOAD_HEDERA = "Archive to download Hedera";
  protected static final String DEFAULT_DESCRIPTION_TO_UPDATE = "An archive that gets updated each time integration tstatic static ests are run";

  protected static final String REFERENCE_FILE = "http://www.unige.ch/rectorat/static/budget-2013.pdf";
  protected static final String YARETA_DATA = "deposit/dlcm/Yareta-small.zip";

  protected static final long REFERENCE_FILE_SIZE = 917380;
  protected static final String NO_ONE = "no_one";

  protected SudoRestClientTool restClientTool;

  protected Environment env;

  public AbstractIT(Environment env, SudoRestClientTool restClientTool) {
    this.env = env;
    this.restClientTool = restClientTool;
  }

  @BeforeEach
  protected void setup() {
    this.setUser();
    this.createFixtures();
  }

  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  protected void createFixtures() {
    // Do nothing
  }

  /**
   * Delete fixtures created by tests
   */
  protected abstract void deleteFixtures();

  /***********************************/

  protected String getNewArchiveToUpdateTitle(String currentTitle) {
    if (!currentTitle.contains("[")) {
      return currentTitle + " [0]";
    } else {
      final int currentUpdateIndex = this.getNumberBetweenSquareBrackets(currentTitle);
      return currentTitle.replace("[" + currentUpdateIndex + "]", "[" + (currentUpdateIndex + 1) + "]");
    }
  }

  protected void assertEqualsWithoutNanoSeconds(OffsetDateTime time1, OffsetDateTime time2) {
    assertEquals(time1.withNano(0), time2.withNano(0));
  }

  protected void assertEqualsWithoutNanoSeconds(String message, OffsetDateTime time1, OffsetDateTime time2) {
    assertEquals(time1.withNano(0), time2.withNano(0), message);
  }

  protected void assertFieldContainsValidationErrorMessage(HttpClientErrorException e, String fieldName, String expectedErrorMessage) {
    List<String> errorMessages = this.getFieldValidationErrors(e, fieldName);
    assertTrue(errorMessages.contains(expectedErrorMessage),
            errorMessages + " doesn't contain the message '" + expectedErrorMessage + "'");

  }

  protected List<String> getFieldValidationErrors(HttpClientErrorException httpClientErrorException, String fieldName) {
    List<Map<String, Object>> validationErrors = this.getValidationErrors(httpClientErrorException);
    if (validationErrors != null) {
      Optional<Map<String, Object>> fieldValidationErrorOpt = validationErrors.stream().filter(map -> map.get("fieldName").equals(fieldName))
              .findFirst();
      if (fieldValidationErrorOpt.isPresent()) {
        return (List<String>) fieldValidationErrorOpt.get().get("errorMessages");
      } else {
        return new ArrayList<>();
      }
    }
    return new ArrayList<>();
  }

  protected List<Map<String, Object>> getValidationErrors(HttpClientErrorException httpClientErrorException) {
    try {
      String responseBody = httpClientErrorException.getResponseBodyAsString();
      Map<String, Object> responseJsonMap = new ObjectMapper().readValue(responseBody, Map.class);
      return (List<Map<String, Object>>) responseJsonMap.get("validationErrors");
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("unable to get validationErrors from HttpClientErrorException", e);
    }
  }

  protected int getNumberBetweenSquareBrackets(String text) {
    int startBracketIndex = text.lastIndexOf('[');
    int endBracketIndex = text.lastIndexOf(']');
    String updateIndexStr = text.substring(startBracketIndex + 1, endBracketIndex);
    return Integer.parseInt(updateIndexStr);
  }

  protected void runParallelCalls(Runnable runnable, int nbThreads, int nbCalls) throws InterruptedException {
    final CountDownLatch countDownLatch = new CountDownLatch(nbCalls);
    final ExecutorService executorService = Executors.newFixedThreadPool(nbThreads);
    for (int i = 0; i < nbCalls; i++) {
      executorService.submit(() -> {
        try {
          runnable.run();
          countDownLatch.countDown();
        } catch (Throwable t) {
          t.printStackTrace();
        }
      });
    }
    countDownLatch.await();
  }

  @AfterEach
  public void tearDown() {
    this.deleteFixtures();
    this.restClientTool.exitSudo();
  }
}

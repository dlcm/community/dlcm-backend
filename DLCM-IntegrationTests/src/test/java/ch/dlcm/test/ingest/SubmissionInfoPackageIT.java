/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - SubmissionInfoPackageIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.ingest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.net.URI;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.oais.SipDataFile;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.ingest.SIPClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.OrgUnitITService;

class SubmissionInfoPackageIT extends AbstractIT {

  private final static String REFERENCE_FILE = "http://www.unige.ch/rectorat/static/budget-2013.pdf";

  private SIPClientService sipClientService;

  private OrgUnitITService orgUnitITService;

  @Autowired
  public SubmissionInfoPackageIT(Environment env, SudoRestClientTool restClientTool, SIPClientService sipClientService,
          OrgUnitITService orgUnitITService) {
    super(env, restClientTool);
    this.sipClientService = sipClientService;
    this.orgUnitITService = orgUnitITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationOnClosedOrganizationalUnitTest() {

    boolean success = true;
    try {
      this.createRemoteSipWithDataFile(OrganizationalUnitStatus.CLOSED);
    } catch (final Exception e) {
      success = false;
    }
    assertFalse(success);
  }

  @Test
  void creationTest() {
    // Create SIP
    this.createRemoteSipWithDataFile(OrganizationalUnitStatus.OPEN);
  }

  @Test
  void deleteTest() {
    // Create SIP
    final SubmissionInfoPackage remoteSip = this.createRemoteSipWithDataFile(OrganizationalUnitStatus.OPEN);

    // Test if data file is present
    final int depositListSize = this.sipClientService.getSipDataFile(remoteSip.getResId()).size();
    assertEquals(1, depositListSize);

    // Remove the data file
    final String sipDataFileId = remoteSip.getDataFiles().get(0).getResId();
    this.sipClientService.removeSipDataFile(remoteSip.getResId(), sipDataFileId);

    // Test if data file is absent
    assertEquals(0, this.sipClientService.getSipDataFile(remoteSip.getResId()).size());

  }

  @Test
  void deleteWhenReadyTest() {
    // Create SIP
    final SubmissionInfoPackage remoteSip = this.createRemoteSipWithDataFile(OrganizationalUnitStatus.OPEN);

    // Wait until data file downloaded
    this.waitDataFileIsReady(remoteSip.getResId(), 1);

    // Remove the data file
    final String sipDataFileId = remoteSip.getDataFiles().get(0).getResId();
    this.sipClientService.removeSipDataFile(remoteSip.getResId(), sipDataFileId);

    // Test if data file is absent
    final int depositListSize = this.sipClientService.getSipDataFile(remoteSip.getResId()).size();
    assertEquals(0, depositListSize);

  }

  @Disabled("Will be fixed with DLCM-2099")
  @Test
  void updateOnlyNameTestAsRoot() {
    // Running the test as root bypass the SubmissionInfoPackagePermissionService
    this.restClientTool.sudoRoot();
    this.updateOnlyNameTest();
    this.restClientTool.exitSudo();
  }

  @Disabled("Will be fixed with DLCM-2099")
  @Test
  void updateOnlyNameTestAsAdmin() {
    // Running the test as admin trigger the SubmissionInfoPackagePermissionService
    // Manager role is needed on the organizational unit
    this.updateOnlyNameTest();
  }

  private void updateOnlyNameTest() {
    final String newName = "New name";
    final SubmissionInfoPackage localSip = this.createLocalSip(OrganizationalUnitStatus.OPEN);
    final SubmissionInfoPackage remoteSip1 = this.sipClientService.create(localSip);
    this.sipClientService.changeName(remoteSip1.getResId(), newName);
    final SubmissionInfoPackage remoteSip2 = this.sipClientService.findOne(remoteSip1.getResId());
    assertNotEquals(newName, remoteSip1.getInfo().getName());
    assertEquals(newName, remoteSip2.getInfo().getName());
    assertEquals(remoteSip1.getInfo().getDescription(), remoteSip2.getInfo().getDescription());
    assertEquals(remoteSip1.getInfo().getOrganizationalUnitId(), remoteSip2.getInfo().getOrganizationalUnitId());
    assertEquals(remoteSip1.getInfo().getStatus(), remoteSip2.getInfo().getStatus());
    assertEquals(remoteSip1.getInfo().getStatusMessage(), remoteSip2.getInfo().getStatusMessage());
    assertEquals(remoteSip1.getInfo().getLicenseId(), remoteSip2.getInfo().getLicenseId());
    assertEquals(remoteSip1.getInfo().getMetadataVersion(), remoteSip2.getInfo().getMetadataVersion());
    assertEquals(remoteSip1.getInfo().getAccess(), remoteSip2.getInfo().getAccess());
    assertEquals(remoteSip1.getInfo().hasEmbargo(), remoteSip2.getInfo().hasEmbargo());
  }

  private boolean checkDataFileListIsCompleted(List<SipDataFile> dataFileList) {
    for (final SipDataFile df : dataFileList) {
      if (df.getStatus() != DataFileStatus.READY) {
        return false;
      }
    }
    return true;
  }

  private SubmissionInfoPackage createLocalSip(OrganizationalUnitStatus orgUnitStatus) {
    final SubmissionInfoPackage localSip = new SubmissionInfoPackage();
    localSip.getInfo().setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("SIP "));
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode.PERMANENT,
            orgUnitStatus);
    localSip.getInfo().setOrganizationalUnitId(organizationalUnit.getResId());
    return localSip;
  }

  private SubmissionInfoPackage createRemoteSipWithDataFile(OrganizationalUnitStatus orgUnitStatus) {
    SubmissionInfoPackage localSip = this.createLocalSip(orgUnitStatus);
    SubmissionInfoPackage remoteSip = this.sipClientService.create(localSip);
    final String sipId = remoteSip.getResId();

    // Create a SIP data file object
    final SipDataFile localSipDataFile = new SipDataFile(localSip);
    localSipDataFile.setSourceData(URI.create(REFERENCE_FILE));
    localSipDataFile.setDataCategory(DataCategory.Package);
    localSipDataFile.setDataType(DataCategory.InformationPackage);
    localSipDataFile.getVirusCheck().setCheckDate(OffsetDateTime.now());
    localSipDataFile.setStatus(DataFileStatus.RECEIVED);
    localSipDataFile.setInfoPackage(localSip);

    this.sipClientService.addSipDataFile(sipId, localSipDataFile);
    final SipDataFile remoteSipDataFile = this.sipClientService.getSipDataFile(sipId).get(0);

    // Set the retrieved data file to the SIP object
    remoteSip.setDataFiles(Collections.singletonList(remoteSipDataFile));

    // Test the creation
    assertEquals(localSip.getResId(), remoteSip.getResId());
    assertEquals(localSipDataFile.getResId(), remoteSipDataFile.getResId());
    assertEquals(localSipDataFile.getInfoPackage().getResId(), remoteSipDataFile.getInfoPackage().getResId());

    return remoteSip;

  }

  private void waitDataFileIsReady(String sipId, int dataFileNumber) {
    List<SipDataFile> dataFileList = this.sipClientService.getSipDataFile(sipId);

    // Test if data file is present
    assertEquals(dataFileList.size(), dataFileNumber);

    int count = 0;
    while (!this.checkDataFileListIsCompleted(dataFileList)) {
      DLCMTestConstants.failAfterNAttempt(count, "Not all data files in SIP " + sipId + " have the Ready status");
      dataFileList = this.sipClientService.getSipDataFile(sipId);
      count++;
    }
  }

  @Override
  protected void deleteFixtures() {
  }

}

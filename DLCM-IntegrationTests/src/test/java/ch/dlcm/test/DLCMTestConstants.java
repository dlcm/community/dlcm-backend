/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DLCMTestConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.concurrent.ThreadLocalRandom;

import ch.unige.solidify.util.SolidifyTime;

public class DLCMTestConstants {
  public static final int TRIES = 180;
  public static final String PRESERVATION_POLICY_TEST = "PreservationPolicy with years retention";
  public static final String PRESERVATION_POLICY_FOREVER_TEST = "PreservationPolicy with forever retention";
  public static final String PRESERVATION_POLICY_MIN_RETENTION_TEST = "PreservationPolicy with minimum retention";
  public static final String SUBMISSION_POLICY_WITHOUT_APPROVAL_WITHOUT_AGREEMENT_TEST = "SubmissionPolicy without approval without agreement";
  public static final String SUBMISSION_POLICY_WITH_APPROVAL_WITHOUT_AGREEMENT_TEST = "SubmissionPolicy with approval without agreement";
  public static final String SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST = "SubmissionPolicy with approval with agreement for first deposit";
  public static final String SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST = "SubmissionPolicy with approval with agreement for each deposits";
  public static final String SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST = "SubmissionPolicy without approval with agreement for first deposit";
  public static final String SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST = "SubmissionPolicy without approval with agreement for each deposits";
  public static final String SUBMISSION_AGREEMENT_TEST_NAME = "SubmissionAgreement";
  public static final String PERMANENT_TEST_DATA_LABEL = "[Permanent Test Data] ";
  public static final String TEMPORARY_TEST_DATA_LABEL = "[Temporary Test Data] ";
  public static final String ARCHIVAL_UNIT_LABEL = "Archival Unit";

  public static final String DEPOSIT_WITH_SENSITIVE_DATA = "Deposit with sensitive data";
  public static final String DEPOSIT_WITH_SENSITIVE_DATA_CRIMSON = DEPOSIT_WITH_SENSITIVE_DATA + " CRIMSON";
  public static final String ARCHIVAL_COLLECTION_LABEL = "Archival Collection";
  public static final String PERMANENT_DEPOSIT = "Permanent Deposit";
  public static final String DEPOSIT_BY_VERSION = "Deposit v";
  public static final String SAMPLE_DUA_FILE = "sample-dua-file.pdf";
  public static final String THUMBNAIL_FILE = "geneve.jpg";
  public static final String README_FILE = "sample-readme-file.txt";
  public static final String SAMPLE_SUBMISSION_AGREEMENT_FILE = "sample-submission-agreement-file.pdf";
  public static final String CLOSED_DEPOSIT = "Closed Deposit";
  public static final String THIRD_PARTY_USER_ID = "THIRD-PARTY-USER";
  public static final String FOR_NO_ONE = "for no one";

  /**
   * Test ORCID account on Sandbox
   * dlcm-test@mailinator.com
   */
  public static final String ORCID_VALUE = "0009-0002-9189-3897";

  public enum OrganizationalUnitStatus {
    CLOSED, OPEN
  }

  public enum PersistenceMode {
    PERMANENT, TEMPORARY
  }

  public static void failAfterNAttempt(int count, String failMessage) {
    DLCMTestConstants.failAfterNAttempt(count, 1, failMessage);
  }

  public static void failAfterNAttempt(int count, int waitInSeconds, String failMessage) {
    if (count > DLCMTestConstants.TRIES) {
      fail(failMessage);
    }
    SolidifyTime.waitInSeconds(waitInSeconds);
  }

  public static String getNameWithPermanentLabel(String name) {
    return PERMANENT_TEST_DATA_LABEL + name;
  }

  public static String getRandomNameWithTemporaryLabel(String name) {
    return TEMPORARY_TEST_DATA_LABEL + DLCMTestConstants.getRandomName(name);
  }

  public static String getRandomName(String name) {
    return name + ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE);
  }

  public static String getRandomOrcId() {
    String orcId = "";
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        orcId = orcId + ThreadLocalRandom.current().nextInt(10);
      }
      orcId = orcId + "-";
    }
    return orcId.substring(0, orcId.length() - 1);
  }
}

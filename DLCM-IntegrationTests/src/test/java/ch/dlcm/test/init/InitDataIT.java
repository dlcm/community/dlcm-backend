/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - InitDataIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.init;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionAgreementType;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.service.admin.ArchiveACLClientService;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.service.admin.PreservationPolicyClientService;
import ch.dlcm.service.admin.RoleClientService;
import ch.dlcm.service.admin.SubmissionAgreementClientService;
import ch.dlcm.service.admin.SubmissionPolicyClientService;
import ch.dlcm.service.admin.UserClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.ArchiveAclITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.InstitutionITService;
import ch.dlcm.test.service.OrgUnitITService;
import ch.dlcm.test.service.PersonITService;

@Order(Integer.MIN_VALUE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class InitDataIT extends AbstractIT {
  private static final Logger logger = LoggerFactory.getLogger(InitDataIT.class);

  private static final int AIU_NUM = 3;
  private static final String YARETA_FILES_DATA = "deposit/dlcm/Yareta.zip";
  private static final String HEDERA_FILES_DATA = "deposit/dlcm/hedera.zip";

  private final PersonITService personITService;
  private final InstitutionITService institutionITService;
  private final OrgUnitITService orgUnitITService;
  private final AipITService aipITService;
  private final DepositITService depositITService;
  private final DepositClientService depositClientService;
  private final RoleClientService roleClientService;
  private final UserClientService userClientService;
  private final AIPClientService aipClientService;
  private final SubmissionPolicyClientService submissionPolicyClientService;
  private final PreservationPolicyClientService preservationPolicyClientService;
  private final SubmissionAgreementClientService submissionAgreementClientService;
  private final ArchiveACLClientService archiveACLClientService;
  private final ArchiveAclITService archiveAclITService;
  private final OrganizationalUnitClientService organizationalUnitClientService;

  @Autowired
  public InitDataIT(
          Environment env,
          SudoRestClientTool restClientTool,
          PersonITService personITService,
          InstitutionITService institutionITService,
          OrgUnitITService orgUnitITService,
          AipITService aipITService,
          DepositITService depositITService,
          AIPClientService aipClientService,
          DepositClientService depositClientService,
          RoleClientService roleClientService,
          UserClientService userClientService,
          SubmissionPolicyClientService submissionPolicyClientService,
          PreservationPolicyClientService preservationPolicyClientService,
          SubmissionAgreementClientService submissionAgreementClientService,
          ArchiveACLClientService archiveACLClientService,
          ArchiveAclITService archiveAclITService, OrganizationalUnitClientService organizationalUnitClientService) {
    super(env, restClientTool);
    this.personITService = personITService;
    this.institutionITService = institutionITService;
    this.orgUnitITService = orgUnitITService;
    this.aipITService = aipITService;
    this.depositITService = depositITService;
    this.aipClientService = aipClientService;
    this.depositClientService = depositClientService;
    this.roleClientService = roleClientService;
    this.userClientService = userClientService;
    this.submissionPolicyClientService = submissionPolicyClientService;
    this.preservationPolicyClientService = preservationPolicyClientService;
    this.submissionAgreementClientService = submissionAgreementClientService;
    this.archiveACLClientService = archiveACLClientService;
    this.archiveAclITService = archiveAclITService;
    this.organizationalUnitClientService = organizationalUnitClientService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Order(10)
  @Test
  void verifyUserPerson() {
    this.initUsers();
    final List<Person> personList = this.personITService.listPeople();
    Map<String, String> properties = new HashMap<>();
    properties.put("firstName", DLCMTestConstants.PERMANENT_TEST_DATA_LABEL);
    final List<User> userList = this.userClientService.searchByProperties(properties);
    final List<String> roles = new ArrayList<>(Arrays.asList("ADMIN", "USER", "ROOT"));
    for (String role : roles) {
      List<Person> rolePersons = personList.stream()
              .filter(person -> person.getFirstName().toLowerCase().contains((DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + role).toLowerCase()))
              .toList();
      List<User> roleUsers = userList.stream()
              .filter(user -> user.getFirstName().toLowerCase().contains((DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + role).toLowerCase()))
              .toList();
      assertEquals(1, rolePersons.size(), "There must be exactly one " + role + " person");
      assertEquals(1, roleUsers.size(), "There must be exactly one " + role + " user");
      assertNotNull(roleUsers.get(0).getPerson(), "The role " + role + " must have a person connected to its user");
      assertEquals(role, roleUsers.get(0).getApplicationRole().getResId(), "User for " + role + " has not a " + role + " role");
    }
  }

  @Order(15)
  @Test
  void verifySubmissionAgreements() {
    this.initSubmissionAgreement();
    List<SubmissionAgreement> list = this.submissionAgreementClientService.searchByProperties(Map.of("title", DLCMTestConstants.PERMANENT_TEST_DATA_LABEL));
    assertEquals(1, list.size());
  }

  @Order(16)
  @Test
  void verifySubmissionPolicies() {
    this.initSubmissionPolicy();
    List<SubmissionPolicy> list = this.submissionPolicyClientService.searchByProperties(Map.of("name", DLCMTestConstants.PERMANENT_TEST_DATA_LABEL));
    assertEquals(6, list.size());
  }

  @Order(17)
  @Test
  void verifyPreservationPolicies() {
    this.initPreservationPolicy();
    List<PreservationPolicy> list = this.preservationPolicyClientService.searchByProperties(Map.of("name", DLCMTestConstants.PERMANENT_TEST_DATA_LABEL));
    assertEquals(3, list.size());
  }

  @Order(20)
  @Test
  void verifyOrganizationalUnits() {
    this.initPersonOrgUnitRole();
    this.createSecondOrgUnit();
    this.createOrgUnitForNoOne();
    List<OrganizationalUnit> list = this.orgUnitITService.findOrganizationalUnits(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL);
    assertEquals(14, list.size());
  }

  @Order(30)
  @Test
  void verifyInstitutions() {
    this.createInstitutionForNoOne(PersistenceMode.PERMANENT);
    List<Institution> list = this.institutionITService.findInstitutions(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL);
    assertEquals(6, list.size());
  }

  @Order(40)
  @Test
  void verifyPermanentDeposits() {
    this.initPermanentDeposit();
    this.initArchiveToUpdate();
    this.initArchiveToDownload();
    this.initCollectionData();
    this.initArchiveWithDua();
    this.initClosedArchiveForEachRole(false);
    this.initClosedArchiveForEachRole(true);
    List<Deposit> list = this.depositITService.findDeposits(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + DLCMTestConstants.ARCHIVAL_UNIT_LABEL);
    assertEquals(AIU_NUM, list.size());
    list = this.depositITService.findDeposits(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + DLCMTestConstants.ARCHIVAL_COLLECTION_LABEL);
    assertEquals(1, list.size());
    list = this.depositITService.findDeposits(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + DLCMTestConstants.PERMANENT_DEPOSIT);
    assertEquals(1, list.size());
    list = this.depositITService.findDeposits(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + DLCMTestConstants.DEPOSIT_WITH_SENSITIVE_DATA);
    assertEquals(4, list.size());
  }

  @Order(50)
  @Test
  void verifyDepositByVersion() {
    for (DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      List<Deposit> depositList = this.depositITService.findDeposits(DLCMTestConstants.DEPOSIT_BY_VERSION + version.toString());
      if (depositList.isEmpty()) {
        depositList.add(this.createDeposit(version));
      }
      Deposit deposit = this.waitDepositIsCompleted(depositList.get(0).getResId());
      assertEquals(version, deposit.getMetadataVersion());
      assertEquals(DepositStatus.COMPLETED, deposit.getStatus());
      this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
    }
    List<Deposit> list = this.depositITService.findDeposits(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + DLCMTestConstants.DEPOSIT_BY_VERSION);
    assertEquals(DLCMMetadataVersion.values().length, list.size());
  }

  @Order(60)
  @Test
  void verifyClosedArchiveInOrgUnitForNoOne() {
    final String title = DLCMTestConstants.CLOSED_DEPOSIT + " " + DLCMTestConstants.FOR_NO_ONE;
    if (!this.aipITService.isExactlyOneAipInStatus(title, PackageStatus.COMPLETED)) {
      this.initClosedArchiveInOrgUnitForNoOne();
    }
    assertTrue(this.aipITService.isExactlyOneAipInStatus(title, PackageStatus.COMPLETED),
            "Unable to create closed archive in orgUnit for no one");
  }

  @Order(70)
  @Test
  void verifyArchiveAcl() {
    try {
      this.archiveAclITService.getPermanentAclForThirdPartyUserOnClosedArchive();
    } catch (SolidifyResourceNotFoundException e) {
      this.createPermanentArchiveACL();
      assertDoesNotThrow(this.archiveAclITService::getPermanentAclForThirdPartyUserOnClosedArchive);
    }
  }

  private void initCollectionData() {
    // Create Archival Unit
    for (int i = 1; i <= AIU_NUM; i++) {
      final String title = DLCMTestConstants.ARCHIVAL_UNIT_LABEL + " " + i;
      if (this.aipITService.isExactlyOneAipInStatus(title, PackageStatus.IN_ERROR)) {
        this.aipITService.resumeAIP(title);
      } else if (this.aipITService.isNoAip(title)) {
        this.createPermanentArchives(title, DEFAULT_DESCRIPTION);
      }
    }
    // Create Archival collection
    final String title = DLCMTestConstants.ARCHIVAL_COLLECTION_LABEL;
    if (this.aipITService.isExactlyOneAipInStatus(title, PackageStatus.IN_ERROR)) {
      logger.warn("Archival Collection IN_ERROR trying to resume");
      this.aipITService.resumeAIP(title);
    } else if (this.aipITService.isNoAip(title)) {
      Deposit deposit = this.createPermanentRemoteDUADepositWithContributor(title, DEFAULT_DESCRIPTION);
      deposit = this.depositITService.createRemoteDepositWithData(deposit);
      this.depositITService.createAIC(deposit, AIU_NUM, false);
      this.depositITService.approveDeposit(deposit);
      this.depositITService.waitDepositIsCompleted(deposit.getResId());
      this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
    }
  }

  private void initArchiveWithDua() {
    for (DataTag dataTag : List.of(DataTag.YELLOW, DataTag.ORANGE, DataTag.RED, DataTag.CRIMSON)) {
      final String title = DLCMTestConstants.DEPOSIT_WITH_SENSITIVE_DATA + " " + dataTag.name();
      if (this.aipITService.isExactlyOneAipInStatus(title, PackageStatus.IN_ERROR)) {
        this.aipITService.resumeAIP(title);
      } else if (this.aipITService.isNoAip(title)) {
        DataUsePolicy dataUsePolicy = switch (dataTag) {
          case YELLOW -> DataUsePolicy.CLICK_THROUGH_DUA;
          case ORANGE, RED -> DataUsePolicy.SIGNED_DUA;
          case CRIMSON -> DataUsePolicy.EXTERNAL_DUA;
          default -> throw new SolidifyRuntimeException("Wrong data tag for testing");
        };
        Deposit deposit = this.depositITService.createRemoteDepositWithDuaType(PersistenceMode.PERMANENT, title, DEFAULT_DESCRIPTION, dataTag,
                dataUsePolicy, Access.CLOSED, false);
        this.addDataAndValidatePermanentDeposit(deposit);
      }
    }
  }

  private void initClosedArchiveForEachRole(boolean inheritedRole) {
    // Add archives for inherited roles
    for (Role role : this.roleClientService.findAll()) {
      final String inheritedString = inheritedRole ? " inherited " : " ";
      final String title = DLCMTestConstants.CLOSED_DEPOSIT + inheritedString + role.getResId();
      if (this.aipITService.isExactlyOneAipInStatus(title, PackageStatus.IN_ERROR)) {
        this.aipITService.resumeAIP(title);
      } else if (this.aipITService.isNoAip(title)) {
        Deposit deposit = this.depositITService.createRemoteDepositWithAccessLevel(PersistenceMode.PERMANENT, title, DEFAULT_DESCRIPTION,
                DataTag.GREEN, DataUsePolicy.NONE, Access.CLOSED, false, role.getResId(), inheritedRole);
        this.addDataAndValidatePermanentDeposit(deposit);
      }
    }
  }

  private void initClosedArchiveInOrgUnitForNoOne() {
    final String title = DLCMTestConstants.CLOSED_DEPOSIT + " " + DLCMTestConstants.FOR_NO_ONE;
    if (this.aipITService.isExactlyOneAipInStatus(title, PackageStatus.IN_ERROR)) {
      this.aipITService.resumeAIP(title);
    } else if (this.aipITService.isNoAip(title)) {
      Deposit deposit = this.depositITService.createLocalDeposit(PersistenceMode.PERMANENT, title, "");
      final OrganizationalUnit orgUnit = this.orgUnitITService.getOrgUnitForNoOne(PersistenceMode.PERMANENT);
      deposit.setOrganizationalUnitId(orgUnit.getResId());
      deposit.setAccess(Access.CLOSED);
      deposit.setDataSensitivity(DataTag.CRIMSON);
      deposit.setDataUsePolicy(DataUsePolicy.EXTERNAL_DUA);
      this.depositITService.createRemoteDepositWithData(deposit);
      final Person person = this.personITService.getOrCreateRemotePerson();
      this.depositClientService.addContributor(deposit.getResId(), person.getResId());
      this.addDataAndValidatePermanentDeposit(deposit);
    }
  }

  private void initUsers() {
    // Make a REST call with test users, in order to present their tokens to the DLCM application and create their users
    this.restClientTool.sudoUser();
    this.submissionPolicyClientService.findAll();
    this.restClientTool.exitSudo();

    this.restClientTool.sudoThirdPartyUser();
    this.submissionPolicyClientService.findAll();
    this.restClientTool.exitSudo();

    this.restClientTool.sudoAdmin();
    this.submissionPolicyClientService.findAll();
    this.restClientTool.exitSudo();

    this.restClientTool.sudoRoot();
    this.submissionPolicyClientService.findAll();
    this.restClientTool.exitSudo();

    // Set last login time and IP for testing purposes
    final User user = this.userClientService.findAll().stream().filter(u -> u.getFirstName().contains(DLCMTestConstants.THIRD_PARTY_USER_ID)).findFirst()
            .orElseThrow(() -> new NoSuchElementException("Missing test user " + DLCMTestConstants.THIRD_PARTY_USER_ID));
    user.setLastLoginTime(OffsetDateTime.now());
    user.setLastLoginIpAddress("8.8.8.8");
    this.userClientService.update(user.getResId(), user);
  }

  private void initSubmissionAgreement() {
    final String submissionAgreementName = DLCMTestConstants.getNameWithPermanentLabel(DLCMTestConstants.SUBMISSION_AGREEMENT_TEST_NAME);
    if (this.submissionAgreementClientService.searchByProperties(Map.of("title", DLCMTestConstants.SUBMISSION_AGREEMENT_TEST_NAME)).isEmpty()) {
      final SubmissionAgreement submissionAgreement = new SubmissionAgreement();
      final Resource submissionAgreementFile = new ClassPathResource(DLCMTestConstants.SAMPLE_SUBMISSION_AGREEMENT_FILE);
      submissionAgreement.setTitle(submissionAgreementName);
      submissionAgreement.setVersion("1");
      this.submissionAgreementClientService.createSubmissionAgreementWithFile(submissionAgreement,
              submissionAgreementFile);
    }
  }

  private void initPreservationPolicy() {
    this.createPreservationPolicy(DLCMTestConstants.PRESERVATION_POLICY_TEST, 5 * DLCMConstants.DAYS_BY_YEAR);
    this.createPreservationPolicy(DLCMTestConstants.PRESERVATION_POLICY_FOREVER_TEST, 0);
    this.createPreservationPolicy(DLCMTestConstants.PRESERVATION_POLICY_MIN_RETENTION_TEST, 1);
  }

  private void initSubmissionPolicy() {
    final List<SubmissionAgreement> submissionAgreementList = this.submissionAgreementClientService
            .searchByProperties(Map.of("name", DLCMTestConstants.SUBMISSION_AGREEMENT_TEST_NAME));

    // Policies without submission agreement
    this.createSubmissionPolicy(DLCMTestConstants.SUBMISSION_POLICY_WITHOUT_APPROVAL_WITHOUT_AGREEMENT_TEST, false, Integer.MAX_VALUE,
            SubmissionAgreementType.WITHOUT, null);
    this.createSubmissionPolicy(DLCMTestConstants.SUBMISSION_POLICY_WITH_APPROVAL_WITHOUT_AGREEMENT_TEST, true, Integer.MAX_VALUE, SubmissionAgreementType.WITHOUT,
            null);

    // Policies with submission agreement
    if (!submissionAgreementList.isEmpty()) {
      this.createSubmissionPolicy(DLCMTestConstants.SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST, true, Integer.MAX_VALUE,
              SubmissionAgreementType.ON_FIRST_DEPOSIT, submissionAgreementList.get(0));
      this.createSubmissionPolicy(DLCMTestConstants.SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST, true, Integer.MAX_VALUE,
              SubmissionAgreementType.FOR_EACH_DEPOSIT, submissionAgreementList.get(0));

      this.createSubmissionPolicy(DLCMTestConstants.SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST, false, Integer.MAX_VALUE,
              SubmissionAgreementType.ON_FIRST_DEPOSIT,
              submissionAgreementList.get(0));
      this.createSubmissionPolicy(DLCMTestConstants.SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST, false, Integer.MAX_VALUE,
              SubmissionAgreementType.FOR_EACH_DEPOSIT,
              submissionAgreementList.get(0));
    }
  }

  private void initPersonOrgUnitRole() {
    // Needed to create org unit for each role as ApplicationRole User
    final List<Role> listRoles = this.roleClientService.findAll();
    for (final Role role : listRoles) {
      this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(
              PersistenceMode.PERMANENT,
              OrganizationalUnitStatus.OPEN,
              role.getName().toUpperCase(),
              AuthApplicationRole.USER_ID,
              true);
      this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(
              PersistenceMode.PERMANENT,
              OrganizationalUnitStatus.OPEN,
              role.getName().toUpperCase(),
              AuthApplicationRole.USER_ID,
              false);
    }
    // create orgUnit with submission approval and MANAGER as role for the user
    this.orgUnitITService.getOrCreateOrganizationalUnitWithApproval(
            PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN,
            Role.MANAGER_ID,
            AuthApplicationRole.USER_ID,
            true);
    this.orgUnitITService.getOrCreateOrganizationalUnitWithApproval(
            PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN,
            Role.MANAGER_ID,
            AuthApplicationRole.USER_ID,
            false);
  }

  private void initPermanentDeposit() {
    List<Deposit> depositList = this.depositClientService.searchByProperties(
            Collections.singletonMap("title", DLCMTestConstants.PERMANENT_DEPOSIT));
    if (depositList.isEmpty()) {
      final Deposit newDeposit = this.depositITService.createRemoteDeposit(PersistenceMode.PERMANENT,
              DLCMTestConstants.PERMANENT_DEPOSIT, Role.MANAGER_ID);

      FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>();
      fileUploadDto.setDataCategory(DataCategory.Primary);
      fileUploadDto.setDataType(DataCategory.Reference);
      this.depositClientService.uploadDepositDataFile(newDeposit.getResId(), new ClassPathResource("bootstrap.yml"), fileUploadDto);
    } else {
      assertEquals(DepositStatus.IN_PROGRESS, depositList.get(0).getStatus(), "Permanent deposit should be IN_PROGRESS");
    }
  }

  private void createPermanentArchiveACL() {
    // Create an ACL for the third party user on a closed archive with crimson data tag
    final User user = this.personITService.findThirdPartyUser();
    final ArchiveACL archiveACL = new ArchiveACL();
    final String archiveName = DLCMTestConstants.FOR_NO_ONE;
    final ArchivalInfoPackage aip = this.aipClientService.searchByProperties(Map.of("info.name", archiveName)).get(0);
    final OrganizationalUnit orgUnit = new OrganizationalUnit();
    orgUnit.setResId(aip.getOrganizationalUnitId());
    archiveACL.setUser(user);
    archiveACL.setOrganizationalUnit(orgUnit);
    archiveACL.setAipId(aip.getResId());
    archiveACL.setExpiration(OffsetDateTime.now().plusYears(20));
    this.archiveACLClientService.create(archiveACL);
  }

  private void createOrgUnitForNoOne() {
    this.orgUnitITService.getOrCreateOrganizationalUnitWithoutApproval(
            PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN,
            NO_ONE,
            null,
            false);
  }

  private void createSecondOrgUnit() {
    this.orgUnitITService.getSecondPermanentOrganizationalUnit();
  }

  private void createInstitutionForNoOne(PersistenceMode persistenceMode) {
    Institution institution = this.institutionITService.createLocalInstitution(persistenceMode, NO_ONE);
    final List<Institution> institutions = this.institutionITService.findInstitutions(institution.getName());
    if (institutions.isEmpty()) {
      this.institutionITService.createRemoteInstitution(institution);
    }
  }

  private void initArchiveToUpdate() {
    if (this.aipITService.isNoAip(DEFAULT_TITLE_TO_UPDATE)) {
      String title = this.getNewArchiveToUpdateTitle(DEFAULT_TITLE_TO_UPDATE);
      this.createPermanentArchives(title, DEFAULT_DESCRIPTION_TO_UPDATE);
    }
  }

  private void initArchiveToDownload() {
    if (this.aipITService.isNoAip(DEFAULT_TITLE_TO_DOWNLOAD)) {
      Deposit deposit = this.createPermanentArchivesWithFile(DEFAULT_TITLE_TO_DOWNLOAD, YARETA_FILES_DATA);
      this.addIIIFDisseminationPolicy(deposit);
    }
    if (this.aipITService.isNoAip(DEFAULT_TITLE_TO_DOWNLOAD_HEDERA)) {
      Deposit deposit = this.createPermanentArchivesWithFile(DEFAULT_TITLE_TO_DOWNLOAD_HEDERA, HEDERA_FILES_DATA);
      this.addHederaDisseminationPolicy(deposit);
    }
  }

  private void createSubmissionPolicy(String policyName, boolean approval, int timeToKeep, SubmissionAgreementType submissionAgreementType,
          SubmissionAgreement submissionAgreement) {
    final String submissionPolicyName = DLCMTestConstants.getNameWithPermanentLabel(policyName);
    if (this.submissionPolicyClientService.searchByProperties(Map.of("name", submissionPolicyName)).isEmpty()) {
      final SubmissionPolicy localSubmissionPolicy = new SubmissionPolicy();
      localSubmissionPolicy.setName(submissionPolicyName);
      localSubmissionPolicy.setSubmissionApproval(approval);
      localSubmissionPolicy.setTimeToKeep(timeToKeep);
      localSubmissionPolicy.setSubmissionAgreementType(submissionAgreementType);
      if (submissionAgreement != null) {
        localSubmissionPolicy.setSubmissionAgreement(submissionAgreement);
      }
      this.submissionPolicyClientService.create(localSubmissionPolicy);
    }
  }

  private void createPreservationPolicy(String policyName, Integer retention) {
    final String pPName = DLCMTestConstants.getNameWithPermanentLabel(policyName);
    if (this.preservationPolicyClientService.searchByProperties(Map.of("name", pPName)).isEmpty()) {
      final PreservationPolicy localPreservationPolicy = new PreservationPolicy();
      localPreservationPolicy.setName(pPName);
      localPreservationPolicy.setRetention(retention);
      this.preservationPolicyClientService.create(localPreservationPolicy);
    }
  }

  private void createPermanentArchives(String title, String description) {
    Deposit deposit = this.createPermanentRemoteDUADepositWithContributor(title, description);
    this.addDataAndValidatePermanentDeposit(deposit);
  }

  private Deposit createPermanentArchivesWithFile(String title, String filePath) {
    final Deposit deposit = this.depositITService.createRemoteDepositWithContributor(PersistenceMode.PERMANENT, title, title);
    // final Deposit deposit = this.depositITService.getRemotePermanentDeposit(title);
    final String depositId = deposit.getResId();
    this.depositITService.uploadArchiveToDeposit(depositId, new ClassPathResource(filePath));
    this.depositITService.waitDepositWithDataIsReady(depositId);
    // made this deposit permanent archive
    this.depositITService.approveDeposit(deposit);
    return deposit;
  }

  private void addIIIFDisseminationPolicy(Deposit deposit) {
    String orgUnitId = deposit.getOrganizationalUnitId();
    final DisseminationPolicy disseminationPolicyWithParameters = this.orgUnitITService.createLocalDisseminationPolicyWithParameters(
            DisseminationPolicy.DisseminationPolicyType.IIIF,
            Map.of(DLCMConstants.DP_NAME_PARAM, "permanent test iiif policy",
                    DLCMConstants.DP_IIIF_SERVER_PARAM, "https://iiif.unige.ch",
                    DLCMConstants.DP_IIIF_CONTEXT_PARAM, "/",
                    DLCMConstants.DP_IIIF_MEDIA_FOLDER_PARAM, "media",
                    DLCMConstants.DP_IIIF_MANIFESTS_FOLDER_PARAM, "manifests",
                    DLCMConstants.DP_IIIF_MEDIA_PATTERNS_PARAM, "**/*.jpeg,**/*.jpg,**/*.png",
                    DLCMConstants.DP_IIIF_MANIFESTS_PATTERNS_PARAM, "**/*.json,**/*.xml"));

    this.organizationalUnitClientService.addDisseminationPolicyWithParameters(orgUnitId, DLCMConstants.DISSEMINATION_POLICY_IIIF_ID,
            disseminationPolicyWithParameters);
  }

  private void addHederaDisseminationPolicy(Deposit deposit) {
    String orgUnitId = deposit.getOrganizationalUnitId();
    final DisseminationPolicy disseminationPolicyWithParameters = this.orgUnitITService.createLocalDisseminationPolicyWithParameters(
            DisseminationPolicy.DisseminationPolicyType.HEDERA,
            Map.ofEntries(
              Map.entry(DLCMConstants.DP_NAME_PARAM, "permanent test hedera policy"),
              Map.entry(DLCMConstants.DP_HEDERA_PROJECT_PARAM, "Pliegos"),
              Map.entry(DLCMConstants.DP_HEDERA_SERVER_PARAM, "https://hedera.unige.ch"),
              Map.entry(DLCMConstants.DP_HEDERA_SOURCE_DATASET_FILE_SOURCE_FOLDER_PARAM, "source-datasets"),
              Map.entry(DLCMConstants.DP_HEDERA_SOURCE_DATASET_FILE_TARGET_FOLDER_PARAM, "source-datasets"),
              Map.entry(DLCMConstants.DP_HEDERA_SOURCE_DATASET_FILE_PATTERN_PARAM, "**/*.xml"),
              Map.entry(DLCMConstants.DP_HEDERA_RDF_DATA_FILE_SOURCE_FOLDER_PARAM, "rml"),
              Map.entry(DLCMConstants.DP_HEDERA_RDF_DATA_FILE_TARGET_FOLDER_PARAM, "rml"),
              Map.entry(DLCMConstants.DP_HEDERA_RDF_DATA_FILE_PATTERN_PARAM, "**/*.ttl"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_IIIF_SOURCE_FOLDER_PARAM, "research-data-files/IIIF"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_IIIF_TARGET_FOLDER_PARAM, "research-data-files/IIIF"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_IIIF_PATTERN_PARAM, "**/*.jpg,**/*.jpeg,**/*.png"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_SOURCE_FOLDER_PARAM, "research-data-files/Manifests_IIIF"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_TARGET_FOLDER_PARAM, "research-data-files/Manifests_IIIF"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_MANIFEST_IIIF_PATTERN_PARAM, "**/*.json"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_TEI_SOURCE_FOLDER_PARAM, "research-data-files/TEI"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_TEI_TARGET_FOLDER_PARAM, "research-data-files/TEI"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_TEI_PATTERN_PARAM, "**/*.xml"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_WEB_SOURCE_FOLDER_PARAM, "research-data-files/WEB"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_WEB_TARGET_FOLDER_PARAM, "research-data-files/WEB"),
              Map.entry(DLCMConstants.DP_HEDERA_RESEARCH_DATA_FILES_WEB_PATTERN_PARAM, "**/*.txt")
            ));

    this.organizationalUnitClientService.addDisseminationPolicyWithParameters(orgUnitId, DLCMConstants.DISSEMINATION_POLICY_HEDERA_ID,
            disseminationPolicyWithParameters);
  }

  private void addDataAndValidatePermanentDeposit(Deposit deposit) {
    deposit.addItem(this.depositITService.createLocalDepositDataFile(deposit));
    deposit = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.waitDepositWithDataIsReady(deposit.getResId());
    this.depositITService.approveDeposit(deposit);
    this.depositITService.waitDepositIsCompleted(deposit.getResId());
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
  }

  private Deposit createDeposit(DLCMMetadataVersion version) {
    final Deposit deposit = this.depositITService.createRemoteDepositWithContributor(PersistenceMode.PERMANENT,
            DLCMTestConstants.DEPOSIT_BY_VERSION + version.toString(), DEFAULT_DESCRIPTION);
    final String depositId = deposit.getResId();
    deposit.setMetadataVersion(version);
    this.depositITService.updateDeposit(depositId, deposit);
    this.depositITService.uploadArchiveToDeposit(deposit.getResId(), new ClassPathResource(YARETA_DATA));
    this.depositITService.waitDepositWithDataIsReady(deposit.getResId());
    this.depositITService.approveDeposit(deposit);
    return deposit;
  }

  private Deposit waitDepositIsCompleted(String depositId) {
    assertTrue(this.depositITService.waitDepositIsCompleted(depositId), "Deposit not completed: " + depositId);
    return this.depositITService.findDeposit(depositId);
  }

  private Deposit createPermanentRemoteDUADepositWithContributor(String title, String description) {
    return this.depositITService.createRemoteDepositWithDuaType(PersistenceMode.PERMANENT, title, description, DataTag.YELLOW,
            DataUsePolicy.CLICK_THROUGH_DUA, Access.RESTRICTED, false);
  }

  @Override
  protected void deleteFixtures() {
    // no clean
  }
}

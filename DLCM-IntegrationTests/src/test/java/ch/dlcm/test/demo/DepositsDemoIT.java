/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositsDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.demo;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.LicenseClientService;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.service.admin.PreservationPolicyClientService;
import ch.dlcm.service.admin.SubmissionPolicyClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.ingest.SIPClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.service.ArchiveCheckITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

@Order(3)
@EnabledIf("${dlcm.demo-data.enabled:false}")
class DepositsDemoIT extends AbstractPreingestDemoIT {

  private SubmissionPolicyClientService submissionPolicyService;
  private PreservationPolicyClientService preservationPolicyService;

  @Autowired
  public DepositsDemoIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          ArchiveCheckITService archiveCheckITService,
          PersonClientService personClientService,
          SIPClientService sipClientService,
          AIPClientService aipClientService,
          DepositClientService depositClientService,
          LicenseClientService licenseClientService,
          SubmissionPolicyClientService submissionPolicyClientService,
          PreservationPolicyClientService preservationPolicyClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, archiveCheckITService, personClientService, sipClientService, aipClientService,
            depositClientService, licenseClientService);
    this.submissionPolicyService = submissionPolicyClientService;
    this.preservationPolicyService = preservationPolicyClientService;
  }

  private static final Logger log = LoggerFactory.getLogger(DepositsDemoIT.class);

  @Test
  void createBodmerDepositsForDemo() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    final SubmissionPolicy submissionPolicy = this.submissionPolicyService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_SUBMISSION_POLICY)).get(0);
    final PreservationPolicy preservationPolicy = this.preservationPolicyService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_PRESERVATION_POLICY_MAIN)).get(0);

    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(submissionPolicy, "Cannot find submission policy: " + DLCMDemoConstants.DEMO_SUBMISSION_POLICY);
    assertNotNull(preservationPolicy, "Cannot find preservation policy: " + DLCMDemoConstants.DEMO_PRESERVATION_POLICY_MAIN);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(
              () -> this.createDeposits(bodmerOrgUnit, version, Access.PUBLIC, DataTag.BLUE, DataUsePolicy.LICENSE, true, submissionPolicy, preservationPolicy,
                      AbstractPreingestDemoIT.BODMER_DEPOSIT_FOLDER, DLCMDemoConstants.ZIP_FILES));
    }
  }

  @Test
  void createBodmerSensitiveDepositsForDemo() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    final SubmissionPolicy submissionPolicy = this.submissionPolicyService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_SUBMISSION_POLICY)).get(0);
    final PreservationPolicy preservationPolicy = this.preservationPolicyService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_PRESERVATION_POLICY_MAIN)).get(0);

    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(submissionPolicy, "Cannot find submission policy: " + DLCMDemoConstants.DEMO_SUBMISSION_POLICY);
    assertNotNull(preservationPolicy, "Cannot find preservation policy: " + DLCMDemoConstants.DEMO_PRESERVATION_POLICY_MAIN);
    assertDoesNotThrow(
            () -> this.createDeposits(bodmerOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.RESTRICTED, DataTag.YELLOW, DataUsePolicy.CLICK_THROUGH_DUA, false,
                    submissionPolicy, preservationPolicy, AbstractPreingestDemoIT.BODMER_DEPOSIT_FOLDER, AbstractPreingestDemoIT.BODMER_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(bodmerOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.RESTRICTED, DataTag.YELLOW, DataUsePolicy.CLICK_THROUGH_DUA, true,
                    submissionPolicy, preservationPolicy, AbstractPreingestDemoIT.BODMER_DEPOSIT_FOLDER, AbstractPreingestDemoIT.BODMER_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(bodmerOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.CLOSED, DataTag.ORANGE, DataUsePolicy.SIGNED_DUA, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.BODMER_DEPOSIT_FOLDER, AbstractPreingestDemoIT.BODMER_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(bodmerOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.CLOSED, DataTag.RED, DataUsePolicy.SIGNED_DUA, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.BODMER_DEPOSIT_FOLDER, AbstractPreingestDemoIT.BODMER_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(bodmerOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.CLOSED, DataTag.CRIMSON, DataUsePolicy.SIGNED_DUA, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.BODMER_DEPOSIT_FOLDER, AbstractPreingestDemoIT.BODMER_FILES));
  }

  @Test
  void createDlcmDepositsForDemo() {
    final OrganizationalUnit dlcmOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.DLCM_ORG_UNIT);
    final SubmissionPolicy submissionPolicy = this.submissionPolicyService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_SUBMISSION_POLICY)).get(0);
    final PreservationPolicy preservationPolicy = this.preservationPolicyService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_PRESERVATION_POLICY_SECOND)).get(0);

    assertNotNull(dlcmOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.DLCM_ORG_UNIT);
    assertNotNull(submissionPolicy, "Cannot find submission policy: " + DLCMDemoConstants.DEMO_SUBMISSION_POLICY);
    assertNotNull(preservationPolicy, "Cannot find preservation policy: " + DLCMDemoConstants.DEMO_PRESERVATION_POLICY_SECOND);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(
              () -> this.createDeposits(dlcmOrgUnit, version, Access.PUBLIC, DataTag.BLUE, DataUsePolicy.LICENSE, true, submissionPolicy, preservationPolicy,
                      AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, DLCMDemoConstants.ZIP_FILES));
    }
  }

  @Test
  void createDlcmSensitiveDepositsForDemo() {
    final OrganizationalUnit dlcmOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.DLCM_ORG_UNIT);
    final SubmissionPolicy submissionPolicy = this.submissionPolicyService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_SUBMISSION_POLICY)).get(0);
    final PreservationPolicy preservationPolicy = this.preservationPolicyService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_PRESERVATION_POLICY_SECOND)).get(0);

    assertNotNull(dlcmOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.DLCM_ORG_UNIT);
    assertNotNull(submissionPolicy, "Cannot find submission policy: " + DLCMDemoConstants.DEMO_SUBMISSION_POLICY);
    assertNotNull(preservationPolicy, "Cannot find preservation policy: " + DLCMDemoConstants.DEMO_PRESERVATION_POLICY_SECOND);
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.RESTRICTED, DataTag.BLUE, DataUsePolicy.LICENSE, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.RESTRICTED, DataTag.GREEN, DataUsePolicy.LICENSE, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.RESTRICTED, DataTag.YELLOW, DataUsePolicy.CLICK_THROUGH_DUA, false,
                    submissionPolicy, preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.RESTRICTED, DataTag.YELLOW, DataUsePolicy.CLICK_THROUGH_DUA, true,
                    submissionPolicy, preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.CLOSED, DataTag.BLUE, DataUsePolicy.LICENSE, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.CLOSED, DataTag.GREEN, DataUsePolicy.LICENSE, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.CLOSED, DataTag.GREEN, DataUsePolicy.LICENSE, true, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.CLOSED, DataTag.YELLOW, DataUsePolicy.CLICK_THROUGH_DUA, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.CLOSED, DataTag.ORANGE, DataUsePolicy.SIGNED_DUA, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.CLOSED, DataTag.RED, DataUsePolicy.SIGNED_DUA, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
    assertDoesNotThrow(
            () -> this.createDeposits(dlcmOrgUnit, DLCMMetadataVersion.getDefaultVersion(), Access.CLOSED, DataTag.CRIMSON, DataUsePolicy.SIGNED_DUA, false, submissionPolicy,
                    preservationPolicy, AbstractPreingestDemoIT.DLCM_DEPOSIT_FOLDER, AbstractPreingestDemoIT.YARETA_SMALL_FILES));
  }

  private void createDeposits(
          OrganizationalUnit orgUnit,
          DLCMMetadataVersion version,
          Access accessLevel,
          DataTag dataTag,
          DataUsePolicy dataUsePolicy,
          boolean isPublicStructure,
          SubmissionPolicy submissionPolicy,
          PreservationPolicy preservationPolicy,
          String depositFolder,
          String fileType) throws IOException {
    log.info("Creating new deposits for {}", version.getVersion());
    for (Resource file : this.listFiles(depositFolder, fileType)) {
      String depositName = this.getFilename(file);
      log.info("Creating '{}' deposit", depositName);
      Deposit deposit = this.createDeposit(orgUnit, version, depositName, depositName, accessLevel, dataTag, dataUsePolicy, isPublicStructure, submissionPolicy,
              preservationPolicy);
      // Add contributors
      this.addContributors(deposit);
      // Add data files
      this.depositITService.uploadArchiveToDeposit(deposit.getResId(), file);
      // Add thumbnail
      if (this.checkIfThumbnailSupported(version)) {
        final Resource[] thmbList = this.listFiles(depositFolder, "thumbnail/" + depositName + ".timg");
        if (thmbList.length > 0) {
          this.addThumbnail(deposit, thmbList[0]); // TIMG = Thumbnail Image
        }
      }
      // Add DUA
      if (this.checkIfDataUsePolicySupported(version)) {
        switch (dataTag) {
          case YELLOW -> {
            this.addDataUseAgreement(deposit, this.listFiles(AbstractPreingestDemoIT.DUA_FOLDER, "click-dua.pdf")[0]);
          }
          case ORANGE, RED, CRIMSON -> {
            this.addDataUseAgreement(deposit, this.listFiles(AbstractPreingestDemoIT.DUA_FOLDER, "signed-dua.pdf")[0]);
          }
          default -> {
            // No DUA needed
          }
        }
      }
      this.depositITService.waitDepositWithDataIsReady(deposit.getResId());
      this.depositITService.submitAndApproveDeposit(deposit);
      assertTrue(this.depositITService.waitDepositIsCompleted(deposit.getResId()), "Deposit not completed: " + deposit.getResId());
    }
  }

}

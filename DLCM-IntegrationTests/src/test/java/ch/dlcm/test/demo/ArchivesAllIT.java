/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchivesDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.demo;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.LicenseClientService;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.ingest.SIPClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.service.ArchiveCheckITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

@Order(9)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@EnabledIf("${dlcm.demo-check.enabled:false}")
class ArchivesAllIT extends AbstractPreingestDemoIT {

  private static final Logger log = LoggerFactory.getLogger(ArchivesAllIT.class);

  @Autowired
  public ArchivesAllIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          ArchiveCheckITService archiveCheckITService,
          PersonClientService personClientService,
          SIPClientService sipClientService,
          AIPClientService aipClientService,
          LicenseClientService licenseClientService,
          DepositClientService depositClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, archiveCheckITService, personClientService, sipClientService, aipClientService,
            depositClientService, licenseClientService);
  }

  @Order(10)
  @Test
  void checkDeposits() {
    for (OrganizationalUnit orgUnit : this.orgUnitITService.listOrganizationalUnits()) {
      this.logOrgUnit(orgUnit);
      this.archiveCheckITService.checkDepositsByOrgUnit(orgUnit.getName());
    }
  }

  @Order(20)
  @Test
  void checkSIP() {
    for (OrganizationalUnit orgUnit : this.orgUnitITService.listOrganizationalUnits()) {
      this.logOrgUnit(orgUnit);
      this.archiveCheckITService.checkSIPByOrgUnit(orgUnit.getName());
    }
  }

  @Order(30)
  @Test
  void checkAIPOnMainStorage() {
    for (OrganizationalUnit orgUnit : this.orgUnitITService.listOrganizationalUnits()) {
      this.logOrgUnit(orgUnit);
      this.archiveCheckITService.checkAIPOnMainStorageByOrgUnit(orgUnit.getName());
    }
  }

  @Order(40)
  @Test
  void checkAIPOnReplicaStorage() {
    for (OrganizationalUnit orgUnit : this.orgUnitITService.listOrganizationalUnits()) {
      this.logOrgUnit(orgUnit);
      this.archiveCheckITService.checkAIPOnReplicaStorage(orgUnit.getName());
    }
  }

  @Order(50)
  @Test
  void checkArchives() {
    for (OrganizationalUnit orgUnit : this.orgUnitITService.listOrganizationalUnits()) {
      this.logOrgUnit(orgUnit);
      this.archiveCheckITService.ByOrgUnitByOrgUnit(orgUnit.getName());
    }
  }

  private void logOrgUnit(OrganizationalUnit orgUnit) {
    String separator = "==========================================================================================================";
    log.info(separator);
    log.info("[{}] {}", orgUnit.getResId(), orgUnit.getName());
    log.info(separator);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - MetadataEditionDepositsDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.demo;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.LicenseClientService;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.ingest.SIPClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.service.ArchiveCheckITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

@Order(7)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@EnabledIf("${dlcm.demo-data.enabled:false}")
class MetadataEditionCollectionsDemoIT extends AbstractPreingestDemoIT {
  @Autowired
  public MetadataEditionCollectionsDemoIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          ArchiveCheckITService archiveCheckITService,
          PersonClientService personClientService,
          SIPClientService sipService,
          AIPClientService aipClientService,
          DepositClientService depositClientService,
          LicenseClientService licenseClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, archiveCheckITService, personClientService, sipService, aipClientService,
            depositClientService, licenseClientService);
  }

  @Order(1)
  // @Test
  void updateTitleBodmerCollections() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDeposit(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2 + DLCMDemoConstants.COLLECTION, UpdatableField.TITLE,
              false));
    }
  }

  @Order(2)
  // @Test
  void updateBodmerCollections() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDeposit(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2 + DLCMDemoConstants.COLLECTION,
              UpdatableField.values()[this.getRandomUpdate()], false));
    }
  }

  @Order(3)
  // @Test
  void cancelUpdateBodmerCollectionss() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDeposit(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2 + DLCMDemoConstants.COLLECTION,
              UpdatableField.values()[this.getRandomUpdate()], this.getRandomBoolean()));
    }
  }

  @Order(4)
  @Test
  void addAipBodmerCollections() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.addAipCollection(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT1 + DLCMDemoConstants.COLLECTION,
              DLCMDemoConstants.BODMER_DEPOSIT2, false));
    }
  }

  @Order(5)
  // @Test
  void removeAipBodmerCollections() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.removeAipCollection(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT1 + DLCMDemoConstants.COLLECTION, false));
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - MetadataEditionDepositsDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.demo;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.LicenseClientService;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.ingest.SIPClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.service.ArchiveCheckITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

@Order(6)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@EnabledIf("${dlcm.demo-data.enabled:false}")
class MetadataEditionDepositsDemoIT extends AbstractPreingestDemoIT {
  @Autowired
  public MetadataEditionDepositsDemoIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService unitITService,
          DepositITService depositITService,
          ArchiveCheckITService archiveCheckITService,
          PersonClientService personClientService,
          SIPClientService sipClientService,
          AIPClientService aipClientService,
          DepositClientService depositClientService,
          LicenseClientService licenseClientService) {
    super(env, restClientTool, unitITService, depositITService, archiveCheckITService, personClientService, sipClientService, aipClientService,
            depositClientService, licenseClientService);
  }

  @Order(10)
  // @Test
  void updateTitleBodmerDeposits() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDeposit(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2, UpdatableField.TITLE, false));
    }
  }

  @Order(20)
  // @Test
  void updateAccessLevelBodmerDeposits() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDeposit(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2, UpdatableField.ACCESS_LEVEL, false));
    }
  }

  @Order(30)
  // @Test
  void updateDataTagBodmerDeposits() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDeposit(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2, UpdatableField.DATA_TAG, false));
    }
  }

  @Order(40)
  // @Test
  void updateEmbargoBodmerDeposits() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDeposit(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2, UpdatableField.EMBARGO, false));
    }
  }

  @Order(45)
  // @Test
  void updateKeywordsBodmerDeposits() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDeposit(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2, UpdatableField.KEYWORDS, false));
    }
  }

  @Order(50)
  @Test
  void updateBodmerDeposits() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDeposit(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2, UpdatableField.values()[this.getRandomUpdate()], false));
    }
  }

  @Order(60)
  @Test
  void cancelUpdateBodmerDeposits() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDeposit(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2, UpdatableField.values()[this.getRandomUpdate()],
              this.getRandomBoolean()));
    }
  }

  @Order(70)
  @Test
  void addOrRemoveContributorBodmerDeposits() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.updateDepositContributors(bodmerOrgUnit, version, DLCMDemoConstants.BODMER_DEPOSIT2));
    }
  }

  @Order(80)
  @Test
  void addOrRemoveDoiDlcmDeposits() {
    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.DLCM_ORG_UNIT);
    assertNotNull(orgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.DLCM_ORG_UNIT);
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> {
        this.updateDepositDoi(orgUnit, version, DLCMDemoConstants.DLCM_DEPOSIT2);
        this.updateDepositDoi(orgUnit, version, DLCMDemoConstants.DLCM_DEPOSIT3);
      });
    }
  }
}

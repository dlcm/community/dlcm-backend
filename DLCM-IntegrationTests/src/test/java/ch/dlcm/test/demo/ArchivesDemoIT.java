/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchivesDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.demo;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.service.admin.LicenseClientService;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.ingest.SIPClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.service.ArchiveCheckITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

@Order(9)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@EnabledIf("${dlcm.demo-check.enabled:false}")
class ArchivesDemoIT extends AbstractPreingestDemoIT {
  @Autowired
  public ArchivesDemoIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositService,
          ArchiveCheckITService archiveCheckITService,
          PersonClientService personClientService,
          SIPClientService sipClientService,
          AIPClientService aipClientService,
          DepositClientService depositClientService,
          LicenseClientService licenseClientService) {
    super(env, restClientTool, orgUnitITService, depositService, archiveCheckITService, personClientService, sipClientService, aipClientService,
            depositClientService, licenseClientService);
  }

  @Order(1)
  @Test
  void checkBodmerDeposits() {
    this.archiveCheckITService.checkDepositsByOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
  }

  @Order(2)
  @Test
  void checkBodmerSIP() {
    this.archiveCheckITService.checkSIPByOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
  }

  @Order(3)
  @Test
  void checkBodmerAIPOnMainStorage() {
    this.archiveCheckITService.checkAIPOnMainStorageByOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
  }

  @Order(4)
  @Test
  void checkBodmerAIPOnReplicaStorage() {
    this.archiveCheckITService.checkAIPOnReplicaStorage(DLCMDemoConstants.BODMER_ORG_UNIT);
  }

  @Order(5)
  @Test
  void checkBodmerArchives() {
    this.archiveCheckITService.ByOrgUnitByOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
  }

  @Order(6)
  @Test
  void checkDlcmDeposits() {
    this.archiveCheckITService.checkDepositsByOrgUnit(DLCMDemoConstants.DLCM_ORG_UNIT);
  }

  @Order(7)
  @Test
  void checkDlcmSIP() {
    this.archiveCheckITService.checkSIPByOrgUnit(DLCMDemoConstants.DLCM_ORG_UNIT);
  }

  @Order(8)
  @Test
  void checkDlcmAIPOnMainStorage() {
    this.archiveCheckITService.checkAIPOnMainStorageByOrgUnit(DLCMDemoConstants.DLCM_ORG_UNIT);
  }

  @Order(9)
  @Test
  void checkDlcmAIPOnReplicaStorage() {
    this.archiveCheckITService.checkAIPOnReplicaStorage(DLCMDemoConstants.DLCM_ORG_UNIT);
  }

  @Order(10)
  @Test
  void checkDlcmArchives() {
    this.archiveCheckITService.ByOrgUnitByOrgUnit(DLCMDemoConstants.DLCM_ORG_UNIT);
  }

}

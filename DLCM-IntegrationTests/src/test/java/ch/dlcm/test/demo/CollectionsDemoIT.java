/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - CollectionsDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.demo;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Map;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.LicenseClientService;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.service.admin.PreservationPolicyClientService;
import ch.dlcm.service.admin.SubmissionPolicyClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.ingest.SIPClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.service.ArchiveCheckITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

@Order(4)
@EnabledIf("${dlcm.demo-data.enabled:false}")
class CollectionsDemoIT extends AbstractPreingestDemoIT {

  private SubmissionPolicyClientService submissionPolicyService;
  private PreservationPolicyClientService preservationPolicyService;

  @Autowired
  public CollectionsDemoIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          ArchiveCheckITService archiveCheckITService,
          PersonClientService personClientService,
          SIPClientService sipClientService,
          AIPClientService aipClientService,
          DepositClientService depositClientService,
          LicenseClientService licenseClientService,
          SubmissionPolicyClientService submissionPolicyClientService,
          PreservationPolicyClientService preservationPolicyClientService) {
    super(env, restClientTool, orgUnitITService, depositITService, archiveCheckITService, personClientService, sipClientService, aipClientService,
            depositClientService, licenseClientService);
    this.submissionPolicyService = submissionPolicyClientService;
    this.preservationPolicyService = preservationPolicyClientService;
  }

  // @Test
  void createBodmerCollections() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    for (final String depositName : this.getDepositList()) {
      final String collectionName = depositName + DLCMDemoConstants.COLLECTION;
      for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
        assertDoesNotThrow(() -> this.createCollection(bodmerOrgUnit, version, collectionName, null, null, depositName, true, false));
      }
    }
  }

  @Test
  void createBodmerCollectionsForDemo() {
    final OrganizationalUnit bodmerOrgUnit = this.orgUnitITService.getOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT);
    final SubmissionPolicy submissionPolicy = this.submissionPolicyService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_SUBMISSION_POLICY)).get(0);
    final PreservationPolicy preservationPolicy = this.preservationPolicyService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_PRESERVATION_POLICY_MAIN)).get(0);

    assertNotNull(bodmerOrgUnit, "Cannot find orgUnit: " + DLCMDemoConstants.BODMER_ORG_UNIT);
    assertNotNull(submissionPolicy, "Cannot find submission policy: " + DLCMDemoConstants.DEMO_SUBMISSION_POLICY);
    assertNotNull(preservationPolicy, "Cannot find preservation policy: " + DLCMDemoConstants.DEMO_PRESERVATION_POLICY_MAIN);
    for (final String depositName : this.getDepositList()) {
      final String collectionName = depositName + DLCMDemoConstants.COLLECTION;
      for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
        assertDoesNotThrow(() -> this.createCollection(bodmerOrgUnit, version, collectionName, submissionPolicy, preservationPolicy, depositName, true, false));
      }
    }
  }
}

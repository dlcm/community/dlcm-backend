/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DLCMDemoConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.demo;

public class DLCMDemoConstants {

  public final static String ALL_FILES = "*.*";
  public final static String PNG_FILES = "*.png";
  public final static String JPG_FILES = "*.jpg";
  public final static String ZIP_FILES = "*.zip";

  // DLCM orgUnit
  public final static String DLCM_ORG_UNIT = "DLCM";
  public final static String DLCM_ORG_UNIT_ID = "dlcm";
  public final static String DLCM_DEPOSIT1 = "DLCM";
  public final static String DLCM_DEPOSIT2 = "Yareta";
  public final static String DLCM_DEPOSIT3 = "OLOS";

  // DLCM orgUnit
  public final static String BODMER_ORG_UNIT = "Bodmer Lab";
  public final static String BODMER_ORG_UNIT_ID = "bodmer-lab";
  public final static String BODMER_DEPOSIT1 = "Faust 1072035571";
  public final static String BODMER_DEPOSIT2 = "Faust 1072055880";
  public final static String BODMER_FAUST = "Faust";
  public final static String COLLECTION = " Collection";
  public final static String FOR_DEMO = " for demo";

  public final static String DEMO_SUBMISSION_POLICY = "Deposit with approval [clean]";
  public final static String DEMO_PRESERVATION_POLICY_MAIN = "Keep It To A Minimum on Main storage [disposal]";
  public final static String DEMO_PRESERVATION_POLICY_SECOND = "Keep It To A Minimum on Second storage [disposal]";

  public final static String[] ORCID_LIST = {
          "0391-2165-8328-7734",
          "1018-8125-5186-0023",
          "0880-4342-1494-606X"
  };
  public final static String[] DOI_LIST = {
          "10.26037/yareta:hudjwzlchzbfjjylgsxovxjw2q",
          "10.26037/yareta:d3dfasrdjrbfda7uyfreqqnpo4",
          "10.5334/dsj-2021-022"
  };

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractPreingestDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.EmbargoInfo;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.settings.License;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.LicenseClientService;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.ingest.SIPClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.ArchiveCheckITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrgUnitITService;

abstract class AbstractPreingestDemoIT extends AbstractIT {

  private static final Logger log = LoggerFactory.getLogger(AbstractPreingestDemoIT.class);

  protected static final String BODMER_DEPOSIT_FOLDER = "deposit/bodmer";
  protected static final String DLCM_DEPOSIT_FOLDER = "deposit/dlcm";
  protected static final String BODMER_FILES = "Faust*1072035571.zip";
  protected static final String YARETA_SMALL_FILES = "Yareta-small.zip";
  protected static final String DUA_FOLDER = "deposit/dua";
  protected static final String UPDATED_BY = " => ";

  protected ArchiveCheckITService archiveCheckITService;

  protected AIPClientService aipClientService;

  protected OrgUnitITService orgUnitITService;
  protected DepositITService depositITService;
  protected DepositClientService depositClientService;
  private final SIPClientService sipClientService;
  private final LicenseClientService licenseClientService;
  private final PersonClientService personClientService;

  protected AbstractPreingestDemoIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          ArchiveCheckITService archiveCheckITService,
          PersonClientService personClientService,
          SIPClientService sipClientService,
          AIPClientService aipClientService,
          DepositClientService depositClientService,
          LicenseClientService licenseClientService) {
    super(env, restClientTool);
    this.orgUnitITService = orgUnitITService;
    this.depositITService = depositITService;
    this.depositClientService = depositClientService;
    this.personClientService = personClientService;
    this.sipClientService = sipClientService;
    this.aipClientService = aipClientService;
    this.licenseClientService = licenseClientService;
    this.archiveCheckITService = archiveCheckITService;
  }

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Override
  protected void deleteFixtures() {
    // do nothing
  }

  protected String getVersionLabel(DLCMMetadataVersion version) {
    return " (" + version.getVersion() + ")";
  }

  protected int getRandomUpdate() {
    return this.getRandomNumberUsingInts(0, UpdatableField.values().length);
  }

  protected boolean getRandomBoolean() {
    Random random = new Random();
    return random.nextBoolean();
  }

  private int getRandomNumberUsingInts(int min, int max) {
    Random random = new Random();
    return random.ints(min, max)
            .findFirst()
            .getAsInt();
  }

  protected String getFilename(Resource file) {
    return file.getFilename().substring(0, file.getFilename().lastIndexOf('.'));
  }

  protected void addThumbnail(Deposit deposit, Resource file) {
    FileUploadDto<DepositDataFile> fileInfo = new FileUploadDto<>();
    fileInfo.setDataCategory(DataCategory.Internal);
    if (deposit.getMetadataVersion().getVersionNumber() < DLCMMetadataVersion.V3_1.getVersionNumber()) {
      fileInfo.setDataType(DataCategory.DatasetThumbnail);
    } else {
      fileInfo.setDataType(DataCategory.ArchiveThumbnail);
    }
    this.depositITService.uploadDataFileToDeposit(deposit.getResId(), file, fileInfo);
  }

  protected void addDataUseAgreement(Deposit deposit, Resource file) {
    FileUploadDto<DepositDataFile> fileInfo = new FileUploadDto<>();
    fileInfo.setDataCategory(DataCategory.Internal);
    fileInfo.setDataType(DataCategory.ArchiveDataUseAgreement);
    this.depositITService.uploadDataFileToDeposit(deposit.getResId(), file, fileInfo);
  }

  protected boolean checkIfThumbnailSupported(DLCMMetadataVersion version) {
    return version.getVersionNumber() >= DLCMMetadataVersion.V2_1.getVersionNumber();
  }

  protected boolean checkIfDataUsePolicySupported(DLCMMetadataVersion version) {
    return version.getVersionNumber() >= DLCMMetadataVersion.V3_1.getVersionNumber();
  }

  protected boolean checkCompatibleVersion(DLCMMetadataVersion version1, DLCMMetadataVersion version2) {
    return version1.getVersionNumber() <= version2.getVersionNumber();
  }

  protected Access getNewAccessLevelToUpdate(Access access) {
    return switch (access) {
      case PUBLIC -> Access.RESTRICTED;
      case RESTRICTED -> Access.CLOSED;
      case CLOSED -> Access.PUBLIC;
    };
  }

  protected String getNewLicenseToUpdate(String licenseId) {
    for (License license : this.licenseClientService.findAll()) {
      if (license.getResId().equals(licenseId)) {
        continue;
      }
      return license.getResId();
    }
    return null;
  }

  protected EmbargoInfo getNewEmbargoToUpdate(EmbargoInfo embargo, Access accessLevel) {
    if (embargo == null) {
      embargo = new EmbargoInfo();
      embargo.setAccess(this.getNewAccessLevelToUpdate(accessLevel));
      embargo.setMonths(6);
    } else {
      embargo.setAccess(this.getNewAccessLevelToUpdate(embargo.getAccess()));
      embargo.setMonths(embargo.getMonths() + 1);
      embargo.setStartDate(null);
    }
    if (this.getRandomBoolean() || embargo.getAccess() == Access.PUBLIC) {
      return null;
    }
    return embargo;
  }

  protected DataTag getNewDataTagToUpdate(DataTag dataSensitivity, Access accessLevel) {
    DataTag dataTag = switch (dataSensitivity) {
      case UNDEFINED -> DataTag.BLUE;
      case BLUE -> DataTag.GREEN;
      case GREEN -> DataTag.YELLOW;
      case YELLOW -> DataTag.ORANGE;
      case ORANGE -> DataTag.RED;
      case RED -> DataTag.CRIMSON;
      case CRIMSON -> DataTag.UNDEFINED;
    };
    if (dataTag.isAccessLevelAllowed(accessLevel)) {
      return dataTag;
    }
    return this.getNewDataTagToUpdate(dataTag, accessLevel);
  }

  private List<String> getNewKeywords(List<String> keywords) {
    if (keywords.isEmpty() || this.getRandomBoolean()) {
      // Add keyword
      keywords.add("keyword" + keywords.size() + 1);
    } else {
      // Remove keyword
      keywords.remove(keywords.size() - 1);
    }
    return keywords;
  }

  protected void updateDeposit(
          OrganizationalUnit orgUnit,
          DLCMMetadataVersion version,
          String title,
          UpdatableField updateField,
          boolean cancelUpdate) {
    // Find deposit
    Deposit originalDeposit = this.findDeposit(orgUnit, version, title);
    assertNotNull(originalDeposit, "Cannot find deposit: [" + version.getVersion() + "] " + title);
    // Start metadata edition
    log.info("Updating deposit {} [{}]", originalDeposit.getTitle(), originalDeposit.getResId());
    Deposit workingDeposit = this.depositITService.startEditingMetadata(originalDeposit);
    // Update deposit
    String changedValues;
    workingDeposit.setTitle(this.getNewArchiveToUpdateTitle(workingDeposit.getTitle()));
    switch (updateField) {
      case ACCESS_LEVEL -> {
        changedValues = workingDeposit.getAccess().toString() + this.UPDATED_BY;
        workingDeposit.setAccess(this.getNewAccessLevelToUpdate(workingDeposit.getAccess()));
        changedValues += workingDeposit.getAccess().toString();
        workingDeposit.setDescription(workingDeposit.getDescription() + "\nUpdate of access level " + changedValues);
      }
      case DATA_TAG -> {
        changedValues = workingDeposit.getDataSensitivity().toString() + this.UPDATED_BY;
        workingDeposit.setDataSensitivity(this.getNewDataTagToUpdate(workingDeposit.getDataSensitivity(), workingDeposit.getAccess()));
        changedValues += workingDeposit.getDataSensitivity().toString();
        workingDeposit.setDescription(workingDeposit.getDescription() + "\nUpdate of data tag " + changedValues);
      }
      case DESCRIPTION -> workingDeposit.setDescription(workingDeposit.getDescription() + "\nUpdate of description");
      case EMBARGO -> {
        changedValues = this.embargoToString(workingDeposit.getEmbargo()) + this.UPDATED_BY;
        workingDeposit.setEmbargo(this.getNewEmbargoToUpdate(workingDeposit.getEmbargo(), workingDeposit.getAccess()));
        changedValues += this.embargoToString(workingDeposit.getEmbargo());
        workingDeposit.setDescription(workingDeposit.getDescription() + "\nUpdate of embargo " + changedValues);
      }
      case LICENSE -> {
        changedValues = workingDeposit.getLicenseId() + this.UPDATED_BY;
        workingDeposit.setLicenseId(this.getNewLicenseToUpdate(workingDeposit.getLicenseId()));
        changedValues += workingDeposit.getLicenseId();
        workingDeposit.setDescription(workingDeposit.getDescription() + "\nUpdate of license " + changedValues);
      }
      case KEYWORDS -> {
        changedValues = workingDeposit.getKeywords().toString() + this.UPDATED_BY;
        workingDeposit.setKeywords(this.getNewKeywords(workingDeposit.getKeywords()));
        changedValues += workingDeposit.getKeywords().toString();
        workingDeposit.setDescription(workingDeposit.getDescription() + "\nUpdate of keywords " +
                changedValues);
      }
      case TITLE -> {
      }
    }
    // Save & submit deposit
    this.saveAndSubmitDeposit(originalDeposit, workingDeposit, cancelUpdate);
  }

  private String embargoToString(EmbargoInfo embargo) {
    if (embargo == null) {
      return "null";
    }
    return "[access=" + embargo.getAccess().toString()
            + " month=" + embargo.getMonths().toString()
            + " startDate=" + embargo.getStartDate()
            + "]";
  }

  protected void updateDepositContributors(OrganizationalUnit orgUnit, DLCMMetadataVersion version, String title) {
    // Find deposit
    Deposit originalDeposit = this.findDeposit(orgUnit, version, title);
    assertNotNull(originalDeposit, "Cannot find deposit: [" + version.getVersion() + "] " + title);
    // Start metadata edition
    log.info("Updating deposit {} [{}]", originalDeposit.getTitle(), originalDeposit.getResId());
    Deposit workingDeposit = this.depositITService.startEditingMetadata(originalDeposit);
    // Update deposit title
    workingDeposit.setTitle(this.getNewArchiveToUpdateTitle(workingDeposit.getTitle()));
    // Deposit contributors
    List<Person> contributors = this.depositITService.getContributors(originalDeposit.getResId());
    switch (contributors.size()) {
      case 1 -> {
        // Add contributor
        for (String orcid : DLCMDemoConstants.ORCID_LIST) {
          if (this.getRandomBoolean()
                  && !contributors.get(0).getOrcid().equals(orcid)) {
            this.addContributor(originalDeposit, orcid);
            workingDeposit.setDescription(workingDeposit.getDescription() + "\nAdd contributor " + orcid);
            break;
          }
        }
      }
      case 2 -> {
        // Remove contributor
        if (this.getRandomBoolean()) {
          this.removeContributor(workingDeposit, contributors.get(1).getResId());
          workingDeposit.setDescription(workingDeposit.getDescription() + "\nRemove contributor " + contributors.get(1).getOrcid());
        } else {
          // Add contributor
          for (String orcid : DLCMDemoConstants.ORCID_LIST) {
            if (this.getRandomBoolean()
                    && !contributors.get(0).getOrcid().equals(orcid)
                    && !contributors.get(1).getOrcid().equals(orcid)) {
              this.addContributor(originalDeposit, orcid);
              workingDeposit.setDescription(workingDeposit.getDescription() + "\nAdd contributor " + orcid);
              break;
            }
          }
        }
      }
      case 3 -> {
        // Remove contributor
        if (this.getRandomBoolean()) {
          this.removeContributor(workingDeposit, contributors.get(2).getResId());
          workingDeposit.setDescription(workingDeposit.getDescription() + "\nRemove contributor " + contributors.get(2).getOrcid());
        }
      }
      default -> {
      }
    }
    List<Person> newContributors = this.depositITService.getContributors(originalDeposit.getResId());
    // Save & submit deposit
    boolean cancelUpdate = contributors.size() == newContributors.size();
    this.saveAndSubmitDeposit(originalDeposit, workingDeposit, cancelUpdate);
  }

  protected void updateDepositDoi(OrganizationalUnit orgUnit, DLCMMetadataVersion version, String title) {
    boolean cancelUpdate = false;
    // Find deposit
    Deposit originalDeposit = this.findDeposit(orgUnit, version, title);
    assertNotNull(originalDeposit, "Cannot find deposit: [" + version.getVersion() + "] " + title);
    // Start metadata edition
    log.info("Updating deposit {} [{}]", originalDeposit.getTitle(), originalDeposit.getResId());
    Deposit workingDeposit = this.depositITService.startEditingMetadata(originalDeposit);
    // Update deposit title
    workingDeposit.setTitle(this.getNewArchiveToUpdateTitle(workingDeposit.getTitle()));
    String newDoi = DLCMDemoConstants.DOI_LIST[this.getRandomNumberUsingInts(0, DLCMDemoConstants.DOI_LIST.length)];
    String oldDoi = null;
    List<String> oldDoiList = null;
    // Deposit contributors
    switch (this.getRandomNumberUsingInts(0, 2)) {
      case 0 -> {
        // 'isIdenticalTo' DOI
        oldDoi = workingDeposit.getIsIdenticalTo();
        if (this.getRandomBoolean()) {
          if (oldDoi == null) {
            workingDeposit.setIsIdenticalTo(newDoi);
            workingDeposit.setDescription(workingDeposit.getDescription() + "\nUpdate 'IdenticalTo' DOI " + newDoi);
          } else {
            workingDeposit.setIsIdenticalTo(null);
            workingDeposit.setDescription(workingDeposit.getDescription() + "\nRemove 'IdenticalTo' DOI");
          }
        }
        cancelUpdate = (workingDeposit.getIsIdenticalTo() == null && oldDoi == null)
                || (workingDeposit.getIsIdenticalTo() != null && workingDeposit.getIsIdenticalTo().equals(oldDoi));
      }
      case 1 -> {
        // 'isReferencedBy' DOI
        oldDoiList = workingDeposit.getIsReferencedBy();
        if (this.getRandomBoolean()) {
          if (oldDoiList.isEmpty()) {
            workingDeposit.getIsReferencedBy().add(newDoi);
            workingDeposit.setDescription(workingDeposit.getDescription() + "\nUpdate 'ReferencedBy' DOI " + newDoi);
          } else {
            workingDeposit.setIsReferencedBy(null);
            workingDeposit.setDescription(workingDeposit.getDescription() + "\nRemove 'ReferencedBy' DOI");
          }
        }
        cancelUpdate = (workingDeposit.getIsReferencedBy() == null && oldDoi == null)
                || (workingDeposit.getIsReferencedBy() != null && workingDeposit.getIsReferencedBy().equals(oldDoi));
      }
      case 2 -> {
        // 'isObsoletedBy' DOI
        oldDoi = workingDeposit.getIsObsoletedBy();
        if (this.getRandomBoolean() && version.getVersionNumber() >= DLCMMetadataVersion.V2_0.getVersionNumber()) {
          if (oldDoi == null) {
            workingDeposit.setIsObsoletedBy(newDoi);
            workingDeposit.setDescription(workingDeposit.getDescription() + "\nUpdate 'ObsoletedBy' DOI " + newDoi);
          } else {
            workingDeposit.setIsObsoletedBy(null);
            workingDeposit.setDescription(workingDeposit.getDescription() + "\nRemove 'ObsoletedBy' DOI");
          }
        }
        cancelUpdate = (workingDeposit.getIsObsoletedBy() == null && oldDoi == null)
                || (workingDeposit.getIsObsoletedBy() != null && workingDeposit.getIsObsoletedBy().equals(oldDoi));
      }
      default -> {
      }
    }
    // Save & submit deposit
    this.saveAndSubmitDeposit(originalDeposit, workingDeposit, cancelUpdate);
  }

  protected List<String> getDepositList() {
    return Arrays.asList(DLCMDemoConstants.BODMER_DEPOSIT1, DLCMDemoConstants.BODMER_DEPOSIT2);
  }

  protected void addAipCollection(OrganizationalUnit orgUnit, DLCMMetadataVersion version, String collectionTitle, String depositTitle,
          boolean cancelUpdate) {
    // Find collection
    Deposit collection = this.findDeposit(orgUnit, version, collectionTitle);
    assertNotNull(collection, "Cannot find deposit: [" + version.getVersion() + "] " + collectionTitle);
    // Start metadata edition
    Deposit workingcollection = this.depositITService.startEditingMetadata(collection);
    // Update collection
    for (final ArchivalInfoPackage aip : this.aipClientService.searchByProperties(Map.of("info.organizationalUnitId", orgUnit.getResId(),
            "info.name", depositTitle))) {
      // Add AIP
      if (!aip.isCollection() && this.checkCompatibleVersion(version, aip.getMetadataVersion())) {
        this.depositITService.addArchive(workingcollection.getResId(), aip.getResId());
        break;
      }
    }
    // Save & submit deposit
    this.saveAndSubmitDeposit(collection, workingcollection, cancelUpdate);
  }

  protected void removeAipCollection(OrganizationalUnit orgUnit, DLCMMetadataVersion version, String collectionTitle, boolean cancelUpdate) {
    // Find collection
    Deposit collection = this.findDeposit(orgUnit, version, collectionTitle);
    assertNotNull(collection, "Cannot find deposit: [" + version.getVersion() + "] " + collectionTitle);
    // Start metadata edition
    Deposit workingcollection = this.depositITService.startEditingMetadata(collection);
    // Update collection
    for (final ArchivalInfoPackage aip : this.depositITService.getDepositAip(workingcollection.getResId())) {
      // Add AIP
      if (!aip.isCollection() && this.checkCompatibleVersion(version, aip.getMetadataVersion())) {
        this.depositITService.removeArchive(workingcollection.getResId(), aip.getResId());
        break;
      }
    }
    // Save & submit deposit
    this.saveAndSubmitDeposit(collection, workingcollection, cancelUpdate);
  }

  protected boolean waitAIPIsProcessed(String depositId) {
    int count = 0;
    Deposit deposit = this.depositITService.findDeposit(depositId);
    SubmissionInfoPackage sip = this.sipClientService.findOne(deposit.getSipId());
    ArchivalInfoPackage aip;

    do {
      aip = this.aipClientService.findOne(sip.getAipId());
      DLCMTestConstants
              .failAfterNAttempt(count,
                      "AIP " + sip.getAipId() + " should have status Completed or InError instead of " + aip.getInfo().getStatus());
      count++;
    } while (aip.getInfo().getStatus() != PackageStatus.COMPLETED && aip.getInfo().getStatus() != PackageStatus.IN_ERROR);
    return (PackageStatus.COMPLETED == aip.getInfo().getStatus());
  }

  protected void saveAndSubmitDeposit(Deposit originalDeposit, Deposit workingDeposit, boolean cancelUpdate) {
    Deposit updatedDeposit = null;
    try {
      updatedDeposit = this.depositITService.updateDeposit(workingDeposit.getResId(), workingDeposit);
      assertEquals(workingDeposit.getTitle(), updatedDeposit.getTitle(), "Deposit title not updated");
      assertEquals(workingDeposit.getAccess(), updatedDeposit.getAccess(), "Deposit access level not updated");
      assertEquals(workingDeposit.getDataSensitivity(), updatedDeposit.getDataSensitivity(), "Deposit data tag not updated");
    } catch (HttpClientErrorException.BadRequest e) {
      log.error(e.getResponseBodyAsString());
      cancelUpdate = true;
    }
    if (cancelUpdate) {
      log.info("Cancelling edition of deposit {} [{}]", originalDeposit.getTitle(), originalDeposit.getResId());
      this.depositITService.cancelEditingMetadata(updatedDeposit);
    } else {
      log.info("Submitting deposit {} [{}]", originalDeposit.getTitle(), originalDeposit.getResId());
      this.depositITService.submitAndApproveDeposit(updatedDeposit);
    }
    assertTrue(this.depositITService.waitDepositIsCompleted(workingDeposit.getResId()), "Deposit not completed: " + workingDeposit.getResId());
    Deposit latestDeposit = this.depositITService.findDeposit(workingDeposit.getResId());
    if (cancelUpdate) {
      this.archiveCheckITService.checkInfo(
              originalDeposit.getResId(),
              "Cancel => ",
              this.archiveCheckITService.getDepositInfo(latestDeposit),
              this.archiveCheckITService.getDepositInfo(originalDeposit),
              true);
    } else {
      this.archiveCheckITService.checkInfo(
              originalDeposit.getResId(),
              "Update => ",
              this.archiveCheckITService.getDepositInfo(latestDeposit),
              this.archiveCheckITService.getDepositInfo(updatedDeposit),
              true);
    }
    assertTrue(this.waitAIPIsProcessed(latestDeposit.getResId()), "AIP not completed for deposit " + latestDeposit.getResId());
    this.archiveCheckITService.verifyDeposit(latestDeposit);
  }

  protected Deposit findDeposit(OrganizationalUnit orgUnit, DLCMMetadataVersion version, String title) {
    String depositTitle = title + this.getVersionLabel(version);
    List<Deposit> deposits = this.depositITService.findDeposit(orgUnit.getResId(), "title", depositTitle);
    assertFalse(deposits.isEmpty(), "Cannot find deposit: " + depositTitle);
    return deposits.get(0);
  }

  protected Resource[] listFiles(String folder, String filePattern) throws IOException {
    final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
    return resolver.getResources("classpath*:/" + folder + "/" + filePattern);
  }

  protected void createCollection(OrganizationalUnit orgUnit, DLCMMetadataVersion version, String collectionName,
          SubmissionPolicy submissionPolicy, PreservationPolicy preservationPolicy, String depositName, boolean includeUnit,
          boolean includeCollection) throws IOException {
    log.info("Creating new collection {} for {}", depositName, version.getVersion());
    Access collectionAccessLevel = Access.PUBLIC;
    DataTag collectionDataTag = DataTag.BLUE;
    Deposit deposit = this.createDeposit(
            orgUnit, version, collectionName, collectionName,
            Access.PUBLIC, DataTag.BLUE, DataUsePolicy.LICENSE, true,
            submissionPolicy, preservationPolicy);
    // Add contributors
    this.addContributors(deposit);
    for (final ArchivalInfoPackage aip : this.aipClientService.searchByProperties(Map.of("info.organizationalUnitId", orgUnit.getResId(),
            "info.name", depositName))) {
      if (aip.getInfo().getStatus() != PackageStatus.COMPLETED
              || aip.getInfo().getAccess() != Access.PUBLIC
              || (!aip.isCollection() && !includeUnit)
              || (aip.isCollection() && !includeCollection)
              || ((submissionPolicy != null || preservationPolicy != null) && !aip.getInfo().getName().contains(DLCMDemoConstants.FOR_DEMO))) {
        continue;
      }
      // Add AIP
      if (this.checkCompatibleVersion(version, aip.getMetadataVersion())) {
        this.depositITService.addArchive(deposit.getResId(), aip.getResId());
        if (aip.getInfo().getAccess().value() > collectionAccessLevel.value()) {
          collectionAccessLevel = aip.getInfo().getAccess();
        }
        if (aip.getInfo().getDataSensitivity().value() > collectionDataTag.value()) {
          collectionDataTag = aip.getInfo().getDataSensitivity();
        }
      }
    }
    // Add thumbnail
    if (this.checkIfThumbnailSupported(version)) {
      this.addThumbnail(deposit, this.listFiles(this.BODMER_DEPOSIT_FOLDER, "thumbnail/Faust.timg")[0]);
    }
    // Update collection
    deposit.setAccess(collectionAccessLevel);
    deposit.setDataSensitivity(collectionDataTag);
    deposit.setContentStructurePublic(true);
    deposit = this.depositITService.updateDeposit(deposit.getResId(), deposit);
    // Check if there are archives
    List<ArchivalInfoPackage> aipList = this.depositITService.getDepositAip(deposit.getResId());
    assertFalse(aipList.isEmpty(), "No archive in deposit: " + deposit.getResId());
    // Submit deposit
    this.depositITService.waitDepositWithDataIsReady(deposit.getResId());
    this.depositITService.submitAndApproveDeposit(deposit);
    assertTrue(this.depositITService.waitDepositIsCompleted(deposit.getResId()), "Deposit not completed: " + deposit.getResId());
  }

  protected void addContributors(Deposit deposit) {
    for (String orcid : DLCMDemoConstants.ORCID_LIST) {
      this.addContributor(deposit, orcid);
    }

  }

  private void addContributor(Deposit deposit, String orcid) {
    Person person = this.personClientService.findByOrcid(orcid).get();
    this.depositITService.addContributor(deposit.getResId(), person.getResId());
  }

  private void removeContributor(Deposit deposit, String personId) {
    this.depositITService.removeContributor(deposit.getResId(), personId);
  }

  protected Deposit createDeposit(
          OrganizationalUnit orgUnit,
          DLCMMetadataVersion version,
          String title,
          String description,
          Access accessLevel,
          DataTag dataTag,
          DataUsePolicy dataUsePolicy,
          boolean isPublicStructure,
          SubmissionPolicy submissionPolicy,
          PreservationPolicy preservationPolicy) {
    String label = this.getVersionLabel(version);
    if (submissionPolicy != null || preservationPolicy != null) {
      label += DLCMDemoConstants.FOR_DEMO;
    }
    final Deposit deposit = new Deposit();
    deposit.init();
    deposit.setMetadataVersion(version);
    deposit.setAccess(accessLevel);
    deposit.setDataSensitivity(dataTag);
    deposit.setDataUsePolicy(dataUsePolicy);
    deposit.setContentStructurePublic(isPublicStructure);
    deposit.setTitle(title + label);
    deposit.setDescription(description + label);
    deposit.setStatus(DepositStatus.IN_PROGRESS);
    deposit.setOrganizationalUnitId(orgUnit.getResId());
    if (submissionPolicy == null) {
      deposit.setSubmissionPolicyId(orgUnit.getDefaultSubmissionPolicy().getResId());
    } else {
      deposit.setSubmissionPolicyId(submissionPolicy.getResId());
    }
    if (preservationPolicy == null) {
      deposit.setPreservationPolicyId(orgUnit.getDefaultPreservationPolicy().getResId());
    } else {
      deposit.setPreservationPolicyId(preservationPolicy.getResId());
    }

    return this.depositITService.createDeposit(deposit);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AdminDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.EnabledIf;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.License;
import ch.dlcm.service.admin.FundingAgencyClientService;
import ch.dlcm.service.admin.InstitutionClientService;
import ch.dlcm.service.admin.LicenseClientService;
import ch.dlcm.service.admin.PreservationPolicyClientService;
import ch.dlcm.service.admin.SubmissionPolicyClientService;

/*
 * Demo execution order
 * =================================================================================
 * 1) AdminDemoIT => To define & update settings
 * 2) OrgUnitDemoIT => To define & update orgUnits
 * 3) DepositsDemoIT => To create archives
 * 4) CollectionsDemoIT => To create collections of archives
 * 5) CollectionsOfCollectionsDemoIT => To create collections of collections (WIP)
 * 6) MetadataEditionDepositsDemoIT => To update archives
 * 7) MetadataEditionCollectionsDemoIT => To update collections of archives
 * 9) ArchivesDemoIT => To check archives
 */

@Order(1)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@EnabledIf("${dlcm.demo-config.enabled:false}")
class AdminDemoIT extends AbstractAdminDemoIT {
  private static final Logger log = LoggerFactory.getLogger(AdminDemoIT.class);

  private final String LICENSE_FOLDER = "license";
  private final String FUNDING_AGENCY_FOLDER = "fundingAgency";
  private final String INSTITUTION_FOLDER = "institution";

  private final SubmissionPolicyClientService submissionPolicyClientService;
  private final PreservationPolicyClientService preservationPolicyClientService;
  private final FundingAgencyClientService fundingAgencyClientService;
  private final LicenseClientService licenseClientService;
  private final InstitutionClientService institutionClientService;

  @Autowired
  public AdminDemoIT(
          Environment env,
          SudoRestClientTool restClientTool,
          SubmissionPolicyClientService submissionPolicyClientService,
          PreservationPolicyClientService preservationPolicyClientService,
          FundingAgencyClientService fundingAgencyClientService,
          LicenseClientService licenseClientService,
          InstitutionClientService institutionClientService) {
    super(env, restClientTool);
    this.submissionPolicyClientService = submissionPolicyClientService;
    this.preservationPolicyClientService = preservationPolicyClientService;
    this.fundingAgencyClientService = fundingAgencyClientService;
    this.licenseClientService = licenseClientService;
    this.institutionClientService = institutionClientService;
  }

  @Order(10)
  @Test
  void createSubmissionPolicy() {
    final List<SubmissionPolicy> submissionPolicies = this.submissionPolicyClientService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_SUBMISSION_POLICY));
    if (submissionPolicies.isEmpty()) {
      // Create a policy
      final SubmissionPolicy policy = new SubmissionPolicy();
      policy.setName(DLCMDemoConstants.DEMO_SUBMISSION_POLICY);
      policy.setSubmissionApproval(true);
      policy.setTimeToKeep(1);
      final SubmissionPolicy createdPolicy = this.submissionPolicyClientService.create(policy);

      // Test the creation
      assertNotNull(createdPolicy, "In creating submission policy");
    }
  }

  @Order(10)
  @Test
  void createPreservationPolicy() {
    List<PreservationPolicy> preservationPolicies = this.preservationPolicyClientService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_PRESERVATION_POLICY_MAIN));
    if (preservationPolicies.isEmpty()) {
      this.createPreservationPolicy(DLCMDemoConstants.DEMO_PRESERVATION_POLICY_MAIN, true, 0);
    }
    preservationPolicies = this.preservationPolicyClientService
            .searchByProperties(Map.of("name", DLCMDemoConstants.DEMO_PRESERVATION_POLICY_SECOND));
    if (preservationPolicies.isEmpty()) {
      this.createPreservationPolicy(DLCMDemoConstants.DEMO_PRESERVATION_POLICY_SECOND, false, 1);
    }

  }

  @Order(15)
  @Test
  void createSwissInstitutions() {
    // Institutions
    this.createInstitution("UNIGE", "Université de Genève", "https://www.unige.ch", "01swzsf04", "unige.ch");
    this.createInstitution("EPFL", "Ecole Polytechnique Fédérale de Lausanne", "https://www.epfl.ch", "02s376052", "epfl.ch");
    this.createInstitution("ETH", "ETH Zürich", "https://www.ethz.ch", "05a28rw58", "ethz.ch");
    this.createInstitution("UZH", "University of Zurich", "https://www.uzh.ch", "02crff812", "uzh.ch");
    this.createInstitution("HES-SO", "Haute Ecole Spécialisée de Suisse Occidentale", "https://www.hes-so.ch", "01xkakk17", "hesge.ch",
            "hes-so.ch");
    this.createInstitution("ZHAW", "Zurich University of Applied Sciences", "https://www.zhaw.ch", "05pmsvm27", "zhaw.ch");
    this.createInstitution("HUG", "Hôpitaux Universitaires de Genève", "https://www.hug-ge.ch", "01m1pv723", "hcuge.ch");
    this.createInstitution("IHEID", "Graduate Institute of International & Development Studies", "https://www.graduateinstitute.ch",
            "007ygn379", "graduateinstitute.ch");
    this.createInstitution("SWITCHedu-ID", "SWITCH edu-ID", "https://www.switch.ch/edu-id", null, "eduid.ch");
    this.createInstitution("UNIL", "Université de Lausanne", "https://www.unil.ch", "019whta54", "unil.ch");
    this.createInstitution("CERN", "European Organization for Nuclear Research", "https://home.cern", "01ggx4157", "cern.ch");
    this.createInstitution("UNIFR", "Université de Fribourg", "https://www.unifr.ch", "022fs9h90", "unifr.ch");
    this.createInstitution("BFH", "Bern University of Applied Sciences", "https://www.bfh.ch", "02bnkt322", "bfh.ch");
    // Funding Agencies
    this.createFundingAgency("swissuniversities", "swissuniversities", "https://www.swissuniversities.ch", "02fsagg23");
    this.createFundingAgency("SNF", "Swiss National Science Foundation", "https://www.snf.ch", "00yjd3n13");
    this.createFundingAgency("CTI", "Commission for Technology and Innovation", "https://www.kti.admin.ch/", "012y9zp98");
  }

  @Order(15)
  @Test
  void createBelgianInstitutions() {
    // Institutions
    this.createInstitution("ULB", "Université Libre de Bruxelles", "https://www.ulb.be", "01r9htc13", "ulb.be");
    this.createInstitution("UGent", "Universiteit Gent", "https://www.ugent.be", "00cv9y106", "ugent.be");
    this.createInstitution("UAntwerp", "Universiteit Antwerpen", "https://www.uantwerpen.be", "008x57b05", "ugent.be");
    this.createInstitution("VUB", "Vrije Universiteit Brussel", "https://www.vub.be", "006e5kg04", "vub.be");
    this.createInstitution("UHasselt", "Universiteit Hasselt", "https://www.uhasselt.be", "04nbhqj75", "uhasselt.be");
    // Funding Agencies
    this.createFundingAgency("FWO", "Fonds voor Wetenschappelijk Onderzoek", "https://www.fwo.be", "03qtxy027");
  }

  @Order(20)
  @Test
  void addLicenseLogo() throws IOException {
    for (Resource file : this.listFiles(this.LICENSE_FOLDER, DLCMDemoConstants.PNG_FILES)) {
      String spdxId = this.getFilename(file);
      log.info("License '{}'", spdxId);
      // Get License by SPDX ID
      License license = this.licenseClientService.findBySpdxId(spdxId);
      assertNotNull(license, "License not found: " + spdxId);
      this.licenseClientService.uploadLogo(license.getResId(), file);
    }
  }

  @Order(30)
  @Test
  void addFundingAgencyRorId() {
    for (Entry<String, String> rorInfo : this.getFundingAgencyRorIdList().entrySet()) {
      log.info("Funding agency '{}'", rorInfo.getKey());
      FundingAgency fa = null;
      try {
        fa = this.fundingAgencyClientService.findByAcronym(rorInfo.getKey());
      } catch (HttpClientErrorException e) {
        if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
          throw e;
        }
        assertNotNull(fa, "Funding agency not found: " + rorInfo.getKey());
      }
      if (StringTool.isNullOrEmpty(fa.getRorId())) {
        fa.setRorId(rorInfo.getValue());
        this.fundingAgencyClientService.update(fa.getResId(), fa);
      } else {
        log.warn("Funding agency '{}' not updated", rorInfo.getKey());
      }
    }
  }

  @Order(31)
  @Test
  void addFundingAgencyLogo() throws IOException {
    for (Resource file : this.listFiles(this.FUNDING_AGENCY_FOLDER, DLCMDemoConstants.PNG_FILES)) {
      String rorId = this.getFilename(file);
      log.info("Funding agency '{}'", rorId);
      FundingAgency fa = null;
      try {
        fa = this.fundingAgencyClientService.findByRorId(rorId);
      } catch (HttpClientErrorException e) {
        if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
          throw e;
        }
        fa = this.fundingAgencyClientService.findByAcronym(rorId);
      }
      this.fundingAgencyClientService.uploadLogo(fa.getResId(), file);
    }
  }

  @Order(40)
  @Test
  void addInstitutionRorId() {
    for (Entry<String, String> rorInfo : this.getInstitutionRorIdList().entrySet()) {
      log.info("Institution '{}'", rorInfo.getKey());
      Institution institution = null;
      try {
        institution = this.institutionClientService.findByName(rorInfo.getKey());
      } catch (HttpClientErrorException e) {
        if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
          throw e;
        }
        assertNotNull(institution, "Institution not found: " + rorInfo.getKey());
      }
      if (StringTool.isNullOrEmpty(institution.getRorId())) {
        institution.setRorId(rorInfo.getValue());
        this.institutionClientService.update(institution.getResId(), institution);
      } else {
        log.warn("Institution '{}' not updated", rorInfo.getKey());
      }
    }
  }

  @Order(41)
  @Test
  void addInstitutionLogo() throws IOException {
    for (Resource file : this.listFiles(this.INSTITUTION_FOLDER, DLCMDemoConstants.PNG_FILES)) {
      String rorId = this.getFilename(file);
      Institution institution = null;
      log.info("Institution '{}'", rorId);
      try {
        institution = this.institutionClientService.findByRorId(rorId);
      } catch (HttpClientErrorException e) {
        if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
          throw e;
        }
        institution = this.institutionClientService.findByName(rorId);
      }
      this.institutionClientService.uploadLogo(institution.getResId(), file);
    }
  }

  private Map<String, String> getFundingAgencyRorIdList() {
    Map<String, String> list = new HashMap<>();
    list.put("SNF", "00yjd3n13");
    list.put("CTI", "012y9zp98");
    list.put("swissuniversities", "02fsagg23");
    list.put("FWO", "03qtxy027");
    return list;
  }

  private Map<String, String> getInstitutionRorIdList() {
    Map<String, String> list = new HashMap<>();
    list.put("UNIGE", "01swzsf04");
    list.put("EPFL", "02s376052");
    list.put("ETH", "05a28rw58");
    list.put("UZH", "02crff812");
    list.put("HES-SO", "01xkakk17");
    list.put("ZHAW", "05pmsvm27");
    list.put("HUG", "01m1pv723");
    list.put("IHEID", "007ygn379");
    list.put("UNIL", "019whta54");
    list.put("CERN", "01ggx4157");
    list.put("UNIFR", "022fs9h90");
    list.put("BFH", "02bnkt322");
    list.put("ULB", "01r9htc13");
    list.put("UGent", "00cv9y106");
    list.put("UAntwerp", "008x57b05");
    list.put("VUB", "006e5kg04");
    list.put("UHasselt", "04nbhqj75");
    return list;
  }

  private Institution createInstitution(String name, String description, String webSite, String rorId,
          String... emailSuffixes) {
    try {
      return this.institutionClientService.findByName(name);
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
        throw e;
      }
      Institution institution = new Institution();
      institution.setName(name);
      institution.setDescription(description);
      // URL
      if (!StringTool.isNullOrEmpty(webSite)) {
        try {
          institution.setUrl(new URL(webSite));
        } catch (MalformedURLException ex) {
          log.warn("Error in creating institution: URL {} is malformed", webSite);
        }
      }
      // ROR ID
      if (!StringTool.isNullOrEmpty(rorId)) {
        institution.setRorId(rorId);
      }
      // Email suffuxes
      if (!StringTool.isNullOrEmpty(emailSuffixes[0])) {
        for (String emailSuffix : emailSuffixes) {
          institution.getEmailSuffixes().add(emailSuffix);
        }
      }
      return this.institutionClientService.create(institution);
    }
  }

  private FundingAgency createFundingAgency(String acronym, String name, String url, String rorId) {
    try {
      return this.fundingAgencyClientService.findByAcronym(acronym);
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
        throw e;
      }
      FundingAgency fa = new FundingAgency();
      fa.setAcronym(acronym);
      fa.setName(name);
      if (!StringTool.isNullOrEmpty(url)) {
        try {
          fa.setUrl(new URL(url));
        } catch (MalformedURLException ex) {
          log.warn("Error in creating funding agency: URL {} is malformed", url);
        }
      }
      if (!StringTool.isNullOrEmpty(rorId)) {
        fa.setRorId(rorId);
      }
      return this.fundingAgencyClientService.create(fa);
    }
  }

  private void createPreservationPolicy(String name, boolean disposalApproval, int mainStorage) {
    final PreservationPolicy policy = new PreservationPolicy();
    policy.setName(name);
    policy.setDispositionApproval(disposalApproval);
    policy.setRetention(1);
    policy.setMainStorage(mainStorage);
    // Test the creation
    assertNotNull(this.preservationPolicyClientService.create(policy), "In creating preservation policy: " + name);
  }
}

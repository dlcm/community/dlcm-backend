/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrgUnitDemoIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.demo;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.EnabledIf;

import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.display.OrganizationalUnitPreservationPolicyDTO;
import ch.dlcm.model.display.OrganizationalUnitSubmissionPolicyDTO;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.FundingAgencyClientService;
import ch.dlcm.service.admin.InstitutionClientService;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.service.admin.PreservationPolicyClientService;
import ch.dlcm.service.admin.SubmissionPolicyClientService;

@Order(2)
@EnabledIf("${dlcm.demo-config.enabled:false}")
class OrgUnitDemoIT extends AbstractAdminDemoIT {

  private static final Logger log = LoggerFactory.getLogger(OrgUnitDemoIT.class);

  private final String ORG_UNIT_FOLDER = "orgUnit";
  private final String PERSON_FOLDER = "person";

  private final OrganizationalUnitClientService organizationalUnitClientService;
  private final FundingAgencyClientService fundingAgencyClientService;
  private final InstitutionClientService institutionClientService;
  private final PersonClientService personClientService;
  private final SubmissionPolicyClientService submissionPolicyClientService;
  private final PreservationPolicyClientService preservationPolicyClientService;

  @Autowired
  public OrgUnitDemoIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrganizationalUnitClientService organizationalUnitClientService,
          FundingAgencyClientService fundingAgencyClientService,
          InstitutionClientService institutionClientService,
          PersonClientService personClientService,
          SubmissionPolicyClientService submissionPolicyClientService,
          PreservationPolicyClientService preservationPolicyClientService) {
    super(env, restClientTool);
    this.organizationalUnitClientService = organizationalUnitClientService;
    this.institutionClientService = institutionClientService;
    this.fundingAgencyClientService = fundingAgencyClientService;
    this.personClientService = personClientService;
    this.submissionPolicyClientService = submissionPolicyClientService;
    this.preservationPolicyClientService = preservationPolicyClientService;
  }

  @Test
  void createOrgUnits() {
    assertDoesNotThrow(() -> {
      List<OrganizationalUnit> orgUnits = this.organizationalUnitClientService.findAll();

      // DLCM OrgUnit
      log.info("OrgUnit '{}'", DLCMDemoConstants.DLCM_ORG_UNIT);
      OrganizationalUnit dlcmOrgUnit = this.checkOrgUnit(DLCMDemoConstants.DLCM_ORG_UNIT, orgUnits);
      if (dlcmOrgUnit == null) {
        log.info("Creating orgUnit '{}'", DLCMDemoConstants.DLCM_ORG_UNIT);
        dlcmOrgUnit = this.createOrgUnit(DLCMDemoConstants.DLCM_ORG_UNIT_ID, DLCMDemoConstants.DLCM_ORG_UNIT, "https://www.dlcm.ch/");
      }
      this.addSubmissionPolicies(dlcmOrgUnit,
              Arrays.asList("Deposit without approval", "Deposit with approval", DLCMDemoConstants.DEMO_SUBMISSION_POLICY));
      this.addPreservationPolicies(dlcmOrgUnit,
              Arrays.asList("Keep It For 10 Years", "Keep It For 15 Years", "Keep It Forever", DLCMDemoConstants.DEMO_PRESERVATION_POLICY_SECOND));
      this.addInstitutions(dlcmOrgUnit, Arrays.asList("UNIGE", "HES-SO"));
      this.addFundingAgencies(dlcmOrgUnit, Arrays.asList("swissuniversities", "SNF"));
      this.organizationalUnitClientService.uploadLogo(dlcmOrgUnit.getResId(), this.listFiles(this.ORG_UNIT_FOLDER, "DLCM.png")[0]);
      // Bodmer Lab OrgUnit
      log.info("OrgUnit '{}'", DLCMDemoConstants.BODMER_ORG_UNIT);
      OrganizationalUnit bodmerOrgUnit = this.checkOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT, orgUnits);
      if (bodmerOrgUnit == null) {
        log.info("Creating orgUnit '{}'", DLCMDemoConstants.BODMER_ORG_UNIT);
        bodmerOrgUnit = this.createOrgUnit(DLCMDemoConstants.BODMER_ORG_UNIT_ID, DLCMDemoConstants.BODMER_ORG_UNIT,
                "https://bodmerlab.unige.ch/");
      }
      this.addSubmissionPolicies(bodmerOrgUnit, Arrays.asList("Deposit with approval", DLCMDemoConstants.DEMO_SUBMISSION_POLICY));
      this.addPreservationPolicies(bodmerOrgUnit, Arrays.asList("Keep It For 10 Years", DLCMDemoConstants.DEMO_PRESERVATION_POLICY_MAIN));
      this.addInstitutions(bodmerOrgUnit, Arrays.asList("UNIGE"));
      this.addFundingAgencies(bodmerOrgUnit, Arrays.asList("SNF"));
      this.organizationalUnitClientService.uploadLogo(bodmerOrgUnit.getResId(), this.listFiles(this.ORG_UNIT_FOLDER, "BodmerLab.png")[0]);
    });
  }

  @Test
  void createResearchers() throws IOException {
    for (String orcid : DLCMDemoConstants.ORCID_LIST) {
      if (this.personClientService.findByOrcid(orcid).isEmpty()) {
        Person researcher = this.personClientService.create(this.getResearcher(orcid));
        for (Institution institution : this.getResearcherInstitutions(orcid)) {
          this.personClientService.addInstitution(researcher.getResId(), institution.getResId());
        }
      }
    }
    this.addPersonAvatar();
  }

  void addPersonAvatar() throws IOException {
    for (Resource file : this.listFiles(this.PERSON_FOLDER, DLCMDemoConstants.PNG_FILES)) {
      String orcid = this.getFilename(file);
      log.info("Person '{}'", orcid);
      Person person = this.personClientService.findByOrcid(orcid).get();
      assertNotNull(person, "Person not found: " + orcid);
      this.personClientService.uploadAvatar(person.getResId(), file);
    }
  }

  private OrganizationalUnit createOrgUnit(String orgUnitId, String orgUnitName, String orgUnitUrl) throws MalformedURLException {
    OrganizationalUnit orgUnit = new OrganizationalUnit();
    if (!StringTool.isNullOrEmpty(orgUnitId)) {
      orgUnit.setResId(orgUnitId);
    }
    orgUnit.setName(orgUnitName);
    orgUnit.setDescription("For demo purpose");
    orgUnit.setUrl(new URL(orgUnitUrl));
    return this.organizationalUnitClientService.create(orgUnit);
  }

  private OrganizationalUnit checkOrgUnit(String orgUnitName, List<OrganizationalUnit> orgUnits) {
    Optional<OrganizationalUnit> orgUnit = orgUnits.stream().filter(ou -> ou.getName().equals(orgUnitName)).findFirst();
    return orgUnit.orElse(null);
  }

  private void addSubmissionPolicies(OrganizationalUnit orgUnit, List<String> submissionPolicies) {
    boolean defaultPolicyExists = false;
    List<OrganizationalUnitSubmissionPolicyDTO> currentSubmissionPolicies = this.organizationalUnitClientService
            .getSubmissionPolicies(orgUnit.getResId());
    if (orgUnit.getDefaultSubmissionPolicy() != null) {
      defaultPolicyExists = true;
    }
    List<SubmissionPolicy> submissionPolicyList = this.submissionPolicyClientService.findAll();
    for (String submissionPolicyName : submissionPolicies) {
      Optional<SubmissionPolicy> submissionPolicy = submissionPolicyList.stream()
              .filter(p -> p.getName().equals(submissionPolicyName))
              .findFirst();
      if (submissionPolicy.isPresent()) {
        Optional<OrganizationalUnitSubmissionPolicyDTO> currentSubmissionPolicy = currentSubmissionPolicies.stream()
                .filter(p -> p.getResId().equals(submissionPolicy.get().getResId()))
                .findFirst();
        if (currentSubmissionPolicy.isEmpty()) {
          log.info("Adding '{}' submission policy to '{}' orgUnit", submissionPolicy.get().getName(), orgUnit.getName());
          this.organizationalUnitClientService.addSubmissionPolicy(orgUnit.getResId(), submissionPolicy.get().getResId());
          if (!defaultPolicyExists) {
            this.organizationalUnitClientService.setSubmissionPolicyDefaultValue(orgUnit.getResId(), submissionPolicy.get().getResId(), true);
            defaultPolicyExists = true;
          }
        }
      }
    }
  }

  private void addPreservationPolicies(OrganizationalUnit orgUnit, List<String> preservationPolicies) {
    boolean defaultPreservationPolicyExists = false;
    List<OrganizationalUnitPreservationPolicyDTO> currentPreservationPolicies = this.organizationalUnitClientService
            .getPreservationPolicies(orgUnit.getResId());
    if (orgUnit.getDefaultPreservationPolicy() != null) {
      defaultPreservationPolicyExists = true;
    }
    List<PreservationPolicy> preservationPolicyList = this.preservationPolicyClientService.findAll();
    for (String preservationPolicyName : preservationPolicies) {
      Optional<PreservationPolicy> preservationPolicy = preservationPolicyList.stream()
              .filter(p -> p.getName().equals(preservationPolicyName))
              .findFirst();
      if (preservationPolicy.isPresent()) {
        Optional<OrganizationalUnitPreservationPolicyDTO> currentPreservationPolicy = currentPreservationPolicies.stream()
                .filter(p -> p.getResId().equals(preservationPolicy.get().getResId()))
                .findFirst();
        if (currentPreservationPolicy.isEmpty()) {
          log.info("Adding '{}' preservation policy to '{}' orgUnit", preservationPolicy.get().getName(), orgUnit.getName());
          this.organizationalUnitClientService.addPreservationPolicy(orgUnit.getResId(), preservationPolicy.get().getResId());
          if (!defaultPreservationPolicyExists) {
            this.organizationalUnitClientService.setPreservationPolicyDefaultValue(orgUnit.getResId(), preservationPolicy.get().getResId(),
                    true);
            defaultPreservationPolicyExists = true;
          }
        }
      }
    }
  }

  private void addFundingAgencies(OrganizationalUnit orgUnit, List<String> fundingAgencies) {
    List<FundingAgency> currentFundingAgencies = this.organizationalUnitClientService.getFundingAgencies(orgUnit.getResId());
    for (String faAcronym : fundingAgencies) {
      FundingAgency fundingAgency = this.fundingAgencyClientService.findByAcronym(faAcronym);
      Optional<FundingAgency> currentFundingAgency = currentFundingAgencies.stream().filter(fa -> fa.getResId().equals(fundingAgency.getResId()))
              .findFirst();
      if (fundingAgency != null && currentFundingAgency.isEmpty()) {
        log.info("Adding '{}' funding agency to '{}' orgUnit", fundingAgency.getAcronym(), orgUnit.getName());
        this.organizationalUnitClientService.addFundingAgency(orgUnit.getResId(), fundingAgency.getResId());
      }
    }
  }

  private void addInstitutions(OrganizationalUnit orgUnit, List<String> institutions) {
    List<Institution> currentInstitutions = this.organizationalUnitClientService.getInstitutions(orgUnit.getResId());
    for (String institutionName : institutions) {
      Institution institution = this.institutionClientService.findByName(institutionName);
      Optional<Institution> currentInstitution = currentInstitutions.stream().filter(fa -> fa.getResId().equals(institution.getResId()))
              .findFirst();
      if (institution != null && currentInstitution.isEmpty()) {
        log.info("Adding '{}' institution to '{}' orgUnit", institution.getName(), orgUnit.getName());
        this.organizationalUnitClientService.addInstitution(orgUnit.getResId(), institution.getResId());
      }
    }
  }

  private List<Institution> getResearcherInstitutions(String orcid) {
    final List<Institution> institutions = new ArrayList<>();
    if (orcid.equals(DLCMDemoConstants.ORCID_LIST[0])) {
      // Jean Piaget
      institutions.add(this.institutionClientService.findByName("UNIGE"));
    } else if (orcid.equals(DLCMDemoConstants.ORCID_LIST[1])) {
      // Tim Berners-Lee
      institutions.add(this.institutionClientService.findByName("SWITCHedu-ID"));
      institutions.add(this.institutionClientService.findByName("CERN"));
    } else if (orcid.equals(DLCMDemoConstants.ORCID_LIST[2])) {
      // Albert Eisntien
      institutions.add(this.institutionClientService.findByName("ETH"));
    }

    return institutions;
  }

  private Person getResearcher(String orcid) {
    if (orcid.equals(DLCMDemoConstants.ORCID_LIST[0])) {
      return this.createPerson("Jean", "Piaget", orcid);
    } else if (orcid.equals(DLCMDemoConstants.ORCID_LIST[1])) {
      return this.createPerson("Tim", "Berners-Lee", orcid);
    } else if (orcid.equals(DLCMDemoConstants.ORCID_LIST[2])) {
      return this.createPerson("Albert", "Einstein", orcid);
    }
    return null;
  }

  private Person createPerson(String firstName, String lastName, String orcid) {
    final Person person = new Person();
    person.setFirstName(firstName);
    person.setLastName(lastName);
    if (!StringTool.isNullOrEmpty(orcid)) {
      person.setOrcid(orcid);
    }
    return person;
  }
}

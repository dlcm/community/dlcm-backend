/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrgUnitITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import static ch.dlcm.DLCMConstants.NO_REPLY_PREFIX;
import static ch.dlcm.test.DLCMTestConstants.THIRD_PARTY_USER_ID;
import static ch.unige.solidify.auth.model.AuthTestConstants.FIRST_NAME_TEST_SUFFIX;
import static ch.unige.solidify.auth.model.AuthTestConstants.LAST_NAME_TEST_SUFFIX;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ValidationTool;

import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.PersonClientService;
import ch.dlcm.service.admin.UserClientService;
import ch.dlcm.test.DLCMTestConstants;

@Service
public class PersonITService extends ITService {

  // Main services
  private final PersonClientService personClientService;
  private final UserClientService userClientService;

  public PersonITService(PersonClientService personClientService, UserClientService userClientService) {
    this.personClientService = personClientService;
    this.userClientService = userClientService;
  }

  public Person getOrCreateRemotePerson() {
    return this.getOrCreateRemotePerson(AuthApplicationRole.USER_ID);
  }

  public Person createRemotePerson() {
    final Person person = new Person();
    person.setFirstName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Any First name"));
    person.setLastName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Any Last name"));
    person.setOrcid(DLCMTestConstants.getRandomOrcId());
    return this.personClientService.create(person);
  }

  public Person getOrCreateRemotePerson(String applicationRole) {
    String firstName = DLCMTestConstants.getNameWithPermanentLabel(applicationRole + FIRST_NAME_TEST_SUFFIX);
    String lastName = DLCMTestConstants.getNameWithPermanentLabel(applicationRole + LAST_NAME_TEST_SUFFIX);
    return this.getOrCreateRemotePerson(firstName, lastName, null);
  }

  public Person getOrCreateRemotePersonWithOrcid() {
    String firstName = DLCMTestConstants.getNameWithPermanentLabel("person_with_orcid" + FIRST_NAME_TEST_SUFFIX);
    String lastName = DLCMTestConstants.getNameWithPermanentLabel("person_with_orcid" + LAST_NAME_TEST_SUFFIX);
    return this.getOrCreateRemotePerson(firstName, lastName, DLCMTestConstants.ORCID_VALUE);
  }

  public Person getOrCreateRemotePerson(String firstName, String lastName, String orcid) {
    final Person person = new Person();
    person.setFirstName(firstName);
    person.setLastName(lastName);
    if (!StringTool.isNullOrEmpty(orcid)) {
      if (!ValidationTool.isValidOrcid(orcid)) {
        throw new SolidifyRuntimeException(orcid + " is not a valid ORCID");
      }
      person.setOrcid(orcid);
    }
    // Return the existing person if already created
    for (final Person p : this.personClientService.findAll()) {
      if (p.getFirstName().equalsIgnoreCase(person.getFirstName()) &&
              p.getLastName().equalsIgnoreCase(person.getLastName())) {
        return p;
      }
    }
    return this.personClientService.create(person);
  }

  public List<Person> listPeople() {
    return this.personClientService.findAll();
  }

  public void clearPeopleFixtures() {
    this.personClientService.findAll().stream()
            .filter(person -> person.getLastName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(person -> this.delete(person.getResId()));
  }

  public void delete(String resId) {
    this.personClientService.delete(resId);
  }

  public Person findOne(String resId) {
    return this.personClientService.findOne(resId);
  }

  public Person update(String resId, Person resource) {
    return this.personClientService.update(resId, resource);
  }

  public User getUserWithRole(ApplicationRole role) {
    return this.userClientService.searchByProperties(Map.of("email", NO_REPLY_PREFIX + role.getName().toLowerCase() + "@unige.ch")).get(0);
  }

  public User findThirdPartyUser() {
    return this.userClientService.searchByProperties(Map.of("email", NO_REPLY_PREFIX + THIRD_PARTY_USER_ID + "@unige.ch")).get(0);
  }

  public Person getLinkedPersonToThirdPartyUser() {
    return this.getLinkedPersonToUser(this.findThirdPartyUser());
  }

  public Person getLinkedPersonToCurrentUser() {
    return this.getLinkedPersonToUser(this.userClientService.getAuthenticatedUser());
  }

  public Person getLinkedPersonToUser(User user) {
    return this.personClientService.findOne(user.getPerson().getResId());
  }

  public Person createTestPerson(String name, int index) {
    final String permanentLabel = DLCMTestConstants.PERMANENT_TEST_DATA_LABEL;
    final String firstName = permanentLabel + name + " First Name";
    final String lastName = permanentLabel + name + " Last Name";
    List<Person> personList = this.personClientService.searchByProperties(Map.of("firstName", firstName));
    if (personList.isEmpty()) {
      Person testPerson = new Person();
      testPerson.setResId(name);
      testPerson.setFirstName(firstName);
      testPerson.setLastName(lastName);
      testPerson.setOrcid("0000-0000-0000-" + String.format("%04d", index));
      return this.personClientService.create(testPerson);
    }
    return personList.get(0);
  }

  @Override
  public void cleanTestData() {
    this.clearPeopleFixtures();
  }
}

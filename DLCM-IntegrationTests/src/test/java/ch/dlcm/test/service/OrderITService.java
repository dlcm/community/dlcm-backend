/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrderITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ch.dlcm.model.access.Order;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.service.access.OrderClientService;
import ch.dlcm.test.DLCMTestConstants;

@Service
public class OrderITService extends ITService {

  private final OrderClientService orderClientService;

  public OrderITService(OrderClientService orderClientService) {
    this.orderClientService = orderClientService;
  }

  public Order getOrder(String orderId) {
    return this.orderClientService.findOne(orderId);
  }

  public List<DisseminationInfoPackage> getDIP(String orderId) {
    return this.orderClientService.getDIP(orderId);
  }

  public List<ArchivalInfoPackage> getAIP(String orderId) {
    return this.orderClientService.getAIP(orderId);
  }

  public boolean waitForPublicOrderIsReadyOrInError(String orderId, String aipId) {
    int count = 0;
    Order order;
    do {
      order = this.orderClientService.findOne(orderId);
      DLCMTestConstants.failAfterNAttempt(count, "Order from archive " + aipId + " is not in ready status.");
      count++;
    } while (!this.isOrderReadyOrError(order));
    return this.isOrderReadyOrError(order);
  }

  private boolean isOrderReadyOrError(Order order) {
    return order.getStatus() == Order.OrderStatus.READY || order.getStatus() == Order.OrderStatus.IN_ERROR;
  }

  @Override
  public void cleanTestData() {
    // TODO Auto-generated method stub

  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - SubmissionAgreementITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import java.util.List;
import java.util.Optional;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import ch.dlcm.model.policies.SubmissionAgreementUser;
import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.service.admin.SubmissionAgreementClientService;
import ch.dlcm.test.DLCMTestConstants;

@Service
public class SubmissionAgreementITService extends ITService {

  private final SubmissionAgreementClientService submissionAgreementClientService;

  public SubmissionAgreementITService(SubmissionAgreementClientService submissionAgreementClientService) {
    this.submissionAgreementClientService = submissionAgreementClientService;
  }

  public SubmissionAgreement createSubmissionAgreementWithFile(Resource submissionAgreementFile) {
    return this.createSubmissionAgreementWithFile(submissionAgreementFile, null);
  }

  public SubmissionAgreement createSubmissionAgreementWithFile(Resource submissionAgreementFile, String mimeType) {
    SubmissionAgreement expectedSubmissionAgreement = new SubmissionAgreement();
    expectedSubmissionAgreement.setTitle(DLCMTestConstants.getRandomNameWithTemporaryLabel("DLCM project"));
    expectedSubmissionAgreement.setDescription("Swiss Research Data Preservation #CREATE");
    expectedSubmissionAgreement.setVersion(DLCMTestConstants.getRandomNameWithTemporaryLabel("1"));
    return this.submissionAgreementClientService.createSubmissionAgreementWithFile(expectedSubmissionAgreement, submissionAgreementFile);
  }

  public List<SubmissionAgreementUser> getMyApprobations(SubmissionAgreement submissionAgreement) {
    return this.submissionAgreementClientService.getMyApprobations(submissionAgreement.getResId());
  }

  public long getMyApprobationsNumber(SubmissionAgreement submissionAgreement) {
    return this.submissionAgreementClientService.getMyApprobationsNumber(submissionAgreement.getResId());
  }

  private void clearUserAgreementsOnSubmissionAgreementFixture() {

    Optional<SubmissionAgreement> submissionAgreementOpt = this.submissionAgreementClientService.findAll().stream()
            .filter(d -> d.getTitle().contains(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + DLCMTestConstants.SUBMISSION_AGREEMENT_TEST_NAME))
            .findFirst();
    submissionAgreementOpt.ifPresent(
            submissionAgreement -> this.submissionAgreementClientService.getAllApprobations(submissionAgreement.getResId())
                    .forEach(sa -> this.submissionAgreementClientService.deleteUserAgreement(sa.getSubmissionAgreement().getResId(),
                            sa.getUser().getResId())));
  }

  @Override
  public void cleanTestData() {
    this.clearUserAgreementsOnSubmissionAgreementFixture();
  }

}

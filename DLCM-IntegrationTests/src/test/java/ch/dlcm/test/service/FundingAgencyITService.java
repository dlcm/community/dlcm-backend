/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - FundingAgencyITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import java.nio.file.Path;
import java.util.List;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import ch.dlcm.model.settings.FundingAgency;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.FundingAgencyClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;

@Service
public class FundingAgencyITService extends ITService {

  private final FundingAgencyClientService fundingAgencyClientService;

  public FundingAgencyITService(FundingAgencyClientService fundingAgencyClientService) {
    this.fundingAgencyClientService = fundingAgencyClientService;
  }

  public FundingAgency createLocalFundingAgency(PersistenceMode persistenceMode, String role) {
    final String faName = this.getFundingAgencyName(persistenceMode, role);
    final FundingAgency fa = new FundingAgency();
    fa.setName(faName);
    this.setRandomUrlFundingAgency(fa);
    return fa;
  }

  public FundingAgency createRemoteFundingAgency(String acronym, String description) {
    FundingAgency fa = new FundingAgency();
    fa.setAcronym(DLCMTestConstants.getRandomNameWithTemporaryLabel(acronym));
    fa.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel(description));
    fa = this.fundingAgencyClientService.create(fa);
    return fa;
  }

  private String getFundingAgencyName(PersistenceMode persistenceMode, String role) {
    String name;
    if (role == null) {
      name = "Funding Agency";
    } else {
      name = "Funding Agency for " + role.toLowerCase();
    }
    return this.createLabel(persistenceMode, name);
  }

  private void setRandomUrlFundingAgency(FundingAgency fa) {
    fa.setUrl(this.generateRandomUrl());
  }

  public FundingAgency getFundingAgency(String fundingAgencyId) {
    return this.fundingAgencyClientService.findOne(fundingAgencyId);
  }

  public FundingAgency uploadLogo(String fundingAgencyId, ClassPathResource logoToUpload) {
    this.fundingAgencyClientService.uploadLogo(fundingAgencyId, logoToUpload);
    return this.fundingAgencyClientService.findOne(fundingAgencyId);
  }

  public void downloadLogo(String fundingAgencyId, Path path) {
    this.fundingAgencyClientService.downloadLogo(fundingAgencyId, path);
  }

  public void deleteLogo(String fundingAgencyId) {
    this.fundingAgencyClientService.deleteLogo(fundingAgencyId);
  }

  public FundingAgency updateInstitution(String fundingAgencyId, FundingAgency fundingAgency) {
    return this.fundingAgencyClientService.update(fundingAgencyId, fundingAgency);
  }

  protected void clearFundingAgencyFixtures() {
    final List<FundingAgency> agencies = this.fundingAgencyClientService.findAll();

    for (final FundingAgency a : agencies) {

      if (a.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {

        /*
         * Remove eventual links with OrganizationalUnits first
         */
        final List<OrganizationalUnit> child_units = this.fundingAgencyClientService.getOrganizationalUnits(a.getResId());
        if (child_units != null) {
          for (final OrganizationalUnit u : child_units) {
            this.fundingAgencyClientService.removeOrganizationalUnit(a.getResId(), u.getResId());
          }
        }

        this.fundingAgencyClientService.delete(a.getResId());
      }
    }
  }

  @Override
  public void cleanTestData() {
    this.clearFundingAgencyFixtures();
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ThreadLocalRandom;

import jakarta.persistence.criteria.CriteriaBuilder;

import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;

public abstract class ITService {

  protected static final String DEFAULT_DESCRIPTION = "Default description";
  protected static final String DEFAULT_TITLE = "Default title";
  protected static final String DEFAULT_TITLE_TO_UPDATE = "Archive to update";
  protected static final String DEFAULT_DESCRIPTION_TO_UPDATE = "An archive that gets updated each time integration tests are run";
  protected final static String REFERENCE_FILE = "http://www.unige.ch/rectorat/static/budget-2013.pdf";
  protected final static String NO_ONE = "no_one";
  protected final static long REFERENCE_FILE_SIZE = 917380;

  public abstract void cleanTestData();

  protected String createLabel(PersistenceMode persistenceMode, String name) {
    if (persistenceMode.equals(PersistenceMode.PERMANENT)) {
      return DLCMTestConstants.getNameWithPermanentLabel(name);
    }
    if (persistenceMode.equals(PersistenceMode.TEMPORARY)) {
      return DLCMTestConstants.getRandomNameWithTemporaryLabel(name);
    }
    throw new IllegalStateException("Unmanaged persistence mode " + persistenceMode);
  }

  protected URL generateRandomUrl() {
    URL url = null;
    try {
      url = new URL("https://unige.ch/" + ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE));
    } catch (final MalformedURLException e) {
      e.printStackTrace();
    }
    return url;
  }

}

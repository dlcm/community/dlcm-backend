/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AipITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.DLCMTestConstants;

@Service
public class AipITService extends ITService {
  private static final Logger logger = LoggerFactory.getLogger(AipITService.class);

  private final AIPClientService aipClientService;
  private final MetadataClientService metadataClientService;

  public AipITService(AIPClientService aipClientService, MetadataClientService metadataClientService) {
    this.aipClientService = aipClientService;
    this.metadataClientService = metadataClientService;
  }

  public AIPClientService getAipClientService() {
    return this.aipClientService;
  }

  public boolean isExactlyOneAipInStatus(String name, PackageStatus status) {
    List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", name));
    if (aipList.size() != 1) {
      return false;
    }
    return status.equals(aipList.get(0).getInfo().getStatus());
  }

  public boolean isNoAip(String name) {
    List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", name));
    return aipList.isEmpty();
  }

  public ArchivalInfoPackage findAIP(String aipId) {
    return this.aipClientService.findOne(aipId);
  }

  public void resumeAIP(String name) {
    ArchivalInfoPackage aip = this.aipClientService.searchByProperties(Map.of("info.name", name)).get(0);
    this.aipClientService.resume(aip.getResId());
  }

  public void waitAIPIsReady(String orgUnitId, String aipName) {
    // Ready means status COMPLETED and readable from index
    int count = 0;
    List<ArchivalInfoPackage> aipList;
    ArchivalInfoPackage aip;
    do {
      aipList = this.aipClientService.searchByProperties(Map.of("info.organizationalUnitId", orgUnitId, "info.name", aipName));
      if (aipList.size() > 1) {
        for (ArchivalInfoPackage duplicateAip : aipList) {
          logger.error("AIP name={}, resId={}", aipName, duplicateAip.getResId());
        }
        fail("Multiple occurrence of AIP with name '"  + aipName + "' in  orgunit " + orgUnitId +
                ". In integration tests, use only one AIP name by orgunit");
      }
      final String aipIdValue;
      if (aipList.size() == 1) {
        aip = aipList.get(0);
        aipIdValue = aip.getResId();
      } else {
        aip = null;
        aipIdValue = "";
      }
      DLCMTestConstants.failAfterNAttempt(count, "AIP " + aipIdValue + " with name '" + aipName +
              "' from organizational Unit " + orgUnitId + " is not ready (COMPLETED and readable from index).");
      count++;
    } while (aipList.isEmpty() || !this.isAIPReady(aip));
    logger.info("AIP {} with name '{}' from orgunit {} is in status COMPLETED and readable from index.", aip.getResId(), aipName, orgUnitId);
  }

  private boolean isAIPReady(ArchivalInfoPackage aip) {
    if (!this.isAIPCompletedOrError(aip)) {
      logger.error("AIP {} with name '{}' has status {}", aip.getResId(), aip.getInfo().getName(), aip.getInfo().getStatus());
      return false;
    } else if (!this.isAIPReadableFromIndex(aip)) {
      logger.error("AIP {} with name '{}' is not readable from index", aip.getResId(), aip.getInfo().getName());
      return false;
    } else {
      return true;
    }
  }

  private boolean isAIPCompletedOrError(ArchivalInfoPackage aip) {
    return aip.getInfo().getStatus() == PackageStatus.COMPLETED || aip.getInfo().getStatus() == PackageStatus.IN_ERROR;
  }

  private boolean isAIPReadableFromIndex(ArchivalInfoPackage aip) {
    // After an AIP is indexed in Elasticsearch, it could take some time before it's readable
    try {
      this.metadataClientService.findOne(aip.getResId());
    } catch (HttpClientErrorException.NotFound e) {
      return false;
    }
    return true;
  }

  @Override
  public void cleanTestData() {
    // no-op
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrgUnitITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import static ch.dlcm.test.DLCMTestConstants.PRESERVATION_POLICY_TEST;
import static ch.dlcm.test.DLCMTestConstants.SUBMISSION_POLICY_WITHOUT_APPROVAL_WITHOUT_AGREEMENT_TEST;
import static ch.dlcm.test.DLCMTestConstants.SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST;
import static ch.dlcm.test.DLCMTestConstants.SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST;
import static ch.dlcm.test.DLCMTestConstants.SUBMISSION_POLICY_WITH_APPROVAL_WITHOUT_AGREEMENT_TEST;
import static ch.dlcm.test.DLCMTestConstants.SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST;
import static ch.dlcm.test.DLCMTestConstants.SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST;
import static org.junit.jupiter.api.Assertions.fail;

import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.model.AuthApplicationRole;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.display.OrganizationalUnitDTO;
import ch.dlcm.model.display.OrganizationalUnitPreservationPolicyDTO;
import ch.dlcm.model.display.OrganizationalUnitSubmissionPolicyDTO;
import ch.dlcm.model.policies.DisseminationPolicy;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.AdditionalFieldsForm;
import ch.dlcm.model.settings.FormDescriptionType;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.DisseminationPolicyClientService;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.service.admin.PreservationPolicyClientService;
import ch.dlcm.service.admin.SubmissionPolicyClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;

@Service
public class OrgUnitITService extends ITService {

  // Main services
  private final PersonITService personITService;
  private final InstitutionITService institutionITService;
  private final OrganizationalUnitClientService organizationalUnitClientService;

  // Settings
  private final SubmissionPolicyClientService submissionPolicyClientService;
  private final PreservationPolicyClientService preservationPolicyClientService;
  private final DisseminationPolicyClientService disseminationPolicyClientService;

  public OrgUnitITService(
          PersonITService personITService,
          InstitutionITService institutionITService,
          OrganizationalUnitClientService organizationalUnitClientService,
          SubmissionPolicyClientService submissionPolicyClientService,
          PreservationPolicyClientService preservationPolicyClientService,
          DisseminationPolicyClientService disseminationPolicyClientService) {
    // Services
    this.personITService = personITService;
    this.institutionITService = institutionITService;
    this.organizationalUnitClientService = organizationalUnitClientService;
    // Settings
    this.submissionPolicyClientService = submissionPolicyClientService;
    this.preservationPolicyClientService = preservationPolicyClientService;
    this.disseminationPolicyClientService = disseminationPolicyClientService;
  }

  private void addAndVerifyPolicies(OrganizationalUnit organizationalUnit, boolean submissionApproval) {
    final String orgUnitId = organizationalUnit.getResId();

    final String preservationPolicyName = DLCMTestConstants.getNameWithPermanentLabel(PRESERVATION_POLICY_TEST);
    final PreservationPolicy preservationPolicy = this.preservationPolicyClientService.searchByProperties(Map.of("name", preservationPolicyName))
            .get(0);
    final List<OrganizationalUnitPreservationPolicyDTO> currentPreservationPolicies = this.organizationalUnitClientService
            .getPreservationPolicies(orgUnitId);
    if (currentPreservationPolicies.isEmpty() ||
            currentPreservationPolicies.stream()
                    .noneMatch(orgUnitPresPolicy -> orgUnitPresPolicy.getName().equals(preservationPolicyName))) {
      this.organizationalUnitClientService.addPreservationPolicy(orgUnitId, preservationPolicy.getResId());
      this.organizationalUnitClientService.setPreservationPolicyDefaultValue(orgUnitId, preservationPolicy.getResId(), true);
    }

    final String submissionPolicyName = submissionApproval ? DLCMTestConstants.getNameWithPermanentLabel(
            SUBMISSION_POLICY_WITH_APPROVAL_WITHOUT_AGREEMENT_TEST)
            : DLCMTestConstants.getNameWithPermanentLabel(SUBMISSION_POLICY_WITHOUT_APPROVAL_WITHOUT_AGREEMENT_TEST);

    final SubmissionPolicy submissionPolicy = this.submissionPolicyClientService.searchByProperties(Map.of("name", submissionPolicyName)).get(0);
    List<OrganizationalUnitSubmissionPolicyDTO> currentSubmissionPolicies = this.organizationalUnitClientService
            .getSubmissionPolicies(orgUnitId);

    if (currentSubmissionPolicies.isEmpty()
            || currentSubmissionPolicies.stream()
            .noneMatch(orgUnitPresPolicy -> orgUnitPresPolicy.getName().equals(submissionPolicyName))) {
      this.organizationalUnitClientService.addSubmissionPolicy(orgUnitId, submissionPolicy.getResId());
      this.organizationalUnitClientService.setSubmissionPolicyDefaultValue(orgUnitId, submissionPolicy.getResId(), true);
    }

    if (submissionApproval) {
      final String submissionPolicyWithAgreementForFirstDepositName = DLCMTestConstants.getNameWithPermanentLabel(
              SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST);
      final SubmissionPolicy submissionPolicyFirstDeposit = this.submissionPolicyClientService.searchByProperties(
                      Map.of("name", submissionPolicyWithAgreementForFirstDepositName))
              .get(0);
      if (currentSubmissionPolicies.isEmpty()
              || currentSubmissionPolicies.stream()
              .noneMatch(orgUnitPresPolicy -> orgUnitPresPolicy.getName().equals(submissionPolicyWithAgreementForFirstDepositName))) {
        this.organizationalUnitClientService.addSubmissionPolicy(orgUnitId, submissionPolicyFirstDeposit.getResId());
      }

      final String submissionPolicyWithAgreementForAllDepositName = DLCMTestConstants.getNameWithPermanentLabel(
              SUBMISSION_POLICY_WITH_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST);
      final SubmissionPolicy submissionPolicyAllDeposit = this.submissionPolicyClientService.searchByProperties(
              Map.of("name", submissionPolicyWithAgreementForAllDepositName)).get(0);
      if (currentSubmissionPolicies.isEmpty()
              || currentSubmissionPolicies.stream()
              .noneMatch(orgUnitPresPolicy -> orgUnitPresPolicy.getName().equals(submissionPolicyWithAgreementForAllDepositName))) {
        this.organizationalUnitClientService.addSubmissionPolicy(orgUnitId, submissionPolicyAllDeposit.getResId());
      }
    } else {
      final String submissionPolicyWithoutApprovalWithAgreementForFirstDepositName = DLCMTestConstants
              .getNameWithPermanentLabel(SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_FIRST_DEPOSIT_TEST);
      final SubmissionPolicy submissionPolicyFirstDeposit = this.submissionPolicyClientService
              .searchByProperties(Map.of("name", submissionPolicyWithoutApprovalWithAgreementForFirstDepositName))
              .get(0);
      if (currentSubmissionPolicies.isEmpty()
              || currentSubmissionPolicies.stream()
              .noneMatch(orgUnitPresPolicy -> orgUnitPresPolicy.getName()
                      .equals(submissionPolicyWithoutApprovalWithAgreementForFirstDepositName))) {
        this.organizationalUnitClientService.addSubmissionPolicy(orgUnitId, submissionPolicyFirstDeposit.getResId());
      }

      final String submissionPolicyWithoutApprovalWithAgreementForAllDepositName = DLCMTestConstants
              .getNameWithPermanentLabel(SUBMISSION_POLICY_WITHOUT_APPROVAL_WITH_AGREEMENT_FOR_EACH_DEPOSIT_TEST);
      final SubmissionPolicy submissionPolicyAllDeposit = this.submissionPolicyClientService
              .searchByProperties(Map.of("name", submissionPolicyWithoutApprovalWithAgreementForAllDepositName)).get(0);
      if (currentSubmissionPolicies.isEmpty()
              || currentSubmissionPolicies.stream()
              .noneMatch(
                      orgUnitPresPolicy -> orgUnitPresPolicy.getName().equals(submissionPolicyWithoutApprovalWithAgreementForAllDepositName))) {
        this.organizationalUnitClientService.addSubmissionPolicy(orgUnitId, submissionPolicyAllDeposit.getResId());
      }
    }
  }

  public OrganizationalUnit createLocalOrganizationalUnit(PersistenceMode persistenceMode,
          OrganizationalUnitStatus orgUnitStatus, String role, boolean approval, boolean inheritedRole) {
    final OrganizationalUnit organizationalUnit = new OrganizationalUnit();
    if (persistenceMode == PersistenceMode.PERMANENT) {
      organizationalUnit.setResId(this.getOrgUnitId(role, approval, inheritedRole));
    }
    organizationalUnit.setName(this.getOrgUnitName(persistenceMode, role, approval, inheritedRole));
    organizationalUnit.setKeywords(Arrays.asList("chromatography", "flocculation", "ultrafiltration"));
    this.setRandomUrlOrganizationalUnit(organizationalUnit);
    switch (orgUnitStatus) {
      case CLOSED -> {
        organizationalUnit.setOpeningDate(LocalDate.now().minusYears(10));
        organizationalUnit.setClosingDate(LocalDate.now().minusYears(5));
      }
      case OPEN -> {
        organizationalUnit.setOpeningDate(LocalDate.now().minusYears(10));
        organizationalUnit.setClosingDate(LocalDate.now().plusYears(5));
      }
    }
    return organizationalUnit;
  }

  public DisseminationPolicy createRemoteDisseminationPolicy() {
    final DisseminationPolicy disseminationPolicy = new DisseminationPolicy();
    disseminationPolicy.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("dissemination policy"));
    disseminationPolicy.setType(DisseminationPolicy.DisseminationPolicyType.BASIC);
    disseminationPolicy.setDownloadFileName(DisseminationPolicy.DownloadFileName.ARCHIVE_NAME);
    return this.disseminationPolicyClientService.create(disseminationPolicy);
  }

  public DisseminationPolicy createLocalDisseminationPolicyWithParameters(DisseminationPolicy.DisseminationPolicyType type,
          Map<String, String> parametersMap) {
    final DisseminationPolicy disseminationPolicy = new DisseminationPolicy();
    disseminationPolicy.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("dissemination policy"));
    disseminationPolicy.setType(type);
    disseminationPolicy.setDownloadFileName(DisseminationPolicy.DownloadFileName.ARCHIVE_NAME);
    disseminationPolicy.setParametersMap(parametersMap);
    return disseminationPolicy;
  }

  public PreservationPolicy createRemotePreservationPolicy() {
    final PreservationPolicy policy = new PreservationPolicy();
    policy.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Keep It For xx Years"));
    policy.setRetention(10 * DLCMConstants.DAYS_BY_YEAR);
    return this.preservationPolicyClientService.create(policy);
  }

  public SubmissionPolicy createRemoteSubmissionPolicy() {
    final SubmissionPolicy policy = new SubmissionPolicy();
    policy.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("submission policy "));
    policy.setSubmissionApproval(true);
    return this.submissionPolicyClientService.create(policy);
  }

  public OrganizationalUnit getOrganizationalUnit(String orgUnitId) {
    return this.organizationalUnitClientService.findOne(orgUnitId);
  }

  public OrganizationalUnit uploadLogo(String orgUnitId, ClassPathResource logoToUpload) {
    this.organizationalUnitClientService.uploadLogo(orgUnitId, logoToUpload);
    return this.organizationalUnitClientService.findOne(orgUnitId);
  }

  public void downloadLogo(String orgUnitId, Path path) {
    this.organizationalUnitClientService.downloadLogo(orgUnitId, path);
  }

  public void setRandomUrlOrganizationalUnit(OrganizationalUnit organizationalUnit) {
    organizationalUnit.setUrl(this.generateRandomUrl());
  }

  public void enforcePersonHasRoleInOrgUnit(OrganizationalUnit organizationalUnit, Person person, String role) {
    Role effectiveRole = this.organizationalUnitClientService.getPersonRole(organizationalUnit.getResId(), person.getResId());
    if (effectiveRole == null) {
      this.organizationalUnitClientService.addPersonRole(organizationalUnit.getResId(), person.getResId(), role);
    } else {
      this.organizationalUnitClientService.updatePersonRole(organizationalUnit.getResId(), person.getResId(), role);
    }
  }

  public OrganizationalUnit getOrgUnitForNoOne(PersistenceMode persistenceMode) {
    final List<OrganizationalUnit> orgUnits = this.organizationalUnitClientService
            .searchByProperties(Map.of("name", this.getOrgUnitName(persistenceMode, NO_ONE, false, false)));
    if (orgUnits.isEmpty()) {
      fail("Organizational unit for no one is missing");
    } else if (orgUnits.size() > 1) {
      fail("There is multiple organizational units for no one");
    }
    return orgUnits.get(0);
  }

  public OrganizationalUnit getSecondPermanentOrganizationalUnit() {
    final String orgUnitName = DLCMTestConstants.getNameWithPermanentLabel("Organizational Unit 2");
    final List<OrganizationalUnit> orgUnits = this.organizationalUnitClientService.searchByProperties(Map.of("name", orgUnitName));

    final Optional<OrganizationalUnit> optionalOrgUnit = orgUnits.stream().filter(item -> item.getName().equals(orgUnitName)).findFirst();

    if (optionalOrgUnit.isEmpty()) {
      final OrganizationalUnit organizationalUnit = new OrganizationalUnit();
      organizationalUnit.setName(orgUnitName);
      this.setRandomUrlOrganizationalUnit(organizationalUnit);
      this.organizationalUnitClientService.create(organizationalUnit);
      return organizationalUnit;
    } else {
      return optionalOrgUnit.get();
    }
  }

  public OrganizationalUnit createTemporaryRemoteOrgUnit(String description, List<Institution> listInstitutions) {
    // Create a new OrganizationalUnit
    OrganizationalUnitDTO organizationalUnit = new OrganizationalUnitDTO();
    organizationalUnit.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel(description));
    this.setRandomUrlOrganizationalUnit(organizationalUnit);
    organizationalUnit.setDescription(description);

    organizationalUnit.setInstitutions(new ArrayList<>());
    listInstitutions.forEach(organizationalUnit::addInstitution);

    organizationalUnit.setKeywords(Arrays.asList("magnetostatics", "photonics", "oceanography"));
    return this.organizationalUnitClientService.create(organizationalUnit);
  }

  public AdditionalFieldsForm getPermanentAdditionalFieldsFormForOrganizationalUnit(String unitId) {
    AdditionalFieldsForm form = this.organizationalUnitClientService.getCurrentAdditionalFieldsForm(unitId);
    if (form == null) {
      form = new AdditionalFieldsForm();
      form.setName("[TEST] Additional fields form");
      form.setType(FormDescriptionType.FORMLY);
      form.setDescription(
              "[{\"key\" : \"custom_field_1\", \"type\" : \"input\", \"templateOptions\": { \"label\" : \"My 1st custom field\" }}]");

      form = this.organizationalUnitClientService.addAdditionalFieldsForm(unitId, form);
    }
    return form;
  }

  public OrganizationalUnit getOrCreateOrganizationalUnitWithoutApproval(PersistenceMode permanent, OrganizationalUnitStatus orgUnitStatus) {
    return this.getOrCreateOrganizationalUnitWithoutApproval(permanent, orgUnitStatus, Role.MANAGER_ID, AuthApplicationRole.USER_ID, false);
  }

  public OrganizationalUnit getOrCreateOrganizationalUnitWithoutApproval(
          PersistenceMode persistenceMode,
          OrganizationalUnitStatus orgUnitStatus,
          String role,
          String applicationRole,
          boolean inheritedRole) {
    return this.getOrCreateOrganizationalUnit(persistenceMode, orgUnitStatus, false, role, applicationRole, inheritedRole);
  }

  public OrganizationalUnit getOrCreateOrganizationalUnitWithApproval(
          PersistenceMode persistenceMode,
          OrganizationalUnitStatus orgUnitStatus,
          String role,
          String applicationRole,
          boolean inheritedRole) {
    return this.getOrCreateOrganizationalUnit(persistenceMode, orgUnitStatus, true, role, applicationRole, inheritedRole);
  }

  public OrganizationalUnit getOrgUnit(String orgUnitName) {
    List<OrganizationalUnit> orgUnits = this.organizationalUnitClientService.findAll();
    Optional<OrganizationalUnit> orgUnit = orgUnits.stream().filter(ou -> ou.getName().equals(orgUnitName)).findFirst();
    return orgUnit.orElse(null);
  }

  private OrganizationalUnit getOrCreateOrganizationalUnit(
          PersistenceMode persistenceMode,
          OrganizationalUnitStatus orgUnitStatus,
          boolean submissionApproval,
          String role,
          String applicationRole,
          boolean inheritedRole) {
    OrganizationalUnit organizationalUnit = this.createLocalOrganizationalUnit(persistenceMode, orgUnitStatus, role, submissionApproval,
            inheritedRole);

    final String orgUnitName = organizationalUnit.getName();

    final List<OrganizationalUnit> orgUnits = this.organizationalUnitClientService.searchByProperties(Map.of("name", orgUnitName));

    final Optional<OrganizationalUnit> optionalOrgUnit = orgUnits.stream().filter(item -> item.getName().equals(orgUnitName)).findFirst();

    if (optionalOrgUnit.isPresent()) {
      organizationalUnit = this.organizationalUnitClientService.update(optionalOrgUnit.get().getResId(), organizationalUnit);
    } else {
      organizationalUnit = this.organizationalUnitClientService.create(organizationalUnit);
    }
    this.addAndVerifyPolicies(organizationalUnit, submissionApproval);
    if (applicationRole != null) {
      final Person person = this.personITService.getOrCreateRemotePerson(applicationRole);

      if (inheritedRole) {
        Institution institution = this.institutionITService.getOrCreatePermanentInstitution(persistenceMode, role, applicationRole);
        this.institutionITService.enforceOrganizationalUnitInInstitution(institution, organizationalUnit);

        // Add person
        this.institutionITService.enforcePersonHasRoleInInstitution(institution, person, role);
      } else {
        // Add person
        this.enforcePersonHasRoleInOrgUnit(organizationalUnit, person, role);
      }
    }

    return organizationalUnit;
  }

  private String getOrgUnitId(String role, boolean approve, boolean inheritedRole) {
    String name;
    if (role == null) {
      name = "org-unit";
    } else if (inheritedRole) {
      name = "inherited-" + role.toLowerCase() + "-org-unit";
    } else {
      name = role.toLowerCase() + "-org-unit";
    }
    if (approve) {
      name += "-w-approval";
    } else {
      name += "-wo-approval";
    }
    return name;
  }

  private String getOrgUnitName(PersistenceMode persistenceMode, String role, boolean approve, boolean inheritedRole) {
    String name;
    if (role == null) {
      name = "Organizational Unit";
    } else if (inheritedRole) {
      name = "Organizational Unit for inherited " + role.toLowerCase();
    } else {
      name = "Organizational Unit for " + role.toLowerCase();
    }
    if (approve) {
      name += " with approval";
    } else {
      name += " without approval";
    }
    return this.createLabel(persistenceMode, name);
  }

  public OrganizationalUnit getTestOrganizationalUnitAsUser(PersistenceMode persistenceMode,
          OrganizationalUnitStatus orgUnitStatus, String role, boolean approval, boolean inheritedRole) {
    final OrganizationalUnit organizationalUnit = this.createLocalOrganizationalUnit(persistenceMode, orgUnitStatus, role, approval,
            inheritedRole);
    final String orgUnitName = organizationalUnit.getName();

    final List<OrganizationalUnit> orgUnits = this.organizationalUnitClientService.searchByProperties(Map.of("name", orgUnitName));

    return orgUnits.stream().filter(item -> item.getName().equals(orgUnitName)).findFirst().orElse(null);
  }

  public List<OrganizationalUnitSubmissionPolicyDTO> getSubmissionPoliciesFromOrgUnit(OrganizationalUnit organizationalUnit) {
    return this.organizationalUnitClientService.getSubmissionPolicies(organizationalUnit.getResId());
  }

  private void clearOrganizationalUnitFixtures() {
    this.organizationalUnitClientService.findAll().stream()
            .filter(orgUnit -> orgUnit.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(orgUnit -> this.organizationalUnitClientService.delete(orgUnit.getResId()));
  }

  private void clearDisseminationPolicyFixtures() {
    this.disseminationPolicyClientService.findAll().stream()
            .filter(dp -> dp.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(dp -> this.disseminationPolicyClientService.delete(dp.getResId()));
  }

  public List<OrganizationalUnit> listOrganizationalUnits() {
    return this.organizationalUnitClientService.findAll();
  }

  @Override
  public void cleanTestData() {
    this.clearOrganizationalUnitFixtures();
    this.clearDisseminationPolicyFixtures();
  }

  public List<OrganizationalUnit> findOrganizationalUnits(String name) {
    return this.organizationalUnitClientService.searchByProperties(Map.of("name", name));
  }

}

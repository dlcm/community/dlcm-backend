/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DepositITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.preingest.ContributorClientService;
import ch.dlcm.service.preingest.DepositClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.OrganizationalUnitStatus;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;

@Service
public class DepositITService extends ITService {

  private static final Logger log = LoggerFactory.getLogger(DepositITService.class);

  protected static final String REFERENCE_FILE = "https://www.unige.ch/rectorat/static/budget-2013.pdf";

  private final PersonITService personITService;
  private final OrgUnitITService orgUnitITService;
  private final AipITService aipITService;

  private final DepositClientService depositClientService;
  private final ContributorClientService contributorClientService;

  public DepositITService(
          PersonITService personITService,
          OrgUnitITService orgUnitITService,
          AipITService aipITService,
          DepositClientService depositClientService,
          ContributorClientService contributorClientService) {
    // Dependencies
    this.personITService = personITService;
    this.orgUnitITService = orgUnitITService;
    this.aipITService = aipITService;
    // Services
    this.depositClientService = depositClientService;
    this.contributorClientService = contributorClientService;
  }

  public ContributorClientService getContributorClientService() {
    return this.contributorClientService;
  }

  // =============
  // Local Deposit
  // =============

  public Deposit createLocalDeposit(PersistenceMode persistenceMode, String title, String description) {
    final Deposit deposit = new Deposit();
    deposit.init();
    deposit.setTitle(this.createLabel(persistenceMode, title));
    deposit.setDescription(this.createLabel(persistenceMode, description));
    deposit.setAccess(Access.PUBLIC);
    deposit.setDataSensitivity(DataTag.BLUE);
    deposit.setDataUsePolicy(DataUsePolicy.LICENSE);
    deposit.setContentStructurePublic(true);
    return deposit;
  }

  public Deposit createLocalDepositWithMetadataVersion(PersistenceMode persistenceMode, String title, String description,
          DLCMMetadataVersion version) {
    final Deposit deposit = this.createLocalDeposit(persistenceMode, title, description);
    deposit.setMetadataVersion(version);
    return deposit;
  }

  public Deposit createLocalDeposit(PersistenceMode persistenceMode, String title, String description, String role, boolean inheritedRole,
          DLCMMetadataVersion version) {
    final Deposit deposit = this.createLocalDepositWithMetadataVersion(persistenceMode, title, description, version);
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getTestOrganizationalUnitAsUser(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN, role, false, inheritedRole);
    deposit.setOrganizationalUnitId(organizationalUnit.getResId());
    return deposit;
  }

  public Deposit createLocalDepositWithSubmissionApprove(PersistenceMode persistenceMode, String title, String description, String role,
          boolean inheritedRole) {
    final Deposit deposit = this.createLocalDeposit(persistenceMode, title, description);
    final OrganizationalUnit organizationalUnit = this.orgUnitITService.getTestOrganizationalUnitAsUser(PersistenceMode.PERMANENT,
            OrganizationalUnitStatus.OPEN, role, true, inheritedRole);
    deposit.setOrganizationalUnitId(organizationalUnit.getResId());
    return deposit;
  }

  public Deposit createLocalDeposit(PersistenceMode persistenceMode, String title) {
    return this.createLocalDeposit(persistenceMode, title, DEFAULT_DESCRIPTION, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
  }

  // ==============
  // Remote Deposit
  // ==============
  public Deposit createRemoteDeposit(PersistenceMode persistenceMode, String title, String description) {
    return this.createRemoteDeposit(persistenceMode, title, description, Role.MANAGER_ID, false, DLCMMetadataVersion.getDefaultVersion());
  }

  public Deposit createRemoteDepositWithVersion(PersistenceMode persistenceMode, String title, String description, DLCMMetadataVersion version) {
    return this.createRemoteDeposit(persistenceMode, title, description, Role.MANAGER_ID, false, version);
  }

  public Deposit createRemoteDeposit(PersistenceMode persistenceMode, String title, String description, String role, boolean inheritedRole,
          DLCMMetadataVersion version) {
    Deposit deposit = this.createLocalDeposit(persistenceMode, title, description, role, inheritedRole, version);
    return this.depositClientService.create(deposit);
  }

  public Deposit createRemoteDepositWithContributor(PersistenceMode persistenceMode, String title, String description) {
    return this.createRemoteDepositWithContributorAndVersion(persistenceMode, title, description, DLCMMetadataVersion.getDefaultVersion());
  }

  public Deposit createRemoteDepositWithContributorAndVersion(PersistenceMode persistenceMode, String title, String description,
          DLCMMetadataVersion version) {
    final Deposit deposit = this.createRemoteDeposit(persistenceMode, title, description, Role.MANAGER_ID, false, version);
    final Person person = this.personITService.getOrCreateRemotePerson();
    this.depositClientService.addContributor(deposit.getResId(), person.getResId());
    return deposit;
  }

  public Deposit createRemoteDepositWithDuaType(PersistenceMode persistenceMode, String title, String description, DataTag dataSensitivity,
          DataUsePolicy dataUsePolicy, Access access, boolean isStructureContentPublic) {
    Deposit deposit = this.createRemoteDepositWithAccessLevel(persistenceMode, title, description, dataSensitivity,
            dataUsePolicy, access, isStructureContentPublic, Role.MANAGER_ID, false);
    // add dua
    if (dataUsePolicy.equals(DataUsePolicy.CLICK_THROUGH_DUA) || dataUsePolicy.equals(DataUsePolicy.SIGNED_DUA)) {
      deposit.addItem(this.createDuaFile(deposit));
      return this.createRemoteDepositWithData(deposit);
    }
    return deposit;
  }

  public Deposit createRemoteDepositWithAccessLevel(PersistenceMode persistenceMode, String title, String description, DataTag dataSensitivity,
          DataUsePolicy dataUsePolicy, Access access, boolean isStructureContentPublic, String role, boolean inheritedRole) {
    Deposit deposit = this.createLocalDeposit(persistenceMode, title, description, role, inheritedRole, DLCMMetadataVersion.getDefaultVersion());
    deposit.setAccess(access);
    deposit.setDataUsePolicy(dataUsePolicy);
    deposit.setDataSensitivity(dataSensitivity);
    deposit.setContentStructurePublic(isStructureContentPublic);
    deposit = this.depositClientService.create(deposit);
    final Person person = this.personITService.getOrCreateRemotePerson();
    this.depositClientService.addContributor(deposit.getResId(), person.getResId());
    return deposit;
  }

  public Deposit createRemoteDepositWithContributorsAndSubmissionApprove(PersistenceMode persistenceMode, String title, String description) {
    return this.createRemoteDepositWithContributorsAndSubmissionApprove(persistenceMode, title, description, false);
  }

  public Deposit createRemoteDepositWithData(Deposit deposit) {
    // Create the deposit on the server if needed
    Deposit remoteDeposit;
    try {
      remoteDeposit = this.depositClientService.findOne(deposit.getResId());
    } catch (HttpClientErrorException.NotFound e) {
      remoteDeposit = this.depositClientService.create(deposit);
    }

    // Create the deposit data file on the server
    for (final DepositDataFile df : deposit.getDataFiles()) {
      this.depositClientService.addDepositDataFile(remoteDeposit.getResId(), df);
    }
    return remoteDeposit;
  }

  private Deposit createRemoteDepositWithSubmissionApprove(PersistenceMode persistenceMode, String title, String description, String role,
          boolean inheritedRole) {
    Deposit deposit = this.createLocalDepositWithSubmissionApprove(persistenceMode, title, description, role, inheritedRole);
    return this.depositClientService.create(deposit);
  }

  private Deposit createRemoteDepositWithContributorsAndSubmissionApprove(PersistenceMode persistenceMode, String title, String description,
          boolean inheritedRole) {
    final Deposit deposit = this.createRemoteDepositWithSubmissionApprove(persistenceMode, title, description, Role.MANAGER_ID, inheritedRole);
    final Person person1 = this.personITService.getOrCreateRemotePerson();
    this.depositClientService.addContributor(deposit.getResId(), person1.getResId());
    final Person person2 = this.personITService.getOrCreateRemotePersonWithOrcid();
    this.depositClientService.addContributor(deposit.getResId(), person2.getResId());
    return deposit;
  }

  public Deposit getRemotePermanentDeposit(String depositTitle) {
    List<Deposit> listDeposit = this.depositClientService
            .searchByProperties(Collections.singletonMap("title", depositTitle));
    if (listDeposit.isEmpty()) {
      throw new SolidifyRuntimeException("The permanent deposit is missing.");
    } else if (listDeposit.size() > 1) {
      throw new SolidifyRuntimeException("There should be only one permanent deposit");
    } else {
      return listDeposit.get(0);
    }
  }

  // ===============
  // Deposit Actions
  // ===============
  public Deposit findDeposit(String depositId) {
    return this.depositClientService.findOne(depositId);
  }

  public Deposit submitDeposit(Deposit deposit) {
    this.depositClientService.submit(deposit.getResId());
    return this.depositClientService.findOne(deposit.getResId());
  }

  public Deposit approveDeposit(Deposit deposit) {
    this.depositClientService.approve(deposit.getResId());
    return this.depositClientService.findOne(deposit.getResId());
  }

  public Deposit rejectDeposit(Deposit deposit, String reason) {
    this.depositClientService.reject(deposit.getResId(), reason);
    return this.depositClientService.findOne(deposit.getResId());
  }

  public Deposit startEditingMetadata(Deposit deposit) {
    this.depositClientService.startEditingMetadata(deposit.getResId());
    return this.depositClientService.findOne(deposit.getResId());
  }

  public Deposit cancelEditingMetadata(Deposit deposit) {
    this.depositClientService.cancelEditingMetadata(deposit.getResId());
    return this.depositClientService.findOne(deposit.getResId());
  }

  public Deposit enableRevision(Deposit deposit) {
    if (!deposit.getStatus().equals(DepositStatus.IN_ERROR)) {
      return deposit;
    }
    this.depositClientService.enableRevision(deposit.getResId());
    return this.depositClientService.findOne(deposit.getResId());
  }

  public String checkSubmissionAgreement(Deposit deposit) {
    return this.depositClientService.checkSubmissionAgreement(deposit.getResId());
  }

  public void approveSubmissionAgreement(Deposit deposit) {
    this.depositClientService.approveSubmissionAgreement(deposit.getResId());
  }

  public List<Deposit> findDeposits(String title) {
    return this.depositClientService.searchByProperties(Collections.singletonMap("title", title));
  }

  public boolean waitDepositIsProcessed(String depositId, DepositStatus endStatus) {
    int count = 0;
    Deposit deposit;

    do {
      deposit = this.depositClientService.findOne(depositId);
      DLCMTestConstants
              .failAfterNAttempt(count, "Deposit " + depositId + " should have status Completed or InError instead of " + deposit.getStatus());
      count++;
    } while (deposit.getStatus() != DepositStatus.COMPLETED && deposit.getStatus() != DepositStatus.IN_ERROR);
    return (endStatus == deposit.getStatus());
  }

  public boolean waitDepositIsInValidation(String depositId) {
    int count = 0;
    boolean depositFound;
    do {
      try {
        Deposit deposit = this.depositClientService.findOne(depositId);
        depositFound = deposit.getStatus() == DepositStatus.IN_VALIDATION;
      } catch (HttpClientErrorException.NotFound e) {
        depositFound = false;
      }
      DLCMTestConstants.failAfterNAttempt(count, "Deposit " + depositId + " should have been deleted");
      count++;
    } while (!depositFound);
    return true;
  }

  public boolean waitDepositIsDeleted(String depositId) {
    int count = 0;
    boolean depositFound;
    do {
      try {
        this.depositClientService.findOne(depositId);
        depositFound = true;
      } catch (HttpClientErrorException.NotFound e) {
        depositFound = false;
      }
      DLCMTestConstants.failAfterNAttempt(count, "Deposit " + depositId + " should have been deleted");
      count++;
    } while (depositFound);

    return true;
  }

  public boolean waitDepositWithDataIsReady(String depositId) {
    int count = 0;
    List<DepositDataFile> dataFileList;

    do {
      this.depositClientService.getDepositDataFile(depositId);
      DLCMTestConstants.failAfterNAttempt(count, "Deposit data files of deposit " + depositId + " are not all in ready status.");
      dataFileList = this.depositClientService.getDepositDataFile(depositId);
      count++;
    } while (!this.isAllReadyInDataFileListOrError(dataFileList));

    return this.isAllReadyInDataFileListOrError(dataFileList);
  }

  public boolean waitDepositIsCompleted(String depositId) {
    return this.waitDepositIsProcessed(depositId, DepositStatus.COMPLETED);
  }

  public List<Deposit> listDeposits() {
    return this.depositClientService.findAll();
  }

  public Deposit submitAndApproveDeposit(Deposit deposit) {
    this.submitDeposit(deposit);
    SolidifyTime.waitOneSecond();
    return this.approveDeposit(deposit);
  }

  public Deposit createDeposit(Deposit deposit) {
    return this.depositClientService.create(deposit);
  }

  public Deposit updateDeposit(String depositId, Deposit deposit) {
    return this.depositClientService.update(depositId, deposit);
  }

  public void addContributor(String depositId, String personId) {
    this.depositClientService.addContributor(depositId, personId);
  }

  public void removeContributor(String depositId, String personId) {
    this.depositClientService.removeContributor(depositId, personId);
  }

  public List<Deposit> findDeposit(String orgUnitId, String fieldToSearch, String valueToSearch) {
    return this.depositClientService
            .searchByProperties(Map.of(DLCMConstants.ORG_UNIT_ID_FIELD, orgUnitId, fieldToSearch, valueToSearch));
  }

  public void addContributorToDeposit(String depositId, String personId) {
    this.depositClientService.addContributor(depositId, personId);
  }

  public List<Person> getContributors(String depositId) {
    return this.depositClientService.getContributors(depositId);
  }

  // ==================
  // Deposit Collection
  // ==================

  public List<ArchivalInfoPackage> createAIC(Deposit deposit, int num, boolean collection) {
    // Add AIU to AIC
    for (final ArchivalInfoPackage aiu : this.getAipList(num, collection)) {
      this.depositClientService.addAip(deposit.getResId(), aiu.getResId());
    }
    return this.depositClientService.getDepositAip(deposit.getResId());
  }

  private List<ArchivalInfoPackage> getAipList(int max, boolean isCollection) {
    final List<ArchivalInfoPackage> list = new ArrayList<>();
    int count = 0;
    final String aipNamePrefix;
    if (isCollection) {
      aipNamePrefix = DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + DLCMTestConstants.ARCHIVAL_COLLECTION_LABEL;
    } else {
      aipNamePrefix = DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + DLCMTestConstants.ARCHIVAL_UNIT_LABEL;
    }
    final List<ArchivalInfoPackage> aipList = this.aipITService.getAipClientService().searchByProperties(Map.of("info.name", aipNamePrefix));
    for (final ArchivalInfoPackage aip : aipList) {
      if (aip.getInfo().getStatus() == PackageStatus.COMPLETED && isCollection == aip.isCollection()) {
        count++;
        list.add(aip);
        if (count == max) {
          break;
        }
      }

    }
    assertEquals(max, list.size(), "Not enough completed AIP");
    return list;
  }

  public List<ArchivalInfoPackage> getDepositAip(String depositId) {
    return this.depositClientService.getDepositAip(depositId);
  }

  public void addArchive(String depositId, String archiveId) {
    this.depositClientService.addAip(depositId, archiveId);
  }

  public void removeArchive(String depositId, String archiveId) {
    this.depositClientService.removeAip(depositId, archiveId);
  }

  // =================
  // Deposit Data File
  // =================

  public DepositDataFile createLocalDepositDataFile(Deposit deposit) {
    // Create a deposit data file object
    final DepositDataFile depositDataFile = new DepositDataFile(deposit);
    URI referenceFileUri = URI.create(REFERENCE_FILE);
    depositDataFile.setSourceData(referenceFileUri);
    depositDataFile.setFileSize(REFERENCE_FILE_SIZE);
    depositDataFile.setDataCategory(DataCategory.Primary);
    depositDataFile.setDataType(DataCategory.Derived);
    depositDataFile.setRelativeLocation("/tmp/dlcm/test");

    return depositDataFile;
  }

  public DepositDataFile createDatasetThumbnailFile(Deposit deposit) {
    return this.createDataFile(deposit, DataCategory.Internal, DataCategory.DatasetThumbnail, "/thumbnail");
  }

  public DepositDataFile createArchiveThumbnailFile(Deposit deposit) {
    return this.createDataFile(deposit, DataCategory.Internal, DataCategory.ArchiveThumbnail, "/thumbnail");
  }

  public DepositDataFile createDuaFile(Deposit deposit) {
    return this.createDataFile(deposit, DataCategory.Internal, DataCategory.ArchiveDataUseAgreement, "/dua");
  }

  public DepositDataFile createReadmeFile(Deposit deposit) {
    return this.createDataFile(deposit, DataCategory.Internal, DataCategory.ArchiveReadme, "/readme");
  }

  public DepositDataFile createDataFile(Deposit deposit) {
    return this.createDataFile(deposit, DataCategory.Primary, DataCategory.Experimental, "/");
  }

  private DepositDataFile createDataFile(Deposit deposit, DataCategory dataFileCategory, DataCategory datafileType, String relativeLocation) {
    final DepositDataFile dataFile = new DepositDataFile(deposit);
    dataFile.setSourceData(URI.create(REFERENCE_FILE));
    dataFile.setFileSize(REFERENCE_FILE_SIZE);
    dataFile.setDataCategory(dataFileCategory);
    dataFile.setDataType(datafileType);
    if (!StringTool.isNullOrEmpty(relativeLocation)) {
      dataFile.setRelativeLocation(relativeLocation);
    }
    return dataFile;
  }

  private boolean isAllReadyInDataFileListOrError(List<DepositDataFile> dataFileList) {
    return dataFileList.stream()
            .allMatch(
                    df -> df.getStatus() == AbstractDataFile.DataFileStatus.READY || df.getStatus() == AbstractDataFile.DataFileStatus.IN_ERROR);
  }

  protected boolean isAllReadyInDataFileList(List<DepositDataFile> dataFileList) {
    return dataFileList.stream().allMatch(df -> df.getStatus() == DataFileStatus.READY);
  }

  public void waitDataFileIsReady(String depositId, int dataFileNumber) {
    List<DepositDataFile> dataFileList = this.depositClientService.getDepositDataFile(depositId);
    int count = 0;

    // Test if data file is present
    assertEquals(dataFileNumber, dataFileList.size());

    while (!this.isAllReadyInDataFileList(dataFileList)) {
      String initialMessage = "Deposit data files of deposit " + depositId + " are not all in ready status.";
      StringBuilder builder = new StringBuilder().append(initialMessage);
      for (final DepositDataFile dataFile : dataFileList) {
        builder.append("\n Deposit datafile ")
                .append(dataFile.getResId())
                .append(" has status ")
                .append(dataFile.getStatus());
      }
      DLCMTestConstants.failAfterNAttempt(count, builder.toString());
      dataFileList = this.depositClientService.getDepositDataFile(depositId);
      count++;
    }
  }

  public void addDataFileToDeposit(String depositId, DepositDataFile dataFile) {
    this.depositClientService.addDepositDataFile(depositId, dataFile);
  }

  public DepositDataFile uploadDataFileToDeposit(String depositId, Resource file, FileUploadDto<DepositDataFile> fileInfo) {
    return this.depositClientService.uploadDepositDataFile(depositId, file, fileInfo);
  }

  public List<DepositDataFile> uploadArchiveToDeposit(String depositId, Resource file, FileUploadDto<DepositDataFile> fileInfo) {
    return this.depositClientService.uploadDepositDataFileArchive(depositId, file, fileInfo);
  }

  public List<DepositDataFile> uploadArchiveToDeposit(String depositId, Resource file) {
    return this.depositClientService.uploadDepositDataFileArchive(depositId, file);
  }

  // ==============
  // Deposit Clean
  // ==============

  private void clearDepositsFixtures() {
    this.depositClientService.findAll().stream()
            .filter(d -> d.getTitle().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(d -> {
              try {
                this.depositClientService.delete(d.getResId());
                log.info("Delete deposit {} ({}) with status ({})", d.getTitle(), d.getResId(), d.getStatus());
              } catch (final RuntimeException e) {
                log.info("Cannot delete deposit {} ({}) with status {}", d.getTitle(), d.getResId(), d.getStatus());
              }
            });
  }

  public void clearDepositsFixtures(OrganizationalUnit organizationalUnit) {
    this.depositClientService.findAll().stream()
            .filter(d -> d.getTitle().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL) &&
                    d.getOrganizationalUnitId().equals(organizationalUnit.getResId()))
            .forEach(d -> {
              try {
                this.depositClientService.delete(d.getResId());
              } catch (final RuntimeException e) {
                log.debug("Cannot delete deposit {} ({}) with status {}", d.getTitle(), d.getResId(), d.getStatus());
              }
            });
  }

  @Override
  public void cleanTestData() {
    this.clearDepositsFixtures();
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - InstitutionITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Institution;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.admin.InstitutionClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;

@Service
public class InstitutionITService extends ITService {

  // Main services
  private final PersonITService personITService;
  private final InstitutionClientService institutionClientService;

  public InstitutionITService(PersonITService personITService, InstitutionClientService institutionClientService) {
    this.personITService = personITService;
    this.institutionClientService = institutionClientService;
  }

  public Institution createLocalInstitution(PersistenceMode persistenceMode, String role) {
    final String institutionName = this.getInstitutionName(persistenceMode, role);
    final Institution institution = new Institution();
    institution.setName(institutionName);
    this.setRandomUrlInstitution(institution);
    return institution;
  }

  public List<Institution> findInstitutions(String name) {
    return this.institutionClientService.searchByProperties(Map.of("name", name));
  }

  public Institution createRemoteInstitution(String description) {
    Institution institution = new Institution();
    institution.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel(description));
    return this.institutionClientService.create(institution);
  }

  public Institution createRemoteInstitution(Institution institution) {
    return this.institutionClientService.create(institution);
  }

  public void enforceOrganizationalUnitInInstitution(Institution institution, OrganizationalUnit organizationalUnit) {
    OrganizationalUnit effectiveOrganizationalUnit = this.institutionClientService.getOrganizationalUnit(institution.getResId(),
            organizationalUnit.getResId());
    if (effectiveOrganizationalUnit == null) {
      this.institutionClientService.addOrganizationalUnit(institution.getResId(), organizationalUnit.getResId());
    }
  }

  public void enforcePersonHasRoleInInstitution(Institution institution, Person person, String role) {
    Role effectiveRole = this.institutionClientService.getPersonRole(institution.getResId(), person.getResId());
    if (effectiveRole == null) {
      this.institutionClientService.addPersonRole(institution.getResId(), person.getResId(), role);
    } else if (role.equals(effectiveRole.getResId())) {
      return;
    } else {
      this.institutionClientService.updatePersonRole(institution.getResId(), person.getResId(), role);
    }
  }

  private String getInstitutionName(PersistenceMode persistenceMode, String role) {
    String name;
    if (role == null) {
      name = "Institution";
    } else {
      name = "Institution for " + role.toLowerCase();
    }
    return this.createLabel(persistenceMode, name);
  }

  public Institution getOrCreatePermanentInstitution(PersistenceMode persistenceMode, String role, String applicationRole) {
    Institution institution = this.createLocalInstitution(persistenceMode, role);
    final String institutionName = institution.getName();

    final List<Institution> institutions = this.institutionClientService.searchByProperties(Map.of("name", institutionName));

    final Optional<Institution> optionalInstitution = institutions.stream().filter(item -> item.getName().equals(institutionName)).findFirst();

    if (optionalInstitution.isPresent()) {
      institution = this.institutionClientService.update(optionalInstitution.get().getResId(), institution);
    } else {
      institution = this.institutionClientService.create(institution);
    }

    // Add person
    final Person person = this.personITService.getOrCreateRemotePerson(applicationRole);
    this.enforcePersonHasRoleInInstitution(institution, person, role);

    institution.setOrganizationalUnit(new ArrayList<>());
    return institution;
  }

  public Institution getTestInstitutionAsUser(PersistenceMode persistenceMode, String role) {
    final Institution institution = this.createLocalInstitution(persistenceMode, role);
    final String institutionName = institution.getName();

    final List<Institution> institutions = this.institutionClientService.searchByProperties(Map.of("name", institutionName));

    Institution institutionResult = institutions.stream().filter(item -> item.getName().equals(institutionName)).findFirst().orElse(null);
    institutionResult.setOrganizationalUnit(new ArrayList<>());
    return institutionResult;
  }

  private void setRandomUrlInstitution(Institution institution) {
    institution.setUrl(this.generateRandomUrl());
  }

  public Institution getInstitution(String instituionId) {
    return this.institutionClientService.findOne(instituionId);
  }

  public Institution uploadLogo(String instituionId, ClassPathResource logoToUpload) {
    this.institutionClientService.uploadLogo(instituionId, logoToUpload);
    return this.institutionClientService.findOne(instituionId);
  }

  public void downloadLogo(String instituionId, Path path) {
    this.institutionClientService.downloadLogo(instituionId, path);
  }

  public void deleteLogo(String instituionId) {
    this.institutionClientService.deleteLogo(instituionId);
  }

  public Institution updateInstitution(String instituionId, Institution institution) {
    return this.institutionClientService.update(instituionId, institution);
  }

  public void addPersonRole(String instituionId, String personId, String roleId) {
    this.institutionClientService.addPersonRole(instituionId, personId, roleId);
  }

  public Role getPersonRole(String instituionId, String personId) {
    return this.institutionClientService.getPersonRole(instituionId, personId);
  }

  protected void clearInstitutionsFixtures() {

    final List<Institution> institutions = this.institutionClientService.findAll();

    for (final Institution institution : institutions) {

      if (institution.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL)) {

        /*
         * Remove eventual links with people first
         */
        final List<Person> people = this.institutionClientService.getMembers(institution.getResId());
        if (people != null && !people.isEmpty()) {
          for (final Person person : people) {
            this.institutionClientService.removeMember(institution.getResId(), person.getResId());
          }
        }

        /*
         * Remove eventual links with organizational units first
         */
        final List<OrganizationalUnit> units = this.institutionClientService.getOrganizationalUnits(institution.getResId());
        if (units != null && !units.isEmpty()) {
          for (final OrganizationalUnit unit : units) {
            this.institutionClientService.removeOrganizationalUnit(institution.getResId(), unit.getResId());
          }
        }

        this.institutionClientService.delete(institution.getResId());
      }
    }
  }

  @Override
  public void cleanTestData() {
    this.clearInstitutionsFixtures();
  }

}

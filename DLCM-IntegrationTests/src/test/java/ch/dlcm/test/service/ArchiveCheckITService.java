/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveCheckITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.EmbargoInfo;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.AipDataFile;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.RepresentationInfo;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.archivalstorage.AIPClientServiceSecondStorage;
import ch.dlcm.service.ingest.SIPClientService;

@Service
public class ArchiveCheckITService extends ITService {

  private static final Logger log = LoggerFactory.getLogger(ArchiveCheckITService.class);

  private OrgUnitITService orgUnitITService;
  private DepositITService depositITService;

  private SIPClientService sipClientService;
  private AIPClientService aipClientService;
  private AIPClientServiceSecondStorage aipClientServiceSecondStorage;
  private MetadataClientService metadataClientService;

  public ArchiveCheckITService(
          OrgUnitITService orgUnitITService,
          DepositITService depositITService,
          SIPClientService sipClientService,
          AIPClientService aipClientService,
          @Qualifier("aipClientServiceForSecondStorage") AIPClientServiceSecondStorage aipClientServiceSecondStorage,
          MetadataClientService metadataClientService) {
    // Dependencies
    this.sipClientService = sipClientService;
    this.aipClientService = aipClientService;
    this.aipClientServiceSecondStorage = aipClientServiceSecondStorage;
    this.metadataClientService = metadataClientService;
    // Services
    this.depositITService = depositITService;
    this.orgUnitITService = orgUnitITService;
  }

  @Override
  public void cleanTestData() {
    // Do nothing
  }

  public void verifyDeposit(Deposit deposit) {
    this.checkDeposit(deposit);
    this.checkSIP(deposit);
    this.checkAIP(deposit, this.aipClientService);
  }

  protected void checkDeposit(Deposit deposit) {
    log.info("Deposit [{}] {}", deposit.getResId(), deposit.getTitle());
    assertTrue(deposit.getStatus() == DepositStatus.COMPLETED
            || deposit.getStatus() == DepositStatus.CLEANED,
            "Deposit not completed: " + deposit.getResId());
  }

  protected void checkSIP(Deposit deposit) {
    RepresentationInfo depositInfo = this.getDepositInfo(deposit);
    SubmissionInfoPackage sip = this.sipClientService.findOne(deposit.getSipId());
    assertNotNull(sip, "Cannot find SIP for deposit " + deposit.getResId());
    log.info("SIP     [{}] {}", sip.getResId(), sip.getInfo().getName());
    assertTrue(sip.getInfo().getStatus() == PackageStatus.COMPLETED
            || sip.getInfo().getStatus() == PackageStatus.CLEANED,
            "SIP not completed for deposit " + deposit.getResId());
    this.checkInfo(deposit.getResId(), "SIP => ", depositInfo, sip.getInfo(), true);
    List<ArchivalInfoPackage> depositCollection = this.depositITService.getDepositAip(deposit.getResId());
    if (!depositCollection.isEmpty()) {
      this.checkCollectionContent(
              deposit.getResId(),
              depositCollection,
              this.sipClientService.getAip(sip.getResId()));
    }
  }

  protected void checkAIP(Deposit deposit, AIPClientService aipService) {
    RepresentationInfo depositInfo = this.getDepositInfo(deposit);
    SubmissionInfoPackage sip = this.sipClientService.findOne(deposit.getSipId());
    ArchivalInfoPackage aip = aipService.findOne(sip.getAipId());
    assertNotNull(aip, "Cannot find AIP for deposit " + deposit.getResId());
    log.info("AIP     [{}] {}", aip.getResId(), aip.getInfo().getName());
    assertEquals(PackageStatus.COMPLETED, aip.getInfo().getStatus(), "AIP not completed for deposit " + deposit.getResId());
    this.checkInfo(deposit.getResId(), "AIP =>", depositInfo, aip.getInfo(), true);
    List<AipDataFile> aipDataFiles = aipService.getAipDataFile(aip.getResId());
    assertTrue(aipDataFiles.isEmpty(), "AIP contains data files for deposit" + deposit.getResId());
    List<ArchivalInfoPackage> depositCollection = this.depositITService.getDepositAip(deposit.getResId());
    if (!depositCollection.isEmpty()) {
      this.checkCollectionContent(
              deposit.getResId(),
              depositCollection,
              this.aipClientService.getAip(aip.getResId()));
    }
  }

  public void checkDepositsByOrgUnit(String orgUnitName) {
    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrgUnit(orgUnitName);
    assertNotNull(orgUnit, "Cannot find orgUnit: " + orgUnitName);
    log.info("Checking deposits");
    for (final Deposit deposit : this.depositITService.listDeposits()) {
      if (!deposit.getOrganizationalUnitId().equals(orgUnit.getResId())) {
        continue;
      }
      this.checkDeposit(deposit);
    }
  }

  public void checkSIPByOrgUnit(String orgUnitName) {
    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrgUnit(orgUnitName);
    assertNotNull(orgUnit, "Cannot find orgUnit: " + orgUnitName);
    log.info("Checking SIP");
    for (final Deposit deposit : this.depositITService.listDeposits()) {
      if (!deposit.getOrganizationalUnitId().equals(orgUnit.getResId())) {
        continue;
      }
      // Check
      this.checkDeposit(deposit);
      this.checkSIP(deposit);
    }
  }

  public void checkAIPOnMainStorageByOrgUnit(String orgUnitName) {
    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrgUnit(orgUnitName);
    assertNotNull(orgUnit, "Cannot find orgUnit: " + orgUnitName);
    log.info("Checking AIP [Main storage]");
    for (final Deposit deposit : this.depositITService.listDeposits()) {
      if (!deposit.getOrganizationalUnitId().equals(orgUnit.getResId())) {
        continue;
      }
      // Check
      this.checkDeposit(deposit);
      this.checkAIP(deposit, this.aipClientService);
    }
  }

  public void checkAIPOnReplicaStorage(String orgUnitName) {
    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrgUnit(orgUnitName);
    assertNotNull(orgUnit, "Cannot find orgUnit: " + orgUnitName);
    log.info("Checking AIP [Replica storage]");
    for (final Deposit deposit : this.depositITService.listDeposits()) {
      if (!deposit.getOrganizationalUnitId().equals(orgUnit.getResId())) {
        continue;
      }
      // Check
      this.checkDeposit(deposit);
      this.checkAIP(deposit, this.aipClientServiceSecondStorage);
    }
  }

  public void ByOrgUnitByOrgUnit(String orgUnitName) {
    final OrganizationalUnit orgUnit = this.orgUnitITService.getOrgUnit(orgUnitName);
    assertNotNull(orgUnit, "Cannot find orgUnit: " + orgUnitName);
    log.info("Checking archives");
    for (final Deposit deposit : this.depositITService.listDeposits()) {
      if (!deposit.getOrganizationalUnitId().equals(orgUnit.getResId())) {
        continue;
      }
      // Check
      this.checkDeposit(deposit);
      this.checkArchive(deposit);
    }
  }

  public RepresentationInfo getDepositInfo(Deposit deposit) {
    RepresentationInfo info = new RepresentationInfo();
    info.setMetadataVersion(deposit.getMetadataVersion());
    info.setName(deposit.getTitle());
    info.setDescription(StringTool.truncateOnSpaceWithEllipsis(
            deposit.getDescription(),
            SolidifyConstants.DB_LONG_STRING_LENGTH));
    info.setOrganizationalUnitId(deposit.getOrganizationalUnitId());
    info.setAccess(deposit.getAccess());
    info.setEmbargo(deposit.getEmbargo());
    info.setDataSensitivity(deposit.getDataSensitivity());
    info.setLicenseId(deposit.getLicenseId());
    info.setComplianceLevel(deposit.getComplianceLevel());
    info.setContainsUpdatedMetadata(deposit.getContainsUpdatedMetadata());
    return info;
  }

  private RepresentationInfo getArchiveInfo(ArchiveMetadata archive) {
    RepresentationInfo info = new RepresentationInfo();
    info.setMetadataVersion(DLCMMetadataVersion.fromVersion(archive.getMetadataVersion()));
    info.setName(archive.getTitle());
    info.setDescription(StringTool.truncateOnSpaceWithEllipsis(
            archive.getAbstract(),
            SolidifyConstants.DB_LONG_STRING_LENGTH));
    info.setOrganizationalUnitId(archive.getOrganizationalUnitId());
    info.setAccess(archive.getFinalAccessLevel());
    if (archive.getEmbargoAccessLevel().isPresent()
            && !archive.getFinalAccessLevel().equals(archive.getEmbargoAccessLevel().get())) {
      OffsetDateTime startDate = null;
      OffsetDateTime endDate = null;
      EmbargoInfo embargo = new EmbargoInfo();
      embargo.setAccess(archive.getEmbargoAccessLevel().get());
      for (Map<String, Object> dateInfo : (ArrayList<Map<String, Object>>) archive
              .getMetadata(DLCMConstants.DATES_FIELD.split(SolidifyConstants.REGEX_FIELD_PATH_SEP))) {
        if (((String) dateInfo.get("dateType")).equals("Created")) {
          startDate = StringTool.getDate((String) dateInfo.get("content"));
        } else if (((String) dateInfo.get("dateType")).equals("Available")) {
          endDate = StringTool.getDate((String) dateInfo.get("content"));
        }
      }
      embargo.setMonths((int) ChronoUnit.MONTHS.between(startDate, endDate));
      embargo.setStartDate(startDate);
      info.setEmbargo(embargo);
    } else {
      info.setEmbargo(null);
    }
    info.setDataSensitivity(DataTag.valueOf((String) archive.getMetadata(DLCMConstants.AIP_DATA_TAG)));
    // info.setLicenseId(archive.getLicenseId());
    info.setComplianceLevel(ComplianceLevel.valueOf((String) archive.getMetadata(DLCMConstants.AIP_COMPLIANCE_LEVEL)));
    return info;
  }

  public void checkInfo(String depositId, String prefix, RepresentationInfo info1, RepresentationInfo info2, boolean allFields) {
    assertEquals(info1.getMetadataVersion(), info2.getMetadataVersion(), prefix + "Not same metadata version for deposit [" + depositId + "]");
    assertEquals(info1.getName(), info2.getName(), prefix + "Not same name");
    assertEquals(info1.getOrganizationalUnitId(), info2.getOrganizationalUnitId(), prefix + "Not same orgUnit for deposit [" + depositId + "]");
    assertEquals(info1.getAccess(), info2.getAccess(), prefix + "Not same access level for deposit " + depositId + "]");
    if (info1.getEmbargo() != null && info2.getEmbargo() != null) {
      assertEquals(info1.getEmbargo().getAccess(), info2.getEmbargo().getAccess(),
              prefix + "Not same embargo access level for deposit [" + depositId + "]");
      assertEquals(info1.getEmbargo().getMonths(), info2.getEmbargo().getMonths(),
              prefix + "Not same embargo month number for deposit [" + depositId + "]");
      if (info1.getEmbargo().getStartDate() != null && info2.getEmbargo().getStartDate() != null) {
        assertEquals(info1.getEmbargo().getStartDate().getYear(), info2.getEmbargo().getStartDate().getYear(),
                prefix + "Not same embargo start date (year) for deposit [" + depositId + "]");
        assertEquals(info1.getEmbargo().getStartDate().getMonth(), info2.getEmbargo().getStartDate().getMonth(),
                prefix + "Not same embargo start date (month) for deposit [" + depositId + "]");
        assertEquals(info1.getEmbargo().getStartDate().getDayOfMonth(), info2.getEmbargo().getStartDate().getDayOfMonth(),
                prefix + "Not same embargo start date (day) for deposit [" + depositId + "]");
      }
    } else if ((info1.getEmbargo() != null && info2.getEmbargo() == null)
            || (info1.getEmbargo() == null && info2.getEmbargo() != null)) {
      assertTrue(false, prefix + "Not same embargo for deposit [" + depositId + "]");
    }
    assertEquals(info1.getDataSensitivity(), info2.getDataSensitivity(), prefix + "Not same data tag for deposit [" + depositId + "]");
    assertEquals(info1.getComplianceLevel(), info2.getComplianceLevel(), prefix + "Not same compliance level for deposit [" + depositId + "]");
    if (allFields) {
      assertEquals(info1.getLicenseId(), info2.getLicenseId(), prefix + "Not same license for deposit [" + depositId + "]");
    }
  }

  private void checkCollectionContent(String depositId, List<ArchivalInfoPackage> aipList1, List<ArchivalInfoPackage> aipList2) {
    assertEquals(aipList1.size(), aipList2.size(), "Different collection content number for deposit " + depositId);
    List<String> ids = aipList1.stream().map(ArchivalInfoPackage::getResId).collect(Collectors.toList());
    ids.removeAll(aipList2.stream().map(ArchivalInfoPackage::getResId).collect(Collectors.toList()));
    assertEquals(0, ids.size(), "Different collection content for deposit " + depositId);
  }

  protected void checkArchive(Deposit deposit) {
    RepresentationInfo depositInfo = this.getDepositInfo(deposit);
    SubmissionInfoPackage sip = this.sipClientService.findOne(deposit.getSipId());
    ArchiveMetadata metadata = this.metadataClientService.findOne(sip.getAipId());
    assertNotNull(metadata, "Cannot find archive for deposit " + deposit.getResId());
    RepresentationInfo archiveInfo = this.getArchiveInfo(metadata);
    log.info("Archive [{}] {}", metadata.getResId(), archiveInfo.getName());
    this.checkInfo(deposit.getResId(), "Archive =>", depositInfo, archiveInfo, false);
    this.checkMetadataInfo(deposit.getResId(), deposit, metadata);
  }

  private void checkMetadataInfo(String depositId, Deposit deposit, ArchiveMetadata metadata) {
    // DOI
    assertEquals(deposit.getDoi(), metadata.getDoi(), "Not same DOI for deposit [" + depositId + "]");
    // Related DOI
    String identicalToDoi = null;
    List<String> referencedByDoi = Collections.emptyList();
    String obsoletedByDoi = null;
    for (Map<String, Object> relationInfo : metadata.getRelations()) {
      if (((String) relationInfo.get(DLCMConstants.DATACITE_RELATION_ID_TYPE_NAME)).equals(DLCMConstants.DOI)) {
        if (((String) relationInfo.get(DLCMConstants.DATACITE_RELATION_TYPE_NAME)).equals("IsIdenticalTo")) {
          identicalToDoi = (String) relationInfo.get(DLCMConstants.CONTENT_FIELD);
        } else if (((String) relationInfo.get(DLCMConstants.DATACITE_RELATION_TYPE_NAME)).equals("IsReferencedBy")) {
          referencedByDoi = Arrays.asList(((String) relationInfo.get(DLCMConstants.CONTENT_FIELD)).split(SolidifyConstants.FIELD_SEP));
        } else if (((String) relationInfo.get(DLCMConstants.DATACITE_RELATION_TYPE_NAME)).equals("IsObsoletedBy")) {
          obsoletedByDoi = (String) relationInfo.get(DLCMConstants.CONTENT_FIELD);
        }
      }
    }
    assertEquals(deposit.getIsIdenticalTo(), identicalToDoi, "Not same 'isIdenticalTo' DOI for deposit [" + depositId + "]");
    assertEquals(deposit.getIsReferencedBy(), referencedByDoi, "Not same 'IsReferencedBy' DOI for deposit [" + depositId + "]");
    assertEquals(deposit.getIsObsoletedBy(), obsoletedByDoi, "Not same 'IsObsoletedBy' DOI for deposit [" + depositId + "]");
    assertEquals(deposit.getKeywords(), metadata.getKeywords(), "Not same keywords for deposit [" + depositId + "]");
    // Collection
    List<ArchivalInfoPackage> depositCollection = this.depositITService.getDepositAip(deposit.getResId());
    List<ArchivalInfoPackage> metadataCollection = new ArrayList<>();
    for (Map<String, Object> relationInfo : metadata.getRelations()) {
      if (((String) relationInfo.get(DLCMConstants.DATACITE_RELATION_TYPE_NAME)).equals("HasPart")) {
        if (((String) relationInfo.get(DLCMConstants.DATACITE_RELATION_ID_TYPE_NAME)).equals(DLCMConstants.DOI)) {
          ArchiveMetadata archive = this.metadataClientService.searchByDoi((String) relationInfo.get(DLCMConstants.CONTENT_FIELD));
          metadataCollection.add(this.aipClientService.findOne(archive.getResId()));
        } else if (((String) relationInfo.get(DLCMConstants.DATACITE_RELATION_ID_TYPE_NAME)).equals(DLCMConstants.HANDLE)) {
          metadataCollection.add(this.aipClientService.findOne((String) relationInfo.get(DLCMConstants.CONTENT_FIELD)));
        }
      }
    }
    if (!depositCollection.isEmpty()) {
      this.checkCollectionContent(deposit.getResId(), depositCollection, metadataCollection);
    }
  }
}

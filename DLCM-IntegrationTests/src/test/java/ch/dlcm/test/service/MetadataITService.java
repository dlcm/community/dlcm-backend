/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - MetadataITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;

import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.service.access.MetadataClientService;

@Service
public class MetadataITService extends ITService {

  private static final String DEPOSIT_ID = "packages.deposits.id";
  private static final String SIP_ID = "packages.sip.id";

  private final MetadataClientService metadataClientService;

  public MetadataITService(MetadataClientService metadataClientService) {
    // Services
    this.metadataClientService = metadataClientService;
  }

  public MetadataClientService getMetadataService() {
    return this.metadataClientService;
  }

  public String getAipIdFromDepositId(String depositId) {
    ArchiveMetadata aip = this.getPackage(DEPOSIT_ID, depositId);
    return aip.getResId();
  }

  public String getAipIdFromSipId(String sipId) {
    ArchiveMetadata aip = this.getPackage(SIP_ID, sipId);
    return aip.getResId();
  }

  public ArchiveMetadata getArchiveMetadata(String aipId) {
    return this.metadataClientService.findOne(aipId);
  }

  private ArchiveMetadata getPackage(String fieldToSearch, String valueToSearch) {
    final String query = "type:package AND " + fieldToSearch + ":\"" + valueToSearch + "\"";
    RestCollection<ArchiveMetadata> result = this.metadataClientService.searchPrivateIndex(query);
    if (result.getPage().getTotalItems() == 1) {
      return result.getData().get(0);
    } else if (result.getPage().getTotalItems() == 0) {
      throw new SolidifyResourceNotFoundException("Archive not found: " + fieldToSearch + " = " + valueToSearch);
    } else {
      throw new SolidifyRuntimeException("Too many archives: " + query);
    }
  }

  @Override
  public void cleanTestData() {
    // TODO Auto-generated method stub

  }

}

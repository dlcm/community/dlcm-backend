/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveAclITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;

import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveACL;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.service.admin.ArchiveACLClientService;

@Service
public class ArchiveAclITService extends ITService {

  private final ArchiveACLClientService archiveAclClientService;
  private final PersonITService personITService;

  public ArchiveAclITService(ArchiveACLClientService archiveAclClientService, PersonITService personITService) {
    this.archiveAclClientService = archiveAclClientService;
    this.personITService = personITService;
  }

  public void deleteArchiveAcl(String aipId, String userId) {
  Map<String, String> map = new HashMap<>();
      map.put("aipId", aipId);
      map.put("user.resId", userId);
  List<ArchiveACL> archiveACLList = this.archiveAclClientService.searchByProperties(map);
      if (!archiveACLList.isEmpty()) {
        Optional<ArchiveACL> archiveACLOpt = archiveACLList.stream().filter(acl -> acl.isDeleted().equals(false)).findFirst();
        archiveACLOpt.ifPresent(acl -> this.archiveAclClientService.delete(acl.getResId()));
      }
  }

  public ArchiveACL getPermanentAclForThirdPartyUserOnClosedArchive() {
    final User user = this.personITService.findThirdPartyUser();
    final List<ArchiveACL> archiveAclList = archiveAclClientService.searchByProperties(Map.of("user.resId", user.getResId()));
    if (archiveAclList.isEmpty()) {
      throw new SolidifyResourceNotFoundException("Third party user has no archive ACL");
    } else if (archiveAclList.size() == 1) {
      return archiveAclList.get(0);
    } else {
      throw new IllegalStateException("Third party user should have only one ACL");
    }
  }

  public ArchiveACL testArchiveAclCreation(User user, OrganizationalUnit orgUnit, String aipId) {
    final ArchiveACL archiveACL = new ArchiveACL();
    archiveACL.setAipId(aipId);
    archiveACL.setUser(user);
    archiveACL.setOrganizationalUnit(orgUnit);
    ArchiveACL remoteArchiveAcl = this.archiveAclClientService.createAndUploadDuaFile(archiveACL, new ClassPathResource("bootstrap.yml"));

    assertEquals(archiveACL.getUser().getResId(), remoteArchiveAcl.getUser().getResId());
    assertEquals(archiveACL.getOrganizationalUnit().getResId(), remoteArchiveAcl.getOrganizationalUnit().getResId());
    assertEquals(archiveACL.getAipId(), remoteArchiveAcl.getAipId());

    return remoteArchiveAcl;
  }

  @Override
  public void cleanTestData() {
    // Do nothing
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - NotificationITService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ch.dlcm.model.notification.Notification;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.service.admin.NotificationClientService;

@Service
public class NotificationITService extends ITService {

  public NotificationClientService notificationClientService;

  public NotificationITService(NotificationClientService notificationClientService) {
    this.notificationClientService = notificationClientService;
  }

  public List<Notification> getInboxNotifications(NotificationType notificationType) {
    return this.notificationClientService.getInboxNotifications(notificationType);
  }

  @Override
  public void cleanTestData() {

  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveAccessIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.access;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.stream.Stream;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.service.access.AIPDownloadClientService;
import ch.dlcm.service.access.ArchiveClientService;
import ch.dlcm.service.access.OrderClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.MetadataITService;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ArchiveAccessIT extends AbstractAccessIT {

  private final DepositITService depositITService;
  private final AipITService aipITService;
  private final MetadataITService metadataITService;
  private final ArchiveClientService archiveClientService;

  @Autowired
  public ArchiveAccessIT(
          Environment env,
          SudoRestClientTool restClientTool,
          AIPClientService aipClientService,
          OrderClientService orderClientService,
          AIPDownloadClientService aipDownloadClientService,
          DepositITService depositITService,
          AipITService aipITService,
          MetadataITService metadataITService,
          ArchiveClientService archiveClientService) {
    super(env, restClientTool, aipClientService, orderClientService, aipDownloadClientService);
    this.depositITService = depositITService;
    this.aipITService = aipITService;
    this.metadataITService = metadataITService;
    this.archiveClientService = archiveClientService;
  }

  @Order(10)
  @ParameterizedTest
  @MethodSource("supportedVersions")
  void testArchiveByResId(DLCMMetadataVersion version) {
    this.restClientTool.sudoAdmin();
    final Deposit deposit = this.depositITService
            .getRemotePermanentDeposit(DLCMTestConstants.PERMANENT_TEST_DATA_LABEL + DLCMTestConstants.DEPOSIT_BY_VERSION + version.toString());
    final ArchivalInfoPackage aip = this.getAIP(deposit);
    this.restClientTool.exitSudo();
    // objects not null
    assertNotNull(deposit);
    assertNotNull(aip);
    // Version
    assertEquals(version, deposit.getMetadataVersion());
    assertEquals(version, aip.getMetadataVersion());
    // Access by resId
    this.checkArchive(deposit, aip, this.metadataITService.getArchiveMetadata(aip.getResId()));
    // Direct Access by resId
    this.checkArchive(deposit, aip, this.archiveClientService.getMetadataByResId(aip.getResId()));
    // Direct Access by DOI
    this.checkArchive(deposit, aip, this.archiveClientService.getMetadataByDOI(deposit.getDoi()));
    if (deposit.getMetadataVersion().getVersionNumber() >= DLCMMetadataVersion.V3_0.getVersionNumber()) {
      // Direct Access by ARK
      this.checkArchive(deposit, aip, this.archiveClientService.getMetadataByARK(deposit.getArk()));
    }
  }

  private void checkArchive(Deposit deposit, ArchivalInfoPackage aip, ArchiveMetadata archive) {
    assertEquals(aip.getResId(), archive.getResId());
    assertEquals(deposit.getDoi(), archive.getDoi());
    if (deposit.getMetadataVersion().getVersionNumber() >= DLCMMetadataVersion.V3_0.getVersionNumber()) {
      assertEquals(deposit.getArk(), archive.getArk());
    } else {
      assertNull(deposit.getArk());
      assertNull(archive.getArk());
    }
  }

  private ArchivalInfoPackage getAIP(Deposit deposit) {
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
    final String aipId = this.metadataITService.getAipIdFromDepositId(deposit.getResId());
    return this.aipITService.findAIP(aipId);
  }

  private static Stream<Arguments> supportedVersions() {
    return Stream.of(
            arguments(DLCMMetadataVersion.V1_0),
            arguments(DLCMMetadataVersion.V1_1),
            arguments(DLCMMetadataVersion.V2_0),
            arguments(DLCMMetadataVersion.V2_1),
            arguments(DLCMMetadataVersion.V3_0),
            arguments(DLCMMetadataVersion.V3_1));
  }
}

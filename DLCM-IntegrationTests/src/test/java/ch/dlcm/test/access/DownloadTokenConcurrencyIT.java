/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - DownloadTokenConcurrencyIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.access;

import static ch.dlcm.rest.ResourceName.PUBLIC_METADATA;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.service.access.AIPDownloadClientService;
import ch.dlcm.service.access.OrderClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;

@Disabled("Run these tests manually")
class DownloadTokenConcurrencyIT extends AbstractAccessIT {

  @Value("${dlcm.module.access.url}")
  private String accessUrl;

  private static final int NB_THREADS = 20;
  private static final int NB_CALLS = 1000;

  @Autowired
  public DownloadTokenConcurrencyIT(
          Environment env,
          SudoRestClientTool restClientTool,
          AIPClientService aipService,
          OrderClientService orderService,
          AIPDownloadClientService aipDownloadService) {
    super(env, restClientTool, aipService, orderService, aipDownloadService);
  }

  @Test
  void downloadTokenConcurrencyTest() throws InterruptedException {
    final String resId = this.aipDownloadClientService.findAll().get(0).getResId();
    final String downloadTokenUrl = this.accessUrl + "/" + PUBLIC_METADATA + "/" + resId + "/" + ActionName.DOWNLOAD_TOKEN;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    this.runParallelCalls(() -> this.callDownloadTokenEndpoint(downloadTokenUrl, restTemplate), NB_THREADS, NB_CALLS);
  }

  private void callDownloadTokenEndpoint(String downloadTokenUrl, RestTemplate restTemplate) {
    restTemplate.getForEntity(downloadTokenUrl, String.class);
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchiveAccessAsManagerIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.access;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.security.Role;
import ch.dlcm.service.access.AIPDownloadClientService;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.access.OrderClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.OrderITService;

class ArchiveAccessAsManagerIT extends AbstractArchiveAccess {

  @Autowired
  public ArchiveAccessAsManagerIT(
          Environment env,
          SudoRestClientTool restClientTool,
          AIPClientService aipClientService,
          OrderClientService orderClientService,
          AIPDownloadClientService aipDownloadClientService,
          MetadataClientService metadataClientService,
          OrderITService orderITService) {
    super(env, restClientTool, aipClientService, orderClientService, aipDownloadClientService, metadataClientService, orderITService);
  }

  @Override
  @Test
  public void downloadArchiveTest() {
    super.downloadArchiveTest();
  }

  @Override
  protected String role() {
    return Role.MANAGER_ID;
  }

  @Test
  void cannotDownloadArchiveWithoutMFATest() {
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(
            Map.of("info.name", DLCMTestConstants.DEPOSIT_WITH_SENSITIVE_DATA_CRIMSON));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    final ArchivalInfoPackage aip = aipList.get(0);
    final DataTag dataSensitivity = aip.getInfo().getDataSensitivity();
    if (!dataSensitivity.equals(DataTag.CRIMSON)) {
      fail("Data sensitivity should be CRIMSON instead of " + dataSensitivity);
    }
    this.restClientTool.sudoRoot();
    assertThrows(HttpClientErrorException.Unauthorized.class, () -> this.metadataClientService.prepareDownload(aip.getResId(),
            DLCMConstants.DISSEMINATION_POLICY_OAIS_ID));
    final Path tmpPath = Paths.get(System.getProperty("java.io.tmpdir"), "download1.tmp");
    assertThrows(HttpClientErrorException.Unauthorized.class,
            () -> this.metadataClientService.downloadArchive(aip.getResId(), tmpPath));
    this.restClientTool.exitSudo();
  }
}

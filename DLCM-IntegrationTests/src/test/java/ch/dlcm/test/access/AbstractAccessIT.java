/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractAccessIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.access;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.access.QueryType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.access.AIPDownloadClientService;
import ch.dlcm.service.access.OrderClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.AbstractIT;
import ch.dlcm.test.DLCMTestConstants;

abstract class AbstractAccessIT extends AbstractIT {

  protected OrderClientService orderClientService;
  protected AIPDownloadClientService aipDownloadClientService;
  protected AIPClientService aipClientService;

  protected AbstractAccessIT(
          Environment env,
          SudoRestClientTool restClientTool,
          AIPClientService aipClientService,
          OrderClientService orderClientService,
          AIPDownloadClientService aipDownloadClientService) {
    super(env, restClientTool);
    this.aipClientService = aipClientService;
    this.orderClientService = orderClientService;
    this.aipDownloadClientService = aipDownloadClientService;
  }

  private Order buildDirectQuery(int num, boolean isUnit, boolean isCollection, boolean isNew) {
    // Get AIP id
    final List<String> aipList = this.getAIP(num, isUnit, isCollection, isNew);
    assertEquals(aipList.size(), num, "Wrong AIP number");

    // Create a deposit
    final Order order = new Order();
    order.setName(DLCMTestConstants
            .getRandomNameWithTemporaryLabel("Direct Query with " + num + " item(s) [unit=" + isUnit + ",collection=" + isCollection + "]"));
    order.setQueryType(QueryType.DIRECT);
    String aips = aipList.get(0);
    if (aipList.size() > 1) {
      for (int i = 1; i < aipList.size(); i++) {
        aips += ";" + aipList.get(i);
      }
    }
    order.setQuery(aips);
    return order;
  }

  protected Order buildSimpleQuery(String q) {
    // Create a deposit
    final Order order = new Order();
    order.setName(DLCMTestConstants.getRandomNameWithTemporaryLabel("Order"));
    order.setQueryType(QueryType.SIMPLE);
    order.setQuery(q);
    return order;
  }

  @Override
  protected void deleteFixtures() {
  }

  protected void executeDirectQuery(int num, boolean isUnit, boolean isCollection, boolean isNew) {
    final Order order = this.orderClientService.create(this.buildDirectQuery(num, isUnit, isCollection, isNew));
    assertNotNull(order, "Cannot create query order");
    this.executeQuery(order, true);
  }

  protected void executeQuery(Order order, boolean save) {
    if (save) {
      // Save AIP
      this.orderClientService.save(order.getResId());
    }

    // Submit query
    final Order order2 = new Order();
    order2.setStatus(OrderStatus.SUBMITTED);
    Order order3 = this.orderClientService.update(order.getResId(), order2);
    assertNotNull(order3, "Cannot save query order");

    // Check status
    int count = 0;

    while (order3.getStatus() != OrderStatus.READY && order3.getStatus() != OrderStatus.IN_ERROR) {
      DLCMTestConstants
              .failAfterNAttempt(count, "Order " + order3.getResId() + " should have status Ready or InError instead of " + order3.getStatus());
      order3 = this.orderClientService.findOne(order.getResId());
      count++;
    }

    // Check if ready
    if (save) {
      assertEquals(OrderStatus.READY, order3.getStatus(), "Wrong query order status");
    } else {
      assertEquals(OrderStatus.IN_ERROR, order3.getStatus(), "Wrong query order status");
    }
  }

  private List<String> getAIP(int max, boolean isUnit, boolean isCollection, boolean isNew) {
    final List<String> list = new ArrayList<>();
    int count = 0;

    if (!isUnit && !isCollection) {
      return list;
    }
    for (final ArchivalInfoPackage aip : this.aipClientService.findAll()) {
      if (aip.getInfo().getStatus() == PackageStatus.COMPLETED) {
        if ((isUnit && !aip.isCollection()) || (isCollection && aip.isCollection())) {
          // Check if already downloaded
          if (isNew && this.aipDownloadClientService.findOne(aip.getResId()) != null) {
            continue;
          }
          count++;
          list.add(aip.getResId());
          if (count == max) {
            break;
          }
        }
      }
    }
    return list;
  }
}

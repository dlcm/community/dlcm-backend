/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrgUnitIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.access;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.access.OrgUnit;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.service.access.OrgUnitClientService;
import ch.dlcm.service.admin.OrganizationalUnitClientService;
import ch.dlcm.test.AbstractIT;

class OrgUnitIT extends AbstractIT {

  @Value("${dlcm.module.access.url}")
  private String accessUrl;

  private final OrgUnitClientService orgUnitClientService;
  private final OrganizationalUnitClientService organizationalUnitClientService;

  @Autowired
  public OrgUnitIT(
          Environment env,
          SudoRestClientTool restClientTool,
          OrgUnitClientService orgUnitClientService,
          OrganizationalUnitClientService organizationalUnitClientService) {
    super(env, restClientTool);
    this.orgUnitClientService = orgUnitClientService;
    this.organizationalUnitClientService = organizationalUnitClientService;
  }

  @Test
  void createOrgUnitNotAllowed() {
    final String url = this.accessUrl + "/" + ResourceName.ORG_UNIT;
    try {
      final RestTemplate restTemplate = this.restClientTool.getClient();
      restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(new OrgUnit()), OrgUnit.class);
      fail("A METHOD_NOT_ALLOWED Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.METHOD_NOT_ALLOWED, e.getStatusCode());
    }
  }

  @Test
  void listOrgUnit() {
    final RestCollection<OrgUnit> orgUnits = this.orgUnitClientService.list();
    // check number of orgunits calling to admin
    final List<OrganizationalUnit> organizationalUnitList = this.organizationalUnitClientService.findAll();
    assertEquals(orgUnits.getPage().getTotalItems(), organizationalUnitList.size());
  }

  @Override
  protected void deleteFixtures() {
  }

}

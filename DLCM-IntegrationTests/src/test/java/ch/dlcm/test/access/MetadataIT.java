/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - MetadataIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.access;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.OrderSubsetItem;
import ch.dlcm.model.display.OrganizationalUnitDisseminationPolicyDTO;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.DisseminationInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.access.AIPDownloadClientService;
import ch.dlcm.service.access.DIPClientService;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.access.OrderClientService;
import ch.dlcm.service.admin.OrganizationalUnitDisseminationPolicyClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.service.datamgmt.DataMgmtClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrderITService;
import ch.dlcm.test.service.PersonITService;
import ch.unige.solidify.model.dto.CitationDto;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;
import ch.unige.solidify.util.ZipTool;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;

class MetadataIT extends AbstractAccessIT {
  private static final Logger logger = LoggerFactory.getLogger(MetadataIT.class);

  private final MetadataClientService metadataClientService;
  private final OrderITService orderITService;
  private final DataMgmtClientService dataMgmtClientService;
  private final DIPClientService dipClientService;
  private final DepositITService depositITService;
  private final PersonITService personITService;
  private final AipITService aipITService;
  private final OrganizationalUnitDisseminationPolicyClientService orgUnitDisseminationPolicyClientService;

  @Autowired
  public MetadataIT(
          Environment env,
          SudoRestClientTool restClientTool,
          AIPClientService aipClientService,
          OrderClientService orderClientService,
          AIPDownloadClientService aipDownloadClientService,
          DataMgmtClientService dataMgmtClientService,
          DIPClientService dipClientService,
          MetadataClientService metadataClientService,
          OrderITService orderITService,
          DepositITService depositITService,
          PersonITService personITService,
          AipITService aipITService, OrganizationalUnitDisseminationPolicyClientService orgUnitDisseminationPolicyClientService) {
    super(env, restClientTool, aipClientService, orderClientService, aipDownloadClientService);
    this.orderITService = orderITService;
    this.dataMgmtClientService = dataMgmtClientService;
    this.dipClientService = dipClientService;
    this.metadataClientService = metadataClientService;
    this.depositITService = depositITService;
    this.personITService = personITService;
    this.aipITService = aipITService;
    this.orgUnitDisseminationPolicyClientService = orgUnitDisseminationPolicyClientService;
  }

  @Test
  void downloadArchiveTest() throws IOException {
    // get the aip from collection created by init test
    final List<ArchivalInfoPackage> aipList = this.aipClientService
            .searchByProperties(Map.of("info.name", DLCMTestConstants.ARCHIVAL_UNIT_LABEL + " 1"));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    final String aipId = aipList.get(0).getResId();
    // check if order has been already created
    String orderId = this.metadataClientService.prepareDownload(aipId, DLCMConstants.DISSEMINATION_POLICY_OAIS_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    Path path1 = Paths.get(System.getProperty("java.io.tmpdir"), "download1.tmp");
    this.metadataClientService.downloadArchive(aipId, path1, orderId);
    assertNotNull(path1.toFile());

    // compare the archive with the one return by dip
    Path path2 = Paths.get(System.getProperty("java.io.tmpdir"), "download2.tmp");
    final DisseminationInfoPackage dip = this.dipClientService.searchByProperties(Map.of("info.name", aipId))
            .stream().max(Comparator.comparing(n -> n.getLastUpdate().getWhen())).get();
    this.dipClientService.downloadArchive(dip.getResId(), path2);

    assertNotNull(path2.toFile());
    assertArrayEquals(Files.readAllBytes(path1), Files.readAllBytes(path2));

    FileTool.deleteFile(path1);
    FileTool.deleteFile(path2);

  }

  @Test
  void checkFilePathInArchiveZipTest() throws IOException {
    // get the aip from collection created by init test
    final List<ArchivalInfoPackage> aipList = this.aipClientService
            .searchByProperties(Map.of("info.name", DLCMTestConstants.ARCHIVAL_UNIT_LABEL + " 1"));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    final String aipId = aipList.get(0).getResId();
    // check if order has been already created
    String orderId = this.metadataClientService.prepareDownload(aipId, DLCMConstants.DISSEMINATION_POLICY_OAIS_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    Path path1 = Paths.get(System.getProperty("java.io.tmpdir"), "download1.tmp");
    this.metadataClientService.downloadArchive(aipId, path1, orderId);
    assertNotNull(path1.toFile());

    // compare the archive with the one return by dip
    Path path2 = Paths.get(System.getProperty("java.io.tmpdir"), "download2.tmp");
    final DisseminationInfoPackage dip = this.dipClientService.searchByProperties(Map.of("info.name", aipId))
            .stream().max(Comparator.comparing(n -> n.getLastUpdate().getWhen())).get();
    this.dipClientService.downloadArchive(dip.getResId(), path2);

    assertNotNull(path2.toFile());
    assertArrayEquals(Files.readAllBytes(path1), Files.readAllBytes(path2));

    // Check content of the zip has the aipId as root.
    final ZipTool zip = new ZipTool(path1.toUri());
    final Path tempPath = Paths.get(System.getProperty("java.io.tmpdir"));
    List<String> pathList = zip.unzipFilesAndReturnList(tempPath, false)
            .stream()
            .map(Path::toString)
            .toList();

    assertTrue(pathList.contains(tempPath.resolve("data/" + aipId + "/researchdata/tmp/dlcm/test/budget-2013.pdf").normalize().toString()));

    FileTool.deleteFile(path1);
    FileTool.deleteFile(path2);

  }

  @Test
  void cannotDownloadSubsetWithOnlyOneFileThatDoesNotExistTest() throws IOException {
    // Try to download a file that does not exist
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", DEFAULT_TITLE_TO_DOWNLOAD));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    // Ensure we have the correct archive to download, as the search return a list of archives
    // And we have two archives to download initiated by the initDataTest
    ArchivalInfoPackage archivalInfoPackage = aipList.stream()
            .filter(e -> e.getInfo().getName()
                    .equals(DLCMTestConstants.getNameWithPermanentLabel(DEFAULT_TITLE_TO_DOWNLOAD)))
            .findFirst().orElse(null);

    if (archivalInfoPackage == null) {
      fail("Missing archive to download necessary for testing");
    }

    final String aipId = archivalInfoPackage.getResId();
    final OrderSubsetItem orderSubsetItem1 = new OrderSubsetItem();
    orderSubsetItem1.setItemPath("file_that_does_not_exist" + ThreadLocalRandom.current().nextInt(Integer.MAX_VALUE) + ".pdf");

    String orderId = this.metadataClientService.prepareDownload(aipId, List.of(orderSubsetItem1), DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);
    // Check status of the order is in_error
    final Order order = this.orderClientService.findOne(orderId);
    assertEquals(Order.OrderStatus.IN_ERROR, order.getStatus());
  }

  @Test
  void downloadSubsetWithAFileThatDoesNotExistTest() throws IOException {
    // Try to download a file that does not exist
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", DEFAULT_TITLE_TO_DOWNLOAD));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    // Ensure we have the correct archive to download, as the search return a list of archives
    // And we have two archives to download initiated by the initDataTest
    ArchivalInfoPackage archivalInfoPackage = aipList.stream()
            .filter(e -> e.getInfo().getName()
                    .equals(DLCMTestConstants.getNameWithPermanentLabel(DEFAULT_TITLE_TO_DOWNLOAD)))
            .findFirst().orElse(null);

    if (archivalInfoPackage == null) {
      fail("Missing archive to download necessary for testing");
    }

    String aipId = archivalInfoPackage.getResId();
    final OrderSubsetItem orderSubsetItem1 = new OrderSubsetItem();
    orderSubsetItem1.setItemPath("file_that_does_not_exist.pdf");
    OrderSubsetItem orderSubsetItem2 = new OrderSubsetItem();
    orderSubsetItem2.setItemPath("Yareta/Yareta 2.0/Yareta-logo.png");
    final String orderId = this.metadataClientService.prepareDownload(aipId, List.of(orderSubsetItem1, orderSubsetItem2),
            DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    final Path path1 = Paths.get(System.getProperty("java.io.tmpdir"), "download1.tmp");
    this.metadataClientService.downloadArchive(aipId, path1, orderId);
    assertNotNull(path1.toFile());

    final ZipTool zip = new ZipTool(path1.toUri());
    final Path tempPath = Paths.get(System.getProperty("java.io.tmpdir"));
    List<String> pathList = zip.unzipFilesAndReturnList(tempPath, false)
            .stream()
            .map(Path::toString)
            .toList();

    assertTrue(pathList.contains(tempPath.resolve("researchdata/Yareta/Yareta 2.0/Yareta-logo.png").normalize().toString()));
    assertFalse(pathList.contains(tempPath.resolve("researchdata/file_that_does_not_exist.pdf").normalize().toString()));

    FileTool.deleteFile(path1);
  }

  @Test
  void downloadSubsetArchiveTest() throws IOException {
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", DEFAULT_TITLE_TO_DOWNLOAD));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    // Ensure we have the correct archive to download, as the search return a list of archives
    // And we have two archives to download initiated by the initDataTest
    ArchivalInfoPackage archivalInfoPackage = aipList.stream()
            .filter(e -> e.getInfo().getName()
                    .equals(DLCMTestConstants.getNameWithPermanentLabel(DEFAULT_TITLE_TO_DOWNLOAD)))
            .findFirst().orElse(null);

    if (archivalInfoPackage == null) {
      fail("Missing archive to download necessary for testing");
    }

    String aipId = archivalInfoPackage.getResId();
    final OrderSubsetItem orderSubsetItem1 = new OrderSubsetItem();
    orderSubsetItem1.setItemPath("Yareta/I-Love-RD-Sticker.pdf");
    OrderSubsetItem orderSubsetItem2 = new OrderSubsetItem();
    orderSubsetItem2.setItemPath("Yareta/Yareta 2.0/Yareta-logo.png");
    final String orderId = this.metadataClientService.prepareDownload(aipId, List.of(orderSubsetItem1, orderSubsetItem2),
            DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    final Path path1 = Paths.get(System.getProperty("java.io.tmpdir"), "download1.tmp");
    this.metadataClientService.downloadArchive(aipId, path1, orderId);
    assertNotNull(path1.toFile());

    // compare the archive with the one return by dip
    final Path path2 = Paths.get(System.getProperty("java.io.tmpdir"), "download2.tmp");

    final List<DisseminationInfoPackage> dipList = this.orderITService.getDIP(orderId);
    assertEquals(1, dipList.size());
    final DisseminationInfoPackage dip = dipList.get(0);
    this.dipClientService.downloadArchive(dip.getResId(), path2);

    assertNotNull(path2.toFile());
    assertEquals(Files.size(path1), Files.size(path2));
    final ZipTool zip = new ZipTool(path2.toUri());
    final Path tempPath = Paths.get(System.getProperty("java.io.tmpdir"));
    final List<String> pathList = zip.unzipFilesAndReturnList(tempPath, false)
            .stream()
            .map(Path::toString)
            .toList();

    assertTrue(pathList.contains(tempPath.resolve("researchdata/Yareta/Yareta 2.0/Yareta-logo.png").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("researchdata/Yareta/I-Love-RD-Sticker.pdf").normalize().toString()));

    FileTool.deleteFile(path1);
    FileTool.deleteFile(path2);
  }

  @Test
  void downloadSubsetFolderArchiveTest() throws IOException {
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", DEFAULT_TITLE_TO_DOWNLOAD));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    // Ensure we have the correct archive to download, as the search return a list of archives
    // And we have two archives to download initiated by the initDataTest
    ArchivalInfoPackage archivalInfoPackage = aipList.stream()
            .filter(e -> e.getInfo().getName()
                    .equals(DLCMTestConstants.getNameWithPermanentLabel(DEFAULT_TITLE_TO_DOWNLOAD)))
            .findFirst().orElse(null);

    if (archivalInfoPackage == null) {
      fail("Missing archive to download necessary for testing");
    }

    String aipId = archivalInfoPackage.getResId();
    final OrderSubsetItem orderSubsetItem1 = new OrderSubsetItem();
    orderSubsetItem1.setItemPath("Yareta/");
    final String orderId = this.metadataClientService.prepareDownload(aipId, List.of(orderSubsetItem1),
            DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    final Path path1 = Paths.get(System.getProperty("java.io.tmpdir"), "download1.tmp");
    this.metadataClientService.downloadArchive(aipId, path1, orderId);
    assertNotNull(path1.toFile());

    // compare the archive with the one return by dip
    final Path path2 = Paths.get(System.getProperty("java.io.tmpdir"), "download2.tmp");

    final List<DisseminationInfoPackage> dipList = this.orderITService.getDIP(orderId);
    assertEquals(1, dipList.size());
    final DisseminationInfoPackage dip = dipList.get(0);
    this.dipClientService.downloadArchive(dip.getResId(), path2);

    assertNotNull(path2.toFile());
    assertEquals(Files.size(path1), Files.size(path2));
    final ZipTool zip = new ZipTool(path2.toUri());
    final Path tempPath = Paths.get(System.getProperty("java.io.tmpdir"));
    final List<String> pathList = zip.unzipFilesAndReturnList(tempPath, false)
            .stream()
            .map(Path::toString)
            .toList();

    assertTrue(pathList.contains(tempPath.resolve("researchdata/Yareta/Yareta 2.0/Yareta-logo.png").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("researchdata/Yareta/I-Love-RD-Sticker.pdf").normalize().toString()));

    FileTool.deleteFile(path1);
    FileTool.deleteFile(path2);
  }

  @Test
  void downloadSubsetForRootFolderArchiveTest() throws IOException {
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", DEFAULT_TITLE_TO_DOWNLOAD));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    // Ensure we have the correct archive to download, as the search return a list of archives
    // And we have two archives to download initiated by the initDataTest
    ArchivalInfoPackage archivalInfoPackage = aipList.stream()
            .filter(e -> e.getInfo().getName()
                    .equals(DLCMTestConstants.getNameWithPermanentLabel(DEFAULT_TITLE_TO_DOWNLOAD)))
            .findFirst().orElse(null);

    if (archivalInfoPackage == null) {
      fail("Missing archive to download necessary for testing");
    }

    String aipId = archivalInfoPackage.getResId();
    final OrderSubsetItem orderSubsetItem1 = new OrderSubsetItem();
    orderSubsetItem1.setItemPath("/");
    final String orderId = this.metadataClientService.prepareDownload(aipId, List.of(orderSubsetItem1),
            DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    final Path path1 = Paths.get(System.getProperty("java.io.tmpdir"), "download1.tmp");
    this.metadataClientService.downloadArchive(aipId, path1, orderId);
    assertNotNull(path1.toFile());

    // compare the archive with the one return by dip
    final Path path2 = Paths.get(System.getProperty("java.io.tmpdir"), "download2.tmp");

    final List<DisseminationInfoPackage> dipList = this.orderITService.getDIP(orderId);
    assertEquals(1, dipList.size());
    final DisseminationInfoPackage dip = dipList.get(0);
    this.dipClientService.downloadArchive(dip.getResId(), path2);

    assertNotNull(path2.toFile());
    assertEquals(Files.size(path1), Files.size(path2));
    final ZipTool zip = new ZipTool(path2.toUri());
    final Path tempPath = Paths.get(System.getProperty("java.io.tmpdir"));
    final List<String> pathList = zip.unzipFilesAndReturnList(tempPath, false)
            .stream()
            .map(Path::toString)
            .toList();

    assertTrue(pathList.contains(tempPath.resolve("researchdata/Yareta/Yareta 2.0/Yareta-logo.png").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("researchdata/Yareta/I-Love-RD-Sticker.pdf").normalize().toString()));

    FileTool.deleteFile(path1);
    FileTool.deleteFile(path2);
  }

  @Test
  void downloadArchiveFileNameTest() throws IOException {
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", DEFAULT_TITLE_TO_DOWNLOAD));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    // Ensure we have the correct archive to download, as the search return a list of archives
    // And we have two archives to download initiated by the initDataTest
    ArchivalInfoPackage archivalInfoPackage = aipList.stream()
            .filter(e -> e.getInfo().getName()
                    .equals(DLCMTestConstants.getNameWithPermanentLabel(DEFAULT_TITLE_TO_DOWNLOAD)))
            .findFirst().orElse(null);

    if (archivalInfoPackage == null) {
      fail("Missing archive to download necessary for testing");
    }

    String aipId = archivalInfoPackage.getResId();
    // check if order has been already created
    String orderId = this.metadataClientService.prepareDownload(aipId, DLCMConstants.DISSEMINATION_POLICY_OAIS_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    Path path1 = Paths.get(System.getProperty("java.io.tmpdir"));
    this.metadataClientService.downloadArchiveWithoutSpecifyingFileName(aipId, path1, orderId);
    Path filePath = path1.resolve(archivalInfoPackage.getResId() + "-oais.zip");
    assertNotNull(path1.toFile());
    assertTrue(FileTool.checkFile(filePath));

    FileTool.deleteFile(filePath);

    // Same test for public dissemination policy
    aipId = archivalInfoPackage.getResId();
    // check if order has been already created
    orderId = this.metadataClientService.prepareDownload(aipId, DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    Path path2 = Paths.get(System.getProperty("java.io.tmpdir"));
    this.metadataClientService.downloadArchiveWithoutSpecifyingFileName(aipId, path2, orderId);
    filePath = path2.resolve(archivalInfoPackage.getInfo().getName() + ".zip");
    assertNotNull(path2.toFile());
    assertTrue(FileTool.checkFile(filePath));

    FileTool.deleteFile(filePath);

  }

  @Test
  void downloadArchiveWithIIIFDisseminationPolicyTest() throws IOException {
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", DEFAULT_TITLE_TO_DOWNLOAD));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    // Ensure we have the correct archive to download, as the search return a list of archives
    // And we have two archives to download initiated by the initDataTest
    ArchivalInfoPackage archivalInfoPackage = aipList.stream()
            .filter(e -> e.getInfo().getName()
                    .equals(DLCMTestConstants.getNameWithPermanentLabel(DEFAULT_TITLE_TO_DOWNLOAD)))
            .findFirst().orElse(null);

    if (archivalInfoPackage == null) {
      fail("Missing archive to download necessary for testing");
    }

    String aipId = archivalInfoPackage.getResId();
    String orgunitId = archivalInfoPackage.getOrganizationalUnitId();
    // find the dissemination policy join relation with organizational unit
    OrganizationalUnitDisseminationPolicyDTO orgUnitDisseminationPolicyDTO = this.orgUnitDisseminationPolicyClientService.find(
            orgunitId, DLCMConstants.DISSEMINATION_POLICY_IIIF_ID);
    assertNotNull(orgUnitDisseminationPolicyDTO);
    // check if order has been already created
    String orderId = this.metadataClientService.prepareDownload(aipId, DLCMConstants.DISSEMINATION_POLICY_IIIF_ID,
            orgUnitDisseminationPolicyDTO.getJoinResource().getCompositeKey());
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    final Path path1 = Paths.get(System.getProperty("java.io.tmpdir"), "download1.tmp");
    this.metadataClientService.downloadArchive(aipId, path1, orderId);
    assertNotNull(path1.toFile());

    final ZipTool zip = new ZipTool(path1.toUri());
    final Path tempPath = Paths.get(System.getProperty("java.io.tmpdir"));
    List<String> pathList = zip.unzipFilesAndReturnList(tempPath, false)
            .stream()
            .map(Path::toString)
            .toList();

    assertEquals(13, pathList.size());
    // Use a relative path (no leading slash)
    assertTrue(pathList.contains(tempPath.resolve("manifests/30066114.json").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("manifests/30066155.json").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("media/pliegos2.jpeg").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("media/pliegos1.jpeg").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("media/pliegos3.jpeg").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("media/Yareta.png").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("media/Yareta-v.png").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("media/Yareta-logo.png").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("media/Yareta-logo-white.png").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("media/Yareta-v-white.png").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("media/Yareta-white.png").normalize().toString()));
    // check that there is a file datacite
    assertTrue(pathList.stream().anyMatch(path -> path.toString().endsWith("-datacite.xml")));

    FileTool.deleteFile(path1);
  }

  @Test
  void downloadArchiveWithHederaDisseminationPolicyTest() throws IOException {
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", DEFAULT_TITLE_TO_DOWNLOAD_HEDERA));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    ArchivalInfoPackage archivalInfoPackage = aipList.stream()
            .filter(e -> e.getInfo().getName()
                    .equals(DLCMTestConstants.getNameWithPermanentLabel(DEFAULT_TITLE_TO_DOWNLOAD_HEDERA)))
            .findFirst().orElse(null);

    if (archivalInfoPackage == null) {
      fail("Missing archive to download hedera necessary for testing");
    }

    String aipId = archivalInfoPackage.getResId();
    String orgunitId = archivalInfoPackage.getOrganizationalUnitId();
    // find the dissemination policy join relation with organizational unit
    OrganizationalUnitDisseminationPolicyDTO orgUnitDisseminationPolicyDTO = this.orgUnitDisseminationPolicyClientService.find(
            orgunitId, DLCMConstants.DISSEMINATION_POLICY_HEDERA_ID);
    assertNotNull(orgUnitDisseminationPolicyDTO);
    // check if order has been already created
    String orderId = this.metadataClientService.prepareDownload(aipId, DLCMConstants.DISSEMINATION_POLICY_HEDERA_ID,
            orgUnitDisseminationPolicyDTO.getJoinResource().getCompositeKey());
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);

    final Path path1 = Paths.get(System.getProperty("java.io.tmpdir"), "download1.tmp");
    this.metadataClientService.downloadArchive(aipId, path1, orderId);
    assertNotNull(path1.toFile());

    final ZipTool zip = new ZipTool(path1.toUri());
    final Path tempPath = Paths.get(System.getProperty("java.io.tmpdir"));
    List<String> pathList = zip.unzipFilesAndReturnList(tempPath, false)
            .stream()
            .map(Path::toString)
            .toList();

    assertEquals(57, pathList.size());
    // Use a relative path (no leading slash)
    assertTrue(pathList.contains(tempPath.resolve("source-datasets/Varios/XML/BCN_001.xml").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("source-datasets/Varios/XML/BCN_002.xml").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("source-datasets/Moreno-v1/XML/Moreno_001.xml").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("source-datasets/Moreno-v1/XML/Moreno_002.xml").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("rml/XML/pliegos-1.0.ttl").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("research-data-files/IIIF/Camona/Carmona_001_1.jpg").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("research-data-files/IIIF/Varios/Varios_001_2.jpg").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("research-data-files/TEI/BCN/BCN_001.xml").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("research-data-files/TEI/BCN/BCN_002.xml").normalize().toString()));
    assertTrue(pathList.contains(tempPath.resolve("research-data-files/TEI/Moreno/Moreno_001.xml").normalize().toString()));

    // check that there is a file datacite
    assertTrue(pathList.stream().anyMatch(path -> path.toString().endsWith("-datacite.xml")));

    FileTool.deleteFile(path1);
  }

  @Test
  void bibliographiesTest() {
    final List<ArchivalInfoPackage> aipList = this.aipClientService
            .searchByProperties(Map.of("info.name", DLCMTestConstants.ARCHIVAL_UNIT_LABEL + " 1"));
    if (!aipList.isEmpty()) {
      Map<String, List<String>> bibliographiesConfig = this.metadataClientService.getBibliographiesConfig();

      final String aipId = aipList.get(0).getResId();
      List<CitationDto> citations = this.metadataClientService.getBibliographies(aipId);
      assertNotNull(citations);

      // checks that all combinations of configured properties have a match
      for (String style : bibliographiesConfig.get("styles")) {
        for (String language : bibliographiesConfig.get("languages")) {
          for (String outputFormat : bibliographiesConfig.get("outputFormats")) {
            CitationDto citationDto = this.getCitation(citations, style, language, outputFormat);
            assertNotNull(citationDto);
            assertFalse(StringTool.isNullOrEmpty(citationDto.getText()));
          }
        }
      }
    }
  }

  @Test
  void citationsTest() {
    final List<ArchivalInfoPackage> aipList = this.aipClientService
            .searchByProperties(Map.of("info.name", DLCMTestConstants.ARCHIVAL_UNIT_LABEL + " 1"));
    if (!aipList.isEmpty()) {
      Map<String, List<String>> citationsConfig = this.metadataClientService.getCitationsConfig();

      final String aipId = aipList.get(0).getResId();
      List<CitationDto> citations = this.metadataClientService.getCitations(aipId);
      assertNotNull(citations);

      // checks that all combinations of configured properties have a match
      for (String style : citationsConfig.get("styles")) {
        for (String language : citationsConfig.get("languages")) {
          for (String outputFormat : citationsConfig.get("outputFormats")) {
            CitationDto citationDto = this.getCitation(citations, style, language, outputFormat);
            assertNotNull(citationDto);
            assertFalse(StringTool.isNullOrEmpty(citationDto.getText()));
          }
        }
      }
    }
  }

  private CitationDto getCitation(List<CitationDto> citationDtos, String style, String language, String outputFormat) {
    Optional<CitationDto> citationDtoOpt = citationDtos.stream()
            .filter(c -> c.getStyle().equalsIgnoreCase(style)
                    && c.getLanguage().equalsIgnoreCase(language.toLowerCase())
                    && c.getOutputFormat().equalsIgnoreCase(outputFormat))
            .findFirst();
    return citationDtoOpt.orElse(null);
  }

  @Test
  void listPublicAndPrivateMetadataTest() {
    final List<ArchiveMetadata> metadataArchivesList = this.metadataClientService.listPublicAndPrivateMetadata().getData();
    final List<ArchiveMetadata> dataMgmtArchivesList = this.dataMgmtClientService.listAllPublicAndPrivateAIP().getData();
    this.verifyMetadataList(dataMgmtArchivesList, metadataArchivesList);
  }

  @Test
  void listPublicAndPrivateMetadataWithoutSecurityTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, this.metadataClientService::listPublicAndPrivateMetadataWithoutSecurity);
  }

  @Test
  void listAllPrivateTest() {
    final List<ArchiveMetadata> metadataArchivesList = this.metadataClientService.listAllPrivate().getData();
    final List<ArchiveMetadata> dataMgmtArchivesList = this.dataMgmtClientService.listAllPrivateAIP().getData();
    this.verifyMetadataList(dataMgmtArchivesList, metadataArchivesList);
  }

  @Test
  void listAllPrivateWithoutSecurityTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, this.metadataClientService::listAllPrivateWithoutSecurity);
  }

  @Test
  void listAllPublicTest() {
    final List<ArchiveMetadata> metadataArchivesList = this.metadataClientService.listAllPublic().getData();
    final List<ArchiveMetadata> dataMgmtArchivesList = this.dataMgmtClientService.listAllPublicAIP().getData();
    this.verifyMetadataList(dataMgmtArchivesList, metadataArchivesList);
  }

  @Test
  void listAllPublicWithoutSecurityTest() {
    final List<ArchiveMetadata> metadataArchivesList = this.metadataClientService.listAllPublicWithoutSecurity().getData();
    final List<ArchiveMetadata> dataMgmtArchivesList = this.dataMgmtClientService.listAllPublicAIP().getData();
    this.verifyMetadataList(dataMgmtArchivesList, metadataArchivesList);
  }

  @Test
  void listDataAndFoldersFromArchiveClosedButContentStructureTrueTest() {
    final Deposit deposit = this.depositITService.createLocalDeposit(DLCMTestConstants.PersistenceMode.TEMPORARY, "with content public true",
            DEFAULT_DESCRIPTION, Role.CREATOR_ID, false, DLCMMetadataVersion.getDefaultVersion());
    final Person person = this.personITService.getOrCreateRemotePerson();
    deposit.setDataSensitivity(DataTag.BLUE);
    deposit.setDataUsePolicy(DataUsePolicy.NONE);
    deposit.setAccess(Access.CLOSED);
    deposit.setContentStructurePublic(true);
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.addContributorToDeposit(remoteDeposit.getResId(), person.getResId());
    this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createDataFile(deposit));

    this.depositITService.waitDepositWithDataIsReady(deposit.getResId());
    this.depositITService.approveDeposit(deposit);
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", deposit.getTitle()));

    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      ArchiveMetadata archiveMetadata = this.metadataClientService.findOne(aipId);
      assertTrue(archiveMetadata.isStructureContentPublic());
      assertEquals(Access.CLOSED, archiveMetadata.getFinalAccessLevel());

      this.metadataClientService.getData(aipId);
      this.metadataClientService.listFolders(aipId);
    }
  }

  @Test
  void cannotListDataAndFoldersFromArchiveClosedButStructureContentFalseTest() {
    final List<ArchivalInfoPackage> aipList = this.aipClientService
            .searchByProperties(Map.of("info.name", DLCMTestConstants.ARCHIVAL_UNIT_LABEL + " 1"));

    if (!aipList.isEmpty()) {
      final String aipId = aipList.get(0).getResId();
      ArchiveMetadata archiveMetadata = this.metadataClientService.findOne(aipId);
      assertFalse(archiveMetadata.isStructureContentPublic());

      assertThrows(HttpClientErrorException.Forbidden.class, () -> this.metadataClientService.getData(aipId));
      assertThrows(HttpClientErrorException.Forbidden.class, () -> this.metadataClientService.listFolders(aipId));
    }
  }

  @Test
  void searchPublicIndexContainsFacet() {
    RestCollection<ArchiveMetadata> results = this.metadataClientService.searchPublicIndex("*");
    assertFalse(results.getFacets().isEmpty());
  }

  @Test
  void searchPrivateIndexContainsFacet() {
    RestCollection<ArchiveMetadata> results = this.metadataClientService.searchPrivateIndex("*");
    assertFalse(results.getFacets().isEmpty());
  }

  @Test
  void searchAllIndexContainsFacet() {
    RestCollection<ArchiveMetadata> results = this.metadataClientService.searchAllIndex("*");
    assertTrue(results.getFacets().isEmpty());
  }

  @Test
  void searchWithSpecialCharacters() {
    try {
      this.metadataClientService.searchPublicIndex("?=)");
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      assertNotNull(e.getMessage());
    }
  }

  @Test
  void searchWithoutSpecialCharacters() {
    RestCollection<ArchiveMetadata> results = this.metadataClientService.searchPublicIndex("abc");
    assertNotNull(results.getPage());
  }

  private void verifyMetadataList(List<ArchiveMetadata> dataMgmtArchivesList, List<ArchiveMetadata> metadataArchivesList) {
    final int nbDataMgmtArchives = dataMgmtArchivesList.size();
    final int nbMetadataArchives = metadataArchivesList.size();
    assertEquals(nbDataMgmtArchives, nbMetadataArchives);
    if (nbDataMgmtArchives <= RestCollectionPage.MAX_SIZE_PAGE) {
      assertThat(dataMgmtArchivesList, containsInAnyOrder(metadataArchivesList.toArray()));
    } else {
      logger.warn("ArchiveMetadata list contains " + nbDataMgmtArchives + " elements which is bigger then MAX_SIZE_PAGE (" +
              RestCollectionPage.MAX_SIZE_PAGE + "), only the size is checked");
    }
  }

}

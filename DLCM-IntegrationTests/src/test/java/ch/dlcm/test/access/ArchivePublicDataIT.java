/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - ArchivePublicDataIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.access;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.index.ArchiveMetadata;
import ch.dlcm.model.index.ArchivePublicData.PublicDataFileType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.security.Role;
import ch.dlcm.model.settings.Person;
import ch.dlcm.service.access.AIPDownloadClientService;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.access.OrderClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.DLCMTestConstants.PersistenceMode;
import ch.dlcm.test.service.AipITService;
import ch.dlcm.test.service.DepositITService;
import ch.dlcm.test.service.OrderITService;
import ch.dlcm.test.service.PersonITService;

class ArchivePublicDataIT extends AbstractArchiveAccess {

  private final PersonITService personITService;
  private final DepositITService depositITService;
  private final AipITService aipITService;

  @Autowired
  public ArchivePublicDataIT(
          Environment env,
          SudoRestClientTool restClientTool,
          AIPClientService aipClientService,
          OrderClientService orderClientService,
          AIPDownloadClientService aipDownloadClientService,
          MetadataClientService metadataClientService,
          OrderITService orderITService,
          PersonITService personITService,
          DepositITService depositITService,
          AipITService aipITService) {
    super(env, restClientTool, aipClientService, orderClientService, aipDownloadClientService, metadataClientService, orderITService);
    this.personITService = personITService;
    this.depositITService = depositITService;
    this.aipITService = aipITService;
  }

  @ParameterizedTest
  @MethodSource("validCombinations")
  void downloadThumbnailTest(DLCMMetadataVersion version) {
    final Deposit deposit = this.createRemoteDepositWithThumbnail(version);
    final ArchivalInfoPackage aip = this.getArchive(deposit);
    // check content-type
    String mimeType = this.getDataFileMimeType(aip.getResId(), PublicDataFileType.ARCHIVE_THUMBNAIL);
    String mimeTypeExpected = URLConnection.guessContentTypeFromName(REFERENCE_FILE);
    assertEquals(mimeType, mimeTypeExpected, "Wrong stored content type ");

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "dua.tmp");
    this.metadataClientService.downloadThumbnail(aip.getResId(), path);
    assertNotNull(path.toFile());
  }

  @Test
  void downloadDuaTest() {
    final Deposit deposit = this.createRemoteDepositWithDua();
    final ArchivalInfoPackage aip = this.getArchive(deposit);
    // check content-type
    String mimeType = this.getDataFileMimeType(aip.getResId(), PublicDataFileType.ARCHIVE_DUA);
    String mimeTypeExpected = URLConnection.guessContentTypeFromName(REFERENCE_FILE);
    assertEquals(mimeType, mimeTypeExpected, "Wrong stored content type ");

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "dua.tmp");
    this.metadataClientService.downloadDua(aip.getResId(), path);
    assertNotNull(path.toFile());
  }

  @Test
  void downloadReadmeTest() {
    final Deposit deposit = this.createRemoteDepositWithReadme();
    final ArchivalInfoPackage aip = this.getArchive(deposit);
    // check content-type
    String mimeType = this.getDataFileMimeType(aip.getResId(), PublicDataFileType.ARCHIVE_README);
    String mimeTypeExpected = URLConnection.guessContentTypeFromName(REFERENCE_FILE);
    assertEquals(mimeType, mimeTypeExpected, "Wrong stored content type");

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "dua.tmp");
    this.metadataClientService.downloadReadme(aip.getResId(), path);
    assertNotNull(path.toFile());
  }

  private String getDataFileMimeType(String aipId, PublicDataFileType publicDataFileType) {
    ArchiveMetadata archiveMetadata = this.metadataClientService.findOne(aipId);
    return archiveMetadata.getPublicDataContentType(publicDataFileType);
  }

  private ArchivalInfoPackage getArchive(Deposit deposit) {
    assertTrue(this.depositITService.waitDepositWithDataIsReady(deposit.getResId()), "Deposit data files in error");
    this.depositITService.approveDeposit(deposit);
    assertTrue(this.depositITService.waitDepositIsCompleted(deposit.getResId()), "Deposit not completed: " + deposit.getResId());
    this.aipITService.waitAIPIsReady(deposit.getOrganizationalUnitId(), deposit.getTitle());
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(Map.of("info.name", deposit.getTitle()));
    return aipList.get(0);
  }

  private Deposit createRemoteDepositWithThumbnail(DLCMMetadataVersion version) {
    final Deposit deposit = this.createLocalDepositForCurrentRole("Deposit with thumbnail");
    final Person person = this.personITService.getOrCreateRemotePerson();
    deposit.setMetadataVersion(version);
    deposit.setDataSensitivity(DataTag.BLUE);
    deposit.setDataUsePolicy(DataUsePolicy.LICENSE);
    deposit.setAccess(Access.PUBLIC);
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.addContributorToDeposit(remoteDeposit.getResId(), person.getResId());
    this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createDataFile(deposit));
    if (version.getVersionNumber() < DLCMMetadataVersion.V3_1.getVersionNumber()) {
      this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createDatasetThumbnailFile(deposit));
    } else {
      this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createArchiveThumbnailFile(deposit));
    }
    return remoteDeposit;
  }

  private Deposit createRemoteDepositWithDua() {
    final Deposit deposit = this.createLocalDepositForCurrentRole("Deposit with DUA");
    final Person person = this.personITService.getOrCreateRemotePerson();
    deposit.setDataSensitivity(DataTag.ORANGE);
    deposit.setDataUsePolicy(DataUsePolicy.SIGNED_DUA);
    deposit.setAccess(Access.CLOSED);
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.addContributorToDeposit(remoteDeposit.getResId(), person.getResId());
    this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createDataFile(deposit));
    this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createDuaFile(deposit));
    return remoteDeposit;
  }

  private Deposit createRemoteDepositWithReadme() {
    final Deposit deposit = this.createLocalDepositForCurrentRole("Deposit with README");
    final Person person = this.personITService.getOrCreateRemotePerson();
    deposit.setDataSensitivity(DataTag.BLUE);
    deposit.setDataUsePolicy(DataUsePolicy.LICENSE);
    deposit.setAccess(Access.PUBLIC);
    final Deposit remoteDeposit = this.depositITService.createRemoteDepositWithData(deposit);
    this.depositITService.addContributorToDeposit(remoteDeposit.getResId(), person.getResId());
    this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createDataFile(deposit));
    this.depositITService.addDataFileToDeposit(remoteDeposit.getResId(), this.depositITService.createReadmeFile(deposit));
    return remoteDeposit;
  }

  private Deposit createLocalDepositForCurrentRole(String title) {
    return this.depositITService.createLocalDeposit(PersistenceMode.TEMPORARY, title, DEFAULT_DESCRIPTION, this.role(), false, DLCMMetadataVersion.getDefaultVersion());
  }

  @Override
  protected String role() {
    return Role.CREATOR_ID;
  }

  private static Stream<Arguments> validCombinations() {
    return Stream.of(arguments(DLCMMetadataVersion.V4_0));
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - AbstractArchiveAccess.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.dlcm.test.access;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.service.access.AIPDownloadClientService;
import ch.dlcm.service.access.MetadataClientService;
import ch.dlcm.service.access.OrderClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.OrderITService;

abstract class AbstractArchiveAccess extends AbstractAccessIT {

  protected final MetadataClientService metadataClientService;
  protected final OrderITService orderITService;

  protected AbstractArchiveAccess(
          Environment env,
          SudoRestClientTool restClientTool,
          AIPClientService aipClientService,
          OrderClientService orderClientService,
          AIPDownloadClientService aipDownloadClientService,
          MetadataClientService metadataClientService,
          OrderITService orderITService) {
    super(env, restClientTool, aipClientService, orderClientService, aipDownloadClientService);
    this.metadataClientService = metadataClientService;
    this.orderITService = orderITService;
  }

  protected abstract String role();

  protected void downloadArchiveTest() {
    final List<ArchivalInfoPackage> aipList = this.aipClientService.searchByProperties(
            Map.of("info.name", DLCMTestConstants.CLOSED_DEPOSIT + " " + this.role()));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    final String aipId = aipList.get(0).getResId();
    String orderId = this.metadataClientService.prepareDownload(aipId, DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    this.orderITService.waitForPublicOrderIsReadyOrInError(orderId, aipId);
    Path path1 = Paths.get(System.getProperty("java.io.tmpdir"), "download1.tmp");
    this.metadataClientService.downloadArchive(aipId, path1);
    assertNotNull(path1.toFile());

    try {
      FileTool.deleteFile(path1);
    } catch (IOException e) {
      throw new RuntimeException("Problem deleting file " + path1);
    }
  }

  protected void cannotDownloadArchiveTest() {
    final List<ArchivalInfoPackage> aipList = this.aipClientService
            .searchByProperties(Map.of("info.name", DLCMTestConstants.CLOSED_DEPOSIT + " " + this.role()));
    if (aipList.isEmpty()) {
      fail("Missing closed archive necessary for testing");
    }
    final String aipId = aipList.get(0).getResId();
    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download2.tmp");
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.metadataClientService.prepareDownload(aipId, DLCMConstants.DISSEMINATION_POLICY_BASIC_ID));
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.metadataClientService.downloadArchive(aipList.get(0).getResId(), path));

  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - OrderIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.access;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.model.access.Order;
import ch.dlcm.model.access.Order.OrderStatus;
import ch.dlcm.model.access.QueryType;
import ch.dlcm.service.access.AIPDownloadClientService;
import ch.dlcm.service.access.OrderClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.DLCMTestConstants;
import ch.dlcm.test.service.PersonITService;

class OrderIT extends AbstractAccessIT {

  private final PersonITService personService;

  @Autowired
  public OrderIT(Environment env, SudoRestClientTool restClientTool, AIPClientService aipClientService, OrderClientService orderClientService,
          AIPDownloadClientService aipDownloadClientService, PersonITService personITService) {
    super(env, restClientTool, aipClientService, orderClientService, aipDownloadClientService);
    this.personService = personITService;
  }

  @AfterEach
  public void cleanOrderFixture() {
    this.restClientTool.sudoAdmin();
    this.orderClientService.findAll().stream().filter(order -> order.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(order -> this.orderClientService.delete(order.getResId()));
    this.restClientTool.exitSudo();
  }

  @Test
  void findAllOnlyForAdminTest() {
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.orderClientService.findAll());

    this.restClientTool.sudoAdmin();
    List<Order> listOrderAdmin = this.orderClientService.findAll();
    assertNotNull(listOrderAdmin);
    this.restClientTool.exitSudo();

    this.restClientTool.sudoRoot();
    List<Order> listOrderRoot = this.orderClientService.findAll();
    assertNotNull(listOrderRoot);
    this.restClientTool.exitSudo();
  }

  @Test
  void findAllPublicTest() {
    List<Order> listOrderPublic = this.orderClientService.findAllPublic(new HashMap<>());
    assertNotNull(listOrderPublic);

    this.restClientTool.sudoAdmin();
    List<Order> listAllOrder = this.orderClientService.findAll();
    this.restClientTool.exitSudo();

    assertEquals(listAllOrder.stream().filter(Order::isPublicOrder).toArray().length, listOrderPublic.size());
  }

  @Test
  void findAllCreatedByUserTest() {
    List<Order> listOrderCreatedByUser = this.orderClientService.findAllCreatedByUser();
    assertNotNull(listOrderCreatedByUser);

    this.restClientTool.sudoAdmin();
    List<Order> listAllOrder = this.orderClientService.findAll();

    // Only possible to list orders with role ADMIN or ROOT
    String externalUid = this.personService.getUserWithRole(AuthApplicationRole.USER).getExternalUid();
    this.restClientTool.exitSudo();

    assertEquals(listAllOrder.stream().filter(order -> order.getCreatedBy().equals(externalUid)).toArray().length,
            listOrderCreatedByUser.size());
  }

  @Test
  void findOnePublicForEveryoneTest() {
    String orderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order");

    // Create an order
    final Order order1 = new Order();
    order1.setName(orderName);
    order1.setQueryType(QueryType.DIRECT);
    order1.setQuery(orderName);
    order1.setPublicOrder(Boolean.TRUE);
    order1.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    final Order order2 = this.orderClientService.create(order1);

    assertNotNull(this.orderClientService.findOne(order2.getResId()));

    this.restClientTool.sudoThirdPartyUser();
    assertNotNull(this.orderClientService.findOne(order2.getResId()));
    this.restClientTool.exitSudo();
  }

  @Test
  void findOnePrivateForCreatorOnlyTest() {
    String orderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order");

    // Create an order
    final Order order1 = new Order();
    order1.setName(orderName);
    order1.setQueryType(QueryType.DIRECT);
    order1.setQuery(orderName);
    order1.setPublicOrder(Boolean.FALSE);
    order1.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    final Order order2 = this.orderClientService.create(order1);

    assertNotNull(this.orderClientService.findOne(order2.getResId()));

    this.restClientTool.sudoThirdPartyUser();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.orderClientService.findOne(order2.getResId()));
    this.restClientTool.exitSudo();

    this.restClientTool.sudoAdmin();
    assertNotNull(this.orderClientService.findOne(order2.getResId()));
    this.restClientTool.exitSudo();

    this.restClientTool.sudoRoot();
    assertNotNull(this.orderClientService.findOne(order2.getResId()));
    this.restClientTool.exitSudo();
  }

  @Test
  void creationTest() {
    String orderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order");

    // Create an order
    final Order order1 = new Order();
    order1.setName(orderName);
    order1.setQueryType(QueryType.DIRECT);
    order1.setQuery(orderName);
    order1.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    final Order order2 = this.orderClientService.create(order1);

    // Test the creation return
    assertNotNull(order2, "Cannot create query order");
    assertEquals(order1.getName(), order2.getName(), "Problem with query order name");
    assertTrue(order2.isPublicOrder(), "Order should be public by default");

    // Test creation
    final Order order3 = this.orderClientService.findOne(order2.getResId());
    assertNotNull(order3, "Cannot find query order");
    assertEquals(order1.getName(), order3.getName(), "Problem with query order name");
    assertEquals(OrderStatus.IN_PROGRESS, order2.getStatus(), "Wrong query order status");

    // Create a private order
    final Order order4 = new Order();
    String privateOrderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Private Order");
    order4.setName(privateOrderName);
    order4.setQueryType(QueryType.DIRECT);
    order4.setQuery(privateOrderName);
    order4.setPublicOrder(false);
    order4.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    final Order order5 = this.orderClientService.create(order4);
    assertFalse(order5.isPublicOrder(), "Order should be private");

  }

  @Test
  void deleteTest() {
    String orderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order");

    // Create a new order
    Order order = new Order();
    order.setName(orderName);
    order.setQueryType(QueryType.DIRECT);
    order.setQuery(orderName);
    order.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    order = this.orderClientService.create(order);

    // Delete the order
    final String resId = order.getResId();
    this.orderClientService.delete(resId);
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.orderClientService.findOne(resId));
  }

  @Test
  void cantDeleteOrderOfThirdUserTest() {
    String orderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order");

    this.restClientTool.sudoThirdPartyUser();
    // Create a new order
    Order order = new Order();
    order.setName(orderName);
    order.setQueryType(QueryType.DIRECT);
    order.setQuery(orderName);
    order.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    order = this.orderClientService.create(order);
    this.restClientTool.exitSudo();

    // Try to delete the order
    final String resId = order.getResId();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.orderClientService.delete(resId));
  }

  @Test
  void submitOrderWithoutAipsTest() {
    String orderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order");

    // Create an order
    Order order = new Order();
    order.setName(orderName);
    order.setQueryType(QueryType.DIRECT);
    order.setQuery(orderName);
    order.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    order = this.orderClientService.create(order);

    try {
      this.orderClientService.setSubmitStatus(order.getResId());
      fail("A BAD REQUEST http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void updateStatusTest() {
    String orderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order");

    // Create an order
    Order order = new Order();
    order.setName(orderName);
    order.setQueryType(QueryType.DIRECT);
    order.setQuery(orderName);
    order.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    order = this.orderClientService.create(order);

    order.setStatus(OrderStatus.SUBMITTED);

    try {
      this.orderClientService.update(order.getResId(), order);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void cantUpdateOrderOfThirdUserTest() {
    String orderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order");

    this.restClientTool.sudoThirdPartyUser();
    // Create a new order
    Order order = new Order();
    order.setName(orderName);
    order.setQueryType(QueryType.DIRECT);
    order.setQuery(orderName);
    order.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    order = this.orderClientService.create(order);
    this.restClientTool.exitSudo();

    order.setStatus(OrderStatus.SUBMITTED);

    try {
      this.orderClientService.update(order.getResId(), order);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void updateTest() {
    // Create an order
    String orderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order");
    Order order = new Order();
    order.setName(orderName);
    order.setQueryType(QueryType.DIRECT);
    order.setQuery(orderName);
    order.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    order = this.orderClientService.create(order);

    // Update the order
    String orderName2 = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order 2");
    order.setName(orderName2);
    order.setQuery(orderName2);
    final Order actualOrder = this.orderClientService.update(order.getResId(), order);
    final Order order2 = this.orderClientService.findOne(order.getResId());

    // Test the update
    assertNotNull(actualOrder, "Cannot find query order");
    assertEquals(order.getName(), order2.getName(), "Problem with query order name");
    this.assertEqualsWithoutNanoSeconds("Problem with query order creation time", order.getCreationTime(), order2.getCreationTime());
  }

  @Test
  void updateIsPublicPropertyTest() {
    // Create an order
    String orderName = DLCMTestConstants.getRandomNameWithTemporaryLabel("Order");
    Order order = new Order();
    order.setName(orderName);
    order.setQueryType(QueryType.DIRECT);
    order.setQuery(orderName);
    order.setPublicOrder(false);
    order.setDisseminationPolicyId(DLCMConstants.DISSEMINATION_POLICY_BASIC_ID);
    order = this.orderClientService.create(order);

    DLCMTestConstants.getRandomNameWithTemporaryLabel("Order 2");
    order.setPublicOrder(true);
    final Order actualOrder = this.orderClientService.update(order.getResId(), order);
    final Order order2 = this.orderClientService.findOne(order.getResId());

    // Test the update
    assertNotNull(actualOrder, "Cannot find query order");
    assertEquals(order.isPublicOrder(), order2.isPublicOrder(), "Problem with is public property");
    this.assertEqualsWithoutNanoSeconds("Problem with query order creation time", order.getCreationTime(), order2.getCreationTime());
  }

}

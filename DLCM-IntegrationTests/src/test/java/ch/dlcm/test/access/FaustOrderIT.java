/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Integration Tests - FaustOrderIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.access;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.dlcm.model.access.Order;
import ch.dlcm.service.access.AIPDownloadClientService;
import ch.dlcm.service.access.OrderClientService;
import ch.dlcm.service.archivalstorage.AIPClientService;
import ch.dlcm.test.DLCMTestConstants;

@Disabled("Data sets must be initialized before executing tests")
class FaustOrderIT extends AbstractAccessIT {

  @Autowired
  public FaustOrderIT(Environment env, SudoRestClientTool restClientTool, AIPClientService aipClientService,
          OrderClientService orderClientService,
          AIPDownloadClientService aipDownloadClientService) {
    super(env, restClientTool, aipClientService, orderClientService, aipDownloadClientService);
  }

  @Test
  void directQueryAICTest() {
    // With 1 AICs
    this.executeDirectQuery(1, false, true, true);
    // With 2 AICs
    this.executeDirectQuery(2, false, true, false);
  }

  @Test
  void directQueryAIPTest() {
    // With 1 AIPs
    this.executeDirectQuery(1, true, true, true);
    // With 3 AIPs
    this.executeDirectQuery(3, true, true, false);
  }

  @Test
  void directQueryAIUTest() {
    // With 1 AIU
    this.executeDirectQuery(1, true, false, true);
    // With 3 AIUs
    this.executeDirectQuery(3, true, false, false);
  }

  @Test
  void simpleQueryTest() {
    final Order order = this.orderClientService.create(this.buildSimpleQuery("faust"));
    assertNotNull(order, "Cannot create query order");
    this.executeQuery(order, true);
  }

  @Test
  void simpleQueryWithoutAIPTest() {
    final Order order = this.orderClientService.create(this.buildSimpleQuery("faust"));
    assertNotNull(order, "Cannot create query order");
    this.executeQuery(order, false);
  }

  @AfterEach
  public void cleanOrderFixture() {
    this.orderClientService.findAll()
            .stream()
            .filter(order -> order.getName().startsWith(DLCMTestConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(order -> this.orderClientService.delete(order.getResId()));
  }
}

[cols="^,^,^,^3,^3",options="header",]
|===
|Path|Type|Mandatory|Description|Constraints 

|`when`
|`String`
|&#10004;
|The date of the executed event
|_Format_: `YYYY-MM-DDThh:mm:ss+nnn`

|`who`
|`String`
| 
|The user who executed the event
|

|===

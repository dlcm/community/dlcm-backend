|===
|Path|Type|Description

|`resId`
|`String`
|The _data file_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]]

|`creation`
|`Object`
|The creation info of the _data file_ resource (see <<change-info>>)

|`lastUpdate`
|`Object`
|The last updated info of the _data file_ resource (see <<change-info>>)

|`status`
|`Object`
|The status of the data file [CREATED, IN_ERROR, READY, RECEIVED, CHANGE_RELATIVE_LOCATION, CHANGE_DATA_CATEGORY, TO_PROCESS, PROCESSED, FILE_FORMAT_IDENTIFIED, FILE_FORMAT_UNKNOWN, VIRUS_CHECKED]
|`sourceData`
|`String`
|The URI of the source data file 

|`relativeLocation`
|`String`
|The relative folder hierarchy of the data file

|`fileName`
|`String`
|The field name of the data file

|`dataCategory`
|`String`
|The category size of the data file (see <<data-file-categories>>)

|`dataType`
|`String`
|The sub-category size of the data file (see <<data-file-categories>>)

|`complianceLevel`
|`String`
|The compliance level of the data file [NO_COMPLIANCE, WEAK_COMPLIANCE, AVERAGE_COMPLIANCE, FULL_COMPLIANCE]

|`size`
|`String`
|The size of the data file (in bytes)

|`smartSize`
|`String`
|The human-readable size of the data file

|`fileFormat`
|`Object`
|The file format detection information of the data file


|`virusCheck`
|`Object`
|The virus check information of the data file

|`checksums`
|`Array`
|The checksum list of the data file

|===

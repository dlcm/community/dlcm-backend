/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Solution - OpenApiConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm;

import org.springdoc.core.models.GroupedOpenApi;
import org.springdoc.core.properties.SwaggerUiOAuthProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.config.SolidifyOpenApiConfig;

import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.rest.ModuleName;

@Configuration
public class OpenApiConfig extends SolidifyOpenApiConfig {

  private final DLCMRepositoryDescription repository;
  private final AuthorizationClientProperties authProperties;

  OpenApiConfig(DLCMRepositoryDescription repository,
          AuthorizationClientProperties authProperties,
          SwaggerUiOAuthProperties swaggerUiOAuthProperties) {
    super(swaggerUiOAuthProperties);
    this.repository = repository;
    this.authProperties = authProperties;
  }

  @Override
  protected String getAuthorizationServerUrl() {
    return this.authProperties.getPublicAuthorizationServerUrl();
  }

  @Override
  protected String getApplicationName() {
    return this.repository.getLongName();
  }

  @Override
  protected String getApplicationDescription() {
    return this.repository.getDescription();
  }

  @Override
  protected String getApplicationVersion() {
    return this.repository.getVersion();
  }

  @Override
  protected String getApplicationScope() {
    return "app-dlcm";
  }

  @Override
  protected String getContactEmail() {
    return this.repository.getEmail();
  }

  @Override
  protected String getApplicationTagName() {
    return DLCMConstants.DLCM.toLowerCase();
  }

  @Bean
  @ConditionalOnProperty(prefix = "dlcm.module.access", name = "enable")
  public GroupedOpenApi accessOpenApi() {
    return this.moduleGroup(ModuleName.ACCESS, "/oai-info/**");
  }

  @Bean
  @ConditionalOnProperty(prefix = "dlcm.module.admin", name = "enable")
  public GroupedOpenApi adminOpenApi() {
    return this.moduleGroup(ModuleName.ADMIN);
  }

  @Bean
  @ConditionalOnProperty(prefix = "dlcm.module.archival-storage", name = "enable")
  public GroupedOpenApi archivalStorageOpenApi() {
    return this.moduleGroup(ModuleName.ARCHIVALSTORAGE);
  }

  @Bean
  @ConditionalOnProperty(prefix = "dlcm.module.data-mgmt", name = "enable")
  public GroupedOpenApi dataMgmtOpenApi() {
    return this.moduleGroup(ModuleName.DATAMGMT, "/index/**");
  }

  @Bean
  @ConditionalOnProperty(prefix = "dlcm.module.ingest", name = "enable")
  public GroupedOpenApi ingestOpenApi() {
    return this.moduleGroup(ModuleName.INGEST);
  }

  @Bean
  @ConditionalOnProperty(prefix = "dlcm.module.preingest", name = "enable")
  public GroupedOpenApi preingestOpenApi() {
    return this.moduleGroup(ModuleName.PREINGEST);
  }

  @Bean
  @ConditionalOnProperty(prefix = "dlcm.module.preservation-planning", name = "enable")
  public GroupedOpenApi preservationPlanningOpenApi() {
    return this.moduleGroup(ModuleName.PRES_PLANNING);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Solution - SolutionRestTestsAndDocumentationConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.doc;

import java.security.Principal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.util.MultiValueMap;

import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.model.index.IndexDefinition;
import ch.unige.solidify.model.index.IndexDefinitionList;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.util.TrustedRestClientTool;

import ch.dlcm.DLCMConstants;

@Configuration
public class SolutionRestTestsAndDocumentationConfig {
  private final static String FAKE_TOKEN_FOR_TESTS = "fake_token_for_tests";

  @Primary
  @Bean
  public HttpRequestInfoProvider authenticationStorage() {
    return new HttpRequestInfoProvider() {
      @Override
      public MultiValueMap<String, String> getAuthorizationHeader() {
        return null;
      }

      @Override
      public Principal getPrincipal() {
        return () -> "DLCM_externalUid";
      }

      @Override
      public String getIncomingToken() {
        return "";
      }
    };
  }

  @Primary
  @Bean
  public TrustedRestClientTool trustedRestClientToolForTests() {
    TrustedRestClientTool trustedRestClientTool = new TrustedRestClientTool();
    trustedRestClientTool.setTrustedToken(FAKE_TOKEN_FOR_TESTS);
    return trustedRestClientTool;
  }

  @Bean
  public AuthorizationClientProperties authorizationClientProperties() {
    AuthorizationClientProperties properties = new AuthorizationClientProperties();
    properties.setTrustedUserName("doc");
    properties.setTrustedUserPassword("doc");
    properties.setClientId("doc");
    properties.setClientSecret("doc");
    properties.setRedirectUri("doc");
    properties.setAuthorizationServerUrl("doc");
    properties.setApplicationName("doc");
    return properties;
  }

  @Primary
  @Bean
  public IndexDefinitionList indexDefinitionListForTests() {
    IndexDefinitionList list = new IndexDefinitionList();
    list.getIndexList().add(new IndexDefinition(AbstractResourceDoc.TEST_STRING_VALUE, null));
    list.getIndexList().add(new IndexDefinition(AbstractResourceDoc.TEST_STRING_VALUE + DLCMConstants.PRIVATE, null));
    return list;
  }
}

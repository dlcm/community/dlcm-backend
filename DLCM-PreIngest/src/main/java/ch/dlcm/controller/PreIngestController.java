/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - PreIngestController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.rest.ModuleName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.DepositStatusService;

@RestController
@ConditionalOnProperty(prefix = "dlcm.module.preingest", name = "enable")
@RequestMapping(UrlPath.PREINGEST)
public class PreIngestController extends ModuleController {

  private static final Logger log = LoggerFactory.getLogger(PreIngestController.class);

  private final DLCMProperties config;

  PreIngestController(DepositStatusService depositStatusService, DLCMProperties config) {
    super(ModuleName.PREINGEST);
    this.config = config;
    this.cleanTmpDirectory();
  }

  private void cleanTmpDirectory() {
    Path tmpPreIngestLocation = Path.of(this.config.getTempLocation(this.config.getPreingestLocation()));

    try {
      FileTool.deleteFolderContent(tmpPreIngestLocation);
    } catch (IOException e) {
      log.error("Unable to clean temporary directory", e);
    }
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositDataFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preingest;

import static ch.dlcm.DLCMConstants.INFO_PACKAGE_RES_ID_KEY;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringParserTool;

import ch.dlcm.business.DepositDataFileService;
import ch.dlcm.controller.AbstractDataFileController;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.HistoryService;

@RestController
@ConditionalOnBean(PreIngestController.class)
@RequestMapping(UrlPath.PREINGEST_DEPOSIT + SolidifyConstants.URL_PARENT_ID + ResourceName.DATAFILE)
public class DepositDataFileController extends AbstractDataFileController<Deposit, DepositDataFile> {

  public DepositDataFileController(HistoryService historyService) {
    super(historyService);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'UPLOAD_FILE')")
  public HttpEntity<DepositDataFile> create(@PathVariable final String parentid, final @Valid @RequestBody DepositDataFile childItem) {
    return super.create(parentid, childItem);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'GET_FILE')")
  public HttpEntity<DepositDataFile> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'LIST_FILES')")
  public HttpEntity<RestCollection<DepositDataFile>> list(@PathVariable String parentid, @ModelAttribute DepositDataFile filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'UPDATE_FILE')")
  public HttpEntity<DepositDataFile> update(@PathVariable final String parentid, @PathVariable final String id,
          @RequestBody final Map<String, Object> newChildItem) {
    return super.update(parentid, id, newChildItem);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'DELETE_FILE')")
  public ResponseEntity<Void> delete(@PathVariable final String parentid, @PathVariable final String id) {
    return super.delete(parentid, id);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'DELETE_FILE')")
  public ResponseEntity<Void> deleteList(@PathVariable final String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'GET_FILE') "
          + "|| @downloadTokenPermissionService.isAllowed(#id, T(ch.dlcm.model.security.DownloadTokenType).DEPOSIT_DATAFILE)")
  public HttpEntity<FileSystemResource> download(@PathVariable String parentid, @PathVariable String id) {
    return super.download(parentid, id);
  }

  @PostMapping("/" + DLCMActionName.DELETE_FOLDER)
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'DELETE_FOLDER')")
  public ResponseEntity<Void> deleteFolder(@PathVariable String parentid,
          @RequestParam(value = "relativeLocation") String relativeLocation) {
    if (relativeLocation == null || !relativeLocation.startsWith("/")) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST, "Wrong relative location");
    }
    List<String> listResId = ((DepositDataFileService) this.subResourceService).findDataFileIdByRelativeLocationFolder(parentid,
            relativeLocation);
    if (listResId.isEmpty()) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND, "No data files found");
    }
    super.deleteList(parentid, listResId.toArray(new String[0]));
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @SuppressWarnings("squid:S4684")
  @GetMapping("/" + DLCMActionName.LIST_CURRENT_STATUS)
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'LIST_FILES')")
  public ResponseEntity<Object> listCurrentStatus(@PathVariable String parentid, @ModelAttribute DepositDataFile filterItem) {
    Map<DataFileStatus, Long> mapStatistics = ((DepositDataFileService) this.subResourceService).getMapStatusOccurences(parentid);
    return new ResponseEntity<>(mapStatistics, HttpStatus.OK);
  }

  @GetMapping("/" + DLCMActionName.LIST_FOLDERS)
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'LIST_FILES')")
  public HttpEntity<List<String>> listFolders(@PathVariable String parentid) {
    final List<String> relativeLocations = ((DepositDataFileService) this.subResourceService).getRelativeLocationList(parentid);
    return new ResponseEntity<>(relativeLocations, HttpStatus.OK);
  }

  @GetMapping("/" + DLCMActionName.LIST_STATUS)
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'LIST_FILES')")
  public HttpEntity<DataFileStatus[]> listStatus(@PathVariable String parentid) {
    return new ResponseEntity<>(DataFileStatus.values(), HttpStatus.OK);
  }

  @PostMapping("/" + DLCMActionName.RESUME_ALL)
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'RESUME_FILE')")
  public ResponseEntity<Void> resumeList(@PathVariable final String parentid) {
    final Deposit deposit = this.resourceService.findOne(parentid);
    for (final DepositDataFile dataFile : deposit.getDataFiles()) {
      this.resume(parentid, dataFile.getResId());
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PostMapping("/" + ActionName.RESUME)
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'RESUME_FILE')")
  public ResponseEntity<Void> resumeList(@PathVariable final String parentid, @RequestBody String[] ids) {
    for (final String id : ids) {
      this.resume(parentid, id);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.RESUME)
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'RESUME_FILE')")
  public HttpEntity<Result> resume(@PathVariable String parentid, @PathVariable String id) {
    final Result res = this.resumeDataFile(parentid, id);
    if (res == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    try {

      final Method resumeMtd = DepositDataFileController.class.getMethod(ActionName.RESUME, String.class, String.class);
      res.add(linkTo(resumeMtd, parentid, id).withSelfRel());
      final Method getMtd = DepositDataFileController.class.getMethod(ActionName.READ, String.class, String.class);
      res.add(linkTo(getMtd, parentid, id).withRel(ActionName.PARENT));
    } catch (final NoSuchMethodException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @GetMapping("/" + ActionName.SEARCH)
  @SuppressWarnings("squid:S4684")
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'LIST_FILES')")
  public HttpEntity<RestCollection<DepositDataFile>> search(@PathVariable String parentid, @ModelAttribute DepositDataFile depositDataFile,
          @RequestParam("search") String search, @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    if (!this.resourceService.existsById(parentid)) {
      throw new NoSuchElementException("No Deposit with id " + parentid);
    }
    final List<SearchCriteria> criterias = StringParserTool.parseSearchString(search);
    final SearchCriteria parent = new SearchCriteria(INFO_PACKAGE_RES_ID_KEY, "~", parentid);
    final Page<DepositDataFile> itemlist = this.subResourceService.findBySubResourceSearchCriteria(depositDataFile, matchtype, criterias, parent,
            pageable);
    for (final DepositDataFile df : itemlist) {
      this.addLinks(parentid, df);
    }
    final RestCollection<DepositDataFile> collection = this.processPageAndLinks(itemlist, parentid, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.VALIDATE)
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'VALIDATE_FILE')")
  public HttpEntity<Result> validate(@PathVariable String parentid, @PathVariable String id) {
    final DepositDataFile datafile = this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, true);
    final Result res = new Result(id);
    if (datafile == null) {
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      return new ResponseEntity<>(res, HttpStatus.NOT_FOUND);
    } else if (datafile.getStatus() == DataFileStatus.IGNORED_FILE) {
      datafile.setFileStatus(DataFileStatus.READY);
      this.subResourceService.save(datafile);
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.datafile.valid"));
    } else {
      res.setStatus(Result.ActionStatus.NON_APPLICABLE);
      res.setMesssage(this.messageService.get("message.datafile.invalid", new Object[] { datafile.getStatus() }));
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PostMapping("/" + DLCMActionName.VALIDATE)
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'VALIDATE_FILE')")
  public ResponseEntity<Void> validateList(@PathVariable final String parentid, @RequestBody String[] ids) {
    for (final String id : ids) {
      this.validate(parentid, id);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(String parentid, W w) {
    w.add(linkTo(methodOn(this.getClass()).listStatus(parentid)).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listFolders(parentid)).withRel(ActionName.VALUES));
  }

  @Override
  protected Deposit getParentResourceProperty(DepositDataFile subResource) {
    return subResource.getInfoPackage();
  }

  @Override
  protected void setParentResourceProperty(DepositDataFile subResource, Deposit parentResource) {
    subResource.setInfoPackage(parentResource);
  }

}

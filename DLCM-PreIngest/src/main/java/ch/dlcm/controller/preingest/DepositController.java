/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preingest;

import static ch.dlcm.DLCMConstants.ANONYMIZED_DEPOSITS_FOLDER;
import static ch.dlcm.DLCMRestFields.STATUS_FIELD;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.APPROVED;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.CANCEL_EDITING_METADATA;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.COMPLETED;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.EDITING_METADATA;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.EDITING_METADATA_REJECTED;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.IN_PROGRESS;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.IN_VALIDATION;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.REJECTED;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.SUBMISSION_AGREEMENT_APPROVED;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.time.Instant;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ZipTool;
import ch.unige.solidify.validation.ValidationError;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.DataFileService;
import ch.dlcm.business.DepositDataFileService;
import ch.dlcm.business.DepositService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMProperties.FileList;
import ch.dlcm.controller.AbstractPackageController;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.message.NotificationMessage;
import ch.dlcm.model.Access;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.dto.AuthorizedOrganizationalUnitDto;
import ch.dlcm.model.dto.DepositDto;
import ch.dlcm.model.dto.FileUploadDto;
import ch.dlcm.model.notification.NotificationStatus;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.policies.SubmissionAgreementType;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.DataCategoryService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.IdentifierService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.propagate.PropagateMetadataTypeRemoteResourceService;
import ch.dlcm.service.rest.propagate.PropagateOrganizationalUnitRemoteResourceService;
import ch.dlcm.specification.DepositSpecification;
import ch.dlcm.specification.dto.DepositDtoSpecification;

@RestController
@ConditionalOnBean(PreIngestController.class)
@RequestMapping(UrlPath.PREINGEST_DEPOSIT)
public class DepositController extends AbstractPackageController<DepositDataFile, Deposit> implements ApplicationRoleListService {

  private final IdentifierService identifierService;
  private final PropagateOrganizationalUnitRemoteResourceService orgUnitRemoteService;

  private final DLCMProperties.FileList excludeList;
  private final DLCMProperties.FileList ignoreList;
  private final String preingestLocation;
  private final String preingestTempLocation;
  private final String preingestModulePublicUrl;

  public DepositController(DLCMProperties dlcmProperties, HistoryService historyService, MetadataService metadataService,
          PropagateMetadataTypeRemoteResourceService metadataTypeService, DataFileService<DepositDataFile> dataFileService,
          DataCategoryService dataCategoryService, IdentifierService identifierService,
          PropagateOrganizationalUnitRemoteResourceService orgUnitRemoteService) {
    super(historyService, metadataService, metadataTypeService, dataFileService, dataCategoryService);
    this.identifierService = identifierService;
    this.orgUnitRemoteService = orgUnitRemoteService;

    this.excludeList = dlcmProperties.getParameters().getExcludeList();
    this.ignoreList = dlcmProperties.getParameters().getIgnoreList();
    this.preingestLocation = dlcmProperties.getPreingestLocation();
    this.preingestTempLocation = dlcmProperties.getTempLocation(dlcmProperties.getPreingestLocation());
    this.preingestModulePublicUrl = dlcmProperties.getModule().getPreingest().getPublicUrl();
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowedToCreate(#deposit)")
  public HttpEntity<Deposit> create(@RequestBody Deposit deposit) {
    deposit = ((DepositService) this.itemService).initValuesFromOrgUnit(deposit);
    return super.create(deposit);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'GET')")
  public HttpEntity<Deposit> get(@PathVariable("id") String depositId) {
    return super.get(depositId);
  }


  @GetMapping(params= "lightVersion")
  @PreAuthorize("@depositPermissionService.isAllowedToListWithoutOrgUnit(#search, 'LIST')")
  public HttpEntity<RestCollection<Deposit>> list(@ModelAttribute Deposit search, @RequestParam boolean lightVersion,
          Pageable pageable) {
    String organizationalUnitId = search.getOrganizationalUnitId();
    final DepositSpecification filter = new DepositSpecification(search);
    if (StringTool.isNullOrEmpty(organizationalUnitId)) {
      filter.setOrganizationalUnits(this.getAuthorizedOrgUnitsForAuthenticatedUser());
    }
    Page<Deposit> listItem;
    if (lightVersion) {
      listItem = ((DepositService) this.itemService).findAllWithoutEmbeddedResources(filter, pageable);
    } else {
      listItem = this.itemService.findAll(filter, pageable);
    }
    this.setResourceLinks(listItem);
    final RestCollection<Deposit> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowedToListWithoutOrgUnit(#search, 'LIST')")
  public HttpEntity<RestCollection<Deposit>> list(@ModelAttribute Deposit search, Pageable pageable) {
   return this.list(search, false, pageable);
  }

  @GetMapping(params = { "statusList" })
  @PreAuthorize("@depositPermissionService.isAllowedToListWithoutOrgUnit(#search, 'LIST')")
  public HttpEntity<RestCollection<Deposit>> list(@ModelAttribute DepositDto search, Pageable pageable) {
    String organizationalUnitId = search.getOrganizationalUnitId();
    final DepositSpecification filter = new DepositDtoSpecification(search);
    if (StringTool.isNullOrEmpty(organizationalUnitId)) {
      filter.setOrganizationalUnits(this.getAuthorizedOrgUnitsForAuthenticatedUser());
    }
    final Page<Deposit> listItem = this.itemService.findAll(filter, pageable);
    this.setResourceLinks(listItem);
    final RestCollection<Deposit> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#search, 'LIST')")
  public HttpEntity<RestCollection<Deposit>> advancedSearch(@ModelAttribute Deposit resource,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(resource, search, matchtype, pageable);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#search, 'LIST')")
  public HttpEntity<RestCollection<Deposit>> advancedSearch(@RequestBody Deposit search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(search, matchtype, pageable);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowedToUpdate(#depositId)")
  public HttpEntity<Deposit> update(@PathVariable("id") String depositId, @RequestBody Map<String, Object> updateMap) {
    // Change of status should be done through specific endpoints: submit, approve,..
    final Deposit originalDeposit = this.itemService.findOne(depositId);
    if (updateMap.containsKey(STATUS_FIELD) && !updateMap.get(STATUS_FIELD).equals(originalDeposit.getStatus().toString())) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    // Check update metadata
    if (originalDeposit.getStatus() == EDITING_METADATA &&
            !((DepositDataFileService) this.dataFileService).hasProcessedAllUpdatedMetadata(depositId)) {
      throw new IllegalStateException(this.messageService.get("validation.deposit.unprocessedUpdateMetadataFile"));

    }
    return super.update(depositId, updateMap);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'DELETE')")
  public ResponseEntity<Void> delete(@PathVariable("id") String depositId) {
    final Deposit deposit = this.itemService.findOne(depositId);
    deposit.setStatus(DepositStatus.DELETING);
    this.itemService.save(deposit);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @Override
  @NoOnePermissions
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'SUBMIT_FOR_APPROVAL')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.SUBMIT_FOR_APPROVAL)
  public HttpEntity<Result> submitForApproval(@PathVariable("id") String depositId) {
    if (this.isSubmissionAgreementApproved(depositId, this.getAuthenticatedUserExternalUid())) {
      return this.changeDepositStatus(IN_VALIDATION, depositId);
    } else {
      throw new IllegalStateException(this.messageService.get("validation.deposit.submissionAgreementApprovalNeeded"));
    }
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'APPROVE')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.APPROVE)
  public HttpEntity<Result> approve(@PathVariable("id") String depositId) {
    // Find submitter if there is approval
    String externalUid = ((DepositService) this.itemService).findSubmitterInHistory(depositId);
    if (externalUid == null) {
      externalUid = this.getAuthenticatedUserExternalUid();
    }
    if (this.isSubmissionAgreementApproved(depositId, externalUid)) {
      SolidifyEventPublisher.getPublisher().publishEvent(
              new NotificationMessage(depositId, NotificationStatus.APPROVED, NotificationType.VALIDATE_DEPOSIT_REQUEST.toString()));
      return this.changeDepositStatus(APPROVED, depositId);
    } else {
      throw new IllegalStateException(this.messageService.get("validation.deposit.submissionAgreementApprovalNeeded"));
    }
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'SUBMIT_FOR_APPROVAL')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.APPROVE_SUBMISSION_AGREEMENT)
  public ResponseEntity<Void> approveSubmissionAgreement(@PathVariable("id") String depositId) {
    final Deposit deposit = this.itemService.findOne(depositId);
    final SubmissionPolicy submissionPolicy = deposit.getSubmissionPolicy();
    if (submissionPolicy == null) {
      throw new SolidifyValidationException(new ValidationError("The deposit has no submission policy"));
    }
    final String externalUid = this.getAuthenticatedUserExternalUid();
    final String message = ((DepositService) this.itemService).approveSubmissionAgreement(deposit, externalUid);
    final StatusHistory statusHistory = new StatusHistory(deposit.getClass().getSimpleName(), deposit.getResId(), deposit.getUpdateTime(),
            SUBMISSION_AGREEMENT_APPROVED.toString(), message, deposit.getUpdatedBy());
    this.historyService.saveEvent(statusHistory);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'SUBMIT_FOR_APPROVAL')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.CHECK_SUBMISSION_AGREEMENT)
  public ResponseEntity<Void> checkSubmissionAgreement(@PathVariable("id") String depositId) {
    final Deposit deposit = this.itemService.findOne(depositId);
    final String submissionPolicyId = deposit.getSubmissionPolicyId();
    if (submissionPolicyId == null) {
      throw new SolidifyValidationException(new ValidationError("The deposit has no submission policy"));
    }
    final String externalUid = this.getAuthenticatedUserExternalUid();
    if (((DepositService) this.itemService).isSubmissionAgreementApproved(deposit, externalUid)) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'START_METADATA_EDITING')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.START_METADATA_EDITING)
  public HttpEntity<Result> putInMetadataEditing(@PathVariable("id") String depositId) {
    // Change deposit status
    return this.changeDepositStatus(EDITING_METADATA, depositId);
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'CANCEL_METADATA_EDITING')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.CANCEL_METADATA_EDITING)
  public HttpEntity<Result> cancelMetadataEditing(@PathVariable("id") String depositId) {
    // Check if applicable for collection
    if (!((DepositService) this.itemService).checkIfCancelApplicableForCollection(depositId)) {
      final Result result = new Result(depositId);
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage(this.messageService.get("message.deposit.error.collection"));
      return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }
    this.deleteLastUnprocessedMetadataFile(depositId);
    this.deleteUpdatedFiles(depositId);
    return this.changeDepositStatus(CANCEL_EDITING_METADATA, depositId);
  }

  @TrustedUserPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.CLEAN)
  public HttpEntity<Result> clean(@PathVariable("id") String depositId) {
    final Deposit deposit = this.itemService.findOne(depositId);
    final Result res = new Result(deposit.getResId());
    if (deposit.getStatus() == COMPLETED) {
      deposit.setStatus(DepositStatus.CLEANING);
      this.itemService.save(deposit);
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.deposit.cleaned", new Object[] { depositId }));
    } else {
      res.setStatus(Result.ActionStatus.NON_APPLICABLE);
      res.setMesssage(
              this.messageService.get("message.deposit.error.cannotclean", new Object[] { depositId }));
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @RootPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.PUT_IN_ERROR)
  public HttpEntity<Result> putInError(@PathVariable("id") String depositId) {
    final Deposit deposit = this.itemService.findOne(depositId);
    final Result result = new Result(deposit.getResId());
    if (deposit.getStatus() == COMPLETED || deposit.getStatus() == DepositStatus.CLEANED) {
      result.setMesssage(this.messageService.get("message.deposit.completed", new Object[] { depositId }));
    } else {
      deposit.setStatus(DepositStatus.IN_ERROR);
      this.itemService.save(deposit);
      result.setStatus(ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.deposit.in_error", new Object[] { depositId }));
    }
    result.add(linkTo(this.getClass()).slash(depositId).slash(DLCMActionName.PUT_IN_ERROR).withSelfRel());
    result.add(linkTo(this.getClass()).slash(depositId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @TrustedUserPermissions
  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.SIP_FILE)
  public ResponseEntity<Void> deleteSIP(@PathVariable("id") String depositId) {
    final Deposit deposit = this.itemService.findOne(depositId);
    // Check if working SIP of the deposit is purged
    if (deposit.getSip() == null) {
      throw new SolidifyHttpErrorException(HttpStatus.GONE, "Working SIP of deposit " + depositId + " is gone");
    }
    // Purge SIP of deposit
    ((DepositService) this.itemService).deleteDepositSip(depositId);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId,'ENABLE_REVISION')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.ENABLE_REVISION)
  public HttpEntity<Result> enableRevision(@PathVariable("id") String depositId) {
    if (this.historyService.hasBeenCompleted(depositId)) {
      this.deleteLastUnprocessedMetadataFile(depositId);
      return this.changeDepositStatus(EDITING_METADATA, depositId);
    } else {
      return this.changeDepositStatus(IN_PROGRESS, depositId);
    }
  }

  @GetMapping("/" + DLCMActionName.LIST_EXCLUDE_FILES)
  public HttpEntity<FileList> excludeList() {
    return new ResponseEntity<>(this.excludeList, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.GET_ANONYMIZED_DEPOSIT_PAGE)
  @EveryonePermissions
  public ModelAndView getAnonymizedDepositPage(@PathVariable("id") String anonymizedDepositPageId) {
    return ((DepositService) this.itemService).buildAnonymizedDepositPage(anonymizedDepositPageId);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.DOWNLOAD_ANONYMIZED_DEPOSIT)
  @EveryonePermissions
  public HttpEntity<FileSystemResource> downloadAnonymizedDeposit(@PathVariable("id") String anonymizedDepositId) {
    final String zipFileLocation = this.preingestLocation + "/" + ANONYMIZED_DEPOSITS_FOLDER + "/" + anonymizedDepositId +
            SolidifyConstants.ZIP_EXT;
    if (Files.notExists(Paths.get(zipFileLocation))) {
      throw new SolidifyResourceNotFoundException("No anonymized deposit found");
    }
    return this.buildDownloadResponseEntity(zipFileLocation, anonymizedDepositId + SolidifyConstants.ZIP_EXT,
            SolidifyConstants.ZIP_MIME_TYPE, FileTool.getSize(Paths.get(zipFileLocation)));
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.GENERATE_ANONYMIZED_DEPOSIT_PAGE)
  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'DOWNLOAD_FILE')")
  public HttpEntity<String> generateAnonymizedDepositPage(@PathVariable("id") String depositId) {
    final Deposit deposit = this.itemService.findOne(depositId);
    final String anonymizedDepositPageId = ((DepositService) this.itemService).buildAnonymizedDeposit(deposit);
    return new HttpEntity<>(this.preingestModulePublicUrl + "/" + ResourceName.DEPOSIT + "/" + anonymizedDepositPageId + "/"
            + DLCMActionName.GET_ANONYMIZED_DEPOSIT_PAGE);
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'DOWNLOAD_FILE') "
          + "|| @downloadTokenPermissionService.isAllowed(#depositId, T(ch.dlcm.model.security.DownloadTokenType).DEPOSIT)")
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.DOWNLOAD)
  public HttpEntity<FileSystemResource> getFiles(@PathVariable("id") String depositId,
          @RequestParam(value = "folder", required = false) String folder) {
    final String zipFileLocation = ((DepositService) this.itemService).buildDownloadableDeposit(depositId, folder);
    if (zipFileLocation == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return this.buildDownloadResponseEntity(zipFileLocation, depositId + SolidifyConstants.ZIP_EXT,
            SolidifyConstants.ZIP_MIME_TYPE, FileTool.getSize(Paths.get(zipFileLocation)));
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'DOWNLOAD_SIP_FILE')")
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.SIP_FILE)
  public HttpEntity<FileSystemResource> getSIP(@PathVariable("id") String depositId) {
    // Check if the deposit exists
    final Deposit deposit = this.itemService.findOne(depositId);

    // Check if working SIP exists
    if (deposit.getSip() == null) {
      // Check if the deposit is completed or cleaned
      if (deposit.getStatus() == COMPLETED || deposit.getStatus() == DepositStatus.CLEANED) {
        // Working SIP purged
        return new ResponseEntity<>(HttpStatus.GONE);
      } else {
        // Working SIP not ready
        throw new SolidifyRuntimeException(
                this.messageService.get("preingest.deposit.notready", new Object[] { depositId }));
      }
    }
    // Download the working SIP of the deposit
    final URI fileUri = deposit.getSip();
    return this.buildDownloadResponseEntity(fileUri.getPath(), depositId + SolidifyConstants.ZIP_EXT, DLCMConstants.SIP_MIME_TYPE,
            FileTool.getSize(fileUri));
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'HISTORY')")
  @Override
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable("id") String depositId, Pageable pageable) {
    return super.history(depositId, pageable);
  }

  @GetMapping("/" + DLCMActionName.LIST_IGNORE_FILES)
  public HttpEntity<FileList> ignoreList() {
    return new ResponseEntity<>(this.ignoreList, HttpStatus.OK);
  }

  @GetMapping("/" + DLCMActionName.LIST_ACCESS_TYPE)
  public HttpEntity<Access[]> listAccessTypes() {
    return new ResponseEntity<>(Access.values(), HttpStatus.OK);
  }

  @GetMapping("/" + DLCMActionName.LIST_STATUS)
  public HttpEntity<DepositStatus[]> listStatus() {
    return new ResponseEntity<>(DepositStatus.values(), HttpStatus.OK);
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'REJECT')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.REJECT)
  public HttpEntity<Result> reject(@PathVariable("id") String depositId, @RequestParam(value = DLCMConstants.REASON) String reason) {
    SolidifyEventPublisher.getPublisher()
            .publishEvent(new NotificationMessage(depositId, NotificationStatus.REFUSED, NotificationType.VALIDATE_DEPOSIT_REQUEST.toString()));
    if (((DepositService) this.itemService).hasBeenInEditingMetadata(depositId)) {
      return this.changeDepositStatus(EDITING_METADATA_REJECTED, depositId, reason);
    } else {
      return this.changeDepositStatus(REJECTED, depositId, reason);
    }
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'RESERVE_DOI')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.RESERVE_DOI)
  public HttpEntity<Deposit> reserveDOI(@PathVariable("id") String depositId) {

    Deposit item = this.itemService.findOne(depositId);

    item.setDoi(this.identifierService.createDOI(item.getResId()));

    item = this.itemService.save(item);
    this.addLinks(item);
    return new ResponseEntity<>(item, HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping("/" + SolidifyConstants.SCHEMA)
  public HttpEntity<StreamingResponseBody> schema(@RequestParam(value = "version", required = false) String version) {
    return this.getSchemaStream(version, new DepositDataFile());
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'UPLOAD_FILE')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.UPLOAD)
  public HttpEntity<DepositDataFile> uploadFile(@PathVariable("id") String depositId,
          @RequestParam(DLCMConstants.FILE) MultipartFile file,
          @RequestParam(value = DLCMConstants.CATEGORY, required = false) String dataCategory,
          @RequestParam(value = DLCMConstants.TYPE, required = false) String dataType,
          @RequestParam(value = DLCMConstants.FOLDER, required = false) String relLocation,
          @RequestParam(value = DLCMConstants.METADATA_TYPE, required = false) String metadataType,
          @RequestParam(value = DLCMConstants.CHECKSUM_MD5, required = false) String checksumMd5,
          @RequestParam(value = DLCMConstants.CHECKSUM_MD5_ORIGIN, required = false) String checksumMd5Origin,
          @RequestParam(value = DLCMConstants.CHECKSUM_SHA1, required = false) String checksumSha1,
          @RequestParam(value = DLCMConstants.CHECKSUM_SHA1_ORIGIN, required = false) String checksumSha1Origin,
          @RequestParam(value = DLCMConstants.CHECKSUM_SHA256, required = false) String checksumSha256,
          @RequestParam(value = DLCMConstants.CHECKSUM_SHA256_ORIGIN, required = false) String checksumSha256Origin,
          @RequestParam(value = DLCMConstants.CHECKSUM_SHA512, required = false) String checksumSha512,
          @RequestParam(value = DLCMConstants.CHECKSUM_SHA512_ORIGIN, required = false) String checksumSha512Origin,
          @RequestParam(value = DLCMConstants.CHECKSUM_CRC32, required = false) String checksumCrc32,
          @RequestParam(value = DLCMConstants.CHECKSUM_CRC32_ORIGIN, required = false) String checksumCrc32Origin)
          throws IOException {

    final FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>(new DepositDataFile());
    fileUploadDto.setOriginalFileName(Normalizer.normalize(file.getOriginalFilename(), Normalizer.Form.NFC));
    fileUploadDto.setInputStream(file.getInputStream());
    if (!StringTool.isNullOrEmpty(dataCategory)) {
      fileUploadDto.setDataCategory(dataCategory);
    }
    if (!StringTool.isNullOrEmpty(dataType)) {
      fileUploadDto.setDataType(dataType);
    }
    if (!StringTool.isNullOrEmpty(metadataType)) {
      fileUploadDto.setMetadataType(metadataType);
    }
    if (!StringTool.isNullOrEmpty(relLocation)) {
      fileUploadDto.setRelLocation(relLocation);
    }
    // check data category and type
    this.checkDataCategoriesAndUploadUpdatedFiles(depositId, fileUploadDto.getDataCategory(), fileUploadDto.getDataType());

    Map<ChecksumAlgorithm, String> checksums = new EnumMap<>(ChecksumAlgorithm.class);
    Map<ChecksumAlgorithm, DataFileChecksum.ChecksumOrigin> checksumsOrigin = new EnumMap<>(ChecksumAlgorithm.class);
    if (!StringTool.isNullOrEmpty(checksumMd5)) {
      checksums.put(ChecksumAlgorithm.MD5, checksumMd5);
      if (!StringTool.isNullOrEmpty(checksumMd5Origin)) {
        checksumsOrigin.put(ChecksumAlgorithm.MD5, DataFileChecksum.ChecksumOrigin.valueOf(checksumMd5Origin));
      }
    }
    if (!StringTool.isNullOrEmpty(checksumSha1)) {
      checksums.put(ChecksumAlgorithm.SHA1, checksumSha1);
      if (!StringTool.isNullOrEmpty(checksumSha1Origin)) {
        checksumsOrigin.put(ChecksumAlgorithm.SHA1, DataFileChecksum.ChecksumOrigin.valueOf(checksumSha1Origin));
      }
    }
    if (!StringTool.isNullOrEmpty(checksumSha256)) {
      checksums.put(ChecksumAlgorithm.SHA256, checksumSha256);
      if (!StringTool.isNullOrEmpty(checksumSha256Origin)) {
        checksumsOrigin.put(ChecksumAlgorithm.SHA256, DataFileChecksum.ChecksumOrigin.valueOf(checksumSha256Origin));
      }
    }
    if (!StringTool.isNullOrEmpty(checksumSha512)) {
      checksums.put(ChecksumAlgorithm.SHA512, checksumSha512);
      if (!StringTool.isNullOrEmpty(checksumSha512Origin)) {
        checksumsOrigin.put(ChecksumAlgorithm.SHA512, DataFileChecksum.ChecksumOrigin.valueOf(checksumSha512Origin));
      }
    }
    if (!StringTool.isNullOrEmpty(checksumCrc32)) {
      checksums.put(ChecksumAlgorithm.CRC32, checksumCrc32);
      if (!StringTool.isNullOrEmpty(checksumCrc32Origin)) {
        checksumsOrigin.put(ChecksumAlgorithm.CRC32, DataFileChecksum.ChecksumOrigin.valueOf(checksumCrc32Origin));
      }
    }
    fileUploadDto.setChecksums(checksums);
    fileUploadDto.setChecksumsOrigin(checksumsOrigin);
    return this.upload(depositId, fileUploadDto, this.preingestTempLocation, UploadMode.STANDARD);
  }

  /**
   * Endpoint allowing to upload a ZIP archive containing many datafiles. If dataCategory and dataType are given, all datafiles in ZIP archive
   * get these values.
   *
   * @param depositId    deposit id
   * @param archiveFile
   * @param dataCategory
   * @param dataType
   * @return List of deposit dataFile
   * @throws IOException
   */
  @Transactional
  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'UPLOAD_FILE')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + ActionName.UPLOAD_ARCHIVE)
  public HttpEntity<List<DepositDataFile>> uploadArchive(@PathVariable("id") String depositId,
          @RequestParam(DLCMConstants.FILE) MultipartFile archiveFile,
          @RequestParam(value = DLCMConstants.CATEGORY, required = false) DataCategory dataCategory,
          @RequestParam(value = DLCMConstants.TYPE, required = false) DataCategory dataType,
          @RequestParam(value = DLCMConstants.METADATA_TYPE, required = false) String metadataType)
          throws IOException, NoSuchAlgorithmException {

    final Deposit deposit = this.itemService.findOne(depositId);
    if (!deposit.isModifiable()) {
      throw new IllegalStateException(this.messageService.get("validation.deposit.unmodifiable"));
    }
    // check data category and type
    this.checkDataCategoriesAndUploadUpdatedFiles(depositId, dataCategory, dataType);
    if (!deposit.checkDataFileToAdd(dataCategory, dataType)) {
      throw new IllegalStateException(this.messageService.get("validation.dataFile.dataCategory.invalid",
              new Object[] { dataCategory, dataType }));
    }

    // Create Deposit tmp folder
    final String tmpFolder = this.preingestTempLocation + File.separatorChar + depositId;
    final Path tmpFolderPath = Paths.get(tmpFolder);
    if (!FileTool.ensureFolderExists(tmpFolderPath)) {
      throw new IOException(this.messageService.get("message.upload.folder.notcreated", new Object[] { tmpFolder }));
    }
    Files.copy(archiveFile.getInputStream(), tmpFolderPath.resolve(archiveFile.getOriginalFilename()));

    // Create folder where zipped files must be extracted
    final Path destinationFilePath = Paths.get(tmpFolder).resolve(archiveFile.getOriginalFilename());
    final String zipFolderName = destinationFilePath.getFileName().toString();
    final String fileHash = ChecksumTool.computeChecksum(
            new BufferedInputStream(new FileInputStream(destinationFilePath.toFile()), SolidifyConstants.BUFFER_SIZE), ChecksumAlgorithm.CRC32);
    final String extractFolder = tmpFolder + "/unzip/" + zipFolderName + "_" + fileHash + "_" + Instant.now().toEpochMilli();
    final Path extractFolderPath = Paths.get(extractFolder);

    if (!FileTool.ensureFolderExists(extractFolderPath)) {
      // Delete ZIP file
      FileTool.deleteFile(destinationFilePath);
      throw new IOException(this.messageService.get("message.upload.folder.notexist", new Object[] { extractFolderPath }));
    }

    final ZipTool zip = new ZipTool(destinationFilePath.toUri());

    if (zip.checkZip()) {

      if (zip.unzipFiles(extractFolderPath, true)) {

        // Delete ZIP file
        FileTool.deleteFile(destinationFilePath);

        // Loop on extracted files and create a DepositDatafile for each
        final List<Path> extractedFilesPaths = FileTool.findFiles(extractFolderPath);

        final List<DepositDataFile> filesList = this.getFileList(deposit, extractFolderPath,
                extractedFilesPaths, dataCategory, dataType, metadataType);

        this.itemService.save(deposit);
        return new ResponseEntity<>(filesList, HttpStatus.OK);

      } else {
        // Delete ZIP file
        FileTool.deleteFile(destinationFilePath);
        throw new IllegalStateException(this.messageService.get("message.upload.zip.invalid"));
      }
    } else {
      // Delete ZIP file
      FileTool.deleteFile(destinationFilePath);
      throw new IllegalStateException(this.messageService.get("message.upload.zip.invalid"));
    }
  }

  private void checkDataCategoriesAndUploadUpdatedFiles(String depositId, DataCategory dataCategory, DataCategory dataType) {
    // Check data category & type
    if (dataCategory != null) {
      if (!this.dataCategoryService.checkDataFileCategory(dataCategory)) {
        throw new IllegalStateException(this.messageService.get("validation.dataFile.dataCategory.unsupported", new Object[] { dataCategory }));
      }
      if (dataType != null && !this.dataCategoryService.checkDataFileCategoryAndType(dataCategory, dataType)) {
        throw new IllegalStateException(this.messageService.get("validation.dataFile.dataCategory.unsupported", new Object[] { dataCategory }));
      }
      // For files of categoryInternal: Check if a file from the same dataType has been already uploaded
      if (dataType != null && DataCategory.Internal.equals(dataCategory) && !((DepositService) this.itemService).canUploadUpdatedFiles(depositId, dataType)) {
        throw new SolidifyValidationException(new ValidationError("A file of type " + dataType + " has been already uploaded."));
      }
    }
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.CHECK_COMPLIANCE)
  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'CHECK_COMPLIANCE')")
  public HttpEntity<Result> checkCompliance(@PathVariable("id") String depositId) {
    final Result res = new Result(depositId);
    final Deposit deposit = this.itemService.findOne(depositId);
    if (deposit.getStatus().equals(DepositStatus.IN_PROGRESS) || deposit.getStatus().equals(DepositStatus.CLEANING)) {
      res.setStatus(Result.ActionStatus.NON_APPLICABLE);
      res.setMesssage(this.messageService.get("message.deposit.checking_compliance.notcompleted", new Object[] { depositId }));
    } else if ((!deposit.getStatus().equals(DepositStatus.COMPLETED) && !deposit.getStatus().equals(DepositStatus.CLEANED))
            || deposit.isCollection()) {
      res.setStatus(Result.ActionStatus.NON_APPLICABLE);
      res.setMesssage(this.messageService.get("message.deposit.checking_compliance.inprogress", new Object[] { depositId }));
    } else {
      // Set to status to trigger check compliance level
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      if (deposit.getStatus().equals(DepositStatus.COMPLETED)) {
        deposit.setStatus(DepositStatus.CHECKING_COMPLIANCE);
      } else {
        deposit.setStatus(DepositStatus.CHECKING_COMPLIANCE_CLEANED);
      }
      this.itemService.save(deposit);
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("message.deposit.status.checking_compliance", new Object[] { depositId }));
    }

    res.add(linkTo(this.getClass()).slash(depositId).slash(DLCMActionName.CHECK_COMPLIANCE).withSelfRel());
    res.add(linkTo(this.getClass()).slash(depositId).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @TrustedUserPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.START_METADATA_UPGRADE)
  public HttpEntity<Result> putInMetadataUpgrading(@PathVariable String id) {
    // Change Deposit status
    return this.changeDepositStatus(DepositStatus.UPGRADING_METADATA, id);
  }

  @PreAuthorize("@depositPermissionService.isAllowed(#depositId, 'UPLOAD_FILE')")
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + DLCMActionName.UPLOAD_METADATA)
  public HttpEntity<DepositDataFile> uploadMetadata(@PathVariable("id") String depositId, @RequestParam(DLCMConstants.FILE) MultipartFile file)
          throws IOException {
    final Deposit deposit = this.itemService.findOne(depositId);
    if (!(deposit.getStatus().equals(EDITING_METADATA))) {
      throw new IllegalStateException(this.messageService.get("validation.deposit.editingMetadata"));
    }
    if (!((DepositDataFileService) this.dataFileService).hasProcessedAllUpdatedMetadata(depositId)) {
      throw new IllegalStateException(this.messageService.get("validation.deposit.unprocessedUpdateMetadataFile"));
    }
    final FileUploadDto<DepositDataFile> fileUploadDto = new FileUploadDto<>(new DepositDataFile());
    fileUploadDto.setOriginalFileName(file.getOriginalFilename());
    fileUploadDto.setInputStream(file.getInputStream());
    fileUploadDto.setDataCategory(DataCategory.Package);
    fileUploadDto.setDataType(DataCategory.UpdatedMetadata);
    fileUploadDto.setRelLocation("/");
    fileUploadDto.setChecksums(new EnumMap<>(ChecksumAlgorithm.class));
    fileUploadDto.setChecksumsOrigin(new EnumMap<>(ChecksumAlgorithm.class));
    return this.upload(depositId, fileUploadDto, this.preingestTempLocation, UploadMode.UPLOAD_METADATA);
  }

  @Override
  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_DATA_CATEGORY)
  public HttpEntity<List<DataCategory>> listDataCategory() {
    return new ResponseEntity<>(this.dataCategoryService.listDataFileCategories(), HttpStatus.OK);
  }

  @Override
  @EveryonePermissions
  @GetMapping("/" + DLCMActionName.LIST_DATA_TYPE)
  public HttpEntity<List<DataCategory>> listDataType(@RequestParam(required = true) String category) {
    return new ResponseEntity<>(this.dataCategoryService.listDataFileTypes(category), HttpStatus.OK);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listStatus()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listAccessTypes()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).excludeList()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).ignoreList()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listVersions()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listDataCategory()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).listDataType(this.dataCategoryService.getDataFileDefaultCategories().toString()))
            .withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).schema(DLCMMetadataVersion.getDefaultVersion().getVersion())).withRel(SolidifyConstants.SCHEMA));
  }

  private HttpEntity<Result> changeDepositStatus(DepositStatus newStatus, String depositId) {
    return this.changeDepositStatus(newStatus, depositId, null);
  }

  private HttpEntity<Result> changeDepositStatus(DepositStatus newStatus, String depositId, String statusMessage) {

    final Deposit deposit = this.itemService.findOne(depositId);

    final Result result = new Result(depositId);
    if (((DepositService) this.itemService).statusUpdateIsValid(deposit, newStatus)) {
      if (newStatus == EDITING_METADATA) {
        Integer preferredStorage = deposit.getPreservationPolicy().getMainStorage();
        // Check if archive is completed
        ((DepositService) this.itemService).checkArchiveIsEditable(depositId, preferredStorage);
        // If archive is in collections, put all its collection in edition
        ((DepositService) this.itemService).putInMetadataEditingCollections(depositId);
        // Purge SIP of the deposit
        ((DepositService) this.itemService).deleteDepositSip(depositId);
      }
      deposit.setStatus(newStatus);
      deposit.setStatusMessage(statusMessage);
      this.itemService.save(deposit);
      result.setStatus(Result.ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("message.deposit.status"));
      return new ResponseEntity<>(result, HttpStatus.OK);
    } else {
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage(this.messageService.get("message.deposit.status.error"));
      return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }
  }

  private List<DepositDataFile> getFileList(Deposit deposit, Path extractFolder, List<Path> extractedFiles, DataCategory dataCategory,
          DataCategory dataType, String metadataType) {
    final List<DepositDataFile> fileList = new ArrayList<>();

    for (final Path extractedFilePath : extractedFiles) {

      final DepositDataFile df = new DepositDataFile(deposit);

      df.setSourceData(extractedFilePath.toUri());
      if (dataCategory != null) {
        df.setDataCategory(dataCategory);
      }
      if (dataType != null) {
        df.setDataType(dataType);
      }
      // Set metadata type
      if (!StringTool.isNullOrEmpty(metadataType)) {
        df.setMetadataType(this.metadataTypeService.findOneWithCache(metadataType));
      }
      final Path relativePath = extractFolder.toAbsolutePath().relativize(extractedFilePath).getParent();
      if (relativePath != null && !StringTool.isNullOrEmpty(relativePath.toString())) {
        df.setRelativeLocation(relativePath.toString());
      }

      this.setAuthenticatedUserProperties(df);

      deposit.getDataFiles().add(df);
      fileList.add(df);
    }
    return fileList;
  }

  private void deleteLastUnprocessedMetadataFile(String depositId) {
    if (!((DepositDataFileService) this.dataFileService).hasProcessedAllUpdatedMetadata(depositId)) {
      // Delete last updated metadata file
      ((DepositDataFileService) this.dataFileService).deleteLastUpdatedMetadaFile(depositId);
    }
  }

  private void deleteUpdatedFiles(String depositId) {
    ((DepositDataFileService) this.dataFileService).deleteUpdatedFiles(depositId);
  }

  private List<OrganizationalUnit> getAuthorizedOrgUnitsForAuthenticatedUser() {
    final List<OrganizationalUnit> orgUnitList;
    if (this.isRootOrTrustedOrAdminRole()) {
      orgUnitList = this.orgUnitRemoteService.getOrgUnitList(null,
              PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE)).getData();
    } else {
      RestCollection<AuthorizedOrganizationalUnitDto> authorizedOrgUnitWithRoleList = this.orgUnitRemoteService
              .getAuthorizedOrgUnitWithVisitorRoleList(
                      PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE));
      orgUnitList = new ArrayList<>();
      authorizedOrgUnitWithRoleList.getData().forEach(auth -> {
        OrganizationalUnit organizationalUnit = new OrganizationalUnit();
        organizationalUnit.setResId(auth.getResId());
        orgUnitList.add(organizationalUnit);
      });
    }
    return orgUnitList;
  }

  private boolean isSubmissionAgreementApproved(String depositId, String externalUid) {
    final Deposit deposit = this.itemService.findOne(depositId);
    if (!SubmissionAgreementType.WITHOUT.equals(deposit.getSubmissionPolicy().getSubmissionAgreementType())) {
      return ((DepositService) this.itemService).isSubmissionAgreementApproved(deposit, externalUid);
    }
    return true;
  }
}

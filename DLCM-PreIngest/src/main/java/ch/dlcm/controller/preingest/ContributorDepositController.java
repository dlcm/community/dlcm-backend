/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - ContributorDepositController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preingest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.service.HttpRequestInfoProvider;

import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositContributor;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;
import ch.dlcm.service.ContributorService;
import ch.dlcm.service.rest.propagate.PropagateOrganizationalUnitRemoteResourceService;
import ch.dlcm.specification.DepositContributorSpecification;

@RestController
@UserPermissions
@ConditionalOnBean(PreIngestController.class)
@RequestMapping(UrlPath.PREINGEST_CONTRIBUTOR + SolidifyConstants.URL_PARENT_ID + ResourceName.DEPOSIT)
public class ContributorDepositController extends SolidifyController {

  protected final HttpRequestInfoProvider httpRequestInfoProvider;

  private final ContributorService contributorService;

  private final PropagateOrganizationalUnitRemoteResourceService orgUnitService;

  public ContributorDepositController(HttpRequestInfoProvider httpRequestInfoProvider, ContributorService contributorService,
          PropagateOrganizationalUnitRemoteResourceService orgUnitService) {
    this.httpRequestInfoProvider = httpRequestInfoProvider;
    this.contributorService = contributorService;
    this.orgUnitService = orgUnitService;
  }

  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<Deposit> get(@PathVariable String parentid, @PathVariable String id) {
    this.contributorService.findOne(parentid);
    final Deposit deposit = this.contributorService.getContributorRepository().findByPersonAndDeposit(parentid, id).getCompositeKey()
            .getDeposit();
    deposit.addLinks(linkTo(methodOn(this.getClass()).list(parentid, null, null)), false, true);
    return new ResponseEntity<>(deposit, HttpStatus.OK);
  }

  @GetMapping
  @SuppressWarnings("squid:S4684")
  public HttpEntity<RestCollection<Deposit>> list(@PathVariable String parentid, @ModelAttribute DepositContributor filterItem,
          Pageable pageable) {
    // Check if contributor exists
    this.contributorService.findOne(parentid);
    // Filter on contributor & authorized orgUnits
    filterItem.getCompositeKey().setPersonId(parentid);
    final DepositContributorSpecification search = new DepositContributorSpecification(filterItem);
    search.setOrganizationalUnits(this.orgUnitService.getAuthorizedOrganizationalUnit(this.httpRequestInfoProvider.getPrincipal()));
    // List deposit contributors
    final Page<DepositContributor> dcList = this.contributorService.getContributorRepository().findAll(search, pageable);
    // Extract deposits
    final List<Deposit> dList = new ArrayList<>();
    for (final DepositContributor dc : dcList) {
      final Deposit d = dc.getCompositeKey().getDeposit();
      d.addLinks(linkTo(methodOn(this.getClass()).list(parentid, null, null)), false, false);
      dList.add(d);
    }
    // create collection & set page info
    final Page<Deposit> deposits = new PageImpl<>(dList, pageable, dcList.getTotalElements());
    final RestCollection<Deposit> collection = new RestCollection<>(deposits, pageable);
    collection.add(linkTo(this.getClass(), parentid).withSelfRel());
    collection.add((Tool.parentLink((linkTo(this.getClass(), parentid)).toUriComponentsBuilder()))
            .withRel(ActionName.PARENT));
    this.addPageLinks(linkTo(this.getClass(), parentid), collection, pageable);
    collection
            .add(Tool.filter(linkTo(this.getClass(), parentid).toUriComponentsBuilder(), "deposit.status", "IN_PROGRESS")
                    .withRel("deposits-in-progress"));
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }
}

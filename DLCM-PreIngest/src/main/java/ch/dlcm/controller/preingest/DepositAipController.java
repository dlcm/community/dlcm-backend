/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositAipController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preingest;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.AssociationRemoteController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.CompositeResourceService;

import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.rest.ResourceName;
import ch.dlcm.rest.UrlPath;

/**
 * The DepositAipController is responsible for associating multiple Archival Information Packages (AIPs) to a deposit
 * during the creation of a collection.
 */
@RestController
@ConditionalOnBean(PreIngestController.class)
@RequestMapping(UrlPath.PREINGEST_DEPOSIT + SolidifyConstants.URL_PARENT_ID + ResourceName.AIP)
public class DepositAipController extends AssociationRemoteController<Deposit, ArchivalInfoPackage> {

  public DepositAipController(CompositeResourceService<Deposit> itemService) {
    super(itemService, ArchivalInfoPackage.class);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'UPDATE')")
  public HttpEntity<List<ArchivalInfoPackage>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'GET')")
  public HttpEntity<ArchivalInfoPackage> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'GET')")
  public HttpEntity<RestCollection<ArchivalInfoPackage>> list(@PathVariable String parentid, Pageable pageable) {
    return super.list(parentid, pageable);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#id, 'UPDATE')")
  public HttpEntity<ArchivalInfoPackage> update(@PathVariable String parentid, @PathVariable String id, @RequestBody ArchivalInfoPackage t2) {
    return super.update(parentid, id, t2);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'UPDATE')")
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  @PreAuthorize("@depositPermissionService.isAllowed(#parentid, 'UPDATE')")
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @PathVariable String[] ids) {
    return super.deleteList(parentid, ids);
  }

}

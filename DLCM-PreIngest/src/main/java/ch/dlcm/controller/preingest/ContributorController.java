/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - ContributorController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.controller.preingest;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.NoSqlResourceReadOnlyController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.service.NoSqlResourceService;

import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.preingest.Contributor;
import ch.dlcm.rest.DLCMActionName;
import ch.dlcm.rest.UrlPath;

@RestController
@UserPermissions
@ConditionalOnBean(PreIngestController.class)
@RequestMapping(UrlPath.PREINGEST_CONTRIBUTOR)
public class ContributorController extends NoSqlResourceReadOnlyController<Contributor> {

  public ContributorController(NoSqlResourceService<Contributor> noSqlResourceService) {
    super(noSqlResourceService);
  }

  @Override
  public HttpEntity<Contributor> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<Contributor>> list(@ModelAttribute Contributor search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(Tool.filter(linkTo(this.getClass()).toUriComponentsBuilder(), "depositNumber", "1").withRel(DLCMActionName.CONTRIBUTOR_WITH_DEPOSITS));
  }

}

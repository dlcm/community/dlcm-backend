/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - PreparationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.preparation;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.DepositService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.exception.DLCMCollectionDepositStatusException;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.policies.SubmissionAgreementType;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.service.DLCMService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePrivateMetadataRemoteResourceService;

public abstract class PreparationService extends DLCMService {

  private final String preingestLocation;
  private boolean researchDataEnabled;
  private boolean administrativeDataEnabled;
  private final long maxFileNumber;

  protected final HistoryService historyService;
  private final DepositService depositService;
  private final FallbackSubmissionInfoPackageRemoteResourceService sipRemoteService;
  private final FallbackArchivalInfoPackageRemoteResourceService aipRemoteService;
  private final TrustedArchivePrivateMetadataRemoteResourceService archiveMetadataRemoteService;

  protected PreparationService(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          HistoryService historyService,
          DepositService depositService,
          FallbackSubmissionInfoPackageRemoteResourceService sipRemoteService,
          FallbackArchivalInfoPackageRemoteResourceService aipRemoteService,
          TrustedArchivePrivateMetadataRemoteResourceService archiveMetadataRemoteService) {
    super(messageService, dlcmProperties);
    this.preingestLocation = dlcmProperties.getPreingestLocation();
    this.maxFileNumber = dlcmProperties.getParameters().getMaxFileNumber();
    this.researchDataEnabled = dlcmProperties.getParameters().isResearchDataEnabled();
    this.administrativeDataEnabled = dlcmProperties.getParameters().isAdministrativeDataEnabled();
    this.historyService = historyService;
    this.depositService = depositService;
    this.sipRemoteService = sipRemoteService;
    this.aipRemoteService = aipRemoteService;
    this.archiveMetadataRemoteService = archiveMetadataRemoteService;
  }

  // **************************
  // ** Methods to implement **
  // **************************

  // Transform Deposit => SIP
  public abstract String generateSIP(Deposit deposit);

  // Transform Deposit => SIP for update
  public abstract String generateSIPForUpdate(Deposit deposit);

  // Get status of Information Package
  public abstract DepositStatus getStatus(Deposit deposit);

  // *********************
  // ** Default Methods **
  // *********************

  // Verify deposit
  public void checkDeposit(Deposit deposit) {
    // Check if there are submission policies
    if (deposit.getSubmissionPolicy() == null) {
      throw new SolidifyCheckingException(this.messageService.get("checking.policy.submission.missing"));
    } else {
      // Check submission policy
      this.checkDepositSubmissionPolicy(deposit);
    }

    // Check if there are preservation policies
    if (deposit.getPreservationPolicy() == null) {
      throw new SolidifyCheckingException(this.messageService.get("checking.policy.preservation.missing"));
    }

    // Check DataUsePolicy absence or presence depending on Metadata version
    final long nbDua = this.getDepositMetadataCategoryCount(deposit, DataCategory.Internal, DataCategory.ArchiveDataUseAgreement);
    this.checkDepositDua(deposit, nbDua, deposit.getContainsUpdatedMetadata());
    // Check if there is no or one deposit thumbnail
    final long datasetThumbnailCount = this.getDepositMetadataCategoryCount(deposit, DataCategory.Internal, DataCategory.DatasetThumbnail);
    final long archiveThumbnailCount = this.getDepositMetadataCategoryCount(deposit, DataCategory.Internal, DataCategory.ArchiveThumbnail);
    this.checkDepositThumbnail(deposit, datasetThumbnailCount, archiveThumbnailCount);
    // Check if there is no or one deposit README
    final long readmeCount = this.getDepositMetadataCategoryCount(deposit, DataCategory.Internal, DataCategory.ArchiveReadme);
    this.checkDepositReadme(deposit, readmeCount);
    // Check data files
    this.checkDepositDataFiles(deposit);
  }

  // Verify deposit for update
  public void checkDepositForUpdate(Deposit deposit) {
    // Check DataUsePolicy absence or presence depending on Metadata version
    final long nbDua = this.depositService.countUpdatedFiles(deposit.getResId(), DataCategory.ArchiveDataUseAgreement);
    this.checkDepositDua(deposit, nbDua, true);
    // Check if there is no or one deposit thumbnail
    final long datasetThumbnailCount = this.depositService.countUpdatedFiles(deposit.getResId(), DataCategory.DatasetThumbnail);
    final long archiveThumbnailCount = this.depositService.countUpdatedFiles(deposit.getResId(), DataCategory.ArchiveThumbnail);
    this.checkDepositThumbnail(deposit, datasetThumbnailCount, archiveThumbnailCount);
    // Check if there is no or one deposit README
    final long readmeCount = this.depositService.countUpdatedFiles(deposit.getResId(), DataCategory.ArchiveReadme);
    this.checkDepositReadme(deposit, readmeCount);
  }

  private void checkDepositSubmissionPolicy(Deposit deposit) {
    // Check if submission need an approval. It means that there is validation before approbation
    if (Boolean.TRUE.equals(deposit.getSubmissionPolicy().getSubmissionApproval())) {
      // Verify history status
      this.historyService.checkLastEvents(deposit.getResId(), this.messageService.get("checking.policy.submission.novalidation"),
              DepositStatus.CHECKED.toString(), DepositStatus.APPROVED.toString(), DepositStatus.IN_VALIDATION.toString());
    }
    // Check submission agreement
    if (!SubmissionAgreementType.WITHOUT.equals(deposit.getSubmissionPolicy().getSubmissionAgreementType())) {
      final String userExternalUid = this.depositService.findSubmitterInHistory(deposit.getResId());
      if (!this.depositService.isSubmissionAgreementApproved(deposit, userExternalUid)) {
        throw new SolidifyCheckingException(this.messageService.get("checking.policy.submission.missing-agreement"));
      }
    }
  }

  private void checkDepositDua(Deposit deposit, long nbDua, boolean update) {
    if (deposit.getMetadataVersion().getVersionNumber() < DLCMMetadataVersion.V3_1.getVersionNumber()) {
      // DUA not supported
      if (nbDua > 0) {
        throw new SolidifyCheckingException(this.messageService.get("checking.dua.notsupported"));
      }
    } else {
      // DUA supported & must be valid
      if (!this.hasNeededDua(deposit, nbDua, update)) {
        throw new SolidifyCheckingException(this.messageService.get("checking.dua.missing"));
      }
      if (this.hasUnneededDua(deposit, nbDua)) {
        throw new SolidifyCheckingException(this.messageService.get("checking.dua.unneeded"));
      }
    }
  }

  private void checkDepositReadme(Deposit deposit, long readmeCount) {
    if (deposit.getMetadataVersion().getVersionNumber() < DLCMMetadataVersion.V3_1.getVersionNumber()) {
      // README not supported
      if (readmeCount > 0) {
        throw new SolidifyCheckingException(this.messageService.get("checking.readme.notsupported"));
      }
    } else {
      // README supported
      if (readmeCount > 1) {
        throw new SolidifyCheckingException(this.messageService.get("checking.readme.multiple"));
      }
    }
  }

  private void checkDepositThumbnail(Deposit deposit, long datasetThumbnailCount, long archiveThumbnailCount) {
    final DLCMMetadataVersion depositVersion = deposit.getMetadataVersion();
    if (depositVersion.getVersionNumber() < DLCMMetadataVersion.V2_1.getVersionNumber()) {
      // Thumbnail not supported
      if (datasetThumbnailCount > 0 || archiveThumbnailCount > 0) {
        throw new SolidifyCheckingException(this.messageService.get("checking.thumbnail.notsupported"));
      }
    } else if (depositVersion.getVersionNumber() >= DLCMMetadataVersion.V2_1.getVersionNumber()
            && depositVersion.getVersionNumber() < DLCMMetadataVersion.V3_1.getVersionNumber()) {
      // Only dataset thumbnail supported
      if (archiveThumbnailCount > 0) {
        throw new SolidifyCheckingException(this.messageService.get("checking.thumbnail.archive.notsupported"));
      }
      if (datasetThumbnailCount > 1) {
        throw new SolidifyCheckingException(this.messageService.get("checking.thumbnail.multiple"));
      }
    } else {
      // Only archive thumbnail supported
      if (datasetThumbnailCount > 0) {
        throw new SolidifyCheckingException(this.messageService.get("checking.thumbnail.dataset.notsupported"));
      }
      if (archiveThumbnailCount > 1) {
        throw new SolidifyCheckingException(this.messageService.get("checking.thumbnail.multiple"));
      }
    }
  }

  public boolean depositHasMetadata(Deposit deposit) {
    final long metadataCount = this.getDepositMetadataCategoryCount(deposit, DataCategory.Package, DataCategory.Metadata);
    return (metadataCount > 0);
  }

  // Get working directory for Deposit
  public Path getDepositFolder(String depositId) {
    return Paths.get(this.preingestLocation).resolve(depositId);
  }

  // Get working directory for Deposit for updating metadata
  public Path getDepositFolderForUpdate(String depositId) {
    return Paths.get(this.preingestLocation).resolve(depositId).resolve(DLCMConstants.UPDATE_FOLDER);
  }

  // Get URI of SIP
  public URI getSIPUri(String depositId) {
    return Paths.get(this.preingestLocation).resolve(depositId + SolidifyConstants.ZIP_EXT).toUri();
  }

  // Send SIP to Ingest module
  public String sendSIP(Deposit deposit) {
    return this.sipRemoteService.sendSIP(deposit);
  }

  // Send updated metadata to Ingest module
  public void sendSIPForUpdate(Deposit deposit) {
    this.sipRemoteService.sendSIPForUpdate(deposit);
  }

  // Purge deposit files
  public void deleteAllFiles(Deposit deposit) {
    // Check if deposit folder exists
    // and Delete file on file system
    if (FileTool.checkFile(this.getDepositFolder(deposit.getResId())) && !FileTool.deleteFolder(this.getDepositFolder(deposit.getResId()))) {
      throw new SolidifyFileDeleteException("Cannot delete deposit folder ('" + deposit.getResId() + "')");
    }
  }

  private long getDepositMetadataCategoryCount(Deposit deposit, DataCategory category, DataCategory datatype) {
    if (datatype != null) {
      return this.depositService.countDataFilesWithDataCategoryAndDataType(deposit.getResId(), category, datatype);
    } else {
      return this.depositService.countDataFilesWithDataCategory(deposit.getResId(), category);
    }
  }

  public void checkCollectionDeposit(Deposit deposit) {

    for (final String aipId : deposit.getCollection()) {
      // Check if deposit status is completed or cleaned
      Deposit aipDeposit = this.depositService.findOne(this.archiveMetadataRemoteService.getDepositId(aipId));
      this.checkAipDepositStatus(aipDeposit);
      // Check if AIP status is completed
      ArchivalInfoPackage aip = this.aipRemoteService.getAIPOnDefaultStorage(aipId);
      if (aip.getInfo().getStatus() != PackageStatus.COMPLETED) {
        throw new SolidifyCheckingException(this.messageService.get("checking.storage.notready", new Object[] { aipId }));
      }
    }

    final BindingResult errors = new BeanPropertyBindingResult(deposit, deposit.getClass().getName());
    this.depositService.checkCompatibilityBetweenCollectionAndArchivesIncluded(deposit, errors);
    if (errors.hasErrors()) {
      throw new SolidifyCheckingException(errors.getAllErrors().get(0).getDefaultMessage());
    }
  }

  private void checkAipDepositStatus(Deposit aipDeposit) {
    if (aipDeposit.getStatus() != DepositStatus.COMPLETED && aipDeposit.getStatus() != DepositStatus.CLEANED) {
      throw new DLCMCollectionDepositStatusException(this.messageService.get("validation.deposit.collection.aipDeposit.status",
              new Object[] { aipDeposit.getResId(), aipDeposit.getStatus() }));
    }
  }

  private boolean hasNeededDua(Deposit deposit, long nbDua, boolean update) {
    final DataUsePolicy dataUsePolicy = deposit.getDataUsePolicy();
    if (DataUsePolicy.LICENSE.equals(dataUsePolicy)
            || DataUsePolicy.EXTERNAL_DUA.equals(dataUsePolicy)
            || DataUsePolicy.NONE.equals(dataUsePolicy)) {
      return true;
    }
    if (update) {
      return nbDua == 1 || nbDua == 0;
    } else {
      return nbDua == 1;
    }
  }

  private boolean hasUnneededDua(Deposit deposit, long nbDua) {
    final DataUsePolicy dataUsePolicy = deposit.getDataUsePolicy();
    if (DataUsePolicy.LICENSE.equals(dataUsePolicy)
            || DataUsePolicy.EXTERNAL_DUA.equals(dataUsePolicy)
            || DataUsePolicy.NONE.equals(dataUsePolicy)) {
      return nbDua == 1;
    }
    return false;
  }

  private void checkDepositDataFiles(Deposit deposit) {
    // Check if there is research data
    final long metadataCount = this.getDepositMetadataCategoryCount(deposit, DataCategory.Package, DataCategory.Metadata);

    if (metadataCount == 0) {
      throw new SolidifyCheckingException(this.messageService.get("checking.metadata.missing"));
    }
    if (metadataCount > 1) {
      throw new SolidifyCheckingException(this.messageService.get("checking.metadata.multiple"));
    }
    // For collection
    if (deposit.isCollection()) {
      this.checkCollectionDeposit(deposit);
    } else {
      // Check mandatory data file category
      this.checkMandatoryDataFiles(deposit);
      // Check if the file number max limit is exceeded
      final int fileNumber = this.depositService.countContentDataFiles(deposit.getResId());
      if (fileNumber > this.maxFileNumber) {
        throw new SolidifyCheckingException(this.messageService.get("checking.data.max", new Object[] { fileNumber, this.maxFileNumber }));
      }
    }
  }

  private void checkMandatoryDataFiles(Deposit deposit) {
    final long researchDataCount = this.getDepositMetadataCategoryCount(deposit, DataCategory.Primary, null);
    final long adminDataCount = this.getDepositMetadataCategoryCount(deposit, DataCategory.Administrative, null);
    if (this.researchDataEnabled && this.administrativeDataEnabled) {
      // Check if there is at least a research data file
      if (researchDataCount == 0 && adminDataCount == 0) {
        throw new SolidifyCheckingException(this.messageService.get("checking.data.missing"));
      }
    } else if (!this.researchDataEnabled && researchDataCount > 0) {
      throw new SolidifyCheckingException(this.messageService.get("checking.data.research.disable"));
    } else if (!this.administrativeDataEnabled && adminDataCount > 0) {
      throw new SolidifyCheckingException(this.messageService.get("checking.data.administrative.disable"));
    } else if (this.researchDataEnabled && researchDataCount == 0) {
      throw new SolidifyCheckingException(this.messageService.get("checking.data.research.missing"));
    } else if (this.administrativeDataEnabled) {
      if (adminDataCount == 0) {
        throw new SolidifyCheckingException(this.messageService.get("checking.data.administrative.missing"));
      } else if (deposit.getMetadataVersion().getVersionNumber() < DLCMMetadataVersion.V3_1.getVersionNumber()) {
        throw new SolidifyCheckingException(
                this.messageService.get("checking.data.administrative.notsupported", new Object[] { deposit.getMetadataVersion() }));
      }
    }
  }

}

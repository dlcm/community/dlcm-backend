/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - TransferStatus.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.preparation.archivematica;

public class TransferStatus {
  private String directory;
  private String message;
  private String microservice;
  private String name;
  private String path;
  private String sipUuid; // If status is COMPLETE
  // Reference
  // https://wiki.archivematica.org/Archivematica_API#Status
  //
  // Fields
  private String status; // One of FAILED, REJECTED, USER_INPUT, COMPLETE or PROCESSING
  private String type; // "transfer"
  private String uuid;

  public String getDirectory() {
    return this.directory;
  }

  public String getMessage() {
    return this.message;
  }

  public String getMicroservice() {
    return this.microservice;
  }

  public String getName() {
    return this.name;
  }

  public String getPath() {
    return this.path;
  }

  public String getSipUuid() {
    return this.sipUuid;
  }

  public String getStatus() {
    return this.status;
  }

  public String getType() {
    return this.type;
  }

  public String getUuid() {
    return this.uuid;
  }

  public void setDirectory(String directory) {
    this.directory = directory;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void setMicroservice(String microservice) {
    this.microservice = microservice;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public void setSipUuid(String sipUuid) {
    this.sipUuid = sipUuid;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

}

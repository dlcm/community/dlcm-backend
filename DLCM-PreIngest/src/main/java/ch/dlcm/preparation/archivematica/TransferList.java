/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - TransferList.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.preparation.archivematica;

import java.util.ArrayList;
import java.util.List;

public class TransferList {
  public static class Transfer {
    // dspace
    private String directory;
    private String type; // One of:standard, unzipped bag, zipped bag,
    private String uuid;

    public String getDirectory() {
      return this.directory;
    }

    public String getType() {
      return this.type;
    }

    public String getUuid() {
      return this.uuid;
    }

    public void setDirectory(String directory) {
      this.directory = directory;
    }

    public void setType(String type) {
      this.type = type;
    }

    public void setUuid(String uuid) {
      this.uuid = uuid;
    }

  }

  // Reference
  // https://wiki.archivematica.org/Archivematica_API#List_Unapproved_Transfers
  //
  // Fields
  private String message;

  private List<Transfer> results;

  public TransferList() {
    this.results = new ArrayList<>();
  }

  public String getMessage() {
    return this.message;
  }

  public List<Transfer> getResults() {
    return this.results;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void setResults(List<Transfer> results) {
    this.results = results;
  }

}

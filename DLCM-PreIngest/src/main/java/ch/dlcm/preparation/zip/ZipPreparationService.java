/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - ZipPreparationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.preparation.zip;

import java.net.URI;
import java.nio.file.Path;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.business.DepositService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.preparation.PreparationService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePrivateMetadataRemoteResourceService;

@Profile("pre-zip")
@Service
@ConditionalOnBean(PreIngestController.class)
public class ZipPreparationService extends PreparationService {

  public ZipPreparationService(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          HistoryService historyService,
          DepositService depositService,
          FallbackSubmissionInfoPackageRemoteResourceService sipRemoteResourceService,
          FallbackArchivalInfoPackageRemoteResourceService aipRemoteResourceService,
          TrustedArchivePrivateMetadataRemoteResourceService archiveMetadataRemoteService) {
    super(dlcmProperties, messageService, historyService, depositService, sipRemoteResourceService, aipRemoteResourceService,
            archiveMetadataRemoteService);
  }

  @Override
  public String generateSIP(Deposit deposit) {
    return this.generatePackage(deposit.getResId(), this.getDepositFolder(deposit.getResId()));
  }

  @Override
  public String generateSIPForUpdate(Deposit deposit) {
    return this.generatePackage(deposit.getResId(), this.getDepositFolderForUpdate(deposit.getResId()));
  }

  @Override
  public DepositStatus getStatus(Deposit deposit) {
    final URI f = this.getSIPUri(deposit.getResId());
    if (FileTool.checkFile(f) && FileTool.isFile(f)) {
      return DepositStatus.COMPLETED;
    }
    return DepositStatus.IN_PROGRESS;
  }

  private String generatePackage(String depositId, Path depositSource) {
    final ZipTool zipTool = new ZipTool(this.getSIPUri(depositId));
    if (!zipTool.zipFiles(depositSource)) {
      throw new SolidifyProcessingException(this.messageService.get("preingest.deposit.error.preparation"));
    }
    return depositId;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - BagItPreparationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.preparation.bagit;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import gov.loc.repository.bagit.hash.StandardSupportedAlgorithms;
import gov.loc.repository.bagit.hash.SupportedAlgorithm;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyBagItCheckingException;
import ch.unige.solidify.exception.SolidifyBagItException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.BagItTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.business.DepositService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.preparation.PreparationService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePrivateMetadataRemoteResourceService;

@Profile("pre-bagit")
@Service
@ConditionalOnBean(PreIngestController.class)
public class BagItPreparationService extends PreparationService {
  private static final Logger log = LoggerFactory.getLogger(BagItPreparationService.class);

  public BagItPreparationService(
          DLCMProperties dlcmProperties,
          MessageService messageService,
          HistoryService historyService,
          DepositService depositService,
          FallbackSubmissionInfoPackageRemoteResourceService sipRemoteResourceService,
          FallbackArchivalInfoPackageRemoteResourceService aipRemoteResourceService,
          TrustedArchivePrivateMetadataRemoteResourceService archiveMetadataRemoteService) {
    super(dlcmProperties, messageService, historyService, depositService, sipRemoteResourceService, aipRemoteResourceService,
            archiveMetadataRemoteService);
  }

  @Override
  public String generateSIP(Deposit deposit) {
    return this.generatePackage(deposit.getResId(), this.getDepositFolder(deposit.getResId()), this.getBagMetadata(deposit));
  }

  @Override
  public String generateSIPForUpdate(Deposit deposit) {
    return this.generatePackage(deposit.getResId(), this.getDepositFolderForUpdate(deposit.getResId()), this.getBagMetadata(deposit));
  }

  @Override
  public DepositStatus getStatus(Deposit deposit) {
    try {
      BagItTool.check(DLCMConstants.BAGIT_PROFILE, this.getDepositFolder(deposit.getResId()).toString());
    } catch (SolidifyBagItException | SolidifyBagItCheckingException e) {
      log.error("Error while retrieving status", e);
      return DepositStatus.IN_ERROR;
    }
    return DepositStatus.COMPLETED;
  }

  private MultiValueMap<String, String> getBagMetadata(Deposit deposit) {
    MultiValueMap<String, String> metadata = new LinkedMultiValueMap<>();
    metadata.add(DLCMConstants.BAGIT_SOURCE, "University of Geneva");
    metadata.add(DLCMConstants.BAGIT_SOURCE_ADDR, "Geneva");
    metadata.add(DLCMConstants.BAGIT_ARC_ID, deposit.getResId());
    metadata.add(DLCMConstants.BAGIT_ARC_NAME, deposit.getTitle());
    return metadata;
  }

  private String generatePackage(String depositId, Path depositSource, MultiValueMap<String, String> depositMetadata) {
    final List<SupportedAlgorithm> algorithms = new ArrayList<>();
    algorithms.add(StandardSupportedAlgorithms.MD5);
    algorithms.add(StandardSupportedAlgorithms.SHA1);
    algorithms.add(StandardSupportedAlgorithms.SHA256);
    try {
      // Create BagIt
      BagItTool.create(depositSource, algorithms, true, depositMetadata);
      // Zip BagIt
      final ZipTool zipTool = new ZipTool(depositSource + SolidifyConstants.ZIP_EXT);
      if (!zipTool.zipFiles(depositSource)) {
        throw new SolidifyProcessingException("Cannot zip BagIt");
      }
    } catch (SolidifyBagItException e) {
      throw new SolidifyProcessingException("Error while generating SIP for deposit " + depositId, e);
    }
    return depositId;
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - PreingestEmailProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import jakarta.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.EmailService;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.DepositService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.message.EmailMessage;
import ch.dlcm.model.preingest.Deposit;

@Service
@ConditionalOnBean(PreIngestController.class)
@Profile("email-service")
public class PreingestEmailProcessingService extends EmailProcessingService {
  private static final Logger log = LoggerFactory.getLogger(PreingestEmailProcessingService.class);

  private final DepositService depositService;

  protected PreingestEmailProcessingService(MessageService messageService, EmailService emailService,
          DLCMProperties dlcmProperties, DepositService depositService) {
    super(messageService, emailService, dlcmProperties);
    this.depositService = depositService;
  }

  @Override
  public void processEmailMessage(EmailMessage emailMessage) {
    log.info("Reading message by Ingestion module {}", emailMessage);
    try {
      switch (emailMessage.getTemplate()) {
        case NEW_DEPOSITS_TO_APPROVE, NEW_DEPOSITS_IN_ERROR -> this.sendEmailWithDepositsAndCreatorList(emailMessage);
        case MY_DEPOSITS_APPROVED, MY_DEPOSITS_COMPLETED, MY_INDIRECT_DEPOSITS_COMPLETED -> this.sendEmailWithDepositsList(emailMessage);
        default -> {
        }
      }
    } catch (MessagingException e) {
      log.error("Cannot send email ({}): {}", emailMessage.getTemplate().getSubject(), e.getMessage(), e);
    }
  }

  private void sendEmailWithDepositsList(EmailMessage emailMessage) throws MessagingException {
    List<Deposit> deposits = new ArrayList<>();
    for (String depositId : emailMessage.getParameters().keySet()) {
      deposits.add(this.depositService.findOne(depositId));
    }

    Map<String, Object> parameters = this.getEmailDefaultParameters();
    parameters.put("deposits", deposits);

    this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(), Collections.singletonList(emailMessage.getTo()),
            emailMessage.getTemplate().getSubject(), parameters);
  }

  private void sendEmailWithDepositsAndCreatorList(EmailMessage emailMessage) throws MessagingException {
    List<Deposit> deposits = new ArrayList<>();
    List<String> creators = new ArrayList<>();
    for (String depositId : emailMessage.getParameters().keySet()) {
      Deposit deposit = this.depositService.findOne(depositId);
      deposits.add(deposit);
      creators.add(deposit.getCreatorName());
    }

    Map<String, Object> parameters = this.getEmailDefaultParameters();
    parameters.put("deposits", deposits);
    parameters.put("creators", creators);

    this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(), Collections.singletonList(emailMessage.getTo()),
            emailMessage.getTemplate().getSubject(), parameters);
  }
}

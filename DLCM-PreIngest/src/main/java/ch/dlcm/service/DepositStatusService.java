/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositStatusService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import jakarta.xml.bind.JAXBException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.message.ResourceCacheMessage;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.DepositDataFileService;
import ch.dlcm.business.DepositService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.message.DataFileMessage;
import ch.dlcm.message.DepositMessage;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.Package;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.preparation.PreparationService;
import ch.dlcm.service.rest.abstractservice.ArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePrivateMetadataRemoteResourceService;

@Service
@ConditionalOnBean(PreIngestController.class)
public class DepositStatusService extends DLCMService {

  private static final String PROCESSING_ERROR = "processing error";
  private final String preingestLocation;

  private final String depositTmpLocation;

  private final DepositDataFileService depositDataFileService;
  private final PreparationService depositPackageBuilder;
  private final DepositService depositService;
  private final IdentifierService identifierService;
  private final MetadataService metadataService;
  private final HistoryService historyService;
  private final ArchivePrivateMetadataRemoteResourceService archiveMetadataRemoteService;

  public DepositStatusService(
          IdentifierService identifierService,
          PreparationService depositPackageBuilder,
          DepositService depositService,
          DLCMProperties dlcmProperties,
          MessageService messageService,
          DepositDataFileService depositDataFileService,
          MetadataService metadataService,
          HistoryService historyService,
          TrustedArchivePrivateMetadataRemoteResourceService archiveMetadataRemoteService) {
    super(messageService, dlcmProperties);
    this.preingestLocation = dlcmProperties.getPreingestLocation();
    this.depositTmpLocation = dlcmProperties.getTempLocation(this.preingestLocation);
    this.identifierService = identifierService;
    this.depositPackageBuilder = depositPackageBuilder;
    this.depositService = depositService;
    this.depositDataFileService = depositDataFileService;
    this.metadataService = metadataService;
    this.historyService = historyService;
    this.archiveMetadataRemoteService = archiveMetadataRemoteService;
  }

  /**
   * Return true if a new DepositMessage must be sent
   * <p>
   * "@Transactional" is only used to create a JPA session, making LAZY loading work on deposit
   *
   * @param depositPayload
   * @return
   */
  @Transactional
  public boolean processDeposit(DepositMessage depositPayload) {

    final Deposit deposit = this.depositService.findOne(depositPayload.getResId());

    this.logDepositMessage(LogLevel.INFO, deposit, "will be processed");

    final DepositStatus currentStatus = deposit.getStatus();
    int initialHashCode = deposit.hashCode();
    try {

      switch (currentStatus) {
        case APPROVED -> this.processApprovedDeposit(deposit);
        case CHECKED -> this.processCheckedDeposit(deposit);
        case SUBMITTED -> this.processSubmittedDeposit(deposit);
        case CLEANING -> this.processCleaningDeposit(deposit);
        case DELETING -> this.processDeletingDeposit(deposit);
        case CHECKING_COMPLIANCE, CHECKING_COMPLIANCE_CLEANED -> this.checkComplianceLevel(deposit, currentStatus);
        case CANCEL_EDITING_METADATA -> this.revertDepositWithIndexData(deposit);
        case UPGRADING_METADATA -> this.processUpgradingMetadataDeposit(deposit);
        default -> this.logDepositMessage(LogLevel.INFO, deposit, "has a status that does not require any processing");
      }
    } catch (final Exception e) {
      this.logDepositMessage(LogLevel.ERROR, deposit, PROCESSING_ERROR, e);
      deposit.setErrorStatusWithMessage(e.getMessage());
    }

    this.logProcessedDeposit(deposit);

    boolean mustSendNewMessage = false;

    try {
      // Test if the deposit has been modified
      if (initialHashCode != deposit.hashCode()) {
        this.depositService.save(deposit);
        this.logDepositMessage(LogLevel.TRACE, deposit, "saved in database");

        /*
         * If deposit status has been modified, the ingest process must go on --> send a new DepositMessage
         */
        if (deposit.getStatus() != currentStatus && deposit.getStatus() != DepositStatus.COMPLETED) {
          this.logDepositMessage(LogLevel.DEBUG, deposit, "a new DepositMessage must be sent to continue deposit processing");
          mustSendNewMessage = true;
        } else if (deposit.getContainsUpdatedMetadata() && deposit.getStatus() == DepositStatus.COMPLETED) {
          this.cleanAipCache(deposit);
        }
      } else {
        this.logDepositMessage(LogLevel.TRACE, deposit, "not saved in database as its hashcode has not been modified");
      }
    } catch (final Exception e) {
      this.logDepositMessage(LogLevel.ERROR, deposit, PROCESSING_ERROR, e);
    }

    this.logDepositMessage(LogLevel.TRACE, deposit, "processDeposit() returns " + mustSendNewMessage);
    return mustSendNewMessage;
  }

  private void cleanAipCache(Deposit deposit) {
    // Clean AIP cache when editing metadata
    String aipId = this.archiveMetadataRemoteService.getAipIdFromDepositId(deposit.getResId());
    SolidifyEventPublisher.getPublisher().publishEvent(new ResourceCacheMessage(ArchivalInfoPackage.class, aipId));
  }

  private void revertDepositWithIndexData(Deposit deposit) {
    this.logDepositMessage(LogLevel.INFO, deposit, "Changes done into the deposit will be reverted");
    final StatusHistory lastStatus = this.historyService.findLastCompletedOrCleaned(deposit.getResId());
    this.metadataService.writeDepositFromIndex(deposit);
    this.logDepositMessage(LogLevel.INFO, deposit, "Changes for the deposit have been reverted");
    if (lastStatus == null) {
      throw new SolidifyCheckingException("Cannot find last 'completed' or 'cleaned' status");
    }
    deposit.setStatus(DepositStatus.valueOf(lastStatus.getStatus()));
  }

  public void checkComplianceLevel(Deposit depo, DepositStatus previousStatus) {
    this.logDepositMessage(LogLevel.INFO, depo, "Deposit datafile status will be updated to CHECK_COMPLIANCE");
    Deposit deposit = this.depositService.findOne(depo.getResId());
    // Trigger the check of compliance level for each data files
    deposit.getDataFiles()
            .stream()
            .filter(e -> e.getStatus() == DataFileStatus.READY
                    || e.getStatus() == DataFileStatus.CLEANED)
            .forEach(depositDataFile -> {
              // Update status
              DataFileStatus statusToSet = previousStatus.equals(DepositStatus.CHECKING_COMPLIANCE) ? DataFileStatus.CHECK_COMPLIANCE
                      : DataFileStatus.CHECK_COMPLIANCE_CLEANED;
              this.depositDataFileService.updateDataFileStatus(depositDataFile, statusToSet);
              // Trigger JMS message
              SolidifyEventPublisher.getPublisher().publishEvent(new DataFileMessage(depositDataFile.getClass(), depositDataFile.getResId()));
            });
    // Depending of previousStatus, the deposit will be set as READY/CLEANED accordingly
    // If one date file is IN_ERROR => set deposit status to ERROR_IN_PROCESSING
    final boolean allDataFilesChecked = this.depositService.hasAllDataFileComplianceLevelCheckedOrInError(deposit.getResId());
    final boolean anyDataFileInError = this.depositService.hasDataFileInError(deposit.getResId());
    if (allDataFilesChecked) {
      this.logDepositMessage(LogLevel.INFO, deposit, "Compliance level checked for all data files");
      if (anyDataFileInError) {
        int countDataFilesInError = this.depositService.countDataFilesInError(deposit.getResId());
        this.logDepositMessage(LogLevel.WARN, deposit, "has datafiles in error");
        deposit.setStatus(DepositStatus.COMPLIANCE_ERROR);
        deposit.setStatusMessage(
                this.messageService.get("message.deposit.checking_compliance.datafiles_in_error", new Object[] { countDataFilesInError }));
      } else if (previousStatus.equals(DepositStatus.CHECKING_COMPLIANCE)) {
        deposit.setStatus(DepositStatus.COMPLETED);
        deposit.setStatusMessage(null);
      } else {
        deposit.setStatus(DepositStatus.CLEANED);
        deposit.setStatusMessage(null);
      }

      // change each data file status accordingly to READY/CLEANED
      deposit.getDataFiles()
              .stream()
              .filter(ddf -> ddf.getStatus().equals(DataFileStatus.CHECKED_COMPLIANCE)
                      || ddf.getStatus().equals(DataFileStatus.CHECK_COMPLIANCE_CLEANED))
              .forEach(ddf -> {
                if (ddf.getStatus().equals(DataFileStatus.CHECKED_COMPLIANCE)) {
                  this.depositDataFileService.updateDataFileStatus(ddf, DataFileStatus.READY);
                } else {
                  this.depositDataFileService.updateDataFileStatus(ddf, DataFileStatus.CLEANED);
                }
              });
    }
  }

  private void processCleaningDeposit(Deposit deposit) {
    final String depositId = deposit.getResId();
    // Purge deposit directory
    this.depositService.deleteDepositFolderFromDisk(depositId);

    // Update all data files status
    this.depositService.setReadyOrCleaningToCleaned(depositId);
    // Check if all data files are CLEANED => set DEPOSIT status to CLEANED
    // If one date file is IN_ERROR => set DEPOSIT status to IN_ERROR
    final boolean allDataFilesCleaned = this.depositService.hasAllDataFileCleaned(depositId);
    final boolean anyDataFileInError = this.depositService.hasDataFileInError(depositId);
    if (allDataFilesCleaned) {
      deposit.setStatus(DepositStatus.CLEANED);
    } else if (anyDataFileInError) {
      deposit.setStatus(DepositStatus.IN_ERROR);
    }
  }

  private void processApprovedDeposit(Deposit deposit) {
    try {

      /*
       * If deposit doesn't have any DOI yet, create it
       */
      if (deposit.getDoi() == null) {
        deposit.setDoi(this.identifierService.createDOI(deposit.getResId()));
        this.logDepositMessage(LogLevel.INFO, deposit, "DOI created (" + deposit.getDoi() + ")");
      }

      /*
       * If deposit doesn't have any ARK yet, create it
       */
      if (deposit.getMetadataVersion().getVersionNumber() >= DLCMMetadataVersion.V3_0.getVersionNumber()
              && deposit.getArk() == null) {
        deposit.setArk(this.identifierService.createARK(deposit.getResId()));
        this.logDepositMessage(LogLevel.INFO, deposit, "ARK created (" + deposit.getArk() + ")");
      }
      /*
       * If deposit has an embargo, set it's start date
       */
      if (deposit.hasEmbargo()) {
        deposit.setEmbargoStartDate(deposit.getCreationTime());
        this.logDepositMessage(LogLevel.INFO, deposit, "Embargo start date set (" + deposit.getEmbargoStartDate() + ")");
      }

      /*
       * Ensure DataCite metadata file exists
       */
      if (!this.depositPackageBuilder.depositHasMetadata(deposit)) {
        this.metadataService.generateDefaultDataCiteFromDeposit(deposit);
        this.logDepositMessage(LogLevel.INFO, deposit, "Default Datacite metadata generated");
      }

      /*
       * If deposit has been already submitted and there is no unprocessed updated metadata file generate
       * updated metadata file from updated deposit
       */
      if (deposit.getPreparationId() != null
              && this.historyService.hasBeenUpdatedSinceLastCompletedStatus(deposit.getResId())
              && this.depositDataFileService.hasProcessedAllUpdatedMetadata(deposit.getResId())) {
        if (deposit.getMetadataVersion().getVersionNumber() >= DLCMMetadataVersion.V4_0.getVersionNumber()) {
          this.generatePremisFileFromUpdatedFiles(deposit);
        }
        this.generateUpdatedMetadataFile(deposit);
      }

      deposit.setStatus(DepositStatus.CHECKED);

    } catch (final Exception e) {
      this.logDepositMessage(LogLevel.ERROR, deposit, PROCESSING_ERROR, e);
      deposit.setErrorStatusWithMessage(e.getMessage());
    }
  }

  private void processCheckedDeposit(Deposit deposit) {

    try {

      if (this.depositService.hasAllDataFileReadyOrCleaned(deposit.getResId())) {

        /*
         * All datafiles are considered as READY --> do some checks before going on with SIP generation
         */
        this.depositPackageBuilder.checkDeposit(deposit);

        if (deposit.getPreparationId() == null) {

          /*
           * Generate Mets metadata
           */
          this.logDepositMessage(LogLevel.INFO, deposit, "Metadata will be generated");
          this.metadataService.generateMetadata(deposit, Paths.get(this.preingestLocation + "/" + deposit.getResId()));
          this.logDepositMessage(LogLevel.INFO, deposit, "Metadata generated");

          /*
           * Generate SIP file
           */
          this.logDepositMessage(LogLevel.INFO, deposit, "SIP file will be generated");
          final String preparationId = this.depositPackageBuilder.generateSIP(deposit);

          if (preparationId != null) {
            this.logDepositMessage(LogLevel.INFO, deposit, "SIP file generated");

            deposit.setPreparationId(preparationId);

            if (this.depositPackageBuilder.getStatus(deposit) == DepositStatus.COMPLETED) {
              deposit.setSip(this.depositPackageBuilder.getSIPUri(deposit.getResId()));
            }

          } else {
            this.logDepositMessage(LogLevel.ERROR, deposit, "SIP file could not be generated");
            deposit.setErrorStatusWithMessage(this.messageService.get("preingest.deposit.error.submission"));
          }
        } else {
          // Check if metadata edition
          if (deposit.getContainsUpdatedMetadata()) {
            this.depositPackageBuilder.generateSIPForUpdate(deposit);
            if (this.depositPackageBuilder.getStatus(deposit) == DepositStatus.COMPLETED) {
              deposit.setSip(this.depositPackageBuilder.getSIPUri(deposit.getResId()));
            }
          }
        }
        deposit.setStatus(DepositStatus.SUBMITTED);

      } else {
        /*
         * Some datafiles are not considered as READY They may just be not ready yet, or they may be
         * IN_ERROR
         */
        if (this.depositService.hasDataFileInError(deposit.getResId())) {
          /*
           * Some datafile are IN_ERROR --> set Deposit itself IN_ERROR
           */
          this.logDepositMessage(LogLevel.ERROR, deposit, "has some datafile(s) in error");
          deposit.setErrorStatusWithMessage(this.messageService.get("preingest.deposit.datafile.error"));
        } else {
          /*
           * No datafile are in error -> do nothing here
           */
          this.logDepositMessage(LogLevel.INFO, deposit, "Not all deposit data files are ready yet");
        }
      }
    } catch (final SolidifyCheckingException e) {
      this.logDepositMessage(LogLevel.ERROR, deposit, PROCESSING_ERROR, e);
      deposit.setErrorStatusWithMessage("[" + this.messageService.get("preingest.deposit.error.technicalvalidation") + "] " + e.getMessage());
    } catch (final SolidifyProcessingException e) {
      this.logDepositMessage(LogLevel.ERROR, deposit, PROCESSING_ERROR, e);
      deposit.setErrorStatusWithMessage("[" + this.messageService.get("preingest.deposit.error.processing") + "] " + e.getMessage());
    } catch (final Exception e) {
      this.logDepositMessage(LogLevel.ERROR, deposit, PROCESSING_ERROR, e);
      deposit.setErrorStatusWithMessage(e.getMessage());
    }
  }

  private void processSubmittedDeposit(Deposit deposit) {

    switch (this.depositPackageBuilder.getStatus(deposit)) {
      case COMPLETED -> {
        /**
         * Send only updated metadata as a SIP datafiles
         */
        if (deposit.getContainsUpdatedMetadata()) {
          try {
            this.depositPackageBuilder.checkDepositForUpdate(deposit);
            this.depositPackageBuilder.sendSIPForUpdate(deposit);
            deposit.setStatus(DepositStatus.COMPLETED);
          } catch (Exception e) {
            deposit.setErrorStatusWithMessage(
                    "[" + this.messageService.get("preingest.deposit.error.sending") + "] "
                            + (e.getCause() != null ? e.getCause().getMessage() : e.getMessage()));
            this.logDepositMessage(LogLevel.ERROR, deposit, "SIP could not be sent to Ingestion");
          }
          break;
        }

        /*
         * Send SIP to ingestion
         */
        this.logDepositMessage(LogLevel.INFO, deposit, "SIP will be sent to Ingestion");
        deposit.setSip(this.depositPackageBuilder.getSIPUri(deposit.getResId()));
        final String sipId = this.depositPackageBuilder.sendSIP(deposit);
        if (sipId != null) {
          deposit.setSipId(sipId);
          deposit.setStatus(DepositStatus.COMPLETED);
          this.logDepositMessage(LogLevel.DEBUG, deposit, "SIP successfully sent to Ingestion");
        } else {
          deposit.setErrorStatusWithMessage(this.messageService.get("preingest.deposit.error.sending"));
          this.logDepositMessage(LogLevel.ERROR, deposit, "SIP could not be sent to Ingestion");
        }

        /*
         * Remove temporary directories of the deposit (only empty ones)
         */
        final String currentDepositTmpLocation = this.depositTmpLocation + File.separatorChar + deposit.getResId();
        FileTool.deleteRecursivelyEmptyDirectories(Path.of(currentDepositTmpLocation));

        /*
         * Delete anonymized deposit
         */
        this.depositService.deleteLastAnonymizedDeposit(deposit);

      }
      case IN_ERROR -> {
        deposit.setErrorStatusWithMessage(this.messageService.get("preingest.deposit.error.preparation"));
        this.logDepositMessage(LogLevel.ERROR, deposit, "An error occurred (statusMessage: " + deposit.getStatusMessage() + ")");
      }
      default -> {
        // Do nothing
      }
    }
  }

  private void processDeletingDeposit(Deposit deposit) {
    this.depositService.delete(deposit.getResId());
    this.depositService.deleteDepositFolderFromDisk(deposit.getResId());
    this.depositService.deleteDepositNotifications(deposit.getResId());
  }

  private void processUpgradingMetadataDeposit(Deposit deposit) {
    deposit.setMetadataVersion(DLCMMetadataVersion.getDefaultVersion());
    deposit.setStatus(DepositStatus.COMPLETED);
  }

  private void generateUpdatedMetadataFile(Deposit deposit) {
    final int randomInt = new SecureRandom().nextInt(Integer.MAX_VALUE);
    final String tmpFileName = DLCMConstants.UPDATED_METADATA_FILE_PREFIX + randomInt;
    final Path tmpFilePath = Paths.get(System.getProperty("java.io.tmpdir"), tmpFileName);
    final File tmpFile = tmpFilePath.toFile();
    try {
      if (!tmpFile.createNewFile()) {
        throw new SolidifyRuntimeException("Unable to create temporary file for storing updated metadata");
      }
      this.metadataService.writeDataciteXmlFromIndexCompletedWithDeposit(deposit.getMetadataVersion(), deposit, new FileOutputStream(tmpFile));
      final DepositDataFile depositDataFile = new DepositDataFile(deposit);
      depositDataFile.setDataCategory(DataCategory.Package);
      depositDataFile.setDataType(DataCategory.UpdatedMetadata);
      depositDataFile.setSourceData(tmpFilePath.toUri());
      deposit.getDataFiles().add(depositDataFile);
      deposit.setContainsUpdatedMetadata(true);
    } catch (IOException | JAXBException e) {
      throw new SolidifyRuntimeException("Exception when generating metadata from updated deposit", e);
    }

  }

  public void generatePremisFileFromUpdatedFiles(Deposit deposit) {
    OffsetDateTime lastTimeCompleted = this.historyService.getLastCompletedUpdate(deposit.getResId());

    List<DepositDataFile> dataFilesUpdatedList = deposit.getDataFiles().stream()
            .filter(df -> df.getDataCategory().equals(DataCategory.Internal) && (df.getDataType().equals(DataCategory.ArchiveThumbnail)
                    || df.getDataType().equals(DataCategory.ArchiveReadme)
                    || df.getDataType().equals(DataCategory.ArchiveDataUseAgreement)))
            .filter(df -> df.getCreationTime().isAfter(lastTimeCompleted))
            .toList();

    if (!dataFilesUpdatedList.isEmpty()) {
      this.generatePremisFileFromUpdatedFiles(deposit, dataFilesUpdatedList);
    }

  }

  private void generatePremisFileFromUpdatedFiles(Deposit deposit, List<DepositDataFile> dataFiles) {

    final String tmpFileName = DLCMConstants.UPDATED_ADMINISTRATIVE_INFO_FILE + "-"
            + OffsetDateTime.now().format(DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT_FOR_FILE)) + SolidifyConstants.XML_EXT;
    final Path tmpFilePath = Paths.get(this.preingestLocation, deposit.getResId(), Package.UPDATE.getName(), tmpFileName);
    final File tmpFile = tmpFilePath.toFile();

    try {
      if (!tmpFile.createNewFile()) {
        throw new SolidifyRuntimeException("Unable to create temporary file for storing premis info");
      }
      this.metadataService.writePremisXmlOfUpdatedFile(deposit.getMetadataVersion(), deposit, dataFiles, tmpFile.getPath());

    } catch (IOException | JAXBException e) {
      throw new SolidifyRuntimeException("Exception when generating metadata from updated deposit", e);
    }
  }

}

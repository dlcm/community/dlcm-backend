/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - ContributorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.StreamSupport;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.NoSqlResourceService;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.preingest.Contributor;
import ch.dlcm.model.settings.Person;
import ch.dlcm.repository.DepositContributorRepository;
import ch.dlcm.service.rest.fallback.FallbackPersonRemoteResourceService;

@Service
@ConditionalOnBean(PreIngestController.class)
public class ContributorService extends NoSqlResourceService<Contributor> {

  private static final String UNKNOWN_LABEL = "Unknown";

  private final DepositContributorRepository contributorRepo;

  private final List<String> exceptionList = Arrays.asList("creation", "lastUpdate", "fullName");

  private final FallbackPersonRemoteResourceService personRemoteResourceService;

  public ContributorService(FallbackPersonRemoteResourceService personRemoteResourceService, DepositContributorRepository contributorRepo) {
    this.personRemoteResourceService = personRemoteResourceService;
    this.contributorRepo = contributorRepo;
  }

  @Override
  public boolean delete(Contributor c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<Contributor> findAll(Contributor search) {
    final RestCollection<Person> result = this.personRemoteResourceService
            .getPersonList(this.convertToQueryString(search), PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE));
    final List<Contributor> list = new ArrayList<>();
    for (final Person person : result.getData()) {
      this.addContributor(list, person, search);
    }
    return list;
  }

  @Override
  public Page<Contributor> findAll(Contributor search, Pageable pageable) {
    RestCollection<Person> result;
    // Filter by deposit number
    if (search.getDepositNumber() != null && search.getDepositNumber() > 0) {
      // Check if no filtering on person field
      if (!StringTool.isNullOrEmpty(search.getFirstName()) || !StringTool.isNullOrEmpty(search.getLastName())
              || !StringTool.isNullOrEmpty(search.getOrcid())) {
        return this.searchByDepositNumberAndPerson(search, pageable);
      } else {
        result = this.searchByDepositNumber(search, pageable);
      }
    } else {
      // Filter by person
      result = this.personRemoteResourceService.getPersonList(this.convertToQueryString(search), pageable);
    }
    final List<Contributor> list = new ArrayList<>();
    for (final Person person : result.getData()) {
      this.addContributor(list, person, search);
    }
    return new PageImpl<>(list, pageable, result.getPage().getTotalItems());
  }

  @Override
  public Contributor findOne(String id) {
    final int depositNumber = this.contributorRepo.findByPerson(id).size();
    return new Contributor(this.personRemoteResourceService.getPerson(id), depositNumber);
  }

  public DepositContributorRepository getContributorRepository() {
    return this.contributorRepo;
  }

  @Override
  public Contributor save(Contributor c) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Contributor update(Contributor c) {
    throw new UnsupportedOperationException();
  }

  private void addContributor(List<Contributor> list, Person person, Contributor search) {
    final int depositNumber = this.contributorRepo.findByPerson(person.getResId()).size();
    if (search.getDepositNumber() == null || depositNumber >= search.getDepositNumber()) {
      list.add(new Contributor(person, depositNumber));
    }
  }

  private String convertToQueryString(Contributor object) {
    return StringTool.convertToQueryString(object, this.exceptionList);
  }

  private String convertPersonToQueryString(Contributor object) {
    return StringTool.convertToQueryString(object.getPerson(), this.exceptionList);
  }

  private RestCollection<Person> searchByDepositNumber(Contributor search, Pageable pageable) {
    final List<Person> personList = new ArrayList<>();
    final Page<String> contributorList = this.contributorRepo.findByDepositNumber(search.getDepositNumber(), pageable);
    for (final String contributor : contributorList) {
      try {
        final Person person = this.personRemoteResourceService.getPerson(contributor);
        personList.add(person);
      } catch (SolidifyResourceNotFoundException e) {
        Person missingPerson = new Person();
        missingPerson.setFirstName(UNKNOWN_LABEL);
        missingPerson.setLastName(UNKNOWN_LABEL);
        personList.add(missingPerson);
      }
    }
    return new RestCollection<>(personList,
            new RestCollectionPage(pageable.getPageNumber(), pageable.getPageSize(), pageable.getOffset(), contributorList.getTotalElements()));
  }

  private Page<Contributor> searchByDepositNumberAndPerson(Contributor search, Pageable pageable) {
    final List<Contributor> contributorList = new ArrayList<>();
    RestCollection<Person> personCollection;
    RestCollection<String> personIdsFromDepositQueryCollection;

    Pageable pageablePersonIntern = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    Pageable pageableDepositIntern = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);

    do {
      personCollection = this.personRemoteResourceService.getPersonList(this.convertPersonToQueryString(search), pageablePersonIntern);
      do {
        final Page<String> personIdDepositNumberFilter = this.contributorRepo.findByDepositNumber(search.getDepositNumber(),
                pageableDepositIntern);
        personIdsFromDepositQueryCollection = new RestCollection<>(personIdDepositNumberFilter, pageableDepositIntern);
        for (final Person person : personCollection.getData()) {
          if (StreamSupport.stream(personIdsFromDepositQueryCollection.getData().spliterator(), false)
                  .anyMatch(idPerson -> idPerson.equals(person.getResId()))) {
            this.addContributor(contributorList, person, search);
          }
        }
        pageableDepositIntern = pageablePersonIntern.next();
      } while (personIdsFromDepositQueryCollection.getPage().hasNext());

      pageablePersonIntern = pageablePersonIntern.next();
    } while (personCollection.getPage().hasNext());

    // apply pagination
    int start = (int) pageable.getOffset();
    int end = (start + pageable.getPageSize()) > contributorList.size() ? contributorList.size() : (start + pageable.getPageSize());
    return new PageImpl<>(start > end ? Collections.emptyList() : contributorList.subList(start, end), pageable, contributorList.size());
  }

}

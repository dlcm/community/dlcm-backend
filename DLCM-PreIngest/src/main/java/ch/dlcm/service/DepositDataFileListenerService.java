/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositDataFileListenerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import static ch.dlcm.model.preingest.Deposit.DepositStatus;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.service.MessageService;

import ch.dlcm.business.DepositDataFileService;
import ch.dlcm.business.DepositService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.message.DataFileMessage;
import ch.dlcm.message.DepositMessage;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
@ConditionalOnBean(PreIngestController.class)
public class DepositDataFileListenerService extends AbstractDataFileListenerService<DepositDataFile> {
  private static final Logger log = LoggerFactory.getLogger(DepositDataFileListenerService.class);

  private final String preingestLocation;

  private final DepositService depositService;

  public DepositDataFileListenerService(DLCMProperties dlcmProperties,
          MessageService messageService,
          DataFileStatusService dataFileStatusService,
          TrustedUserRemoteResourceService userRemoteService,
          TrustedNotificationRemoteResourceService notificationRemoteService,
          TrustedOrganizationalUnitRemoteResourceService organizationalUnitRemoteService,
          DepositService depositService,
          DepositDataFileService depositDataFileService) {
    super(dlcmProperties,
            messageService,
            dataFileStatusService,
            userRemoteService,
            notificationRemoteService,
            organizationalUnitRemoteService,
            depositDataFileService);
    this.preingestLocation = dlcmProperties.getPreingestLocation();
    this.depositService = depositService;
  }

  @JmsListener(destination = "${dlcm.queue.deposit-datafile}")
  @Override
  public void receiveDataFileMessage(DataFileMessage dataFileMessage) {
    super.receiveDataFileMessage(dataFileMessage);
  }

  @Override
  protected void processDataFile(DataFileMessage dataFileMessage) {
    log.info("Reading Deposit Data File message {}", dataFileMessage);
    try {
      final DepositDataFile depositDf = this.processDataFile(this.preingestLocation, dataFileMessage.getResId());
      /*
       * When a DepositDataFile processing is over (meaning it is either READY or IN_ERROR), notify its
       * Deposit in order to allow it to continue the ingestion process
       */
      Deposit deposit = this.depositService.findOne(depositDf.getInfoPackage().getResId());
      if (deposit.getStatus() == DepositStatus.CHECKED) {
        this.sendDepositMessage(deposit);
      }
      //After checking the compliance level, it is needed to notify to the deposit parent
      if (depositDf.getStatus() == DataFileStatus.CHECKED_COMPLIANCE ||
              depositDf.getStatus() == DataFileStatus.CHECKED_COMPLIANCE_CLEANED ||
              depositDf.getStatus() == DataFileStatus.IN_ERROR) {
        boolean isBigPackage = this.depositService.getSize(deposit.getResId()) > this.fileSizeLimit;
        SolidifyEventPublisher.getPublisher().publishEvent(new DepositMessage(deposit.getResId(), isBigPackage));
      }
    } catch (final NoSuchElementException e) {
      log.error(e.getMessage());
    }
  }

  /**
   * If the Deposit has been validated and its processing is not over (meaning its status is CHECKED), send a JMS message to the deposit queue to
   * allow the ingestion process to continue
   *
   * @param deposit The deposit
   */
  private void sendDepositMessage(Deposit deposit) {
    boolean bigPackage = this.depositService.getSize(deposit.getResId()) > this.fileSizeLimit;
    SolidifyEventPublisher.getPublisher().publishEvent(new DepositMessage(deposit.getResId(), bigPackage));
  }
}

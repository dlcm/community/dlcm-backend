/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositMgmt.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;

import ch.dlcm.config.DLCMProperties;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.message.DepositMessage;

@Service
@ConditionalOnBean(PreIngestController.class)
public class DepositMgmt extends MessageProcessorBySize<DepositMessage> {
  private static final Logger log = LoggerFactory.getLogger(DepositMgmt.class);

  final DepositStatusService depositStatusService;

  public DepositMgmt(DepositStatusService depositStatusService, DLCMProperties dlcmProperties) {
    super(dlcmProperties);
    this.depositStatusService = depositStatusService;
  }

  @JmsListener(destination = "${dlcm.queue.deposit}")
  @Override
  public void receiveMessage(DepositMessage depositMessage) {
    this.sendForParallelProcessing(depositMessage);
  }

  @Override
  public void processMessage(DepositMessage depositPayload) {
    this.setSecurityContext();
    log.info("Reading message {}", depositPayload);

    final boolean mustSendNewMessage = this.depositStatusService.processDeposit(depositPayload);

    /*
     * Sending a new message is done outside processDeposit() to ensure Transaction is commited when the
     * message is sent
     */
    if (mustSendNewMessage) {

      log.info("----> DepositMgmt will send a DepositMessage for deposit '{}'", depositPayload.getResId());

      SolidifyEventPublisher.getPublisher().publishEvent(new DepositMessage(depositPayload.getResId(), depositPayload.isBigPackage()));
    }
  }
}

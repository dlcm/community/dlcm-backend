/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.service.security;

import java.text.ParseException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import com.nimbusds.jwt.SignedJWT;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.util.StringTool;

import ch.dlcm.business.DepositService;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.OrganizationalUnitAwareResource;
import ch.dlcm.model.dto.AuthorizedOrganizationalUnitDto;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.security.Role;
import ch.dlcm.service.rest.propagate.PropagateOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedPersonRemoteResourceService;

@Service
@ConditionalOnBean(PreIngestController.class)
public class DepositPermissionService extends AbstractPermissionWithOrgUnitService {
  private static final Logger log = LoggerFactory.getLogger(DepositPermissionService.class);

  private final DepositService depositService;
  private final HttpRequestInfoProvider httpRequestInfoProvider;
  private final PropagateOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService;

  public DepositPermissionService(DepositService depositService,
          TrustedPersonRemoteResourceService personRemoteResourceService,
          PropagateOrganizationalUnitRemoteResourceService organizationalUnitRemoteResourceService,
          HttpRequestInfoProvider httpRequestInfoProvider) {
    super(personRemoteResourceService);
    this.depositService = depositService;
    this.organizationalUnitRemoteResourceService = organizationalUnitRemoteResourceService;
    this.httpRequestInfoProvider = httpRequestInfoProvider;
  }

  public boolean isAllowed(Deposit deposit, String search) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }

    DLCMControllerAction action = this.getControllerAction(search);
    final String personId = this.getPersonId();
    return this.isAllowedToPerformActionOnResource(personId, deposit, action);
  }

  public boolean isAllowedToListWithoutOrgUnit(Deposit deposit, String search) {
    DLCMControllerAction action = this.getControllerAction(search);
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    final String organizationalUnitId = deposit.getOrganizationalUnitId();
    if (!StringTool.isNullOrEmpty(organizationalUnitId) && action == DLCMControllerAction.LIST) {
      Optional<AuthorizedOrganizationalUnitDto> authorizedOrganizationalUnit = this.organizationalUnitRemoteResourceService
              .getAuthorizedOrganizationalUnit(organizationalUnitId);
      if (authorizedOrganizationalUnit.isEmpty()) {
        log.error("Unable to check permissions: you don't have any organizational unit associated",
                new SolidifyRuntimeException("You don't have any organizational unit associated"));
        return false;
      } else {
        // check that the person has minimum role visitor in the organizational unit or inherited from the institution
        AuthorizedOrganizationalUnitDto organizationalUnitDto = authorizedOrganizationalUnit.get();
        return (organizationalUnitDto.getRoleFromOrganizationalUnit() != null
                && organizationalUnitDto.getRoleFromOrganizationalUnit().getLevel() <= Role.VISITOR.getLevel()) ||
                (organizationalUnitDto.getRoleFromInstitution() != null
                        && organizationalUnitDto.getRoleFromInstitution().getLevel() <= Role.VISITOR.getLevel());
      }
    }
    return false;
  }

  @Override
  protected boolean isManagerAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    final DepositStatus status = this.getCurrentStatus(existingResource);
    log.info("Action {} required on deposit '{}' with status {}", action.name(), existingResource.getResId(), status);
    if (status == DepositStatus.IN_PROGRESS) {
      return true;
    } else if (status == DepositStatus.IN_VALIDATION) {
      if (action == DLCMControllerAction.APPROVE || action == DLCMControllerAction.REJECT) {
        return !this.isDepositOfCurrentUser((Deposit) existingResource);
      }
      return true;
    } else if (status == DepositStatus.IN_ERROR || status == DepositStatus.REJECTED) {
      return action == DLCMControllerAction.ENABLE_REVISION;
    } else if (status == DepositStatus.COMPLETED || status == DepositStatus.CLEANED || status == DepositStatus.EDITING_METADATA_REJECTED) {
      return action == DLCMControllerAction.START_METADATA_EDITING;
    } else if (status == DepositStatus.EDITING_METADATA) {
      return action == DLCMControllerAction.CANCEL_METADATA_EDITING || action == DLCMControllerAction.UPDATE
              || action == DLCMControllerAction.SUBMIT_FOR_APPROVAL || action == DLCMControllerAction.APPROVE
              || action == DLCMControllerAction.UPLOAD_FILE;
    }

    return false;
  }

  @Override
  protected boolean isStewardAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {
    final DepositStatus status = this.getCurrentStatus(existingResource);

    if (status == DepositStatus.COMPLETED || status == DepositStatus.CLEANED || status == DepositStatus.EDITING_METADATA_REJECTED) {
      return action == DLCMControllerAction.START_METADATA_EDITING;
    } else if (status == DepositStatus.EDITING_METADATA) {
      return action == DLCMControllerAction.CANCEL_METADATA_EDITING || action == DLCMControllerAction.UPDATE
              || action == DLCMControllerAction.SUBMIT_FOR_APPROVAL || action == DLCMControllerAction.APPROVE
              || action == DLCMControllerAction.UPLOAD_FILE;
    }
    return this.isApproverAllowed(existingResource, action);
  }

  @Override
  protected boolean isApproverAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    final DepositStatus status = this.getCurrentStatus(existingResource);

    if (status == DepositStatus.IN_PROGRESS) {
      return this.isActionAllowedWhenInProgress(action);
    } else if (status == DepositStatus.IN_VALIDATION) {
      if (action == DLCMControllerAction.APPROVE || action == DLCMControllerAction.REJECT) {
        return !this.isDepositOfCurrentUser((Deposit) existingResource);
      }
      return action == DLCMControllerAction.UPLOAD_FILE ||
              action == DLCMControllerAction.RESUME_FILE ||
              action == DLCMControllerAction.VALIDATE_FILE ||
              action == DLCMControllerAction.DELETE_FOLDER ||
              action == DLCMControllerAction.DELETE_FILE ||
              action == DLCMControllerAction.RESERVE_DOI ||
              action == DLCMControllerAction.DELETE ||
              action == DLCMControllerAction.ENABLE_REVISION ||
              action == DLCMControllerAction.REJECT;
    } else if (status == DepositStatus.IN_ERROR || status == DepositStatus.REJECTED) {
      return action == DLCMControllerAction.ENABLE_REVISION;
    } else if (status == DepositStatus.CLEANED || status == DepositStatus.COMPLETED) {
      return action == DLCMControllerAction.CHECK_COMPLIANCE;
    } else if (status == DepositStatus.EDITING_METADATA) {
      return action == DLCMControllerAction.APPROVE
              || action == DLCMControllerAction.SUBMIT_FOR_APPROVAL;
    }

    return false;
  }

  @Override
  protected boolean isCreatorAllowed(OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    final DepositStatus status = this.getCurrentStatus(existingResource);

    if (status == DepositStatus.IN_PROGRESS) {
      return this.isActionAllowedWhenInProgress(action);
    } else if (status == DepositStatus.IN_VALIDATION) {
      return action == DLCMControllerAction.ENABLE_REVISION;
    }

    return false;
  }

  private DepositStatus getCurrentStatus(OrganizationalUnitAwareResource existingResource) {
    final DepositStatus depositStatus = ((Deposit) existingResource).getStatus();
    if (depositStatus == null) {
      return ((Deposit) existingResource).getDefaultStatus();
    } else {
      return depositStatus;
    }
  }

  private boolean isActionAllowedWhenInProgress(DLCMControllerAction action) {
    return action == DLCMControllerAction.CREATE ||
            action == DLCMControllerAction.UPDATE ||
            action == DLCMControllerAction.UPLOAD_FILE ||
            action == DLCMControllerAction.RESUME_FILE ||
            action == DLCMControllerAction.VALIDATE_FILE ||
            action == DLCMControllerAction.DELETE_FILE ||
            action == DLCMControllerAction.DELETE_FOLDER ||
            action == DLCMControllerAction.UPDATE_FILE ||
            action == DLCMControllerAction.RESERVE_DOI ||
            action == DLCMControllerAction.SUBMIT_FOR_APPROVAL ||
            action == DLCMControllerAction.APPROVE ||
            action == DLCMControllerAction.DELETE;
  }

  @Override
  protected Deposit getExistingResource(String resId) {
    return this.depositService.findOne(resId);
  }

  @Override
  boolean isRoleAllowed(Role role, OrganizationalUnitAwareResource existingResource, DLCMControllerAction action) {

    /*
     * All roles except visitor can perform these actions
     */
    if (!role.getResId().equals(Role.VISITOR_ID) &&
            (action == DLCMControllerAction.GET ||
                    action == DLCMControllerAction.GET_FILE ||
                    action == DLCMControllerAction.LIST ||
                    action == DLCMControllerAction.LIST_FILES ||
                    action == DLCMControllerAction.DOWNLOAD_FILE ||
                    action == DLCMControllerAction.HISTORY)) {
      return true;
    }
    return this.checkActionByRole(role, existingResource, action);
  }

  private boolean isDepositOfCurrentUser(Deposit deposit) {
    try {
      final String token = this.httpRequestInfoProvider.getIncomingToken();
      final String externalUid = SignedJWT.parse(token).getJWTClaimsSet().getClaim("user_name").toString();
      return deposit.getCreatedBy().equals(externalUid);
    } catch (ParseException ex) {
      return false;
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositDataFileService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.dto.DepositDataFileStatisticsDto;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.repository.DepositDataFileRepository;
import ch.dlcm.service.DataCategoryService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.specification.DepositDataFileSpecification;

@Service
@ConditionalOnBean(PreIngestController.class)
public class DepositDataFileService extends DataFileService<DepositDataFile> {

  private final HistoryService historyService;

  public DepositDataFileService(
          HistoryService historyService,
          DataCategoryService dataCategoryService) {
    super(dataCategoryService);
    this.historyService = historyService;
  }

  public List<String> getRelativeLocationList(String depositId) {
    final DepositDataFileRepository repo = (DepositDataFileRepository) this.itemRepository;
    final List<String> results = repo.findDistinctRelativeLocationForDeposit(depositId);
    return new ArrayList<>(results);
  }

  public Map<DataFileStatus, Long> getMapStatusOccurences(String depositId) {
    List<DepositDataFileStatisticsDto> listStatistics = ((DepositDataFileRepository) this.itemRepository).getStatusStatistics(depositId);
    return DepositDataFileStatisticsDto.getMapFromList(listStatistics);
  }

  public List<String> findDataFileIdByRelativeLocationFolder(String depositId, String relativeLocationFolder) {
    final DepositDataFileRepository repo = (DepositDataFileRepository) this.itemRepository;
    String relativeLocationSubFolder = relativeLocationFolder;
    if (!relativeLocationFolder.endsWith("/")) {
      relativeLocationSubFolder = relativeLocationSubFolder + "/";
    }
    relativeLocationSubFolder = relativeLocationSubFolder + "%";
    return repo.findDataFileIdByRelativeLocationFolder(depositId, relativeLocationFolder, relativeLocationSubFolder);
  }

  public void updateDataFileStatus(DepositDataFile dataFile, DataFileStatus status) {
    dataFile.setFileStatus(status);
    this.save(dataFile);
  }

  public void deleteLastUpdatedMetadaFile(String depositId) {
    final List<DepositDataFile> updatedMetadataFileList = ((DepositDataFileRepository) this.itemRepository)
            .findUpdatedMetadataFileOrderByDate(depositId);
    if (!updatedMetadataFileList.isEmpty()) {
      this.delete(updatedMetadataFileList.get(0).getResId());
    }
  }

  public void deleteUpdatedFiles(String depositId) {
    final OffsetDateTime lastCompletedOrCleanedTime = this.historyService.findLastCompletedOrCleanedTime(depositId);
    final List<DepositDataFile> updatedFilesList = ((DepositDataFileRepository) this.itemRepository)
            .findUpdatedFileByDataTypeOrderByDate(depositId);
    for (DepositDataFile depositDataFile: updatedFilesList) {
      if (depositDataFile.getCreationTime().isAfter(lastCompletedOrCleanedTime)) {
        this.delete(depositDataFile.getResId());
      }
    }
  }

  public boolean hasProcessedAllUpdatedMetadata(String depositId) {
    final List<DepositDataFile> updatedMetadataFileList = ((DepositDataFileRepository) this.itemRepository)
            .findUpdatedMetadataFileOrderByDate(depositId);
    if (updatedMetadataFileList.isEmpty()) {
      return true;
    }
    final OffsetDateTime lastCompletedOrCleanedTime = this.historyService.findLastCompletedOrCleanedTime(depositId);
    if (lastCompletedOrCleanedTime == null) {
      throw new IllegalStateException("There should not be updated metadata file if the deposit is nor CLEANED nor COMPLETED");
    } else {
      final OffsetDateTime lastMetadataUpdated = updatedMetadataFileList.get(0).getUpdateTime();
      return lastCompletedOrCleanedTime.isAfter(lastMetadataUpdated);
    }
  }

  @Override
  protected void validateItemSpecificRules(DepositDataFile depositDataFile, BindingResult errors) {
    super.validateItemSpecificRules(depositDataFile, errors);

    // Check fileName uniqueness
    if (!this.isDataFileUniqueInPackageAndRelativeLocation(depositDataFile)) {
      errors.addError(new FieldError(depositDataFile.getClass().getSimpleName(), "fileName",
              this.messageService.get("validation.dataFile.fileNameUniqueness",
                      new Object[] { depositDataFile.getFileName(), depositDataFile.getRelativeLocation() })));
    }
  }

  @Override
  public DepositDataFileSpecification getSpecification(DepositDataFile resource) {
    return new DepositDataFileSpecification(resource);
  }
}

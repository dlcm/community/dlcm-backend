/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.business;

import static ch.dlcm.DLCMConstants.ANONYMIZED_DEPOSITS_FOLDER;
import static ch.dlcm.DLCMRestFields.STATUS_FIELD;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.CANCEL_EDITING_METADATA;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.CLEANED;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.CLEANING;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.COMPLETED;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.DELETING;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.EDITING_METADATA;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.IN_PROGRESS;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.IN_VALIDATION;
import static ch.dlcm.model.preingest.Deposit.DepositStatus.UPGRADING_METADATA;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.unit.DataSize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.ModelAndView;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.model.Language;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.service.CompositeResourceService;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.ValidationTool;
import ch.unige.solidify.util.ZipTool;
import ch.unige.solidify.validation.ValidationError;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.exception.DLCMCollectionDepositStatusException;
import ch.dlcm.message.DepositMessage;
import ch.dlcm.model.AbstractDataFile;
import ch.dlcm.model.Access;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.DataUsePolicy;
import ch.dlcm.model.PackageStatus;
import ch.dlcm.model.StatusHistory;
import ch.dlcm.model.dto.DepositDto;
import ch.dlcm.model.notification.NotificationType;
import ch.dlcm.model.oais.ArchivalInfoPackage;
import ch.dlcm.model.oais.SubmissionInfoPackage;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.preingest.AnonymizedDeposit;
import ch.dlcm.model.preingest.AnonymizedDepositEntry;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.security.User;
import ch.dlcm.model.settings.ArchiveType;
import ch.dlcm.model.settings.License;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.model.settings.SubjectArea;
import ch.dlcm.model.settings.SubmissionAgreement;
import ch.dlcm.model.xml.dlcm.v4.mets.RelationType;
import ch.dlcm.preparation.PreparationService;
import ch.dlcm.repository.AnonymizedDepositRepository;
import ch.dlcm.repository.DepositDataFileRepository;
import ch.dlcm.repository.DepositRepository;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.rest.fallback.FallbackAdditionalFieldsFormRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchiveTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLanguageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLicenseRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPersonRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPreservationPolicyRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubjectAreaRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionAgreementRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionPolicyRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackUserRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedNotificationRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedSubmissionAgreementUserRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedUserRemoteResourceService;
import ch.dlcm.specification.DepositSpecification;
import ch.dlcm.specification.dto.DepositDtoSpecification;

@Service
@ConditionalOnBean(PreIngestController.class)
public class DepositService extends CompositeResourceService<Deposit> {

  private static final Logger log = LoggerFactory.getLogger(DepositService.class);

  private static final String AIP_BAD_ORGANIZATIONAL_UNIT = "validation.deposit.collection.aip.badOrganizationalUnit";
  private static final String VALIDATION_NOTIFICATION_DEPOSIT = "validation.notification.deposit";
  private static final String NOTIFICATION_DEPOSIT_APPROVED = "preingest.notification.deposit.approved";
  private static final String NOTIFICATION_DEPOSIT_CREATED = "preingest.notification.deposit.created";
  private static final String NOTIFICATION_DEPOSIT_ERROR = "preingest.notification.deposit.error";
  private static final String BAD_EMBARGO = "validation.access.badEmbargoAccess";
  private static final String INVALID_EMBARGO = "validation.embargo.invalid";
  private static final String COLLECTION_BEGIN_NOT_BEFORE_COLLECTION_END = "validation.dates.collectionBeginMustBeBeforeCollectionEnd";
  private static final String DATA_SENSITIVITY = "validation.deposit.dataSensitivity";
  private static final String CONTENT_STRUCTURE_PUBLIC = "validation.deposit.isContentStructurePublic";
  private static final String ARCHIVE_TYPE_INCONSISTANT = "validation.deposit.archiveType.inconsistant";
  private static final String ARCHIVE_TYPE_WRONG_CONFIG = "validation.deposit.archiveType.wrongConfig";
  private static final String DATA_FILES_NOT_READY_OR_NOT_CLEANED = "validation.deposit.datafile.notReadyOrNotCleaned";
  private static final String HAS_DATA_FILES_BEFORE_VALIDATION = "validation.deposit.status.containsFilesBeforeInValidation";
  private static final String NOT_EXIST = "validation.resource.notexist";
  private static final String LICENSE_MISSING = "validation.deposit.license.missing";
  private static final String UNIT_CLOSED = "validation.organizationalUnit.closed";
  private static final String INVALID_POLICY = "validation.organizationalUnit.invalid-policy";
  private static final String INVALID_ADDITIONAL_FIELDS_VALUES = "validation.deposit.additionalFieldsValues.invalidJson";
  private static final String ADDITIONAL_FIELDS_FORM_REQUIRED_WHEN_ADDITIONAL_FIELDS_VALUES_PRESENT = "validation.deposit.additionalFieldsValues.formIdMissing";
  private static final String NOT_SUPPORTED = "validation.deposit.notSupported";
  private static final String DATA_USE_POLICY_MISSING = "validation.dataUsePolicy.missing";
  private static final String DATA_USE_POLICY_SHOULD_BE_LICENCE = "validation.dataUsePolicy.license";
  private static final String DATA_USE_POLICY_SHOULD_BE_NONE_OR_LICENCE = "validation.dataUsePolicy.noneOrLicense";
  private static final String DATA_USE_POLICY_SHOULD_BE_CLICK_THROUGH = "validation.dataUsePolicy.clickThrough";
  private static final String DATA_USE_POLICY_SHOULD_BE_SIGNED_OR_EXTERNAL = "validation.dataUsePolicy.signedOrExternal";
  private static final String ACCESS_LEVEL_COLLECTION_SHOULD_BE_GREATER = "validation.access.collection";
  private static final String DATATAG_COLLECTION_SHOULD_BE_GREATER = "validation.dataTag.collection";
  private static final String DATA_USE_POLICY_COLLECTION_SHOULD_BE_GREATER = "validation.dataUsePolicy.collection";
  private static final String EMBARGO_ACCESS_LEVEL_COLLECTION_SHOULD_BE_GREATER = "validation.embargo.access.collection";
  private static final String EMBARGO_END_DATE_COLLECTION_SHOULD_BE_GREATER = "validation.embargo.endDate.collection";
  private static final String COLLECTION_SHOULD_CONTAIN_EMBARGO = "validation.embargo.shouldExitsInCollection";
  private static final String LICENSE_ID_FIELD = "licenseId";
  private static final String DATA_USE_POLICY_FIELD = "dataUsePolicy";
  private static final String DATA_SENSITIVITY_FIELD = "dataSensitivity";
  private static final String ACCESS_LEVEL_FIELD = "accessLevel";
  private static final String CONTENT_STRUCTURE_PUBLIC_FIELD = "isContentStructurePublic";
  private static final String EMBARGO_ACCESS_LEVEL_FIELD = "embargoAccess";
  private static final String EMBARGO_LEVEL_FIELD = "embargo";
  private static final String EMBARGO_END_DATE_FIELD = "embargoMonths";
  private final DepositDataFileRepository depositDataFileRepository;
  private final SpecificationPermissionsFilter<Deposit> specificationPermissionFilter;
  private final HttpRequestInfoProvider httpRequestInfoProvider;
  private final FallbackArchivalInfoPackageRemoteResourceService aipService;
  private final FallbackLanguageRemoteResourceService languageService;
  private final FallbackLicenseRemoteResourceService licenseService;
  private final FallbackPersonRemoteResourceService personService;
  private final FallbackSubjectAreaRemoteResourceService subjectAreaService;
  private final FallbackPreservationPolicyRemoteResourceService preservationPolicyService;
  private final FallbackSubmissionAgreementRemoteResourceService submissionAgreementService;
  private final FallbackSubmissionPolicyRemoteResourceService submissionPolicyService;
  private final FallbackUserRemoteResourceService userService;
  private final FallbackOrganizationalUnitRemoteResourceService orgUnitService;
  private final FallbackAdditionalFieldsFormRemoteResourceService additionalFieldsFormService;
  private final FallbackArchiveTypeRemoteResourceService archiveTypeService;
  private final TrustedNotificationRemoteResourceService trustedNotificationRemoteResourceService;
  private final TrustedArchivePrivateMetadataRemoteResourceService trustedArchiveMetadataRemoteService;
  private final TrustedUserRemoteResourceService trustedUserRemoteResourceService;
  private final TrustedSubmissionAgreementUserRemoteResourceService trustedSubmissionAgreementUserRemoteResourceService;
  private final HistoryService historyService;
  private final PreparationService preparationService;
  private final ChecksumAlgorithm defaultChecksumAlgorithm;
  private final AnonymizedDepositRepository anonymizedDepositRepository;
  private final TrustedSubmissionInfoPackageRemoteResourceService trustedSubmissionInfoPackageRemoteResourceService;

  private final String preingestLocation;
  private final String preingestTempLocation;
  private final String preingestPublicUrl;
  private final String archiveHomePage;

  private final DataSize fileSizeLimit;
  private final String defaultLicense;
  private final String defaultMasterArchiveType;

  public DepositService(DLCMProperties dlcmProperties,
          DepositDataFileRepository depositDataFileRepository,
          SpecificationPermissionsFilter<Deposit> specificationPermissionFilter,
          HttpRequestInfoProvider httpRequestInfoProvider,
          FallbackLanguageRemoteResourceService languageService,
          FallbackArchivalInfoPackageRemoteResourceService aipService,
          FallbackLicenseRemoteResourceService licenseService,
          FallbackOrganizationalUnitRemoteResourceService organizationalUnitService,
          FallbackPersonRemoteResourceService personService,
          FallbackPreservationPolicyRemoteResourceService preservationPolicyService,
          FallbackUserRemoteResourceService userService,
          FallbackSubmissionAgreementRemoteResourceService submissionAgreementService,
          FallbackSubmissionPolicyRemoteResourceService submissionPolicyService,
          FallbackAdditionalFieldsFormRemoteResourceService additionalFieldsFormService,
          FallbackArchiveTypeRemoteResourceService archiveTypeService,
          TrustedNotificationRemoteResourceService trustedNotificationRemoteResourceService,
          TrustedArchivePrivateMetadataRemoteResourceService trustedArchiveMetadataRemoteService,
          TrustedUserRemoteResourceService trustedUserRemoteResourceService,
          TrustedSubmissionAgreementUserRemoteResourceService trustedSubmissionAgreementUserRemoteResourceService,
          FallbackSubjectAreaRemoteResourceService subjectAreaService,
          HistoryService historyService,
          @Lazy PreparationService preparationService,
          AnonymizedDepositRepository anonymizedDepositRepository,
          TrustedSubmissionInfoPackageRemoteResourceService trustedSubmissionInfoPackageRemoteResourceService,
          DLCMRepositoryDescription dlcmRepository) {
    this.depositDataFileRepository = depositDataFileRepository;
    this.specificationPermissionFilter = specificationPermissionFilter;
    this.httpRequestInfoProvider = httpRequestInfoProvider;
    this.languageService = languageService;
    this.aipService = aipService;
    this.licenseService = licenseService;
    this.orgUnitService = organizationalUnitService;
    this.personService = personService;
    this.preservationPolicyService = preservationPolicyService;
    this.userService = userService;
    this.submissionAgreementService = submissionAgreementService;
    this.submissionPolicyService = submissionPolicyService;
    this.additionalFieldsFormService = additionalFieldsFormService;
    this.archiveTypeService = archiveTypeService;
    this.trustedNotificationRemoteResourceService = trustedNotificationRemoteResourceService;
    this.trustedArchiveMetadataRemoteService = trustedArchiveMetadataRemoteService;
    this.trustedUserRemoteResourceService = trustedUserRemoteResourceService;
    this.trustedSubmissionAgreementUserRemoteResourceService = trustedSubmissionAgreementUserRemoteResourceService;
    this.historyService = historyService;
    this.subjectAreaService = subjectAreaService;
    this.preparationService = preparationService;
    this.anonymizedDepositRepository = anonymizedDepositRepository;
    this.trustedSubmissionInfoPackageRemoteResourceService = trustedSubmissionInfoPackageRemoteResourceService;

    this.defaultChecksumAlgorithm = ChecksumAlgorithm.valueOf(dlcmProperties.getParameters().getDefaultChecksum());
    this.preingestLocation = dlcmProperties.getPreingestLocation();
    this.preingestTempLocation = dlcmProperties.getTempLocation(this.preingestLocation);
    this.preingestPublicUrl = dlcmProperties.getModule().getPreingest().getPublicUrl();
    this.fileSizeLimit = dlcmProperties.getParameters().getFileSizeLimit();
    this.defaultLicense = dlcmProperties.getParameters().getDefaultLicense();
    this.defaultMasterArchiveType = dlcmProperties.getParameters().getDefaultMasterArchiveType();
    this.archiveHomePage = dlcmRepository.getArchiveHomePage();
  }

  /**
   * Delete Deposit from database
   * <p>
   * The use of transaction with propagation to Propagation.REQUIRES_NEW ensure the associated
   * DepositDataFile are deleted as well when the call to super.delete() is over
   * <p>
   * Note: As associated DepositDatafiles are deleted from database and disk as well, this process may
   * be time-consuming. Therefore deleting a Deposit should better be done by setting its status to
   * DELETING, which then trigger the call to the delete() method asynchronously.
   *
   * @param depositId
   */
  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void delete(String depositId) {
    super.delete(depositId);
  }

  public void checkArchiveIsEditable(String depositId, Integer preferredStorage) {
    final String cannotEditMessage = this.messageService.get("preingest.deposit.error.cannot_edit_deposit", new Object[] { depositId });
    try {
      final String aipId = this.trustedArchiveMetadataRemoteService.getAipIdFromDepositId(depositId);
      final ArchivalInfoPackage aip = this.aipService.findOne(aipId, preferredStorage);
      if (aip.getInfo().getStatus() != PackageStatus.COMPLETED) {
        throw new SolidifyUnmodifiableException(cannotEditMessage);
      }
    } catch (SolidifyResourceNotFoundException e) {
      throw new SolidifyUnmodifiableException(cannotEditMessage);
    }
  }

  public void putInMetadataEditingCollections(String depositId) {
    try {
      String aipId = this.trustedArchiveMetadataRemoteService.getAipIdFromDepositId(depositId);
      List<String> collections = this.trustedArchiveMetadataRemoteService.getCollections(aipId, true);
      if (!collections.isEmpty()) {
        for (String collectionId : collections) {
          Deposit collectionDeposit = this.findOne(this.trustedArchiveMetadataRemoteService.getDepositId(collectionId));
          if (this.statusUpdateIsValid(collectionDeposit, DepositStatus.EDITING_METADATA)) {
            collectionDeposit.setStatus(DepositStatus.EDITING_METADATA);
            this.save(collectionDeposit);
            log.info("Editing metadata for collection deposit: resId={} status={}", collectionDeposit.getResId(), collectionDeposit.getStatus());
          }
        }
      }
    } catch (Exception e) {
      throw new SolidifyRuntimeException(this.messageService.get("preingest.deposit.error.cannot_edit_collection", new Object[] { depositId }),
              e);
    }
  }

  /**
   * Check that cancelling the update of the collection doesn't revert the collection to an invalid state. A collection can be invalid if:
   * - its access level is not coherent with its archives' access levels
   * - collection datatag is not coherent with its archives' datatags
   * - embargoes on collection's archives are not compatible with collection's access level / embargo
   *
   * @param depositId
   * @return
   */
  public boolean checkIfCancelApplicableForCollection(String depositId) {
    try {
      Deposit deposit = this.findOne(depositId);

      if (deposit.isCollection()) {
        this.preparationService.checkCollectionDeposit(deposit);
      }
      return true;

    } catch (DLCMCollectionDepositStatusException e) {
      throw new SolidifyValidationException(
              new ValidationError(this.messageService.get("validation.deposit.collection.cannot_cancel_collection_error")),
              this.messageService.get("validation.deposit.collection.cannot_cancel_collection_update",
                      new Object[] { depositId, e.getMessage() }),
              e);
    } catch (Exception e) {
      throw new SolidifyValidationException(
              new ValidationError(this.messageService.get("validation.deposit.collection.cannot_cancel_collection_error")),
              this.messageService.get("validation.deposit.collection.cannot_cancel_collection_update_validate_new_version",
                      new Object[] { depositId, e.getMessage() }),
              e);
    }
  }

  public Deposit deleteDepositSip(String depositId) {
    try {
      Deposit deposit = this.findOne(depositId);
      if (deposit.getSip() != null) {
        FileTool.deleteFile(deposit.getSip());
        deposit.setSip(null);
        deposit = this.save(deposit);
        log.info("SIP package purged for deposit: resId={} status={}", deposit.getResId(), deposit.getStatus());
      }
      return deposit;
    } catch (IOException e) {
      throw new SolidifyRuntimeException(this.messageService.get("preingest.deposit.error.cannotpurged", new Object[] { depositId }), e);
    }
  }

  /**
   * Delete the folder containing deposit's files if it exists
   *
   * @param depositId
   */
  public void deleteDepositFolderFromDisk(String depositId) {
    final Path depositPath = Paths.get(this.preingestLocation).resolve(depositId);
    if (FileTool.isFolder(depositPath)) {
      if (FileTool.deleteFolder(depositPath)) {
        log.info("Deposit's folder deleted: {}", depositPath);
      } else {
        throw new SolidifyFileDeleteException("Cannot delete deposit's folder '" + depositPath + "'");
      }
    } else {
      log.info("Deposit's folder cannot be deleted as it does not exist: {}", depositPath);
    }
  }

  /**
   * Delete notifications related to this Deposit on admin module
   *
   * @param depositId
   */
  public void deleteDepositNotifications(String depositId) {
    try {
      log.info("Will delete notifications for deposit {}", depositId);
      this.trustedNotificationRemoteResourceService.deleteNotificationsOnDeposit(depositId);
    } catch (SolidifyResourceNotFoundException e) {
      log.info("No notification for deposit {} to delete", depositId);
    }
  }

  @Override
  public Deposit save(Deposit deposit) {
    final Deposit result = super.save(deposit);

    if (deposit.isStatusDirty()) {
      User updaterUser = this.userService.findByExternalUid(deposit.getLastUpdate().getWho());
      String message;
      switch (deposit.getStatus()) {
        case IN_VALIDATION -> {
          if (Boolean.TRUE.equals(deposit.getSubmissionPolicy().getSubmissionApproval())) {
            message = this.messageService.get(VALIDATION_NOTIFICATION_DEPOSIT);
            this.trustedNotificationRemoteResourceService.createNotification(updaterUser, message, NotificationType.VALIDATE_DEPOSIT_REQUEST,
                    deposit.getOrganizationalUnit(), deposit.getResId());
          }
        }
        case APPROVED -> {
          message = this.messageService.get(NOTIFICATION_DEPOSIT_APPROVED);
          this.trustedNotificationRemoteResourceService.createdDepositApprovedNotificationForCreators(deposit, updaterUser, message);
        }
        case IN_ERROR -> {
          message = this.messageService.get(NOTIFICATION_DEPOSIT_ERROR, new Object[] { deposit.getResId() });
          this.trustedNotificationRemoteResourceService.createNotification(updaterUser, message, NotificationType.IN_ERROR_DEPOSIT_INFO,
                  deposit.getOrganizationalUnit(), deposit.getResId());
        }
        case COMPLETED -> {
          // Check if the deposit was already completed to avoid recreating notifications and resending emails
          if (!this.historyService.hasBeenCompleted(deposit.getResId())
                  || this.historyService.hasBeenUpdatingMetadata(deposit.getResId())) {
            message = this.messageService.get(NOTIFICATION_DEPOSIT_CREATED, new Object[] { deposit.getResId() });
            this.trustedNotificationRemoteResourceService.createDepositCompletedNotifications(deposit, updaterUser, message);
          }
        }
        default -> log.debug("sonar lint rule");
      }
    }

    final boolean statusHasChangedAndNeedsProcessing = deposit.isStatusDirty() &&
            (deposit.getStatus().equals(DepositStatus.APPROVED)
                    || deposit.getStatus().equals(DepositStatus.CLEANING)
                    || deposit.getStatus().equals(DepositStatus.DELETING)
                    || deposit.getStatus().equals(DepositStatus.CANCEL_EDITING_METADATA)
                    || deposit.getStatus().equals(DepositStatus.CHECKING_COMPLIANCE)
                    || deposit.getStatus().equals(DepositStatus.CHECKING_COMPLIANCE_CLEANED)
                    || deposit.getStatus().equals(DepositStatus.UPGRADING_METADATA)
                    || (deposit.getStatus().equals(DepositStatus.CHECKED) && deposit.getPreparationId() != null));

    if (statusHasChangedAndNeedsProcessing) {
      log.info("DepositService will send a DepositMessage for deposit '{}' with status '{}'", deposit.getResId(), deposit.getStatus());
      boolean bigPackage = this.getSize(deposit.getResId()) > this.fileSizeLimit.toBytes();
      SolidifyEventPublisher.getPublisher().publishEvent(new DepositMessage(deposit.getResId(), bigPackage));
    }
    return result;
  }

  /**
   * Check if a deposit status can be manually updated to the new given status, according to the
   * deposit's management workflow. Some status transitions (e.g. from APPROVED to CHECK) are
   * considered here as invalid, because even if they exist in the workflow, they are automatically
   * done, and should not be forced manually.
   *
   * @param deposit The deposit to check
   * @param newStatus The new status to apply
   * @return True if the status update is valid
   */
  public boolean statusUpdateIsValid(Deposit deposit, DepositStatus newStatus) {

    final DepositStatus currentStatus = deposit.getStatus();

    return switch (currentStatus) {
      case IN_PROGRESS -> (newStatus == IN_VALIDATION && this.needsValidation(deposit))
              || (newStatus == DepositStatus.APPROVED && !this.needsValidation(deposit));
      case IN_VALIDATION -> newStatus == DepositStatus.IN_PROGRESS
              || newStatus == DepositStatus.EDITING_METADATA
              || newStatus == DepositStatus.APPROVED
              || newStatus == DepositStatus.EDITING_METADATA_REJECTED
              || newStatus == DepositStatus.REJECTED;
      case IN_ERROR, REJECTED -> newStatus == DepositStatus.IN_PROGRESS
              || newStatus == DepositStatus.EDITING_METADATA;
      case EDITING_METADATA_REJECTED, COMPLETED, CLEANED -> newStatus == DepositStatus.EDITING_METADATA
              || (newStatus == DepositStatus.UPGRADING_METADATA
                      && currentStatus == DepositStatus.CLEANED
                      && deposit.getMetadataVersion() != DLCMMetadataVersion.getDefaultVersion());
      case EDITING_METADATA -> newStatus == DepositStatus.CLEANED
              || newStatus == DepositStatus.CANCEL_EDITING_METADATA
              || (newStatus == IN_VALIDATION && this.needsValidation(deposit))
              || (newStatus == DepositStatus.APPROVED && !this.needsValidation(deposit));
      case APPROVED, SUBMITTED, CHECKED -> false;
      default -> false;
    };
  }

  public int countUpdatedFiles(String depositId, DataCategory dataType) {
    // Check if there is already a permitted file uploaded
    OffsetDateTime lastCompletedTime = this.historyService.findLastCompletedOrCleanedTime(depositId);
    return ((DepositRepository) this.itemRepository).countDataFilesWithDataCategoryAndDataTypeAfterDate(depositId, DataCategory.Internal,
            dataType, lastCompletedTime);
  }

  public boolean canUploadUpdatedFiles(String depositId, DataCategory dataType) {
    OffsetDateTime lastCompletedTime = this.historyService.findLastCompletedOrCleanedTime(depositId);
    if (lastCompletedTime != null && ((DepositRepository) this.itemRepository).countDataFilesWithDataCategoryAndDataTypeAfterDate(depositId, DataCategory.Internal, dataType, lastCompletedTime) > 0 ) {
      return false;
    } else if (lastCompletedTime == null && ((DepositRepository) this.itemRepository).countDataFilesWithDataCategoryAndDataType(depositId, DataCategory.Internal, dataType) > 0 ) {
      return false;
    }
    return true;
  }

  @Override
  public void validateLinkedResources(Deposit deposit, BindingResult errors) {
    if (!List.of(DELETING, CANCEL_EDITING_METADATA).contains(deposit.getStatus())) {
      this.checkOrganizationalUnitExistence(deposit, errors);
      this.checkSubmissionPolicyExistence(deposit, errors);
      this.checkPreservationPolicyExistence(deposit, errors);
      this.checkContributorsExistence(deposit, errors);
      this.checkLicenseExistence(deposit, errors);
      this.checkLanguageExistence(deposit, errors);
      this.checkArchiveTypeExistence(deposit, errors);
      this.checkAdditionalFieldsFormExistence(deposit, errors);
    }
  }

  private void checkAdditionalFieldsFormExistence(Deposit deposit, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(deposit.getAdditionalFieldsFormId())) {
      try {
        this.additionalFieldsFormService
                .findOneWithCache(deposit.getOrganizationalUnitId(), deposit.getAdditionalFieldsFormId());
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), "additionalFieldsFormId",
                this.messageService.get(NOT_EXIST, new Object[] { deposit.getAdditionalFieldsFormId() })));
      }
    }
  }

  private void checkArchiveTypeExistence(Deposit deposit, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(deposit.getArchiveTypeId())) {
      try {
        this.archiveTypeService.findOne(deposit.getArchiveTypeId());
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), DLCMConstants.ARCHIVE_TYPE_ID_FIELD,
                this.messageService.get(NOT_EXIST, new Object[] { deposit.getArchiveTypeId() })));
      }
    }
  }

  private void checkLanguageExistence(Deposit deposit, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(deposit.getLanguageId())) {
      try {
        this.languageService.findOne(deposit.getLanguageId());
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), "languageId",
                this.messageService.get(NOT_EXIST, new Object[] { deposit.getLanguageId() })));
      }
    }
  }

  private void checkLicenseExistence(Deposit deposit, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(deposit.getLicenseId())) {
      try {
        this.licenseService.findOne(deposit.getLicenseId());
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), LICENSE_ID_FIELD,
                this.messageService.get(NOT_EXIST, new Object[] { deposit.getLicenseId() })));
      }
    }
  }

  private void checkContributorsExistence(Deposit deposit, BindingResult errors) {
    if (!deposit.getContributorIds().isEmpty()) {
      for (final String contributorId : deposit.getContributorIds()) {
        if (!StringTool.isNullOrEmpty(contributorId)) {
          try {
            this.personService.findOne(contributorId);
          } catch (final SolidifyResourceNotFoundException e) {
            errors.addError(new FieldError(deposit.getClass().getSimpleName(), "person",
                    this.messageService.get(NOT_EXIST, new Object[] { contributorId })));
          }
        }
      }
    }
  }

  private void checkPreservationPolicyExistence(Deposit deposit, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(deposit.getPreservationPolicyId())) {
      try {
        // Check if exists
        this.preservationPolicyService.findOne(deposit.getPreservationPolicyId());
        // Check if compatible with orgUnit
        if (this.orgUnitService.getPreservationPolicies(deposit.getOrganizationalUnitId()).stream()
                .noneMatch(p -> p.getResId().equals(deposit.getPreservationPolicyId()))) {
          errors.addError(new FieldError(deposit.getClass().getSimpleName(), DLCMConstants.PRESERVATION_POLICY_ID_FIELD,
                  this.messageService.get(INVALID_POLICY, new Object[] { deposit.getPreservationPolicyId() })));
        }
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), DLCMConstants.PRESERVATION_POLICY_ID_FIELD,
                this.messageService.get(NOT_EXIST, new Object[] { deposit.getPreservationPolicyId() })));
      }
    }
  }

  private void checkSubmissionPolicyExistence(Deposit deposit, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(deposit.getSubmissionPolicyId())) {
      try {
        // Check if exists
        this.submissionPolicyService.findOne(deposit.getSubmissionPolicyId());
        // Check if compatible with orgUnit
        if (this.orgUnitService.getSubmissionPolicies(deposit.getOrganizationalUnitId()).stream()
                .noneMatch(p -> p.getResId().equals(deposit.getSubmissionPolicyId()))) {
          errors.addError(new FieldError(deposit.getClass().getSimpleName(), DLCMConstants.SUBMISSION_POLICY_ID_FIELD,
                  this.messageService.get(INVALID_POLICY, new Object[] { deposit.getSubmissionPolicyId() })));
        }
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), DLCMConstants.SUBMISSION_POLICY_ID_FIELD,
                this.messageService.get(NOT_EXIST, new Object[] { deposit.getSubmissionPolicyId() })));
      }
    }
  }

  private void checkOrganizationalUnitExistence(Deposit deposit, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(deposit.getOrganizationalUnitId())) {
      try {
        // Need a trusted remote service because this method can be called by a Job
        final OrganizationalUnit subitem = this.orgUnitService.findOne(deposit.getOrganizationalUnitId());
        if (!subitem.isOpen()) {
          errors.addError(new FieldError(deposit.getClass().getSimpleName(), DLCMConstants.ORG_UNIT_ID_FIELD,
                  this.messageService.get(UNIT_CLOSED, new Object[] { deposit.getOrganizationalUnitId() })));
        }
      } catch (final SolidifyResourceNotFoundException e) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), DLCMConstants.ORG_UNIT_ID_FIELD,
                this.messageService.get(NOT_EXIST, new Object[] { deposit.getOrganizationalUnitId() })));
      }
    }
  }

  public long getSize(String depositId) {
    return ((DepositRepository) this.itemRepository).getSize(depositId);
  }

  @Override
  public DepositSpecification getSpecification(Deposit resource) {
    if (resource instanceof DepositDto depositDto) {
      return new DepositDtoSpecification(depositDto);
    } else {
      return new DepositSpecification(resource);
    }
  }

  public int getDataFileNumber(String depositId) {
    return ((DepositRepository) this.itemRepository).countDataFiles(depositId);
  }

  /**
   * Return the minimum compliance level value found amongst deposit datafiles
   *
   * @param depositId
   * @return
   */
  public ComplianceLevel getComplianceLevel(String depositId) {
    Integer minComplianceLevel = ((DepositRepository) this.itemRepository).getMinimumDataFilesComplianceLevel(depositId);
    if (minComplianceLevel != null) {
      return ComplianceLevel.fromValue(minComplianceLevel);
    }
    return null;
  }

  public boolean hasDataFiles(String depositId) {
    return this.getDataFileNumber(depositId) > 0;
  }

  public boolean hasDataFileInError(String depositId) {
    return ((DepositRepository) this.itemRepository).countDataFilesInError(depositId) > 0;
  }

  public boolean hasAllDataFileCleaned(String depositId) {
    final int nbDataFilesNotCleaned = ((DepositRepository) this.itemRepository).countDataFilesNotCleaned(depositId);
    return nbDataFilesNotCleaned == 0;
  }

  public boolean hasAllDataFileReadyOrCleaned(String depositId) {
    final int nbDataFilesNotCleanedOrReady = ((DepositRepository) this.itemRepository).countDataFilesNotCleanedOrReady(depositId);
    return nbDataFilesNotCleanedOrReady == 0;
  }

  public boolean hasAllDataFileComplianceLevelCheckedOrInError(String depositId) {
    final int nbDataFilesNotCheckedOrInError = ((DepositRepository) this.itemRepository).countDataFilesNotComplianceLevelCheckedOrInError(
            depositId);
    return nbDataFilesNotCheckedOrInError == 0;
  }

  public int countDataFilesInError(String depositId) {
    return ((DepositRepository) this.itemRepository).countDataFilesInError(depositId);
  }

  public int countDataFilesWithDataCategory(String depositId, DataCategory dataCategory) {
    return ((DepositRepository) this.itemRepository).countDataFilesWithDataCategory(depositId, dataCategory);
  }

  public int countDataFilesWithDataCategoryAndDataType(String depositId, DataCategory dataCategory, DataCategory datatype) {
    String fileName = this.getReferenceFileNameFromDataType(datatype);
    if (StringTool.isNullOrEmpty(fileName)) {
      return ((DepositRepository) this.itemRepository).countDataFilesWithDataCategoryAndDataType(depositId, dataCategory, datatype);
    } else {
      return ((DepositRepository) this.itemRepository).countDataFilesWithDataCategoryAndDataTypeAndFileName(depositId, dataCategory, datatype,
              fileName);
    }
  }

  private String getReferenceFileNameFromDataType(DataCategory dataType) {
    switch (dataType) {
      case ArchiveThumbnail -> {
        return DLCMConstants.ARCHIVE_THUMBNAIL;
      }
      case ArchiveReadme -> {
        return DLCMConstants.ARCHIVE_README;
      }
      case ArchiveDataUseAgreement -> {
        return DLCMConstants.ARCHIVE_DUA;
      }
      default -> {
        return null;
      }
    }
  }

  public int countContentDataFiles(String depositId) {
    return ((DepositRepository) this.itemRepository).countContentDataFiles(depositId);
  }

  @Override
  public Page<Deposit> findAll(Pageable pageable) {
    Page<Deposit> depositPage = super.findAll(pageable);
    return this.addComputedValues(depositPage);
  }

  @Override
  public Page<Deposit> findAll(Specification<Deposit> spec, Pageable pageable) {
    Page<Deposit> depositPage = super.findAll(spec, pageable);
    return this.addComputedValues(depositPage);
  }

  public Page<Deposit> findAllWithoutEmbeddedResources(Specification<Deposit> spec, Pageable pageable) {
    final Specification<Deposit> completedSpec = this.addSpecificationFilter(spec);
    Page<Deposit> results = this.itemRepository.findAll(completedSpec, pageable);
    results.forEach(this::afterFind);

    return this.addComputedValues(results);
  }

  @Override
  public Deposit findOne(String id) {
    Deposit deposit = super.findOne(id);
    return this.addComputedValues(deposit);
  }

  public String findSubmitterInHistory(String id) {
    // Get last deposit process
    final List<StatusHistory> lastProcess = this.historyService.getDepositLastSubmission(id);
    // Find if there is an approval
    Optional<StatusHistory> inValidation = lastProcess.stream().filter(e -> e.getStatus().equals(DepositStatus.IN_VALIDATION.toString()))
            .findFirst();
    if (inValidation.isPresent()) {
      return inValidation.get().getCreatedBy();
    }
    // If not
    Optional<StatusHistory> approved = lastProcess.stream().filter(e -> e.getStatus().equals(DepositStatus.APPROVED.toString())).findFirst();
    if (approved.isPresent()) {
      return approved.get().getCreatedBy();
    }
    return null;
  }

  public void setReadyOrCleaningToCleaned(String depositId) {
    ((DepositRepository) this.itemRepository).setReadyOrCleaningToCleaned(depositId);
  }

  public boolean hasBeenInEditingMetadata(String depositId) {
    return this.historyService.hasBeenUpdatingMetadata(depositId);
  }

  @Override
  protected Specification<Deposit> addSpecificationFilter(Specification<Deposit> spec) {
    return this.specificationPermissionFilter.addPermissionsFilter(spec, this.httpRequestInfoProvider.getPrincipal());
  }

  @Override
  protected void beforeValidate(Deposit deposit) {
    if (!StringTool.isNullOrEmpty(deposit.getOrganizationalUnitId())) {
      if (deposit.getAccess() == Access.PUBLIC && StringTool.isNullOrEmpty(deposit.getLicenseId())) {
        OrganizationalUnit orgUnit = this.orgUnitService.findOne(deposit.getOrganizationalUnitId());
        License orgUnitDefaultLicense = orgUnit.getDefaultLicense();
        if (orgUnitDefaultLicense != null) {
          deposit.setLicenseId(orgUnitDefaultLicense.getResId());
        } else {
          deposit.setLicenseId(this.defaultLicense);
        }
      }
      if (deposit.getAdditionalFieldsValues() != null && deposit.getAdditionalFieldsValues().isEmpty()) {
        deposit.setAdditionalFieldsValues(null);
      }
      if (deposit.getAdditionalFieldsValues() == null
              || (deposit.getAdditionalFieldsFormId() != null && deposit.getAdditionalFieldsFormId().isEmpty())) {
        deposit.setAdditionalFieldsFormId(null);
      }
    }
  }

  /*****************************************/

  @Override
  protected <S extends Resource> RemoteResourceService<S> getResourceService(Class<S> type) {

    if (type == Language.class) {
      return (RemoteResourceService<S>) this.languageService;
    } else if (type == Person.class) {
      return (RemoteResourceService<S>) this.personService;
    } else if (type == OrganizationalUnit.class) {
      return (RemoteResourceService<S>) this.orgUnitService;
    } else if (type == SubmissionAgreement.class) {
      return (RemoteResourceService<S>) this.submissionAgreementService;
    } else if (type == License.class) {
      return (RemoteResourceService<S>) this.licenseService;
    } else if (type == ArchivalInfoPackage.class) {
      return (RemoteResourceService<S>) this.aipService;
    } else if (type == SubmissionPolicy.class) {
      return (RemoteResourceService<S>) this.submissionPolicyService;
    } else if (type == PreservationPolicy.class) {
      return (RemoteResourceService<S>) this.preservationPolicyService;
    } else if (type == ArchiveType.class) {
      return (RemoteResourceService<S>) this.archiveTypeService;
    } else if (type == SubjectArea.class) {
      return (RemoteResourceService<S>) this.subjectAreaService;
    }

    throw new UnsupportedOperationException("Missing resource service for type " + type.getName());
  }

  @Override
  protected void validateItemSpecificRules(Deposit deposit, BindingResult errors) {
    this.checkEmbargo(deposit, errors);
    this.checkCollectionDates(deposit, errors);
    this.checkCollection(deposit, errors);
    this.checkDataSensitivity(deposit, errors);
    this.checkArchiveType(deposit, errors);
    this.checkDataFiles(deposit, errors);
    this.checkDoi(deposit, errors);
    this.checkDataUsePolicy(deposit, errors);
    this.checkCustomMetadata(deposit, errors);
  }

  private void checkDataUsePolicy(Deposit deposit, BindingResult errors) {
    // Check that DuaType is compatible with DataTag only when metadaversion is > 3.1
    if (this.isDepositVersion31AtLeast(deposit)) {
      this.checkDataUsePolicyForPublicDeposit(deposit, errors);
      this.checkDataUsePolicyForRestrictedDeposit(deposit, errors);
      this.checkDataUsePolicyForClosedDeposit(deposit, errors);
    } else {
      // Check DataUsePolicy compatibility with metadata version
      if (deposit.getDataUsePolicy() == null) {
        errors.addError(
                new FieldError(deposit.getClass().getSimpleName(), DATA_USE_POLICY_FIELD, this.messageService.get(DATA_USE_POLICY_MISSING)));
      }
      this.checkLicenseForPublicDeposit(deposit, errors);
      this.checkLicenseForRestrictedOrClosedDeposit(deposit, errors);
    }

  }

  private void checkLicenseForPublicDeposit(Deposit deposit, BindingResult errors) {
    if (!Access.PUBLIC.equals(deposit.getAccess())) {
      return;
    }
    if (deposit.getDataUsePolicy() != DataUsePolicy.LICENSE) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), DATA_USE_POLICY_FIELD, this.messageService.get(
              NOT_SUPPORTED, new Object[] { deposit.getMetadataVersion().getVersion() })));
    }
    if (deposit.getLicenseId() == null) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), LICENSE_ID_FIELD, this.messageService.get(LICENSE_MISSING)));
    }
  }

  private void checkLicenseForRestrictedOrClosedDeposit(Deposit deposit, BindingResult errors) {
    if (Access.PUBLIC.equals(deposit.getAccess())) {
      return;
    }
    if (deposit.getDataUsePolicy() != DataUsePolicy.LICENSE && deposit.getDataUsePolicy() != DataUsePolicy.NONE) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), DATA_USE_POLICY_FIELD,
              this.messageService.get(DATA_USE_POLICY_SHOULD_BE_NONE_OR_LICENCE)));
    }
    if (deposit.getDataUsePolicy() == DataUsePolicy.LICENSE && deposit.getLicenseId() == null) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), LICENSE_ID_FIELD, this.messageService.get(LICENSE_MISSING)));
    }
  }

  private void checkDataUsePolicyForClosedDeposit(Deposit deposit, BindingResult errors) {
    if (!Access.CLOSED.equals(deposit.getAccess())) {
      return;
    }
    if ((Access.CLOSED.equals(deposit.getAccess()) || Access.RESTRICTED.equals(deposit.getAccess()))
            && DataTag.YELLOW.equals(deposit.getDataSensitivity())
            && !DataUsePolicy.CLICK_THROUGH_DUA.equals(deposit.getDataUsePolicy())) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), DATA_USE_POLICY_FIELD,
              this.messageService.get(DATA_USE_POLICY_SHOULD_BE_CLICK_THROUGH)));
    }

    if (Access.CLOSED.equals(deposit.getAccess())
            && (DataTag.ORANGE.equals(deposit.getDataSensitivity())
                    || DataTag.RED.equals(deposit.getDataSensitivity())
                    || DataTag.CRIMSON.equals(deposit.getDataSensitivity()))
            && (!DataUsePolicy.SIGNED_DUA.equals(deposit.getDataUsePolicy()) && !DataUsePolicy.EXTERNAL_DUA.equals(
                    deposit.getDataUsePolicy()))) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), DATA_USE_POLICY_FIELD,
              this.messageService.get(DATA_USE_POLICY_SHOULD_BE_SIGNED_OR_EXTERNAL)));
    }
  }

  private void checkDataUsePolicyForRestrictedDeposit(Deposit deposit, BindingResult errors) {
    if (!Access.RESTRICTED.equals(deposit.getAccess())) {
      return;
    }
    if (DataTag.UNDEFINED.equals(deposit.getDataSensitivity())
            || DataTag.BLUE.equals(deposit.getDataSensitivity())
            || DataTag.GREEN.equals(deposit.getDataSensitivity())) {
      this.checkDataUsePolicyForGreenDeposit(deposit, errors);
    } else if (DataTag.YELLOW.equals(deposit.getDataSensitivity())) {
      this.checkDataUsePolicyForYellowDeposit(deposit, errors);
    }
  }

  private void checkDataUsePolicyForYellowDeposit(Deposit deposit, BindingResult errors) {
    if (DataTag.YELLOW.equals(deposit.getDataSensitivity())
            && !DataUsePolicy.CLICK_THROUGH_DUA.equals(deposit.getDataUsePolicy())) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), DATA_USE_POLICY_FIELD,
              this.messageService.get(DATA_USE_POLICY_SHOULD_BE_CLICK_THROUGH)));
    }
  }

  private void checkDataUsePolicyForGreenDeposit(Deposit deposit, BindingResult errors) {
    if ((DataTag.UNDEFINED.equals(deposit.getDataSensitivity())
            || DataTag.BLUE.equals(deposit.getDataSensitivity())
            || DataTag.GREEN.equals(deposit.getDataSensitivity()))
            && (!DataUsePolicy.NONE.equals(deposit.getDataUsePolicy())
                    && !DataUsePolicy.LICENSE.equals(deposit.getDataUsePolicy()))) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), DATA_USE_POLICY_FIELD,
              this.messageService.get(DATA_USE_POLICY_SHOULD_BE_NONE_OR_LICENCE)));
    }
  }

  private void checkDataUsePolicyForPublicDeposit(Deposit deposit, BindingResult errors) {
    if (!Access.PUBLIC.equals(deposit.getAccess())) {
      return;
    }
    if ((DataTag.BLUE.equals(deposit.getDataSensitivity()) || DataTag.UNDEFINED.equals(deposit.getDataSensitivity()))
            && !DataUsePolicy.LICENSE.equals(deposit.getDataUsePolicy())) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), DATA_USE_POLICY_FIELD,
              this.messageService.get(DATA_USE_POLICY_SHOULD_BE_LICENCE)));
    }
  }

  private boolean isDepositVersion31AtLeast(Deposit deposit) {
    return DLCMMetadataVersion.isAtLeastVersion(DLCMMetadataVersion.V3_1, deposit.getMetadataVersion());
  }

  private void checkDoi(Deposit deposit, BindingResult errors) {
    // Check DOI compatibility with metadata version
    if (!StringTool.isNullOrEmpty(deposit.getIsObsoletedBy())
            && deposit.getMetadataVersion().getVersionNumber() < DLCMMetadataVersion.V2_0.getVersionNumber()) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), "isObsoletedBy", this.messageService.get(
              NOT_SUPPORTED, new Object[] { deposit.getMetadataVersion().getVersion() })));
    }
    // Check that if a value is given for DOI isdIdenticalTo, isReferencedBy and isObsoletedBy, is a valid DOI
    List<RelationType> relationTypeList = Arrays.asList(RelationType.IS_IDENTICAL_TO, RelationType.IS_REFERENCED_BY,
            RelationType.IS_OBSOLETED_BY);
    for (RelationType rType : relationTypeList) {
      this.validateDoiRelationType(deposit, rType, errors);
    }
  }

  private void checkCustomMetadata(Deposit deposit, BindingResult errors) {

    // Check that if additional fields values are present an additional fields form is also present
    if (!StringTool.isNullOrEmpty(deposit.getAdditionalFieldsValues()) && StringTool.isNullOrEmpty(deposit.getAdditionalFieldsFormId())) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), "additionalFieldsFormId",
              this.messageService.get(ADDITIONAL_FIELDS_FORM_REQUIRED_WHEN_ADDITIONAL_FIELDS_VALUES_PRESENT)));
    }

    // Check that if a value is given for custom metadata, it is valid JSON
    if (!StringTool.isNullOrEmpty(deposit.getAdditionalFieldsValues())) {
      try {
        JSONTool.wellformed(deposit.getAdditionalFieldsValues());
      } catch (SolidifyCheckingException e) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), "customMetadata", this.messageService.get(
                INVALID_ADDITIONAL_FIELDS_VALUES)));
      }
    }
  }

  private void checkDataFiles(Deposit deposit, BindingResult errors) {
    // Check that deposit contains data files before sending to IN_VALIDATION status
    if (deposit.getStatus() == DepositStatus.APPROVED && ((!deposit.isCollection() && !this.hasDataFiles(deposit.getResId())) ||
            (deposit.isCollection() && deposit.getCollection().isEmpty()))) {

      errors.addError(
              new FieldError(deposit.getClass().getSimpleName(), STATUS_FIELD, this.messageService.get(HAS_DATA_FILES_BEFORE_VALIDATION)));
    }

    // Check that deposit data files are ready before sending to IN_VALIDATION status
    if (deposit.getStatus() == DepositStatus.APPROVED && !deposit.isCollection() && !this.hasAllDataFileReadyOrCleaned(deposit.getResId())) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), STATUS_FIELD, this.messageService.get(
              DATA_FILES_NOT_READY_OR_NOT_CLEANED)));
    }
  }

  private void checkArchiveType(Deposit deposit, BindingResult errors) {
    // Check if archive type is compatible with deposit only if there are datafiles/aip
    if (!deposit.getDataFiles().isEmpty() || !deposit.getCollection().isEmpty()) {
      // Default
      if (StringTool.isNullOrEmpty(deposit.getArchiveTypeId())) {
        this.initializeArchiveType(deposit, errors);
      } else {
        // Check if collection compatible
        deposit.setArchiveType(this.archiveTypeService.findOneWithCache(deposit.getArchiveTypeId()));
        String masterTypeId = deposit.getArchiveType().getMasterType().getResId();
        if ((deposit.isCollection() && !masterTypeId.equals(ArchiveType.COLLECTION_ID))
                || (!deposit.isCollection() && masterTypeId.equals(ArchiveType.COLLECTION_ID))) {
          errors.addError(new FieldError(deposit.getClass().getSimpleName(), DLCMConstants.ARCHIVE_TYPE_ID_FIELD,
                  this.messageService.get(ARCHIVE_TYPE_INCONSISTANT, new Object[] { deposit.getArchiveType().getTypeName() })));
        }
      }
    }
  }

  private void initializeArchiveType(Deposit deposit, BindingResult errors) {
    String archiveTypeId = this.defaultMasterArchiveType;
    if (deposit.isCollection()) {
      archiveTypeId = ArchiveType.COLLECTION_ID;
    }
    List<ArchiveType> archiveTypes = this.archiveTypeService.get(archiveTypeId).getData();
    if (archiveTypes.isEmpty()) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), DLCMConstants.ARCHIVE_TYPE_ID_FIELD,
              this.messageService.get(ARCHIVE_TYPE_WRONG_CONFIG)));
    } else {
      deposit.setArchiveTypeId(archiveTypes.get(0).getResId());
      deposit.setArchiveType(archiveTypes.get(0));
    }
  }

  private void checkDataSensitivity(Deposit deposit, BindingResult errors) {
    // Check if data sensitivity (DataTag) is compatible with access level
    if (deposit.getDataSensitivity() != null && !deposit.getDataSensitivity().isAccessLevelAllowed(deposit.getAccess())) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), DATA_SENSITIVITY_FIELD,
              this.messageService.get(DATA_SENSITIVITY, new Object[] { deposit.getDataSensitivity(), deposit.getAccess() })));
    }

    // Check if data sensitivity (DataTag) is compatible with access level
    if (deposit.getAccess() == Access.PUBLIC && Boolean.FALSE.equals(deposit.getContentStructurePublic())) {
      errors.addError(new FieldError(deposit.getClass().getSimpleName(), CONTENT_STRUCTURE_PUBLIC_FIELD,
              this.messageService.get(CONTENT_STRUCTURE_PUBLIC, new Object[] { deposit.getAccess(), deposit.getContentStructurePublic() })));
    }
  }

  private void checkCollectionDates(Deposit deposit, BindingResult errors) {
    // Check collection dates
    if (deposit.getCollectionBegin() != null && deposit.getCollectionEnd() != null
            && deposit.getCollectionBegin().isAfter(deposit.getCollectionEnd())) {

      errors.addError(new FieldError(deposit.getClass().getSimpleName(), "collectionEnd", this.messageService
              .get(COLLECTION_BEGIN_NOT_BEFORE_COLLECTION_END, new Object[] { deposit.getCollectionBegin(), deposit.getCollectionEnd() })));
    }
  }

  private void checkCollection(Deposit deposit, BindingResult errors) {

    /*
     * Check that all AIPs in collection belong to the same Organizational Unit as the Deposit itself
     * (collections with AIPs from multiple Organizational Unit are not allowed)
     */
    if (!deposit.getCollection().isEmpty()) {
      for (final String aipId : deposit.getCollection()) {
        final ArchivalInfoPackage aip = this.aipService.findOne(aipId);
        if (!aip.getInfo().getOrganizationalUnitId().equals(deposit.getOrganizationalUnitId())) {
          final OrganizationalUnit badOrgUnit = this.orgUnitService.findOne(aip.getInfo().getOrganizationalUnitId());
          errors.addError(new FieldError(deposit.getClass().getSimpleName(), "collection",
                  this.messageService.get(AIP_BAD_ORGANIZATIONAL_UNIT, new Object[] { aip.getInfo().getName(), badOrgUnit.getName() })));
        }
      }
    }
    this.checkCompatibilityBetweenCollectionAndArchivesIncluded(deposit, errors);
  }

  /**
   * Check that a collection and the archives included meet the following criteria :
   * - embargoes on collection's archives are compatible with collection's access level / embargo
   * - dataUsePolicy of a collection is greater or equal to the max DataUsePolicy of all elements of the collections
   * - datatag of a collection is greater or equal to the max datatag of all elements of the collections
   * - accessLevel of a collection is greater or equal to the max accessLevel of all elements of the collections
   * - sensitivity of a collection is greater or equal to the max sensitivity of all elements of the collections
   * - embargo access level of a collection is greater or equal to the max access level embargo of all elements of the collections
   * - embargo end date of a collection is later than the latest end date embargo of all elements of the collections
   */
  public void checkCompatibilityBetweenCollectionAndArchivesIncluded(Deposit deposit, BindingResult errors) {
    // Check DataUsePolicy of a collection is greater or equal to the max DataUsePolicy of all elements of the collections
    if (deposit.isCollection() && (!deposit.getCollection().isEmpty())) {
      DataUsePolicy maxDua = DataUsePolicy.NONE;
      DataTag maxDataTag = DataTag.UNDEFINED;
      Access maxAccessLevel = Access.PUBLIC;
      boolean hasEmbargo = false;
      Access maxEmbargoLevel = Access.PUBLIC;
      OffsetDateTime maxEmbargoDateEnd = OffsetDateTime.now();

      // For each AIP, find max value for Access level, data tag, dua & embargo
      for (final String aipId : deposit.getCollection()) {
        final ArchivalInfoPackage aip = this.aipService.findOne(aipId);
        if (aip.getInfo().getAccess().value() > maxAccessLevel.value()) {
          maxAccessLevel = aip.getInfo().getAccess();
        }
        if (aip.getInfo().getDataUsePolicy().value() > maxDua.value()) {
          maxDua = aip.getInfo().getDataUsePolicy();
        }
        if (aip.getInfo().getDataSensitivity().value() > maxDataTag.value()) {
          maxDataTag = aip.getInfo().getDataSensitivity();
        }
        if (aip.getInfo().hasEmbargo()) {
          hasEmbargo = true;
          if (aip.getInfo().getEmbargo().getAccess().value() > maxEmbargoLevel.value()) {
            maxEmbargoLevel = aip.getInfo().getEmbargo().getAccess();
          }
          if (aip.getInfo().getEmbargo().getEndDate().isAfter(maxEmbargoDateEnd)) {
            maxEmbargoDateEnd = aip.getInfo().getEmbargo().getEndDate();
          }
        }
      }
      // check Access Level for the collection
      if (deposit.getAccess().value() < maxAccessLevel.value()) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), ACCESS_LEVEL_FIELD,
                this.messageService.get(ACCESS_LEVEL_COLLECTION_SHOULD_BE_GREATER, new Object[] { deposit.getAccess(), maxAccessLevel })));
      }
      // check DataTag for the collection
      if (deposit.getDataSensitivity().value() < maxDataTag.value()) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), DATA_SENSITIVITY_FIELD,
                this.messageService.get(DATATAG_COLLECTION_SHOULD_BE_GREATER, new Object[] { deposit.getDataSensitivity(), maxDataTag })));
      }
      // check DUA for the collection
      if (deposit.getDataUsePolicy().value() < maxDua.value()) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), DATA_USE_POLICY_FIELD,
                this.messageService.get(DATA_USE_POLICY_COLLECTION_SHOULD_BE_GREATER,
                        new Object[] { deposit.getDataUsePolicy().name(), maxDua.name() })));
      }
      // check Embargo, embargo access level and duration for the collection
      if (hasEmbargo && maxEmbargoDateEnd.isAfter(OffsetDateTime.now())) {
        // Check that embargo levels are compatibility ( >= max AIP access level)
        if (!deposit.hasEmbargo()) {
          errors.addError(new FieldError(deposit.getClass().getSimpleName(), EMBARGO_LEVEL_FIELD,
                  this.messageService.get(COLLECTION_SHOULD_CONTAIN_EMBARGO)));
        }
        if (deposit.hasEmbargo() && deposit.getEmbargoAccess().value() < maxEmbargoLevel.value()) {
          errors.addError(new FieldError(deposit.getClass().getSimpleName(), EMBARGO_ACCESS_LEVEL_FIELD,
                  this.messageService.get(EMBARGO_ACCESS_LEVEL_COLLECTION_SHOULD_BE_GREATER,
                          new Object[] { deposit.getEmbargoAccess(), maxEmbargoLevel })));
        }
        // Check embargo end compatibility
        if (deposit.hasEmbargo() && deposit.getEmbargo().calculateEndDate(deposit.getCreationTime()).isBefore(maxEmbargoDateEnd)) {
          errors.addError(new FieldError(deposit.getClass().getSimpleName(), EMBARGO_END_DATE_FIELD,
                  this.messageService.get(EMBARGO_END_DATE_COLLECTION_SHOULD_BE_GREATER,
                          new Object[] { deposit.getEmbargo().calculateEndDate(deposit.getCreationTime()), maxEmbargoDateEnd })));
        }
      }
    }
  }

  private void checkEmbargo(Deposit deposit, BindingResult errors) {
    // Check embargo definition
    if (deposit.getEmbargo() != null) {
      if (deposit.getEmbargo().getAccess() != null && (deposit.getEmbargo().getMonths() == null || deposit.getEmbargo().getMonths() == 0)) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), "embargo.months", this.messageService.get(INVALID_EMBARGO)));
      }
      if (deposit.getEmbargo().getMonths() != null && deposit.getEmbargo().getMonths() > 0 && deposit.getEmbargo().getAccess() == null) {
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), "embargo.access", this.messageService.get(INVALID_EMBARGO)));
      }
    }
    // Check embargo values
    if (deposit.hasEmbargo() && ((deposit.getAccess() == deposit.getEmbargoAccess()) || (deposit.getAccess() == Access.RESTRICTED
            && deposit.getEmbargoAccess() == Access.PUBLIC) || (deposit.getAccess() == Access.CLOSED && deposit.getEmbargo() != null))) {

      errors.addError(new FieldError(deposit.getClass().getSimpleName(), "access",
              this.messageService.get(BAD_EMBARGO, new Object[] { deposit.getAccess().name(), deposit.getEmbargoAccess().name() })));
    }
  }

  private boolean needsValidation(Deposit deposit) {
    final SubmissionPolicy submissionPolicy = deposit.getSubmissionPolicy();
    if (submissionPolicy != null) {
      return submissionPolicy.getSubmissionApproval();
    } else {
      throw new SolidifyCheckingException(this.messageService.get("checking.policy.submission.missing"));
    }
  }

  private void validateDoiRelationType(Deposit deposit, RelationType relationType, BindingResult errors) {
    List<String> dois = new ArrayList<>();
    String fieldName;
    switch (relationType) {
      case IS_IDENTICAL_TO -> {
        dois.add(deposit.getIsIdenticalTo());
        fieldName = "isIdenticalTo";
      }
      case IS_REFERENCED_BY -> {
        dois.addAll(deposit.getIsReferencedBy());
        fieldName = "isReferencedBy";
      }
      case IS_OBSOLETED_BY -> {
        dois.add(deposit.getIsObsoletedBy());
        fieldName = "isObsoletedBy";
      }
      default -> throw new IllegalArgumentException("RelationType not supported : " + relationType);
    }
    for (String doiRef : dois) {
      if (!StringTool.isNullOrEmpty(doiRef) && !ValidationTool.isValidDOI(doiRef)) {
        final String errorMessage = this.messageService.get("validation.deposit.doi.invalid", new Object[] { doiRef });
        errors.addError(new FieldError(deposit.getClass().getSimpleName(), fieldName, errorMessage));
      }
    }
  }

  private void addDataFileNumber(Deposit deposit) {
    final int dataFileNumber = this.getDataFileNumber(deposit.getResId());
    deposit.setDataFileNumber(dataFileNumber);
  }

  private void addComplianceLevel(Deposit deposit) {
    final ComplianceLevel complianceLevel = this.getComplianceLevel(deposit.getResId());
    deposit.setComplianceLevel(complianceLevel);
  }

  private void addAnonymizedPageId(Deposit deposit) {
    Optional<AnonymizedDeposit> optionalAnonymizedDeposit =this.anonymizedDepositRepository.findLastAnonymizedDeposit(deposit);
    optionalAnonymizedDeposit.ifPresent(anonymizedDeposit -> deposit.setAnonymizedDepositPageId(anonymizedDeposit.getPageId()));
  }

  private Deposit addComputedValues(Deposit deposit) {
    this.addDataFileNumber(deposit);
    this.addComplianceLevel(deposit);
    this.addAnonymizedPageId(deposit);
    return deposit;
  }

  private Page<Deposit> addComputedValues(Page<Deposit> depositPage) {
    for (Deposit deposit : depositPage) {
      this.addComputedValues(deposit);
    }
    return depositPage;
  }

  public Deposit initValuesFromOrgUnit(Deposit deposit) {

    // If not set use keywords from organizational unit
    if (deposit.getKeywords() == null && deposit.getOrganizationalUnitId() != null) {
      final List<String> orgUnitKeywords = this.orgUnitService.findOne(deposit.getOrganizationalUnitId())
              .getKeywords();
      deposit.setKeywords(orgUnitKeywords);
    }

    // If there is no any subject area, it would take the ones from organizational unit
    if (deposit.getSubjectAreas().isEmpty() && deposit.getOrganizationalUnitId() != null) {
      final List<SubjectArea> subjectAreaList = this.orgUnitService.getSubjectAreas(deposit.getOrganizationalUnitId());
      subjectAreaList.forEach(deposit::addItem);
    }
    return deposit;
  }

  public boolean isSubmissionAgreementApproved(Deposit deposit, String userExternalUid) {
    final SubmissionPolicy submissionPolicy = deposit.getSubmissionPolicy();
    final SubmissionAgreement submissionAgreement = submissionPolicy.getSubmissionAgreement();
    final User user = this.trustedUserRemoteResourceService.findByExternalUid(userExternalUid);
    return switch (submissionPolicy.getSubmissionAgreementType()) {
      case WITHOUT -> true;
      case ON_FIRST_DEPOSIT -> this.trustedSubmissionAgreementUserRemoteResourceService.checkSubmissionAgreement(submissionAgreement.getResId(),
              user.getResId());
      case FOR_EACH_DEPOSIT -> this.historyService.hasSubmissionAgreementApproved(deposit.getResId());
    };
  }

  public String approveSubmissionAgreement(Deposit deposit, String userExternalUid) {
    final User user = this.trustedUserRemoteResourceService.findByExternalUid(userExternalUid);
    String userInfo = user.getFullName();
    if (!StringTool.isNullOrEmpty(user.getPerson().getOrcid())) {
      userInfo += " (" + user.getPerson().getOrcid() + ")";
    }
    final SubmissionAgreement submissionAgreement = deposit.getSubmissionPolicy().getSubmissionAgreement();
    final String submissionAgreementId = submissionAgreement.getResId();
    final String submissionAgreementTitle = submissionAgreement.getTitle();
    final String submissionAgreementVersion = submissionAgreement.getVersion();
    this.trustedSubmissionAgreementUserRemoteResourceService.approveSubmissionAgreement(submissionAgreementId, user.getResId());
    return "Submission agreement : " + submissionAgreementTitle
            + " [version : " + submissionAgreementVersion + "] (" + submissionAgreementId + ")"
            + " approved by user " + userInfo;
  }

  public String buildAnonymizedDeposit(Deposit deposit) {
    if (deposit.getStatus() != IN_PROGRESS) {
      throw new SolidifyValidationException(new ValidationError(HttpStatus.BAD_REQUEST.getReasonPhrase()),
              this.messageService.get("message.deposit.anonymized_download_link_only_for_in_progress"));
    }
    if (deposit.getDataFiles().isEmpty()) {
      throw new SolidifyValidationException(new ValidationError(HttpStatus.BAD_REQUEST.getReasonPhrase()),
              this.messageService.get("message.deposit.anonymized_download_link_without_files"));
    }
    final Optional<AnonymizedDeposit> lastOptionalAnonymizedDeposit = this.anonymizedDepositRepository.findLastAnonymizedDeposit(deposit);
    final String depositDatafilesChecksum = this.getDepositDatafilesChecksum(deposit.getResId(), this.defaultChecksumAlgorithm);
    final Path anonymizedDepositFolder = Path.of(this.preingestLocation, ANONYMIZED_DEPOSITS_FOLDER);

    // Return the existing anonymized deposit page id if no updates has been made and anonymized deposit is still present
    if (lastOptionalAnonymizedDeposit.isPresent() &&
            depositDatafilesChecksum.equals(lastOptionalAnonymizedDeposit.get().getAggregateChecksum())) {
      final String lastAnonymizedDepositId = lastOptionalAnonymizedDeposit.get().getResId();
      final Path lastAnonymizedDepositPath = anonymizedDepositFolder.resolve(lastAnonymizedDepositId + SolidifyConstants.ZIP_EXT);
      if (Files.exists(lastAnonymizedDepositPath)) {
        return lastOptionalAnonymizedDeposit.get().getPageId();
      } else {
        log.warn("Anonymized deposit {} is missing. Regenerating it.", lastAnonymizedDepositPath);
      }
    }

    // Create a new anonymized deposit
    final AnonymizedDeposit anonymizedDeposit = new AnonymizedDeposit();
    anonymizedDeposit.setDeposit(deposit);
    anonymizedDeposit.setTimestamp(OffsetDateTime.now());
    anonymizedDeposit.setAggregateChecksum(depositDatafilesChecksum);
    if (lastOptionalAnonymizedDeposit.isPresent()) {
      anonymizedDeposit.setPageId(lastOptionalAnonymizedDeposit.get().getPageId());
    } else {
      anonymizedDeposit.setPageId(UUID.randomUUID().toString());
    }
    this.anonymizedDepositRepository.save(anonymizedDeposit);
    new Thread(() -> {
      final Path newAnonymizedDepositPath = anonymizedDepositFolder.resolve(anonymizedDeposit.getResId() + SolidifyConstants.ZIP_EXT);
      final ZipTool zipTool = new ZipTool(newAnonymizedDepositPath.toString());
      zipTool.zipFiles(Paths.get(this.preingestLocation + "/" + deposit.getResId()));
    }).start();

    // Delete old anonymized deposit
    this.deleteLastAnonymizedDeposit(lastOptionalAnonymizedDeposit);

    return anonymizedDeposit.getPageId();
  }

  public void deleteLastAnonymizedDeposit(Deposit deposit) {
    final Optional<AnonymizedDeposit> lastOptionalAnonymizedDeposit = this.anonymizedDepositRepository.findLastAnonymizedDeposit(deposit);
    this.deleteLastAnonymizedDeposit(lastOptionalAnonymizedDeposit);
  }

  private void deleteLastAnonymizedDeposit(Optional<AnonymizedDeposit> lastOptionalAnonymizedDeposit) {
    final Path anonymizedDepositFolder = Path.of(this.preingestLocation, ANONYMIZED_DEPOSITS_FOLDER);
    if (lastOptionalAnonymizedDeposit.isPresent()) {
      final String lastAnonymizedDepositId = lastOptionalAnonymizedDeposit.get().getResId();
      try {
        final Path lastAnonymizedDepositPath = anonymizedDepositFolder.resolve(lastAnonymizedDepositId + SolidifyConstants.ZIP_EXT);
        Files.deleteIfExists(lastAnonymizedDepositPath);
      } catch (IOException e) {
        throw new SolidifyRuntimeException("Unable to delete old anonymized deposit " + lastAnonymizedDepositId, e);
      }
    }
  }

  public ModelAndView buildAnonymizedDepositPage(String anonymizedDepositPageId) {
    final Optional<Deposit> depositOptional = this.anonymizedDepositRepository.findDepositByPageId(anonymizedDepositPageId);
    if (depositOptional.isEmpty()) {
      throw new SolidifyResourceNotFoundException("No anonymized deposit page found");
    }
    final Deposit deposit = depositOptional.get();
    final DepositStatus depositStatus = deposit.getStatus();
    final List<AnonymizedDepositEntry> anonymizedDepositEntryList = this.buildAnonymizedDepositEntryList(anonymizedDepositPageId);
    if (IN_PROGRESS.equals(depositStatus)) {
      final ModelAndView modelAndView = new ModelAndView("anonymizedInProgressDeposit");
      modelAndView.addObject("anonymizedDepositEntryList", anonymizedDepositEntryList);
      modelAndView.addObject("preingestPublicUrl", this.preingestPublicUrl);
      return modelAndView;
    } else {
      String archiveLink = null;
      if (List.of(COMPLETED, EDITING_METADATA, CLEANED, CLEANING, UPGRADING_METADATA).contains(depositStatus)) {
        final String sipId = deposit.getSipId();
        if (sipId != null) {
          final SubmissionInfoPackage sip = this.trustedSubmissionInfoPackageRemoteResourceService.findOne(sipId);
          if (sip != null && sip.getAipId() != null) {
            archiveLink = this.archiveHomePage + sip.getAipId();
          }
        }
      }
      final ModelAndView modelAndView = new ModelAndView("anonymizedCompletedDeposit");
      modelAndView.addObject("archiveLink", archiveLink);
      return modelAndView;
    }
  }

  public List<AnonymizedDepositEntry> buildAnonymizedDepositEntryList(String anonymizedDepositPageId) {
    final List<AnonymizedDepositEntry> anonymizedDepositEntryList = new ArrayList<>();
    for (AnonymizedDeposit anonymizedDeposit : this.anonymizedDepositRepository
            .findAnonymizedDepositByPageIdOrderByTimestamp(anonymizedDepositPageId)) {
      anonymizedDepositEntryList.add(new AnonymizedDepositEntry(anonymizedDeposit.getResId(), anonymizedDeposit.getTimestamp().toString()));
    }
    return anonymizedDepositEntryList;
  }

  public String buildDownloadableDeposit(String depositId, String folder) {
    final Deposit deposit = this.findOne(depositId);

    final String zipFileLocation = this.preingestTempLocation + "/" + deposit.getResId() + SolidifyConstants.ZIP_EXT;
    final ZipTool zipTool = new ZipTool(zipFileLocation);
    if (folder == null) {
      zipTool.zipFiles(Paths.get(this.preingestLocation + "/" + deposit.getResId()));
    } else {
      final String folderMatch = folder.equals("/") ? "(\\/.*)*" : folder + "(\\/.+)*";
      final List<Path> dataFilesToZip = deposit.getDataFiles().stream().filter(df -> df.getRelativeLocation().matches(folderMatch))
              .map(AbstractDataFile::getPath).toList();
      if (dataFilesToZip.isEmpty()) {
        return null;
      }
      final Path sourcePath = Paths.get(this.preingestLocation + "/" + deposit.getResId());
      // list of destination path without the category folder (if exists)
      final List<Path> destinationPaths = deposit.getDataFiles().stream().filter(df -> df.getRelativeLocation().matches(folderMatch))
              .map(df -> {
                if (df.getDataCategory().equals(DataCategory.Package)) {
                  Path intermediatePath = Paths
                          .get(df.getInitialPath().equals("/") ? sourcePath.toString() : sourcePath + "/" + df.getInitialPath())
                          .relativize(df.getPath());
                  return Paths.get(df.getRelativeLocation()).resolve(intermediatePath);
                } else {
                  return sourcePath.resolve(df.getInitialPath()).relativize(df.getPath());
                }
              }).toList();
      zipTool.addFilesToZip(sourcePath, dataFilesToZip, destinationPaths, false, false);
    }
    return zipFileLocation;
  }

  public String getDepositDatafilesChecksum(String depositId, ChecksumAlgorithm checksumAlgo) {
    final StringBuilder checksumBuilder = new StringBuilder();
    for (DataFileChecksum dataFileChecksum : this.depositDataFileRepository.getListOfDepositDataFilesChecksum(depositId, checksumAlgo)) {
      checksumBuilder.append(dataFileChecksum.getChecksum());
    }
    try (InputStream checksumInputStream = new ByteArrayInputStream(checksumBuilder.toString().getBytes(StandardCharsets.UTF_8))) {
      return ChecksumTool.computeChecksum(checksumInputStream, checksumAlgo);
    } catch (IOException | NoSuchAlgorithmException e) {
      throw new SolidifyRuntimeException("Exception when computing datafiles checksum of deposit " + depositId, e);
    }
  }
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositContributorRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.dlcm.DLCMConstants;
import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.DepositContributor;

@Repository
@ConditionalOnBean(PreIngestController.class)
public interface DepositContributorRepository extends SolidifyRepository<DepositContributor> {

  // *************
  // ** Queries **
  // *************

  @Query("SELECT DISTINCT d FROM Deposit d JOIN d.contributors oup WHERE oup.compositeResId.personId=:"
          + DLCMConstants.DB_PERSON_ID)
  List<Deposit> findByPerson(String personId);

  @Query("SELECT dc FROM DepositContributor dc WHERE dc.compositeResId.personId=:"
          + DLCMConstants.DB_PERSON_ID + " AND dc.compositeResId.deposit.resId=:" + DLCMConstants.DB_DEPOSIT_ID)
  DepositContributor findByPersonAndDeposit(String personId, String depositId);

  @Query("SELECT c.compositeResId.personId FROM DepositContributor c GROUP BY c.compositeResId.personId HAVING COUNT(c.compositeResId.personId) >= :depositNumber")
  Page<String> findByDepositNumber(long depositNumber, Pageable pageable);

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositDataFileRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.ChecksumAlgorithm;

import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.dto.DepositDataFileStatisticsDto;
import ch.dlcm.model.preingest.DepositDataFile;

@Repository
@ConditionalOnBean(PreIngestController.class)
public interface DepositDataFileRepository extends DataFileRepository<DepositDataFile> {

  @Query("SELECT DISTINCT ddf.relativeLocation FROM DepositDataFile ddf "
          + "WHERE ddf.infoPackage.resId = :depositId ORDER BY ddf.relativeLocation")
  List<String> findDistinctRelativeLocationForDeposit(String depositId);

  @Query("SELECT ddf.resId FROM DepositDataFile ddf "
          + " WHERE ddf.infoPackage.resId = :depositId"
          + " AND (ddf.relativeLocation = :relativeLocationFolder OR ddf.relativeLocation like :relativeLocationSubFolder)")
  List<String> findDataFileIdByRelativeLocationFolder(String depositId,
          String relativeLocationFolder,
          String relativeLocationSubFolder);

  @Query("SELECT new ch.dlcm.model.dto.DepositDataFileStatisticsDto(ddf.status, COUNT(ddf.status)) "
          + "FROM DepositDataFile ddf "
          + "WHERE ddf.infoPackage.resId = :depositId "
          + "GROUP BY ddf.status")
  List<DepositDataFileStatisticsDto> getStatusStatistics(String depositId);

  @Query("SELECT ddf FROM DepositDataFile ddf "
          + "WHERE ddf.dataType=ch.dlcm.model.DataCategory.UpdatedMetadata "
          + "AND ddf.infoPackage.resId=:depositId "
          + "ORDER BY ddf.lastUpdate.when DESC")
  List<DepositDataFile> findUpdatedMetadataFileOrderByDate(String depositId);

  @Query("SELECT ddf FROM DepositDataFile ddf "
          + "WHERE (ddf.dataType=ch.dlcm.model.DataCategory.ArchiveThumbnail "
          + "  OR ddf.dataType=ch.dlcm.model.DataCategory.ArchiveReadme "
          + "  OR ddf.dataType=ch.dlcm.model.DataCategory.ArchiveDataUseAgreement)"
          + "AND ddf.infoPackage.resId=:depositId "
          + "ORDER BY ddf.lastUpdate.when DESC")
  List<DepositDataFile> findUpdatedFileByDataTypeOrderByDate(String depositId);

  @Query("SELECT ddfc FROM DepositDataFile ddf "
         + "LEFT JOIN ddf.checksums ddfc "
         + "WHERE ddf.infoPackage.resId = :depositId "
         + "AND ddfc.checksumAlgo = :checksumAlgo")
  List<DataFileChecksum> getListOfDepositDataFilesChecksum(String depositId, ChecksumAlgorithm checksumAlgo);

}

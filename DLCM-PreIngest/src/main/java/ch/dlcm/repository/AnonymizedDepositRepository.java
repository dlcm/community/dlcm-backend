/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.preingest.AnonymizedDeposit;
import ch.dlcm.model.preingest.Deposit;

@Repository
@ConditionalOnBean(PreIngestController.class)
public interface AnonymizedDepositRepository extends SolidifyRepository<AnonymizedDeposit> {

  @Query("SELECT ad FROM AnonymizedDeposit ad WHERE ad.deposit = :deposit ORDER BY ad.timestamp DESC LIMIT 1")
  Optional<AnonymizedDeposit> findLastAnonymizedDeposit(Deposit deposit);

  List<AnonymizedDeposit> findAnonymizedDepositByPageIdOrderByTimestamp(String pageId);

  @Query("SELECT deposit FROM AnonymizedDeposit ad WHERE ad.pageId = :pageId")
  Optional<Deposit> findDepositByPageId(String pageId);
}

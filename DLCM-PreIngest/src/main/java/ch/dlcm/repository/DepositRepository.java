/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - DepositRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.repository;

import java.time.OffsetDateTime;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.dlcm.controller.PreIngestController;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.preingest.Deposit;

@Repository
@ConditionalOnBean(PreIngestController.class)
public interface DepositRepository extends SolidifyRepository<Deposit> {

  @Query("SELECT COALESCE(SUM(df.fileSize),0) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId")
  long getSize(String depositId);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId")
  int countDataFiles(String depositId);

  @Query("SELECT MIN(df.complianceLevel) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId")
  Integer getMinimumDataFilesComplianceLevel(String depositId);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId AND df.status = 'IN_ERROR'")
  int countDataFilesInError(String depositId);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId AND df.dataCategory = :dataCategory")
  int countDataFilesWithDataCategory(String depositId, DataCategory dataCategory);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId AND df.dataCategory = :dataCategory AND df.dataType = :dataType")
  int countDataFilesWithDataCategoryAndDataType(String depositId, DataCategory dataCategory, DataCategory dataType);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId AND df.dataCategory = :dataCategory AND df.dataType = :dataType AND df.creation.when >:date")
  int countDataFilesWithDataCategoryAndDataTypeAfterDate(String depositId, DataCategory dataCategory, DataCategory dataType, OffsetDateTime date);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId AND df.dataCategory = :dataCategory AND df.dataType = :dataType"
          + " AND df.fileName = :fileName")
  int countDataFilesWithDataCategoryAndDataTypeAndFileName(String depositId, DataCategory dataCategory, DataCategory dataType, String fileName);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId AND df.status <> 'READY'")
  int countDataFilesNotReady(String depositId);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId AND df.status <> 'CLEANED'")
  int countDataFilesNotCleaned(String depositId);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId AND df.status NOT IN ('CHECKED_COMPLIANCE','CHECKED_COMPLIANCE_CLEANED','IN_ERROR')")
  int countDataFilesNotComplianceLevelCheckedOrInError(String depositId);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId AND df.status NOT IN ('READY','CLEANED')")
  int countDataFilesNotCleanedOrReady(String depositId);

  @Query("SELECT COUNT(df.resId) FROM DepositDataFile df WHERE df.infoPackage.resId = :depositId AND df.dataCategory NOT IN ('Package','Internal')")
  int countContentDataFiles(String depositId);

  @Modifying
  @Query("UPDATE DepositDataFile df SET df.status = 'CLEANED' WHERE  df.infoPackage.resId = :depositId AND " +
          "(df.status = 'READY' OR df.status = 'CLEANING')")
  void setReadyOrCleaningToCleaned(String depositId);
}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - PreIngestPermissionEvaluatorTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test;

import static ch.dlcm.controller.DLCMControllerAction.APPROVE;
import static ch.dlcm.controller.DLCMControllerAction.CREATE;
import static ch.dlcm.controller.DLCMControllerAction.DELETE;
import static ch.dlcm.controller.DLCMControllerAction.DELETE_FILE;
import static ch.dlcm.controller.DLCMControllerAction.DELETE_FOLDER;
import static ch.dlcm.controller.DLCMControllerAction.DOWNLOAD_FILE;
import static ch.dlcm.controller.DLCMControllerAction.GET;
import static ch.dlcm.controller.DLCMControllerAction.GET_FILE;
import static ch.dlcm.controller.DLCMControllerAction.HISTORY;
import static ch.dlcm.controller.DLCMControllerAction.LIST;
import static ch.dlcm.controller.DLCMControllerAction.LIST_FILES;
import static ch.dlcm.controller.DLCMControllerAction.RESERVE_DOI;
import static ch.dlcm.controller.DLCMControllerAction.RESUME_FILE;
import static ch.dlcm.controller.DLCMControllerAction.SUBMIT_FOR_APPROVAL;
import static ch.dlcm.controller.DLCMControllerAction.UPDATE;
import static ch.dlcm.controller.DLCMControllerAction.UPDATE_FILE;
import static ch.dlcm.controller.DLCMControllerAction.UPLOAD_FILE;
import static ch.dlcm.controller.DLCMControllerAction.VALIDATE_FILE;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.dlcm.business.DepositService;
import ch.dlcm.controller.DLCMControllerAction;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.service.security.DepositPermissionService;

@ExtendWith(SpringExtension.class)
class PreIngestPermissionEvaluatorTest extends AbstractPermissionEvaluatorTest {

  private final Map<Authentication, List<DLCMControllerAction>> mapAllowedDepositActions = new LinkedHashMap<>();

  @Mock
  private DepositService depositService;

  private DepositPermissionService depositPermissionService;

  @Override
  @BeforeEach
  void setUp() {
    super.setUp();

    // Set the map of allowed actions, valid with a Deposit with InProgress status
    this.mapAllowedDepositActions.put(this.authManager, Arrays.asList(DLCMControllerAction.values()));
    this.mapAllowedDepositActions.put(this.authApprover,
            Arrays.asList(CREATE, DELETE, DELETE_FILE, DELETE_FOLDER, UPDATE, UPDATE_FILE, RESERVE_DOI, RESUME_FILE, VALIDATE_FILE,
                    DOWNLOAD_FILE, UPLOAD_FILE, HISTORY, GET, GET_FILE, LIST_FILES, APPROVE, SUBMIT_FOR_APPROVAL, LIST));
    this.mapAllowedDepositActions.put(this.authSteward,
            Arrays.asList(CREATE, DELETE, DELETE_FILE, DELETE_FOLDER, UPDATE, UPDATE_FILE, RESERVE_DOI, RESUME_FILE, VALIDATE_FILE,
                    DOWNLOAD_FILE, UPLOAD_FILE, HISTORY, GET, GET_FILE, LIST_FILES, APPROVE, SUBMIT_FOR_APPROVAL, LIST));
    this.mapAllowedDepositActions.put(this.authCreator,
            Arrays.asList(CREATE, DELETE, DELETE_FILE, DELETE_FOLDER, UPDATE, UPDATE_FILE, RESERVE_DOI, RESUME_FILE, VALIDATE_FILE,
                    DOWNLOAD_FILE, UPLOAD_FILE, HISTORY, GET, GET_FILE, LIST_FILES, APPROVE, SUBMIT_FOR_APPROVAL, LIST));

    this.mapAllowedDepositActions.put(this.authVisitor, List.of());

    // Mock the findOne method on depositService
    final Deposit deposit = new Deposit();
    deposit.setOrganizationalUnitId(ORGANIZATIONAL_UNIT_ID);
    deposit.setStatus(Deposit.DepositStatus.IN_PROGRESS);
    when(this.depositService.findOne(EXISTENT_ID)).thenReturn(deposit);

    // Construct the permissions service
    this.depositPermissionService = new DepositPermissionService(this.depositService,
            this.trustedPersonRemoteResourceService,
            this.organizationalUnitRemoteResourceService,
            this.httpRequestInfoProvider);
  }

  @Test
  void depositAllowedActions() {
    this.allowedActionsTest(this.mapAllowedDepositActions, this.depositPermissionService);
  }

  @Test
  void depositForbiddenActions() {
    this.forbiddenActionsTest(this.mapAllowedDepositActions, this.depositPermissionService);
  }

  @Test
  void wrongAction() {
    this.setSecurityContextAuthentication(this.authManager);
    assertThrows(SolidifyRuntimeException.class, () -> this.depositPermissionService.isAllowed(EXISTENT_ID, INEXISTENT_ACTION));
  }

  @Test
  void wrongResId() {
    this.setSecurityContextAuthentication(this.authManager);
    assertThrows(NoSuchElementException.class, () -> this.depositPermissionService.isAllowed(INEXISTENT_ID, DELETE.toString()));

  }

}

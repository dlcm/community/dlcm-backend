/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - PreparationServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preparation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.OffsetDateTime;

import org.junit.jupiter.api.AfterEach;
import org.mockito.Mock;

import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;
import ch.unige.solidify.util.ZipTool;

import ch.dlcm.DLCMConstants;
import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.business.DepositDataFileService;
import ch.dlcm.business.DepositService;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.fileformat.FileFormatService;
import ch.dlcm.model.AbstractDataFile.DataFileStatus;
import ch.dlcm.model.Access;
import ch.dlcm.model.ComplianceLevel;
import ch.dlcm.model.DataCategory;
import ch.dlcm.model.DataFileChecksum;
import ch.dlcm.model.DataFileChecksum.ChecksumOrigin;
import ch.dlcm.model.DataFileChecksum.ChecksumType;
import ch.dlcm.model.DataTag;
import ch.dlcm.model.FileFormat;
import ch.dlcm.model.MetadataFormat;
import ch.dlcm.model.Package;
import ch.dlcm.model.Tool;
import ch.dlcm.model.VirusCheck;
import ch.dlcm.model.policies.PreservationPolicy;
import ch.dlcm.model.policies.SubmissionAgreementType;
import ch.dlcm.model.policies.SubmissionPolicy;
import ch.dlcm.model.preingest.Deposit;
import ch.dlcm.model.preingest.Deposit.DepositStatus;
import ch.dlcm.model.preingest.DepositDataFile;
import ch.dlcm.model.settings.ArchiveType;
import ch.dlcm.model.settings.License;
import ch.dlcm.model.settings.MetadataType;
import ch.dlcm.model.settings.OrganizationalUnit;
import ch.dlcm.model.settings.Person;
import ch.dlcm.preparation.PreparationService;
import ch.dlcm.service.HistoryService;
import ch.dlcm.service.IdentifierService;
import ch.dlcm.service.MetadataService;
import ch.dlcm.service.rest.fallback.FallbackArchivalInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchivePublicMetadataRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackArchiveTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLanguageRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackLicenseRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackMetadataTypeRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackOrganizationalUnitRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPersonRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackPreservationPolicyRemoteResourceService;
import ch.dlcm.service.rest.fallback.FallbackSubmissionInfoPackageRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedArchivePrivateMetadataRemoteResourceService;
import ch.dlcm.service.rest.trusted.TrustedDepositRemoteResourceService;

public abstract class PreparationServiceTest {
  private static final String LICENSE_ID = "licenseId";
  private static final String XML_TYPE_ID = "xml-1.0";
  private static final String JSON_TYPE_ID = "json-1.0";
  private static final String ORG_UNIT_ID = DLCMConstants.DB_ORG_UNIT_ID;
  private static final String PERSON_ID = DLCMConstants.DB_PERSON_ID;

  protected Path workingDir = Paths.get("src", "test", "resources", "testing");
  protected GitInfoProperties gitInfoProperties = new GitInfoProperties();
  protected DLCMProperties dlcmProperties;
  protected DLCMRepositoryDescription repositoryDescription;
  protected IdentifierService identifierService;
  protected MetadataService metadataService;
  protected PreparationService preparationService;

  @Mock
  protected MessageService messageService;
  @Mock
  protected HistoryService historyService;
  @Mock
  protected FileFormatService fileFormatService;
  @Mock
  protected DepositService depositService;
  @Mock
  protected DepositDataFileService depositDataFileService;

  @Mock
  protected FallbackArchivalInfoPackageRemoteResourceService fallbackAipRemoteService;
  @Mock
  protected FallbackLanguageRemoteResourceService fallbackLanguageRemoteService;
  @Mock
  protected FallbackLicenseRemoteResourceService fallbackLicenseRemoteService;
  @Mock
  protected FallbackMetadataTypeRemoteResourceService fallbackMetadataTypeRemoteService;
  @Mock
  protected FallbackOrganizationalUnitRemoteResourceService fallbackOrgUnitRemoteService;
  @Mock
  protected FallbackPersonRemoteResourceService fallbackPersonRemoteService;
  @Mock
  protected FallbackPreservationPolicyRemoteResourceService fallbackPreservationPolicyRemoteService;
  @Mock
  protected FallbackSubmissionInfoPackageRemoteResourceService fallbackSipRemoteService;
  @Mock
  protected FallbackArchivePublicMetadataRemoteResourceService archivePublicMetadataRemoteService;
  @Mock
  protected FallbackArchivePrivateMetadataRemoteResourceService archivePrivateMetadataRemoteService;
  @Mock
  protected FallbackArchiveTypeRemoteResourceService fallbackArchiveTypeResourceService;
  @Mock
  protected TrustedArchivePrivateMetadataRemoteResourceService trustedArchiveMetadataRemoteService;
  @Mock
  protected TrustedDepositRemoteResourceService trustedDepositRemoteService;

  @AfterEach
  public void purgeData() {
    FileTool.deleteFolder(this.workingDir);
  }

  public void runTestWithMetadata(Path source, DLCMMetadataVersion version) throws Exception {
    final Deposit depositWithMetadata = this.getDepositWithMetadata(source, version);
    this.runDepositTest(this.dlcmProperties.getPreingestLocation(), depositWithMetadata, depositWithMetadata.getDataFiles().size());
  }

  public void runTestWithoutMetadata(Path source, DLCMMetadataVersion version) throws Exception {
    final Deposit depositWithoutMetadata = this.getDepositWithoutMetadata(source, version);
    this.runDepositTest(this.dlcmProperties.getPreingestLocation(), depositWithoutMetadata, depositWithoutMetadata.getDataFiles().size() + 1);
  }

  public void setUp() {
    // Mock the findOne method for Organizational Unit
    final OrganizationalUnit orgUnit = new OrganizationalUnit();
    orgUnit.setResId(ORG_UNIT_ID);
    orgUnit.setName("Organization Unit");
    when(this.fallbackOrgUnitRemoteService.findOneWithCache(ORG_UNIT_ID)).thenReturn(orgUnit);

    // Mock the findOne method for Person
    final Person person = new Person();
    person.setResId(PERSON_ID);
    person.setFirstName("First Name");
    person.setLastName("Last Name");
    when(this.fallbackPersonRemoteService.findOneWithCache(PERSON_ID)).thenReturn(person);

    // Mock the findOne method for License
    final License license = new License();
    license.setResId(LICENSE_ID);
    license.setOpenLicenseId(LICENSE_ID);
    license.setTitle("License");
    try {
      license.setUrl(new URL("http://www.dlcm.ch"));
    } catch (final MalformedURLException e) {
      throw new SolidifyRuntimeException("License URL error", e);
    }
    when(this.fallbackLicenseRemoteService.findOneWithCache(LICENSE_ID)).thenReturn(license);

    // Mock the findOne method for metadataType
    final MetadataType xmlMetadataType = new MetadataType();
    xmlMetadataType.setResId(XML_TYPE_ID);
    xmlMetadataType.setMetadataFormat(MetadataFormat.XML);
    xmlMetadataType.setName("XML");
    xmlMetadataType.setVersion("1.0");
    when(this.fallbackMetadataTypeRemoteService.findOneWithCache(XML_TYPE_ID)).thenReturn(xmlMetadataType);
    final MetadataType jsonMetadataType = new MetadataType();
    jsonMetadataType.setResId(JSON_TYPE_ID);
    jsonMetadataType.setMetadataFormat(MetadataFormat.JSON);
    jsonMetadataType.setName("JSON");
    jsonMetadataType.setVersion("1.0");
    when(this.fallbackMetadataTypeRemoteService.findOneWithCache(JSON_TYPE_ID)).thenReturn(jsonMetadataType);

    // Mock the getDepositMetadataCategoryCount method on DepositService
    final String[] metatdataTypes = { "metadata", "nometadata" };
    for (String metadataType : metatdataTypes) {
      for (DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
        final String depositId = "faust-1072055880-" + metadataType + "-" + version.getVersion();
        when(this.depositService.countDataFilesWithDataCategoryAndDataType(depositId, DataCategory.Package,
                DataCategory.Metadata)).thenReturn(0, 1);
        when(this.depositService.countDataFilesWithDataCategoryAndDataType(depositId, DataCategory.Internal,
                DataCategory.DatasetThumbnail)).thenReturn(0);
        when(this.depositService.countDataFilesWithDataCategory(depositId, DataCategory.Primary)).thenReturn(7);
      }
    }
  }

  private void addDataFiles(Deposit deposit, Path source) {
    // Page images
    for (int i = 2; i < 9; i++) {
      final Path file = Paths.get(source.toString(), deposit.getResId(), Package.RESEARCH.getName()).resolve("1072055880_00" + i + ".jpg");
      final DepositDataFile ddf = new DepositDataFile(deposit);
      ddf.setDataCategory(DataCategory.Primary);
      ddf.setDataType(DataCategory.Digitalized);
      ddf.setSourceData(file.toUri());
      ddf.setFileSize(FileTool.getSize(file));

      ddf.getCreation().setWhen(OffsetDateTime.now());
      ddf.getLastUpdate().setWhen(OffsetDateTime.now());
      if (!deposit.addItem(ddf)) {
        throw new SolidifyRuntimeException("Cannot add data file " + ddf.getFileName());
      }
    }
    // JSON metadata
    final Path json = Paths.get(source.toString(), deposit.getResId(), Package.METADATA.getName()).resolve("1072055880.json");
    final DepositDataFile jsonddf = new DepositDataFile(deposit);
    jsonddf.setDataCategory(DataCategory.Package);
    jsonddf.setDataType(DataCategory.CustomMetadata);
    jsonddf.setMetadataType(this.fallbackMetadataTypeRemoteService.findOne(JSON_TYPE_ID));
    jsonddf.setSourceData(json.toUri());
    jsonddf.setFileSize(FileTool.getSize(json.toUri()));

    jsonddf.getCreation().setWhen(OffsetDateTime.now());
    jsonddf.getLastUpdate().setWhen(OffsetDateTime.now());
    if (!deposit.addItem(jsonddf)) {
      throw new SolidifyRuntimeException("Cannot add data file " + jsonddf.getFileName());
    }
    // XML metadata
    final Path xml = Paths.get(source.toString(), deposit.getResId(), Package.METADATA.getName()).resolve("1072055880.xml");
    final DepositDataFile xmlddf = new DepositDataFile(deposit);
    xmlddf.setDataCategory(DataCategory.Package);
    xmlddf.setDataType(DataCategory.CustomMetadata);
    xmlddf.setMetadataType(this.fallbackMetadataTypeRemoteService.findOne(XML_TYPE_ID));
    xmlddf.setSourceData(xml.toUri());
    xmlddf.setFileSize(FileTool.getSize(xml.toUri()));
    xmlddf.getCreation().setWhen(OffsetDateTime.now());
    xmlddf.getLastUpdate().setWhen(OffsetDateTime.now());
    if (!deposit.addItem(xmlddf)) {
      throw new SolidifyRuntimeException("Cannot add data file " + xmlddf.getFileName());
    }
    // Dataset Thumbnail
    final Path thumbnail = Paths.get(source.toString(), deposit.getResId(), "internal").resolve("dataset.thumbnail");
    if (FileTool.checkFile(thumbnail)) {
      final DepositDataFile thumbnailddf = new DepositDataFile(deposit);
      thumbnailddf.setDataCategory(DataCategory.Internal);
      thumbnailddf.setDataType(DataCategory.DatasetThumbnail);
      thumbnailddf.setSourceData(xml.toUri());
      thumbnailddf.setFileSize(FileTool.getSize(xml.toUri()));
      thumbnailddf.getCreation().setWhen(OffsetDateTime.now());
      thumbnailddf.getLastUpdate().setWhen(OffsetDateTime.now());
      if (!deposit.addItem(thumbnailddf)) {
        throw new SolidifyRuntimeException("Cannot add data file " + thumbnailddf.getFileName());
      }
    }
  }

  private void checkDataFiles(Deposit deposit) {
    for (final DepositDataFile df : deposit.getDataFiles()) {
      // Copy file

      if (df.getDataCategory() == DataCategory.Package && df.getDataType() == DataCategory.Metadata) {
        XMLTool.wellformed(Paths.get(df.getFinalData()));
      }
      assertTrue(FileTool.checkFile(df.getFinalData()), "Cannot find data file");
      assertEquals(df.getFileSize().longValue(), FileTool.getSize(df.getSourceData()), "Wrong size");
      assertEquals(DataFileStatus.READY, df.getStatus(), "Wrong status");
    }
  }

  private void completeDeposit(Deposit deposit, DLCMMetadataVersion version) {
    final String resId = StringTool.generateResId();
    deposit.setMetadataVersion(version);
    deposit.setStatus(DepositStatus.IN_PROGRESS);
    deposit.setDoi(this.identifierService.createDOI(resId));
    deposit.setArk(this.identifierService.createARK(resId));
    deposit.setTitle("Faust 1072055880");
    deposit.setDescription("Testing");
    deposit.setAccess(Access.PUBLIC);
    if (version.getVersionNumber() < DLCMMetadataVersion.V2_0.getVersionNumber()) {
      deposit.setDataSensitivity(DataTag.UNDEFINED);
    } else {
      deposit.setDataSensitivity(DataTag.BLUE);
    }
    deposit.setLicenseId(LICENSE_ID);
    deposit.setPublicationDate(LocalDate.now());
    deposit.setOrganizationalUnitId(ORG_UNIT_ID);
    deposit.setOrganizationalUnit(this.fallbackOrgUnitRemoteService.findOne(ORG_UNIT_ID));
    deposit.getCreation().setWhen(OffsetDateTime.now());
    deposit.getLastUpdate().setWhen(OffsetDateTime.now());
    final SubmissionPolicy submissionPolicy = new SubmissionPolicy();
    submissionPolicy.setResId("submissionPolicyId");
    submissionPolicy.setName("test");
    submissionPolicy.setSubmissionApproval(false);
    submissionPolicy.setSubmissionAgreementType(SubmissionAgreementType.WITHOUT);
    deposit.setSubmissionPolicy(submissionPolicy);
    deposit.setSubmissionPolicyId(submissionPolicy.getResId());
    final PreservationPolicy preservationPolicy = new PreservationPolicy();
    preservationPolicy.setResId("preservationPolicyId");
    preservationPolicy.setName("test");
    preservationPolicy.setDispositionApproval(true);
    preservationPolicy.setRetention(1);
    deposit.setPreservationPolicy(preservationPolicy);
    deposit.setPreservationPolicyId(preservationPolicy.getResId());
    final ArchiveType masterType = new ArchiveType();
    masterType.setResId(ch.dlcm.model.xml.dlcm.v4.mets.ResourceType.DATASET.value().toUpperCase());
    masterType.setTypeName(ch.dlcm.model.xml.dlcm.v4.mets.ResourceType.DATASET.value());
    final ArchiveType archiveType = new ArchiveType();
    archiveType.setResId("archiveTypeId");
    archiveType.setMasterType(masterType);
    archiveType.setTypeName("my dataset");
    deposit.setArchiveTypeId(archiveType.getResId());
    deposit.setArchiveType(archiveType);
  }

  private Deposit getDepositWithMetadata(Path source, DLCMMetadataVersion version) {
    final Deposit deposit = new Deposit();
    deposit.init();
    deposit.setResId("faust-1072055880-metadata-" + version.getVersion());
    this.completeDeposit(deposit, version);
    this.addDataFiles(deposit, source);
    // DLCM metadata
    final Path xml = Paths.get(source.toString(), deposit.getResId()).resolve(Package.METADATA_FILE.getName());
    final DepositDataFile xmlddf = new DepositDataFile(deposit);
    xmlddf.setDataCategory(DataCategory.Package);
    xmlddf.setDataType(DataCategory.Metadata);
    xmlddf.setSourceData(xml.toUri());
    xmlddf.setFileSize(FileTool.getSize(xml.toUri()));
    if (!deposit.addItem(xmlddf)) {
      throw new SolidifyRuntimeException("Cannot add data file " + xmlddf.getFileName());
    }
    return deposit;
  }

  private Deposit getDepositWithoutMetadata(Path source, DLCMMetadataVersion version) {
    final Deposit deposit = new Deposit();
    deposit.init();
    deposit.setResId("faust-1072055880-nometadata-" + version.getVersion());
    this.completeDeposit(deposit, version);
    this.addDataFiles(deposit, source);
    if (!deposit.addItem(this.fallbackPersonRemoteService.findOneWithCache(PERSON_ID))) {
      throw new SolidifyRuntimeException("Cannot add contributor");
    }
    return deposit;
  }

  private Tool getTool(String name) {
    final Tool t = new Tool();
    t.setName(name);
    t.setDescription("Testing tool");
    t.setVersion("1.0.0");
    return t;
  }

  private void processDataFiles(Path home, Deposit deposit) throws NoSuchAlgorithmException, IOException {
    for (final DepositDataFile df : deposit.getDataFiles()) {
      // Copy file
      final Path target = df.defineTargetFile(home.resolve(deposit.getResId()).toString());
      FileTool.copyFile(Paths.get(df.getSourceData()), target);
      df.setFinalData(target.toUri());
      df.setFileSize(FileTool.getSize(target));
      // Compliance level
      if (df.getDataCategory() == DataCategory.Package && df.getDataType() == DataCategory.Metadata) {
        df.setComplianceLevel(ComplianceLevel.FULL_COMPLIANCE);
      } else {
        df.setComplianceLevel(ComplianceLevel.WEAK_COMPLIANCE);
      }
      // generate checksum
      this.setChecksum(df);
      // generate tool info
      this.setFileFormat(df);
      this.setViruscheck(df);
      df.setStatus(DataFileStatus.READY);
    }
  }

  private void runDepositTest(String home, Deposit deposit, int fileNumber) throws JAXBException, IOException, NoSuchAlgorithmException {
    // Check metadata
    if (!this.preparationService.depositHasMetadata(deposit)) {
      this.metadataService.generateDefaultDataCiteFromDeposit(deposit);
    }
    // process data file
    this.processDataFiles(Paths.get(home), deposit);
    // Check data files
    this.checkDataFiles(deposit);
    // Check deposit
    this.preparationService.checkDeposit(deposit);
    // Generate METS metadata
    this.metadataService.generateMetadata(deposit, Paths.get(home).resolve(deposit.getResId()));
    // Generate SIP
    this.preparationService.generateSIP(deposit);
    final Path sipFile = Paths.get(home).resolve(deposit.getResId() + SolidifyConstants.ZIP_EXT);
    assertTrue(FileTool.checkFile(sipFile), "[SIP] Cannot find file");
    final ZipTool sip = new ZipTool(sipFile.toUri());
    assertTrue(sip.checkZip(), "[SIP] Incorrect zip file");
    assertEquals(fileNumber, sip.getFileNumber(), "[SIP] Incorrect file number");
  }

  private void setChecksum(DepositDataFile df) throws NoSuchAlgorithmException, IOException {
    final String[] algoList = this.dlcmProperties.getParameters().getChecksumList();
    final String[] checksumList = ChecksumTool.computeChecksum(
            new BufferedInputStream(new FileInputStream(Paths.get(df.getFinalData()).toFile()), SolidifyConstants.BUFFER_SIZE),
            algoList);
    for (int i = 0; i < algoList.length; i++) {
      df.getChecksums()
              .add(new DataFileChecksum(ChecksumAlgorithm.valueOf(algoList[i]), ChecksumType.COMPLETE, ChecksumOrigin.DLCM, checksumList[i]));
    }
  }

  private void setFileFormat(DepositDataFile df) {
    final FileFormat ff = new FileFormat();
    String contentType = FileTool.getContentType(df.getFinalData()).toString();
    ff.setContentType(contentType);
    ff.setFormat(contentType);
    ff.setTool(this.getTool("FileFormat"));
    df.setFileFormat(ff);
  }

  private void setViruscheck(DepositDataFile df) {
    final VirusCheck vc = new VirusCheck();
    vc.setCheckDate(OffsetDateTime.now());
    vc.setTool(this.getTool("VirusCheck"));
    df.setVirusCheck(vc);
  }

}

/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM PreIngest - ZipPreparationServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.dlcm.test.preparation;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.nio.file.Paths;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.GitInfoProperties;

import ch.dlcm.DLCMMetadataVersion;
import ch.dlcm.config.DLCMProperties;
import ch.dlcm.config.DLCMRepositoryDescription;
import ch.dlcm.preparation.zip.ZipPreparationService;
import ch.dlcm.service.IdentifierService;
import ch.dlcm.service.MetadataService;

@ActiveProfiles({ "pre-zip", "sec-noauth" })
@ExtendWith(SpringExtension.class)
class ZipPreparationServiceTest extends PreparationServiceTest {

  @Test
  void preparationWithMetadataTest() {
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.runTestWithMetadata(Paths.get("src", "test", "resources", "deposits").toAbsolutePath(), version));
    }
  }

  @Test
  void preparationWithoutMetadataTest() {
    for (final DLCMMetadataVersion version : DLCMMetadataVersion.values()) {
      assertDoesNotThrow(() -> this.runTestWithoutMetadata(Paths.get("src", "test", "resources", "deposits").toAbsolutePath(), version));
    }
  }

  @BeforeEach
  void setup() {
    super.setUp();
    // GitInfo
    // DLCM Properties
    this.dlcmProperties = new DLCMProperties(new SolidifyProperties(new GitInfoProperties()), new MultipartProperties());
    this.dlcmProperties.setHome(this.workingDir.toAbsolutePath().toString());
    this.dlcmProperties.getParameters().setDefaultChecksum("MD5");
    this.dlcmProperties.getParameters().setChecksumList(new String[] { "MD5" });
    // DLCM Repository description
    this.repositoryDescription = new DLCMRepositoryDescription(this.gitInfoProperties);
    // Services
    this.preparationService = new ZipPreparationService(
            this.dlcmProperties,
            this.messageService,
            this.historyService,
            this.depositService,
            this.fallbackSipRemoteService,
            this.fallbackAipRemoteService,
            this.trustedArchiveMetadataRemoteService);
    this.metadataService = new MetadataService(
            this.fallbackLanguageRemoteService,
            this.fallbackOrgUnitRemoteService,
            this.fallbackAipRemoteService,
            this.historyService,
            this.dlcmProperties,
            this.repositoryDescription,
            this.messageService,
            this.gitInfoProperties,
            this.fileFormatService,
            this.fallbackMetadataTypeRemoteService,
            this.fallbackPersonRemoteService,
            this.fallbackLicenseRemoteService,
            this.archivePublicMetadataRemoteService,
            this.archivePrivateMetadataRemoteService,
            this.fallbackPreservationPolicyRemoteService,
            this.fallbackArchiveTypeResourceService,
            this.trustedDepositRemoteService);
    this.identifierService = new IdentifierService(this.repositoryDescription);
  }

}
